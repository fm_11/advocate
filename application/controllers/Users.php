<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('common/insert_model', 'common/edit_model', 'common/custom_methods_model', 'User_role'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data['title'] = 'User';
        $data['heading_msg'] = "User List";
        $data['is_show_button'] = "add";
        $query = "SELECT u.*,l.`role_name` FROM tbl_user AS u
LEFT JOIN `user_roles` AS l ON l.`id` = u.`user_role` WHERE u.`is_school360_user` = '0'";
        $data['users'] = $this->custom_methods_model->query_data($query);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('users/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $data = array();

        if ($_POST) {
           $file_name = $_FILES['txtPhoto']['name'];
           $user_photo_name = "";
           if ($file_name != '') {
               $this->load->library('upload');
               $config['upload_path'] = MEDIA_FOLDER . '/logos/';
               $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
               $config['max_size'] = '5000';
               $config['max_width'] = '5000';
               $config['max_height'] = '5000';
               $this->upload->initialize($config);
               $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
               foreach ($_FILES as $field => $file) {
                   if ($file['error'] == 0) {
                       $new_photo_name = "user_" . time() . "_" . date('Y-m-d') . "." . $extension;
                       if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/logos/" . $new_photo_name)) {
                           $user_photo_name = $new_photo_name;
                       } else {
                           $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                           $this->session->set_userdata($sdata);
                           redirect("users/index");
                       }
                   } else {
                       $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                       $this->session->set_userdata($sdata);
                       redirect("users/index");
                   }
               }
           }


            $data = array(
                'name' => $this->input->post("name"),
                'user_name' => $this->input->post("user_name"),
                'mobile' => $this->input->post("mobile"),
                'user_password' => md5($this->input->post("password")),
                'user_role' => $this->input->post("role_id"),
                'photo_location' => $user_photo_name
            );

            $insert = $this->insert_model->add("tbl_user", $data);

            if ($insert == true) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("users/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("users/add");
            }
        } else {
            $data['title'] = 'Add New User';
            $data['heading_msg'] = "Add New User";
            $data['action'] = '';
            $data['is_show_button'] = "index";
            $data['user_roles'] = $this->User_role->get_list();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('users/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function edit($id=null)
    {
        $data = array();

        if ($_POST) {

          $file_name = $_FILES['txtPhoto']['name'];
          $user_photo_name = "";
          if ($file_name != '') {
              $this->load->library('upload');
              $config['upload_path'] = MEDIA_FOLDER . '/logos/';
              $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
              $config['max_size'] = '5000';
              $config['max_width'] = '5000';
              $config['max_height'] = '5000';
              $this->upload->initialize($config);
              $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
              foreach ($_FILES as $field => $file) {
                  if ($file['error'] == 0) {
                      $new_photo_name = "user_" . time() . "_" . date('Y-m-d') . "." . $extension;
                      if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/logos/" . $new_photo_name)) {
                          $user_photo_name = $new_photo_name;
                      } else {
                          $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                          $this->session->set_userdata($sdata);
                          redirect("users/index");
                      }
                  } else {
                      $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                      $this->session->set_userdata($sdata);
                      redirect("users/index");
                  }
              }
          }

          $updated_data = array();
          $updated_data['name'] = $this->input->post("name");
          $updated_data['user_name'] = $this->input->post("user_name");
          $updated_data['mobile'] = $this->input->post("mobile");
          $updated_data['user_role'] = $this->input->post("role_id");

          $password = $this->input->post("password");
          if($password != ''){
            $updated_data['user_password'] =  md5($password);
          }

          if($user_photo_name != ""){
            $updated_data['photo_location'] = $user_photo_name;
          }

            $id = $this->input->post("id");
            //echo $id; die;
            //$insert=$this->insert_model->add("tbl_user", $data);
            $this->db->where('id', $id);
            $this->db->update('tbl_user', $updated_data);

            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("users/index");
        } else {
            $data['title'] = 'Edit User';
            $data['heading_msg'] = "Edit User";
            $data['user_roles'] = $this->User_role->get_list();
            $data['user_info'] = $this->custom_methods_model->get_where("tbl_user", "id", $id);
            // echo '<pre>'; print_r($data['user_info']); die();
            $data['action'] = 'edit';
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('users/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function delete($id)
    {
        $this->db->delete('tbl_user', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("users/index");
    }
}
