<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing_page extends CI_Controller {

     function __construct() {

        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
    }

	public function index(){
	    $data = array();
	    $this->load->view('landing_page/index', $data);
	}

}
