<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Student Information Report';
        $data['heading_msg'] = 'Student Information Report';
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $data['HeaderInfo'] = $SchoolInfo;
            $data['name'] = $this->input->post("name");
            $data['roll'] = $this->input->post("roll");
            $data['shift_id'] = $this->input->post("shift_id");
            $data['class_id'] = $this->input->post("class_id");
            $data['section_id'] = $this->input->post("section_id");
            $data['group'] = $this->input->post("group");
            $data['religion'] = $this->input->post("religion");
            $data['gender'] = $this->input->post("gender");
            $data['blood_group_id'] = $this->input->post("blood_group_id");
            $data['student_status'] = $this->input->post("student_status");

            $cond = array();
            $cond['name'] = $this->input->post("name");
            $cond['roll'] = $this->input->post("roll");
            $cond['class_id'] = $this->input->post("class_id");
            $cond['shift_id'] = $this->input->post("shift_id");
            $cond['section_id'] = $this->input->post("section_id");
            $cond['group'] = $this->input->post("group");
            $cond['religion'] = $this->input->post("religion");
            $cond['gender'] = $this->input->post("gender");
            $cond['blood_group_id'] = $this->input->post("blood_group_id");
            $cond['student_status'] = $this->input->post("student_status");
            $data['students'] = $this->Student->get_all_student_list(0, 0, $cond);
            $data['post_data'] = $_POST;
            $data['report'] = $this->load->view('student_reports/report', $data, true);
            $this->load->view('report_content/report_main_content', $data);
        }
        // if (isset($_POST['pdf_download'])) {
        //     $data['is_pdf'] = 1;
        //     //Dom PDF
        //     $this->load->library('mydompdf');
        //     $html = $this->load->view('student_reports/report', $data, true);
        //     $this->mydompdf->createPDF($html, 'StudentReport', true, $paper='A4', $orientation='landscape');
        // //Dom PDF
        // } else {
        else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['shift_list'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->db->query("SELECT * FROM `tbl_student_group`")->result_array();
            $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_reports/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
