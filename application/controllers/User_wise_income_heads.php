
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_wise_income_heads extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Account', 'Admin_login'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = "User Wise Asset Head Configuration";
        $data['heading_msg'] = "User Wise Asset Head Configuration";
        if ($_POST) {
            $loop_time = $this->input->post("loop_time");
            $this->db->empty_table('tbl_user_wise_asset_head');
            $i = 0;
            while ($i < $loop_time) {
                $asset_category_id = $this->input->post("asset_category_id_" . $i);
                if ($asset_category_id != '') {
                    $data = array();
                    $data['user_id'] = $this->input->post("user_id_" . $i);
                    $data['asset_category_id'] = $asset_category_id;
                    $data['opening_balance'] = 0;
                    $this->db->insert("tbl_user_wise_asset_head", $data);
                }
                $i++;
            }
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('user_wise_income_heads/index');
        }
        $data['user_info'] = $this->db->query("SELECT u.`name`,u.`id` AS user_id_main,ui.* FROM `tbl_user` AS u
LEFT JOIN `tbl_user_wise_asset_head` AS ui ON ui.`user_id` = u.`id`")->result_array();
        $data['asset_category'] = $this->db->query("SELECT * FROM tbl_asset_category")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_wise_income_heads/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
