<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class File_upload extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Advocate','Student', 'Message','Admin_login'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }

    }

    public function index()
    {
        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['title'] = $this->lang->line('forms').' '.$this->lang->line('file_upload_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $forms_types_id = $this->input->post("forms_types_id");
            $sdata['name'] = $name;
            $sdata['forms_types_id'] = $forms_types_id;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['forms_types_id'] = $forms_types_id;
        } else {
            $name = $this->session->userdata('name');
            $forms_types_id = $this->session->userdata('forms_types_id');
            $cond['name'] = $name;
            $cond['forms_types_id'] = $forms_types_id;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('form/index/');
        $data['is_show_button'] = "add";
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $config['total_rows'] = count($this->Advocate->get_all_file_upload_list(0, 0, $cond));
        $data['file_upload_list'] = $this->Advocate->get_all_file_upload_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['language'] =$lang;
        $data['forms_types'] = $this->Advocate->get_forms_types_list_for_dropdown_by_language($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('file_upload/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }




    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = $type.time() . "_" . date('Y-m-d') . '.' .end($ext);
      // print_r(rand());
      // die();
      $this->load->library('upload');
      $config = array(
          'upload_path' => "core_media/file_upload/",
          'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
          'max_size' => "99999999999",
          'max_height' => "3000",
          'max_width' => "3000",
          'file_name' => $new_file_name
        );
      $this->upload->initialize($config);
      if (!$this->upload->do_upload($filename)) {
        return '0';
      } else {
        return $new_file_name;
      }
    }

    public function add()
    {
      $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $data = array();
            $data['name']=$this->input->post('name');
            $data['forms_types_id']=$this->input->post('forms_types_id');
            if (isset($_FILES['txtPhoto']) && $_FILES['txtPhoto']['name'] != '') {
                    $image_name = 'txtPhoto';
                    $small = explode(' ',$this->input->post('name'));
                    $filename="file_";
                    // for ($i=0; $i <count($small) ; $i++) {
                    //   $filename=$filename.$small[$i]."_";
                    // }
                    $new_file_name = $this->my_file_upload($image_name, $filename);
                   if ($new_file_name == '0') {
                        $sdata['exception'] = "Sorry file doesn't upload." . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("file_upload/add");
                    }
                    $data['file_path'] = $new_file_name;
             }
            if($this->Advocate->add_file_upload($data))
            {
              $sdata['message'] = $this->lang->line('add_success_message');
              $this->session->set_userdata($sdata);
            }else{
              $sdata['exception'] = "Information could not add";
              $this->session->set_userdata($sdata);
            }
            redirect("file_upload/add");

        } else {
            $data = array();
            $data['action'] = 'add';
            $data['is_show_button'] = "index";
            $data['title'] =  $this->lang->line('file_add');
            $data['heading_msg'] =  $this->lang->line('file_add');
            $data['forms_types'] = $this->Advocate->get_forms_types_list_for_dropdown_by_language($lang);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('file_upload/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



    public function edit($id = null)
    {
      $lang=$this->session->userdata('site_lang');
        if ($_POST) {
          $data = array();
          $old_file="";
          $post_id=$this->input->post('id');
          $data['id']=$post_id;
          $data['name']=$this->input->post('name');
          $data['forms_types_id']=$this->input->post('forms_types_id');
          if (isset($_FILES['txtPhoto']) && $_FILES['txtPhoto']['name'] != '') {
                  $image_name = 'txtPhoto';
                  $small = explode(' ',$this->input->post('name'));
                  $filename="file_";
                  // for ($i=0; $i <count($small) ; $i++) {
                  //   $filename=$filename.$small[$i]."_";
                  // }
                  $new_file_name = $this->my_file_upload($image_name, $filename);
                 if ($new_file_name == '0') {
                      $sdata['exception'] = "Sorry file doesn't upload." . $this->upload->display_errors();
                      $this->session->set_userdata($sdata);
                      redirect("file_upload/edit/".$post_id);
                  }
                  $data['file_path'] = $new_file_name;
                  $old_file=$this->input->post('oldtxtPhoto');
           }
          if($this->Advocate->edit_file_upload($data,$post_id))
          {
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
              $this->remove_file($old_file);

            redirect("file_upload/index");
          }else{
            $sdata['exception'] = "Information could not updated";
            $this->session->set_userdata($sdata);
            redirect("file_upload/edit/".$post_id);
          }

        } else {
            $data = array();
            $data['title'] =$data['heading_msg'] = $this->lang->line('file').''.$this->lang->line('update');
            $data['action'] = 'edit';
            $data['is_show_button'] = "index";
            $data['files']=$this->Advocate->read_file_upload($id);
            $data['forms_types'] = $this->Advocate->get_forms_types_list_for_dropdown_by_language($lang);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('file_upload/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



    public function delete($id)
    {
       $files=$this->Advocate->read_file_upload($id);
        $old_file=$files->file_path;
        $this->Advocate->delete_file_upload($id);
        $this->remove_file($old_file);
        $sdata['message'] =$this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("file_upload/index");
    }
    function remove_file($value='')
    {
      if (!empty($value)) {
        $value="core_media/file_upload/".$value;
        unlink($value);
      }
    }

}
