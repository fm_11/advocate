<?php

class Case_file_upload extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate','Email_process'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('case_file_upload_list');
        $cond = array();
        if ($_POST) {
            $case_master_id = $this->input->post("case_master_id");
            $sdata['case_master_id'] = $case_master_id;
            $this->session->set_userdata($sdata);
            $cond['case_master_id'] = $case_master_id;
        } else {
            $case_master_id = $this->session->userdata('case_master_id');
            $cond['case_master_id'] = $case_master_id;
        }
        $lang=$this->session->userdata('site_lang');
        $this->load->library('pagination');
        $config['base_url'] = site_url('case_file_upload/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Advocate->get_all_case_file_upload_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['case_file_list'] = $this->Advocate->get_all_case_file_upload_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['case_list'] = $this->Advocate->get_all_case_list_for_dropdown_by_language($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_file_upload/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $file_data = array();
            $file_loop_time = $this->input->post("file_upload_num_of_row");
            $case_master_id = $this->input->post("case_master_id");
            $file_label_name = $this->input->post("name");
            $reference_id=$this->Advocate->get_max_reference_no_by_case_file_details();
            $j = 0;
            while ($j < $file_loop_time) {
              if (isset($_FILES['file_name_' . $j]) && $_FILES['file_name_' . $j]['name'] != '') {
                      $image_name = 'file_name_' . $j;
                      $new_file_name = $this->my_file_upload($image_name, "case_file".$j."_");
                     if ($new_file_name == '0') {
                          $sdata['exception'] = "Sorry! this file '".$this->input->post("file_label_name_". $j)."'  doesn't upload." . $this->upload->display_errors();
                          $this->session->set_userdata($sdata);
                          redirect("case_file_upload/add/");
                      }
                      $file_data[$j]['case_master_id']=$case_master_id;
                      $file_data[$j]['label_name']=$file_label_name;
                      $file_data[$j]['file_path']=$new_file_name;
                      $file_data[$j]['reference_id']=$reference_id;
              }
              $j++;
            }
            if(count($file_data)>0)
            {
              $this->db->insert_batch('ad_case_file_details', $file_data);
              $sdata['message'] = $this->lang->line('add_success_message');
              $this->session->set_userdata($sdata);
            }
            else{
              $sdata['exception'] = "Information could not add";
              $this->session->set_userdata($sdata);
              redirect("case_file_upload/add");
            }
        }

        $data = array();
        $lang=$this->session->userdata('site_lang');
        $data['heading_msg'] = $data['title'] = $this->lang->line('Case_file_upload');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['language']=$lang;
        $data['case_list'] = $this->Advocate->get_all_case_list_for_dropdown_by_language($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_file_upload/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id=NULL)
    {
      $lang=$this->session->userdata('site_lang');
      if ($_POST) {
          $file_data = array();
          $file_loop_time = $this->input->post("file_upload_num_of_row");
          $case_master_id = $this->input->post("case_master_id");
          $file_label_name = $this->input->post("name");
          $reference_id=$this->input->post("reference_id");
          $j = 0;
          while ($j < $file_loop_time) {
            if (isset($_FILES['file_name_' . $j]) && $_FILES['file_name_' . $j]['name'] != '') {
                    $image_name = 'file_name_' . $j;
                    $new_file_name = $this->my_file_upload($image_name, "case_file".$j."_");
                   if ($new_file_name == '0') {
                        $sdata['exception'] = "Sorry! this file '".$this->input->post("file_label_name_". $j)."'  doesn't upload." . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("case_file_upload/edit/".$reference_id);
                    }
                    $file_data[$j]['case_master_id']=$case_master_id;
                    $file_data[$j]['label_name']=$file_label_name;
                    $file_data[$j]['file_path']=$new_file_name;
                    $file_data[$j]['reference_id']=$reference_id;
            }
            $j++;
          }
          if(count($file_data)>0)
          {
            $this->db->insert_batch('ad_case_file_details', $file_data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("case_file_upload/index");
          }
          else{
            $data=array();
            $data['case_master_id']=$case_master_id;
            $data['label_name']=$file_label_name;
            $data['reference_id']=$reference_id;
            $this->Advocate->edit_case_file_details($data,$reference_id);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("case_file_upload/index");
          }
      }

      $data = array();
      $lang=$this->session->userdata('site_lang');
      $data['heading_msg'] = $data['title'] = $this->lang->line('case_file_upload');
      $data['action'] = 'edit/' . $id;
      $data['is_show_button'] = "index";
      $data['language']=$lang;
      $data['case_file_details']=$this->Advocate->get_case_file_details_by_reference_no($id);
      $data['case_list'] = $this->Advocate->get_all_case_list_for_dropdown_by_language($lang);
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('case_file_upload/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function view($id)
    {
        $mail_config=  $this->Advocate->get_email_configuration();
        $lang=$this->session->userdata('site_lang');
        if(empty($mail_config))
        {
          $sdata['exception'] = "Please email configuration first.";
          $this->session->set_userdata($sdata);
          redirect("case_file_upload/index");
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('case_file_upload');
        $data['action'] = 'view';
        $data['is_show_button'] = "index";
        $data['language']=$lang;
        $data['case_file_details']=$this->Advocate->get_case_file_details_by_reference_no($id);

        $data['mail_config']=  $mail_config;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_file_upload/view', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete_case_file()
    {
        $id = $this->input->get('id', true);
        $case_file=$this->Advocate->read_case_file_details($id);
        $old_file=$case_file->file_path;
        $this->Advocate->delete_case_file_details($id);
        $this->remove_file($old_file);
    }


    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = $type.time() . "_" . date('Y-m-d') . '.' .end($ext);
      // print_r(rand());
      // die();
      $this->load->library('upload');
      $config = array(
          'upload_path' => MEDIA_FOLDER."/case_file/",
          'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
          'max_size' => "99999",
          'max_height' => "6000",
          'max_width' => "6000",
          'file_name' => $new_file_name
        );
      $this->upload->initialize($config);
      if (!$this->upload->do_upload($filename)) {
        return '0';
      } else {
        return $new_file_name;
      }
    }
    function remove_file($value='')
    {
      if (!empty($value)) {
        $value=MEDIA_FOLDER."/case_file/".$value;
        unlink($value);
      }
    }

    function email_send(){
            if($_POST){
              $receivers = $this->input->post('recipient', true);
              $message = $this->input->post('message', true);
              $subject = $this->input->post('subject', true);
              $p_id = $this->input->post('id', true);
              $dear = $this->input->post('dear', true);
              $file_path = $this->input->post('file_path', true);
              $mail_config=  $this->Advocate->get_email_configuration();
              $email_data = array();
              $email_data['receiver_name'] =$dear;
              $email_data['ukildoptor_logo'] =  base_url()."core_media/admin_v3/images/school360-logo.png";
              $email_data['title'] = $subject;
              $email_data['details'] = $message;
              $email_data['signature'] = $mail_config->mail_signatures;
              $email_data['facebook'] = $mail_config->facebook;
              $email_data['twitter'] = $mail_config->twitter;
              $email_data['instagram'] = $mail_config->instagram;
              $email_data['youtube'] = $mail_config->youtube;
              $email_data['linkedin'] = $mail_config->linkedin;
              $email_data['file_link'] = base_url(). MEDIA_FOLDER."/case_file/".$file_path;
              $email_message = $this->load->view('admin_logins/email_template',$email_data,true);
              // echo "<pre>";
              // print_r($email_message);
              // die;
              if($this->Email_process->send_email($receivers,$subject,$email_message,$mail_config)){
                $sdata['message'] = "You are Successfully Message Send.";
                $this->session->set_userdata($sdata);
               redirect("case_file_upload/index");
              }else{
                $sdata['exception'] = "Sorry Message Doesn't Send !";
                $this->session->set_userdata($sdata);
               redirect("case_file_upload/index");
              }
            }else{
               redirect("case_file_upload/index");
            }
          }


}
