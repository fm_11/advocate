<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Sms_purchases extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
  	$this->load->helpers(array('sslc_helper'));
		$this->load->library(array('Shurjopay','session'));
		$this->load->model(array('Message','Admin_login'));
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'SMS Purchase';
		$data['heading_msg'] = "SMS Purchase";
		$data['is_show_button'] = "add";
		$cond = array();
		$this->load->library('pagination');
		$config['base_url'] = site_url('donor_lists/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Message->get_all_sms_purchase_list(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['purchase_list'] = $this->Message->get_all_sms_purchase_list(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_purchases/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add()
	{
		if ($_POST) {
			$user_info = $this->session->userdata('user_info');
			$data = array();
			$data['date'] = $_POST['date'];
			$data['stakeholder'] = $_POST['stakeholder'];
			$data['unit_price'] = $_POST['unit_price'];
			$data['quantity'] = $_POST['quantity'];
			$data['total_price'] = $_POST['total_price'];
			$data['added_by'] = $user_info[0]->id;
			$data['added_on'] = date('Y-m-d H:i:s');
			$this->db->insert("tbl_sms_purchase", $data);
			$insert_id = $this->db->insert_id();

			//$this->makePaymentBySP($insert_id, $data['total_price']);
			$this->makePaymentBySSL($insert_id, $data['total_price'], $user_info[0]->id);
		}
		$data = array();
		$data['title'] = 'SMS Purchase';
		$data['heading_msg'] = "SMS Purchase";
		$data['is_show_button'] = "index";
		$data['msg_config'] = $this->db->query("SELECT * FROM tbl_message_config")->row();
		if($data['msg_config']->stakeholder == 'SPATEINON'){
			$data['message_type'] = 'Non-Masking';
		}else{
			$data['message_type'] = 'Masking';
		}
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_purchases/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	//make payment by SSL
	public function makePaymentBySSL($id, $payable_amount, $user_id){
		$transaction_id_for_payment = "SSLC".uniqid();
		$this->session->set_userdata('transaction_id_for_payment', $transaction_id_for_payment);
		$post_data = array();
		$post_data['total_amount'] = $payable_amount;
		$post_data['currency'] = "BDT";
		$post_data['tran_id'] = $transaction_id_for_payment;
		$post_data['success_url'] = base_url() . 'sms_purchases/success_payment/' . $id . '/' . $user_id;
		$post_data['fail_url'] = base_url() . 'sms_purchases/fail_payment/' . $id . '/' . $user_id;
		$post_data['cancel_url'] = base_url() . 'sms_purchases/cancel_payment/' . $id . '/' . $user_id;
		$post_data['ipn_url'] = base_url() . 'sms_purchases/ipn_listener';

		$SchoolInfo = $this->Admin_login->fetReportHeader();
		# CUSTOMER INFORMATION
		$post_data['cus_name'] = $SchoolInfo[0]['school_name'] . ' (' . $SchoolInfo[0]['school_short_name']. ')';
		$email = $SchoolInfo[0]['email'];
		$post_data['cus_email'] = $email;
		$present_address = $SchoolInfo[0]['address'];
		$post_data['cus_add1'] = $present_address;
		$post_data['cus_city'] = 'Dhaka';
		$post_data['cus_state'] = 'Dhanmondi';
		$post_data['cus_postcode'] = '1207';
		$post_data['cus_country'] = "Bangladesh";
		$post_data['cus_phone'] = $SchoolInfo[0]['mobile'];

		$SchoolInfo = $this->Admin_login->fetReportHeader();
		# SHIPMENT INFORMATION
		$post_data['ship_name'] = $SchoolInfo[0]['school_name'] . ' (' . $SchoolInfo[0]['school_short_name']. ')';;
		$post_data['ship_add1'] = $SchoolInfo[0]['address'];
		$post_data['ship_city'] = 'Dhaka';
		$post_data['ship_state'] = 'Dhanmondi';
		$post_data['ship_postcode'] = '1207';
		$post_data['ship_country'] = "Bangladesh";

		# OPTIONAL PARAMETERS
		$post_data['value_a'] = "ref001";
		$post_data['value_b'] = "ref002";
		$post_data['value_c'] = "ref003";
		$post_data['value_d'] = "ref004";

		$post_data['product_profile'] = "physical-goods";
		$post_data['shipping_method'] = "YES";
		$post_data['num_of_item'] = "1";
		$post_data['product_name'] = "SMS Purchase for Institute";
		$post_data['product_category'] = "Ecommerce";


		$session = array(
			'tran_id' => $post_data['tran_id'],
			'amount' => $post_data['total_amount'],
			'currency' => $post_data['currency']
		);
		$this->session->set_userdata('tarndata', $session);

		//echo "<pre>";
		//print_r($post_data);
		//echo SSLCZ_STORE_PASSWD;die;
		ini_set('display_errors', 'On');
		error_reporting(-1);
		if($this->sslcommerz->RequestToSSLC($post_data, SSLCZ_STORE_ID, SSLCZ_STORE_PASSWD))
		{
			echo "Pending";
			/***************************************
			# Change your database status to Pending.
			 ****************************************/
		}
	}

	public function success_payment($reference_id, $user_id)
	{
		$database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
		$sesdata = $this->session->userdata('tarndata');
		echo '<pre>';
    print_r('success_payment');
		print_r($sesdata);
		die;
//		echo $sesdata['tran_id'];
//		echo "<pre>";
//		print_r($_POST);exit;
		//if(($sesdata['tran_id'] == $_POST['tran_id']) && ($sesdata['amount'] == $_POST['currency_amount']) && ($sesdata['currency'] == 'BDT'))
		if(isset($_POST['tran_id']) && isset($_POST['currency_amount']))
		{
			if($this->sslcommerz->ValidateResponse($_POST['currency_amount'], 'BDT', $_POST))
			{
				if($database_order_status == 'Pending')
				{
					/*****************************************************************************
					# Change your database status to Processing & You can redirect to success page from here
					 ******************************************************************************/
//					echo "Transaction Successful<br>";
//					echo "Processing";
//					echo "<pre>";
//					print_r($_POST);exit;

					$data = array();
					$data['id'] = $reference_id;
					$data['total_payment_amount'] = $_POST['amount'];
					$data['transaction_id'] = $_POST['tran_id'];
					$data['payment_option'] = $_POST['card_type'];
					$data['payment_status'] = "S";
					$this->db->where('id', $data['id']);
					$this->db->update('tbl_sms_purchase', $data);

					$this->sendDataToFamilyPortal($reference_id, $user_id, $data['payment_option'], $data['total_payment_amount'], $data['transaction_id']);

					$this->session->unset_userdata('tarndata');
					$sdata['message'] = "You are successfully payment completed";
					$this->session->set_userdata($sdata);
					redirect('sms_purchases/index');
				}
				else
				{
					/******************************************************************
					# Just redirect to your success page status already changed by IPN.
					 ******************************************************************/
					echo "<pre>";
					print_r($_POST);exit;
					echo "Just redirect to your success page";
				}
			}
		}
	}

	public function fail_payment($reference_id, $user_id)
	{
		$database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
		if($database_order_status == 'Pending')
		{
			/*****************************************************************************
			# Change your database status to FAILED & You can redirect to failed page from here
			 ******************************************************************************/
//			echo "<pre>";
//			print_r($_POST);
//			die;
//			echo "Transaction Faild";

			$data = array();
			$data['id'] = $reference_id;
			$data['total_payment_amount'] = $_POST['amount'];
			$data['transaction_id'] = $_POST['tran_id'];
			$data['payment_option'] = $_POST['card_issuer'];
			$data['payment_status'] = "F";
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_sms_purchase', $data);

			$sdata['exception'] = $_POST['error'];
			$this->session->set_userdata($sdata);
			redirect('sms_purchases/index');

		}
		else
		{
			/******************************************************************
			# Just redirect to your success page status already changed by IPN.
			 ******************************************************************/
			echo "Just redirect to your failed page";
		}
	}

	public function cancel_payment($reference_id, $user_id)
	{
		$database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
		if($database_order_status == 'Pending')
		{
			/*****************************************************************************
			# Change your database status to CANCELLED & You can redirect to cancelled page from here
			 ******************************************************************************/
//			echo "<pre>";
//			print_r($_POST);
//			die;
//			echo "Transaction Canceled";

			$data = array();
			$data['id'] = $reference_id;
			$data['total_payment_amount'] = $_POST['amount'];
			$data['transaction_id'] = $_POST['tran_id'];
			$data['payment_option'] = "NOT APPLICABLE";
			$data['payment_status'] = "C";
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_sms_purchase', $data);

			$sdata['exception'] = $_POST['error'];
			$this->session->set_userdata($sdata);
			redirect('sms_purchases/index');

		}
		else
		{
			/******************************************************************
			# Just redirect to your cancelled page status already changed by IPN.
			 ******************************************************************/
			echo "Just redirect to your failed page";
		}
	}

	//end SSL payment

	function balanceTransaction($transaction_id, $quantity){
		$data = array();
		$data['transaction_id'] = $transaction_id;
		$data['quantity'] = $quantity;
		$data['added_on'] = date('Y-m-d H:i:s');
		$this->db->insert('tbl_sms_balance', $data);

		$t_data = array();
		$t_data['type'] = 'I';
		$t_data['quantity'] = $quantity;
		$t_data['date'] = date('Y-m-d');
		$t_data['csms_id'] = $transaction_id;
		$this->db->insert('tbl_sms_balance_transaction', $t_data);
		return true;
	}


	function sendDataToFamilyPortal($reference_id,$user_id,$payment_option,$amount,$transaction_id){
		$tran_info = $this->db->query("SELECT * FROM tbl_sms_purchase WHERE id = $reference_id;")->row();
		$school_code = SCHOOL_NAME;
		//echo $school_code; die;
		$family_db = $this->load->database('family_db', true);
		$school_info = $family_db->query("SELECT * FROM `tbl_school` WHERE `code` = '$school_code'")->row();

		//sms purchase data sent to family portal
		$pdata = array();
		$pdata['partner_id'] = $school_info->partner_id;
		$pdata['region_id'] = $school_info->region_id;
		$pdata['zone_id'] = $school_info->zone_id;
		$pdata['date'] = date('Y-m-d');
		$pdata['sms_type'] = $school_info->sms_type;
		$pdata['total_sms'] = $tran_info->quantity;
		$pdata['sms_rate'] = $tran_info->unit_price;
		$pdata['total_amount'] = $tran_info->total_price;
		$pdata['remarks'] = "Purchase From School (School:" . SCHOOL_NAME . ", Payment method:" . $payment_option . ", Amount:" . $amount . ")";
		$pdata['added_by'] = $user_id;
		$pdata['added_on'] = date('Y-m-d H:i:s');
		$pdata['is_approved'] = '1';
		$family_db->insert('tbl_sms_purchases', $pdata);
		//sms purchase data sent to family portal

		//sms transfer data sent to family portal
		$data = array();
		if($school_info->sms_type == 'NM'){
			$data['is_approved'] = '1';
		}else{
			$data['is_approved'] = '0';
		}
		$data['partner_id'] = $school_info->partner_id;
		$data['transaction_id'] = $transaction_id;
		$data['region_id'] = $school_info->region_id;
		$data['zone_id'] = $school_info->zone_id;
		$data['school_id'] = $school_info->id;
		$data['quantity'] = $tran_info->quantity;
		$data['sms_type'] = $school_info->sms_type;
		$data['remarks'] = "Purchase From School (School:" . SCHOOL_NAME . ", Payment method:" . $payment_option . ", Amount:" . $amount . ")";
		$data['added_by'] = $user_id;
		$data['added_on'] = date('Y-m-d H:i:s');
		$family_db->insert('tbl_sms_transfer', $data);
		//end sms balance transfer data to family portal
		$family_db->close();
		$this->load->database('default', true);
		if($school_info->sms_type == 'NM'){
			$this->balanceTransaction($transaction_id, $tran_info->quantity);
		}
		return true;
	}


	//make payment by surjopay
	public function makePaymentBySP($id, $payable_amount){
		$return_url = base_url() . 'sms_purchases/sp_payment_return/' . $id;
		$this->shurjopay->sendPayment($payable_amount,$return_url);
	}

	public function sp_payment_return($reference_id)
	{
		$response_encrypted = $this->input->post('spdata', true);
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => "https://shurjopay.com/merchant/decrypt.php?data=" . $response_encrypted,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "",
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			$sdata['exception'] = "cURL Error #:" . $err;
			$this->session->set_userdata($sdata);
			redirect('sms_purchases/add');
		} else {
			$sp_data = simplexml_load_string($response) or die("Error: Cannot create object");
			switch($sp_data->spCode) {
				case '000':
					$res = array('status'=>true,'msg'=>'Action Successful');
					break;
				case '001':
					$res = array('status'=>false,'msg'=>'Action Failed');
					break;
			}
			if(!isset($res)){
				$res['status'] = false;
			}
			$msg = "Not actual message found from API";
			if(isset($res) && isset($res['msg'])){
				$msg = $res['msg'];
			}

			if($res['status']) {
				$data = array();
				$data['id'] = $reference_id;
				$data['total_payment_amount'] = $sp_data->txnAmount;
				$data['transaction_id'] = $sp_data->txID;
				$data['payment_option'] = $sp_data->paymentOption;
				$data['payment_status'] = 1;
				$this->db->where('id', $data['id']);
				$this->db->update('tbl_sms_purchase', $data);

				$this->sendDataToFamilyPortal($reference_id, $data['payment_option'], $data['amount'], $data['transaction_id']);

				$sdata['message'] = "You are successfully SMS purchase completed";
				$this->session->set_userdata($sdata);
				redirect('sms_purchases/add');
			} else {
				$this->db->delete('tbl_sms_purchase', array('id' => $reference_id));
				$sdata['exception'] = "Sorry ! Your payment could not be made. (" . $msg . ")";
				$this->session->set_userdata($sdata);
				redirect('sms_purchases/add');
			}
		}
	}
	//end make payment by surjopay


}
