<?php

class Appointments extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate','Admin_login','Message'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('appointment');
        $cond = array();
        if ($_POST) {
            $from_date = date("Y-m-d", strtotime($this->input->post('from_date')));
            $to_date = date("Y-m-d", strtotime($this->input->post('to_date')));

            $sdata['from_date'] = $from_date;
            $sdata['to_date'] = $to_date;

            $this->session->set_userdata($sdata);
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        } else {
            $from_date = $this->session->userdata('from_date');
            $to_date = $this->session->userdata('to_date');
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('appointments/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_all_appointments_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['appointments'] = $this->Cases->get_all_appointments_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('appointments/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
          // print_r($_POST);
          // die;
            $data = array();
            $is_sms=$this->input->post('is_sms');
            $mobile_no=$this->input->post('mobile');
            $notes=$this->input->post('notes');
            $client_id=$this->input->post('client_id');
            $is_any_number=$this->input->post('is_any_number');
            $data['is_sms']=$is_sms;
            if($is_any_number==0)
            {
              $data['client_id']=$client_id;
            }

            $data['case_master_id']=$this->input->post('case_master_id');
            $data['date']=date("Y-m-d", strtotime($this->input->post('date')));
            $data['time']=$this->input->post('time');
            $data['notes']=$notes;
            $data['is_any_number']=$is_any_number;
            $data['mobile']=$mobile_no;
            if($is_sms==1)
            {
              if (!$this->Admin_login->validate_mobile_number($mobile_no)) {
                $sdata['exception'] = "Your mobile number is invalid. please write the correct mobile number.";
                $this->session->set_userdata($sdata);
                redirect("appointments/add");
              }
            }
            // print_r($data); die();
            if ($this->Cases->add_appointments($data)) {
                $appointment_id = $this->db->insert_id();
            if($is_sms==1)
            {
              $template_id = 0;
              $template_body  =$notes;
              $send_from = "APS";
              $numbers = array();
              $numbers_with_student_id = array();
              //sms send data ready
              $number =  trim($mobile_no);
              $numbers[] = $number;
              $numbers_with_student_id[$number]['client_id'] = $client_id;
              $numbers_with_student_id[$number]['number'] = $number;
              $numbers_with_student_id[$number]['appointment_id'] = $appointment_id;
              //sms send data ready
              $status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
              if (strtolower($status['status']) == 'success') {
                $sdata['message'] = "SMS send successfully";
                $this->session->set_userdata($sdata);
                redirect("appointments/index");
              } else {
                $sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
                $this->session->set_userdata($sdata);
                redirect("appointments/add");
              }
            }

            $sdata['message'] =$this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect("appointments/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("appointments/add");
            }
        }
        $data = array();
        $lang=$this->session->userdata('site_lang');
        $data['heading_msg'] = $data['title'] = $this->lang->line('appointments');
        $data['setting']=$this->Advocate->read_advocate_setting();
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['clients'] = $this->Cases->get_client_list();
        $data['mobile'] = $this->Cases->get_mobile_by_client_id(0);
        $data['case_list'] = $this->Advocate->get_all_case_list_with_thana_for_dropdown_by_language($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('appointments/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          //
          // print_r($_POST);
          // die;
          $data = array();
          $data['id']=$id;
          // $data['mobile']=$mobile;
          $is_sms=$this->input->post('is_sms');
          $mobile_no=$this->input->post('mobile');
          $notes=$this->input->post('notes');
          $client_id=$this->input->post('client_id');
          $data['is_sms']=$is_sms;
          $data['client_id']=$client_id;
          $data['case_master_id']=$this->input->post('case_master_id');
          $data['date']=date("Y-m-d", strtotime($this->input->post('date')));
          $data['time']=$this->input->post('time');
          $data['notes']=$notes;
          if ($this->Cases->edit_appointments($data, $id)) {
              $sdata['message'] = $this->lang->line('edit_success_message');
              $this->session->set_userdata($sdata);
              redirect("appointments/index");
          } else {
              $sdata['exception'] = $this->lang->line('edit_error_message');
              $this->session->set_userdata($sdata);
              redirect("appointments/edit/".$id);
          }
        }
        $data = array();
        $lang=$this->session->userdata('site_lang');
        $data['heading_msg'] = $data['title'] = $this->lang->line('appointments');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_data'] = $this->Cases->read_appointments($id);
        // print_r($data['row_data']);
        // die;
        $data['case_list'] = $this->Advocate->get_all_case_list_with_thana_for_dropdown_by_language($lang);
        $data['clients'] = $this->Cases->get_client_list();
        $data['mobile'] = $this->Cases->get_mobile_by_client_id($data['row_data']->client_id);
        $data['appointments'] = $this->db->query("SELECT a.*,c.`first_name`,c.`last_name`,c.`mobile` FROM ad_appointment as a
            INNER JOIN `ad_clients` AS c ON c.`id` = a.`client_id`
             WHERE a.id = $id")->row();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('appointments/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Cases->delete_appointments($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("appointments/index");
    }

    public function ajax_mobile_by_client_id()
    {
        $client_id = $_GET['client_id'];
        $data["dropdown_list"]= $this->Cases->get_mobile_by_client_id($client_id);
        $this->load->view('appointments/dropdown_list', $data);
    }

    public function updateMsgClientsStatus()
    {
      $status = $this->input->get('status', true);
      $id = $this->input->get('id', true);
      $data = array();
      $data['id'] = $id;
      if ($status == 'O') {
          $data['status'] = 'O';
      } if ($status == 'CBC') {
          $data['status'] = 'CBC';
      } if ($status == 'CBA') {
          $data['status'] = 'CBA';
      }
      // print_r($data); die();
      $this->db->where('id', $data['id']);
      $this->db->update('ad_appointment', $data);
    }
    public function ajax_get_client_info_by_case_id()
    {
       $lang=$this->session->userdata('site_lang');
       $case_master_id = $_GET['case_master_id'];
       $data =array();
       $data['language']=$lang;
       $data['case_master_id']=$case_master_id;
       $data['contact_info'] = $this->Advocate->get_client_info_by_case_master_id($case_master_id);
       $data['setting']=$this->Advocate->read_advocate_setting();
       $this->load->view('appointments/add_list', $data);

  }
}
