<?php

class Case_closed extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $lang=$this->session->userdata('site_lang');
        $data['title'] = $this->lang->line('closed').' '.$this->lang->line('case').' '.$this->lang->line('list');
        $cond = array();
        $cond['case_status'] = 'closed';
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('case_closed/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_case_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['cases'] = $this->Advocate->get_all_case_list(20, (int)$this->uri->segment(3), $cond);
        $data['language'] = $lang;
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_closed/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id=NULL)
    {
        if ($_POST) {
          // echo "<pre>";
          // print_r($_POST);
          // die;
            $case_master_id=$this->input->post('id');
            $user_info = $this->session->userdata('user_info');
            $last_cast_history = $this->Advocate->get_case_last_history_details_by_mastr_id($case_master_id);
            $business_on_date=date("Y-m-d", strtotime($this->input->post('bussines_on_date')));
            $data['case_master_id']=$case_master_id;
            $data['case_no']=$this->input->post('case_no');
            $data['case_status_id']=$this->input->post('case_status_id');
            $data['bussines_on_date']=$business_on_date;
            $data['parties_name']=$this->input->post('parties_name');
            $data['court_type_id']=$last_cast_history->court_type_id;
            $data['court_id']=$last_cast_history->court_id;
            $data['district_id']=$last_cast_history->district_id;
            $data['upazilla_id']=$last_cast_history->upazilla_id;
            $data['is_sms']='0';
            $data['created_by']=$user_info[0]->client_user_id;
            $data['created_on']=date('Y-m-d H:i:s');

            if ($this->Advocate->add_case_history($data)) {
              if($this->input->post('last_id')!=0)
              {
                $last_data = array();
                $last_data['hearing_date']=$business_on_date;
                $this->Advocate->edit_case_history($last_data,$this->input->post('last_id'));
              }
              $case_details=array();
              $case_details['case_status_id']=$this->input->post('case_status_id');
              if($this->Advocate->edit_case_master($case_details,$case_master_id)){
                  $sdata['message'] = $this->lang->line('edit_success_message');
                  $this->session->set_userdata($sdata);
                  redirect("case_closed/index/");
              } else {
                  $sdata['exception'] = $this->lang->line('edit_error_message');
                  $this->session->set_userdata($sdata);
                  redirect("case_closed/edit/".$case_master_id);
              }

            }else {
                $sdata['exception'] = $this->lang->line('edit_error_message');
                $this->session->set_userdata($sdata);
                redirect("case_closed/edit/".$case_master_id);
            }


        }

        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('case').' '.$this->lang->line('closed');
        $data['action'] = 'edit/';
        $data['language']=$lang;
        $data['last_cast_history'] = $this->Advocate->get_case_last_history_details_by_mastr_id($id);
        $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
        $data['row_case_master'] = $this->Advocate->read_case_master($id);
        $data['case_type'] = $this->Advocate->get_case_type_dropdown_list_by_language($lang);
        $data['case_sub_type'] =$this->Advocate->get_case_sub_type_dropdown_list_by_language($data['row_case_master']->case_type_id,$lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_closed/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
