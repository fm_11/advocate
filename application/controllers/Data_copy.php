<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Data_copy extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login','Cases','Advocate'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$user_info = $this->session->userdata('user_info');
		if (!empty($user_info)) {
			$client_user_id = $user_info[0]->client_user[0]->id;
			$this->notification = $this->Advocate->get_notification($client_user_id);
		}
	}


	public function index()
	{

		$lang=$this->session->userdata('site_lang');
    $user_info = $this->session->userdata('user_info');
		// echo "<pre>";
		// print_r($user_info[0]->client_user[0]->division_id);
		// die;
		$data  = array();
		$data['title'] =$this->lang->line('district_wise_court_allowcations');
		$data['heading_msg'] = $this->lang->line('district_wise_court_allowcations');
	//	$data['divisions'] = $this->Cases->get_division_list_by_language($lang);
		$data['divisions'] = $this->Cases->get_division_list_by_language_division_id($lang,$user_info[0]->client_user[0]->division_id);

		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('data_copy/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	public function district($id=NULL){
		$lang=$this->session->userdata('site_lang');
		  $user_info = $this->session->userdata('user_info');
		if($_POST){

   $name_district = $_POST['district'];
    $save_data = array();
		$loop=0;
     foreach ($name_district as $key => $value) {
				for ($i=0; $i < $value['total_court']; $i++) {
					if(isset($value['selected_court_'.$i]))
					{
						$save_data[$loop]['division_id']=$value['division_id'];
						$save_data[$loop]['district_id']=$value['district_id'];
						$save_data[$loop]['court_id']=$value['selected_court_'.$i];
						$save_data[$loop]['court_type_id']=$value['selected_court_type_'.$i];
						$loop++;
					}
				}

     }
			// echo '<pre>';
			// print_r($save_data[0]['division_id']);
			// die;
     $this->db->delete('district_wise_court_allowcations', array('division_id' => $save_data[0]['division_id']));
		 $this->db->insert_batch('district_wise_court_allowcations', $save_data);

			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('data_copy/index');
		}
		$data = array();
	//	$data['district_list'] = $this->Cases->get_all_districts_list_by_division_and_language($id,$lang);
		$data['district_list'] = $this->Cases->get_all_districts_list_by_district_division_and_language($id,$user_info[0]->client_user[0]->district_id,$lang);

		$data['title'] = $this->lang->line('district_wise_court_allowcations');
		$data['heading_msg'] = $this->lang->line('district_wise_court_allowcations');
	  $data['division_name'] = $this->Cases->get_division_name_by_language($id,$lang);
		$data['is_show_button'] = "index";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('data_copy/district', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function class_copy(){
		if($_POST){
			echo '<pre>';
			print_r($_POST);
			die;
			$classes = $this->class_array();
			foreach ($classes as $key=>$value){
				if(isset($_POST['selected_item_' . $key])){
					$name = $_POST['name_english_' . $key];
					$check = $this->db->query("SELECT * FROM tbl_class WHERE name = '$name'")->row();
					if(!empty($check)){
						$sdata['exception'] = "This class is already exists";
						$this->session->set_userdata($sdata);
						redirect('classes/index');
					}
					$data = array();
					$data['name'] = $name;
					$data['name_bangla'] = $_POST['name_bangla_' . $key];
					$data['student_code_short_form'] = $_POST['short_form_' . $key];
					$this->db->insert("tbl_class", $data);
				}
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('data_copy/index');
		}
		$data = array();
		$data['classes'] = $this->class_array();
		$data['title'] = "Class Copy From Default Data";
		$data['heading_msg'] = "Class Copy From Default Data";
		$data['is_show_button'] = "index";
		$data['class_list'] = $this->Admin_login->getClassListByNameIndex();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('data_copy/class_copy', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	function class_array(){
		$classes = array();
		$classes[0]['name_english'] = "PLAY";
		$classes[0]['name_bangla'] = "প্লে";
		$classes[0]['short_form'] = "P";

		$classes[1]['name_english'] = "NURSERY";
		$classes[1]['name_bangla'] = "নার্সারি";
		$classes[1]['short_form'] = "N";

		$classes[2]['name_english'] = "ONE";
		$classes[2]['name_bangla'] = "প্রথম";
		$classes[2]['short_form'] = "1";

		$classes[3]['name_english'] = "TWO";
		$classes[3]['name_bangla'] = "দ্বিতীয়";
		$classes[3]['short_form'] = "2";

		$classes[4]['name_english'] = "THREE";
		$classes[4]['name_bangla'] = "তৃতীয়";
		$classes[4]['short_form'] = "3";

		$classes[5]['name_english'] = "FOUR";
		$classes[5]['name_bangla'] = "চতুর্থ";
		$classes[5]['short_form'] = "4";

		$classes[6]['name_english'] = "FIVE";
		$classes[6]['name_bangla'] = "পঞ্চম";
		$classes[6]['short_form'] = "5";

		$classes[7]['name_english'] = "SIX";
		$classes[7]['name_bangla'] = "ষষ্ঠ";
		$classes[7]['short_form'] = "6";

		$classes[8]['name_english'] = "SEVEN";
		$classes[8]['name_bangla'] = "সপ্তম";
		$classes[8]['short_form'] = "7";

		$classes[9]['name_english'] = "EIGHT";
		$classes[9]['name_bangla'] = "অষ্টম";
		$classes[9]['short_form'] = "8";

		$classes[10]['name_english'] = "NINE";
		$classes[10]['name_bangla'] = "নবম";
		$classes[10]['short_form'] = "9";

		$classes[11]['name_english'] = "TEN";
		$classes[11]['name_bangla'] = "দশম";
		$classes[11]['short_form'] = "10";

		$classes[12]['name_english'] = "Eleven";
		$classes[12]['name_bangla'] = "একাদশ";
		$classes[12]['short_form'] = "11";

		$classes[13]['name_english'] = "Twelve";
		$classes[13]['name_bangla'] = "দ্বাদশ";
		$classes[13]['short_form'] = "12";

		$classes[14]['name_english'] = "Nurani 1st";
		$classes[14]['name_bangla'] = "নূরানী প্রথম";
		$classes[14]['short_form'] = "N1";

		$classes[15]['name_english'] = "Nurani 2nd";
		$classes[15]['name_bangla'] = "নূরানী দ্বিতীয়";
		$classes[15]['short_form'] = "N2";

		$classes[16]['name_english'] = "Poheli Jamat";
		$classes[16]['name_bangla'] = "পহেলি জামাত";
		$classes[16]['short_form'] = "PJ";

		$classes[17]['name_english'] = "Bishes Jamat";
		$classes[17]['name_bangla'] = "বিশেষ জামাত";
		$classes[17]['short_form'] = "BJ";

		$classes[18]['name_english'] = "Pharshi Poheli";
		$classes[18]['name_bangla'] = "ফারসি পহেলি";
		$classes[18]['short_form'] = "PP";

		$classes[19]['name_english'] = "Nahumir";
		$classes[19]['name_bangla'] = "নাহুমীর";
		$classes[19]['short_form'] = "N";

		$classes[20]['name_english'] = "Hedayatun Nahu";
		$classes[20]['name_bangla'] = "হেদায়াতুন নাহু";
		$classes[20]['short_form'] = "HN";

		$classes[21]['name_english'] = "Tadakhul";
		$classes[21]['name_bangla'] = "তাদাখুল";
		$classes[21]['short_form'] = "T";

		$classes[22]['name_english'] = "Shorhe Bekaya";
		$classes[22]['name_bangla'] = "শরহে বেকায়া";
		$classes[22]['short_form'] = "SB";

		$classes[23]['name_english'] = "Jala Lain";
		$classes[23]['name_bangla'] = "জালালাইন";
		$classes[23]['short_form'] = "JL";

		$classes[24]['name_english'] = "Fozilot";
		$classes[24]['name_bangla'] = "ফজিলত";
		$classes[24]['short_form'] = "F";

		$classes[25]['name_english'] = "Takmil";
		$classes[25]['name_bangla'] = "তাকমীল";
		$classes[25]['short_form'] = "T";

		$classes[26]['name_english'] = "Hifj";
		$classes[26]['name_bangla'] = "হিফায";
		$classes[26]['short_form'] = "H";

		$classes[27]['name_english'] = "Nazara";
		$classes[27]['name_bangla'] = "নাজারা";
		$classes[27]['short_form'] = "N";

		$classes[28]['name_english'] = "Kerat";
		$classes[28]['name_bangla'] = "কেরাত";
		$classes[28]['short_form'] = "K";

		$classes[29]['name_english'] = "Ifta";
		$classes[29]['name_bangla'] = "ইফতা";
		$classes[29]['short_form'] = "I";

		return $classes;
	}



}
