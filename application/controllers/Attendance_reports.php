<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Attendance_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login','Advocate'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index($flag=null)
    {
      $lang=$this->session->userdata('site_lang');
      $user_info = $this->session->userdata('user_info');
      $data = array();
      $data['title'] = $this->lang->line('attendance_report');
      $data['heading_msg'] = $this->lang->line('attendance_report');
  if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
          $this->load->library('numbertowords');
          $SchoolInfo = $this->Admin_login->fetReportHeader();
          $data['HeaderInfo'] = $SchoolInfo;
          $data['HeaderInfo'] = $SchoolInfo;
          $data['from_date'] = $this->input->post("from_date");
          $data['to_date'] = $this->input->post("to_date");
          $data['case_no'] = $this->input->post("case_no");
          $data['district_id'] = $this->input->post("district_id");
          $data['court_type_id'] = $this->input->post("court_type_id");
          $data['court_id'] = $this->input->post("court_id");
          $data['client_user_id'] = $this->input->post("client_user_id");
          $data['case_master_id'] = $this->input->post("case_master_id");
          $data['page_type'] = 'landscape';
          $data['paper_size'] = 'A4';
          $cond = array();
          $cond['from_date'] = $this->input->post("from_date");
          $cond['to_date'] = $this->input->post("to_date");
          $cond['case_no'] = $this->input->post("case_no");
          $cond['district_id'] = $this->input->post("district_id");
          $cond['court_type_id'] = $this->input->post("court_type_id");
          $cond['court_id'] = $this->input->post("court_id");
          $cond['client_user_id'] = $this->input->post("client_user_id");
          $cond['case_master_id'] = $this->input->post("case_master_id");
          $data['idata'] = $this->Advocate->get_all_attendance_list_by_date_range(0, 0, $cond);
          $data['language'] = $lang;

    //sub view pass
    $data['report_header'] = $this->load->view('report_content/inside_report_header', $data, true);
    $data['report_footer'] = $this->load->view('report_content/inside_report_footer', $data, true);

    $data['report'] = $this->load->view('attendance_reports/report', $data, true);
      }
      if (isset($_POST['pdf_download'])) {
          $data['is_pdf'] = 1;
          //Dom PDF
          $this->load->library('mydompdf');
          $html = $this->load->view('attendance_reports/report', $data, true);
      //    print_r($html);die;
          $this->mydompdf->createPDF($html, 'ATTENDANCE REPORT', true, 'A4', 'landscape');
      //Dom PDF
      } else {
        if($flag==1 || !$_POST)
        {

          $data['from_date'] = date("Y-m-d");
          $data['to_date'] = date("Y-m-d");
        }
          $data['districts'] = $this->Advocate->get_districts_list_by_lang_and_districtid($lang,$user_info[0]->client_user[0]->district_id);
          $data['case_list'] = $this->Advocate->get_all_case_list_for_dropdown_by_language($lang);
          $data['language']=$lang;
          $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id(0);
          $data['courts'] = $this->Advocate->get_court_dropdown_list(0);
          $data['client_user'] = $this->Advocate->get_client_user_dropdown_list();
          $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
          $data['maincontent'] = $this->load->view('attendance_reports/index', $data, true);
          $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
      }

    }
}
