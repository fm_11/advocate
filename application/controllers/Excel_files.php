<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Excel_files extends CI_Controller
{

    public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    function to_excel_file()
    {
        if($_POST){
            //For Excel Read
            require_once APPPATH . 'third_party/PHPExcel.php';
            $this->excel = new PHPExcel();
            //For Excel Read

            $template_id = $this->input->post('template_id', true);
            $template_body = $this->input->post('template_body', true);
            $sms_type = $this->input->post('sms_type', true);

            //For Excel Upload
            $configUpload['upload_path'] = FCPATH . 'uploads/excel/';
            $configUpload['allowed_types'] = 'xls|xlsx|csv';
            $configUpload['max_size'] = '15000';
            $this->load->library('upload', $configUpload);
            $this->upload->do_upload('txtFile');
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension = $upload_data['file_ext'];    // uploded file extension

            if ($extension == '.xlsx') {
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
            } else {
                $objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
            }


            //Set to read only
            $objReader->setReadDataOnly(true);
            //Load excel file
            $objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            //loop from first data untill last data
            $num = "";
            for ($i = 2; $i <= $totalrows; $i++) {
                $number = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
                if($number == ''){
                    break;
                }
                if($template_id == 'N'){
                    $message_body = ltrim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
                    if($sms_type == 'B'){
                        $is_bangla_sms = 1;
                    }else{
                        $is_bangla_sms = 0;
                    }

                    $single_num = "";
                    $single_num = $this->Admin_login->generateNumberlist($single_num,$number);

                    $sms_info = array();
                    $sms_info['message'] = $message_body;
                    $sms_info['numbers'] = $single_num;
                    $sms_info['is_bangla_sms'] = $is_bangla_sms;
                    $this->Message->sms_send($sms_info);
                }else{
                    if($sms_type == 'B'){
                        $is_bangla_sms = 1;
                    }else{
                        $is_bangla_sms = 0;
                    }
                    //sms send data ready
                    $num = $this->Admin_login->generateNumberlist($num,$number);
                    //sms send data ready end
                }
            }
            //For Excel Upload
            // echo $num; die;
            //only number sms send
            if($template_id != 'N'){
                $sms_info = array();
                $sms_info['message'] = $template_body;
                $sms_info['numbers'] = $num;
                $sms_info['is_bangla_sms'] = $is_bangla_sms;
                //echo '<pre>';
                // print_r($sms_info);
                // die;
                $status = $this->Message->sms_send($sms_info);
            }
            //only number sms send

            $filedel = PUBPATH . 'uploads/excel/' . $file_name;
            if (unlink($filedel)) {
                $sdata['message'] = "You are Successfully Send (" . ($i-2) . ") SMS";
                $this->session->set_userdata($sdata);
                redirect("excel_files/to_excel_file");
            }

        }else{
            $data = array();
            $data['title'] = 'Send SMS From Excel File';
            $data['heading_msg'] = "Send SMS From Excel File";
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('excel_files/to_excel_file_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
