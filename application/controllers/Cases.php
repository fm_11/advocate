<?php

class Cases extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $lang=$this->session->userdata('site_lang');
        $data['title'] = $this->lang->line('case').' '.$this->lang->line('list');
        $cond = array();
        $cond['case_status'] = 'open';
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
          //  $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('cases/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_case_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['cases'] = $this->Advocate->get_all_case_list(0, (int)$this->uri->segment(3), $cond);
        //   echo '<pre>';
        // print_r($data['cases']);
        // die();
        $data['language'] = $lang;
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('cases/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $user_info = $this->session->userdata('user_info');
      $lang=$this->session->userdata('site_lang');
        if ($_POST) {
        //   echo '<pre>';
        // print_r($_POST);

          //   echo '<pre>';
          //
          //

          // print_r(date('Y-m-d H:i:s'));
          // die();
          $client_id=$this->input->post('client_id');
          $case_no=$this->input->post('case_no');
          $case_sub_type_id=  $this->input->post('case_sub_type_id');
          if($this->Advocate->checkifexist_case_by_case_no($case_no,$case_sub_type_id))
          {
            $sdata['exception'] = "This case no '".$case_no."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("cases/add");
          }
          $mobile=$this->input->post('mobile');
          $name=$this->input->post('first_name');
          if(!empty($mobile) && !empty($name))
          {
            $clients=    $this->Advocate->get_clients_by_mobile($mobile,$name);
            if(empty($clients))
            {
              $client_data = array();
              $client_data['mobile']=$mobile;
              $client_data['first_name']=$name;
              $client_data['last_name']=$this->input->post('last_name');
              //$client_data['last_name']=$this->input->post('last_name');
              $client_data['gender']='M';
            //  $client_data['email']=$this->input->post('email');
            //  $client_data['alternate_mobile_no']=$this->input->post('alternate_mobile_no');
              $client_data['address']=$this->Advocate->get_clients_address_by_language($lang,$this->input->post('client_upazilla_id'));
              $client_data['country_id']=1;//$this->input->post('country_id');
            //  $client_data['reference_name']=$this->input->post('reference_name');
            //  $client_data['reference_mobile']=$this->input->post('reference_mobile');
              $client_data['district_id']=$this->input->post('client_district_id');
              $client_data['upazilas_id']= $this->input->post('client_upazilla_id');
              $client_data['reference_name']= $this->input->post('reference_name');
              $client_data['reference_mobile']= $this->input->post('reference_mobile');
            //  $client_data['total_person']=$this->input->post('total_person');
              if ($this->Advocate->add_clients($client_data)) {
                  $client_id = $this->db->insert_id();
               }
            }else {
            $client_id=$clients->id;
            }
          }

            $master_data = array();
            $master_data['client_id']=$client_id;
            $master_data['is_petitioner']=$this->input->post('is_petitioner');
            $master_data['gr_cr_district_id']=$this->input->post('gr_cr_district_id');
            $master_data['gr_cr_upazilla_id']=$this->input->post('gr_cr_upazilla_id');
            $master_data['created_by']=$user_info[0]->client_user_id;
            $master_data['created_on']=date('Y-m-d H:i:s');
            $master_data['file_colour']=$this->input->post('file_colour');
            $master_data['client_user_id']=$this->input->post('advocate_user_id');
           //court
           $master_data['court_type_id']=$this->input->post('court_type_id');
           $master_data['court_id']=$this->input->post('court_id');
           $master_data['judge_type_id']=$this->input->post('judge_type_id');
           $master_data['judge_name']=$this->input->post('judge_name');
           $master_data['remarks']=$this->input->post('remarks');
           $master_data['district_id']=$this->input->post('district_id');
           //end court
           //case details
           $master_data['case_no']=$case_no;
           $master_data['case_type_id']=$this->input->post('case_type_id');
           $master_data['case_sub_type_id']=$case_sub_type_id;
           $master_data['case_status_id']=$this->input->post('case_status_id');
           $master_data['priority']=$this->input->post('priority');
           $master_data['registration_date']=date("Y-m-d", strtotime($this->input->post('registration_date')));
           $master_data['first_hearing_date']=date("Y-m-d", strtotime($this->input->post('registration_date')));
           $master_data['description']=$this->input->post('description');
           $master_data['parties_name']=$this->input->post('parties_name');
           $master_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
           $master_data['reason_for_next_date']=$this->input->post('description');
           $master_data['section_of_the_case']=$this->input->post('section_of_the_case');
           //end case details
          //   echo '<pre>';
          // print_r($master_data);
          // die();
            if ($this->Advocate->add_case_master($master_data)) {

                $master_id = $this->db->insert_id();
                // $file_data = array();
                // $file_loop_time = $this->input->post("file_upload_num_of_row");
                // $j = 0;
                // while ($j < $file_loop_time) {
                //   if (isset($_FILES['file_name_' . $j]) && $_FILES['file_name_' . $j]['name'] != '') {
                //           $image_name = 'file_name_' . $j;
                //           $new_file_name = $this->my_file_upload($image_name, "case_file".$j."_");
                //          if ($new_file_name == '0') {
                //               $sdata['exception'] = "Sorry! this file '".$this->input->post("file_label_name_". $j)."'  doesn't upload." . $this->upload->display_errors();
                //               $this->session->set_userdata($sdata);
                //               $this->db->delete('ad_master_case', array('id' => $master_id));
                //               redirect("cases/add/");
                //           }
                //         	$file_data[$j]['case_master_id']=$master_id;
                //           $file_data[$j]['label_name']=$this->input->post("file_label_name_". $j);
                //           $file_data[$j]['file_path']=$new_file_name;
                //   }
                //
                //     $j++;
                // }
                // if(count($file_data)>0)
                // {
                //   $this->db->insert_batch('ad_case_file_details', $file_data);
                // }
                $client_user_id=  $this->input->post('client_user_id');
                if(!empty($client_user_id))
                {
                  $case_client_user_data = array();
                  $i=0;
                  foreach ($client_user_id as $value) {
                      $case_client_user_data[$i]['case_master_id']=$master_id;
                      $case_client_user_data[$i]['client_user_id']=$value;
                      $i++;
                  }
                  $this->Advocate->add_batch_case_client_user_details($case_client_user_data);
                }

                // $case_data = array();
                // $case_data['case_master_id']=$master_id;
                // $case_data['case_no']=$case_no;
                // $case_data['case_type_id']=$this->input->post('case_type_id');
                // $case_data['case_sub_type_id']=$this->input->post('case_sub_type_id');
                // $case_data['case_status_id']=$this->input->post('case_status_id');
                // $case_data['priority']=$this->input->post('priority');
                // $case_data['act']=$this->input->post('act');
                // $case_data['filing_number']=$this->input->post('filing_number');
                // $case_data['filling_date']=$this->input->post('filling_date');
                // $case_data['registration_number']=$this->input->post('registration_number');
                // $case_data['registration_date']=$this->input->post('registration_date');
                // $case_data['first_hearing_date']=$this->input->post('registration_date');
                // $case_data['cnr_number']=$this->input->post('cnr_number');
                // $case_data['description']=$this->input->post('description');
                // $case_data['parties_name']=$this->input->post('parties_name');
                // $case_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
                // $case_data['reason_for_next_date']=$this->input->post('description');
                // $this->Advocate->add_case_details($case_data);

                // $court_data = array();
                // $court_data['case_master_id']=$master_id;
                // $court_data['court_no']=$this->input->post('court_no');
                // $court_data['court_type_id']=$this->input->post('court_type_id');
                // $court_data['court_id']=$this->input->post('court_id');
                // $court_data['judge_type_id']=$this->input->post('judge_type_id');
                // $court_data['judge_name']=$this->input->post('judge_name');
                // $court_data['remarks']=$this->input->post('remarks');
                // $court_data['district_id']=$this->input->post('district_id');
                // $this->Advocate->add_court_details($court_data);

                $history_data = array();
                $history_data['case_master_id']=$master_id;
                $history_data['case_no']=$case_no;
                $history_data['bussines_on_date']=date("Y-m-d", strtotime($this->input->post('registration_date')));
                $history_data['case_status_id']=$this->input->post('case_status_id');
                $history_data['parties_name']=$this->input->post('parties_name');
                $history_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
                $history_data['reason_for_next_date']=$this->input->post('description');
                $history_data['remarks']=$this->input->post('remarks');
                $history_data['district_id']=$this->input->post('gr_cr_district_id');
                $history_data['upazilla_id']=$this->input->post('gr_cr_upazilla_id');
                $history_data['court_type_id']=$this->input->post('court_type_id');
                $history_data['court_id']=$this->input->post('court_id');
                $history_data['created_by']=$user_info[0]->client_user_id;
                $history_data['created_on']=date('Y-m-d H:i:s');
                $this->Advocate->add_case_history($history_data);

                $loop_time = $this->input->post("num_of_row");
                $i = 0;
                while ($i < $loop_time) {
                    $petitioner_data = array();
                    $petitioner_data['case_master_id']=$master_id;
                    $petitioner_data['name']=$this->input->post("petitioner_name_" . $i);
                    $petitioner_data['advocate_name']=$this->input->post("advocate_" . $i);
                    $this->Advocate->add_petitioner_details($petitioner_data);
                    $i++;
                }

                $sdata['message'] =  $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("cases/add");
            }else{
              $sdata['exception'] =  $this->lang->line('add_error_message');
              $this->session->set_userdata($sdata);
              redirect("cases/add");
            }
        }
        $data = array();
        $district_id=$user_info[0]->client_user[0]->district_id;

        $data['heading_msg'] = $data['title'] = $this->lang->line('case');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['clients'] = $this->Advocate->get_client_dropdown_list();
        $data['case_type'] = $this->Advocate->get_case_type_dropdown_list_by_language($lang);
        $data['case_sub_type'] = $this->Advocate->get_case_sub_type_dropdown_list_by_language(0,$lang);
        $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
        $data['courts_type'] =$this->Advocate->get_court_type_list_by_district_id_and_language($district_id,$lang);
        $data['courts'] = $this->Advocate->get_court_dropdown_list_by_language(0,$lang);
        $data['judges'] = $this->Advocate->get_judge_dropdown_list_by_language($lang);
        $data['file_colour'] = $this->Advocate->get_file_colour_dropdown_list_by_language($lang);
        $data['client_user'] = $this->Advocate->get_client_user_dropdown_list();
        $data['fir_case_type'] = $this->Advocate->get_case_type_list_by_lang($lang);
        $data['language']=$lang;
        $district_list=$this->Advocate->get_districts_list_by_lang_and_districtid($lang,$district_id);
        $data['fir_district_id'] = $district_list;
        $data['fir_upazilla_id'] = $this->Advocate->get_upazilas_list_by_district_and_lang($lang,$district_id);
        $data['district_id'] = $district_list;
        $data['user_district_id'] = $district_id;
        $data['upazilla_id'] = $user_info[0]->client_user[0]->upazilas_id;
        $data['client_user_id'] = $user_info[0]->client_user[0]->id;
        $data['all_districts'] =  $this->Advocate->get_all_districts_list_by_lang($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('cases/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        $user_info = $this->session->userdata('user_info');
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
          // echo "<pre>";
          // print_r($_POST);
          // die;
          $edit_client_name=$this->input->post('edit_client_name');
          $edit_client_mobile=$this->input->post('edit_client_mobile');
          $edit_client_id=$this->input->post('edit_client_id');



          if($this->Advocate->checkifexist_update_clients_by_mobile($edit_client_mobile,$edit_client_name,$edit_client_id))
          {
            $sdata['exception'] = "This name '".$name."' and mobile number'".$mobile."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("cases/edit/".$id);
          }

          $case_no=$this->input->post('case_no');
          $case_sub_type_id=  $this->input->post('case_sub_type_id');
          if($this->Advocate->checkifexist_update_case_by_case_no($case_no,$case_sub_type_id,$id))
          {
            $sdata['exception'] = "This case no '".$case_no."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("cases/edit/".$id);
          }
          $master_data = array();

          $master_data['client_id']=$this->input->post('client_id');
          $master_data['is_petitioner']=$this->input->post('is_petitioner');
          $master_data['gr_cr_district_id']=$this->input->post('gr_cr_district_id');
          $master_data['gr_cr_upazilla_id']=$this->input->post('gr_cr_upazilla_id');
          $master_data['updated_by']=$user_info[0]->client_user_id;
          $master_data['updated_on']=date('Y-m-d H:i:s');
          $master_data['file_colour']=$this->input->post('file_colour');
          $master_data['client_user_id']=$this->input->post('advocate_user_id');
          $master_data['id']=$id;
          //court
          $master_data['court_type_id']=$this->input->post('court_type_id');
          $master_data['court_id']=$this->input->post('court_id');
          $master_data['judge_type_id']=$this->input->post('judge_type_id');
          $master_data['judge_name']=$this->input->post('judge_name');
          $master_data['remarks']=$this->input->post('remarks');
          $master_data['district_id']=$this->input->post('district_id');
          //end court
          //case details
          $master_data['case_no']=$case_no;
          $master_data['case_type_id']=$this->input->post('case_type_id');
          $master_data['case_sub_type_id']=$case_sub_type_id;
          $master_data['case_status_id']=$this->input->post('case_status_id');
          $master_data['priority']=$this->input->post('priority');
          $master_data['registration_date']=date("Y-m-d", strtotime($this->input->post('registration_date')));
          $master_data['first_hearing_date']=date("Y-m-d", strtotime($this->input->post('registration_date')));
          $master_data['description']=$this->input->post('description');
          $master_data['parties_name']=$this->input->post('parties_name');
          $master_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
          $master_data['reason_for_next_date']=$this->input->post('description');
          $master_data['section_of_the_case']=$this->input->post('section_of_the_case');
          //end case details
          // print_r($master_data);
          // die();
          if ($this->Advocate->edit_case_master($master_data,$id)) {

            $this->Advocate->delete_case_client_user_details($id);
            $client_user_id=  $this->input->post('client_user_id');
            if(!empty($client_user_id))
            {
              $case_client_user_data = array();
              $i=0;
              foreach ($client_user_id as $value) {
                  $case_client_user_data[$i]['case_master_id']=$id;
                  $case_client_user_data[$i]['client_user_id']=$value;
                  $i++;
              }
              $this->Advocate->add_batch_case_client_user_details($case_client_user_data);
            }

              $master_id = $id;
              // $case_data = array();
              // $case_data['case_master_id']=$master_id;
              // $case_data['case_no']=$case_no;
              // $case_data['case_type_id']=$this->input->post('case_type_id');
              // $case_data['case_sub_type_id']=$this->input->post('case_sub_type_id');
              // $case_data['case_status_id']=$this->input->post('case_status_id');
              // $case_data['priority']=$this->input->post('priority');
              // $case_data['act']=$this->input->post('act');
              // $case_data['filing_number']=$this->input->post('filing_number');
              // $case_data['filling_date']=$this->input->post('filling_date');
              // $case_data['registration_number']=$this->input->post('registration_number');
              // $case_data['registration_date']=$this->input->post('registration_date');
              // $case_data['first_hearing_date']=$this->input->post('registration_date');
              // $case_data['cnr_number']=$this->input->post('cnr_number');
              // $case_data['description']=$this->input->post('description');
              // $case_data['parties_name']=$this->input->post('parties_name');
              // $case_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
              // $case_data['reason_for_next_date']=$this->input->post('description');
              // $this->Advocate->edit_case_details($case_data,$master_id);
              //
              // $court_data = array();
              // $court_data['case_master_id']=$master_id;
              // $court_data['court_no']=$this->input->post('court_no');
              // $court_data['court_type_id']=$this->input->post('court_type_id');
              // $court_data['court_id']=$this->input->post('court_id');
              // $court_data['judge_type_id']=$this->input->post('judge_type_id');
              // $court_data['judge_name']=$this->input->post('judge_name');
              // $court_data['remarks']=$this->input->post('remarks');
              // $court_data['district_id']=$this->input->post('district_id');
              // $this->Advocate->edit_court_details($court_data,$master_id);

              $history_data = array();
              $history_id=$this->Advocate->get_case_first_history_id($master_id);
              $history_data['id']=$history_id;
              $history_data['case_master_id']=$master_id;
              $history_data['case_no']=$case_no;
              $history_data['bussines_on_date']=date("Y-m-d", strtotime($this->input->post('registration_date')));
              $history_data['case_status_id']=$this->input->post('case_status_id');
              $history_data['parties_name']=$this->input->post('parties_name');
              $history_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
              $history_data['reason_for_next_date']=$this->input->post('description');
              $history_data['remarks']=$this->input->post('remarks');
              $history_data['district_id']=$this->input->post('gr_cr_district_id');
              $history_data['upazilla_id']=$this->input->post('gr_cr_upazilla_id');
              $history_data['court_type_id']=$this->input->post('court_type_id');
              $history_data['court_id']=$this->input->post('court_id');
              $history_data['updated_by']=$user_info[0]->client_user_id;
              $history_data['updated_on']=date('Y-m-d H:i:s');

              $this->Advocate->edit_case_history($history_data,$history_id);

              $this->Advocate->delete_petitioner_details($master_id);
              $loop_time = $this->input->post("num_of_row");
              $i = 0;
              while ($i < $loop_time) {
                  $petitioner_data = array();
                  $petitioner_data['case_master_id']=$master_id;
                  $petitioner_data['name']=$this->input->post("petitioner_name_" . $i);
                  $petitioner_data['advocate_name']=$this->input->post("advocate_" . $i);
                  $this->Advocate->add_petitioner_details($petitioner_data);
                  $i++;
              }

              $client_data = array();
              $client_data['mobile']=$edit_client_mobile;
              $client_data['first_name']=$edit_client_name;
              $client_data['id']=$edit_client_id;
              $this->Advocate->edit_clients($client_data, $edit_client_id);

              $sdata['message'] =$this->lang->line('add_success_message');
              $this->session->set_userdata($sdata);
              redirect("cases/index");
          }else{
            $sdata['exception'] =$this->lang->line('error_success_message');
            $this->session->set_userdata($sdata);
            redirect("cases/edit/".$id);
          }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('case');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_case_master'] = $this->Advocate->read_case_master($id);
        //$data['row_case_details'] = $this->Advocate->read_case_details($id);
      //  $data['row_court_details'] = $this->Advocate->read_court_details($id);
        $data['row_petitioner_details'] = $this->Advocate->read_petitioner_details($id);
        $data['row_case_client_user_details'] = $this->Advocate->read_case_client_user_details($id);
        // print_r($data['row_court_details']->court_type_id); echo die;


        $district_list=$this->Advocate->get_districts_list_by_lang_and_districtid($lang,$user_info[0]->client_user[0]->district_id);
        $data['clients'] = $this->Advocate->get_client_dropdown_list();
        $data['case_type'] = $this->Advocate->get_case_type_dropdown_list_by_language($lang);
        $data['case_sub_type'] =$this->Advocate->get_case_sub_type_dropdown_list_by_language($data['row_case_master']->case_type_id,$lang);// $this->Advocate->get_case_sub_type_dropdown_list_by_language(0,$lang);
        $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
        $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id_and_language($data['row_case_master']->district_id,$lang);//$this->Advocate->get_court_type_dropdown_list_by_language($lang);
        $data['courts'] = $this->Advocate->get_court_list_by_district_id_and_court_type_id_and_language($data['row_case_master']->district_id,$data['row_case_master']->court_type_id,$lang);// $this->Advocate->get_court_dropdown_list_by_language(0,$lang);
        $data['judges'] = $this->Advocate->get_judge_dropdown_list_by_language($lang);
        $data['file_colour'] = $this->Advocate->get_file_colour_dropdown_list_by_language($lang);
        $data['client_user'] = $this->Advocate->get_client_user_dropdown_list();
        $data['fir_case_type'] = $this->Advocate->get_case_type_list_by_lang($lang);
        $data['language']=$lang;
        $district_list=$this->Advocate->get_districts_list_by_lang_and_districtid($lang,$user_info[0]->client_user[0]->district_id);
        $data['fir_district_id'] = $district_list;
        $data['fir_upazilla_id'] = $this->Advocate->get_upazilas_list_by_district_and_lang($lang,$data['row_case_master']->gr_cr_district_id);
        $data['district_id'] = $district_list;
        $history_count=count($this->Advocate->read_case_history($id));
        $data['case_disable']=  $history_count>1?"y":"n";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('cases/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
       $history=$this->Advocate->read_case_history($id);
       if(empty($history))
       {
         $this->Advocate->delete_case_master($id);
         // $this->Advocate->delete_case_details($id);
         // $this->Advocate->delete_court_details($id);
         $this->Advocate->delete_case_history($id);
         $this->Advocate->delete_petitioner_details($id);
         $this->Advocate->delete_case_client_user_details($id);
         $sdata['message'] = $this->lang->line('delete_success_message');
         $this->session->set_userdata($sdata);
           redirect("cases/index");
       }
       $sdata['exception'] =$this->lang->line('delete_error_message').'. '.$this->lang->line('due_to_history_dependency');
       $this->session->set_userdata($sdata);
        redirect("cases/index");
    }
    public function updateMsgStatusMemberStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_client_user', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }
    public function ajax_case_sub_type_by_type_id()
    {
        $lang=$this->session->userdata('site_lang');
        $type_id = $_GET['type_id'];
        $data["dropdown_list"]= $this->Advocate->get_case_sub_type_dropdown_list_by_language($type_id,$lang);
        $this->load->view('cases/dropdown_list', $data);
    }
    public function ajax_courts_type_by_court()
    {
        $lang=$this->session->userdata('site_lang');
        $court_id = $_GET['court_id'];
        $district_id = $_GET['district_id'];
        $data["dropdown_list"]= $this->Advocate->get_court_list_by_district_id_and_court_type_id_and_language($district_id,$court_id,$lang);
        $this->load->view('cases/dropdown_list', $data);
    }
    public function ajax_client_name_by_client_id()
    {
        $client_id = $_GET['client_id'];
        $data["client_name"]= $this->Advocate->get_client_name_by_client_id($client_id);
        print_r($data["client_name"]); echo die();
        $this->load->view('cases/client_info', $data);
    }
    public function ajax_disTrictWiseThanaOrCourt_by_district_id()
    {
       $lang=$this->session->userdata('site_lang');
        $district_id = $_GET['district_id'];
        $type = $_GET['type'];
        if($type=="T" || $type=="CT")
        {
         $data["dropdown_list"]= $this->Advocate->get_upazilas_list_by_district_and_lang($lang,$district_id);
        }elseif ($type=="C") {
          $data["dropdown_list"]= $this->Advocate->get_court_type_list_by_district_id_and_language($district_id,$lang);
        }

        $this->load->view('cases/dropdown_list', $data);
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = $type.time() . "_" . date('Y-m-d') . '.' .end($ext);
      // print_r(rand());
      // die();
      $this->load->library('upload');
      $config = array(
          'upload_path' => "core_media/case_file/",
          'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|doc|docx",
          'max_size' => "99999999999",
          'max_height' => "3000",
          'max_width' => "3000",
          'file_name' => $new_file_name
        );
      $this->upload->initialize($config);
      if (!$this->upload->do_upload($filename)) {
        return '0';
      } else {
        return $new_file_name;
      }
    }
    public function get_client_user_dropdown_list()
    {
        $client_dropdown = $this->Advocate->get_client_user_dropdown_list();

        echo json_encode($client_dropdown);
    }
}
