<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ad_case_status extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('case_status');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('ad_case_status/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_case_status_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['case_status'] = $this->Cases->get_case_status_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('ad_case_status/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
        $data = array();
        $name=$this->input->post('name');
        $data['name']=$name;
        $data['bn_name']=$this->input->post('bn_name');
        if($this->Cases->checkifexist_case_status_by_name($name))
        {
          $sdata['exception'] = "This name '".$name."' already exist.";
          $this->session->set_userdata($sdata);
          redirect("ad_case_status/add");
        }
        if ($this->Cases->add_case_status($data)) {
            $sdata['message'] = "Information Successfully Added";
            $this->session->set_userdata($sdata);
            redirect("ad_case_status/index");
        } else {
            $sdata['exception'] = "Information could not add";
            $this->session->set_userdata($sdata);
            redirect("ad_case_status/add");
        }
        $this->db->insert("ad_case_status", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('ad_case_status/index');
      }
      $data = array();
      $data['heading_msg'] = $data['title'] = $this->lang->line('case_status');
      $data['action'] = 'add';
      $data['is_show_button'] = "index";
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('ad_case_status/add', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
      if ($_POST) {
        $data = array();
        $data['id'] = $_POST['id'];
        $data['name'] = $_POST['name'];
        $data['bn_name'] = $_POST['bn_name'];
        $this->db->where('id', $data['id']);
        $this->db->update("ad_case_status", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('ad_case_status/index');
      }
      $data = array();
      $data['heading_msg'] = $data['title'] = $this->lang->line('case_status');
      $data['action'] = 'edit/' . $id;
      $data['is_show_button'] = "index";
      $data['row'] = $this->Cases->read_case_status($id);
      $data['case_status'] = $this->db->query("SELECT * FROM `ad_case_status` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('ad_case_status/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('ad_case_status', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('ad_case_status/index');
    }

    public function updateCaseStatus()
    {
        $status = $this->input->get('is_active', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_case_status', $data);
        if ($status == 0) {
            echo '<a class="deleteTag" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a class="deleteTag" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }

  }

 ?>
