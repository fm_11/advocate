<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ad_case_types extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       $this->load->model(array('Cases','Advocate'));
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('case_type');
        $data['heading_msg'] = $this->lang->line('case_type');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['case_type'] = $this->db->query("SELECT * FROM ad_case_type")->result_array();
        $data['maincontent'] = $this->load->view('ad_case_types/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
        $data = array();
        $data['name'] = $_POST['name'];
        $data['bn_name'] = $_POST['bn_name'];
        $this->db->insert("ad_case_type", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('ad_case_types/index');
      }
      $data = array();
      $data['title'] = $this->lang->line('add').' '.$this->lang->line('case_type');
      $data['heading_msg'] = $this->lang->line('add').' '.$this->lang->line('case_type');
      $data['is_show_button'] = "index";
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('ad_case_types/add', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
      if ($_POST) {
        $data = array();
        $data['id'] = $_POST['id'];
        $data['name'] = $_POST['name'];
        $data['bn_name'] = $_POST['bn_name'];
        $this->db->where('id', $data['id']);
        $this->db->update("ad_case_type", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('ad_case_types/index');
      }
      $data = array();
      $data['title'] = $this->lang->line('update').' '.$this->lang->line('case_type');
      $data['heading_msg'] = $this->lang->line('update').' '.$this->lang->line('case_type');
      $data['is_show_button'] = "index";
      $data['case_type'] = $this->db->query("SELECT * FROM `ad_case_type` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('ad_case_types/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('ad_case_type', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('ad_case_types/index');
    }

  }

 ?>
