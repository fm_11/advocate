
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Chart_of_accounts extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data['title'] = "Accounts Head";
        //$data['is_show_button'] = "add";
        $data['asset_head_list'] = $this->db->query("SELECT * FROM tbl_asset_category")->result_array();
        $data['lib_head_list'] = $this->db->query("SELECT * FROM tbl_liabilities_category")->result_array();
        $data['income_head_list'] = $this->db->query("SELECT * FROM tbl_ba_income_category")->result_array();
        $data['exp_head_list'] = $this->db->query("SELECT * FROM tbl_ba_expense_category")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('chart_of_accounts/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['code'] = $_POST['code'];
            $data['parentID'] = $_POST['category_id'];
            $data['opening_balance'] = $_POST['opening_balance'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->insert("tbl_chart_of_accounts", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('chart_of_accounts/add');
        } else {
            $data['title'] = "Accounts Head";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['main_head_list'] = $this->db->query("SELECT * FROM tbl_chart_of_accounts
               WHERE parentID = 0")->result_array();
            $data['maincontent'] = $this->load->view('chart_of_accounts/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
