<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_guardian_panels extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login', 'Timekeeping', 'Student', 'Student_fee', 'Student_guardian_panel', 'common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library('session');
       date_default_timezone_set('Asia/Dhaka');
        $student_info = $this->session->userdata('session_student_info');
        //echo '<pre>';
        //print_r($student_info); die;
        if (empty($student_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Valid Student !";
            $this->session->set_userdata($sdata);
            redirect("login/student_guardian_login");
        }

    }


    public function index()
    {
        $student_info = $this->session->userdata('session_student_info');
        $student_id = $student_info[0]->id;

        $from_date = date('Y-m').'-01';
        $to_date = date('Y-m').'-31';
        $data = array();
        $data['active_menu'] = 'dashboard';
        $data['title'] = 'Student and Guardian Panel';
        $data['heading_msg'] = "Student and Guardian Panel";
        $data['present'] = count($this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND l.`login_status` = 'P' AND
(l.`date` BETWEEN '$from_date' AND '$to_date')")->result_array());
        $data['absent'] = count($this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND l.`login_status` = 'A' AND
(l.`date` BETWEEN '$from_date' AND '$to_date')")->result_array());
        $data['leave'] = count($this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND l.`login_status` = 'L' AND
(l.`date` BETWEEN '$from_date' AND '$to_date')")->result_array());
        $data['dashboard_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 6")->result_array();
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/dashboard', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }

    function timekeeping_report(){
        $data = array();
        if($_POST){
            $month = $this->input->post('month', true);
            $year = $this->input->post('year', true);
            $student_id = $this->input->post('student_id', true);
            $from_date = $year.'-'.$month.'-01';
            $to_date = $year.'-'.$month.'-31';
            $data['timekeeping_info'] = $this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND
(l.`date` BETWEEN '$from_date' AND '$to_date')  ORDER BY l.`date`")->result_array();
        }
        $student_info = $this->session->userdata('session_student_info');
        $data['student_id'] = $student_info[0]->id;
        $data['active_menu'] = 'timekeeping';
        $data['title'] = 'Student Attendance Report';
        $data['heading_msg'] = "Student Attendance Report";
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/timekeeping_report_form', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }

    function get_student_fees_info(){
        $data = array();
        if($_POST){
            $SchoolInfo = $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $student_info = $this->session->userdata('session_student_info');
            $student_id = $student_info[0]->id;
            $data['student_id'] = $student_id;
            $data['student_info'] = $this->db->query("SELECT s.`name`,s.`roll_no`,c.`name` AS class_name,sf.`name` AS shift_name FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_shift` AS sf ON s.`shift_id` = sf.`id`
WHERE s.`id` = '$student_id'")->result_array();
            $data['year'] = $this->input->post('year', true);
            $data['sub_category'] = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
            $data['rdata'] = $this->Student_fee->getStudentFeeStatusDetailsStandard($data);
            //echo '<pre>';
            //print_r($data['rdata']);
           // die;
        }


        $student_info = $this->session->userdata('session_student_info');
        $data['student_id'] = $student_info[0]->id;
        $data['active_menu'] = 'student_fees';
        $data['title'] = 'Student Fees Information';
        $data['heading_msg'] = "Student Fees Information";
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/get_student_fees_info', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }

    function get_student_info()
    {
        $student_info = $this->session->userdata('session_student_info');
        $student_id = $student_info[0]->id;
        $data = array();
        $data['active_menu'] = 'student_info';
        $data['title'] = 'Student Profile';
        $data['heading_msg'] = "Student Profile";
        $data['student_info'] = $this->Student->get_student_info_by_student_id($student_id);
        $data['subject_list_by_student'] = $this->db->query("SELECT
  su.*,
  s.`is_optional`
FROM
  `tbl_student_wise_subject` AS s
  LEFT JOIN `tbl_subject` AS su
    ON su.`id` = s.`subject_id`
WHERE s.`student_id` = '$student_id' ")->result_array();
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/student_profile', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }


    function get_student_leave_info()
    {
        $data = array();
        $data['active_menu'] = 'leave_info';
        $data['title'] = 'Student Leave';
        $data['heading_msg'] = "Student Leave";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_guardian_panels/get_student_leave_info/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Timekeeping->get_all_student_leave_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leaves'] = $this->Timekeeping->get_all_student_leave_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/student_leave_list', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }

    function get_student_leave_application()
    {
        if ($_POST) {
            $student_id = $this->input->post('student_id', true);
            $date_from = $this->input->post('txtFromDate', true);
            $date_to = $this->input->post('txtToDate', true);
            $login_info = $this->db->query("SELECT id FROM tbl_logins WHERE person_id = '$student_id' AND person_type = 'S'
AND (date BETWEEN '$date_from' AND '$date_to')")->result_array();
            if (!empty($login_info)) {
                $sdata['exception'] = "Login Information Found for this student !";
                $this->session->set_userdata($sdata);
                redirect("student_guardian_panels/get_student_leave_application");
            }
            $data = array();
            $data['student_id'] = $student_id;
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['group'] = $this->input->post('group', true);
            $data['date_from'] = $date_from;
            $data['date_to'] = $date_to;
            $data['status'] = 0;
            $data['syn_date'] = date('Y-m-d');
            $data['reason'] = $this->input->post('txtReason', true);
            $this->db->insert('tbl_student_leave_applications', $data);
            $sdata['message'] = "You are Successfully Added Leave Application";
            $this->session->set_userdata($sdata);
            redirect("student_guardian_panels/get_student_leave_application");
        }
        $student_info = $this->session->userdata('session_student_info');
        $data = array();
        $data['active_menu'] = 'leave_info';
        $data['title'] = 'Student Leave';
        $data['heading_msg'] = "Student Leave";
        $data['student_id'] = $student_info[0]->id;
        $data['name'] = $student_info[0]->name;
        $data['student_code'] = $student_info[0]->student_code;
        $data['class_id'] = $student_info[0]->class_id;
        $data['section_id'] = $student_info[0]->section_id;
        $data['group'] = $student_info[0]->group;
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/student_leave_add', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }


    function get_all_notice()
    {
        $data = array();
        $data['title'] = 'Notice';
        $data['active_menu'] = 'message';
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_guardian_panels/get_all_notice');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Admin_login->get_all_notice(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['notices'] = $this->Admin_login->get_all_notice(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
        $data['maincontent'] = $this->load->view('student_guardian_panels/notice_list', $data, true);
        $this->load->view('student_guardian_panels/index', $data);
    }


    function change_password()
    {
        if ($_POST) {
            $student_id = $this->input->post("student_id");
            $password = $this->input->post("current_pass");
            $password = md5($password);
            $is_authenticated = $this->custom_methods_model->num_of_data("tbl_student", " WHERE id='$student_id' AND password='$password'");

            if ($is_authenticated > 0) {
                $data = array();
                $data['password'] = md5($this->input->post("new_pass"));
                $data['id'] = $student_id;
                $this->db->where('id', $data['id']);
                $this->db->update('tbl_student', $data);
                $sdata['message'] = "You are Successfully Password Changes";
                $this->session->set_userdata($sdata);
                redirect("student_guardian_panels/change_password");
            } else {
                $sdata['exception'] = "Sorry Current Password Does'nt Match !";
                $this->session->set_userdata($sdata);
                redirect("student_guardian_panels/change_password");
            }
        } else {
            $data = array();
            $data['active_menu'] = '';
            $data['title'] = 'Change Password';
            $data['heading_msg'] = "Change Password";
            $student_info = $this->session->userdata('session_student_info');
            $data['student_id'] = $student_info[0]->id;
            $data['name'] = $student_info[0]->name;
            $data['student_code'] = $student_info[0]->student_code;
            $data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
            $data['maincontent'] = $this->load->view('student_guardian_panels/change_password', $data, true);
            $this->load->view('student_guardian_panels/index', $data);
        }
    }


}


?>
