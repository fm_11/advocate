<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Income_categories extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Account', 'Admin_login'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('income') . ' ' . $this->lang->line('category');
        $data['heading_msg'] = $this->lang->line('income') . ' ' . $this->lang->line('category');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['is_show_button'] = "add";
        $data['income_category'] = $this->db->query("SELECT * FROM tbl_ba_income_category")->result_array();
        $data['maincontent'] = $this->load->view('income_categories/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['code'] = $_POST['code'];
            $data['opening_balance'] = $_POST['opening_balance'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->insert("tbl_ba_income_category", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('income_categories/add');
        }
        $data = array();
        $data['title'] = $this->lang->line('income') . ' ' . $this->lang->line('category'). ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('income') . ' ' . $this->lang->line('category'). ' ' . $this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('income_categories/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $id = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['code'] = $_POST['code'];
            $data['opening_balance'] = $_POST['opening_balance'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->where('id', $id);
            $this->db->update('tbl_ba_income_category', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('income_categories/index');
        }
        $data = array();
        $data['title'] = $this->lang->line('income') . ' ' . $this->lang->line('category'). ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('income') . ' ' . $this->lang->line('category'). ' ' . $this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['income_category_info'] = $this->db->query("SELECT * FROM tbl_ba_income_category WHERE `id` = '$id'")->result_array();
        $data['maincontent'] = $this->load->view('income_categories/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_ba_income_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("income_categories/index");
    }
}
