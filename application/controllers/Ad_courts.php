<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ad_courts extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       $this->load->model(array('Cases','Advocate'));
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('court').' '.$this->lang->line('information');
        $data['heading_msg'] = $this->lang->line('court').' '.$this->lang->line('information');
        $data['is_show_button'] = "add";
        $data['language']=$this->session->userdata('site_lang');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['court'] = $this->db->query("SELECT c.name as court_name,c.bn_name as bn_name,t.name as court_type,t.bn_name as bn_court_type,c.id,c.is_active FROM ad_court as c left join ad_court_type as t on c.court_type_id=t.id")->result_array();
        $data['maincontent'] = $this->load->view('ad_courts/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
        $data = array();
        $data['name'] = $_POST['name'];
        $data['bn_name'] = $_POST['bn_name'];
        $data['court_type_id'] = $_POST['court_type_id'];
        $this->db->insert("ad_court", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('ad_courts/index');
      }
      $data = array();
      $data['title'] = $this->lang->line('add').' '.$this->lang->line('court').' '.$this->lang->line('information');
      $data['heading_msg'] = $this->lang->line('add').' '.$this->lang->line('court').' '.$this->lang->line('information');
      $data['is_show_button'] = "index";
      $data['court_type'] = $this->db->query("SELECT * FROM ad_court_type")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('ad_courts/add', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
      if ($_POST) {
        $data = array();
        $data['id'] = $_POST['id'];
        $data['name'] = $_POST['name'];
        $data['bn_name'] = $_POST['bn_name'];
        $data['court_type_id'] = $_POST['court_type_id'];
        $this->db->where('id', $data['id']);
        $this->db->update("ad_court", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('ad_courts/index');
      }
      $data = array();
      $data['title'] = $this->lang->line('update').' '.$this->lang->line('court').' '.$this->lang->line('information');
      $data['heading_msg'] = $this->lang->line('update').' '.$this->lang->line('court').' '.$this->lang->line('information');
      $data['is_show_button'] = "index";
      $data['court_type'] = $this->db->query("SELECT * FROM ad_court_type")->result_array();
      $data['court'] = $this->db->query("SELECT * FROM `ad_court` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('ad_courts/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('ad_court', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('ad_courts/index');
    }

    public function updateCourtStatus()
    {
        $status = $this->input->get('is_active', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_court', $data);
        if ($status == 0) {
            echo '<a class="deleteTag" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a class="deleteTag" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }

  }

 ?>
