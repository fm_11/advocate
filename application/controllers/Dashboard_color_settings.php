<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Dashboard_color_settings extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login','Advocate'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$user_info = $this->session->userdata('user_info');
		if (!empty($user_info)) {
			$client_user_id = $user_info[0]->client_user[0]->id;
			$this->notification = $this->Advocate->get_notification($client_user_id);
		}
	}


	public function index()
	{
		$data = array();
		$data['title'] = "Dashboard Color Settings";
		$data['heading_msg'] = "Dashboard Color Settings";
		$config_info = $this->db->query("SELECT * FROM `tbl_dashboard_color_settings`")->row();
		if (empty($config_info)) {
			$sdata['exception'] = "Initial configuration not found to database";
			$this->session->set_userdata($sdata);
			redirect('dashboard/index');
		}
		$data['config_info'] = $config_info;
		if ($_POST) {
			$data = array();
			$data['id'] = $_POST['id'];
			if(isset($_POST['submit'])){
				$data['left_menu_background_color'] = '#' . $_POST['left_menu_background_color'];
				$data['top_bar_background_color'] = '#' . $_POST['top_bar_background_color'];
				$data['top_menu_show_hide_button_color'] = '#' . $_POST['top_menu_show_hide_button_color'];

				$data['school_name_border_color'] = "#" . $_POST['school_name_border_color'];
				$data['head_name_border_color'] = "#" . $_POST['head_name_border_color'];
				$data['today_attendance_border_color'] = "#" . $_POST['today_attendance_border_color'];

				$data['support_center_border_color'] = "#" . $_POST['support_center_border_color'];
				$data['total_running_case_border_color'] = "#" . $_POST['total_running_case_border_color'];
				$data['total_colsed_case_border_color'] = "#" . $_POST['total_colsed_case_border_color'];

				$data['total_missaed_attendance_border_color'] = "#" . $_POST['total_missaed_attendance_border_color'];
				$data['sms_balance_border_color'] = "#" . $_POST['sms_balance_border_color'];
				$data['birthday_border_color'] = "#" . $_POST['birthday_border_color'];
				$data['total_client_border_color'] = "#" . $_POST['total_client_border_color'];
	      $data['total_importan_case_border_color'] = "#" . $_POST['total_importan_case_border_color'];

				$data['todays_collection_border_color'] = "#" . $_POST['todays_collection_border_color'];
				$data['monthly_collection_border_color'] = "#" . $_POST['monthly_collection_border_color'];
				$data['monthly_expense_border_color'] = "#" . $_POST['monthly_expense_border_color'];

				// $data['leave_student_background_color'] = "#" . $_POST['leave_student_background_color'];
				$data['table_header_row_color'] = "#" . $_POST['table_header_row_color'];
				$data['table_body_row_color'] = "#" . $_POST['table_body_row_color'];
				$data['table_row_enable_for_report'] = $_POST['table_row_enable_for_report'];
			}else{
				$data['left_menu_background_color'] = '#282f3a';
				$data['top_bar_background_color'] = '#44519e';
				$data['top_menu_show_hide_button_color'] = '#282f3a';
				$data['school_name_border_color'] = "#63CF72";
		  	$data['head_name_border_color'] = "#2E5CB8";
				$data['today_attendance_border_color'] = "#2E5CB8";


				$data['support_center_border_color'] = "#5192AB";
				$data['total_running_case_border_color'] = "#5192AB";
				$data['total_colsed_case_border_color'] = "#325A69";

				$data['total_missaed_attendance_border_color'] = "#325A69";
				$data['sms_balance_border_color'] = "#00BFFF";
				$data['birthday_border_color'] = "#ECB3FF";
				$data['total_client_border_color'] ="#FFC299";
				$data['total_importan_case_border_color'] = "#F36368";

				$data['sms_balance_border_color'] = "#B3B3FF";
				$data['birthday_border_color'] = "#34BFA3";
				// $data['class_wise_student_border_color'] = "#5192AB";
				// $data['gender_wise_wise_border_color'] = "#5192AB";
				// $data['income_vs_expense_border_color'] = "#325A69";
				// $data['last_7_days_collection_border_color'] = "#325A69";
				$data['todays_collection_border_color'] = "#76C1FA";
				$data['monthly_collection_border_color'] = "#63CF72";
				$data['monthly_expense_border_color'] = "#F36368";
				// $data['total_student_background_color'] = "#00BFFF";
				// $data['present_student_background_color'] = "#ECB3FF";
				//$data['absent_student_background_color'] = "#F36368";
				// $data['leave_student_background_color'] = "#FFC299";
				$data['table_header_row_color'] = "#44519e";
				$data['table_body_row_color'] = "#dbdff0";
				$data['table_row_enable_for_report'] = "0";
			}

			$this->db->where('id', $data['id']);
			$this->db->update('tbl_dashboard_color_settings', $data);

			$sdata['message'] = "Dashboard configuration has been updated successfully";
			$this->session->set_userdata($sdata);
			redirect('dashboard_color_settings/index');
		}
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('dashboard_color_settings/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
