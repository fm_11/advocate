<?php

class Client_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate','Cases'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('member_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('client_user/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_member_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['members'] = $this->Advocate->get_all_member_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('client_user/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      	$lang=$this->session->userdata('site_lang');
        $user_info = $this->session->userdata('user_info');
        if ($_POST) {
            $data = array();
            $mobile=$this->input->post('mobile');
            if($this->Advocate->checkifexist_member_by_mobile($mobile))
            {
              $sdata['exception'] = "This mobile number'".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("client_user/add");
            }
            $photo_img='0';
            if (isset($_FILES['photo']) && $_FILES['photo']['name'] != '') {
                $photo_img = $this->Advocate->my_file_upload('photo', 'member');
                if ($photo_img=='0') {
                    $sdata['exception'] = "Photo doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                  redirect("client_user/add");
                }
            }
            // print_r($photo_img);
            // die();
            $data['mobile']=$mobile;
            $data['zip_code']=$this->input->post('zip_code');
            $data['first_name']=$this->input->post('first_name');
            $data['last_name']=$this->input->post('last_name');
            $data['email']=$this->input->post('email');
            $data['role_id']=$this->input->post('role_id');
            $data['address']=$this->input->post('address');
            $data['country_id']=1;//$this->input->post('country_id');
            $data['division_id']=$this->input->post('division_id');
            $data['district_id']=$this->input->post('district_id');
            $data['upazilas_id']=$this->input->post('upazilas_id');
            $data['photo']=$photo_img;
            $data['password'] = md5($this->input->post('password', true));
            if ($this->Advocate->add_member($data)) {
                $last_id = $this->db->insert_id();
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/logos/';
                $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
                $config['max_size'] = '5000';
                $config['max_width'] = '5000';
                $config['max_height'] = '5000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
                $new_photo_name = "user_" . time() . "_" . date('Y-m-d') . "." . $extension;
                move_uploaded_file($_FILES["photo"]["tmp_name"], MEDIA_FOLDER . "/logos/" . $new_photo_name);
                $data_user = array(
                    'name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                    'user_name' => $mobile,
                    'mobile' => $mobile,
                    'user_password' => md5($this->input->post("password")),
                    'user_role' => $this->input->post("role_id"),
                    'client_user_id' => $last_id,
                    'photo_location' => $new_photo_name,

                );
                $this->db->insert('tbl_user', $data_user);
                redirect("client_user/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("client_user/add");
            }
        }
        $data = array();
        $district_id=$user_info[0]->client_user[0]->district_id;
        $division_id=$user_info[0]->client_user[0]->division_id;

        $upazilla_id=$user_info[0]->client_user[0]->upazilas_id;
        $data['heading_msg'] = $data['title'] = $this->lang->line('member');
        $data['action'] = 'add';
        $data['language']=$lang;
	    	$data['is_show_button'] = "index";
        $data['countries'] = $this->Advocate->get_country_list_by_language($lang);
        $data['divisions']=$this->Cases->get_division_list_by_language_country($lang,1);
        $data['districts'] = $this->Cases->get_all_districts_list_by_division($division_id,$lang);
        $data['upazilas'] = $this->Cases->get_upazilas_list_by_district_and_lang($lang,$district_id);
        $data['roles'] = $this->Advocate->get_role_dropdown_list();
	      $data['district_id'] =$district_id;
  	    $data['upazilla_id'] = $upazilla_id;
    	  $data['division_id'] = $division_id;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('client_user/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
      	$lang=$this->session->userdata('site_lang');
        if ($_POST) {
          $data = array();
          $mobile=$this->input->post('mobile');
          if($this->Advocate->checkifexist_update_member_by_mobile($mobile,$id))
          {
            $sdata['exception'] = "This mobile number'".$name."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("client_user/edit/".$id);
          }
          if (isset($_FILES['photo']) && $_FILES['photo']['name'] != '') {
            $photo_img = $this->Advocate->my_file_upload('photo', 'member');
            if ($photo_img=='0') {
                $sdata['exception'] = "Photo doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                  redirect("client_user/edit/".$id);
            }
              $old_photo_file = $this->input->post('photo_img_name', true);
              $msg =$this->Advocate->my_file_remove($old_photo_file, $photo_img, MEDIA_FOLDER.'_media/member/');
           $data['photo']=$photo_img;
          }
          $data['id']=$id;
          $data['mobile']=$mobile;
          $data['zip_code']=$this->input->post('zip_code');
          $data['first_name']=$this->input->post('first_name');
          $data['last_name']=$this->input->post('last_name');
          $data['email']=$this->input->post('email');
          $data['role_id']=$this->input->post('role_id');
          $data['address']=$this->input->post('address');
          $data['country_id']=$this->input->post('country_id');
          $data['division_id']=$this->input->post('division_id');
          $data['district_id']=$this->input->post('district_id');
          $data['upazilas_id']=$this->input->post('upazilas_id');
          $pass=$this->input->post('password', true);
          if($pass!='' && $pass!='password')
          {
            $data['password'] = md5($pass);
          }

          if ($this->Advocate->edit_member($data, $id)) {
              $sdata['message'] = $this->lang->line('edit_success_message');
              $this->session->set_userdata($sdata);
            $new_photo_name='';
              if (isset($_FILES['photo']) && $_FILES['photo']['name'] != '') {
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/logos/';
                $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
                $config['max_size'] = '5000';
                $config['max_width'] = '5000';
                $config['max_height'] = '5000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
                $new_photo_name = "user_" . time() . "_" . date('Y-m-d') . "." . $extension;
                move_uploaded_file($_FILES["photo"]["tmp_name"], MEDIA_FOLDER . "/logos/" . $new_photo_name);
              }

              $data_user = array();
              $data_user['mobile']=$mobile;
              $data_user['user_name']=$mobile;
              $data_user['name']=$this->input->post('first_name').' '.$this->input->post('last_name');
              if($pass!='' && $pass!='password')
              {
                  $data_user['user_password'] = md5($pass);
              }
              if($new_photo_name!='')
              {
                   $data_user['photo_location']=$new_photo_name;
              }
              $data_user['client_user_id']=$id;
              $data_user['user_role']=$this->input->post("role_id");
              $this->db->update('tbl_user', $data_user, array('client_user_id' => $id));

              redirect("client_user/index");
          } else {
              $sdata['exception'] = $this->lang->line('edit_error_message');
              $this->session->set_userdata($sdata);
              redirect("client_user/index");
          }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('member');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_data'] = $this->Advocate->read_member($id);
        $data['row_data']->password='password';
        $data['language']=$lang;
        // print_r($data['row_data']->password='password');
        // die;
        $data['countries'] = $this->Advocate->get_country_list_by_language($lang);
        $data['divisions']=$this->Cases->get_division_list_by_language_country($lang,$data['row_data']->country_id);
        $data['districts'] = $this->Cases->get_all_districts_list_by_division($data['row_data']->division_id,$lang);
        $data['upazilas'] = $this->Cases->get_upazilas_list_by_district_and_lang($lang,$data['row_data']->district_id);

        $data['roles'] = $this->Advocate->get_role_dropdown_list();
        // $data['countries'] = $this->Advocate->get_country_list();
        // $data['districts'] = $this->Advocate->get_districts_list_by_country($data['row_data']->country_id);
        // $data['upazilas'] = $this->Advocate->get_upazilas_list_by_district($data['row_data']->district_id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('client_user/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $user_info = $this->session->userdata('user_info');
        if($user_info[0]->client_user_id==$id)
        {
          $sdata['exception'] = "This user cannot be deleted.Please contact your administrator.";
          $this->session->set_userdata($sdata);
          redirect("client_user/index");
        }
        die;
        $data = $this->db->where('id', $id)->get('ad_client_user')->row();
        $photo=$data->photo;
        $msg =$this->Advocate->my_file_remove($photo, 'test', MEDIA_FOLDER .'_media/member/');
        $this->Advocate->delete_member($id);
        $this->db->delete('tbl_user', array('client_user_id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("client_user/index");
    }
    public function updateMsgStatusMemberStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_client_user', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }
    public function ajax_division_by_country()
    {
      	$lang=$this->session->userdata('site_lang');
        $country_id = $_GET['country_id'];
        $data["dropdown_list"]= $this->Cases->get_division_list_by_language_country($lang,$country_id);
        $this->load->view('client_user/dropdown_list', $data);
    }
    public function ajax_district_by_division()
    {
      	$lang=$this->session->userdata('site_lang');
        $division_id = $_GET['division_id'];
        $data["dropdown_list"]= $this->Cases->get_all_districts_list_by_division($division_id,$lang);
        $this->load->view('client_user/dropdown_list', $data);
    }
    public function ajax_upazilas_by_district()
    {
      	$lang=$this->session->userdata('site_lang');
        $district_id = $_GET['district_id'];
        $data["dropdown_list"]= $this->Cases->get_upazilas_list_by_district_and_lang($lang,$district_id);
        $this->load->view('client_user/dropdown_list', $data);
    }
}
