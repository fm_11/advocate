<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Expense_details extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Account', 'Admin_login'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $cond = array();
        if ($_POST) {
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $sdata['from_date'] = $from_date;
            $sdata['to_date'] = $to_date;

            $this->session->set_userdata($sdata);
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        } else {
            $from_date = $this->session->userdata('from_date');
            $to_date = $this->session->userdata('to_date');
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        }
        $data['title'] = $this->lang->line('expense');
        $data['heading_msg'] = $this->lang->line('expense');

        $this->load->library('pagination');
        $config['base_url'] = site_url('expense_details/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Account->get_all_expense(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['expenses'] = $this->Account->get_all_expense(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('expense_details/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $user_info = $this->session->userdata('user_info');
        //echo $user_info[0]->id;
        //  die;
        $user_asset_head = $this->Admin_login->getUserWiseAssetHead($user_info[0]->id);
        // echo '<pre>';
        // print_r($user_asset_head);
        // die;
        if (empty($user_asset_head)) {
            $sdata['exception'] = "Please set asset head for this user";
            $this->session->set_userdata($sdata);
            redirect("user_wise_income_heads/index");
        }
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;

            $cost_center = $this->input->post("cost_center");

            $mdata = array();
            $mdata['date'] = $this->input->post("date");
            $mdata['expense_voucher_id'] = $this->input->post("expense_voucher_id");
            $mdata['expense_total_amount'] = $this->input->post("expense_total_amount");
            $mdata['cost_center_id'] = $cost_center;
            $mdata['add_time'] = date("Y-m-d H:i:s");
            $user_info = $this->session->userdata('user_info');
            $mdata['user_id'] = $user_info[0]->id;
            if ($this->db->insert('tbl_ba_expense', $mdata)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post("num_of_row");
                $i = 0;
                while ($i < $loop_time) {
                    $data = array();
                    $data['expense_id'] = $insert_id;
                    $data['expense_category_id'] = $this->input->post("expense_category_id_" . $i);
                    $data['expense_description'] = $this->input->post("expense_description_" . $i);
                    $data['deposit_method_id'] = $this->input->post("deposit_method_id_" . $i);
                    $data['cost_center'] = $cost_center;
                    $data['date'] =  date("Y-m-d H:i:s");
                    $data['expense_voucher_id'] =  $this->input->post("expense_voucher_id");
                    $data['amount'] = $this->input->post("amount_" . $i);
                    $this->db->insert('tbl_ba_expense_details', $data);
                    $i++;
                }

                //asset entry
                $assetdata = array();
                $assetdata['user_id'] = $user_asset_head[0]['user_id'];
                $assetdata['asset_id'] = $user_asset_head[0]['asset_category_id'];
                $assetdata['amount'] = $this->input->post("expense_total_amount");
                $assetdata['type'] = 'O';
                $assetdata['from_transaction_type'] = 'EX';
                $assetdata['date'] = $this->input->post("date");
                $assetdata['reference_id'] = $insert_id;
                $assetdata['added_by'] = $user_info[0]->id;
                $assetdata['remarks'] = 'From Expense (' . $this->input->post("expense_voucher_id") . ')';
                $this->db->insert('tbl_user_wise_asset_transaction', $assetdata);
                //asset entry

                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("expense_details/add");
            }
        }
        $data = array();
        $data['title'] = $this->lang->line('expense') .' ' .  $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('expense') . ' ' . $this->lang->line('add');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['is_show_button'] = "index";
        $data['expense_category'] = $this->db->query("SELECT * FROM tbl_ba_expense_category")->result_array();
        $data['deposit_method'] = $this->db->query("SELECT * FROM tbl_ba_deposit_method")->result_array();
        $data['cost_center'] = $this->db->query("SELECT * FROM tbl_cost_center")->result_array();
        $data['maincontent'] = $this->load->view('expense_details/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function expense_details_view($id)
    {
        $this->load->library('numbertowords');
        $data = array();
        $data['title'] = $this->lang->line('expense') . ' ' . $this->lang->line('view');
        $data['heading_msg'] = $this->lang->line('expense') . ' ' . $this->lang->line('view');
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['expense_info'] = $this->db->query("SELECT tbl_ba_expense.*,cc.name as cost_center FROM tbl_ba_expense
          left join tbl_cost_center as cc on cc.id = tbl_ba_expense.cost_center_id
           WHERE tbl_ba_expense.id = '$id'")->result_array();
        $data['expense_details_info'] = $this->db->query("SELECT ed.*,c.name as ex_category_name,dm.name as deposit_method_name FROM tbl_ba_expense_details as ed
                                        left join tbl_ba_expense_category as c on c.id = ed.expense_category_id
                                        left join tbl_ba_deposit_method as dm on dm.id = ed.deposit_method_id
                                        WHERE ed.expense_id = '$id'")->result_array();
        //echo '<pre>';
        //print_r($data['expense_details_info']);
        //die;
        $this->load->view('expense_details/expense_details_view', $data);
    }

    public function delete($id)
    {
        if ($this->db->delete('tbl_ba_expense_details', array('expense_id' => $id))) {
            $this->db->delete('tbl_ba_expense', array('id' => $id));
            $this->db->delete('tbl_user_wise_asset_transaction', array('reference_id' => $id,'type' => 'O','from_transaction_type' => 'EX'));
            $sdata['message'] = $this->lang->line('delete_success_message');
            $this->session->set_userdata($sdata);
            redirect("expense_details/index");
        }
    }
}
