<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class History extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login','Advocate','Message'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
      $lang=$this->session->userdata('site_lang');
      $user_info = $this->session->userdata('user_info');
      $data = array();
      $data['title'] = $this->lang->line('case').' '.$this->lang->line('history');
      $data['heading_msg'] = $this->lang->line('case').' '.$this->lang->line('history');
      if ($_POST) {
         $error=0;
          $case_master_id=$this->input->post('case_master_id');
          $business_on_date=date("Y-m-d", strtotime($this->input->post('bussines_on_date')));
          if($this->Advocate->checkifexist_case_history_status_closed_by_case_master_id($case_master_id))
          {
            $sdata['exception'] = "Sorry!.This case already closed.you can not add history.";
            $this->session->set_userdata($sdata);
            $error++;
          }

          if($this->Advocate->checkifexist_case_history_date_by_case_master_id($case_master_id,$business_on_date))
          {
           $sdata['exception'] = "This date '".$business_on_date."' already present for this case.";
           $this->session->set_userdata($sdata);
           $error++;
          }
          $case_no=$this->input->post('case_no');
          $case_sub_type_id=  $this->input->post('case_sub_type_id');
          if($this->Advocate->checkifexist_update_case_by_case_no($case_no,$case_sub_type_id,$case_master_id))
          {
            $sdata['exception'] = "This case no '".$case_no."' already exist.";
            $this->session->set_userdata($sdata);
            $error++;
          }

          $is_sms=$this->input->post('is_sms');
          $contact_info = $this->Advocate->get_client_info_by_case_master_id($case_master_id);
          if($is_sms==1)
          {
            if (!$this->Admin_login->validate_mobile_number($contact_info[0]['mobile'])) {
              $sdata['exception'] = "Your mobile number is invalid. please write the correct mobile number.";
              $this->session->set_userdata($sdata);
              $error++;
            }
          }
        //  $user_info = $this->session->userdata('user_info');
         if($error==0)
         {
           $history_data=array();
           $history_data['case_master_id']=$case_master_id;
           $history_data['case_no']=$case_no;
           $history_data['case_status_id']=$this->input->post('case_status_id');
           $history_data['bussines_on_date']=$business_on_date;
           $history_data['remarks']=$this->input->post('remarks');
           $history_data['parties_name']=$this->input->post('parties_name');
           $history_data['reason_for_assignment']=$this->input->post('reason_for_assignment');
           $history_data['reason_for_next_date']=$this->input->post('reason_for_next_date');
           $history_data['court_type_id']=$this->input->post('court_type_id');
           $history_data['court_id']=$this->input->post('court_id');
           $history_data['district_id']=$this->input->post('district_id');
           $history_data['upazilla_id']=$this->input->post('upazilla_id');
           $history_data['is_sms']=$is_sms;
           $history_data['created_by']=$user_info[0]->client_user_id;
           $history_data['created_on']=date('Y-m-d H:i:s');
            if($this->Advocate->checkifexist_advocate_setting())
            {
              $sdata['exception'] = $this->lang->line('advocate_setting_message');
              $this->session->set_userdata($sdata);
              redirect("history/index/");
            }


           if ($this->Advocate->add_case_history($history_data)){
              $history_id = $this->db->insert_id();
              if($this->input->post('last_id')!=0)
              {
                $last_data = array();
                $last_data['hearing_date']=$business_on_date;
                $this->Advocate->edit_case_history($last_data,$this->input->post('last_id'));
              }
              $case_details=array();
              $case_details['case_status_id']=$this->input->post('case_status_id');
              $this->Advocate->edit_case_master($case_details,$case_master_id);

              $sdata['message'] = $this->lang->line('add_success_message');
              $this->session->set_userdata($sdata);
              if ($history_data['is_sms']==1) {
                $setting=$this->Advocate->read_advocate_setting();
                $case_name=$this->Advocate->get_case_name_with_case_sub_type_by_language_and_caseid($lang,$case_master_id,$case_no);
                 $date = date("d-m-Y", strtotime($history_data['bussines_on_date']));
                 $user_name = $contact_info[0]['user_name'];
                 $client_id = $contact_info[0]['client_id'];
                //  if ($this->Admin_login->validate_mobile_number($contact_info[0]['mobile'])) {
                  $template_id = 0;
                  $template_body =$case_name." আপনার পরবর্তী হাজিরা তারিখ: $date ($setting->name) ধন্যবাদ।";
                  $send_from = "HS";
                  $loop_time = $this->input->post('loop_time', true);
                  $numbers = array();
                  $i = 0;
                  $numbers_with_student_id = array();
                  //sms send data ready
                  $number =  trim($contact_info[0]['mobile']);
                //  if($this->Admin_login->validate_mobile_number($number)){
                    $numbers[] = $number;
                //  }
                  $numbers_with_student_id[$number]['client_id'] = $client_id;
                  $numbers_with_student_id[$number]['number'] = $number;
                  $numbers_with_student_id[$number]['case_history_id'] = $history_id;

                  //sms send data ready
                  $status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
                  if (strtolower($status['status']) == 'success') {
                    $sdata['message'] = "SMS send successfully";
                    $this->session->set_userdata($sdata);
                    redirect("case_history/index/".$case_master_id);
                  } else {
                    $sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
                    $this->session->set_userdata($sdata);
                    redirect("case_history/add/".$case_master_id);
                  }
                  //sms send
              //  }
              }

           } else {
              $sdata['exception'] = $this->lang->line('add_error_message');
              $this->session->set_userdata($sdata);
           }
         }

      }
        $data['case_list'] = $this->Advocate->get_all_case_list_with_thana_for_dropdown_by_language($lang);
        // print_r($data['case_list']);
        // die;
        $data['language']=$lang;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('history/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);

    }
      public function ajax_get_case_history_by_case_id()
      {
         $lang=$this->session->userdata('site_lang');
         $case_master_id = $_GET['case_master_id'];
         $data =array();
         $data['language']=$lang;
         $data['case_master_id']=$case_master_id;
         $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
         $data['last_cast_history'] = $this->Advocate->get_case_last_history_details_by_mastr_id($case_master_id);
         $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id_and_language($data['last_cast_history']->district_id,$lang);
         $data['courts'] = $this->Advocate->get_court_list_by_district_id_and_court_type_id_and_language($data['last_cast_history']->district_id,$data['last_cast_history']->court_type_id,$lang);

         $this->load->view('history/dropdown_list', $data);

    }
}
