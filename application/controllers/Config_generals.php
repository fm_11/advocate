<?php

class Config_generals extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Loading Helper Class
        $this->load->database();
        $this->load->helper(array('form'));
        //Loading the model. The first param-> Model Name, 2nd Param: Custom Name, 3rd Param: AutoLoad Database. If 3rd parameter is not used, you have to load the database manually

        $this->load->model(array('Admin_login','Config_general'));
        //$this->output->enable_profiler(TRUE);
       date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    /**
     * Action for default general configuration view page
     * @uses     Creating default general configuration view page
     * @access	public
     * @param    void
     * @return	void
     * @author   Matin
     */
    public function index()
    {
        $data['title'] = 'General Configuration';
        $data['headline'] = 'General Configuration';
        // This Array is used for Showing Title For Each Purpose
        $data['purposes'] = array('general' => "General Configuration", 'dashboard' => "Dashboard Configuration", 'marksheet' => "Marksheet Configuration");
        // This Array is used for Showing Label Image for each purpose
        //  $data['label_image'] = array('general' => "configuration_16.png", 'operational_policy' => "administration_24.png");
        foreach ($data['purposes'] as $purpose => $label) {
            $data[$purpose] = $this->Config_general->get_list($purpose);
        }
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('config_generals/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    /**
     * View general configuration data
     * @uses     To view general configuration data
     * @access	public
     * @param    void
     * @return	void
     * @author   Matin
     */
    public function view()
    {
        $data['title'] = 'General Configuration';
        $data['headline'] = 'General Configuration';
        // This Array is used for Showing Title For Each Purpose
        $data['purposes'] = array('general' => "General Configuration", 'dashboard' => "Dashboard Configuration");
        // This Array is used for Showing Label Image for each purpose
        foreach ($data['purposes'] as $purpose => $label) {
            $data[$purpose] = $this->Config_general->get_list($purpose);
        }

        //If data is not posted or validation fails, the add view is displayed
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up general configurations
     * @author  :   Matin
     * @uses    :   To set up general configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function general_configuration()
    {
        if ($_POST) {
            $operation = 'general';
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'General Configuration has been updated successfully');
                $row = $this->Config_general->read('general');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'General Configuration';
        $data['headline'] = 'General Configuration';
        $row = $this->Config_general->get_list('general');
        $data['row'] = $row;
        //print_r($data);die;
        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';

        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('config_generals/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }



    public function dashboard_configuration()
    {
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $operation = 'dashboard';
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'Dashboard Configuration has been updated successfully');
                $row = $this->Config_general->read('dashboard');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'General Configuration';
        $data['headline'] = 'General Configuration';
        $row = $this->Config_general->get_list('dashboard');
        $data['row'] = $row;
        $data['load_view'] = 'config_generals/config_general_new';

        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('config_generals/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

	public function marksheet_configuration()
    {
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $operation = 'marksheet';
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'Marksheet Configuration Configuration has been updated successfully');
                $row = $this->Config_general->read('marksheet');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'Marksheet Configuration';
        $data['headline'] = 'Marksheet Configuration';
        $row = $this->Config_general->get_list('marksheet');
        $data['row'] = $row;
        $data['load_view'] = 'config_generals/config_general_new';

        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('config_generals/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    /**
     * Set up samity configurations
     * @author  :   Matin
     * @uses    :   To set up samity configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function samity_configuration()
    {
        if ($_POST) {
            $operation = 'samity';
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'Samity Configuration has been updated successfully');
                $row = $this->Config_general->read($operation);
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data['title'] = 'Samity Configuration';
        $data['headline'] = 'Samity Configuration';
        $row = $this->Config_general->get_list('samity');
        $data['row'] = $row;

        //print_r($data);die;
        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up member configurations
     * @author  :   Matin
     * @uses    :   To set up member configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function member_configuration()
    {
        $operation = 'member';

        if ($_POST) {
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'Member Configuration has been updated successfully');
                $row = $this->Config_general->read('member');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $row = $this->Config_general->get_list('member');
        $data['row'] = $row;
        //print_r($data);die;
        $data['title'] = 'Member Configuration';
        $data['headline'] = 'Member Configuration';

        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up savings configurations
     * @author  :   Matin
     * @uses    :   To set up savings configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function savings_configuration()
    {
        $operation = 'saving';
        if ($_POST) {
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'Savings Configuration has been updated successfully');
                $row = $this->Config_general->read();
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'Savings Configuration';
        $data['headline'] = 'Savings Configuration';
        $row = $this->Config_general->get_list('saving');
        $data['row'] = $row;

        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up loans configurations
     * @author  :   Matin
     * @uses    :   To set up loans configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function loan_configuration()
    {
        if ($_POST) {
            //$opreation='loan_configuration';
            $operation = 'loan';
            if ($this->edit($operation)) {
                $this->session->set_flashdata('message', 'Loan Configuration has been updated successfully');
                $row = $this->Config_general->read('loan');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'Loan Configuration';
        $data['headline'] = 'Loan Configuration';
        $row = $this->Config_general->get_list('loan');
        $data['row'] = $row;

        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up transfer configurations
     * @author  :   Matin
     * @uses    :   To set up transfer configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function transfer_configuration()
    {
        if ($_POST) {
            $opreation = 'transfer';
            if ($this->edit($opreation)) {
                $this->session->set_flashdata('message', 'Transfer Configuration has been updated successfully');
                $row = $this->Config_general->read('transfer');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data['title'] = 'Transfer Configuration';
        $data['headline'] = 'Transfer Configuration';
        $row = $this->Config_general->get_list('transfer');
        $data['row'] = $row;

        //print_r($data);die;
        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up report configurations
     * @author  :   Matin
     *
     * @uses    :   To set up report configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function report_configuration()
    {
        if ($_POST) {
            $opreation = 'report';
            if ($this->edit($opreation)) {
                $this->session->set_flashdata('message', 'Report Configuration has been updated successfully');
                $row = $this->Config_general->read('report');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data['title'] = 'Report Configuration';
        $data['headline'] = 'Report Configuration';
        $row = $this->Config_general->get_list('report');
        $data['row'] = $row;

        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up operational policy
     * @author   :   Matin
     * @uses     :   To set up operational policy
     * @access   :public
     * @param    :void
     * @return	 : void
     */
    public function operational_policy()
    {
        if ($_POST) {
            $opreation = 'operational_policy';
            if ($this->edit($opreation)) {
                //die;
                $this->session->set_flashdata('message', 'operational policy has been updated successfully');
                $row = $this->Config_general->read('operational_policy');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'Operational Policy';
        $data['headline'] = 'Operational Policy';
        $row = $this->Config_general->get_list('operational_policy');
        $data['row'] = $row;

        //print_r($data);die;
        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('config_generals/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function sms_service()
    {
        if ($_POST) {
            $opreation = 'sms';
            if ($this->edit($opreation)) {
                //die;
                $this->session->set_flashdata('message', 'SMS Service has been updated successfully');
                $row = $this->Config_general->read('sms');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'SMS Service';
        $data['headline'] = 'SMS Service';
        $row = $this->Config_general->get_list('sms');
        $data['row'] = $row;

        //print_r($data);die;
        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    public function day_end_configuration()
    {
        if ($_POST) {
            $opreation = 'day_end';
            if ($this->edit($opreation)) {
                //die;
                $this->session->set_flashdata('message', 'Day End Configuration has been updated successfully');
                $row = $this->Config_general->read('day_end');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'Day End';
        $data['headline'] = 'Day End';
        $row = $this->Config_general->get_list('day_end');
        $data['row'] = $row;

        //print_r($data);die;
        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }
    /**
     * Updates data of configurations
     * @author  :   Saiful Islam Feroz
     * @uses    :   To update data of configurations
     * @access  :   public
     * @param   :   $operation
     * @return  ;   void
     */
    public function edit($operation)
    {

        //If the form is posted, perform the validation. is_posted is a hidden input used to detect if the form is posted
        $is_validate_error = false;
        //$this->_prepare_validation($operation);
        //  if ($this->form_validation->run() === true) {
        $initial_data = $this->Config_general->get_list($operation);

        foreach ($initial_data as $value) {
            $updated_data=isset($_POST[$value->field_name])?trim($this->input->post($value->field_name)):"N$#!A";
            // echo "<pre>";
            // print_r($updated_data);
            // die;
            if ($updated_data!="N$#!A") {
                $data[$value->db_field_name] = $this->input->post($value->field_name);
                //echo $data[$value->db_field_name].'/';
                if ($value->field_type=="checkbox") {
                    if (isset($_POST[$value->field_name])) {
                        $data[$value->db_field_name] = $this->input->post($value->field_name);
                    } else {
                        $data[$value->db_field_name] =null;
                    }
                }
            }
            //  }
            // echo '<pre>';
            // print_r($data);
            // die;
            $this->Config_general->edit($data);
        }
        return true;
    }

    /**
     * Set up validation rules
     * @author  :   Saiful Islam Feroz
     * @uses    :   config_generals/add,config_generals/edit
     * @access	:   private
     * @param   ;   void
     * @return	:  void
     */
    public function _prepare_validation($operation)
    {
        //Loading Validation Library to Perform Validation
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $row = $this->Config_general->get_list($operation);
        $login_name = "admin";
        //Setting Validation Rule
        foreach ($row as $value) {
            if ($value->is_required == 1 && isset($_POST[$value->field_name]) && in_array($value->field_type, array("select", "text", "radio", 'file')) && ($value->is_admin_access_only == 0 || ($value->is_admin_access_only == 1 || ($value->is_admin_access_only == 1 && $login_name == "admin")))) {
                if ($value->field_type == "file" && $value->field_name == "txt_po_logo") {
                    $this->form_validation->set_rules('txt_po_logo', 'Organization logo', 'callback_po_logo_check_img');
                } elseif ($value->field_name == "txt_savings_minimum_balance_required_for_interest_calculation") {
                    $this->form_validation->set_rules('txt_savings_minimum_balance_required_for_interest_calculation', 'Minimum Balance required for Interest Calculation', 'trim|xss_clean|callback_savings_minimum_balance_required_for_interest_calculation|numeric');
                } elseif ($value->field_name == "txt_skt_amount") {
                    $this->form_validation->set_rules('txt_skt_amount', 'SKT Amount', 'trim|xss_clean|callback_is_required|positive_numeric|numeric');
                } else {
                    $this->form_validation->set_rules($value->field_name, $value->label_name, 'trim|xss_clean|max_length[150]|required');
                }
            }
            if ($value->is_required == 1 && $value->field_type == "date" && ($value->is_admin_access_only == 0 || ($value->is_admin_access_only == 1 || ($value->is_admin_access_only == 1 && $login_name == "admin")))) {
                $this->form_validation->set_rules($value->field_name, $value->label_name, 'trim|xss_clean|required|is_date');
            }
            if ($value->is_required == 1 && isset($_POST[$value->field_name]) && $value->field_type == "number" && ($value->is_admin_access_only == 0 || ($value->is_admin_access_only == 1 || ($value->is_admin_access_only == 1 && $login_name == "admin")))) {
                $this->form_validation->set_rules($value->field_name, $value->label_name, 'trim|xss_clean|required|is_numeric');
            }
            if ($value->is_required == 1 && isset($_POST[$value->field_name]) && $value->field_type == "password" && ($value->is_admin_access_only == 0 || ($value->is_admin_access_only == 1 || ($value->is_admin_access_only == 1 && $login_name == "admin")))) {
                $this->form_validation->set_rules($value->field_name, $value->label_name, 'trim|xss_clean|required|min_length[8]');
            }
            if ($value->is_required == 0) {
                $this->form_validation->set_rules($value->field_name, $value->label_name, 'trim|xss_clean');
            }
        }
    }

    /**
     * Checks po logo image
     * @author  :  Matin
     * @uses    :  Checks po logo image
     * @access  :  public
     * @param   :  void
     * @return  :  boolean
     */
    public function po_logo_check_img()
    {
        $res = true;
        $data = $this->session->userdata('txt_po_logo.error');
        //print_r($data);die;
        if (isset($data['error'])) {
            $this->form_validation->set_message('po_logo_check_img', $data['error']);
            $this->session->unset_userdata('txt_po_logo.error');
            $res = false;
        }
        return $res;
    }

    /**
     * Loads data to combo box
     * @author  :   Amlan Chowdhury
     * @uses    :   To load data to combo box
     * @access  :   private
     * @param   :   void
     * @return  :   array
     */
    public function _load_combo_data()
    {
        //This function is for listing of Holiday
        $data['interest_calculation_methods'] = array('' => '--Select--', 'FLAT' => 'FLAT METHOD', 'REDUCING' => 'DECLINING BALANCE');
        $data['options'] = array('0' => 'No', '1' => 'Yes');
        $data['savings_balance_used'] = array('' => '--Select--', 'MINIMUM_BALANCE' => 'MINIMUM BALANCE', 'AVERAGE_BALANCE' => 'AVERAGE BALANCE');
        $data['financial_year_start_month'] = array('' => '--Select--', 'January' => 'January', 'July' => 'July');
        $data['month_list'] = array(
            '' => '--Select--',
            'January' => 'January',
            'February' => 'February',
            'March' => 'March',
            'April' => 'April',
            'May' => 'May',
            'June' => 'June',
            'July' => 'July',
            'August' => 'August',
            'September' => 'September',
            'October' => 'October',
            'November' => 'November',
            'December' => 'December'
        );
        //echo json_encode($data['month_list']);
        $data['frequency_in_months'] = array('' => '--select--', '0' => '0 Month', '1' => '1 Month', '2' => '2 Month', '3' => '3 Month', '4' => '4 Month', '5' => '5 Month', '6' => '6 Month', '12' => '12 Month');
        //$data['member_wise_nominee_info_required']=array(''=>'--select--','1'=>'Member-wise','2'=>'Savings-wise');
        return $data;
    }

    /**
     * Checks skt amount required or not
     * @author  :  Matin
     * @uses    : Checks skt amount required or not
     * @access  :   private
     * @param   :   void
     * @return  :   boolean
     */
    public function is_required()
    {
        $is_skt_required = $this->input->post('cbo_is_skt_required');
        $txt_skt_amount = $this->input->post('txt_skt_amount');
        if ($is_skt_required == 1 && empty($txt_skt_amount)) {
            $this->form_validation->set_message('is_required', "You must give the SKT amount.");
            return false;
        }
        return true;
    }

    /**
     * Checks minimum savings amount given or not
     * @author  :   matin
     * @uses    :  Checks minimum savings amount given or not
     * @access  :   private
     * @param   :   void
     * @return  :   boolean
     */
    public function savings_minimum_balance_required_for_interest_calculation()
    {
        $savings_balance_used_for_interest_calculation = $this->input->post('cbo_savings_balance_used_for_interest_calculation');
        $savings_minimum_balance = $this->input->post('txt_savings_minimum_balance_required_for_interest_calculation');
        if ($savings_balance_used_for_interest_calculation == 'MINIMUM_BALANCE' && empty($savings_minimum_balance)) {
            $this->form_validation->set_message('savings_minimum_balance_required_for_interest_calculation', "You must give the savings minimum balance required for interest calculation'.");
            return false;
        }
        return true;
    }

    public function config_collectionsheets()
    {
        $data['title'] = "CollectionSheet Configuration";
        $data['collection_sheet_configurations'] = $this->Config_general->get_collection_sheet_configuration('collection_sheet');
        if ($_POST) {
            foreach ($data['collection_sheet_configurations'] as $config) {
                $status = $this->Config_general->update_collectionsheet_configuration($config->id, $this->input->post($config->field_name));
            }
            $this->session->set_flashdata('message', 'Collection Configuration has been updated successfully');
            $row = $this->Config_general->read();
            $data['config_general'] = $row;
            redirect('config_generals/index', $data);
        }
        $data['load_view'] = 'config_generals/collectionsheet_configuration';
        $this->load->view('config_generals/index', $data);
    }

    public function config_daily_collections()
    {
        $data['title'] = "Daily Collection Configuration";
        $data['daily_collection_configurations'] = $this->Config_general->get_daily_collection_configuration('daily_collections');
        //echo "<pre>";print_r($data);//die;
        if ($_POST) {
            foreach ($data['daily_collection_configurations'] as $config) {
                //echo "<pre>";print_r($config->field_name);//die;
                echo $field_name=$this->input->post($config->field_name);
                if ($config->field_name=='cbo_today_total_refund') {
                    //echo "<pre>";print_r($config);die;
                }

                $status = $this->Config_general->update_daily_collection_configuration($config->id, $field_name);
            }
            $this->session->set_flashdata('message', 'Collection Configuration has been updated successfully');
            $row = $this->Config_general->read();
            $data['config_general'] = $row;
            redirect('config_generals/index', $data);
        }
        $data['load_view'] = 'config_generals/daily_collection_configuration';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up Branch Opening configurations
     * @author  :   Tuhin
     *
     * @uses    :   To set up Branch Opening configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function branch_opening()
    {
        if ($_POST) {
            $opreation = 'branch_opening';
            if ($this->edit($opreation)) {
                //die;
                $this->session->set_flashdata('message', 'Branch Opening Configuration has been updated successfully');
                $row = $this->Config_general->read('branch_opening');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data = $this->_load_combo_data();
        $data['title'] = 'Branch Opening Configuration';
        $data['headline'] = 'Branch Opening Configuration';
        $row = $this->Config_general->get_list('branch_opening');
        $data['row'] = $row;

        //If data is not posted or validation fails, the add view is displayed
        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }

    /**
     * Set up report configurations
     * @author  :   Rafiur Rabby
     *
     * @uses    :   To set up report signature configurations
     * @access	public
     * @param    void
     * @return	void
     */
    public function report_signature()
    {
        if ($_POST) {
            $opreation = 'report_signature';
            if ($this->edit($opreation)) {
                $this->session->set_flashdata('message', 'Report Configuration has been updated successfully');
                $row = $this->Config_general->read('report');
                $data['config_general'] = $row;
                redirect('config_generals/index', $data);
            }
        }
        $data['title'] = 'Report Signature';
        $data['headline'] = 'Report Signature';
        $row = $this->Config_general->get_list('report_signature');
        //echo "<pre>"; print_r($row);
        $field=array();
        $i=0;
        $rows=array();
        $field_array=array();
        foreach ($row as $key=>$r) {
            $rows[$r->field_name]=$r;
        }
        foreach ($rows as $key=>$r) {
            $filed_name=$r->field_name;
            if (!array_key_exists($filed_name, $field_array)) {
                $field_array[$filed_name]=1;
                $field[$i]=$r;
                $i++;
                unset($row[$key]);
                foreach ($rows as $key2=>$r2) {
                    if ($r2->field_name == $filed_name.'_value') {
                        $field_array[$key2]=1;
                        $field[$i]=$r2;
                        $i++;
                        break;
                    }
                }
            }
            //}
        }
        //echo "<pre>"; print_r($field);
        //echo "<pre>"; print_r($row);
        $data['row'] = $field;

        $data['load_view'] = 'config_generals/config_general_new';
        $this->load->view('config_generals/index', $data);
    }
}
