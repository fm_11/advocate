<?php

class Divisions extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('division_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $bn_name = $this->input->post("bd_name");
            $sdata['name'] = $name;
            $sdata['bn_name'] = $bn_name;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['bn_name'] = $bn_name;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('divisions/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_divisions_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['divisions'] = $this->Advocate->get_all_divisions_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        //$data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('divisions/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
