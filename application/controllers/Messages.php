<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Messages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Message', 'Timekeeping', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }



    public function audio_message_upload()
    {
        //echo MEDIA_FOLDER; die;
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/audio/';
            $config['allowed_types'] = 'mp3';
            $config['max_size'] = '600000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            //echo $extension; die;
            if ($extension != 'mp3') {
                $sdata['exception'] = "Sorry .$extension file not allowed. Please upload .mp3 file !";
                $this->session->set_userdata($sdata);
                redirect("messages/audio_message_upload");
            }
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "audio_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/audio/" . $new_file_name)) {
                        $data = array();
                        $data['title'] = $this->input->post('title', true);
                        $data['location'] = $new_file_name;
                        $this->db->insert('tbl_audio_sms', $data);
                        $sdata['message'] = "You are Successfully Audio Uploaded ! ";
                        $this->session->set_userdata($sdata);
                        redirect("messages/audio_message_upload");
                    } else {
                        $sdata['exception'] = "Sorry Audio Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("messages/audio_message_upload");
                    }
                } else {
                    $sdata['exception'] = "Sorry Audio Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("messages/audio_message_upload");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Audio SMS/Messages Upload';
            $data['heading_msg'] = "Audio SMS/Messages Upload";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('messages/audio_message_upload', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function voice_sms_send()
    {
        $data = array();
        if ($_POST) {
            $data['class_id'] = $this->input->post('txtClass', true);
            $data['section_id'] = $this->input->post('txtSection', true);
            $data['group_id'] = $this->input->post('txtGroup', true);
            $data['student_info'] = $this->Message->get_student_info_by_student_id($data);
            $data['audios'] = $this->db->query("SELECT * FROM tbl_audio_sms")->result_array();
        }
        $data['title'] = 'Send Voice SMS To Guardian';
        $data['heading_msg'] = "Send Voice SMS To Guardian";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/to_guardian_voice_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function to_guardian_voice_sms_send()
    {
        if ($_POST) {
            $data = array();
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['group'] = $this->input->post('group_id', true);
            $data['audio_id'] = $this->input->post('audio_id', true);
            $data['date'] = date('Y-m-d H:i:s');

            $audio_id = $this->input->post('audio_id', true);
            $audio_info = $this->db->query("SELECT * FROM tbl_audio_sms WHERE id = '$audio_id'")->result_array();
            $location = $audio_info[0]['location'];


            $loop_time = $this->input->post('loop_time', true);
            $i = 0;
            $num = "";
            while ($i < $loop_time) {
                $is_send = $this->input->post('is_send_' . $i, true);
                if (isset($is_send) && $is_send != '') {

                        //sms send data ready
                    if ($i == 0) {
                        $num .= '"88'. $this->input->post('guardian_mobile_' . $i, true) .'"';
                    } else {
                        $num .= ',"88'. $this->input->post('guardian_mobile_' . $i, true) .'"';
                    }
                    //sms send data ready end
                }
                $i++;
            }

            // echo $num; die;

            $school_url = base_url()  . MEDIA_FOLDER . '/audio/'.$location;
            // echo $school_url; die;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                  CURLOPT_URL => "http://107.20.199.106/restapi/tts/3/multi",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  //CURLOPT_POSTFIELDS => "{ \n \"messages\":[ \n { \n \"from\":\"41793026700\",\n \"to\":[ \n \"8801711040536\",\n \"8801717136452\"\n ],\n \"audioFileUrl\": \"https://school360.app/school360/demo_media/audio/$location\"\n }]}",
                                         //{ "messages":[ { "from":"41793026700", "to":[ \"8801711040536\",\n \"8801911969314\" ], "audioFileUrl": "https://school360.app/school360/demo_media/audio/audio_1541955394_2018-11-11.mp3" }]}
                  CURLOPT_POSTFIELDS => "{ \n \"messages\":[ \n { \n \"from\":\"41793026700\",\n \"to\":[ \n $num \n ],\n \"audioFileUrl\": \"$school_url\"\n }]}",
                  CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "authorization: Basic c2Nob29sMzYwYXBwOjM3NjAhQFNhenphZA==",
                    "content-type: application/json"
                  ),
                ));

            $response = curl_exec($curl);
            //echo $response; die;
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $sdata['exception'] =  "cURL Error #:" . $err;
                $this->session->set_userdata($sdata);
                redirect("messages/voice_sms_send");
            } else {
                $sdata['message'] = "Voice Message Successfully.";
                $this->session->set_userdata($sdata);
                redirect("messages/voice_sms_send");
            }
        }
    }

    public function index()
    {
        $data = array();
        $message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
        if (empty($message_config)) {
            $sdata['exception'] = "Message panel configuration not found to database.";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }

        if ($message_config[0]['user_id'] != '') {
            //echo 555;die;
            $url = "http://api.smscpanel.net/g_api.php?token=". $message_config[0]['api_key'] ."&balance";
            $ch = curl_init(); // Initialize cURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
            $smsresult = curl_exec($ch);
            if (UKIL_DOPTOR_ID == 'greenlandms') {
                $data['sms_balance'] = ($smsresult / .40) . " SMS";
            } else {
                $data['sms_balance'] = $smsresult;
            }
        } else {
            $api_url = $message_config[0]['server_url'] . "/miscapi/" . $message_config[0]['api_key'] . "/getBalance";
            $ch = curl_init($api_url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $result = curl_exec($ch);
            curl_close($ch);
            $rdata = explode(":", $result);
            //echo '<pre>';
            // print_r($rdata);
            //die;
            $data['sms_balance'] = $rdata[1];
        }

        $data['title'] = 'SMS/Messages';
        $data['heading_msg'] = "SMS/Messages";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function to_result_notify()
    {
        $data = array();
        if ($_POST) {
            $data['class_id'] = $this->input->post('txtClass', true);
            $data['section_id'] = $this->input->post('txtSection', true);
            $data['group_id'] = $this->input->post('txtGroup', true);
            $data['shift_id'] = $this->input->post('txtShift', true);
            $data['exam_id'] = $this->input->post('txtExam', true);
            $data['sms_type'] = $this->input->post('txtSMSType', true);
            $data['with_school_name'] = $this->input->post('txtWithSchoolName', true);
            $data['student_info'] = $this->Message->get_student_info_by_student_id($data);
        }
        $data['title'] = 'Result Notify SMS Send';
        $data['heading_msg'] = "Result Notify SMS Send";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['shift'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
        $data['exam'] = $this->db->query("SELECT * FROM tbl_exam")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/result_notify_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function to_result_notify_sms_send()
    {
        if ($_POST) {
            $data = array();
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['group'] = $this->input->post('group_id', true);
            $data['exam_id'] = $this->input->post('exam_id', true);
            $data['date'] = date('Y-m-d');
            $sms_type = $this->input->post('sms_type', true);
            $with_school_name = $this->input->post('with_school_name', true);
            $school_name = "";
            if ($with_school_name == 'Y') {
                $school_info = $this->db->query("SELECT school_short_name FROM tbl_contact_info")->result_array();
                $school_name = $school_info[0]['school_short_name'];
            }
            $exam_id = $this->input->post('exam_id', true);
            $exam = $this->db->query("SELECT * FROM tbl_exam WHERE id = '$exam_id'")->result_array();
            $exam_name = $exam[0]['name'];
            if ($this->db->insert('tbl_message_result_notify', $data)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post('loop_time', true);
                $i = 0;
                while ($i < $loop_time) {
                    $is_send = $this->input->post('is_send_' . $i, true);
                    if (isset($is_send) && $is_send != '') {
                        if ($this->Admin_login->validate_mobile_number($this->input->post("guardian_mobile_" . $i))) {
                            $msg = $this->generate_result_message($this->input->post("student_id_" . $i), $data['class_id'], $data['exam_id'], $sms_type);
                            $msg .= ', ' . $school_name;
                            if ($msg != "") {
                                $single_num = "";
                                $single_num = $this->Admin_login->generateNumberlist($single_num, $this->input->post("guardian_mobile_" . $i));

                                $sms_info = array();
                                $sms_info['message'] = $exam_name . " Result, Name: " . $this->input->post("name_" . $i) . "::" . $msg . ". Thank You";
                                $sms_info['numbers'] = $single_num;
                                $sms_info['is_bangla_sms'] = 0;
                                $status = $this->Message->sms_send($sms_info);
                            } else {
                                $status = false;
                            }
                        }

                        $cdata = array();
                        $cdata['to_message_id'] = $insert_id;
                        $cdata['student_id'] = $this->input->post('student_id_' . $i, true);
                        $cdata['mobile_number'] = $this->input->post('guardian_mobile_' . $i, true);
                        $cdata['date'] = date('Y-m-d');
                        if ($status) {
                            $cdata['message'] = $sms_info['message'];
                        } else {
                            $cdata['message'] = "Not Send SMS. Coz Result Info. Not Found";
                        }

                        $this->db->insert('tbl_message_result_notify_student_list', $cdata);
                    }
                    $i++;
                }
                $sdata['message'] = "SMS send successfully.";
                $this->session->set_userdata($sdata);
                redirect("messages/to_result_notify");
            } else {
                $sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
                $this->session->set_userdata($sdata);
                redirect("messages/to_result_notify");
            }
        }
    }


    //result message
    public function generate_result_message($StudentID, $ClassID, $ExamID, $sms_type)
    {
        $result_info = $this->Student->result_by_student_after_process($StudentID, $ExamID);

        // echo '<pre>';
        //print_r($result_info);
        //die;
        $msg = "";
        if (!empty($result_info)) {
            for ($A = 0; $A < (count($result_info['result_process_details'])); $A++) {
                if ($A != 0) {
                    if ($sms_type == 'MG') {
                        $msg .= ',' . $result_info['result_process_details'][$A]['subject_code'] . '=' . $result_info['result_process_details'][$A]['total_obtain'] . '(' . $result_info['result_process_details'][$A]['alpha_gpa'] . ')';
                    } elseif ($sms_type == 'M') {
                        $msg .= ',' . $result_info['result_process_details'][$A]['subject_code'] . '=' . $result_info['result_process_details'][$A]['total_obtain'];
                    } else {
                        $msg .= ',' . $result_info['result_process_details'][$A]['subject_code'] . '=' . $result_info['result_process_details'][$A]['alpha_gpa'];
                    }
                } else {
                    if ($sms_type == 'MG') {
                        $msg .= $result_info['result_process_details'][$A]['subject_code'] . '=' . $result_info['result_process_details'][$A]['total_obtain'] . '(' . $result_info['result_process_details'][$A]['alpha_gpa'] . ')';
                    } elseif ($sms_type == 'M') {
                        $msg .= $result_info['result_process_details'][$A]['subject_code'] . '=' . $result_info['result_process_details'][$A]['total_obtain'];
                    } else {
                        $msg .= $result_info['result_process_details'][$A]['subject_code'] . '=' . $result_info['result_process_details'][$A]['alpha_gpa'];
                    }
                }
            }
            // echo $A;die;
            $msg .= ", Total Mark :" . $result_info['result_process_info'][0]['total_obtain_mark'] . ", CGPA :" . $result_info['result_process_info'][0]['gpa_with_optional'];
        }
        //echo $msg; die;
        return $msg;
    }

    //result message

    public function to_excel_file()
    {
        if ($_POST) {
            //For Excel Read
            require_once APPPATH . 'third_party/PHPExcel.php';
            $this->excel = new PHPExcel();
            //For Excel Read

            $template_id = $this->input->post('template_id', true);
            $template_body = $this->input->post('template_body', true);
            $sms_type = $this->input->post('sms_type', true);

            //For Excel Upload
            $configUpload['upload_path'] = FCPATH . 'uploads/excel/';
            $configUpload['allowed_types'] = 'xls|xlsx|csv';
            $configUpload['max_size'] = '15000';
            $this->load->library('upload', $configUpload);
            $this->upload->do_upload('txtFile');
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension = $upload_data['file_ext'];    // uploded file extension

            if ($extension == '.xlsx') {
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
            } else {
                $objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
            }


            //Set to read only
            $objReader->setReadDataOnly(true);
            //Load excel file
            $objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            //loop from first data untill last data
            $num = "";
            for ($i = 2; $i <= $totalrows; $i++) {
                $number = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
                if ($number == '') {
                    break;
                }
                if ($template_id == 'N') {
                    $message_body = ltrim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
                    if ($sms_type == 'B') {
                        $is_bangla_sms = 1;
                    } else {
                        $is_bangla_sms = 0;
                    }

                    $single_num = "";
                    $single_num = $this->Admin_login->generateNumberlist($single_num, $number);

                    $sms_info = array();
                    $sms_info['message'] = $message_body;
                    $sms_info['numbers'] = $single_num;
                    $sms_info['is_bangla_sms'] = $is_bangla_sms;
                    $this->Message->sms_send($sms_info);
                } else {
                    if ($sms_type == 'B') {
                        $is_bangla_sms = 1;
                    } else {
                        $is_bangla_sms = 0;
                    }
                    //sms send data ready
                    $num = $this->Admin_login->generateNumberlist($num, $number);
                    //sms send data ready end
                }
            }
            //For Excel Upload
            // echo $num; die;
            //only number sms send
            if ($template_id != 'N') {
                $sms_info = array();
                $sms_info['message'] = $template_body;
                $sms_info['numbers'] = $num;
                $sms_info['is_bangla_sms'] = $is_bangla_sms;
                //echo '<pre>';
                // print_r($sms_info);
                // die;
                $status = $this->Message->sms_send($sms_info);
            }
            //only number sms send

            $filedel = PUBPATH . 'uploads/excel/' . $file_name;
            if (unlink($filedel)) {
                $sdata['message'] = "You are Successfully Send (" . ($i-2) . ") SMS";
                $this->session->set_userdata($sdata);
                redirect("messages/to_excel_file");
            }
        } else {
            $data = array();
            $data['title'] = 'Send SMS From Excel File';
            $data['heading_msg'] = "Send SMS From Excel File";
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('messages/to_excel_file_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function to_teacher_sms_send_form()
    {
        $data = array();
        $data['title'] = 'Send SMS To Teacher';
        $data['heading_msg'] = "Send SMS To Teacher";
        $data['teacher_info'] = $this->Message->get_all_teachers();
        $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/to_teacher_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function to_teacher_sms_send()
    {
        if ($_POST) {
            $data = array();
            $data['template_id'] = $this->input->post('template_id', true);
            $data['template_body'] = $this->input->post('template_body', true);
            $data['date'] = date('Y-m-d H:i:s');

            $template_id = $this->input->post('template_id', true);
            $template_info = $this->db->query("SELECT is_bangla_sms FROM tbl_message_template WHERE id = '$template_id'")->result_array();
            $is_bangla_sms_template = $template_info[0]['is_bangla_sms'];

            if ($this->db->insert('tbl_message_to_teacher', $data)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post('loop_time', true);
                $i = 0;
                $num = "";
                while ($i < $loop_time) {
                    $is_send = $this->input->post('is_send_' . $i, true);
                    if (isset($is_send) && $is_send != '') {

                        //sms send data ready
                        $num = $this->Admin_login->generateNumberlist($num, $this->input->post('mobile_' . $i, true));
                        //sms send data ready end


                        $cdata = array();
                        $cdata['to_teacher_message_id'] = $insert_id;
                        $cdata['teacher_id'] = $this->input->post('teacher_id_' . $i, true);
                        $cdata['mobile'] = $this->input->post('mobile_' . $i, true);
                        $cdata['date'] = date('Y-m-d');
                        $this->db->insert('tbl_message_to_teacher_list', $cdata);
                    }
                    $i++;
                }

                $sms_info = array();
                $sms_info['message'] = $this->input->post('template_body', true);
                $sms_info['numbers'] = $num;
                $sms_info['is_bangla_sms'] = $is_bangla_sms_template;
                $status = $this->Message->sms_send($sms_info);
                if ($status) {
                    $sdata['message'] = "SMS send successfully.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/to_teacher_sms_send_form");
                } else {
                    $sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/to_teacher_sms_send_form");
                }
            } else {
                $sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
                $this->session->set_userdata($sdata);
                redirect("messages/to_teacher_sms_send_form");
            }
        }
    }


    public function absent_sms_send_form()
    {
        $data = array();
        if ($_POST) {
            $data['class_id'] = $this->input->post('txtClass', true);
            $data['section_id'] = $this->input->post('txtSection', true);
            $data['group_id'] = $this->input->post('txtGroup', true);
            $data['student_info'] = $this->Message->get_student_info_by_student_id($data);
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Absent SMS Send To Guardian';
        $data['heading_msg'] = "Absent SMS Send To Guardian";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['shift'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/absent_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function absent_sms_send()
    {
        $date = $_POST['txtDate'];
        $shift_id = $_POST['txtShift'];
        $data['student_info'] = $this->Timekeeping->get_student_report_absentee_for_sms_by_date_and_shift($date, $shift_id);
        //echo '<pre>';
        //print_r($data['student_info']);
        //die;
        $data['title'] = 'Absent SMS Send To Guardian';
        $data['heading_msg'] = "Absent SMS Send To Guardian";
        $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/absent_sms_send_form_table', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function absent_sms_send_to_guardian()
    {
        if ($_POST) {
            $loop_time = $this->input->post('loop_time', true);
            $num = "";
            $i = 0;
            while ($i < $loop_time) {
                $is_send = $this->input->post('is_send_' . $i, true);
                if (isset($is_send) && $is_send != '') {

                    //sms send data ready
                    $num = $this->Admin_login->generateNumberlist($num, $this->input->post('guardian_mobile_' . $i, true));
                    //sms send data ready end
                }
                $i++;
            }
            //echo $num;
            //die;

            $template_id = $this->input->post('template_id', true);
            $template_info = $this->db->query("SELECT is_bangla_sms FROM tbl_message_template WHERE id = '$template_id'")->result_array();
            $is_bangla_sms_template = $template_info[0]['is_bangla_sms'];
            //echo $template_id; die;

            $sms_info = array();
            $sms_info['message'] = $this->input->post('template_body', true);
            $sms_info['numbers'] = $num;
            $sms_info['is_bangla_sms'] = $is_bangla_sms_template;
            //echo '<pre>';
            // print_r($sms_info);
            // die;
            $status = $this->Message->sms_send($sms_info);
            if ($status) {
                $sdata['message'] = "SMS send successfully.";
                $this->session->set_userdata($sdata);
                redirect("messages/absent_sms_send_form");
            } else {
                $sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
                $this->session->set_userdata($sdata);
                redirect("messages/absent_sms_send_form");
            }
        }
    }





    
    public function getTemplateBodyById()
    {
        $template_id = $this->input->get('template_id', true);
        $template_info = $this->db->query("SELECT template_body FROM tbl_message_template WHERE `id` = '$template_id'")->result_array();
        echo $template_info[0]['template_body'];
    }

    public function all_type_send_sms()
    {
        $data = array();
        $data['title'] = 'SMS Send';
        $data['heading_msg'] = "SMS Send";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/all_type_send_sms_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }








    public function AddNewContactFromExcel()
    {
        if ($_POST) {

            //For Excel Read
            require_once APPPATH . 'third_party/PHPExcel.php';
            $this->excel = new PHPExcel();
            //For Excel Read

            $category_id = $this->input->post('category_id', true);

            //For Excel Upload
            $configUpload['upload_path'] = FCPATH . 'uploads/excel/';
            $configUpload['allowed_types'] = 'xls|xlsx|csv';
            $configUpload['max_size'] = '15000';
            $this->load->library('upload', $configUpload);
            $this->upload->do_upload('txtFile');
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension = $upload_data['file_ext'];    // uploded file extension

            if ($extension == '.xlsx') {
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
            } else {
                $objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
            }


            //Set to read only
            $objReader->setReadDataOnly(true);
            //Load excel file
            $objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            //loop from first data untill last data
            $num = "";
            for ($i = 2; $i <= $totalrows; $i++) {
                $name = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
                if ($name == '') {
                    break;
                }
                $number = ltrim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
                if ($this->validate_mobile_number($number)) {
                    $insertData = array();
                    $insertData['contact_name'] = $name;
                    $insertData['contact_number'] = $number;
                    $insertData['category_id'] = $category_id;
                    $tableName = "tbl_message_contact_list";
                    $this->Message->DoCommonInsert($insertData, $tableName);
                }
            }
            //For Excel Upload

            $filedel = PUBPATH . 'uploads/excel/' . $file_name;
            if (unlink($filedel)) {
                $sdata['message'] = "All Contact Has Been Added Successfully.";
                $this->session->set_userdata($sdata);
                redirect('messages/AddNewContactFromExcel', 'refresh');
            }
        }
        $data = array();
        $data['title'] = 'Add New Contact From Excel';
        $data['heading_msg'] = "Add New Contact From Excel";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('messages/AddNewContactFromExcel', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function AddNewContact()
    {
        if (isset($_POST['submit'])) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $insertData['contact_name'] = $_POST['ContactName'];
            $insertData['contact_number'] = $_POST['ContactNumber'];
            $insertData['category_id'] = $_POST['PhoneBookCategory'];
            $tableName = "tbl_message_contact_list";
            $returnData = $this->Message->DoCommonInsert($insertData, $tableName);
            if ($returnData['status'] == 1) {
                $sdata['message'] = "New Contact Has Been Added Successfully.";
            } else {
                $sdata['exception'] = "New Contact Can't Be Added Successfully.";
            }
            $this->session->set_userdata($sdata);

            redirect('messages/AddNewContact', 'refresh');
        }
        $data = array();
        $data['title'] = 'Add New Contact';
        $data['heading_msg'] = "Add New Contact";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('messages/AddNewContact', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function validate_mobile_number($phoneNumber)
    {
        if (!empty($phoneNumber)) { // phone number is not empty
            if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
                return true;
            // your other code here
            } else { // phone number is not valid
                return false;
            }
        } else { // phone number is empty
            return false;
        }
    }

    public function DeletePhoneBookData($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_message_contact_list` WHERE `id` = '$id' LIMIT 1")->result_array();
        /* echo '<pre>';
        print_r($Check); */
        if (!empty($Check)) {
            if ($this->db->query("DELETE FROM tbl_message_contact_list WHERE `id` = '$id'")) {
                $sdata['message'] = "Data Deleted Successfully.";
            } else {
                $sdata['exception'] = "Data Can't Be Deleted Successfully!";
            }
            $this->session->set_userdata($sdata);
            redirect('messages/PhoneBookIndex/', 'refresh');
        } else {
            $sdata['exception'] = "Invalid Data Which You Want To Delete !";
            $this->session->set_userdata($sdata);
            redirect('messages/PhoneBookIndex/', 'refresh');
        }
    }

    public function EditPhoneBookData($id)
    {
        if ($_POST) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $updateData = array();
            $updateData['contact_name'] = $_POST['ContactName'];
            $updateData['contact_number'] = $_POST['ContactNumber'];
            $updateData['category_id'] = $_POST['PhoneBookCategory'];
            $this->db->where('id', $id);
            $this->db->update('tbl_message_contact_list', $updateData);
            $sdata['message'] = "Data Updated Successfully.";
            $this->session->set_userdata($sdata);
            redirect('messages/PhoneBookIndex', 'refresh');
        }
        $data = array();
        $data['title'] = 'Edit Phone Book';
        $data['heading_msg'] = "Edit Phone Book";
        $data['EditData'] = $this->db->query("SELECT * FROM tbl_message_contact_list WHERE `id` = '$id'")->row_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('messages/EditPhoneBookData', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    //////// edit by dip (06-06-17)////////////////

    //////// edit by dip (07-06-17)////////////////
    public function SendBirthDaySMS()
    {
        $data = array();
        if ($_POST) {
            $ReceiverType = $this->input->post('ReceiverType', true);
            if ($ReceiverType == 'Student') {
                $data['teacherSMSStatus'] = 0;
                $data['studentSMSStatus'] = 1;
                $student_inforeturn = $this->Message->GetStudentInfoByBirthday();
                if ($student_inforeturn['status'] == 1) {
                    $data['studentInfoStatus'] = 1;
                    $data['studentInfo'] = $student_inforeturn['studentData'];
                } else {
                    $data['studentInfoStatus'] = 0;
                }
            } elseif ($ReceiverType == 'Teacher') {
                $data['teacherSMSStatus'] = 1;
                $data['studentSMSStatus'] = 0;
                $teacher_inforeturn = $this->Message->GetTeacherInfoByBirthday();
                if ($teacher_inforeturn['status'] == 1) {
                    $data['teacherInfoStatus'] = 1;
                    $data['teacherInfo'] = $teacher_inforeturn['teacherData'];
                } else {
                    $data['teacherInfoStatus'] = 0;
                }
            }

            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Send Birth Day SMS';
        $data['heading_msg'] = "Send Birth Day SMS";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/SendBirthDaySMS', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    //////// edit by dip (07-06-17)////////////////

    //////// edit by dip (09-06-17)////////////////
    public function studentBirthDaySMS()
    {
        if ($_POST) {
            /* echo '<pre>';
            print_r($_POST); */
            $data = array();
            $data['birthDate'] = $this->input->post('birthDate', true);
            $data['template_id'] = $this->input->post('template_id', true);
            $data['template_body'] = $this->input->post('template_body', true);
            $data['date'] = date('Y-m-d H:i:s');
            if ($this->db->insert('tbl_message_for_birthday', $data)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post('loop_time', true);
                $i = 0;
                $num = "";
                while ($i < $loop_time) {
                    $is_send = $this->input->post('is_send_' . $i, true);
                    if (isset($is_send) && $is_send != '') {

                        //sms send data ready
                        $num = $this->Admin_login->generateNumberlist($num, $this->input->post('guardian_mobile_' . $i, true));
                        //sms send data ready end

                        $cdata = array();
                        $cdata['to_message_for_birthday_id'] = $insert_id;
                        $cdata['student_id'] = $this->input->post('student_id_' . $i, true);
                        $cdata['class_id'] = $this->input->post('student_class_id_' . $i, true);
                        $cdata['section_id'] = $this->input->post('student_section_id_' . $i, true);
                        $cdata['group_id'] = $this->input->post('student_group_id_' . $i, true);
                        $cdata['mobile_number'] = $this->input->post('guardian_mobile_' . $i, true);
                        $cdata['date'] = date('Y-m-d');
                        $this->db->insert('tbl_message_for_birthday_to_guardian_student_list', $cdata);
                    }
                    $i++;
                }
                $sms_info = array();
                $sms_info['message'] = $this->input->post('template_body', true);
                $sms_info['numbers'] = $num;
                $sms_info['is_bangla_sms'] = 1;
                $status = $this->Message->sms_send($sms_info);
                if ($status) {
                    $sdata['message'] = "SMS send successfully.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/SendBirthDaySMS");
                } else {
                    $sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/view_category_phoneBook_list");
                }
            } else {
                $sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
                $this->session->set_userdata($sdata);
                redirect("messages/SendBirthDaySMS");
            }
        }
    }

    public function teacherBirthDaySMS()
    {
        if ($_POST) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $data = array();
            $data['birthDate'] = $this->input->post('birthDate', true);
            $data['template_id'] = $this->input->post('template_id', true);
            $data['template_body'] = $this->input->post('template_body', true);
            $data['date'] = date('Y-m-d H:i:s');
            if ($this->db->insert('tbl_message_for_birthday', $data)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post('loop_time', true);
                $i = 0;
                $num = "";
                while ($i < $loop_time) {
                    $is_send = $this->input->post('is_send_' . $i, true);
                    if (isset($is_send) && $is_send != '') {

                        //sms send data ready
                        $num = $this->Admin_login->generateNumberlist($num, $this->input->post('teacher_mobile_' . $i, true));
                        //sms send data ready end

                        $cdata = array();
                        $cdata['to_message_for_birthday_id'] = $insert_id;
                        $cdata['teacher_id'] = $this->input->post('teacher_id_' . $i, true);
                        $cdata['mobile_number'] = $this->input->post('teacher_mobile_' . $i, true);
                        $cdata['date'] = date('Y-m-d');
                        $this->db->insert('tbl_message_for_birthday_to_teacher', $cdata);
                    }
                    $i++;
                }
                $sms_info = array();
                $sms_info['message'] = $this->input->post('template_body', true);
                $sms_info['numbers'] = $num;
                $status = $this->Message->sms_send($sms_info);
                if ($status) {
                    $sdata['message'] = "SMS send successfully.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/SendBirthDaySMS");
                } else {
                    $sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/SendBirthDaySMS");
                }
            } else {
                $sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
                $this->session->set_userdata($sdata);
                redirect("messages/SendBirthDaySMS");
            }
        }
    }

    //////// edit by dip (09-06-17)////////////////
    //////// edit by dip (11-06-17)////////////////
    public function view_category_phoneBook_list()
    {
        $data = array();
        if ($_POST) {
            $CategoryType = $this->input->post('CategoryType', true);
            $CategoryInfo = $this->Message->GetCategoryInfo($CategoryType);
            $data['CategoryInfo'] = $CategoryInfo;
            /* echo '<pre>';
            print_r($CategoryInfo);
            die(); */
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Send SMS From Phone Book';
        $data['heading_msg'] = "Send SMS From Phone Book";
        $data['category'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/view_category_phoneBook_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function SendcategoryPhoneBookSMS()
    {
        if ($_POST) {
            $data = array();
            $data['categoryId'] = $this->input->post('categoryId', true);
            $data['template_id'] = $this->input->post('template_id', true);
            $data['template_body'] = $this->input->post('template_body', true);
            $data['date'] = date('Y-m-d H:i:s');
            $num = "";
            if ($this->db->insert('tbl_message_to_phonebook', $data)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post('loop_time', true);
                $i = 0;
                while ($i < $loop_time) {
                    $is_send = $this->input->post('is_send_' . $i, true);
                    if (isset($is_send) && $is_send != '') {

                        //sms send data ready
                        $num = $this->Admin_login->generateNumberlist($num, $this->input->post('mobile_' . $i, true));
                        //sms send data ready end

                        $cdata = array();
                        $cdata['to_message_for_birthday_id'] = $insert_id;
                        $cdata['smsReciver_id'] = $this->input->post('smsReciver_id_' . $i, true);
                        $cdata['mobile_number'] = $this->input->post('mobile_' . $i, true);
                        $cdata['date'] = date('Y-m-d');
                        $this->db->insert('tbl_message_to_phonebook_list', $cdata);
                    }
                    $i++;
                }

                $sms_info = array();
                $sms_info['message'] = $this->input->post('template_body', true);
                $sms_info['numbers'] = $num;
                $sms_info['is_bangla_sms'] = 1;
                $status = $this->Message->sms_send($sms_info);
                if ($status) {
                    $sdata['message'] = "SMS send successfully.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/view_category_phoneBook_list");
                } else {
                    $sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
                    $this->session->set_userdata($sdata);
                    redirect("messages/view_category_phoneBook_list");
                }
            } else {
                $sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
                $this->session->set_userdata($sdata);
                redirect("messages/view_category_phoneBook_list");
            }
        }
    }

    public function GetGuardianSMSReport()
    {
        $data = array();
        if (isset($_POST['submit'])) {
            /*  */
            $FromDate = $this->input->post('FromDate', true);
            $ToDate = $this->input->post('ToDate', true);
            $data['GuardianSMSReport'] = $this->Message->GetGuardianSMSReport($FromDate, $ToDate);
            /* echo '<pre>';
            print_r($data['GuardianSMSReport']);
            die(); */
        }

        $data['title'] = 'Guardian SMS Report';
        $data['heading_msg'] = "Guardian SMS Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        /* echo '<pre>';
        print_r($data); */
        $data['maincontent'] = $this->load->view('messages/GetGuardianSMSReport', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function GetDetailsSMSReport()
    {
        if (isset($_POST['submit'])) {
            /*  */
            $FromDate = $this->input->post('FromDate', true);
            $ToDate = $this->input->post('ToDate', true);
            $data['DetailsSMSReport'] = $this->Message->GetDetailsSMSReport($FromDate, $ToDate);
            /* echo '<pre>';
            print_r($data['DetailsSMSReport']);
            die(); */
        }


        $data['title'] = 'Details SMS Report';
        $data['heading_msg'] = "Details SMS Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/GetDetailsSMSReport', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function SMSSummaryReport()
    {
        if (isset($_POST['submit'])) {
            /*  */
            $FromDate = $this->input->post('FromDate', true);
            $ToDate = $this->input->post('ToDate', true);
            $data['DetailsSMSReport'] = $this->Message->GetDetailsSMSReport($FromDate, $ToDate);
            /* echo '<pre>';
            print_r($data['DetailsSMSReport']);
            die(); */
        }
        $data['title'] = 'SMS Summary Report';
        $data['heading_msg'] = "SMS Summary Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/SMSSummaryReport', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    //////// edit by dip (11-06-17)////////////////
}
