
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fund_transfer_report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $data['from_date'] = $this->input->post("from_date");
            $data['to_date'] = $this->input->post("to_date");


            $data['idata'] = $this->db->query("SELECT
acf.`name` AS from_asset_head,
act.`name` AS to_asset_head,
u.`name` AS user_name,
ft.*
FROM `tbl_ba_fund_transfer` AS ft
INNER JOIN `tbl_asset_category` AS acf ON acf.`id` = ft.`from_asset_category_id`
INNER JOIN `tbl_asset_category` AS act ON act.`id` = ft.`to_asset_category_id`
LEFT JOIN `tbl_user` AS u ON u.`id` = ft.`user_id`
WHERE ft.`date` BETWEEN '$from_date' AND '$to_date'
ORDER BY ft.`date`")->result_array();
            $data['report'] = $this->load->view('fund_transfer_report/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('fund_transfer_report/report', $data, true);
            $this->mydompdf->createPDF($html, 'FundTransferReport', true);
        //Dom PDF
        } else {
            $data['title'] = 'Fund Transfer ' . $this->lang->line('report');
            $data['heading_msg'] = 'Fund Transfer ' . $this->lang->line('report');
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('fund_transfer_report/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
