<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class File_colours extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('file_colours');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $sdata['name'] = $name;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
        } else {
            $name = $this->session->userdata('name');
            $cond['name'] = $name;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('file_colours/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_file_colours_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['file_colours'] = $this->Advocate->get_all_file_colours_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('file_colours/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $name=$this->input->post('name');
            $data['name']=$name;
            $data['bn_name']=$this->input->post('bn_name');
            if($this->Advocate->checkifexist_file_colours_by_name($name))
            {
              $sdata['exception'] = "This name '".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("file_colours/add");
            }
            if ($this->Advocate->add_file_colours($data)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                  redirect("file_colours/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("file_colours/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('file_colours');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('file_colours/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $name=$this->input->post('name');
          $data['name']=$name;
          $data['id']=$id;
          $data['bn_name']=$this->input->post('bn_name');
          if($this->Advocate->checkifexist_update_file_colours_by_name($name,$id))
          {
            $sdata['exception'] = "This name '".$name."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("file_colours/edit/".$id);
          }
            if ($this->Advocate->edit_file_colours($data, $id)) {
                $sdata['message'] = $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                redirect("file_colours/index");
            } else {
                $sdata['exception'] = "Information could not updated";
                $this->session->set_userdata($sdata);
                redirect("file_colours/edit/".$id);
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('file_colours');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row'] = $this->Advocate->read_file_colours($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('file_colours/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Advocate->delete_file_colours($id);
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("file_colours/index");
    }

}
