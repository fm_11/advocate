<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Signature extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('signature');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['signatures'] = $this->db->query("SELECT * FROM tbl_signature ORDER BY `access_code`")->result_array();
        $data['maincontent'] = $this->load->view('signature/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function data_save()
    {
        if ($_POST) {
            //echo phpinfo();
            // echo '<pre>';
            // print_r($_POST);
            // die;


            $loop_time = $this->input->post("loop_time");
            $i = 1;
            while ($i <= $loop_time) {
                $id = $this->input->post("id_" . $i);
                $file = $_FILES["file_" . $i]['name'];
                if ($file != '') {
                    $old_photo_info = $this->db->where('id', $id)->get('tbl_signature')->result_array();
                    $old_photo = $old_photo_info[0]['location'];
                    $old_photo = "";
                    if (!empty($old_photo)) {
                        $filedel = PUBPATH . MEDIA_FOLDER . '/signature/' . $old_photo;
                        unlink($filedel);
                    }

                    if (isset($_FILES["file_" . $i]) && $_FILES["file_" . $i]['name'] != '') {
                        $upload_file_name = $this->my_file_upload('file_' . $i, 'signature');
                        //echo $upload_file_name;
                        //die;
                    }

                    $data = array(
                        'id' => $id,
                        'location' => $upload_file_name,
                        'is_use' => $this->input->post("is_use_" . $i),
                        'level' => $this->input->post("level_" . $i),
                        'is_use_class_teacher' => $this->input->post("is_use_class_teacher_" . $i)
                    );
                    // echo '<pre>';
                    // print_r($data);
                    // die;
                    $this->db->where('id', $id);
                    $this->db->update('tbl_signature', $data);
                } else {
                    $data = array(
                      'id' => $id,
                      'is_use' => $this->input->post("is_use_" . $i),
                      'is_use_class_teacher' => $this->input->post("is_use_class_teacher_" . $i)
                  );
                    $this->db->where('id', $id);
                    $this->db->update('tbl_signature', $data);
                }
                $i++;
            }
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("signature/index");
        }
    }

    public function my_file_upload($filename, $type)
    {
        //echo $filename . '/' . $type;
        $new_file_name = time().'_'. $type . '_' .$_FILES[$filename]['name'];
        //echo $new_file_name;
        //  print_r($filename);
        //  die;
        $this->load->library('upload');

        $config = array(
                'upload_path' =>  MEDIA_FOLDER . '/signature/',
                'allowed_types' => "gif|jpg|png|jpeg|JPEG|JPG|PNG|GIF|pdf|csv|doc|docx",
                'max_size' => "5000",
                'max_height' => "5000",
                'max_width' => "5000",
                'file_name' => $new_file_name
            );

        // echo '<pre>';
        // print_r($config);
        // die;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
}
