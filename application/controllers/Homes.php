<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Home','Teacher_info','Admin_login','Timekeeping','Student'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
    }


	function counter(){
		$data = array();
		$data['online'] = 2;
		$data['day_value'] = 5;
		$data['yesterday_value'] = 3;
		$data['week_value'] = 150;
		$data['month_value'] = 500;
		$data['year_value'] = 1002;
		$data['all_value'] = 1002;
		$data['record_value'] = 50;
		$data['record_date'] = date('Y-m-d');
		return $data;
	}




    public function index() {
        $data = array();
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['title'] =  $data['eiin_number'][0]['school_name'];
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['head_message'] = $this->db->query("SELECT * FROM tbl_headmasters_message")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
		$data['founder_info'] = $this->db->query("SELECT * FROM tbl_founder_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['slide_images'] = $this->db->query("SELECT * FROM tbl_slide_images WHERE is_view = '1' ORDER BY id DESC")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 1;
        $data['slide'] = $this->load->view('homes/sources/slide', $data, true);
		$data['student_birthday_info'] = $this->db->query("SELECT s.`name`,s.`roll_no`,c.`name` AS class_name FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON c.`id` = s.`class_id`
WHERE MONTH(s.`date_of_birth`) = MONTH(CURDATE()) AND DAY(s.`date_of_birth`) = DAY(CURDATE())")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/main_content', $data, true);
		$data['counter'] = $this->counter();
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getTeacherInfo(){
        $data = array();
        $data['title'] = 'Teacher Information';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $cond['status'] = 1;
        $this->load->library('pagination');
        $config['base_url'] = site_url('homes/getTeacherInfo/');
        $config['per_page'] = 25;
        $config['total_rows'] = count($this->Teacher_info->get_all_teachers(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['teachers'] = $this->Teacher_info->get_all_teachers(25, (int) $this->uri->segment(3), $cond);
        $data['counter'] = (int) $this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/teacher_infos', $data, true);
        $data['counter'] = $this->counter();
        $data['is_open_slide'] = 0;
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getTeacherInfoByTeacherID($id){
        $data = array();
        $data['title'] = 'Teacher Profile';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['teacher_info'] = $this->Teacher_info->get_teacher_photo_info_by_teacher_id($id);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/teacher_profile', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);

    }

    function getAllNoticeInfo(){
        $data = array();
        $data['title'] = 'Notice';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('homes/getAllNoticeInfo/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Admin_login->get_all_notice(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['notices'] = $this->Admin_login->get_all_notice(20, (int) $this->uri->segment(3), $cond);
        $data['counter'] = (int) $this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/notice_list', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getHistoryInfo(){
        $data = array();
        $data['title'] = 'History';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['history'] = $this->db->query("SELECT * FROM tbl_history_info")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/history', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

	function getPhotoGallery($event_id = null){
        $data = array();
        $data['title'] = 'Photo Gallery';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 30")->result_array();
        $data['photo_event'] = $this->db->query("SELECT * FROM tbl_photo_event")->result_array();
		if($event_id != ''){
			$where = " WHERE event_id = '$event_id'";
		}
		else{
			$where = "";
		}
		$data['photos'] = $this->db->query("SELECT * FROM tbl_gallery_image $where ORDER BY id DESC LIMIT 100")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/photo_gallery', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


	function getMcMemberInfo(){
        $data = array();
        $data['title'] = 'Managing Committee';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('homes/getMcMemberInfo/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Home->get_all_MC_member(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['mc_members'] = $this->Home->get_all_MC_member(15, (int) $this->uri->segment(3), $cond);
        $data['counter'] = (int) $this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/mc_members_infos', $data, true);
        $data['counter'] = $this->counter();
        $data['is_open_slide'] = 0;
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

	function getStaffInfo(){
		$data = array();
        $data['title'] = 'Staff Information';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('homes/getStaffInfo/');
        $config['per_page'] = 25;
        $config['total_rows'] = count($this->Home->get_all_staff(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['staff'] = $this->Home->get_all_staff(25, (int) $this->uri->segment(3), $cond);
        $data['counter'] = (int) $this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/staff_infos', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
	}

	function getTeacherTimekeepingInfo(){
        $data = array();
        $data['title'] = 'Teacher Timekeeping';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
		$cond = array();
		if($_POST){
			$date = $this->input->post("date");
            $login_status = $this->input->post("login_status");
            if($date != ''){
				$sdata['date'] = $date;
                $this->session->set_userdata($sdata);
				$cond['date'] = $date;
			}else{
                $sdata['date'] = "";
                $this->session->set_userdata($sdata);
                $cond['date'] = "";
            }
            if($login_status != ''){
                $sdata['login_status'] = $login_status;
                $this->session->set_userdata($sdata);
                $cond['login_status'] = $login_status;
            }else{
                $sdata['login_status'] = "";
                $this->session->set_userdata($sdata);
                $cond['login_status'] = "";
            }
		} else {
            $cond['date'] = date('Y-m-d');
            $cond['login_status'] = "";
            $sdata['date'] = $cond['date'];
            $sdata['login_status'] = $cond['login_status'];
            $this->session->set_userdata($sdata);
        }

           $date = $this->session->userdata('date');
           $login_status = $this->session->userdata('login_status');
           if($date != ''){
               $cond['date'] = $date;
           }
           if($login_status != ''){
               $cond['login_status'] = $login_status;
           }

        $this->load->library('pagination');
        $config['base_url'] = site_url('homes/getTeacherTimekeepingInfo/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Timekeeping->get_all_teacher_timekeeping_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['teacher_timekeeping_info'] = $this->Timekeeping->get_all_teacher_timekeeping_list(15, (int) $this->uri->segment(3), $cond);
        $data['counter'] = (int) $this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/teacher_timekeeping_info', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


	function getRulesAndRegulation(){
		$data = array();
        $data['title'] = 'Rules and Regulation';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
		$data['rules'] = $this->db->query("SELECT * FROM tbl_rules_and_regulation")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/rules', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
	}

    function getHeadteacherMessage(){
        $data = array();
        $data['title'] = 'Head Teacher Message';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['rules'] = $this->db->query("SELECT * FROM tbl_rules_and_regulation")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['head_message'] = $this->db->query("SELECT * FROM tbl_headmasters_message")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/head_message', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getPresidentMessage(){
        $data = array();
        $data['title'] = 'President Message';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['rules'] = $this->db->query("SELECT * FROM tbl_rules_and_regulation")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/president_message', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

	function getDashboard(){
        $data = array();
        $data['title'] = 'Dashboard';
        $date = date('Y-m-d');
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['today_present_teacher'] = count($this->db->query("SELECT * FROM `tbl_teacher_staff_logins` WHERE `date` = '$date'")->result_array());
        $data['today_leave_teacher'] = count($this->db->query("SELECT * FROM `tbl_teacher_staff_leave_days` WHERE `date` = '$date'")->result_array());
        $data['total_teacher'] = count($this->db->query("SELECT * FROM tbl_teacher WHERE status = '1'")->result_array());
		$data['today_absent_teacher'] = $data['total_teacher'] - $data['today_present_teacher'] + $data['today_leave_teacher'];
		$data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['total_student'] = count($this->db->query("SELECT * FROM `tbl_student`")->result_array());
        $data['student_present'] = count($this->db->query("SELECT * FROM `tbl_student_logins` WHERE `date` = '$date' AND login_status ='P'")->result_array());
        $data['student_absent'] = count($this->db->query("SELECT * FROM `tbl_student_logins` WHERE `date` = '$date' AND login_status ='A'")->result_array());
        $data['student_leave'] = count($this->db->query("SELECT * FROM `tbl_student_logins` WHERE `date` = '$date' AND login_status ='L'")->result_array());
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
		$data['students'] = $this->db->query("SELECT c.`name` AS class_name,COUNT(s.`id`) AS total_student FROM `tbl_class` AS c
LEFT JOIN `tbl_student` AS s ON s.`class_id` = c.`id`
GROUP BY c.`id`")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/dashboard', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
	}

    function getAllUploadedResultInfo(){
        $data = array();
        $data['title'] = 'Result';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $cond['status'] = 1;
        $this->load->library('pagination');
        $config['base_url'] = site_url('homes/getAllUploadedResultInfo/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student->get_all_uploaded_result(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['results'] = $this->Student->get_all_uploaded_result(15, (int) $this->uri->segment(3), $cond);
        $data['counter'] = (int) $this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/uploaded_result_list', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


    function getAboutInfo(){
        $data = array();
        $data['title'] = 'About';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['about'] = $this->db->query("SELECT * FROM tbl_about_info")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('homes/sources/about', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getContactInfo(){
        if($_POST){
            $data = array();
            $data['name'] = $this->input->post('name', true);
            $data['message'] = $this->input->post('message', true);
            $data['email'] = $this->input->post('email', true);
            $data['date'] = date('Y-m-d');
            $this->db->insert('tbl_contact', $data);
            $sdata['message'] = "You are Successfully Message Send.";
            $this->session->set_userdata($sdata);
            redirect('homes/getContactInfo');
        }else{
            $data = array();
            $data['title'] = 'Contact';
            $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
            $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
            $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
            $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
            $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
            $data['contact'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
            $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
            $data['slide'] = $this->load->view('homes/sources/slide', '', true);
            $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
            $data['main_content'] = $this->load->view('homes/sources/contact', $data, true);
            $data['counter'] = $this->counter();
            $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
            $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
            $data['is_open_slide'] = 0;
            $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
            $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
            $this->load->view('homes/index', $data);
        }
    }



}
