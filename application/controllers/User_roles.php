<?php

class User_roles extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('User_role', 'User_role_wise_privilege','Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('user') . ' ' . $this->lang->line('role');
        $data['is_show_button'] = "add";
        $data['user_roles'] = $this->User_role->get_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            if ($this->User_role->add($this->input->post())) {
                $sdata['message'] = "Information Added";
                $this->session->set_userdata($sdata);
                redirect("user_roles/index");
            } else {
                $sdata['message'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("user_roles/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = 'User role';
        $data['action'] = 'add';
		$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/save', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
            if ($this->User_role->edit($this->input->post(), $id)) {
                $sdata['message'] = "Information Successfully updated.";
                $this->session->set_userdata($sdata);
                redirect("user_roles/index");
            } else {
                $sdata['message'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("user_roles/index");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = 'User role';
        $data['action'] = 'edit/' . $id;
		$data['is_show_button'] = "index";
        $data['row'] = $this->User_role->read($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/save', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->User_role->delete($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("user_roles/index");
    }
}
