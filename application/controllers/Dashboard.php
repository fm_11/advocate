<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Message','Advocate','Student_fee','Admin_login','Config_general' ,'common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));
        $this->lang->load('message', 'english');
        $this->checkpermission();
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
        date_default_timezone_set("Asia/Dhaka");
    }

    public function index()
    {

        //  echo CI_VERSION; die;
        //  echo  $this->language_number_convert->en2bn(55); die;
        $data['title'] = $this->lang->line('dashboard_title');
        $data['heading_msg'] = $this->lang->line('dashboard_title');
//         $data['students'] = $this->db->query("SELECT c.`name` AS class_name,COUNT(s.`id`) AS total_student FROM `tbl_class` AS c
// LEFT JOIN `tbl_student` AS s ON s.`class_id` = c.`id` where status = 1
// GROUP BY c.`id`")->result_array();


        $data['students_chart_value'] =""; // implode(', ', array_column($data['students'], 'total_student'));

        $data['total_clients'] = $this->Advocate->get_total_client_count();
        $data['total_running_case'] =$this->Advocate->get_total_running_case_count();
        $data['total_colsed_case'] =$this->Advocate->get_total_closed_case_count();
        $data['total_important_case'] = $this->Advocate->get_total_important_case_count();
        $data['total_missed_attendance']=count($this->Advocate->get_all_missed_attendance_history_list());


        $color = array();
        $color[0] = "#5d78ff";
        $color[1] = "#ffb61b";
        $color[2] = "#2cd9c5";
        $color[3] = "#44519e";
        $color[4] = "#80B2FF";
        $color[5] = "#A34719";
        $color[6] = "#7519FF";
        $color[7] = "#666699";
        $color[8] = "#009900";
        $color[9] = "#00b300";
        $color[10] = "#ff8533";
        $color[11] = "#000099";
        $color[12] = "#00b3b3";
        $color[13] = "#993366";
        $color[14] = "#ff5c33";
        $color[15] = "#00cc99";
        $data['students_chart_level'] = "";
        $data['students_chart_color'] = "";
        $i = 0;
        // foreach ($data['students'] as $row) {
        //     $data['students_chart_level'] .= $row['class_name'] . ',';
        //     $data['students_chart_color'] .= $color[$i] . ',';
        //     $i++;
        // }

//         $data['teacher_birthday_info'] = $this->db->query("SELECT t.`name`,t.`teacher_index_no`,t.`date_of_birth` FROM `tbl_teacher` AS t
// WHERE MONTH(t.`date_of_birth`) = MONTH(CURDATE()) AND DAY(t.`date_of_birth`) = DAY(CURDATE())")->result_array();
//
//         $data['total_teacher'] = $this->db->query("SELECT id FROM tbl_teacher WHERE status='1'")->num_rows();
//         $data['total_student'] = $this->db->query("SELECT id FROM tbl_student where status = 1")->num_rows();
        $date = date('Y-m-d');


        $date = new DateTime('now');
        $date->modify('last day of this month');
        $first_date = date('Y-m-01');
        $last_date = $date->format('Y-m-d');



        //male female data
//         $gender_wise_st = $this->db->query('SELECT s.`gender`,COUNT(s.`id`) AS total_student FROM `tbl_student` AS s WHERE s.`status` = 1
// GROUP BY s.`gender`')->result_array();
        $data['male_student'] = 0;
        $data['female_student'] = 0;
        $data['other_gender_student'] = 0;

        // foreach ($gender_wise_st as $row_g) {
        //     if ($row_g['gender'] == 'M') {
        //         $data['male_student'] =$row_g['total_student'];
        //     } elseif ($row_g['gender'] == 'F') {
        //         $data['female_student'] =$row_g['total_student'];
        //     } else {
        //         $data['other_gender_student'] = $row_g['total_student'];
        //     }
        // }

        //dashboard teacher data
		//$data['dashboard_teacher_info'] = $this->db->query('SELECT t.`name`,t.`teacher_code`,
							// p.`name` AS designation,t.`photo_location` FROM `tbl_teacher` AS t
							// LEFT JOIN `tbl_teacher_post` AS p ON p.`id` = t.`post`
							// WHERE t.`is_dashboard_show` = 1')->row();


        //income vs Expense data
        $year = date('Y');

        //echo $data['i_vs_e_income_data'] . '/' . $data['i_vs_e_exense_data'];
        //  die;

		$data['dashboard_color_info'] = $this->db->query("SELECT * FROM tbl_dashboard_color_settings")->row();

        $data['general_config'] = $this->Config_general->get_general_configurations('dashboard');
        // echo '<pre>';
        // print_r($data['general_config']);
        // die;
        $sms_rate = 0.5;
        $sms_config = $this->db->query("SELECT sms_rate FROM `tbl_message_config`")->result_array();
        if(!empty($sms_config)){
          if(isset($sms_config[0]['sms_rate']) && $sms_config[0]['sms_rate'] >= 0){
            $sms_rate = $sms_config[0]['sms_rate'];
          }
        }
        $data['sms_rate'] = $sms_rate;
		$data['sms_quantity'] = $this->Message->getAvailableSmsBalance();
        $data['sms_balance_tk'] = $data['sms_quantity'] * $data['sms_rate'];



        $cond = array();
        $cond['from_date'] = date("Y-m-d");
        $cond['to_date'] = date("Y-m-d");
        $data['today_attendace_list'] = $this->Advocate->get_all_attendance_list_by_date_range(10, 0, $cond);

        $tomorrow_date = date('Y-m-d');
        $tomorrow_date = date('Y-m-d', strtotime($tomorrow_date . ' +1 day'));
        $cond = array();
        $cond['from_date'] = $tomorrow_date;
        $cond['to_date'] = $tomorrow_date;
        $data['tomorrow_attendace_list'] = $this->Advocate->get_all_attendance_list_by_date_range(10, 0, $cond);

      //  $cond = array();
      //  $data['last_invoices'] = $this->Student_fee->get_all_collected_fee(10, 0, $cond);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function getLastNDays($days, $format = 'd/m')
    {
        $m = date("m");
        $de= date("d");
        $y= date("Y");
        $dateArray = array();
        for ($i=0; $i<=$days-1; $i++) {
            $dateArray[] = '"' . date($format, mktime(0, 0, 0, $m, ($de-$i), $y)) . '"';
        }
        return array_reverse($dateArray);
    }


    public function get_education_office_notice_list()
    {
        $contact_info = $this->db->query("SELECT eiin_number FROM tbl_contact_info")->result_array();
        $eiin_number = $contact_info[0]['eiin_number'];
        if ($eiin_number == '') {
            $sdata['exception'] = "Please update institute EIIN number.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/contact_info");
        }

        $row_per_page = 15;

        $postdata = http_build_query(
            array(
                'security_pin' => 12345,
                'eiin_number' => $eiin_number,
                'segment' => (int)$this->uri->segment(3),
                'row_per_page' => $row_per_page
            )
        );

        $postdataCountRow = http_build_query(
            array(
                'security_pin' => 12345,
                'eiin_number' => $eiin_number,
                'segment' => 0,
                'row_per_page' => 0
            )
        );
        $eo_domain = "http://www.deo.jamalpurdistrict.org/";
        $data = array();
        $data['title'] = 'Education Office Notice';
        $data['heading_msg'] = "Education Office Notice";
        $this->load->library('pagination');
        $config['base_url'] = site_url('dashboard/get_education_office_notice_list/');
        $config['per_page'] = $row_per_page;
        $config['total_rows'] = count($this->getSchoolData($postdataCountRow, $eo_domain . 'ServiceBridge/getSendNoticeForSchool'));
        //echo $config['total_rows']; die;
        $this->pagination->initialize($config);
        $data['notices'] = $this->getSchoolData($postdata, $eo_domain . 'ServiceBridge/getSendNoticeForSchool');
        $data['counter'] = (int)$this->uri->segment(3);
        $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['eo_domain'] = $eo_domain;
        $data['maincontent'] = $this->load->view('dashboard/education_office_notice_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function getSchoolData($postdata, $api)
    {
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents("$api", false, $context);
        return json_decode($result);
    }


    public function notice_list()
    {
        $data = array();
        $data['title'] = 'Notice';
        $data['heading_msg'] = "Notice";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('dashboard/notice_list/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_all_notice(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['notices'] = $this->Admin_login->get_all_notice(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/notice_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function document_list()
    {
        $data = array();
        $data['title'] = 'School Document';
        $data['heading_msg'] = "School Document";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('dashboard/document_list/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Admin_login->get_all_document(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['documents'] = $this->Admin_login->get_all_document(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/document_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function document_add()
    {
        if ($_POST) {
            $file_name = $_FILES['txtFile']['name'];
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/document/';
            $config['allowed_types'] = 'pdf|doc|png|JPEG|jpeg|jpg|docx|xls';
            $config['max_size'] = '60000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "Document_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/document/" . $new_file_name)) {
                        $data = array();
                        $data['title'] = $this->input->post('txtTitle', true);
                        $data['date'] = $this->input->post('txtDate', true);
                        $data['url'] = $new_file_name;
                        $this->Admin_login->add_document($data);
                        $sdata['message'] = "You are Successfully Document Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/document_add");
                    } else {
                        $sdata['exception'] = "Sorry Document Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/document_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Document Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("dashboard/document_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Document';
            $data['heading_msg'] = "Add New Document";
            $data['action'] = '';
            $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/document_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function document_delete($id)
    {
        $document_info = $this->Admin_login->get_document_info_by_id($id);

        $file = $document_info[0]['url'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/document/' . $file;
            if (unlink($filedel)) {
                $this->Admin_login->delete_document_info_by_id($id);
            } else {
                $this->Admin_login->delete_document_info_by_id($id);
            }
        } else {
            $this->Admin_login->delete_document_info_by_id($id);
        }

        $sdata['message'] = "Document Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/document_list");
    }


    public function notice_delete($id)
    {
        $notice_info = $this->Admin_login->get_notice_info_by_id($id);

        $file = $notice_info[0]['url'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/notice/' . $file;
            if (unlink($filedel)) {
                $this->Admin_login->delete_notice_info_by_id($id);
            } else {
                $this->Admin_login->delete_notice_info_by_id($id);
            }
        } else {
            $this->Admin_login->delete_notice_info_by_id($id);
        }

        $sdata['message'] = "Notice Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/notice_list");
    }

    public function get_contact_message()
    {
        $data = array();
        $data['title'] = 'Contact Message';
        $data['heading_msg'] = "Contact Message";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('dashboard/get_contact_message/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Admin_login->get_contact_message(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['messages'] = $this->Admin_login->get_contact_message(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/message_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function message_delete($id)
    {
        $this->db->delete('tbl_contact', array('id' => $id));
        $sdata['message'] = "Message Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/get_contact_message");
    }


    public function notice_add()
    {
        if ($_POST) {
            $file_name = $_FILES['txtFile']['name'];
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/notice/';
            $config['allowed_types'] = 'pdf|doc|png|JPEG|jpeg|jpg|docx|xls';
            $config['max_size'] = '60000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "Notice_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/notice/" . $new_file_name)) {
                        $data = array();
                        $data['title'] = $this->input->post('txtTitle', true);
                        $data['date'] = $this->input->post('txtDate', true);
                        $data['url'] = $new_file_name;
                        $this->Admin_login->add_notice($data);
                        $sdata['message'] = "You are Successfully Notice Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/notice_add");
                    } else {
                        $sdata['exception'] = "Sorry Notice Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/notice_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Notice Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("dashboard/notice_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Notice';
            $data['heading_msg'] = "Add New Notice";
            $data['action'] = '';
            $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/notice_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function managing_committee_list()
    {
        $data['title'] = 'Managing Committee';
        $data['heading_msg'] = 'Managing Committee';
        $data['managing_committee'] = $this->db->query("SELECT * FROM tbl_managing_committee ORDER BY id ASC")->result_array();
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/managing_committee_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function all_headmasters_list()
    {
        $data['title'] = 'All Headmasters';
        $data['heading_msg'] = 'All Headmasters';
        $data['all_headmasters'] = $this->db->query("SELECT * FROM tbl_all_headmasters ORDER BY id ASC")->result_array();
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/all_headmasters_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function success_student_list()
    {
        $data['title'] = 'Success Students';
        $data['heading_msg'] = 'Success Students';
        $data['success_student'] = $this->db->query("SELECT * FROM tbl_success_students ORDER BY id ASC")->result_array();
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/success_student_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function success_student_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtName', true);
            $data['year_of_passing'] = $this->input->post('txtYearPassing', true);
            $data['edu_qua'] = $this->input->post('txtEduQua', true);
            $data['org'] = $this->input->post('txtOrg', true);
            $data['position'] = $this->input->post('txtPosition', true);
            $data['mobile'] = $this->input->post('txtMobile', true);
            $data['email'] = $this->input->post('txtEmail', true);
            $data['address'] = $this->input->post('txtAddress', true);
            $this->db->insert('tbl_success_students', $data);
            $sdata['message'] = "You are Successfully Student Added ! ";
            $this->session->set_userdata($sdata);
            redirect("dashboard/success_student_add");
        } else {
            $data = array();
            $data['title'] = 'Success Students';
            $data['heading_msg'] = "Success Students";
            $data['action'] = '';
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/success_student_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function all_headmaster_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtName', true);
            $data['edu_qua'] = $this->input->post('txtEduQua', true);
            $data['from_date'] = $this->input->post('txtFromDate', true);
            $data['to_date'] = $this->input->post('txtToDate', true);
            $this->db->insert('tbl_all_headmasters', $data);
            $sdata['message'] = "You are Successfully Info. Added ! ";
            $this->session->set_userdata($sdata);
            redirect("dashboard/all_headmaster_add");
        } else {
            $data = array();
            $data['title'] = 'All Headmasters';
            $data['heading_msg'] = "All Headmasters";
            $data['action'] = '';
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/all_headmasters_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function all_headmasters_edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('txtName', true);
            $data['edu_qua'] = $this->input->post('txtEduQua', true);
            $data['from_date'] = $this->input->post('txtFromDate', true);
            $data['to_date'] = $this->input->post('txtToDate', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_all_headmasters', $data);
            $sdata['message'] = "You are Successfully Info. Updated ! ";
            $this->session->set_userdata($sdata);
            redirect("dashboard/all_headmasters_list");
        } else {
            $data = array();
            $data['title'] = 'All Headmasters';
            $data['heading_msg'] = "All Headmasters";
            $data['action'] = 'edit';
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['headmasters'] = $this->db->query("SELECT * FROM tbl_all_headmasters WHERE id = '$id'")->result_array();
            $data['maincontent'] = $this->load->view('dashboard/all_headmasters_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function all_headmasters_delete($id)
    {
        $this->db->delete('tbl_all_headmasters', array('id' => $id));
        $sdata['message'] = "Headmasters Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/all_headmasters_list");
    }

    public function success_student_delete($id)
    {
        $this->db->delete('tbl_success_students', array('id' => $id));
        $sdata['message'] = "Success Student Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/success_student_list");
    }

    public function administration_list()
    {
        $data['title'] = 'Administration Member';
        $data['heading_msg'] = 'Administration Member';
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member ORDER BY id ASC")->result_array();
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/administration_member_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function administration_member_delete($id)
    {
        $member_photo_info = $this->db->where('id', $id)->get('tbl_administration_member')->result_array();
        $file = $member_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER. '/headteacher/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_administration_member', array('id' => $id));
            } else {
                $this->db->delete('tbl_administration_member', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_administration_member', array('id' => $id));
        }

        $sdata['message'] = "Member Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/administration_list");
    }


    public function administration_member_edit($id = null)
    {
        if ($_POST) {
            $file = $_FILES["txtPhoto"]['name'];
            if ($file != '') {
                $m_photo_info = $this->db->where('id', $this->input->post('id', true))->get('tbl_administration_member')->result_array();
                $old_file = $m_photo_info[0]['photo_location'];
                if (!empty($old_file)) {
                    $filedel = PUBPATH . MEDIA_FOLDER . '/headteacher/' . $old_file;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "AM_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
                            $data['name'] = $this->input->post('txtName', true);
                            $data['post'] = $this->input->post('txtPost', true);
                            $data['photo_location'] = $new_photo_name;
                            $this->db->where('id', $data['id']);
                            $this->db->update('tbl_administration_member', $data);
                            $sdata['message'] = "You are Successfully Member Information Updated ! ";
                            $this->session->set_userdata($sdata);
                            redirect("dashboard/administration_list");
                        } else {
                            $sdata['exception'] = "Sorry Member Doesn't Updated !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("dashboard/administration_list");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Photo Does't Upload !";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/administration_list");
                    }
                }
            } else {
                $data = array();
                $data['id'] = $this->input->post('id', true);
                $data['name'] = $this->input->post('txtName', true);
                $data['post'] = $this->input->post('txtPost', true);
                $this->db->where('id', $data['id']);
                $this->db->update('tbl_administration_member', $data);
                $sdata['message'] = "You are Successfully Member Information Updated ! ";
                $this->session->set_userdata($sdata);
                redirect("dashboard/administration_list");
            }
        } else {
            $data = array();
            $data['title'] = 'Update Administration Member Information';
            $data['heading_msg'] = "Update Administration Member Information";
            $data['action'] = 'edit';
            $data['members'] = $this->db->query("SELECT * FROM tbl_administration_member WHERE id = '$id'")->result_array();
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/administration_member_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function administration_member_add()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
            $config['allowed_types'] = 'png|JPEG|jpeg|jpg';
            $config['max_height'] = '400';
            $config['max_width'] = '400';
            $config['max_size'] = '2000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "AM_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                        $data = array();
                        $data['name'] = $this->input->post('txtName', true);
                        $data['post'] = $this->input->post('txtPost', true);
                        $data['photo_location'] = $new_photo_name;
                        $this->db->insert('tbl_administration_member', $data);
                        $sdata['message'] = "You are Successfully Member Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/administration_member_add");
                    } else {
                        $sdata['exception'] = "Sorry Member Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/administration_member_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Member Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("dashboard/administration_member_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Administration Member';
            $data['heading_msg'] = "Add New Administration Member";
            $data['action'] = '';
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/administration_member_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function managing_committee_add()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/managing_committee/';
            $config['allowed_types'] = 'png|JPEG|jpeg|jpg';
            $config['max_height'] = '400';
            $config['max_width'] = '400';
            $config['max_size'] = '2000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "MC_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/managing_committee/" . $new_photo_name)) {
                        $data = array();
                        $data['name'] = $this->input->post('txtName', true);
                        $data['post'] = $this->input->post('txtPost', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['photo_location'] = $new_photo_name;
                        $this->db->insert('tbl_managing_committee', $data);
                        $sdata['message'] = "You are Successfully Member Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/managing_committee_add");
                    } else {
                        $sdata['exception'] = "Sorry Member Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/managing_committee_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Member Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("dashboard/managing_committee_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Managing Committee';
            $data['heading_msg'] = "Add New Member";
            $data['action'] = '';
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/managing_committee_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function managing_committee_edit($id = null)
    {
        if ($_POST) {
            $file = $_FILES["txtPhoto"]['name'];
            if ($file != '') {
                $mc_photo_info = $this->db->where('id', $this->input->post('id', true))->get('tbl_managing_committee')->result_array();
                $old_file = $mc_photo_info[0]['photo_location'];
                if (!empty($old_file)) {
                    $filedel = PUBPATH . MEDIA_FOLDER . '/managing_committee/' . $old_file;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/managing_committee/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "MC_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/managing_committee/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
                            $data['name'] = $this->input->post('txtName', true);
                            $data['post'] = $this->input->post('txtPost', true);
                            $data['mobile'] = $this->input->post('txtMobile', true);
                            $data['photo_location'] = $new_photo_name;
                            $this->db->where('id', $data['id']);
                            $this->db->update('tbl_managing_committee', $data);
                            $sdata['message'] = "You are Successfully Managing Committee Information Updated ! ";
                            $this->session->set_userdata($sdata);
                            redirect("dashboard/managing_committee_list");
                        } else {
                            $sdata['exception'] = "Sorry Managing Committee Doesn't Updated !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("dashboard/managing_committee_list");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Photo Does't Upload !";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/managing_committee_list");
                    }
                }
            } else {
                $data = array();
                $data['id'] = $this->input->post('id', true);
                $data['name'] = $this->input->post('txtName', true);
                $data['post'] = $this->input->post('txtPost', true);
                $data['mobile'] = $this->input->post('txtMobile', true);
                $this->db->where('id', $data['id']);
                $this->db->update('tbl_managing_committee', $data);
                $sdata['message'] = "You are Successfully Managing Committee Information Updated ! ";
                $this->session->set_userdata($sdata);
                redirect("dashboard/managing_committee_list");
            }
        } else {
            $data = array();
            $data['title'] = 'Update Managing Committee Information';
            $data['heading_msg'] = "Update Managing Committee Information";
            $data['action'] = 'edit';
            $data['members'] = $this->db->query("SELECT * FROM tbl_managing_committee WHERE id = '$id'")->result_array();
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/managing_committee_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function managing_committee_delete($id)
    {
        $member_photo_info = $this->db->where('id', $id)->get('tbl_managing_committee')->result_array();
        $file = $member_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/managing_committee/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_managing_committee', array('id' => $id));
            } else {
                $this->db->delete('tbl_managing_committee', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_managing_committee', array('id' => $id));
        }

        $sdata['message'] = "Member Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/managing_committee_list");
    }

    public function get_slide_images()
    {
        $data['title'] = 'Slide Images';
        $data['heading_msg'] = 'Slide Images';
        $data['images'] = $this->db->query("SELECT * FROM tbl_slide_images ORDER BY id DESC")->result_array();
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/slide_images_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function slide_image_delete($id)
    {
        $image_info = $this->db->where('id', $id)->get('tbl_slide_images')->result_array();
        $file = $image_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/images/slide/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_slide_images', array('id' => $id));
            } else {
                $this->db->delete('tbl_slide_images', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_slide_images', array('id' => $id));
        }

        $sdata['message'] = "Slide Image Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/get_slide_images");
    }



    public function slide_image_add()
    {
        if ($_POST) {
            $total_slide_image = count($this->db->query("SELECT * FROM tbl_slide_images")->result_array());
            if ($total_slide_image >= 10) {
                $sdata['exception'] = "Sorry Slide Image Doesn't Upload.Because already 10 slide image Exist.Please Delete one and Upload !";
                $this->session->set_userdata($sdata);
                redirect("dashboard/slide_image_add");
            } else {
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/images/slide/';
                $config['allowed_types'] = 'png|JPEG|jpeg|jpg';
                $config['max_height'] = '306';
                $config['max_width'] = '940';
                $config['max_size'] = '8000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "SL_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/images/slide/" . $new_photo_name)) {
                            $data = array();
                            $data['title'] = $this->input->post('txtTitle', true);
                            $data['date'] = $this->input->post('txtDate', true);
                            $data['is_view'] = 0;
                            $data['photo_location'] = $new_photo_name;
                            $this->db->insert('tbl_slide_images', $data);
                            $sdata['message'] = "You are Successfully Slide Image Added ! ";
                            $this->session->set_userdata($sdata);
                            redirect("dashboard/slide_image_add");
                        } else {
                            $sdata['exception'] = "Sorry Slide Image Doesn't Upload.Please Check Image Size or Height Width !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("dashboard/slide_image_add");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Slide Image Doesn't Upload.Please Check Image Size or Height Width !";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/slide_image_add");
                    }
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Slide Image';
            $data['heading_msg'] = "Add Slide Image";
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/slide_image_add_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function get_digital_display()
    {
        $data = array();
        $data['title'] = 'Digital Contents';
        $data['heading_msg'] = "Digital Contents";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('dashboard/get_digital_display/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Admin_login->getAllDigitalContent(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['digital_content_info'] = $this->Admin_login->getAllDigitalContent(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('dashboard/digital_content_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function digital_content_add()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/managing_committee/';
            $config['allowed_types'] = 'pdf|doc|ppt|pptx|docx';
            $config['max_size'] = '50000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
//            echo '<pre>';
//            print_r($_FILES);
//            die;
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "DC_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/digital_content/" . $new_file_name)) {
                        $data = array();
                        $data['title'] = $this->input->post('txtTitle', true);
                        $data['date'] = $this->input->post('txtDate', true);
                        $data['location'] = $new_file_name;
                        $this->db->insert('tbl_digital_contents', $data);
                        $sdata['message'] = "You are Successfully Digital Content Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/digital_content_add");
                    } else {
                        $sdata['exception'] = "Sorry Digital Content Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("dashboard/digital_content_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Digital Content Doesn't Added !";
                    $this->session->set_userdata($sdata);
                    redirect("dashboard/digital_content_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Digital Content';
            $data['heading_msg'] = "Add Digital Content";
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('dashboard/digital_content_add_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function digital_content_delete($id)
    {
        $file_info = $this->db->where('id', $id)->get('tbl_digital_contents')->result_array();
        $file = $file_info[0]['location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/digital_content/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_digital_contents', array('id' => $id));
            } else {
                $this->db->delete('tbl_digital_contents', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_digital_contents', array('id' => $id));
        }

        $sdata['message'] = "Digital Content Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("dashboard/get_digital_display");
    }

    public function checkpermission()
    {
        $user_info = $this->session->userdata('user_info');
        //echo '<pre>'; print_r($user_info); die();
        if (!empty($user_info[0])) {
        } else {
            $sdata['exception'] = "You are not logged in!";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }
}
