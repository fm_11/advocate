<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Global_configs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Account', 'Admin_login'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('global_configuration');
        $config_info = $this->db->query("SELECT * FROM `tbl_config`")->result_array();
        if (empty($config_info)) {
            $sdata['exception'] = "Initial configuration not found to database";
            $this->session->set_userdata($sdata);
            redirect('admin_logins/index');
        }
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['fee_module_type'] = $_POST['fee_module_type'];
            $data['accounts_module_type'] = $_POST['accounts_module_type'];
            $data['is_device_integrated_attendance_system'] = $_POST['is_device_integrated_attendance_system'];
            $data['is_pass_to_different_segment_of_result'] = $_POST['is_pass_to_different_segment_of_result'];
            $data['place_show_will_be_marksheet'] = $_POST['place_show_will_be_marksheet'];
            $data['is_calculate_cgpa_when_fail'] = $_POST['is_calculate_cgpa_when_fail'];
            $data['marksheet_design'] = $_POST['marksheet_design'];
            $data['is_process_bangla_english_separate'] = $_POST['is_process_bangla_english_separate'];
            $data['how_to_generate_position'] = $_POST['how_to_generate_position'];
            $data['multiple_atudent_allow_same_position'] = $_POST['multiple_atudent_allow_same_position'];
            $data['num_of_subject_allow_for_grace'] = $_POST['num_of_subject_allow_for_grace'];
            $data['calculable_total_mark_percentage'] = $_POST['calculable_total_mark_percentage'];
            $data['add_extra_mark_with_total_obtained'] = $_POST['add_extra_mark_with_total_obtained'];
            $data['annual_mark_calculate_method'] = $_POST['annual_mark_calculate_method'];
            $data['software_start_year'] = $_POST['software_start_year'];
            $data['software_current_year'] = $_POST['software_current_year'];
            $data['is_transport_fee_applicable'] = $_POST['is_transport_fee_applicable'];
            $data['default_language'] = $_POST['default_language'];
			$data['menu_type'] = $_POST['menu_type'];
            $data['show_due_amount_to_collection_sheet'] = $_POST['show_due_amount_to_collection_sheet'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_config', $data);
            $sdata['message'] = "Configuration has been updated successfully";
            $this->session->set_userdata($sdata);
            redirect('global_configs/index');
        }
        $data = array();
        $data['config_info'] = $config_info;
        $data['title'] = $this->lang->line('global_configuration');
        $data['heading_msg'] = $this->lang->line('global_configuration');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('global_configs/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
