<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home_page_news extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Home Page News';
        $data['heading_msg'] = "Home Page News";
        $data['is_show_button'] = "add";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('home_page_news/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Admin_login->get_home_page_news_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['news_info'] = $this->Admin_login->get_home_page_news_info(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('home_page_news/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['text_color'] = "#" . $this->input->post('txtTextColor', true);
            $data['news'] = $this->input->post('txtNews', true);
            $this->db->insert('tbl_home_page_news', $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect("home_page_news/add");
        } else {
            $data = array();
            $data['title'] = 'Home Page News Information';
            $data['heading_msg'] = "Home Page News Information";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('home_page_news/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['text_color'] = "#" . $this->input->post('txtTextColor', true);
            $data['news'] = $this->input->post('txtNews', true);
            $this->Admin_login->home_page_news_info_update($data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("home_page_news/index");
        } else {
            $data = array();
            $data['title'] = 'Update Home Page News Information';
            $data['heading_msg'] = "Update Home Page News Information";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['news_info'] = $this->Admin_login->get_home_page_news_info_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('home_page_news/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function delete($id)
    {
        $this->db->delete('tbl_home_page_news', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("home_page_news/index");
    }

}
?>
