<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sms_send_reports extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Message', 'Admin_login', 'Student'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }



    	public function index()
    	{
    		$data = array();
    		$data['title'] = $this->lang->line('sms').' '.$this->lang->line('send').' '.$this->lang->line('report');
    		$data['heading_msg'] = $this->lang->line('sms').' '.$this->lang->line('send').' '.$this->lang->line('report');
    		if($_POST){
    			$from_date =date("Y-m-d", strtotime($this->input->post('from_date'))); //$this->input->post("from_date");
    			$to_date =date("Y-m-d", strtotime($this->input->post('to_date'))); //$this->input->post("to_date");
    			$sms_from = $this->input->post("sms_from");
    			$sms_status = $this->input->post("sms_status");
    			$data['from_date'] = $from_date;
    			$data['to_date'] = $to_date;
    			$data['sms_from'] = $sms_from;
    			$data['sms_status'] = $sms_status;

    			$SchoolInfo = $this->Admin_login->fetReportHeader();
    			$data['HeaderInfo'] = $SchoolInfo;

    			$where = "";
    			if($sms_from != 'ALL'){
    				$where = " AND r.`sender_type` = '$sms_from'";
    			}
    			$status_where = "";
    			if($sms_status != 'all'){
    				$where = " AND LCASE(r.`status_message`) LIKE '%$sms_status%'";
    			}
          $query="SELECT c.`first_name` AS client_name,c.`mobile`,r.*
                  FROM `tbl_sms_response` AS r
                  LEFT JOIN `ad_clients` AS c ON r.`client_id`=c.`id`
                  LEFT JOIN `ad_case_history` AS h ON r.`case_history_id`=h.`id`
                  LEFT JOIN `ad_appointment` AS a ON r.`appointment_id`=a.`id`
    									WHERE DATE(r.`date_time`) BETWEEN '$from_date' AND '$to_date' $where $status_where;";

            $data['history_list'] = $this->db->query($query)->result_array();
    			// $data['history_list'] = $this->db->query("SELECT s.`name` AS student_name,s.`student_code`,t.`name` AS teacher_name,t.`teacher_code`,r.*
    			// 						FROM `tbl_sms_response` AS r
    			// 						LEFT JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
    			// 						LEFT JOIN `tbl_teacher` AS t ON t.`id` = r.`teacher_id`
    			// 						WHERE DATE(r.`date_time`) BETWEEN '$from_date' AND '$to_date' $where $status_where;")->result_array();

                      // print_r($query);
                      // print_r($data['history_list']);
                      // die;
    			if(isset($_POST['pdf_download'])){
    				$data['page_type'] = 'landscape';
    				$data['paper_size'] = 'A4';
    				$data['is_pdf'] = 1;
    			}
    			$data['report_header'] = $this->load->view('report_content/inside_report_header', $data, true);
    			$data['report_footer'] = $this->load->view('report_content/inside_report_footer', $data, true);
    			$data['report'] = $this->load->view('sms_send_reports/history_list', $data, true);
    		}
    		if (isset($_POST['pdf_download'])) {
    			//Dom PDF
    			$this->load->library('mydompdf');
    			$html = $this->load->view('sms_send_reports/history_list', $data, true);
    			$this->mydompdf->createPDF($html, 'SMS_Report', true, $data['paper_size'], $data['page_type']);
    		}else{
    			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
    			$data['maincontent'] = $this->load->view('sms_send_reports/index', $data, true);
    			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    		}
    	}
}
