<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Missed_attendance extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Advocate'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] =$this->lang->line('missed').' '.$this->lang->line('attendance').' '.$this->lang->line('list');
        $config['base_url'] = site_url('missed_attendance/index/');
        $data['missed_attendance'] = $this->Advocate->get_all_missed_attendance_history_list();
        $data['language']=$this->session->userdata('site_lang');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('missed_attendance/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
