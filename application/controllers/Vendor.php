<?php

class Vendor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('vendor_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('vendor/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_vendor_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['vendors'] = $this->Advocate->get_all_vendor_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('vendor/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $mobile=$this->input->post('mobile');
            if($this->Advocate->checkifexist_vendor_by_mobile($mobile))
            {
              $sdata['exception'] = "This mobile number'".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("vendor/add");
            }
            $data['mobile']=$mobile;
            $data['company_name']=$this->input->post('company_name');
            $data['first_name']=$this->input->post('first_name');
            $data['last_name']=$this->input->post('last_name');
            $data['email']=$this->input->post('email');
            $data['alternative_mobile']=$this->input->post('alternative_mobile');
            $data['address']=$this->input->post('address');
            $data['country_id']=$this->input->post('country_id');
            $data['gstin']=$this->input->post('gstin');
            $data['pan']=$this->input->post('pan');
            $data['district_id']=$this->input->post('district_id');
            $data['upazilas_id']=$this->input->post('upazilas_id');
            $data['is_active']=$this->input->post('is_active');
            if ($this->Advocate->add_vendor($data)) {
                $sdata['message'] = "Information Successfully Added";
                $this->session->set_userdata($sdata);
                redirect("vendor/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("vendor/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('vendor');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['countries'] = $this->Advocate->get_country_list();
        $data['districts'] = $this->Advocate->get_districts_list_by_country(0);
        $data['upazilas'] = $this->Advocate->get_upazilas_list_by_district(0);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('vendor/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $mobile=$this->input->post('mobile');
          if($this->Advocate->checkifexist_update_vendor_by_mobile($mobile,$id))
          {
            $sdata['exception'] = "This mobile number'".$name."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("vendor/edit/".$id);
          }
          $data['id']=$id;
          $data['mobile']=$mobile;
          $data['company_name']=$this->input->post('company_name');
          $data['first_name']=$this->input->post('first_name');
          $data['last_name']=$this->input->post('last_name');
          $data['email']=$this->input->post('email');
          $data['alternative_mobile']=$this->input->post('alternative_mobile');
          $data['address']=$this->input->post('address');
          $data['country_id']=$this->input->post('country_id');
          $data['gstin']=$this->input->post('gstin');
          $data['pan']=$this->input->post('pan');
          $data['district_id']=$this->input->post('district_id');
          $data['upazilas_id']=$this->input->post('upazilas_id');
          $data['is_active']=$this->input->post('is_active');
          if ($this->Advocate->edit_vendor($data, $id)) {
              $sdata['message'] = "Information Successfully updated.";
              $this->session->set_userdata($sdata);
              redirect("vendor/index");
          } else {
              $sdata['exception'] = "Information could not add";
              $this->session->set_userdata($sdata);
              redirect("vendor/index");
          }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('vendor');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_data'] = $this->Advocate->read_vendor($id);
        // print_r($data['row_data']);
        // die;
        $data['countries'] = $this->Advocate->get_country_list();
        $data['districts'] = $this->Advocate->get_districts_list_by_country($data['row_data']->country_id);
        $data['upazilas'] = $this->Advocate->get_upazilas_list_by_district($data['row_data']->district_id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('vendor/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Advocate->delete_vendor($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("vendor/index");
    }
    public function updateMsgStatusVendorStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_vendor', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }
    public function ajax_district_by_country()
    {
        $country_id = $_GET['country_id'];
        $data["dropdown_list"]= $this->Advocate->get_districts_list_by_country($country_id);
        $this->load->view('vendor/dropdown_list', $data);
    }
    public function ajax_upazilas_by_district()
    {
        $district_id = $_GET['district_id'];
        $data["dropdown_list"]= $this->Advocate->get_upazilas_list_by_district($district_id);
        $this->load->view('vendor/dropdown_list', $data);
    }
}
