<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Institute_setup extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Advocate'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('institute') . ' ' . $this->lang->line('information');
        $data['heading_msg'] = $this->lang->line('institute') . ' ' . $this->lang->line('information');
        $data['contact_info'] = $this->Admin_login->get_contact_info();
        $data['institute_type'] = $this->db->query("SELECT * FROM tbl_institute_type")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('institute_setup/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update()
    {
        $file_name = $_FILES['txtPhoto']['name'];
        if ($file_name != '') {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/logos/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Contact_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/logos/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['school_name'] = $this->input->post('txtSchoolName', true);
                        $data['school_name_bangla'] = $this->input->post('school_name_bangla', true);
                        $data['institute_type'] = $this->input->post('institute_type', true);
                        $data['eiin_number'] = $this->input->post('txtEiinNumber', true);
                        $data['address'] = $this->input->post('txtAddress', true);
                        $data['address_bangla'] = $this->input->post('address_bangla', true);
                        $data['email'] = $this->input->post('txtEmail', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['facebook_address'] = $this->input->post('txtFacebookAddress', true);
                        $data['web_address'] = $this->input->post('txtWebAddress', true);
                        $data['picture'] = $new_photo_name;
                        $this->Admin_login->update_contact_info($data);
                        $sdata['message'] = $this->lang->line('edit_success_message');
                        $this->session->set_userdata($sdata);
                        redirect("institute_setup/index");
                    } else {
                        $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                        $this->session->set_userdata($sdata);
                        redirect("institute_setup/index");
                    }
                } else {
                    $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                    $this->session->set_userdata($sdata);
                    redirect("institute_setup/index");
                }
            }
        } else {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['school_name'] = $this->input->post('txtSchoolName', true);
            $data['school_name_bangla'] = $this->input->post('school_name_bangla', true);
            $data['institute_type'] = $this->input->post('institute_type', true);
            $data['school_short_name'] = $this->input->post('txtSchoolShortName', true);
            $data['eiin_number'] = $this->input->post('txtEiinNumber', true);
            $data['address'] = $this->input->post('txtAddress', true);
            $data['address_bangla'] = $this->input->post('address_bangla', true);
            $data['email'] = $this->input->post('txtEmail', true);
            $data['mobile'] = $this->input->post('txtMobile', true);
            $data['facebook_address'] = $this->input->post('txtFacebookAddress', true);
            $data['web_address'] = $this->input->post('txtWebAddress', true);
            $this->Admin_login->update_contact_info($data);
            $sdata['message'] = "You are Successfully Updated Contact Info ! ";
            $this->session->set_userdata($sdata);
            redirect("institute_setup/index");
        }
    }
}
