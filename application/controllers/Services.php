<?php

class Services extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('services');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('services/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_all_services_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['services'] = $this->Cases->get_all_services_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('services/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $name=$this->input->post('name');
            $amount=$this->input->post('amount');
            $data['name']=$name;
            $data['amount']=$amount;
            $data['is_active']=$this->input->post('is_active');
            if ($amount<=0) {
              $sdata['exception'] = "This value '".$amount."' is not acceptable.";
              $this->session->set_userdata($sdata);
              redirect("services/add/");
            }
            if($this->Cases->checkifexist_services_by_name($name))
            {
              $sdata['exception'] = "This name '".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("services/add");
            }
            if ($this->Cases->add_services($data)) {
                $sdata['message'] = "Information Successfully Added";
                $this->session->set_userdata($sdata);
                redirect("services/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("services/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('services');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('services/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $name=$this->input->post('name');
          $amount=$this->input->post('amount');
          $data['name']=$name;
          $data['id']=$id;
          $data['amount']=$amount;
          $data['is_active']=$this->input->post('is_active');
          if ($amount<=0) {
            $sdata['exception'] = "This value '".$amount."' is not acceptable.";
            $this->session->set_userdata($sdata);
            redirect("services/edit/".$id);
          }
          // print_r($id);
          // die()
          if($this->Cases->checkifexist_update_services_by_name($name,$id))
          {
            $sdata['exception'] = "This name '".$name."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("services/edit/".$id);
          }
            if ($this->Cases->edit_services($data, $id)) {
                $sdata['message'] = "Information Successfully updated.";
                $this->session->set_userdata($sdata);
                redirect("services/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("services/index");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('services');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row'] = $this->Cases->read_services($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('services/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Cases->delete_services($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("services/index");
    }
    public function updateMsgServiceStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_service', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }
}
