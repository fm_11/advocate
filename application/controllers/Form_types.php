<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Form_types extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('form_type_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('form_types/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_forms_types_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['form_types'] = $this->Advocate->get_all_forms_types_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('form_types/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $name=$this->input->post('name');
            $data['name']=$name;
            $data['bn_name']=$this->input->post('bn_name');
            $data['is_active']=1;
            if($this->Advocate->checkifexist_forms_types_by_name($name))
            {
              $sdata['exception'] = "This name '".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("form_types/add");
            }
            if ($this->Advocate->add_forms_types($data)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                  redirect("form_types/add");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("form_types/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('form_type');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('form_types/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $name=$this->input->post('name');
          $data['name']=$name;
          $data['id']=$id;
          $data['bn_name']=$this->input->post('bn_name');
          $data['is_active']=$this->input->post('is_active');
          // print_r($id);
          // die()
          if($this->Advocate->checkifexist_update_forms_types_by_name($name,$id))
          {
            $sdata['exception'] = "This name '".$name."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("form_types/edit/".$id);
          }
            if ($this->Advocate->edit_forms_types($data, $id)) {
                $sdata['message'] = $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                redirect("form_types/index");
            } else {
                $sdata['exception'] = "Information could not updated";
                $this->session->set_userdata($sdata);
                redirect("form_types/edit/".$id);
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('judge');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row'] = $this->Advocate->read_forms_types($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('form_types/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Advocate->delete_forms_types($id);
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("form_types/index");
    }
    public function updateMsgStatusJudgeStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_forms_types', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }

}
