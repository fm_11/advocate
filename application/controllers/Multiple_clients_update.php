<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Multiple_clients_update extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       $this->load->model(array('Cases','Advocate'));
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        if ($_POST) {
          $cond = array();
          $mobile = $this->input->post("mobile");
          $district_id = $this->input->post("district_id");
          $upazilas_id = $this->input->post("upazilas_id");
          $sdata['mobile'] = $mobile;
          $sdata['district_id'] = $district_id;
          $sdata['upazilas_id'] = $upazilas_id;
          $this->session->set_userdata($sdata);
          $cond['mobile'] = $mobile;
          $cond['district_id'] = $district_id;
          $cond['upazilas_id'] = $upazilas_id;

           $data['clients'] = $this->Cases->get_all_clients_list(0, 0, $cond);
        //  echo "<pre>";
         //  print_r($_POST);
         //    print_r($data['clients']);
         //  die;
          //$data['data_table'] = $this->load->view('multiple_clients_update/report', $data, true);
           //   print_r($this->load->view('multiple_clients_update/report', $data, true));
           // echo "string";
           //  die;
        }




        $user_info = $this->session->userdata('user_info');
        $lang=$this->session->userdata('site_lang');
        $district_ids=$user_info[0]->client_user[0]->district_id;
        $data['title'] = $this->lang->line('multiple').' '.$this->lang->line('client').' '.$this->lang->line('update');
        $data['heading_msg'] = $this->lang->line('multiple').' '.$this->lang->line('client').' '.$this->lang->line('update');
        $data['districts'] = $this->Advocate->get_districts_list_by_lang_and_districtid($lang,$district_ids);
        $data['upazillas'] = $this->Advocate->get_upazilas_list_by_district_and_lang($lang,$district_ids);
        $data['language']=$lang;
        //   echo "<pre>";
        // print_r($data['upazillas']);
        // echo "string";
        //   die;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('multiple_clients_update/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }



    public function process()
    {

      $data = array();
      if ($_POST) {
        $cond = array();
        // if ($_POST) {
        //     $name = $this->input->post("name");
        //     $status = $this->input->post("status");
        //     $sdata['name'] = $name;
        //     $sdata['status'] = $status;
        //     $this->session->set_userdata($sdata);
        //     $cond['name'] = $name;
        //     $cond['status'] = $status;
        // } else {
        //     $name = $this->session->userdata('name');
        //     $status = $this->session->userdata('status');
        //     $cond['name'] = $name;
        //     $cond['status'] = $status;
        // }
        // if ($_POST) {
            $mobile = $this->input->post("mobile");
            $district_id = $this->input->post("district_id");
            $upazilas_id = $this->input->post("upazilas_id");
            $sdata['mobile'] = $mobile;
            $sdata['district_id'] = $district_id;
            $sdata['upazilas_id'] = $upazilas_id;
            $this->session->set_userdata($sdata);
            $cond['mobile'] = $mobile;
            $cond['district_id'] = $district_id;
            $cond['upazilas_id'] = $upazilas_id;
        // } else {
        //     $name = $this->session->userdata('name');
        //     $status = $this->session->userdata('status');
        //     $cond['name'] = $name;
        //     $cond['status'] = $status;
        // }
        $data['clients'] = $this->Cases->get_all_clients_list(0, 0, $cond);
        $data['data_table'] = $this->load->view('multiple_clients_update/report', $data, true);


      }

      $data['title'] = $this->lang->line('update').' '.$this->lang->line('court').' '.$this->lang->line('information');
      $data['heading_msg'] = $this->lang->line('update').' '.$this->lang->line('court').' '.$this->lang->line('information');
      $data['is_show_button'] = "index";
      $data['court_type'] = $this->db->query("SELECT * FROM ad_court_type")->result_array();
      $data['court'] = $this->db->query("SELECT * FROM `ad_court` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('multiple_clients_update/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function data_save()
    {
      if ($_POST) {
        // echo "<pre>";
        // print_r($_POST);
        // die;
       $loop_time = $this->input->post("loop_time");
        $i = 1;
        $update_data = array();
        $loop=0;
        while ($i <= $loop_time) {
          if ($this->input->post("is_allow_" . $i)) {
            $update_data[$loop]['id']=$this->input->post("id_" . $i);
            $update_data[$loop]['first_name'] = $this->input->post("first_name_" . $i);
            $update_data[$loop]['last_name'] = $this->input->post("last_name_" . $i);
            $update_data[$loop]['total_person'] = $this->input->post("total_person_" . $i);
            $update_data[$loop]['mobile'] = $this->input->post("mobile_" . $i);
            $update_data[$loop]['upazilas_id'] = $this->input->post("upazilas_id_" . $i);
            $update_data[$loop]['gender'] = $this->input->post("gender_" . $i);
            $loop++;
          }
          $i++;
        }
        if(count($update_data)>0)
        {
          // echo '<pre>';
          // print_r($update_data); die;

          $this->db->update_batch('ad_clients',$update_data, 'id');
          $sdata['message'] = $this->lang->line('edit_success_message');
          $this->session->set_userdata($sdata);
        }else{
         $sdata['exception'] = "Please choose at least one clients.";
         $this->session->set_userdata($sdata);
        }
        redirect("multiple_clients_update/index");
      }
    }

  }

 ?>
