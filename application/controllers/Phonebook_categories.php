<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Phonebook_categories extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->load->model(array('Message', 'Timekeeping', 'Admin_login'));
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] =  $this->lang->line('sms') . ' ' . $this->lang->line('category');
        $data['heading_msg'] =  $this->lang->line('sms') . ' ' . $this->lang->line('category');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('phonebook_categories/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $insertData['name'] = $_POST['categoryName'];
            $insertData['remarks'] = $_POST['CategoryNote'];
            $tableName = "tbl_message_category";
            $returnData = $this->Message->DoCommonInsert($insertData, $tableName);
            if ($returnData['status'] == 1) {
                $sdata['message'] = $this->lang->line('add_success_message');
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
            }
            $this->session->set_userdata($sdata);
            redirect('phonebook_categories/add');
        }
        $data = array();
        $data['is_show_button'] = "index";
        $data['title'] = $this->lang->line('sms') . ' ' . $this->lang->line('category') . ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('sms') . ' ' . $this->lang->line('category') . ' ' . $this->lang->line('add');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('phonebook_categories/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id=null)
    {
        if ($_POST) {
            $updateData['id'] = $_POST['id'];
            $updateData['name'] = $_POST['categoryName'];
            $updateData['remarks'] = $_POST['CategoryNote'];
            $this->db->where('id', $updateData['id']);
            $this->db->update('tbl_message_category', $updateData);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('phonebook_categories/index');
        }
        $data = array();
        $data['is_show_button'] = "index";
        $data['title'] = $this->lang->line('sms') . ' ' . $this->lang->line('category') . ' ' . $this->lang->line('edit');
        $data['categoryInfo'] = $this->db->query("SELECT * FROM tbl_message_category WHERE id = '$id'")->result_array();
        $data['heading_msg'] = $this->lang->line('sms') . ' ' . $this->lang->line('category') . ' ' . $this->lang->line('edit');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('phonebook_categories/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_message_contact_list` WHERE `category_id` = '$id' LIMIT 1")->result_array();
        /* echo '<pre>';
        print_r($Check); */
        if (empty($Check)) {
            if ($this->db->query("DELETE FROM tbl_message_category WHERE `id` = '$id'")) {
                $sdata['message'] = $this->lang->line('delete_success_message');
            } else {
                $sdata['exception'] = $this->lang->line('delete_error_message');
            }
            $this->session->set_userdata($sdata);
            redirect('phonebook_categories/index');
        } else {
            $sdata['exception'] = $this->lang->line('delete_error_message') . ' (Depended number found)';
            $this->session->set_userdata($sdata);
            redirect('phonebook_categories/index');
        }
    }
}
