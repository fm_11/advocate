<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Todays_attendance extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login','Advocate'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index($flag=null)
    {
      $lang=$this->session->userdata('site_lang');
      $data = array();
      $data['title'] = $this->lang->line('today_attendance');
      $data['heading_msg'] = $this->lang->line('today_attendance');
      $cond = array();
      $cond['from_date'] = date("Y-m-d");
      $cond['to_date'] = date("Y-m-d");
      $data['today_attendace_list'] = $this->Advocate->get_all_attendance_list_by_date_range(0, 0, $cond);
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('todays_attendance/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
      }
      public function tomorrow($flag=null)
      {
        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['title'] = $this->lang->line('tomorrow_attendence');
        $data['heading_msg'] = $this->lang->line('tomorrow_attendence');
        $tomorrow_date = date('Y-m-d');
        $tomorrow_date = date('Y-m-d', strtotime($tomorrow_date . ' +1 day'));
        $cond = array();
        $cond['from_date'] = $tomorrow_date;
        $cond['to_date'] = $tomorrow_date;
        $data['tomorrow_attendace_list'] = $this->Advocate->get_all_attendance_list_by_date_range(0, 0, $cond);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('todays_attendance/tomorrow', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }


}
