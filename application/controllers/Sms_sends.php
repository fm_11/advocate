<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sms_sends extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Student','Admin_login','Message','Timekeeping','Advocate'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function to_guardian_sms_send()
    {
        $data = array();
        if ($_POST) {
            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $data['class_shift_section_id'] = $class_shift_section_id;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['shift_id'] = $shift_id;
            $data['group'] = $this->input->post('group_id', true);

            $sdata['class_shift_section_id'] = $class_shift_section_id;
            $sdata['group_id'] = $data['group'];
            $this->session->set_userdata($sdata);

            if($data['group'] == 'all'){
              $data['group'] = '';
            }
            $data['student_info'] = $this->Student->get_all_student_list(0, 0, $data);
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Send SMS To Guardian';
        $data['heading_msg'] = "Send SMS To Guardian";
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('sms_sends/to_guardian_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function guardian_sms_send_action()
    {
        if ($_POST) {
			$template_id = $this->input->post('template_id', true);
			$template_body = $this->input->post('template_body', true);
            $loop_time = $this->input->post('loop_time', true);
			$i = 0;
			$numbers = array();
			$numbers_with_student_id = array();
			while ($i < $loop_time) {
				$is_send = $this->input->post('is_send_' . $i, true);
				if (isset($is_send) && $is_send != '') {
					//sms send data ready
					$number =  trim($this->input->post('guardian_mobile_' . $i, true));
					if($this->Admin_login->validate_mobile_number($number)){
						$numbers[] = $number;
					}
					$numbers_with_student_id[$number]['student_id'] = $this->input->post('student_id_' . $i, true);
					$numbers_with_student_id[$number]['number'] = $number;
					//sms send data ready end
				}
				$i++;
			}
//			echo '<pre>';
//			print_r($numbers_with_student_id);
//			die;
			$send_from = 'TG';
			$status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
			if (strtolower($status['status']) == 'success') {
				$sdata['message'] = "SMS send successfully";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/to_guardian_sms_send");
			} else {
				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
				$this->session->set_userdata($sdata);
				redirect("sms_sends/to_guardian_sms_send");
			}
        }
    }

    public function to_client_sms_send()
    {
        $data = array();
        if ($_POST) {
            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $data['class_shift_section_id'] = $class_shift_section_id;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['shift_id'] = $shift_id;
            $data['group'] = $this->input->post('group_id', true);

            $sdata['class_shift_section_id'] = $class_shift_section_id;
            $sdata['group_id'] = $data['group'];
            $this->session->set_userdata($sdata);

            if($data['group'] == 'all'){
              $data['group'] = '';
            }
            $data['student_info'] = $this->Student->get_all_student_list(0, 0, $data);
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Send SMS To Guardian';
        $data['heading_msg'] = "Send SMS To Guardian";
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('sms_sends/to_guardian_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

//     public function guardian_sms_send_action()
//     {
//         if ($_POST) {
// 			$template_id = $this->input->post('template_id', true);
// 			$template_body = $this->input->post('template_body', true);
//             $loop_time = $this->input->post('loop_time', true);
// 			$i = 0;
// 			$numbers = array();
// 			$numbers_with_student_id = array();
// 			while ($i < $loop_time) {
// 				$is_send = $this->input->post('is_send_' . $i, true);
// 				if (isset($is_send) && $is_send != '') {
// 					//sms send data ready
// 					$number =  trim($this->input->post('guardian_mobile_' . $i, true));
// 					if($this->Admin_login->validate_mobile_number($number)){
// 						$numbers[] = $number;
// 					}
// 					$numbers_with_student_id[$number]['student_id'] = $this->input->post('student_id_' . $i, true);
// 					$numbers_with_student_id[$number]['number'] = $number;
// 					//sms send data ready end
// 				}
// 				$i++;
// 			}
// //			echo '<pre>';
// //			print_r($numbers_with_student_id);
// //			die;
// 			$send_from = 'TG';
// 			$status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
// 			if (strtolower($status['status']) == 'success') {
// 				$sdata['message'] = "SMS send successfully";
// 				$this->session->set_userdata($sdata);
// 				redirect("sms_sends/to_guardian_sms_send");
// 			} else {
// 				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
// 				$this->session->set_userdata($sdata);
// 				redirect("sms_sends/to_guardian_sms_send");
// 			}
//         }
//     }

	public function to_excel_file()
	{
		if ($_POST) {
			//For Excel Read
			require_once APPPATH . 'third_party/PHPExcel.php';
			$this->excel = new PHPExcel();
			//For Excel Read
			$template_id = $this->input->post('template_id', true);
			$template_body = $this->input->post('template_body', true);
			//For Excel Upload
			$configUpload['upload_path'] = FCPATH . 'uploads/excel/';
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '15000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('txtFile');
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension = $upload_data['file_ext'];    // uploded file extension

			if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
			} else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
			}

			//Set to read only
			$objReader->setReadDataOnly(true);
			//Load excel file
			$objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
			$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			//loop from first data untill last data

			$send_from = 'EX';
			$messageData = array();
			$numbers = array();
			$sms_index = 0;
			$total_num_of_sms_for_dynamic_send = 0;
			for ($i = 2; $i <= $totalrows; $i++) {
				$number = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
				if ($number == '') {
					break;
				}
				if ($template_id == 'N') {
					$message_body = ltrim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
					$messageData[$sms_index]['msisdn'] = $number;
					$messageData[$sms_index]['text'] = $message_body;
					$total_num_of_sms_for_dynamic_send += $this->Message->smsCount($message_body);
					$messageData[$sms_index]['csms_id'] = $this->Message->generateSMSTransactionID();
					$sms_index++;
				} else {
					//sms send data ready
					$numbers[] = $number;
					//sms send data ready end
				}
			}
			//For Excel Upload

			//only number sms send
			if ($template_id != 'N') {

				$status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,NULL);
			}else{
				$blank_array = array();
				$status = $this->Message->dynamicSMSSend($messageData, $send_from,$blank_array,$total_num_of_sms_for_dynamic_send);
			}
			//only number sms send

			$filedel = PUBPATH . 'uploads/excel/' . $file_name;
			if (unlink($filedel)) {
				if (strtolower($status['status']) == 'success') {
					$sdata['message'] = "SMS send successfully";
					$this->session->set_userdata($sdata);
					redirect("sms_sends/to_excel_file");
				} else {
					$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
					$this->session->set_userdata($sdata);
					redirect("sms_sends/to_excel_file");
				}
			}
		} else {
			$data = array();
			$data['title'] = 'Send SMS From Excel File';
			$data['heading_msg'] = "Send SMS From Excel File";
			$data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('sms_sends/to_excel_file_form', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	public function to_result_notify()
	{
		$data = array();
		if ($_POST) {
			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['class_id'] = $class_id;
			$data['shift_id'] = $shift_id;
			$data['section_id'] = $section_id;
			$data['group_id'] = $this->input->post('txtGroup', true);
			$data['exam_id'] = $this->input->post('txtExam', true);
			$data['sms_type'] = $this->input->post('txtSMSType', true);
			$data['with_school_name'] = $this->input->post('txtWithSchoolName', true);
			$data['student_info'] = $this->Message->get_student_info_for_result($class_id,$shift_id,$section_id,$data['group_id'],$data['exam_id']);
		}
		$data['title'] = 'Result Notify SMS Send';
		$data['heading_msg'] = "Result Notify SMS Send";
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$current_year = $this->Admin_login->getCurrentSystemYear();
		$data['exams'] = $this->db->query("SELECT * FROM tbl_exam WHERE year = $current_year ORDER BY exam_order;")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_sends/result_notify_sms_send_form', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function to_result_notify_sms_send()
	{
		if ($_POST) {
			$data = array();
			$data['class_id'] = $this->input->post('class_id', true);
			$data['section_id'] = $this->input->post('section_id', true);
			$data['group'] = $this->input->post('group_id', true);
			$data['exam_id'] = $this->input->post('exam_id', true);
			$data['shift_id'] = $this->input->post('shift_id', true);
			$data['date'] = date('Y-m-d');
			$sms_type = $this->input->post('sms_type', true);
			$with_school_name = $this->input->post('with_school_name', true);
			$school_name = "";
			if ($with_school_name == 'Y') {
				$school_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
				$school_name = $school_info[0]['school_name'];
			}
			$exam_id = $this->input->post('exam_id', true);
			$exam = $this->db->query("SELECT * FROM tbl_exam WHERE id = '$exam_id'")->result_array();
			$exam_name = $exam[0]['name'];
			$loop_time = $this->input->post('loop_time', true);

			$i = 0;
			$send_from = "RN";
			$total_num_of_sms_for_dynamic_send = 0;
			$messageData = array();
			$numbers_with_student_id = array();
			$sms_index = 0;
			while ($i < $loop_time) {
				$is_send = $this->input->post('is_send_' . $i, true);
				if (isset($is_send) && $is_send != '') {
					if ($this->Admin_login->validate_mobile_number($this->input->post("guardian_mobile_" . $i))) {
						$number = $this->input->post("guardian_mobile_" . $i);
						$msg = $this->generate_result_message($this->input->post("student_id_" . $i), $data['class_id'], $data['exam_id'], $sms_type);
						if ($msg != "") {
							$msg .= ', ' . $school_name;
							$messageData[$sms_index]['msisdn'] = $number;
							$message_body = $exam_name . " Result, Name: " . $this->input->post("name_" . $i) . ', Roll: ' . $this->input->post("roll_no_" . $i) .  " :: " . $msg . ". Thank You";
							$messageData[$sms_index]['text'] = $message_body;
							$total_num_of_sms_for_dynamic_send += $this->Message->smsCount($message_body);
							$messageData[$sms_index]['csms_id'] = $this->Message->generateSMSTransactionID();
							$sms_index++;
						}
						$numbers_with_student_id[$number]['student_id'] = $this->input->post('student_id_' . $i, true);
						$numbers_with_student_id[$number]['number'] = $number;
					}
				}
				$i++;
			}
			$status = $this->Message->dynamicSMSSend($messageData, $send_from,$numbers_with_student_id,$total_num_of_sms_for_dynamic_send);
			if (strtolower($status['status']) == 'success') {
				$sdata['message'] = "SMS send successfully";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/to_result_notify");
			} else {
				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
				$this->session->set_userdata($sdata);
				redirect("sms_sends/to_result_notify");
			}
		}
	}

	//result message
	public function generate_result_message($StudentID, $ClassID, $ExamID, $sms_type)
	{
		$result_info = $this->Student->result_by_student_after_process($StudentID, $ExamID);
		$msg = "";
		if (!empty($result_info)) {
			for ($A = 0; $A < (count($result_info['result_process_details'])); $A++) {
				if ($A != 0) {
					if ($sms_type == 'MG') {
						$msg .= ', ' . $result_info['result_process_details'][$A]['subject_name'] . '=' . round($result_info['result_process_details'][$A]['total_obtain'],2) . '(' . $result_info['result_process_details'][$A]['alpha_gpa'] . ')';
					} elseif ($sms_type == 'M') {
						$msg .= ', ' . $result_info['result_process_details'][$A]['subject_name'] . '=' . round($result_info['result_process_details'][$A]['total_obtain'],2);
					} else {
						$msg .= ', ' . $result_info['result_process_details'][$A]['subject_name'] . '=' . $result_info['result_process_details'][$A]['alpha_gpa'];
					}
				} else {
					if ($sms_type == 'MG') {
						$msg .= $result_info['result_process_details'][$A]['subject_name'] . '=' . round($result_info['result_process_details'][$A]['total_obtain'],2) . '(' . $result_info['result_process_details'][$A]['alpha_gpa'] . ')';
					} elseif ($sms_type == 'M') {
						$msg .= $result_info['result_process_details'][$A]['subject_name'] . '=' . round($result_info['result_process_details'][$A]['total_obtain'],2);
					} else {
						$msg .= $result_info['result_process_details'][$A]['subject_name'] . '=' . $result_info['result_process_details'][$A]['alpha_gpa'];
					}
				}
			}
			// echo $A;die;
			$msg .= ", Total Mark :" . round($result_info['result_process_info'][0]['total_obtain_mark'],2) . ", CGPA :" . round($result_info['result_process_info'][0]['gpa_with_optional'],2);
		}
		//echo $msg; die;
		return $msg;
	}

	//result message



	public function send_from_phoneBook_list()
	{
		$data = array();
		if ($_POST) {
			$CategoryType = $this->input->post('CategoryType', true);
			$CategoryInfo = $this->Message->GetCategoryInfo($CategoryType);
			$data['CategoryType'] = $CategoryType;
			$data['CategoryInfo'] = $CategoryInfo;
			/* echo '<pre>';
			print_r($CategoryInfo);
			die(); */
			$data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
		}
		$data['title'] = 'Send SMS From Phone Book';
		$data['heading_msg'] = "Send SMS From Phone Book";
		$data['category'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_sends/view_category_phoneBook_list', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function SendcategoryPhoneBookSMS()
	{
		if ($_POST) {
			$template_id = $this->input->post('template_id', true);
			$template_body = $this->input->post('template_body', true);
			$send_from = "PH";
			$loop_time = $this->input->post('loop_time', true);
			$i = 0;
			$numbers = array();
			while ($i < $loop_time) {
				$is_send = $this->input->post('is_send_' . $i, true);
				if (isset($is_send) && $is_send != '') {
					//sms send data ready
					$number =  trim($this->input->post('mobile_' . $i, true));
					if($this->Admin_login->validate_mobile_number($number)){
						$numbers[] = $number;
					}
					//sms send data ready end
				}
				$i++;
			}
			$numbers_with_student_id = array();
			$status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
			if (strtolower($status['status']) == 'success') {
				$sdata['message'] = "SMS send successfully";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/send_from_phoneBook_list");
			} else {
				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
				$this->session->set_userdata($sdata);
				redirect("sms_sends/send_from_phoneBook_list");
			}
		}
	}


	public function attendance_sms()
	{
		$data = array();
		$data['title'] = $this->lang->line('attendance').' '.$this->lang->line('sms');
		$data['heading_msg'] = $this->lang->line('attendance').' '.$this->lang->line('sms');
		if ($_POST) {
      if($this->Advocate->checkifexist_advocate_setting())
      {
        $sdata['exception'] = $this->lang->line('advocate_setting_message');
        $this->session->set_userdata($sdata);
        redirect("sms_sends/attendance_sms/");
      }
      $setting=$this->Advocate->read_advocate_setting();
      $day=$setting->advance_day;
      $data['setting']=$setting;
      $stop_date = new DateTime(date("Y-m-d", strtotime($this->input->post('txtDate'))));
      $stop_date->modify('+'.$day.' day');
      $date=$stop_date->format('Y-m-d');
      // echo "<pre>";
      // print_r($day.'<br>');
      // print_r($date);
      // die;
			$data['business_on_date'] = date("Y-m-d", strtotime($this->input->post('txtDate')));

			$data['attendance_sms'] = $this->Timekeeping->getAttendancebydate($date);

			$data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
			$data['data_table'] = $this->load->view('sms_sends/attendance_sms_send_form_table', $data, true);
		}else{
			$data['date'] = date("Y-m-d", strtotime($this->input->post('txtDate')));

		}

		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_sends/attendance_sms_send_form', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function attendance_sms_send_post()
	{
		if ($_POST) {
			$template_id = 0;

			$send_from = "HS";
			$loop_time = $this->input->post('loop_time', true);
      $eorr_msg="";
			$i = 0;
      $total_num_of_sms_for_dynamic_send = 0;
			$messageData = array();
      $sms_index = 0;
			$numbers_with_student_id = array();
			while ($i < $loop_time) {
      	$numbers = array();
        $template_body = $this->input->post('message_template_'. $i, true);
				$is_send = $this->input->post('is_send_' . $i, true);
				if (isset($is_send) && $is_send != '') {

					//sms send data ready
					$number =  trim($this->input->post('mobile_' . $i, true));
					// if($this->Admin_login->validate_mobile_number($number)){
					// 	$numbers[] = $number;
					// }

          $messageData[$sms_index]['msisdn'] = $number;
          $messageData[$sms_index]['text'] = $template_body;
          $total_num_of_sms_for_dynamic_send += $this->Message->smsCount($template_body);
          $messageData[$sms_index]['csms_id'] = $this->Message->generateSMSTransactionID();
          $sms_index++;
					$numbers_with_student_id[$number]['client_id'] = $this->input->post('client_id_' . $i, true);
					$numbers_with_student_id[$number]['number'] = $number;
        	$numbers_with_student_id[$number]['case_history_id'] =$this->input->post('case_history_id_' . $i, true);

				}
				$i++;
			}

      $status = $this->Message->dynamicSMSSend($messageData, $send_from, $numbers_with_student_id, $total_num_of_sms_for_dynamic_send);
			if (strtolower($status['status']) == 'success') {
				$sdata['message'] = "SMS send successfully";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/attendance_sms");
			} else {
				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] . ')';
				$this->session->set_userdata($sdata);
				redirect("sms_sends/attendance_sms");
			}
		}
	}


	public function to_teacher_sms_send_form()
	{
		$data = array();
		$data['title'] = 'Send SMS To Teacher';
		$data['heading_msg'] = "Send SMS To Teacher";
		$data['teacher_info'] = $this->Message->get_all_teachers();
		$data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_sends/to_teacher_sms_send_form', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function to_teacher_sms_send()
	{
		if ($_POST) {
			$template_id = $this->input->post('template_id', true);
			$template_body = $this->input->post('template_body', true);
			$data = array();
			$data['template_id'] = $template_id;
			$data['template_body'] = $template_body;
			$data['date'] = date('Y-m-d H:i:s');
			$send_from = "TS";
			$loop_time = $this->input->post('loop_time', true);
			$numbers = array();
			$i = 0;
			$numbers_with_teacher_id = array();
			while ($i < $loop_time) {
				$is_send = $this->input->post('is_send_' . $i, true);
				if (isset($is_send) && $is_send != '') {

					//sms send data ready
					$number =  trim($this->input->post('mobile_' . $i, true));
					if($this->Admin_login->validate_mobile_number($number)){
						$numbers[] = $number;
					}
					$numbers_with_teacher_id[$number]['teacher_id'] = $this->input->post('teacher_id_' . $i, true);
					$numbers_with_teacher_id[$number]['number'] = $number;
					//sms send data ready end

				}
				$i++;
			}

			$status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_teacher_id);
			if (strtolower($status['status']) == 'success') {
				$sdata['message'] = "SMS send successfully";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/to_teacher_sms_send_form");
			} else {
				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
				$this->session->set_userdata($sdata);
				redirect("sms_sends/to_teacher_sms_send_form");
			}
		}
	}

	public function SendBirthDaySMS()
	{
		$data = array();
		if ($_POST) {
			$ReceiverType = $this->input->post('ReceiverType', true);
			if ($ReceiverType == 'Student') {
				$data['teacherSMSStatus'] = 0;
				$data['studentSMSStatus'] = 1;
				$student_inforeturn = $this->Message->GetStudentInfoByBirthday();
				if ($student_inforeturn['status'] == 1) {
					$data['studentInfoStatus'] = 1;
					$data['studentInfo'] = $student_inforeturn['studentData'];
				} else {
					$data['studentInfoStatus'] = 0;
				}
			} elseif ($ReceiverType == 'Teacher') {
				$data['teacherSMSStatus'] = 1;
				$data['studentSMSStatus'] = 0;
				$teacher_inforeturn = $this->Message->GetTeacherInfoByBirthday();
				if ($teacher_inforeturn['status'] == 1) {
					$data['teacherInfoStatus'] = 1;
					$data['teacherInfo'] = $teacher_inforeturn['teacherData'];
				} else {
					$data['teacherInfoStatus'] = 0;
				}
			}
			$data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
		}
		$data['title'] = 'Send Birth Day SMS';
		$data['heading_msg'] = "Send Birth Day SMS";
		$data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
		$data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
		$data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_sends/SendBirthDaySMS', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function studentBirthDaySMS()
	{
		if ($_POST) {
			/* echo '<pre>';
			print_r($_POST); */
			$data = array();
			$data['birthDate'] = $this->input->post('birthDate', true);
			$data['template_id'] = $this->input->post('template_id', true);
			$data['template_body'] = $this->input->post('template_body', true);
			$data['date'] = date('Y-m-d H:i:s');
			if ($this->db->insert('tbl_message_for_birthday', $data)) {
				$insert_id = $this->db->insert_id();
				$loop_time = $this->input->post('loop_time', true);
				$i = 0;
				$num = "";
				while ($i < $loop_time) {
					$is_send = $this->input->post('is_send_' . $i, true);
					if (isset($is_send) && $is_send != '') {

						//sms send data ready
						$num = $this->Admin_login->generateNumberlist($num, $this->input->post('guardian_mobile_' . $i, true));
						//sms send data ready end

						$cdata = array();
						$cdata['to_message_for_birthday_id'] = $insert_id;
						$cdata['student_id'] = $this->input->post('student_id_' . $i, true);
						$cdata['class_id'] = $this->input->post('student_class_id_' . $i, true);
						$cdata['section_id'] = $this->input->post('student_section_id_' . $i, true);
						$cdata['group_id'] = $this->input->post('student_group_id_' . $i, true);
						$cdata['mobile_number'] = $this->input->post('guardian_mobile_' . $i, true);
						$cdata['date'] = date('Y-m-d');
						$this->db->insert('tbl_message_for_birthday_to_guardian_student_list', $cdata);
					}
					$i++;
				}
				$sms_info = array();
				$sms_info['message'] = $this->input->post('template_body', true);
				$sms_info['numbers'] = $num;
				$sms_info['is_bangla_sms'] = 1;
				$status = $this->Message->sms_send($sms_info);
				if ($status) {
					$sdata['message'] = "SMS send successfully.";
					$this->session->set_userdata($sdata);
					redirect("sms_sends/SendBirthDaySMS");
				} else {
					$sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
					$this->session->set_userdata($sdata);
					redirect("sms_sends/view_category_phoneBook_list");
				}
			} else {
				$sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/SendBirthDaySMS");
			}
		}
	}

	public function teacherBirthDaySMS()
	{
		if ($_POST) {
			/* echo '<pre>';
			print_r($_POST);
			die(); */
			$data = array();
			$data['birthDate'] = $this->input->post('birthDate', true);
			$data['template_id'] = $this->input->post('template_id', true);
			$data['template_body'] = $this->input->post('template_body', true);
			$data['date'] = date('Y-m-d H:i:s');
			if ($this->db->insert('tbl_message_for_birthday', $data)) {
				$insert_id = $this->db->insert_id();
				$loop_time = $this->input->post('loop_time', true);
				$i = 0;
				$num = "";
				while ($i < $loop_time) {
					$is_send = $this->input->post('is_send_' . $i, true);
					if (isset($is_send) && $is_send != '') {

						//sms send data ready
						$num = $this->Admin_login->generateNumberlist($num, $this->input->post('teacher_mobile_' . $i, true));
						//sms send data ready end

						$cdata = array();
						$cdata['to_message_for_birthday_id'] = $insert_id;
						$cdata['teacher_id'] = $this->input->post('teacher_id_' . $i, true);
						$cdata['mobile_number'] = $this->input->post('teacher_mobile_' . $i, true);
						$cdata['date'] = date('Y-m-d');
						$this->db->insert('tbl_message_for_birthday_to_teacher', $cdata);
					}
					$i++;
				}
				$sms_info = array();
				$sms_info['message'] = $this->input->post('template_body', true);
				$sms_info['numbers'] = $num;
				$status = $this->Message->sms_send($sms_info);
				if ($status) {
					$sdata['message'] = "SMS send successfully.";
					$this->session->set_userdata($sdata);
					redirect("sms_sends/SendBirthDaySMS");
				} else {
					$sdata['exception'] = "Sorry SMS Server Error. Please Try again later.";
					$this->session->set_userdata($sdata);
					redirect("sms_sends/SendBirthDaySMS");
				}
			} else {
				$sdata['exception'] = "Main Data Doesn't Save. Please Contact Your Vendor.";
				$this->session->set_userdata($sdata);
				redirect("sms_sends/SendBirthDaySMS");
			}
		}
	}






}
