<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Accounts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Account', 'Admin_login'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
    }


    public function index()
    {
        $data = array();
        $data['title'] = 'Finance -> Accounts';
        $data['heading_msg'] = "Finance -> Accounts";
        $config = $this->db->query("SELECT accounts_module_type FROM tbl_config")->result_array();
        $data['accounts_module_type'] = $config[0]['accounts_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function get_income_category_for_list()
    {
        $income_category = $this->db->query("SELECT * FROM tbl_ba_income_category")->result_array();
        echo json_encode($income_category);
    }

    public function get_deposit_method_for_list()
    {
        $income_category = $this->db->query("SELECT * FROM tbl_ba_deposit_method")->result_array();
        echo json_encode($income_category);
    }

    public function get_expense_category_for_list()
    {
        $expense_category = $this->db->query("SELECT * FROM tbl_ba_expense_category")->result_array();
        echo json_encode($expense_category);
    }
}
