<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Case_sub_type_report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login','Advocate'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index($flag=null)
    {
      $lang=$this->session->userdata('site_lang');
      $user_info = $this->session->userdata('user_info');
      $data = array();
      $data['title'] = $this->lang->line('case_sub_type').' '.$this->lang->line('report');
      $data['heading_msg'] = $this->lang->line('case_sub_type').' '.$this->lang->line('report');
      $casetypeid=0;
  if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
         $casetypeid=$this->input->post("case_type_id");
          $this->load->library('numbertowords');
          $SchoolInfo = $this->Admin_login->fetReportHeader();
          $data['HeaderInfo'] = $SchoolInfo;
          $data['HeaderInfo'] = $SchoolInfo;
          $data['from_date'] = $this->input->post("from_date");
          $data['to_date'] = $this->input->post("to_date");
          $data['district_id'] = $this->input->post("district_id");
          $data['court_type_id'] = $this->input->post("court_type_id");
          $data['court_id'] = $this->input->post("court_id");
          $data['case_type_id'] = $casetypeid;
          $data['case_sub_type_id'] = $this->input->post("case_sub_type_id");
          $data['case_status_id'] = $this->input->post("case_status_id");
          $data['page_type'] = 'landscape';
          $data['paper_size'] = 'A4';
          $cond = array();
          $cond['from_date'] = $this->input->post("from_date");
          $cond['to_date'] = $this->input->post("to_date");
          $cond['district_id'] = $this->input->post("district_id");
          $cond['court_type_id'] = $this->input->post("court_type_id");
          $cond['court_id'] = $this->input->post("court_id");
          $cond['case_type_id'] = $casetypeid;
          $cond['case_sub_type_id'] = $this->input->post("case_sub_type_id");
          $cond['case_status_id'] = $this->input->post("case_status_id");
          $data['idata'] = $this->Advocate->get_case_sub_type_list_report(0, 0, $cond);
          $data['language'] = $lang;

    //sub view pass
    $data['report_header'] = $this->load->view('report_content/inside_report_header', $data, true);
    $data['report_footer'] = $this->load->view('report_content/inside_report_footer', $data, true);

    $data['report'] = $this->load->view('case_sub_type_report/report', $data, true);
      }
      if (isset($_POST['pdf_download'])) {
          $data['is_pdf'] = 1;
          //Dom PDF
          $this->load->library('mydompdf');
          $html = $this->load->view('case_sub_type_report/report', $data, true);
      //    print_r($html);die;
          $this->mydompdf->createPDF($html, 'ATTENDANCE REPORT', true, 'A4', 'landscape');
      //Dom PDF
      } else {

          $data['districts'] = $this->Advocate->get_districts_list_by_lang_and_districtid($lang,$user_info[0]->client_user[0]->district_id);
          $data['language']=$lang;
          $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id(0);
          $data['courts'] = $this->Advocate->get_court_dropdown_list(0);
          $data['case_type'] = $this->Advocate->get_case_type_dropdown_list_by_language($lang);
          $data['case_sub_type'] = $this->Advocate->get_case_sub_type_dropdown_list_by_language($casetypeid,$lang);
          $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
          $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
          $data['maincontent'] = $this->load->view('case_sub_type_report/index', $data, true);
          $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
      }

    }
}
