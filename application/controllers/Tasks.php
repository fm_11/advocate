<?php

class Tasks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('tasks');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('tasks/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_all_tasks_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['tasks'] = $this->Cases->get_all_tasks_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('tasks/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['subject']=$this->input->post('subject');
            $data['start_date']=date("Y-m-d", strtotime($this->input->post('start_date')));
            $data['deadline_date']=date("Y-m-d", strtotime($this->input->post('deadline_date')));//$this->input->post('deadline_date');
            $data['status_id']=$this->input->post('status_id');
            $data['priority_id']=$this->input->post('priority_id');
            $data['related_to']=$this->input->post('related_to');
            $data['case_id']=$this->input->post('case_id');
            $data['description']=$this->input->post('description');
            $data['client_user_id']=$this->input->post('client_user_id[0]');
            // print_r($data['client_user_id']);
            // die();
            $data['process_date']=$this->input->post('process_date');

            if ($this->Cases->add_tasks($data)) {
                $sdata['message'] = "Information Successfully Added";
                $this->session->set_userdata($sdata);
                redirect("tasks/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("tasks/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('tasks');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['status'] = $this->Cases->get_status_list();
        $data['priority'] = $this->Cases->get_priority_list();
        $data['client_user'] = $this->Cases->get_client_user_list();
        // $data['case'] = $this->Cases->get_case_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('tasks/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $data['id']=$id;
          $data['subject']=$this->input->post('subject');
          $data['start_date']=date("Y-m-d", strtotime($this->input->post('start_date')));
          $data['deadline_date']=date("Y-m-d", strtotime($this->input->post('deadline_date')));
          $data['status_id']=$this->input->post('status_id');
          $data['priority_id']=$this->input->post('priority_id');
          $data['related_to']=$this->input->post('related_to');
          $data['case_id']=$this->input->post('case_id');
          $data['description']=$this->input->post('description');
          $data['client_user_id']=$this->input->post('client_user_id[0]');
          $data['process_date']=$this->input->post('process_date');
          if ($this->Cases->edit_tasks($data, $id)) {
              $sdata['message'] = "Information Successfully updated.";
              $this->session->set_userdata($sdata);
              redirect("tasks/index");
          } else {
              $sdata['exception'] = "Information could not add";
              $this->session->set_userdata($sdata);
              redirect("tasks/index");
          }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('clients');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_data'] = $this->Cases->read_tasks($id);
        // print_r($data['row_data']);
        // die;
        $data['status'] = $this->Cases->get_status_list($data['row_data']->status_id);
        $data['priority'] = $this->Cases->get_priority_list($data['row_data']->priority_id);
        $data['client_user'] = $this->Cases->get_client_user_list($data['row_data']->client_user_id);
        // $data['case'] = $this->Cases->get_case_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('tasks/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Cases->delete_tasks($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("tasks/index");
    }

    public function ajax_related_to()
    {
        $related_to = $_GET['related_to'];
        $data["dropdown_list"]= $this->Cases->get_districts_list_by_country($country_id);
        $this->load->view('tasks/dropdown_list', $data);
    }
}
