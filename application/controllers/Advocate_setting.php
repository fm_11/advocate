<?php

class Advocate_setting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        if($_POST){

          $data_save =array();
          $data_save['id'] = $this->input->post('id', true);
          $name=$this->input->post('name');
          $designation=$this->input->post('designation');

          $data_save['designation']=$designation;
          $data_save['name']=$name;
          $data_save['address']=$this->input->post('address');
          $data_save['mobile']=$this->input->post('mobile');
          $data_save['advance_day']=$this->input->post('advance_day');
          $this->db->where('id', $data_save['id']);
          $this->db->update('ad_advocate_setting', $data_save);
          $sdata['message'] = $this->lang->line('edit_success_message');
          $this->session->set_userdata($sdata);
        }
      $data = array();
      $data['title'] = $this->lang->line('advocate').' '.$this->lang->line('setting');
      $data['heading_msg'] =  $this->lang->line('advocate').' '.$this->lang->line('setting');
      $setting=$this->db->query("SELECT * FROM ad_advocate_setting")->result_array();
     if(count($setting)<1)
     {
       redirect("advocate_setting/add");
     }
      $data['setting'] = $setting;
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('advocate_setting/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add()
    {
        $user_info = $this->session->userdata('user_info');
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $data = array();
            $name=$this->input->post('name');
            $designation=$this->input->post('designation');

            $data['designation']=$designation;
            $data['name']=$name;
            $data['address']=$this->input->post('address');
            $data['mobile']=$this->input->post('mobile');
            $data['advance_day']=$this->input->post('advance_day');
            if ($this->Cases->add_advocate_setting($data)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("advocate_setting/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("advocate_setting/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('advocate').' '.$this->lang->line('setting');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['language']=$lang;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('advocate_setting/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
