<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Case_file_download_report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login','Advocate'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
       date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['title'] = $this->lang->line('download_the_case_file');
        $data['heading_msg'] = $this->lang->line('download_the_case_file');
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $data['HeaderInfo'] = $SchoolInfo;
            $data['from_date'] = $this->input->post("from_date");
            $data['to_date'] = $this->input->post("to_date");
            $data['case_no'] = $this->input->post("case_no");
            $data['district_id'] = $this->input->post("district_id");
            $data['court_type_id'] = $this->input->post("court_type_id");
            $data['court_id'] = $this->input->post("court_id");
            $data['client_user_id'] = $this->input->post("client_user_id");
            $data['case_master_id'] = $this->input->post("case_master_id");

            $cond = array();
            $cond['from_date'] = $this->input->post("from_date");
            $cond['to_date'] = $this->input->post("to_date");
            $cond['case_no'] = $this->input->post("case_no");
            $cond['district_id'] = $this->input->post("district_id");
            $cond['court_type_id'] = $this->input->post("court_type_id");
            $cond['court_id'] = $this->input->post("court_id");
            $cond['client_user_id'] = $this->input->post("client_user_id");
            $cond['case_master_id'] = $this->input->post("case_master_id");
            $data['attendaces'] = $this->Advocate->get_all_attendance_list_by_date_range(0, 0, $cond);
            $data['post_data'] = $_POST;
            $data['report'] = $this->load->view('Case_file_download_report/report', $data, true);

        }
        else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
          //  $data['districts'] = $this->Advocate->get_all_districts_list_by_lang($lang);
            $data['case_list'] = $this->Advocate->get_all_case_list_for_dropdown();
            $data['language']=$lang;
          //  $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id(0);
          //  $data['courts'] = $this->Advocate->get_court_dropdown_list(0);
            $data['client_user'] = $this->Advocate->get_client_user_dropdown_list();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('Case_file_download_report/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
