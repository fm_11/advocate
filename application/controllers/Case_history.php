<?php

class Case_history extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate','Admin_login','Message'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index($id=NULL)
    {

        $data = array();
        $data['title'] = $this->lang->line('case_history_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $case_master = $this->input->post("case_master_id");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
            $cond['case_master'] = $case_master;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
            $cond['case_master'] = $id;
        }
        $notifId =  $this->uri->segment(4);
        if(!empty($notifId))
        {
          $user_info = $this->session->userdata('user_info');
          $notice_data  = array();
          $notice_data['id']=$notifId;
          $notice_data['it_seen']=1;
          $notice_data['seen_user_id']=$user_info[0]->client_user[0]->id;
          $notice_data['seen_time']=date('Y-m-d H:i:s');
          $this->Advocate->update_notification($notice_data,$notifId);
        }


        $this->load->library('pagination');
        $config['base_url'] = site_url('case_history/index/');

        $data['case_historys'] = $this->Advocate->get_all_case_history_list(0, 0, $cond);
      //  $data['counter'] = (int)$this->uri->segment(3);
        $data['case_master_id']=$id;
        $data['language']=$this->session->userdata('site_lang');

        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_history/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add($id=NULL)
    {
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $data = array();
            //   echo '<pre>';
            // print_r($_POST);
            //
            // die();

             $case_master_id=$this->input->post('case_master_id');
             $business_on_date=date("Y-m-d", strtotime($this->input->post('bussines_on_date')));
             if($this->Advocate->checkifexist_case_history_status_closed_by_case_master_id($case_master_id))
             {
               $sdata['exception'] = "Sorry!.This case already closed.you can not add history.";
               $this->session->set_userdata($sdata);
               redirect("case_history/add/".$case_master_id);
             }

            if($this->Advocate->checkifexist_case_history_date_by_case_master_id($case_master_id,$business_on_date))
            {
              $sdata['exception'] = "This date '".$business_on_date."' already present for this case.";
              $this->session->set_userdata($sdata);
              redirect("case_history/add/".$case_master_id);
            }
            if($this->Advocate->checkifexist_advocate_setting())
            {
              $sdata['exception'] = $this->lang->line('advocate_setting_message');
              $this->session->set_userdata($sdata);
              redirect("case_history/add/".$case_master_id);
            }
            $case_no=$this->input->post('case_no');
            $case_sub_type_id=  $this->input->post('case_sub_type_id');
            if($this->Advocate->checkifexist_update_case_by_case_no($case_no,$case_sub_type_id,$case_master_id))
            {
              $sdata['exception'] = "This case no '".$case_no."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("case_history/add/".$case_master_id);
            }

            $user_info = $this->session->userdata('user_info');

            $data['case_master_id']=$case_master_id;
            $data['case_no']=$case_no;
            $data['case_status_id']=$this->input->post('case_status_id');
            $data['bussines_on_date']=$business_on_date;
            $data['remarks']=$this->input->post('remarks');
            $data['parties_name']=$this->input->post('parties_name');
            $data['reason_for_assignment']=$this->input->post('reason_for_assignment');
            $data['reason_for_next_date']=$this->input->post('reason_for_next_date');
            $data['court_type_id']=$this->input->post('court_type_id');
            $data['court_id']=$this->input->post('court_id');
            $data['district_id']=$this->input->post('district_id');
            $data['upazilla_id']=$this->input->post('upazilla_id');
            $data['is_sms']=$this->input->post('is_sms');
            $data['created_by']=$user_info[0]->client_user_id;
            $data['created_on']=date('Y-m-d H:i:s');
            $contact_info = $this->Advocate->get_client_info_by_case_master_id($case_master_id);
            if($data['is_sms']==1)
            {
              if (!$this->Admin_login->validate_mobile_number($contact_info[0]['mobile'])) {
                $sdata['exception'] = "Your mobile number is invalid. please write the correct mobile number.";
                $this->session->set_userdata($sdata);
                redirect("case_history/add/".$case_master_id);
              }
            }
            if ($this->Advocate->add_case_history($data)) {
               $history_id = $this->db->insert_id();
              if($this->input->post('last_id')!=0)
              {
                $last_data = array();
                $last_data['hearing_date']=$business_on_date;
                $this->Advocate->edit_case_history($last_data,$this->input->post('last_id'));
              }
              $case_details=array();
              $case_details['case_status_id']=$this->input->post('case_status_id');
              $this->Advocate->edit_case_master($case_details,$case_master_id);

                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                    //sms send
                if ($data['is_sms']==1) {
                  $case_name=$this->Advocate->get_case_name_with_case_sub_type_by_language_and_caseid($lang,$case_master_id,$case_no);
                  $setting=$this->Advocate->read_advocate_setting();
                  $date = date("d-m-Y", strtotime($data['bussines_on_date']));
                  $user_name = $contact_info[0]['user_name'];
                  $client_id = $contact_info[0]['client_id'];
                //    if ($this->Admin_login->validate_mobile_number($contact_info[0]['mobile'])) {
                    $template_id = 0;
              			$template_body = $case_name." আপনার পরবর্তী হাজিরা তারিখ: $date ($setting->name) ধন্যবাদ।";
              			$send_from = "HS";
              			$loop_time = $this->input->post('loop_time', true);
              			$numbers = array();
              			$i = 0;
              			$numbers_with_student_id = array();
                    //sms send data ready
                    $number =  trim($contact_info[0]['mobile']);
                  //  if($this->Admin_login->validate_mobile_number($number)){
                      $numbers[] = $number;
                  //  }
                    $numbers_with_student_id[$number]['client_id'] = $client_id;
                    $numbers_with_student_id[$number]['number'] = $number;
                    $numbers_with_student_id[$number]['case_history_id'] = $history_id;


                    //sms send data ready
              			$status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
              			if (strtolower($status['status']) == 'success') {
              				$sdata['message'] = "SMS send successfully";
              				$this->session->set_userdata($sdata);
              				redirect("case_history/index/".$case_master_id);
              			} else {
              				$sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
              				$this->session->set_userdata($sdata);
              				redirect("case_history/add/".$case_master_id);
              			}
                    //sms send
              //    }
                }

                redirect("case_history/index/".$case_master_id);
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
              redirect("case_history/add/".$case_master_id);
            }
        }

        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['case_master_id']=$id;
        $data['language']=$lang;
        $data['heading_msg'] = $data['title'] = $this->lang->line('case').' '.$this->lang->line('history');
        $data['action'] = 'add/';
	    	//$data['is_show_button'] = "index";
        $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
        $data['last_cast_history'] = $this->Advocate->get_case_last_history_details_by_mastr_id($id);
        $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id_and_language($data['last_cast_history']->district_id,$lang);
        $data['courts'] = $this->Advocate->get_court_list_by_district_id_and_court_type_id_and_language($data['last_cast_history']->district_id,$data['last_cast_history']->court_type_id,$lang);

        $data['district_id'] = $this->Advocate->get_all_districts_list_by_lang($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_history/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function edit($id=NULL)
    {
          $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $data = array();

            $case_master_id=$this->input->post('case_master_id');
            $business_on_date=date("Y-m-d", strtotime($this->input->post('bussines_on_date')));
            $key_id=$this->input->post('id');
            if($this->Advocate->checkifexist_update_case_history_date_by_case_master_id($case_master_id,$business_on_date,$key_id))
            {
              $sdata['exception'] = "This date '".$business_on_date."' already present for this case.'";
              $this->session->set_userdata($sdata);
              redirect("case_history/edit/".$key_id);
            }
            $case_no=$this->input->post('case_no');
            $case_sub_type_id=  $this->input->post('case_sub_type_id');
            if($this->Advocate->checkifexist_update_case_by_case_no($case_no,$case_sub_type_id,$case_master_id))
            {
              $sdata['exception'] = "This case no '".$case_no."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("case_history/edit/".$key_id);
            }


            $user_info = $this->session->userdata('user_info');
            $data['id']=$key_id;
            $data['case_no']=$case_no;
            $data['case_master_id']=$case_master_id;
            $data['case_status_id']=$this->input->post('case_status_id');
            $data['bussines_on_date']=$business_on_date;
            $data['remarks']=$this->input->post('remarks');
            $data['parties_name']=$this->input->post('parties_name');
            $data['reason_for_assignment']=$this->input->post('reason_for_assignment');
            $data['reason_for_next_date']=$this->input->post('reason_for_next_date');
            $data['court_type_id']=$this->input->post('court_type_id');
            $data['court_id']=$this->input->post('court_id');
            $data['district_id']=$this->input->post('district_id');
            $data['upazilla_id']=$this->input->post('upazilla_id');
            $data['updated_by']=$user_info[0]->client_user_id;
            $data['is_sms']=$this->input->post('is_sms');
            $data['updated_on']=date('Y-m-d H:i:s');

            $contact_info = $this->Advocate->get_client_info_by_case_master_id($case_master_id);
            if($data['is_sms']==1)
            {
              if (!$this->Admin_login->validate_mobile_number($contact_info[0]['mobile'])) {
                $sdata['exception'] = "Your mobile number is invalid. please write the correct mobile number.";
                $this->session->set_userdata($sdata);
                redirect("case_history/edit/".$key_id);
              }
              if($this->Advocate->checkifexist_advocate_setting())
              {
                $sdata['exception'] = $this->lang->line('advocate_setting_message');
                $this->session->set_userdata($sdata);
                  redirect("case_history/edit/".$key_id);
              }
            }

            if ($this->Advocate->edit_case_history($data,$key_id)) {
                $last_data = array();
                $last_data['hearing_date']=$business_on_date;
                $this->Advocate->edit_case_history($last_data,$this->input->post('last_id'));
                $case_details=array();
                $case_details['case_status_id']=$this->input->post('case_status_id');
                $this->Advocate->edit_case_master($case_details,$case_master_id);

                $sdata['message'] = $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                //sms send
            if ($data['is_sms']==1) {
               $case_name=$this->Advocate->get_case_name_with_case_sub_type_by_language_and_caseid($lang,$case_master_id,$case_no);
               $setting=$this->Advocate->read_advocate_setting();
               $date = date("d-m-Y", strtotime($data['bussines_on_date']));
               $user_name = $contact_info[0]['user_name'];
               $client_id = $contact_info[0]['client_id'];
                //if ($this->Admin_login->validate_mobile_number($contact_info[0]['mobile'])) {
                $template_id = 0;
                $template_body = $case_name." আপনার পরবর্তী হাজিরা তারিখ: $date ($setting->name) ধন্যবাদ।";
                $send_from = "HS";
                $loop_time = $this->input->post('loop_time', true);
                $numbers = array();
                $i = 0;
                $numbers_with_student_id = array();
                //sms send data ready
                $number =  trim($contact_info[0]['mobile']);
            //    if($this->Admin_login->validate_mobile_number($number)){
                  $numbers[] = $number;
              //  }
                $numbers_with_student_id[$number]['client_id'] = $client_id;
                $numbers_with_student_id[$number]['number'] = $number;
                $numbers_with_student_id[$number]['case_history_id'] = $key_id;
              //   echo '<pre>';
              // print_r($template_body);
              //
              // die();
                //sms send data ready
                $status = $this->Message->bulkSMSSend($numbers, $template_id, $template_body, $send_from,$numbers_with_student_id);
                if (strtolower($status['status']) == 'success') {
                  $sdata['message'] = "SMS send successfully";
                  $this->session->set_userdata($sdata);
                  redirect("case_history/index/".$case_master_id);
                } else {
                  $sdata['exception'] = "SMS Send Failed. Error Code - " . $status['status_code'] . ' (' . $status['error_message'] .')';
                  $this->session->set_userdata($sdata);
                  redirect("case_history/add/".$case_master_id);
                }
                //sms send
              //}
            }
                redirect("case_history/index/".$case_master_id);
            } else {
                $sdata['exception'] = $this->lang->line('edit_error_message');
                $this->session->set_userdata($sdata);
              redirect("case_history/edit/".$key_id);
            }
        }


        $data = array();
        $case_history=$this->Advocate->get_case_history_details_by_id($id);
        $data['case_master_id']=$case_history->case_master_id;
        $data['heading_msg'] = $data['title'] = $this->lang->line('case').' '.$this->lang->line('history');
        $data['action'] = 'edit/';
        $data['language']=$lang;
	    	$data['last_cast_history'] = $this->Advocate->get_case_last_history_details_by_master_and_key_id($id,$case_history->case_master_id);
        $data['case_status'] = $this->Advocate->get_case_status_dropdown_list_by_language($lang);
        $data['case_history'] = $case_history;
        $data['courts_type'] = $this->Advocate->get_court_type_list_by_district_id_and_language($case_history->district_id,$lang);
        $data['courts'] = $this->Advocate->get_court_list_by_district_id_and_court_type_id_and_language($case_history->district_id,$case_history->court_type_id,$lang);
        $data['district_id'] = $this->Advocate->get_all_districts_list_by_lang($lang);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('case_history/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete()
    {
        $id =  $this->uri->segment(3);
        $mid =  $this->uri->segment(4);
        $data=$this->Advocate->read_case_history_by_key_id($id);
      	$last_cast_history = $this->Advocate->get_case_last_history_details_by_master_and_key_id($id,$mid);
        $this->Advocate->delete_case_history_by_id($id);
        $last_cast_history->hearing_date=NULL;
        $this->Advocate->edit_case_history($last_cast_history,$last_cast_history->id);
        $case_details=array();
        $case_details['case_status_id']=$data[0]['case_status_id'];
        $this->Advocate->edit_case_master($case_details,$data[0]['case_master_id']);
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('case_history/index/'.$mid);

    }
}
