<?php

class Upazilas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate','Cases'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['title'] = $this->lang->line('upazila_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $district_id = $this->input->post("district_id");
            $sdata['name'] = $name;
            $sdata['district_id'] = $district_id;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['district_id'] = $district_id;
        } else {
            $name = $this->session->userdata('name');
            $district_id = $this->session->userdata('district_id');
            $cond['name'] = $name;
            $cond['district_id'] = $district_id;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('upazilas/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_all_upazila_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['upazilas'] = $this->Cases->get_all_upazila_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['language'] =  $lang;
        $data['district_list'] =  $this->Cases->get_district_list_by_language($lang);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('upazilas/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function add()
    {
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
          $data = array();

          // $post_id=$this->input->post('id');
          // $data['id']=$post_id;
          $data['name']=$this->input->post('name');
          $data['bn_name']=$this->input->post('bn_name');
         $data['district_id']=$this->input->post('district_id');
          $data['serial_no']=$this->input->post('serial_no');

          if($this->Cases->add_upazila($data))
          {
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect("upazilas/index");
          }else{
            $sdata['exception'] =$this->lang->line('add_error_message');
            $this->session->set_userdata($sdata);
            redirect("upazilas/add/");
          }

        } else {
            $data = array();
            $data['title'] =$data['heading_msg'] = $this->lang->line('upazila').''.$this->lang->line('add');
            $data['action'] = 'add';
            $data['is_show_button'] = "index";
            $data['district_list'] = $this->Cases->get_district_list_by_language($lang);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('upazilas/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
    public function edit($id = null)
    {
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
          $data = array();
          $old_file="";
          $post_id=$this->input->post('id');
          $data['id']=$post_id;
          $data['name']=$this->input->post('name');
          $data['bn_name']=$this->input->post('bn_name');
        //  $data['division_id']=$this->input->post('division_id');
          $data['serial_no']=$this->input->post('serial_no');

          if($this->Cases->edit_upazila($data,$post_id))
          {
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("upazilas/index");
          }else{
            $sdata['exception'] = "Information could not updated";
            $this->session->set_userdata($sdata);
            redirect("upazilas/edit/".$post_id);
          }

        } else {
            $data = array();
            $data['title'] =$data['heading_msg'] = $this->lang->line('upazila').''.$this->lang->line('update');
            $data['action'] = 'edit';
            $data['is_show_button'] = "index";
            $data['upazila']=$this->Cases->read_upazila_by_upazila_id($id);
            $data['district_list'] = $this->Cases->get_district_list_by_language($lang);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('upazilas/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

}
