<?php

class Email_setting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate'));
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        if($_POST){

          $data_save =array();
          $data_save['id'] = $this->input->post('id', true);
          $from_email=$this->input->post('from_email');
          $txt_password=$this->input->post('txt_password');

          $data_save['from_email']=$from_email;
          if($txt_password!='ukildoptor')
          {
            $data_save['password']=$txt_password;
          }
          $data_save['subject']=$this->input->post('subject');
          $data_save['message_body']=$this->input->post('message_body');
          $data_save['mail_signatures']=$this->input->post('mail_signatures');
          $data_save['facebook']=$this->input->post('facebook');
          $data_save['linkedin']=$this->input->post('linkedin');
          $data_save['twitter']=$this->input->post('twitter');
          $data_save['instagram']=$this->input->post('instagram');
          $data_save['youtube']=$this->input->post('youtube');
          $this->db->where('id', $data_save['id']);
          $this->db->update('tbl_mail_configuration', $data_save);
          $sdata['message'] = $this->lang->line('edit_success_message');
          $this->session->set_userdata($sdata);
            redirect("email_setting/index");
        }
      $data = array();
      $data['title'] = $this->lang->line('email').' '.$this->lang->line('setting');
      $data['heading_msg'] =  $this->lang->line('advocate').' '.$this->lang->line('setting');
      $setting=$this->db->query("SELECT * FROM tbl_mail_configuration")->result_array();
     if(count($setting)<1)
     {
       redirect("email_setting/add");
     }
      $data['setting'] = $setting;
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('email_setting/index', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add()
    {
        $user_info = $this->session->userdata('user_info');
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $data_save = array();
            $from_email=$this->input->post('from_email');
            $txt_password=$this->input->post('txt_password');
            $data_save['from_email']=$from_email;
            $data_save['password']=$txt_password;
            $data_save['subject']=$this->input->post('subject');
            $data_save['message_body']=$this->input->post('message_body');
            $data_save['mail_signatures']=$this->input->post('mail_signatures');
            $data_save['facebook']=$this->input->post('facebook');
            $data_save['linkedin']=$this->input->post('linkedin');
            $data_save['twitter']=$this->input->post('twitter');
            $data_save['instagram']=$this->input->post('instagram');
            $data_save['youtube']=$this->input->post('youtube');

            if ($this->Cases->add_email_setting($data_save)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("email_setting/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("email_setting/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('email').' '.$this->lang->line('setting');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['language']=$lang;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('email_setting/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
