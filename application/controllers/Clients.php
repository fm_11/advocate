<?php

class Clients extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('clients');
        $lang=$this->session->userdata('site_lang');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('clients/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_all_clients_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['clients'] = $this->Cases->get_all_clients_list(0, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['language']=$lang;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('clients/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $user_info = $this->session->userdata('user_info');
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
            $data = array();
            $mobile=$this->input->post('mobile');
            $name=$this->input->post('first_name');
            if($this->Cases->checkifexist_clients_by_mobile($mobile,$name))
            {
              $sdata['exception'] = "This name '".$name."' and mobile number'".$mobile."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("clients/add");
            }
            $data['mobile']=$mobile;
            $data['first_name']=$name;
            $data['last_name']=$this->input->post('last_name');
            $data['gender']=$this->input->post('gender');
            $data['email']=$this->input->post('email');
            $data['alternate_mobile_no']=$this->input->post('alternate_mobile_no');
            $data['address']=$this->input->post('address');
            $data['country_id']=1;//$this->input->post('country_id');
            $data['reference_name']=$this->input->post('reference_name');
            $data['reference_mobile']=$this->input->post('reference_mobile');
            $data['district_id']=$this->input->post('district_id');
            $data['upazilas_id']=$this->input->post('upazilas_id');
            $data['total_person']=$this->input->post('total_person');
            if ($this->Cases->add_clients($data)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("clients/add");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("clients/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('clients');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['language']=$lang;
        $data['district_id']= $user_info[0]->client_user[0]->district_id;
        $data['upazilla_id'] = $user_info[0]->client_user[0]->upazilas_id;
        $data['countries'] = $this->Cases->get_country_list();
        $data['districts'] = $this->Advocate->get_all_districts_list_by_lang($lang);
        $data['upazilas'] = $this->Advocate->get_upazilas_list_by_district_and_lang($lang,$user_info[0]->client_user[0]->district_id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('clients/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        $user_info = $this->session->userdata('user_info');
          $lang=$this->session->userdata('site_lang');
        if ($_POST) {
          $data = array();
          $mobile=$this->input->post('mobile');
          $name=$this->input->post('first_name');
          if($this->Cases->checkifexist_update_clients_by_mobile($mobile,$name,$id))
          {
            $sdata['exception'] = "This name '".$name."' and mobile number'".$mobile."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("clients/edit/".$id);
          }
          $data['id']=$id;
          $data['mobile']=$mobile;
          $data['first_name']=$name;
          $data['last_name']=$this->input->post('last_name');
          $data['gender']=$this->input->post('gender');
          $data['email']=$this->input->post('email');
          $data['alternate_mobile_no']=$this->input->post('alternate_mobile_no');
          $data['address']=$this->input->post('address');
          $data['country_id']=1;//$this->input->post('country_id');
          $data['reference_name']=$this->input->post('reference_name');
          $data['reference_mobile']=$this->input->post('reference_mobile');
          $data['district_id']=$this->input->post('district_id');
          $data['upazilas_id']=$this->input->post('upazilas_id');
          $data['total_person']=$this->input->post('total_person');
          if ($this->Cases->edit_clients($data, $id)) {
              $sdata['message'] = $this->lang->line('edit_success_message');
              $this->session->set_userdata($sdata);
                  redirect("clients/index");
          } else {
              $sdata['exception'] = $this->lang->line('edit_error_message');
              $this->session->set_userdata($sdata);

                redirect("clients/edit/".$id);
          }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('clients');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_data'] = $this->Cases->read_clients($id);
        // print_r($data['row_data']);
        // die;
        $data['language']=$lang;
        $data['countries'] = $this->Cases->get_country_list();
        $data['districts'] =  $this->Advocate->get_all_districts_list_by_lang($lang);
        $data['upazilas'] =$this->Advocate->get_upazilas_list_by_district_and_lang($lang,$data['row_data']->district_id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('clients/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
      if($this->Advocate->check_client_dependency_by_client_id($id))
      {
        $sdata['exception'] = $this->lang->line('client_delete_msg');
        $this->session->set_userdata($sdata);
          redirect("clients/index");
      }
        $this->Cases->delete_clients($id);
        $sdata['message'] =$this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("clients/index");
    }
    public function updateMsgClientsStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_clients', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }
    public function ajax_district_by_country()
    {
        $country_id = $_GET['country_id'];
        $data["dropdown_list"]= $this->Cases->get_districts_list_by_country($country_id);
        $this->load->view('clients/dropdown_list', $data);
    }
    public function ajax_upazilas_by_district()
    {
        $district_id = $_GET['district_id'];
        $data["dropdown_list"]= $this->Advocate->get_upazilas_list_by_district_and_lang($lang,$district_id);
        $this->load->view('clients/dropdown_list', $data);
    }
}
