<?php

class Tax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Cases','Advocate'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('tax');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $status = $this->input->post("status");
            $sdata['name'] = $name;
            $sdata['status'] = $status;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['status'] = $status;
        } else {
            $name = $this->session->userdata('name');
            $status = $this->session->userdata('status');
            $cond['name'] = $name;
            $cond['status'] = $status;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('tax/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Cases->get_tax_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['tax'] = $this->Cases->get_tax_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('tax/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $name=$this->input->post('name');
            $percentage=$this->input->post('percentage');
            $data['name']=$name;
            $data['percentage']=$percentage;
            $data['is_active']=$this->input->post('is_active');
            if ($percentage<=0) {
              $sdata['exception'] = "This value '".$percentage."' is not acceptable.";
              $this->session->set_userdata($sdata);
              redirect("tax/add/");
            }
            if($this->Cases->checkifexist_tax_by_name($name))
            {
              $sdata['exception'] = "This name '".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("tax/add");
            }
            if ($this->Cases->add_tax($data)) {
                $sdata['message'] = "Information Successfully Added";
                $this->session->set_userdata($sdata);
                redirect("tax/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("tax/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('tax');
        $data['action'] = 'add';
	    	$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('tax/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $name=$this->input->post('name');
          $percentage=$this->input->post('percentage');
          $data['name']=$name;
          $data['id']=$id;
          $data['percentage']=$percentage;
          $data['is_active']=$this->input->post('is_active');
          if ($percentage<=0) {
            $sdata['exception'] = "This value '".$percentage."' is not acceptable.";
            $this->session->set_userdata($sdata);
            redirect("tax/edit/".$id);
          }
          // print_r($id);
          // die()
          if($this->Cases->checkifexist_update_tax_by_name($name,$id))
          {
            $sdata['exception'] = "This name '".$name."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("tax/edit/".$id);
          }
            if ($this->Cases->edit_tax($data, $id)) {
                $sdata['message'] = "Information Successfully updated.";
                $this->session->set_userdata($sdata);
                redirect("tax/index");
            } else {
                $sdata['exception'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("tax/index");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('tax');
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row'] = $this->Cases->read_tax($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('tax/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Cases->delete_tax($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("tax/index");
    }
    public function updateMsgTaxStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $id;
        if ($status == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('ad_tax', $data);
        if ($status == 0) {
            echo '<a title="Active" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a title="Inactive" class="deleteTag" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }
}
