<?php

class Districts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Advocate','Cases'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (!empty($user_info)) {
          $client_user_id = $user_info[0]->client_user[0]->id;
          $this->notification = $this->Advocate->get_notification($client_user_id);
        }
    }

    public function index()
    {
        $lang=$this->session->userdata('site_lang');
        $data = array();
        $data['title'] = $this->lang->line('district_list');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $division_id = $this->input->post("division_id");
            $sdata['name'] = $name;
            $sdata['division_id'] = $division_id;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
            $cond['division_id'] = $division_id;
        } else {
            $name = $this->session->userdata('name');
            $division_id = $this->session->userdata('division_id');
            $cond['name'] = $name;
            $cond['division_id'] = $division_id;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('districts/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Advocate->get_all_district_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['districts'] = $this->Advocate->get_all_district_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['language'] =  $lang;
        $data['division_list'] =  $this->Cases->get_division_list_by_language($lang);
      //  $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('districts/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        $lang=$this->session->userdata('site_lang');
        if ($_POST) {
          $data = array();
          $old_file="";
          $post_id=$this->input->post('id');
          $data['id']=$post_id;
          $data['name']=$this->input->post('name');
          $data['bn_name']=$this->input->post('bn_name');
        //  $data['division_id']=$this->input->post('division_id');
          $data['serial_no']=$this->input->post('serial_no');

          if($this->Cases->edit_districts($data,$post_id))
          {
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("districts/index");
          }else{
            $sdata['exception'] = "Information could not updated";
            $this->session->set_userdata($sdata);
            redirect("districts/edit/".$post_id);
          }

        } else {
            $data = array();
            $data['title'] =$data['heading_msg'] = $this->lang->line('district').''.$this->lang->line('update');
            $data['action'] = 'edit';
            $data['is_show_button'] = "index";
            $data['districts']=$this->Cases->read_districts_by_district_id($id);
            $data['division_list'] = $this->Cases->get_division_list_by_language($lang);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('districts/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

}
