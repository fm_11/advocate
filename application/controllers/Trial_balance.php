
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trial_balance extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Account'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = "Trial Balance";
        $data['heading_msg'] = "Trial Balance";
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            $data['class_id'] = 'all';

            $date_for_opening = date('Y-m-d', strtotime('-1 day', strtotime($from_date)));
            $assetData = $this->Account->getAllAssetHeadWithOpeningHead();
            $incomeData = $this->Account->getAllIncomeDataForBalanceSheet($date_for_opening);
            $expenseData = $this->Account->getAllExpenseDataForBalanceSheet($date_for_opening);
            $incomeDataForDuringThePeriod = $this->Account->getAllIncomeTransactionByDateRange($from_date, $to_date);
            $expensDataForDuringThePeriod = $this->Account->getAllExpenseTransactionByDateRange($from_date, $to_date);
            $return_data = array();
            $i = 0;
            foreach ($assetData as $asset) {
                $return_data[$i]['asset_id'] = $asset['asset_id'];
                $return_data[$i]['name'] = $asset['name'];

                $opening_balance = $asset['opening_balance'];

                $total_income = 0;
                if (isset($incomeData[$asset['asset_id']])) {
                    $total_income = $incomeData[$asset['asset_id']]['total_amount'];
                }

                $total_expense = 0;
                if (isset($expenseData[$asset['asset_id']])) {
                    $total_expense = $expenseData[$asset['asset_id']]['total_amount'];
                }
                $return_data[$i]['opening_balance_for_this_period'] = ($opening_balance + $total_income) - $total_expense;

                $return_data[$i]['income_for_this_period'] = 0;
                if (isset($incomeDataForDuringThePeriod[$asset['asset_id']])) {
                    $return_data[$i]['income_for_this_period'] = $incomeDataForDuringThePeriod[$asset['asset_id']]['total_amount'];
                }

                $return_data[$i]['expense_for_this_period'] = 0;
                if (isset($expensDataForDuringThePeriod[$asset['asset_id']])) {
                    $return_data[$i]['expense_for_this_period'] = $expensDataForDuringThePeriod[$asset['asset_id']]['total_amount'];
                }


                $return_data[$i]['closing_balance_for_this_period'] = ($return_data[$i]['opening_balance_for_this_period'] + $return_data[$i]['income_for_this_period']) - $return_data[$i]['expense_for_this_period'];


                $i++;
            }

            $data['income_data'] = $this->Student_fee->getCollectionDetailsStandard($data);
            $data['expense_data'] = $this->Student_fee->getExpenseDataForTrailBalance($data);


            $data['other_deposit_info'] = $this->db->query("SELECT SUM(dd.`amount`) AS total_amount, ic.`name` AS income_cat_name FROM `tbl_ba_deposit_details` AS dd
INNER JOIN `tbl_ba_deposit` AS d ON d.`id` = dd.`deposit_id`
INNER JOIN `tbl_ba_income_category` AS ic ON ic.`id` = dd.`income_category_id`
WHERE d.`date` BETWEEN '$from_date' AND '$to_date'
GROUP BY dd.`income_category_id`")->result_array();
            // echo '<pre>';
            // print_r($data['other_deposit_info']);
            // die;


            $fundTransferInData = $this->Account->getAllFundTransferByType('I', $from_date, $to_date);
            $fundTransferOutData = $this->Account->getAllFundTransferByType('O', $from_date, $to_date);

            $j = 0;
            $return_fund_transfer_data = array();
            foreach ($assetData as $assetForTransfer) {
                $return_fund_transfer_data[$j]['in'] = 0;
                $return_fund_transfer_data[$j]['out'] = 0;
                $return_fund_transfer_data[$j]['name'] = 'NA';
                if (isset($fundTransferInData[$assetForTransfer['asset_id']])) {
                    $return_fund_transfer_data[$j]['in'] = $fundTransferInData[$assetForTransfer['asset_id']]['total_amount'];
                    $return_fund_transfer_data[$j]['name'] = $fundTransferInData[$assetForTransfer['asset_id']]['name'];
                }

                if (isset($fundTransferOutData[$assetForTransfer['asset_id']])) {
                    $return_fund_transfer_data[$j]['out'] = $fundTransferOutData[$assetForTransfer['asset_id']]['total_amount'];
                    $return_fund_transfer_data[$j]['name'] = $fundTransferOutData[$assetForTransfer['asset_id']]['name'];
                }
                $j++;
            }

            $data['asset_head_data'] = $return_data;
            $data['return_fund_transfer_data'] = $return_fund_transfer_data;

            // echo '<pre>';
            // print_r($return_fund_transfer_data);
            // die;
            $data['report'] = $this->load->view('trial_balance/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('trial_balance/report', $data, true);
            $this->mydompdf->createPDF($html, 'TrialBalance', true, 'A4', 'portrait');
        //Dom PDF
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('trial_balance/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
