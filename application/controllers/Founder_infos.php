<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Founder_infos extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('common/insert_model', 'Admin_login','common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

public function index()
{
    $data = array();
    $data['title'] = 'Founder Information';
    $data['heading_msg'] = "Founder Information";
    $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
    $data['founder_info'] = $this->Admin_login->get_founder_info();
    $data['maincontent'] = $this->load->view('founder_infos/index', $data, true);
    $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
}

public function edit()
{
    $file_name = $_FILES['txtPhoto']['name'];
    if ($file_name != '') {
        $this->load->library('upload');
        $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
        $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
        foreach ($_FILES as $field => $file) {
            if ($file['error'] == 0) {
                $new_photo_name = "Contact_" . time() . "_" . date('Y-m-d') . "." . $extension;
                if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                    $data = array();
                    $data['id'] = $this->input->post('id', true);
                    $data['name'] = $this->input->post('name', true);
                    $data['years_of_active'] = $this->input->post('years_of_active', true);
                    $data['born'] = $this->input->post('born', true);
                    $data['address'] = $this->input->post('address', true);
                    $data['edu_qua'] = $this->input->post('edu_qua', true);
                    $data['occupation'] = $this->input->post('occupation', true);
                    $data['picture'] = $new_photo_name;
                    $this->Admin_login->update_founder_info($data);
                    $sdata['message'] = $this->lang->line('edit_success_message');
                    $this->session->set_userdata($sdata);
                    redirect("founder_infos/index");
                } else {
                    $sdata['exception'] = "Sorry Founder Info. Doesn't Updated !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("founder_infos/edit");
                }
            } else {
                $sdata['exception'] = "Sorry Founder Info. Doesn't Updated  !" . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("founder_infos/edit");
            }
        }
    } else {
        $data = array();
        $data['id'] = $this->input->post('id', true);
        $data['name'] = $this->input->post('name', true);
        $data['years_of_active'] = $this->input->post('years_of_active', true);
        $data['born'] = $this->input->post('born', true);
        $data['address'] = $this->input->post('address', true);
        $data['edu_qua'] = $this->input->post('edu_qua', true);
        $data['occupation'] = $this->input->post('occupation', true);
        $this->Admin_login->update_founder_info($data);
        $sdata['message'] = $this->lang->line('edit_success_message');
        $this->session->set_userdata($sdata);
        redirect("founder_infos/index");
    }
}

}

?>
