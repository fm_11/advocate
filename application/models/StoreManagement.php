<?php

class StoreManagement extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

	function addDataInTable($table,$data){
		return $this->db->insert($table, $data);
	}
}
