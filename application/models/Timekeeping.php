<?php

class Timekeeping extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        $this->load->model(array('Teacher_info', 'Student','Admin_login'));
        parent::__construct();
    }


    function getAttendanceTimeForAllTeacherStaff(){
		$returnList = array();
		$teachers_times = $this->db->query("SELECT * FROM `tbl_teacher_attendance_settings`")->result_array();
		$i = 0;
		foreach ($teachers_times as $row) {
			$returnList[$row['teacher_id']]['teacher_id'] = $row['teacher_id'];
			$returnList[$row['teacher_id']]['in_time'] = $row['in_time'];
			$returnList[$row['teacher_id']]['out_time'] = $row['out_time'];
			$returnList[$row['teacher_id']]['flexible_min_for_late'] = $row['flexible_min_for_late'];
			$i++;
		}
		return $returnList;
	}


    public function get_all_teacher_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_teacher_logins.*,t.name,teacher_index_no as index_no,tp.name as post_name');
        $this->db->from('tbl_teacher_logins');
        $this->db->join('tbl_teacher AS t', 'tbl_teacher_logins.teacher_id=t.id');
        $this->db->join('tbl_teacher_post AS tp', 't.post=tp.id');
        if (isset($value) && !empty($value) && $value['date'] != '') {
            $this->db->where('tbl_teacher_logins.date', $value['date']);
        }
        if (isset($value) && !empty($value['login_status']) && $value['login_status'] != '') {
            $this->db->where('tbl_teacher_logins.login_status', $value['login_status']);
        }
        $this->db->order_by("tbl_teacher_logins.date", "desc");
        $this->db->order_by("tbl_teacher_logins.teacher_id", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_teacher_staff_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_teacher_staff_logins.*,t.name,t.teacher_code,
                           teacher_index_no as index_no,tp.name as post_name');
        $this->db->from('tbl_teacher_staff_logins');
        $this->db->join('tbl_teacher AS t', 'tbl_teacher_staff_logins.teacher_id=t.id');
        $this->db->join('tbl_teacher_post AS tp', 't.post=tp.id');
        if (isset($value) && !empty($value['date']) && $value['date'] != '') {
            $this->db->where('tbl_teacher_staff_logins.date', $value['date']);
        }

		if (isset($value) && !empty($value['teacher_id']) && $value['teacher_id'] != '') {
			$this->db->where('tbl_teacher_staff_logins.teacher_id', $value['teacher_id']);
		}

		if (isset($value) && !empty($value['form_date']) && $value['form_date'] != '') {
			$this->db->where('tbl_teacher_staff_logins.date >=', $value['form_date']);
			$this->db->where('tbl_teacher_staff_logins.date <=', $value['to_date']);
		}

        $this->db->order_by("tbl_teacher_staff_logins.date", "desc");
        $this->db->order_by("tp.shorting_order", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_student_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_student_logins.*,s.name as student_name,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_student_logins');
        $this->db->join('tbl_student AS s', 'tbl_student_logins.student_id=s.id');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id', 'left');
        if (isset($value) && !empty($value)) {
            $this->db->where('tbl_student_logins.date', $value['date']);
        }
        $this->db->order_by("tbl_student_logins.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_student_timekeeping_list_for_device_data($limit, $offset, $value = '')
    {
        $this->db->select('tbl_logins.*,s.name as student_name,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_logins');
        $this->db->join('tbl_student AS s', 'tbl_logins.person_id=s.id');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id', 'left');
        if (isset($value) && !empty($value['date'])) {
            $this->db->where('tbl_logins.date', $value['date']);
        }
        if (isset($value) && !empty($value['person_id']) && $value['person_id'] != '') {
            $this->db->where('tbl_logins.person_id', $value['person_id']);
        }
        $this->db->where('tbl_logins.person_type', 'S');
        $this->db->order_by("tbl_logins.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_teacher_staff_leave_list($limit, $offset, $value = '')
    {
        $this->db->select('l.*,t.name,t.teacher_code,p.name as teacher_post_name,lt.name as leave_type_name');
        $this->db->from('tbl_teacher_staff_leave_applications as l');
		$this->db->join('tbl_leave_type AS lt', 'l.leave_type_id=lt.id');
        $this->db->join('tbl_teacher AS t', 'l.teacher_id=t.id');
		$this->db->join('tbl_teacher_post AS p', 't.post=p.id');
        $this->db->order_by("l.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }



    public function get_all_student_leave_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_student_leave_applications.*,s.name,s.roll_no,s.student_code,c.name as class,sc.name as section_name');
        $this->db->from('tbl_student_leave_applications');
        $this->db->join('tbl_student AS s', 'tbl_student_leave_applications.student_id=s.id');
        $this->db->join('tbl_class AS c', 'tbl_student_leave_applications.class_id=c.id');
        $this->db->join('tbl_section AS sc', 'tbl_student_leave_applications.section_id=sc.id');

        $student_info = $this->session->userdata('student_info');
        if (!empty($student_info)) {
            $student_id = $student_info[0]->id;
            $this->db->where('tbl_student_leave_applications.student_id', $student_id);
        }

        $this->db->where('tbl_student_leave_applications.is_deleted', '0');
        $this->db->order_by("tbl_student_leave_applications.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_staff_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_staff_logins.*,s.name as staff_name,s.staff_index_no,tp.name as post_name');
        $this->db->from('tbl_staff_logins');
        $this->db->join('tbl_staff_info AS s', 'tbl_staff_logins.staff_id=s.id');
        $this->db->join('tbl_teacher_post AS tp', 's.post=tp.id');
        if (isset($value) && !empty($value)) {
            $this->db->where('tbl_staff_logins.date', $value['date']);
        }

        $this->db->order_by("tbl_staff_logins.date", "desc");
        $this->db->order_by("tbl_staff_logins.staff_id", "asc");

        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_staff_timekeeping_list_for_device_data($limit, $offset, $value = '')
    {
        $this->db->select('tbl_logins.*,s.name as name,s.staff_index_no as index_no,tp.name as post_name');
        $this->db->from('tbl_logins');
        $this->db->join('tbl_staff_info AS s', 'tbl_logins.person_id=s.id');
        $this->db->join('tbl_teacher_post AS tp', 's.post=tp.id');
        if (isset($value) && !empty($value['date'])) {
            $this->db->where('tbl_logins.date', $value['date']);
        }

        if (isset($value) && !empty($value['person_id']) && $value['person_id'] != '') {
            $this->db->where('tbl_logins.person_id', $value['person_id']);
        }

        if (isset($value) && !empty($value['form_date']) && $value['form_date'] != '') {
            $this->db->where('tbl_logins.date >=', $value['form_date']);
            $this->db->where('tbl_logins.date <=', $value['to_date']);
        }

        $this->db->where('tbl_logins.person_type', 'ST');
        $this->db->order_by("tbl_logins.date", "desc");
        $this->db->order_by("tbl_logins.person_id", "asc");

        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_holiday_list($limit, $offset, $value = '')
    {
        $this->db->select('h.*,ht.name as holiday_name');
        $this->db->from('tbl_holiday as h');
        $this->db->join('tbl_holiday_type AS ht', 'h.holiday_type=ht.id');
        if (isset($value) && !empty($value) && $value['from_date'] != '' && $value['to_date'] != '') {
            $from_date = $value['from_date'];
            $to_date = $value['to_date'];
            $this->db->where("h.date between '$from_date' and '$to_date' ");
        }
		if (isset($value) && !empty($value['holiday_type_id']) && $value['holiday_type_id'] != '') {
			$this->db->where('h.holiday_type', $value['holiday_type_id']);
		}
        $this->db->order_by("h.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_movement_register_list($limit, $offset, $value = '')
    {
        $this->db->select('m.*,ts.name');
        $this->db->from('tbl_teacher_staff_movement_registers AS m');
		$this->db->join('tbl_teacher AS ts', 'm.teacher_id=ts.id');

        if (isset($value) && !empty($value['name']) && $value['name'] != '') {
            $this->db->like('ts.name', $value['name']);
        }

        if (isset($value) && !empty($value['date']) && $value['date'] != '') {
            $this->db->where('m.from_date', $value['date']);
        }
        $this->db->order_by("m.from_date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function check_holiday($date)
    {
        $sql = "SELECT `tbl_holiday`.*,`tbl_holiday_type`.`short_name` FROM `tbl_holiday`
                left join `tbl_holiday_type` on `tbl_holiday_type`.`id` = `tbl_holiday`.`holiday_type`
				WHERE `tbl_holiday`.`date` = '$date'";
        $result_info = $this->db->query($sql);
        return $result_info->result();
    }


    public function get_teacher_staff_report_absentee($date)
    {
		$leave_info = $this->get_teacher_staff_leave_info($date);
		$sql = " SELECT id AS teacher_id
				 FROM `tbl_teacher` WHERE `tbl_teacher`.`status` = '1' ORDER BY `tbl_teacher`.id";
        $teachers_info = $this->db->query($sql)->result_array();
		$teachers_table = array();
        foreach ($teachers_info as $info) {
			$teachers_table[] = $info['teacher_id'];
        }
        //echo "<br /><br /><pre>";print_r($persons_table); echo "</pre>"; die;

        $login_info = $this->get_login_info($date);

        $result_information = array();
        $s = 0;
        foreach ($teachers_table as $teacher_id) {
            $person_info = $this->get_teachers_information($teacher_id);
            if (empty($leave_info[$teacher_id]) && empty($login_info[$teacher_id])) {
                $result_information[$s]['name'] = $person_info[0]['name'];
				$result_information[$s]['teacher_code'] = $person_info[0]['teacher_code'];
                $result_information[$s]['post_name'] = $person_info[0]['post_name'];
                $s++;
            }
        }
        return $result_information;
    }

	public function get_teacher_staff_leave_info($date)
	{
		$rows = $this->db->query("SELECT * FROM `tbl_teacher_staff_leave_days` AS t
                WHERE t.`date` <= '$date'")->result_array();
		$leave_info = array();
		foreach ($rows as $row) {
			$leave_info[$row['teacher_id']]['id'] = $row['id'];
			$leave_info[$row['teacher_id']]['teacher_id'] = $row['teacher_id'];
			$leave_info[$row['teacher_id']]['date'] = $row['date'];
		}
		return $leave_info;
	}


    public function get_student_leave_info_for_sms($date)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_student_leave_applications` AS s WHERE s.`is_deleted` = 0 AND  s.`date_from` <= '$date' AND s.`date_to` >= '$date'")->result_array();
        $leave_info = array();
        foreach ($rows as $row) {
            $leave_info[$row['student_id']]['id'] = $row['id'];
            $leave_info[$row['student_id']]['student_id'] = $row['student_id'];
            $leave_info[$row['student_id']]['date_from'] = $row['date_from'];
            $leave_info[$row['student_id']]['date_to'] = $row['date_to'];
        }
        return $leave_info;
    }



    public function get_student_report_absentee_for_sms_by_date_and_shift($date, $shift_id)
    {
        /// echo $shift_id; die;
        $leave_info = $this->get_student_leave_info_for_sms($date);
        $sql = "SELECT s.id FROM `tbl_student` AS s WHERE s.`status` = '1' AND s.`shift_id` = '$shift_id'  ORDER BY s.class_id,s.roll_no";

        $students_info = $this->db->query($sql)->result_array();
        $students_table = array();
        foreach ($students_info as $info) {
            $students_table[] = $info['id'];
        }
        //echo "<br /><br /><pre>";print_r($students_table); echo "</pre>"; die;
        $person_type = 'S';
        $login_info = $this->get_login_info($date, $person_type);

        $result_information = array();
        $s = 0;
        foreach ($students_table as $student_id) {
            $person_info = $this->get_persons_information($student_id, $person_type);
            if (empty($leave_info[$student_id]) && empty($login_info[$student_id])) {
                $result_information[$s]['name'] = $person_info[0]['name'];
                $result_information[$s]['student_code'] = $person_info[0]['student_code'];
                $result_information[$s]['roll_no'] = $person_info[0]['roll_no'];
                $result_information[$s]['class_name'] = $person_info[0]['class_name'];
                $result_information[$s]['reg_no'] = $person_info[0]['reg_no'];
                $result_information[$s]['guardian_mobile'] = $person_info[0]['guardian_mobile'];
                $s++;
            }
        }
//        echo "<pre>";
//        print_r($result_information);
//        echo "</pre>";
//        die;
        return $result_information;
    }


    public function get_student_report_absentee_for_sms($date)
    {
        $leave_info = $this->get_student_leave_info_for_sms($date);
        $sql = "SELECT s.id FROM `tbl_student` AS s WHERE s.`status` = '1'  ORDER BY s.class_id,s.roll_no";

        $students_info = $this->db->query($sql)->result_array();
        $students_table = array();
        foreach ($students_info as $info) {
            $students_table[] = $info['id'];
        }
        //echo "<br /><br /><pre>";print_r($students_table); echo "</pre>"; die;
        $person_type = 'S';
        $login_info = $this->get_login_info($date, $person_type);

        $result_information = array();
        $s = 0;
        foreach ($students_table as $student_id) {
            $person_info = $this->get_persons_information($student_id, $person_type);
            if (empty($leave_info[$student_id]) && empty($login_info[$student_id])) {
                $result_information[$s]['name'] = $person_info[0]['name'];
                $result_information[$s]['student_code'] = $person_info[0]['student_code'];
                $result_information[$s]['roll_no'] = $person_info[0]['roll_no'];
                $result_information[$s]['class_name'] = $person_info[0]['class_name'];
                $result_information[$s]['reg_no'] = $person_info[0]['reg_no'];
                $result_information[$s]['guardian_mobile'] = $person_info[0]['guardian_mobile'];
                $s++;
            }
        }
//        echo "<pre>";
//        print_r($result_information);
//        echo "</pre>";
//        die;
        return $result_information;
    }


    public function getStudentReportAbsentees($date, $class_id, $shift_id, $section_id, $group_id, $period_id){
		return $this->db->query("SELECT l.`date`,l.`date`,s.`name`,s.`student_code` FROM `tbl_student_logins` AS l
INNER JOIN `tbl_student` AS s ON s.`id` = l.`student_id`
WHERE l.`login_status` = 'A' AND l.`date` = '$date'
AND l.`class_id` = $class_id AND l.`shift_id` = $shift_id AND l.`section_id` = $section_id
AND l.`group_id` = $group_id AND l.`period_id` = $period_id;")->result_array();
	}




    public function get_student_report_absentee($date, $class_id, $section_id, $group)
    {
        $leave_info = $this->get_student_leave_info($date, $class_id, $section_id, $group);
        $sql = "SELECT s.id FROM `tbl_student` AS s WHERE s.`status` = '1' AND s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group'  ORDER BY s.roll_no";

        $students_info = $this->db->query($sql)->result_array();
        $students_table = array();
        foreach ($students_info as $info) {
            $students_table[] = $info['id'];
        }
        //echo "<br /><br /><pre>";print_r($students_table); echo "</pre>"; die;
        $person_type = 'S';
        $login_info = $this->get_login_info($date, $person_type);

        $result_information = array();
        $s = 0;
        foreach ($students_table as $student_id) {
            $person_info = $this->get_persons_information($student_id, $person_type);
            if (empty($leave_info[$student_id]) && empty($login_info[$student_id])) {
                $result_information[$s]['name'] = $person_info[0]['name'];
                $result_information[$s]['roll_no'] = $person_info[0]['roll_no'];
                $result_information[$s]['reg_no'] = $person_info[0]['reg_no'];
                $result_information[$s]['guardian_mobile'] = $person_info[0]['guardian_mobile'];
                $s++;
            }
        }
//        echo "<pre>";
//        print_r($result_information);
//        echo "</pre>";
//        die;
        return $result_information;
    }

    public function get_student_leave_info($date, $class_id, $section_id, $group)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_student_leave_applications` AS s WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`is_deleted` = 0 AND  s.`date_from` <= '$date' AND s.`date_to` >= '$date'")->result_array();
        $leave_info = array();
        foreach ($rows as $row) {
            $leave_info[$row['student_id']]['id'] = $row['id'];
            $leave_info[$row['student_id']]['student_id'] = $row['student_id'];
            $leave_info[$row['student_id']]['date_from'] = $row['date_from'];
            $leave_info[$row['student_id']]['date_to'] = $row['date_to'];
        }
        return $leave_info;
    }

	public function get_teachers_information($teacher_id)
	{
		$result_info = $this->Teacher_info->get_teacher_photo_info_by_teacher_id($teacher_id);
		return $result_info;
	}


    public function get_persons_information($person_id, $person_type)
    {
        if ($person_type == 'T') {
            $result_info = $this->Teacher_info->get_teacher_photo_info_by_teacher_id($person_id);
        } elseif ($person_type == 'S') {
            $result_info = $this->Student->get_student_info_by_student_id($person_id);
        } else {
            $this->db->select('tbl_staff_info.*,tp.name as post_name,ts.name as section_name');
            $this->db->from('tbl_staff_info');
            $this->db->join('tbl_teacher_post AS tp', 'tbl_staff_info.post=tp.id');
            $this->db->join('tbl_teacher_section AS ts', 'tbl_staff_info.section=ts.id');
            $this->db->where('tbl_staff_info.id', $person_id);
            $query = $this->db->get();
            $result_info = $query->result_array();
        }
        return $result_info;
    }

    public function get_teacher_leave_info($date)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_teacher_leave_applications` AS t WHERE t.`is_deleted` = 0 AND  t.`date_from` <= '$date' AND t.`date_to` >= '$date'")->result_array();
        $leave_info = array();
        foreach ($rows as $row) {
            $leave_info[$row['teacher_id']]['id'] = $row['id'];
            $leave_info[$row['teacher_id']]['teacher_id'] = $row['teacher_id'];
            $leave_info[$row['teacher_id']]['date_from'] = $row['date_from'];
            $leave_info[$row['teacher_id']]['date_to'] = $row['date_to'];
        }
        return $leave_info;
    }


    public function get_staff_leave_info($date)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_staff_leave_applications` AS t WHERE t.`is_deleted` = 0 AND t.`date_from` <= '$date' AND t.`date_to` >= '$date'")->result_array();
        $leave_info = array();
        foreach ($rows as $row) {
            $leave_info[$row['staff_id']]['id'] = $row['id'];
            $leave_info[$row['staff_id']]['staff_id'] = $row['staff_id'];
            $leave_info[$row['staff_id']]['date_from'] = $row['date_from'];
            $leave_info[$row['staff_id']]['date_to'] = $row['date_to'];
        }
        return $leave_info;
    }

    public function get_login_info($date)
    {
        $rows = $this->db->query("SELECT id,teacher_id,date FROM `tbl_teacher_staff_logins` WHERE `date`= '$date';")->result_array();
        $login_info = array();
        foreach ($rows as $row) {
			$login_info[$row['teacher_id']]['id'] = $row['id'];
            $login_info[$row['teacher_id']]['teacher_id'] = $row['teacher_id'];
            $login_info[$row['teacher_id']]['date'] = $row['date'];
        }
        return $login_info;
    }

    public function get_login_info_by_person_id($date, $person_type, $person_id)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_logins` WHERE `person_id` = '$person_id' AND `person_type` = '$person_type' AND `date`= '$date' AND login_time >'00:00:00' ")->result_array();
        return $rows;
    }

    public function get_teacher_leave_info_by_teacher_id($date, $teacher_id)
    {
        $leave_info = $this->db->query("SELECT * FROM `tbl_teacher_leave_applications` AS t WHERE t.`teacher_id` = $teacher_id AND t.`is_deleted` = 0 AND  t.`date_from` <= '$date' AND t.`date_to` >= '$date'")->result_array();
        return $leave_info;
    }

	public function get_login_info_by_teacher_id($date, $teacher_id)
	{
		$rows = $this->db->query("SELECT * FROM `tbl_teacher_staff_logins` WHERE `teacher_id` = $teacher_id AND `date`= '$date'")->result_array();
		return $rows;
	}

	public function get_teacher_leave_data_by_teacher_id($date, $teacher_id){
		$rows = $this->db->query("SELECT ld.`date`,ld.`teacher_id`,lt.`short_name`,lt.`name` FROM `tbl_teacher_staff_leave_days` AS ld
INNER JOIN `tbl_teacher_staff_leave_applications` AS lp ON lp.`id` = ld.`leave_id`
INNER JOIN `tbl_leave_type` AS lt ON lt.`id` = lp.`leave_type_id`
WHERE ld.`date` = '$date'  AND ld.`teacher_id` = $teacher_id")->row();
		return $rows;
	}


	public function get_teacher_staff_attendance_details($data)
	{
		$form_date = $data['form_date'];
		$to_date = $data['to_date'];
		//echo $teacher_id; die;
		$number_of_days =  $data['number_of_days'];

		$where = "";
		if(isset($data['teacher_id'])){
			$teacher_id = $data['teacher_id'];
			if($teacher_id != ''){
				$where = " AND `tbl_teacher`.`id` = $teacher_id ";
			}
		}

		$sql = "SELECT `tbl_teacher`.*, p.name as post_name FROM `tbl_teacher`
        LEFT JOIN tbl_teacher_post as p on p.id = `tbl_teacher`.post
         WHERE `tbl_teacher`.`status` = '1' $where ORDER BY p.shorting_order";

		$teachers_info = $this->db->query($sql)->result_array();

		$result_information = array();

		$s = 0;
		foreach ($teachers_info as $teacher) {
			$date = $form_date;
			$total_present_days = 0;
			$total_absent_days = 0;
			$total_leave_days = 0;
			$total_holidays = 0;
			$attendance_details = array();
			$i = 1;
			while ($i <= $number_of_days) {
				$login_info = $this->get_login_info_by_teacher_id($date, $teacher['id']);
				if (!empty($login_info)) {
					$attendance_details[$i]['status'] = 'Present';
					$attendance_details[$i]['status_short'] = 'P';
					$attendance_details[$i]['login_time'] = $login_info[0]['login_time'];
					$attendance_details[$i]['logout_time'] = $login_info[0]['logout_time'];
					$total_present_days = $total_present_days + 1;
				} else {
					$check_holiday = $this->check_holiday($date);
					if (!empty($check_holiday)) {
						$attendance_details[$i]['status'] = 'Holiday';
						$attendance_details[$i]['status_short'] = $check_holiday[0]->short_name;
						$attendance_details[$i]['holiday_type'] = $check_holiday[0]->short_name;
						$total_holidays = $total_holidays + 1;
					} else {
						$leave_info = $this->get_teacher_leave_data_by_teacher_id($date, $teacher['id']);
						if (!empty($leave_info)) {
							$attendance_details[$i]['status'] = 'Leave';
							$attendance_details[$i]['status_short'] = $leave_info->short_name;
							$attendance_details[$i]['leave_type'] = $leave_info->short_name;
							$total_leave_days = $total_leave_days + 1;
						} else {
							$attendance_details[$i]['status'] = 'Absent';
							$attendance_details[$i]['status_short'] = 'A';
							$total_absent_days = $total_absent_days + 1;
						}
					}
				}
				$attendance_details[$i]['date'] = $date;
				$date = date('Y-m-d', strtotime($date . ' +1 day'));
				$i++;
			}
			//echo $status;
			//die;
			$result_information[$s]['name'] = $teacher['name'];
			$result_information[$s]['teacher_code'] = $teacher['teacher_code'];
			$result_information[$s]['post_name'] = $teacher['post_name'];
			$result_information[$s]['total_present_days'] = $total_present_days;
			$result_information[$s]['total_absent_days'] = $total_absent_days;
			$result_information[$s]['total_leave_days'] = $total_leave_days;
			$result_information[$s]['total_holidays'] = $total_holidays;
			$result_information[$s]['attendance_data'] = $attendance_details;
			$s++;
		}
//		 echo "<pre>";
//		 print_r($result_information);
//		 echo "</pre>";
//		 die;
		return $result_information;
	}


    public function get_teacher_attendance_details($data)
    {
        $month = $data['month'];
        $year = $data['year'];
        $number_of_days = $data['number_of_days'];
        $sql = " SELECT `tbl_teacher`.id FROM `tbl_teacher`
        LEFT JOIN tbl_teacher_post as p on p.id = `tbl_teacher`.post
         WHERE `tbl_teacher`.`status` = '1' ORDER BY p.shorting_order";

        $teachers_info = $this->db->query($sql)->result_array();
        $teachers_table = array();
        foreach ($teachers_info as $info) {
            $teachers_table[] = $info['id'];
        }
        //echo "<pre>";print_r($teachers_table); echo "</pre>"; die;


        $result_information = array();
        $s = 0;
        foreach ($teachers_table as $teacher_id) {
            $person_info = $this->get_persons_information($teacher_id, 'T');
            $total_present_days = 0;
            $total_absent_days = 0;
            $total_leave_days = 0;
            $total_holidays = 0;
            $status = "";
            $i = 1;
            while ($i <= $number_of_days) {
                if ($i < 10) {
                    $date = $year . '-' . $month . '-0' . $i;
                } else {
                    $date = $year . '-' . $month . '-' . $i;
                }

                $login_info = $this->get_login_info_by_person_id($date, 'T', $teacher_id);
                if (!empty($login_info)) {
                    $t = $login_info[0]['login_time'] . '<br>' . $login_info[0]['logout_time'];
                    $status .= "<td align='center'>" . $t . "</td>";
                    $total_present_days = $total_present_days + 1;
                } else {
                    $check_holiday = $this->check_holiday($date);
                    if (!empty($check_holiday)) {
                        $status .= "<td align='center'>". $check_holiday[0]->short_name ."</td>";
                        $total_holidays = $total_holidays + 1;
                    } else {
                        $leave_info = $this->get_teacher_leave_info_by_teacher_id($date, $teacher_id);
                        if (!empty($leave_info)) {
                            $status .= "<td align='center'>L</td>";
                            $total_leave_days = $total_leave_days + 1;
                        } else {
                            $status .= "<td align='center'>A</td>";
                            $total_absent_days = $total_absent_days + 1;
                        }
                    }
                }
                $i++;
            }
            //echo $status;
            //die;
            $result_information[$s]['name'] = $person_info[0]['name'];
            $result_information[$s]['teacher_index_no'] = $person_info[0]['teacher_index_no'];
            $result_information[$s]['total_present_days'] = $total_present_days;
            $result_information[$s]['total_absent_days'] = $total_absent_days;
            $result_information[$s]['total_leave_days'] = $total_leave_days;
            $result_information[$s]['total_holidays'] = $total_holidays;
            $result_information[$s]['data'] = $status;
            $s++;
        }
        // echo "<pre>";
        //  print_r($result_information);
        // echo "</pre>";
        // die;
        return $result_information;
    }


    public function get_total_student_by_class_id($class_id)
    {
        return count($this->db->query("SELECT id FROM `tbl_student` WHERE class_id = '$class_id'")->result_array());
    }

    public function get_total_present_student_by_class_id($class_id, $date)
    {
        return count($this->db->query("SELECT id FROM `tbl_logins` AS l
WHERE l.`date` = '$date' AND l.`person_type` = 'S' AND
l.`person_id` IN (SELECT id FROM `tbl_student` AS s WHERE s.`class_id` = '$class_id')")->result_array());
    }

    public function get_student_leave_info_by_class_id($date, $class_id)
    {
        return count($this->db->query("SELECT * FROM `tbl_student_leave_applications` AS s WHERE s.`class_id` = '$class_id' AND s.`is_deleted` = 0 AND  s.`date_from` <= '$date' AND s.`date_to` >= '$date'")->result_array());
    }


    public function get_student_attendance_summery($data)
    {
        $month = $data['month'];
        $year = $data['year'];
        $number_of_days = $data['number_of_days'];
        $sql = "SELECT * FROM `tbl_class` ORDER BY id";
        $class_info = $this->db->query($sql)->result_array();

        $result_information = array();
        $i = 1;
        while ($i <= $number_of_days) {
            $status = "";
            if ($i < 10) {
                $date = $year . '-' . $month . '-0' . $i;
            } else {
                $date = $year . '-' . $month . '-' . $i;
            }
            $s = 0;
            $grand_total_student = 0;
            $grand_total_present = 0;
            $grand_total_leave = 0;
            $grand_total_absent = 0;

            foreach ($class_info as $class) {
                $total_student = $this->get_total_student_by_class_id($class['id']);
                $total_present = $this->get_total_present_student_by_class_id($class['id'], $date);
                $total_leave = $this->get_student_leave_info_by_class_id($date, $class['id']);

                $grand_total_student += $total_student;
                $grand_total_present += $total_present;
                $grand_total_absent += ($total_student - ($total_present + $total_leave));
                $grand_total_leave += $total_leave;

                $status .= "<td><table width='100%'>";
                $status .= "<tr><td align='center'>" . $total_student . "</td></tr>";
                $status .= "<tr><td align='center'>" . $total_present . "</td></tr>";
                $status .= "<tr><td align='center'>" . ($total_student - ($total_present + $total_leave)) . "</td></tr>";
                $status .= "<tr><td align='center'>" . $total_leave . "</td></tr>";
                $status .= "</table></td>";
                $s++;
            }

            $status .= "<td><table width='100%'>";
            $status .= "<tr><td align='center'>" . $grand_total_student . "</td></tr>";
            $status .= "<tr><td align='center'>" . $grand_total_present . "</td></tr>";
            $status .= "<tr><td align='center'>" . $grand_total_absent . "</td></tr>";
            $status .= "<tr><td align='center'>" . $grand_total_leave . "</td></tr>";
            $status .= "</table></td>";

            $result_information[$i]['data'] = $status;
            $i++;
        }


        return $result_information;
    }


//    public function get_student_attendance_details_summery($data)
//    {
//        $from_date = $data['from_date'];
//        $to_date = $data['to_date'];
//        $class_id = $data['class_id'];
//		$shift_id = $data['shift_id'];
//        $section_id = $data['section_id'];
//		$group_id = $data['group_id'];
//		$period_id = $data['period_id'];
//		$year = DateTime::createFromFormat("Y-m-d", $from_date)->format("Y");
//
//        $total_working_days = $data['total_working_days'];
//
//		if($group_id == '' || $group_id == 'all'){
//			$group_id = '';
//		}
//
//		$students_info = $this->Admin_login->getStudentListByYear($year, $class_id, $shift_id, $section_id, $group_id);
//
//        $result_information = array();
//        $s = 0;
//        foreach ($students_info as $student) {
//            $student_id = $student['id'];
//            $total_present_days = 0;
//            $total_absent_days = 0;
//            $total_leave_days = 0;
//            $total_holidays = 0;
//
//            $login_rows = $this->db->query("SELECT id FROM `tbl_logins` WHERE `person_id` = '$student_id'
//            AND `person_type` = 'S' AND (`date` BETWEEN '$from_date' AND '$to_date')  AND login_time >'00:00:00' GROUP BY `date`")->result_array();
//            //echo $to_date . '/' . $to_date;;
//            //echo $this->db->last_query();
//            //  echo "<pre>";
//            // print_r($login_rows);
//            // die;
//            $total_present_days = count($login_rows);
//
//            $holiday_rows = $this->db->query("SELECT id,date FROM `tbl_holiday` WHERE (`date` BETWEEN '$from_date' AND '$to_date')  GROUP BY `date`")->result_array();
//            // echo "<pre>";
//            //print_r($holiday_rows); die;
//            $total_holidays = count($holiday_rows);
//
//            $leave_rows = $this->db->query("select count(`date`) as total_leave from `tbl_student_leave_days`
//                            WHERE (`date` BETWEEN '$from_date' AND '$to_date') AND student_id = '$student_id'")->result_array();
//            $total_leave_days = $leave_rows[0]['total_leave'];
//
//            $total_absent_days = ($total_working_days - ($total_present_days + $total_holidays + $total_leave_days));
//            if ($total_absent_days < 1) {
//                $total_absent_days = 0;
//            }
//
//            if ($total_leave_days < 1) {
//                $total_leave_days = 0;
//            }
//
//            $result_information[$s]['id'] = $student['id'];
//            $result_information[$s]['name'] =$student['name'];
//            $result_information[$s]['student_code'] = $student['student_code'];
//            $result_information[$s]['roll_no'] = $student['roll_no'];
//            $result_information[$s]['total_present_days'] = $total_present_days;
//            $result_information[$s]['total_absent_days'] = $total_absent_days;
//            $result_information[$s]['total_leave_days'] = $total_leave_days;
//            $result_information[$s]['total_holidays'] = $total_holidays;
//            $result_information[$s]['total_working_days'] = $total_working_days - $total_holidays;
//            $s++;
//        }
//        //  echo "<pre>";
//        //print_r($result_information);
//        //echo "</pre>";
//        //die;
//        return $result_information;
//    }


    public function get_student_attendance_details($data)
    {
        $year = $data['year'];
        $class_id = $data['class_id'];
		$shift_id = $data['shift_id'];
        $section_id = $data['section_id'];
        $group_id = $data['group_id'];
		$period_id = $data['period_id'];
		$from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $number_of_days = $data['number_of_days'];

        if($group_id == '' || $group_id == 'all'){
			$group_id = '';
		}

        $students_info = $this->Admin_login->getStudentListByYear($year, $class_id, $shift_id, $section_id, $group_id);

        if(empty($students_info['students'])){
			$sdata['exception'] =  "Student Not Found";
			$this->session->set_userdata($sdata);
			redirect("student_attendance_details/index");
		}

        $result_information = array();
        $s = 0;

        foreach ($students_info['students'] as $student) {
			$student_id = $student['id'];

            $total_present_days = 0;
            $total_absent_days = 0;
            $total_leave_days = 0;
            $total_holidays = 0;
			$date = $from_date;
            $status = "";
            $i = 1;
            while ($i <= $number_of_days) {

                if($period_id == 'FM'){
					$login_info = $this->get_login_info_by_person_id($date, 'S', $student_id);
				}else{
					$login_info = $this->get_student_period_wise_login_info_by_student_id($date, $student_id, $period_id);
				}

                //echo $date.'/';

                if (!empty($login_info)) {
					if($period_id == 'FM'){
						$t = $login_info[0]['login_time'] . '<br>' . $login_info[0]['logout_time'];
						$total_present_days = $total_present_days + 1;
					}else{
						$t = $login_info[0]['login_status'];
						if($login_info[0]['login_status'] == 'P'){
							$total_present_days = $total_present_days + 1;
						}else if($login_info[0]['login_status'] == 'L'){
							$total_leave_days = $total_leave_days + 1;
						}else{
							$total_absent_days = $total_absent_days + 1;
						}
					}
                    $status .= "<td class='td_center'>" . $t . "</td>";
                } else {

                    $check_holiday = $this->check_holiday($date);
                    //echo '<pre>';
                    //print_r($check_holiday);
                    // die;
                    if (!empty($check_holiday)) {
                        $status .= "<td class='td_center'>". $check_holiday[0]->short_name ."</td>";
                        $total_holidays = $total_holidays + 1;
                    } else {
						if($period_id == 'FM'){
							$leave_info = $this->get_student_leave_info_by_student_id($date, $student_id);
							if (!empty($leave_info)) {
								$status .= "<td class='td_center'>L</td>";
								$total_leave_days = $total_leave_days + 1;
							} else {
								$status .= "<td class='td_center'>A</td>";
								$total_absent_days = $total_absent_days + 1;
							}
						}else{
							$status .= "<td class='td_center'>-</td>";
						}
                    }
                }
				$date = date('Y-m-d', strtotime($date . ' +1 day'));
                $i++;
            }
            //echo $status;
            //die;
            $result_information[$s]['name'] = $student['name'];
            $result_information[$s]['student_code'] = $student['student_code'];
            $result_information[$s]['roll_no'] = $student['roll_no'];
            $result_information[$s]['total_present_days'] = $total_present_days;
            $result_information[$s]['total_absent_days'] = $total_absent_days;
            $result_information[$s]['total_leave_days'] = $total_leave_days;
            $result_information[$s]['total_holidays'] = $total_holidays;
			$result_information[$s]['total_working_days'] = $number_of_days - $total_holidays;
            $result_information[$s]['data'] = $status;
            $s++;

        }
//        echo "<pre>";
//         print_r($result_information);
//        echo "</pre>";
//        die;
        return $result_information;
    }

    function get_student_period_wise_login_info_by_student_id($date, $student_id, $period_id){
		$login_info = $this->db->query("SELECT login_status FROM `tbl_student_logins`
						WHERE `student_id` = '$student_id' AND `date` = '$date'
						AND `period_id` = '$period_id'")->result_array();
		return $login_info;
	}

    public function get_student_leave_info_by_student_id($date, $student_id)
    {
        $leave_info = $this->db->query("SELECT * FROM `tbl_student_leave_applications` AS s WHERE s.`student_id` = '$student_id' AND s.`is_deleted` = 0 AND  s.`date_from` <= '$date' AND s.`date_to` >= '$date'")->result_array();
        return $leave_info;
    }

    public function getAttendancebydate($date){
		return $this->db->query("SELECT s.`id`,u.`first_name` AS user_name,h.`bussines_on_date`,h.`case_master_id`,h.`case_status_id`,h.`parties_name`, c.`first_name`,c.`last_name`,c.`mobile`,h.`id`,
          c.`id` AS client_id, cs.`bn_name` AS case_name, h.`case_no`,h.`id` as case_history_id
          FROM `ad_master_case` AS s
          INNER JOIN `ad_case_history` AS h ON h.`case_master_id` = s.`id`
          INNER JOIN `ad_client_user` AS u ON u.`id` = s.`client_user_id`
          INNER JOIN `ad_clients` AS c ON c.`id` = s.`client_id`
          INNER JOIN `ad_case_sub_type` AS cs ON s.`case_sub_type_id` = cs.`id`
          INNER JOIN `ad_case_status` AS st ON h.`case_status_id` = st.`id`
          WHERE  st.`name`!='Closed' AND h.`bussines_on_date` = '$date';")->result_array();
          	}
}
