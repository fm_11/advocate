<?php

class Photo_gallery extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function add_gallery_event($data)
    {
        return $this->db->insert('tbl_photo_event', $data);
    }

    function get_all_gallery_events($limit, $offset, $value = '')
    {
        $this->db->select('pe.*');
        $this->db->from('tbl_photo_event AS pe');
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    function delete_gallery_event_by_event_id($id)
    {
        return $this->db->delete('tbl_photo_event', array('id' => $id));
    }

    function get_event_info_by_event_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_photo_event')->result_array();
    }

    function update_gallery_event($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_photo_event', $data);
    }

    function get_all_gallery_photos($limit, $offset, $value = '')
    {
        $this->db->select('pe.event_name ,gi.*');
        $this->db->from('tbl_gallery_image AS gi');
        $this->db->join('tbl_photo_event AS pe', 'gi.event_id=pe.id');
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_gallery_event_by_unit_id($unit_id)
    {
        return $this->db->where('unit_id', $unit_id)->get('tbl_photo_event')->result_array();
    }

    function add_gallery_photo($data)
    {
        return $this->db->insert('tbl_gallery_image', $data);
    }

    function delete_gallery_photo_by_photo_id($id)
    {
        return $this->db->delete('tbl_gallery_image', array('id' => $id));
    }

    function get_gallery_photo_info_by_photo_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_gallery_image')->result_array();
    }

    function get_photo_info_by_photo_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_gallery_image')->result_array();
    }

    function get_all_gallery_event_for_fontend($limit, $offset, $value = '')
    {
        $this->db->select('u.unit_name ,pe.*');
        $this->db->from('tbl_photo_event AS pe');
        $this->db->join('tbl_unit AS u', 'pe.unit_id=u.id');
        $this->db->where('u.id', $value['unit_id']);
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_unit_for_fontend()
    {
        return $this->db->get('tbl_unit')->result_array();
    }

    function get_all_images_by_unit_and_event_id($data)
    {
        $this->db->select('tg.*');
        $this->db->from('tbl_gallery_image AS tg');
        $this->db->where(array('tg.unit_id' => $data['unit_id'], 'tg.event_id' => $data['event_id']));
        $query = $this->db->get();
        return $query->result_array();
    }

}

?>