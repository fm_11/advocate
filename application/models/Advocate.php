<?php


class Advocate extends CI_Model
{

    function __construct()
    {
        $this->load->database();
        $this->load->library('session');
        parent::__construct();
    }
    public function get_notification($employee_id)
    {
        // return $this->db->query("SELECT n.*,h.`bussines_on_date` FROM `tbl_notification` AS n
        //                         LEFT JOIN `ad_case_history` AS h ON n.`reference_id`= h.`id`
        //                         WHERE  n.`it_seen` = 0
        //                         ")->result_array();
        $arrayName = array();
        return $arrayName;
    }
    function update_notification($data, $id)
    {
          $count=$this->db->get_where('tbl_notification', array('id'=> $id  , 'it_seen' => 1))->num_rows();
          if($count==0)
          {
            return $this->db->update('tbl_notification', $data, array('id' => $id));
          }
    }

    public function get_all_divisions_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_divisions as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value) && !empty($value) && isset($value['bn_name']) && $value['bn_name'] != '') {
            $this->db->like('s.bn_name', $value['bn_name']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_district_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*, d.name as div_name, d.bn_name as div_bnname');
        $this->db->from('ad_districts as s');
        $this->db->join('ad_divisions AS d', 's.division_id=d.id', 'left');

        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
           $this->db->group_start();
            $this->db->like('s.name', $value['name']);
            $this->db->or_like('s.bn_name', $value['name']);
            $this->db->group_end();
        }
        if (isset($value) && !empty($value) && isset($value['division_id']) && $value['division_id'] != '') {
            $this->db->like('s.division_id', $value['division_id']);
        }
        $this->db->order_by("d.name", "asce");
        $this->db->order_by("s.serial_no", "asce");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_judge_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_judge as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_judge($data)
    {
        return $this->db->insert('ad_judge', $data);
    }


    function edit_judge($data, $id)
    {
        return $this->db->update('ad_judge', $data, array('id' => $id));
    }


    function read_judge($judge_id)
    {
        return $this->db->get_where('ad_judge', array('id' => $judge_id))->row();
    }

    function delete_judge($judge_id)
    {
        return $this->db->delete('ad_judge', array('id' => $judge_id));
    }
    public function checkifexist_judge_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_judge where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_judge_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_judge where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    function get_tax_list()
    {
        return $this->db->query("SELECT * FROM `ad_tax` ORDER BY `id`")->result();
    }

    function add_tax($data)
    {
        return $this->db->insert('ad_tax', $data);
    }


    function edit_tax($data, $id)
    {
        return $this->db->update('ad_tax', $data, array('id' => $id));
    }


    function read_tax($judge_id)
    {
        return $this->db->get_where('ad_tax', array('id' => $role_id))->row();
    }

    function delete_tax($judge_id)
    {
        return $this->db->delete('ad_tax', array('id' => $role_id));
    }
    public function get_all_vendor_list($limit, $offset, $value = '')
    {
        // $this->db->select('v.*,c.`name` AS country_name,d.`name` AS district_name,d.`name` AS upazilas_name');
        $this->db->select('v.*,c.`name` AS country_name');
        $this->db->from('`ad_vendor AS v');
        $this->db->join('ad_country AS c', 'v.country_id=c.id', 'left');
        // $this->db->join('ad_districts AS d', 'c.id=d.country_id', 'left');
       // $this->db->join('ad_upazilas AS u', 'd.id=u.district_id', 'left');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('v.company_name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('v.is_active', $value['status']);
        }
        $this->db->order_by("v.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_vendor($data)
    {
        return $this->db->insert('ad_vendor', $data);
    }


    function edit_vendor($data, $id)
    {
        return $this->db->update('ad_vendor', $data, array('id' => $id));
    }


    function read_vendor($vendor_id)
    {
        return $this->db->get_where('ad_vendor', array('id' => $vendor_id))->row();
    }

    function delete_vendor($vendor_id)
    {
        return $this->db->delete('ad_vendor', array('id' => $vendor_id));
    }
    public function checkifexist_vendor_by_mobile($mobile)
    {
      $count = $this->db->query("SELECT id FROM ad_vendor where mobile = '$mobile'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_vendor_by_mobile($mobile,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_vendor where mobile = '$mobile' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    function get_country_list()
    {
        return $this->db->query("SELECT * FROM `ad_country` ")->result_array();
    }
    function get_country_list_by_language($language)
    {
      if($language=="english")
      {
        return $this->db->query("SELECT id,name FROM `ad_country` ")->result_array();
      }
      return $this->db->query("SELECT id,bd_name as name FROM `ad_country` ")->result_array();
    }
    function get_districts_list_by_country($country_id)
    {
        return $this->db->query("SELECT id,name FROM `ad_districts` where country_id= $country_id")->result_array();
    }
    function get_upazilas_list_by_district($district_id=0)
    {
        return $this->db->query("SELECT * FROM `ad_upazilas` where district_id= $district_id")->result_array();
    }
    function get_all_districts_list_by_lang($lang)
    {
      if($lang=="english")
      {
        return $this->db->query("SELECT id,name FROM `ad_districts`")->result_array();
      }else{
        return $this->db->query("SELECT id, bn_name as name FROM `ad_districts`")->result_array();
      }
    }
    function get_districts_list_by_lang_and_districtid($lang,$district_id)
    {
      if($lang=="english")
      {
        return $this->db->query("SELECT id,name FROM `ad_districts` where id=$district_id")->result_array();
      }else{
        return $this->db->query("SELECT id, bn_name as name FROM `ad_districts` where id=$district_id")->result_array();
      }
    }
    function get_upazilas_list_by_district_and_lang($lang,$district_id)
    {
      if($lang=="english")
      {
        return $this->db->query("SELECT id,name,district_id FROM `ad_upazilas` where district_id= $district_id")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name as name,district_id FROM `ad_upazilas` where district_id= $district_id")->result_array();
      }
    }
    function get_case_type_list_by_lang($lang)
    {
      $data = array();
      if($lang=="english")
      {
        $data[0]['name'] = "Court";
        $data[0]['id'] = "Court";

        $data[1]['name'] = "Thana";
        $data[1]['id'] = "Thana";
      }else{
        $data[0]['name'] = "আদালত";
        $data[0]['id'] = "Court";

        $data[1]['name'] = "থানা";
        $data[1]['id'] = "Thana";
      }
      return $data;
    }
    function get_court_type_list_by_district_id($district_id)
    {
        return $this->db->query("SELECT id,name FROM `ad_court_type` WHERE id IN(
                                SELECT DISTINCT `court_type_id` FROM `district_wise_court_allowcations`
                                WHERE `district_id`=$district_id
                                ) ORDER BY order_no")->result_array();
    }
    function get_court_type_list_by_district_id_and_language($district_id,$lang)
    {
        if($lang=="english"){
          return $this->db->query("SELECT id,name FROM `ad_court_type` WHERE is_active=1 and id IN(
                                  SELECT DISTINCT `court_type_id` FROM `district_wise_court_allowcations`
                                  WHERE `district_id`=$district_id
                                  ) ORDER BY order_no")->result_array();
        }
        return $this->db->query("SELECT id,bn_name as name FROM `ad_court_type` WHERE is_active=1 and id IN(
                                SELECT DISTINCT `court_type_id` FROM `district_wise_court_allowcations`
                                WHERE `district_id`=$district_id
                                ) ORDER BY order_no")->result_array();
    }
    function get_court_list_by_district_id_and_court_type_id($district_id,$court_type_id)
    {
        return $this->db->query("SELECT id,name FROM `ad_court` WHERE id IN(
                                SELECT DISTINCT `court_id` FROM `district_wise_court_allowcations`
                                WHERE `district_id`=$district_id AND court_type_id=$court_type_id
                                ) ORDER BY NAME")->result_array();
    }
    function get_court_list_by_district_id_and_court_type_id_and_language($district_id,$court_type_id,$lang)
    {
      if($lang=="english")
      {
        return $this->db->query("SELECT id,name FROM `ad_court` WHERE is_active=1 and id IN(
                                SELECT DISTINCT `court_id` FROM `district_wise_court_allowcations`
                                WHERE `district_id`=$district_id AND court_type_id=$court_type_id
                              ) ORDER BY name")->result_array();
      }
        return $this->db->query("SELECT id,bn_name as name FROM `ad_court` WHERE is_active=1 and id IN(
                                SELECT DISTINCT `court_id` FROM `district_wise_court_allowcations`
                                WHERE `district_id`=$district_id AND court_type_id=$court_type_id
                              ) ORDER BY name")->result_array();
    }

    function get_vendor_dropdown_list()
    {
        return $this->db->query("SELECT id,CONCAT(`mobile`,'-',`company_name`) AS name FROM `ad_vendor` ORDER BY `company_name`")->result_array();
    }
    public function get_all_member_list($limit, $offset, $value = '')
    {
        // $this->db->select('v.*,c.`name` AS country_name,d.`name` AS district_name,d.`name` AS upazilas_name');
        $this->db->select('m.*,c.`name` AS country_name,r.role_name');
        $this->db->from('`ad_client_user AS m');
        $this->db->join('tbl_user AS u', 'm.id=u.client_user_id');
        $this->db->join('ad_country AS c', 'm.country_id=c.id', 'left');
        $this->db->join('user_roles AS r', 'm.role_id=r.id', 'left');

       // $this->db->join('ad_upazilas AS u', 'd.id=u.district_id', 'left');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('m.first_name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('m.is_active', $value['status']);
        }
        $this->db->where('u.`is_school360_user`', '0');
        $this->db->order_by("m.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_role_dropdown_list()
    {
        return $this->db->query("SELECT id,role_name AS name FROM `user_roles` ORDER BY `role_name`")->result_array();
    }
    function add_member($data)
    {
        return $this->db->insert('ad_client_user', $data);
    }
    function edit_member($data, $id)
    {
        return $this->db->update('ad_client_user', $data, array('id' => $id));
    }
    function read_member($vendor_id)
    {
        return $this->db->get_where('ad_client_user', array('id' => $vendor_id))->row();
    }
    function delete_member($vendor_id)
    {
        return $this->db->delete('ad_client_user', array('id' => $vendor_id));
    }
    public function checkifexist_member_by_mobile($mobile)
    {
      $count = $this->db->query("SELECT id FROM ad_client_user where mobile = '$mobile'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_member_by_mobile($mobile,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_client_user where mobile = '$mobile' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = time().'_'. $type . '.' .end($ext);

        // print_r(rand());
        // die($new_file_name);
        $this->load->library('upload');
        $config = array(
                'upload_path' => MEDIA_FOLDER."/member/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                'max_size' => "99999999999",
                'max_height' => "1500",
                'max_width' => "1500",
                'file_name' => $new_file_name
            );

         $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function my_file_remove($old_file, $new_file, $location)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH.$location.$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
    public function get_all_case_list($limit, $offset, $value = '')
    {
      $subquery = "SELECT `case_no`,`upazilla_id`,`case_master_id`,`court_id`,`court_type_id`,`case_status_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL";
        // $this->db->select('v.*,c.`name` AS country_name,d.`name` AS district_name,d.`name` AS upazilas_name');
        $this->db->select('cl.`first_name` AS  client_first_name,cl.`last_name` AS client_last_name,ch.`case_no`,ct.`name` AS case_name,
c.`name` AS court_name,c.bn_name as court_bn_name,j.`name` AS judge_name,(SELECT `bussines_on_date` FROM `ad_case_history` WHERE `case_master_id`=m.id ORDER BY `bussines_on_date` DESC LIMIT 1) AS next_date,cs.bn_name as status_bn_name,cs.`name` AS status_name,
(SELECT NAME FROM `ad_petitioner_details` WHERE `case_master_id`=m.`id` LIMIT 1 ) AS petitionervs,m.`id`,
u.name as upazila_name,u.bn_name as upazila_bnname,cst.name as case_sub_type_name,cst.bn_name as case_sub_type_bname,
      (SELECT bn_name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_bn_name,
      (SELECT name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_name,cl.mobile as client_mobile,cl.total_person,
      fc.name as file_colour,fc.bn_name as bn_file_colour',FALSE);

        $this->db->from('`ad_master_case AS m');
        $this->db->join("($subquery)  ch","m.id = ch.case_master_id");
      //  $this->db->join('`ad_case_details` AS cd', 'm.`id`=cd.`case_master_id`', 'left');
        $this->db->join('`ad_case_type` AS ct', 'm.`case_type_id`=ct.`id`', 'left');
      //  $this->db->join('`ad_court_details` AS cod', 'm.`id`=cod.`case_master_id`', 'left');
        $this->db->join('`ad_court` AS c', 'ch.`court_id`=c.`id`', 'left');
        $this->db->join('`ad_judge` AS j', 'm.`judge_type_id`=j.`id`', 'left');
        $this->db->join('`ad_case_status` AS cs', 'ch.`case_status_id`=cs.`id`', 'left');
        $this->db->join('`ad_clients` AS cl', 'm.`client_id`=cl.`id`', 'left');
        $this->db->join('`ad_upazilas` AS clu ', 'cl.`upazilas_id`=clu.`id`', 'left');
        $this->db->join('`ad_client_user` AS acl', 'm.`client_user_id`=acl.`id`', 'left');
        $this->db->JOIN('`ad_upazilas` AS u ', 'm.`gr_cr_upazilla_id`=u.`id`', 'left');
        $this->db->JOIN('`ad_case_sub_type` AS cst ', 'm.`case_sub_type_id`=cst.`id`', 'left');
        $this->db->JOIN('`ad_file_colours` AS fc ', 'm.file_colour=fc.`id`', 'left');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->group_start();
    				$this->db->like('m.case_no', $value['name']);
    				$this->db->or_like('acl.mobile', $value['name']);
    				$this->db->or_like('cl.mobile', $value['name']);
            $this->db->or_like('cl.first_name', $value['name']);
            $this->db->or_like('cl.last_name', $value['name']);
            $this->db->or_like('clu.name', $value['name']);
            $this->db->or_like('clu.bn_name', $value['name']);
    				$this->db->group_end();
        }
        if (isset($value) && !empty($value) && isset($value['case_no']) && $value['case_no'] != '') {
          $this->db->where('m.`case_no`', $value['case_no']);
        }
        if (isset($value['case_status']) && $value['case_status'] != '') {
          if($value['case_status']=='closed')
          {
              $this->db->where('LOWER(cs.`name`)', 'closed');
          }else{
              $this->db->where('LOWER(cs.`name`) !=', 'closed');
          }
        }

        $this->db->order_by("m.id", "DESC");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_all_case_list_by_date_range($limit, $offset, $value = '')
    {

      // print_r($value);
      // die;
      $subquery = "SELECT `case_no`,`upazilla_id`,`case_master_id`,`court_id`,`court_type_id`,`bussines_on_date`,`district_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL";

      $this->db->select('cl.`first_name` AS  client_first_name,cl.`last_name` AS client_last_name,ch.`case_no`,ct.`name` AS case_name,m.`registration_date` AS befor_date,m.`first_hearing_date` AS after_date ,
      c.`name` AS court_name,fc.name as file_colour,fc.bn_name as bn_file_colour,c.bn_name as court_bn_name,j.`name` AS judge_name,ch.`bussines_on_date` AS next_date,cs.bn_name as status_bn_name,cs.`name` AS status_name,
      (SELECT NAME FROM `ad_petitioner_details` WHERE `case_master_id`=m.`id` LIMIT 1 ) AS petitionervs,m.`id`,(SELECT name FROM `ad_court_type` WHERE id=m.`court_type_id`) AS court_type_name,
      u.name as upazila_name,u.bn_name as upazila_bnname,cst.name as case_sub_type_name,cst.bn_name as case_sub_type_bname,
      (SELECT bn_name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_bn_name,
      (SELECT name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_name,cl.mobile as client_mobile,cl.total_person',FALSE);

      $this->db->from('`ad_master_case AS m');
      $this->db->join("($subquery)  ch","m.id = ch.case_master_id");
    //  $this->db->join('`ad_case_details` AS cd', 'm.`id`=cd.`case_master_id`', 'left');
      $this->db->join('`ad_case_type` AS ct', 'm.`case_type_id`=ct.`id`', 'left');
      //$this->db->join('`ad_court_details` AS cod', 'm.`id`=cod.`case_master_id`', 'left');
      $this->db->join('`ad_court` AS c', 'ch.`court_id`=c.`id`', 'left');
      $this->db->join('`ad_judge` AS j', 'm.`judge_type_id`=j.`id`', 'left');
      $this->db->join('`ad_case_status` AS cs', 'm.`case_status_id`=cs.`id`', 'left');
      $this->db->join('`ad_clients` AS cl', 'm.`client_id`=cl.`id`', 'left');
      $this->db->join('`ad_client_user` AS acl', 'm.`client_user_id`=acl.`id`', 'left');
      $this->db->JOIN('`ad_upazilas` AS u ', 'm.`gr_cr_upazilla_id`=u.`id`', 'left');
      $this->db->JOIN('`ad_case_sub_type` AS cst ', 'm.`case_sub_type_id`=cst.`id`', 'left');
      $this->db->JOIN('`ad_file_colours` AS fc ', 'm.file_colour=fc.`id`', 'left');
        if (isset($value) && !empty($value) && isset($value['case_no'])  && $value['case_no'] != '') {
            $this->db->where('ch.`case_no`', $value['case_no']);
        }
        if (isset($value) && !empty($value) && isset($value['district_id']) && $value['district_id'] != '') {
            $this->db->where('ch.`district_id`', $value['district_id']);
        }
        if (isset($value) && !empty($value) && isset($value['court_type_id']) && $value['court_type_id'] != '' && $value['court_type_id'] != '--Please Select--') {
            $this->db->where('ch.`court_type_id`', $value['court_type_id']);

        }
        if (isset($value) && !empty($value) && isset($value['from_date']) && $value['from_date'] != '' && isset($value['to_date']) && $value['to_date'] != '') {
          $this->db->where('ch.`bussines_on_date` BETWEEN "'. date('Y-m-d', strtotime($value['from_date'])). '" and "'. date('Y-m-d', strtotime($value['to_date'])).'"');
        }

        if (isset($value) && !empty($value) && isset($value['court_id']) && $value['court_id'] != '' && $value['court_id'] != '--Please Select---') {
            $this->db->where('ch.`court_id`', $value['court_id']);
        }
        if (isset($value) && !empty($value) && isset($value['client_user_id']) && $value['client_user_id'] != '') {
            $client_user_id=$value['client_user_id'];
            $this->db->where_in('m.id', "SELECT `case_master_id` FROM `case_client_user_details` WHERE `client_user_id` = '$client_user_id'",false);
        }
        if (isset($value) && !empty($value) && isset($value['case_master_id']) && $value['case_master_id'] != '') {
            $this->db->where('m.`id`', $value['case_master_id']);
        }

        // $this->db->order_by("ch.`case_no`", "asc");
        $this->db->order_by("m.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_client_dropdown_list()
    {
        return $this->db->query("SELECT id,CONCAT(`first_name`,'-',`mobile`) AS name FROM `ad_clients` ORDER BY `first_name`")->result_array();
    }
    function get_client_name_by_client_id($client_id)
    {
      $result=   $this->db->query("SELECT id,CONCAT(`first_name`) AS name FROM `ad_clients` where id=$client_id")->row();
      return $result->name;
    }
    function get_case_type_dropdown_list()
    {
        return $this->db->query("SELECT * FROM `ad_case_type` ORDER BY `NAME`")->result_array();
    }
    function get_case_type_dropdown_list_by_language($lang)
    {
        if($lang=="english")
        {
          return $this->db->query("SELECT * FROM `ad_case_type` ORDER BY `id`")->result_array();
        }
        return $this->db->query("SELECT id,bn_name as name FROM `ad_case_type` ORDER BY `NAME`")->result_array();
    }
    function get_case_sub_type_dropdown_list($case_type_id)
    {
        return $this->db->query("SELECT * FROM `ad_case_sub_type` where case_type_id='$case_type_id' ORDER BY `NAME`")->result_array();
    }
    function get_case_sub_type_dropdown_list_by_language($case_type_id,$lang)
    {
        if($lang=="english")
        {
          return $this->db->query("SELECT * FROM `ad_case_sub_type` where is_active=1 and case_type_id='$case_type_id' ORDER BY `name`")->result_array();
        }
        return $this->db->query("SELECT id,bn_name as name,case_type_id,is_active FROM `ad_case_sub_type` where is_active and case_type_id='$case_type_id' and bn_name is not null ORDER BY `name`")->result_array();
    }
    function get_case_status_dropdown_list()
    {
        return $this->db->query("SELECT * FROM `ad_case_status` ORDER BY `id`")->result_array();
    }
    function get_case_status_dropdown_list_by_language($lang)
    {
      if($lang=="english")
      {
        return $this->db->query("SELECT * FROM `ad_case_status` ORDER BY `id`")->result_array();
      }
      return $this->db->query("SELECT id,bn_name as name FROM `ad_case_status` ORDER BY `id`")->result_array();
    }
    function get_court_dropdown_list($court_type_id)
    {
        return $this->db->query("SELECT * FROM `ad_court` where court_type_id='$court_type_id' ORDER BY `NAME`")->result_array();
    }
    function get_court_dropdown_list_by_language($court_type_id,$lang)
    {
      if($lang=="english")
      {
          return $this->db->query("SELECT * FROM `ad_court` where court_type_id='$court_type_id' and is_active=1 ORDER BY `id`")->result_array();
      }
      return $this->db->query("SELECT id,bn_name as name FROM `ad_court` where court_type_id='$court_type_id' and is_active=1 ORDER BY `id`")->result_array();
    }
    function get_court_type_dropdown_list()
    {
        return $this->db->query("SELECT * FROM `ad_court_type` ORDER BY `NAME`")->result_array();
    }
    function get_court_type_dropdown_list_by_language($lang)
    {
      if($lang=="english")
      {
        return $this->db->query("SELECT * FROM `ad_court_type` where is_active=1 ORDER BY `id`")->result_array();
      }
        return $this->db->query("SELECT id,bn_name as name,order_no FROM `ad_court_type` where is_active=1 ORDER BY `id`")->result_array();
    }
    function get_judge_dropdown_list()
    {
        return $this->db->query("SELECT * FROM `ad_judge` where is_active=1 ORDER BY id")->result_array();
    }
    function get_judge_dropdown_list_by_language($language)
    {
      if($language=="english")
      {
        return $this->db->query("SELECT * FROM `ad_judge` where is_active=1 ORDER BY id")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name as name FROM `ad_judge` where is_active=1 ORDER BY id")->result_array();
      }

    }
    function get_client_user_dropdown_list()
    {
        return $this->db->query("SELECT c.id,CONCAT(c.`first_name`,'-',c.`mobile`) AS name
                                FROM `ad_client_user` AS c
                                INNER JOIN `tbl_user` AS u ON c.id=u.`client_user_id`
                                WHERE `is_school360_user`='0' AND `is_active`='1'
                                ORDER BY c.`first_name`")->result_array();
    }
    function add_case_master($data)
    {
        return $this->db->insert('ad_master_case', $data);
    }
    function edit_case_master($data, $id)
    {
        return $this->db->update('ad_master_case', $data, array('id' => $id));
    }
    function read_case_master($id)
    {
        return $this->db->get_where('ad_master_case', array('id' => $id))->row();
    }
    function delete_case_master($id)
    {
        return $this->db->delete('ad_master_case', array('id' => $id));
    }
    function add_case_details($data)
    {
        return $this->db->insert('ad_case_details', $data);
    }
    function add_batch_case_client_user_details($data)
    {
        return $this->db->insert_batch('case_client_user_details', $data);
    }

    function edit_case_details($data, $master_id)
    {
        return $this->db->update('ad_case_details', $data, array('case_master_id' => $master_id));
    }
    function read_case_details($master_id)
    {
        return $this->db->get_where('ad_case_details', array('case_master_id' => $master_id))->row();
    }
    function delete_case_details($master_id)
    {
        return $this->db->delete('ad_case_details', array('case_master_id' => $master_id));
    }
    function add_court_details($data)
    {
        return $this->db->insert('ad_court_details', $data);
    }
    function edit_court_details($data, $master_id)
    {
        return $this->db->update('ad_court_details', $data, array('case_master_id' => $master_id));
    }
    function read_court_details($master_id)
    {
        return $this->db->get_where('ad_court_details', array('case_master_id' => $master_id))->row();
    }
    function delete_court_details($master_id)
    {
        return $this->db->delete('ad_court_details', array('case_master_id' => $master_id));
    }
    function add_case_history($data)
    {
        return $this->db->insert('ad_case_history', $data);
    }
    function edit_case_history($data, $id)
    {
        return $this->db->update('ad_case_history', $data, array('id' => $id));
    }
    function read_case_history($master_id)
    {
        return $this->db->get_where('ad_case_history', array('case_master_id' => $master_id))->result_array();
    }
    function read_case_history_by_key_id($id)
    {
        return $this->db->get_where('ad_case_history', array('id' => $id))->result_array();
    }
    function delete_case_history($master_id)
    {
        return $this->db->delete('ad_case_history', array('case_master_id' => $master_id));
    }
    function delete_case_history_by_id($id)
    {
        return $this->db->delete('ad_case_history', array('id' => $id));
    }
    function add_petitioner_details($data)
    {
        return $this->db->insert('ad_petitioner_details', $data);
    }
    function read_petitioner_details($master_id)
    {
        return $this->db->get_where('ad_petitioner_details', array('case_master_id' => $master_id))->result_array();
    }
    function delete_petitioner_details($master_id)
    {
        return $this->db->delete('ad_petitioner_details', array('case_master_id' => $master_id));
    }
    function delete_case_client_user_details($master_id)
    {
        return $this->db->delete('case_client_user_details', array('case_master_id' => $master_id));
    }
    function get_case_first_history_id($master_id)
    {
        $result= $this->db->query("SELECT id FROM `ad_case_history` WHERE `case_master_id`='$master_id' LIMIT 1")->row();
        return $result->id;
    }
    function get_case_last_history_details_by_mastr_id($master_id)
    {
        $case_history= $this->db->query("SELECT h.*,m.`parties_name`,c.`mobile`,u.`bn_name` AS upazila_bn_name,u.`name` AS upazila_name,m.case_sub_type_id
          FROM `ad_case_history` AS h
          INNER JOIN `ad_master_case` AS m ON h.`case_master_id`=m.`id`
          LEFT JOIN `ad_upazilas` AS u ON h.`upazilla_id`=u.`id`
          INNER JOIN`ad_clients` AS c ON m.`client_id`=c.`id`
           WHERE h.`case_master_id`=$master_id  ORDER  BY h.id DESC LIMIT 1")->row();
        if(!empty($case_history))
        {
          return $case_history;
        }
        $data=$this->db->query("SELECT m.`registration_date` AS bussines_on_date,m.`case_status_id` AS case_status_id,
                            c.`first_name` AS parties_name,m.id as case_master_id,m.`reason_for_next_date` AS remarks,m.`court_id`,
                            m.`court_type_id`,m.`district_id`,m.gr_cr_upazilla_id as upazilla_id,m.`case_no`,0 AS id,c.`mobile`,u.`bn_name` AS upazila_bn_name,u.`name` AS upazila_name,m.case_sub_type_id
                             FROM `ad_master_case` AS m
                            INNER JOIN`ad_clients` AS c ON m.`client_id`=c.`id`
                            LEFT JOIN `ad_upazilas` AS u ON m.`gr_cr_upazilla_id`=u.`id`
                            WHERE m.`id`=$master_id")->row();
        return $data;


    }
    function get_case_last_history_details_id_by_mastr_id($master_id)
    {
        $result= $this->db->query("SELECT id,`bussines_on_date` FROM ad_case_history WHERE `case_master_id`=$master_id ORDER BY id DESC LIMIT 1")->row();
        return $result;
    }
    function get_case_last_history_details_by_master_and_key_id($id,$master_id)
    {
        $result= $this->db->query("SELECT * FROM ad_case_history
              WHERE `case_master_id`= $master_id
              AND id<$id ORDER BY id DESC LIMIT 1")->row();
        return $result;
    }
    function get_case_history_details_by_id($id)
    {
        $result= $this->db->query("SELECT h.*,m.`parties_name`,c.`mobile`,u.`bn_name` AS upazila_bn_name,u.`name` AS upazila_name,m.case_sub_type_id
                                  FROM `ad_case_history` AS h
                                  INNER JOIN `ad_master_case` AS m ON h.`case_master_id`=m.`id`
                                  LEFT JOIN `ad_upazilas` AS u ON h.`upazilla_id`=u.`id`
                                  LEFT JOIN`ad_clients` AS c ON m.`client_id`=c.`id`
                                  WHERE h.id=$id")->row();
        return $result;
    }
    public function get_all_case_history_list($limit, $offset, $value = '')
    {

        $this->db->select('h.id,h.`case_no`,j.`name` AS judge_name,h.`bussines_on_date`,h.`hearing_date`,s.`name` AS status_name,h.remarks,
        h.reason_for_next_date,h.reason_for_assignment,h.parties_name,dis.name as district_name,dis.bn_name as district_bn_name,
        ct.name as court_type_name,ct.bn_name AS court_type_bn_name,cc.name as court_name,cc.bn_name as court_bn_name,cl.mobile as client_mobile,cl.total_person,s.bn_name as status_bn_name,
          u.name as upazila_name,u.bn_name as upazila_bnname,cst.name as case_sub_type_name,cst.bn_name as case_sub_type_bname  ');
      //  $this->db->select('h.*');
        $this->db->from('ad_case_history AS h');
        $this->db->join('`ad_master_case` AS m', 'h.`case_master_id`=m.`id`', 'left');
      //  $this->db->join('`ad_case_details` AS d', 'm.`id`=d.`case_master_id`', 'left');
      //  $this->db->join('`ad_court_details` AS c', 'm.`id`=c.`case_master_id`', 'left');
        $this->db->join('`ad_judge` AS j ', 'h.`court_id`=j.`id`', 'left');
        $this->db->join('`ad_case_status` AS s', 'h.`case_status_id`=s.`id`', 'left');
        $this->db->join('`ad_clients` AS cl', 'm.`client_id`=cl.`id`', 'left');
        $this->db->join('`ad_districts` AS dis', 'h.`district_id`=dis.`id`', 'left');
        $this->db->join('`ad_court_type` AS ct', 'h.`court_type_id`=ct.`id`', 'left');
        $this->db->join('`ad_court` AS cc ', 'h.`court_id`=cc.`id`', 'left');
        $this->db->join('`ad_upazilas` AS u ', 'm.`gr_cr_upazilla_id`=u.`id`', 'left');
        $this->db->join('`ad_case_sub_type` AS cst ', 'm.`case_sub_type_id`=cst.`id`', 'left');
// // print_r($value);
// die();
        // if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
        //     $this->db->like('m.first_name', $value['name']);
        // }
        if (isset($value['case_master']) && $value['case_master'] != '') {
            $this->db->where('h.case_master_id', $value['case_master']);
        }
       $this->db->order_by("h.id", "asc");
        // if (isset($limit) && $limit > 0) {
        //     $this->db->limit($limit, $offset);
        // }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_total_client_count()
    {
      $count = $this->db->query("SELECT id FROM `ad_clients`")->num_rows();
      return (int)$count;
    }
    public function get_total_running_case_count()
    {
      $count = $this->db->query("SELECT m.`id` FROM `ad_master_case` AS m
                                  INNER JOIN  (
                                                 SELECT `case_status_id`,`case_master_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL
                                              ) AS history ON m.id= history.`case_master_id`
                                   INNER JOIN  `ad_case_status` AS s ON history.`case_status_id` =s.`id`
                                   WHERE LOWER(s.`name`)!='closed'")->num_rows();
      return (int)$count;
    }
    public function get_total_closed_case_count()
    {
      $count = $this->db->query("SELECT m.`id` FROM `ad_master_case` AS m
                                  INNER JOIN  (
                                                 SELECT `case_status_id`,`case_master_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL
                                              ) AS history ON m.id= history.`case_master_id`
                                   INNER JOIN  `ad_case_status` AS s ON history.`case_status_id` =s.`id`
                                   WHERE LOWER(s.`name`)='closed'")->num_rows();
      return (int)$count;
    }
    public function get_total_important_case_count()
    {
      $count = $this->db->query("SELECT m.id FROM `ad_master_case` AS m  WHERE m.`priority`='H'")->num_rows();
      return (int)$count;
    }
    public function get_all_attendance_list_by_date_range($limit, $offset, $value = '')
    {
        // echo '<pre>';
        // print_r($value); die;
        $this->db->select('ch.`case_no`,ch.`bussines_on_date` AS befor_date,ch.`hearing_date` AS after_date ,
      c.`name` AS court_name,c.bn_name as court_bn_name,(SELECT name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_name,ch.`parties_name`,
      ch.`remarks`,cl.mobile as client_mobile,cl.total_person,s.bn_name as status_bn_name,s.`name` AS status_name,
      u.name as upazila_name,u.bn_name as upazila_bnname,cst.name as case_sub_type_name,cst.bn_name as case_sub_type_bname,
      (SELECT bn_name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_bn_name,cm.is_petitioner,fc.name as file_colour,fc.bn_name as bn_file_colour,
      (SELECT GROUP_CONCAT(pd.`name`) FROM `ad_petitioner_details` pd WHERE `case_master_id`=ch.`case_master_id`)  AS petitioner_or_respondent_names');
        $this->db->from('`ad_master_case` AS cm');
        $this->db->join('`ad_case_history` AS ch', 'cm.id=ch.`case_master_id`');
      //  $this->db->join('`ad_case_details` AS cd', 'cm.`id`=cd.`case_master_id`');
        $this->db->join('`ad_court` AS c', 'ch.`court_id`=c.`id`', 'left');
        $this->db->JOIN('`ad_case_status` AS s', 'ch.`case_status_id`=s.`id`', 'left');
        $this->db->JOIN('`ad_clients` AS cl', 'cm.`client_id`=cl.`id`', 'left');
        $this->db->JOIN('`ad_upazilas` AS u ', 'cm.`gr_cr_upazilla_id`=u.`id`', 'left');
        $this->db->JOIN('`ad_case_sub_type` AS cst ', 'cm.`case_sub_type_id`=cst.`id`', 'left');
         $this->db->join('ad_file_colours AS fc', 'cm.file_colour=fc.id', 'left');
        // $this->db->join('tbl_blood_group AS bg', 's.blood_group_id=bg.id', 'left');

        if (isset($value) && !empty($value) && isset($value['case_no'])  && $value['case_no'] != '') {
            $this->db->where('ch.`case_no`', $value['case_no']);
        }
        if (isset($value) && !empty($value) && isset($value['district_id']) && $value['district_id'] != '') {
            $this->db->where('ch.`district_id`', $value['district_id']);
        }
        if (isset($value) && !empty($value) && isset($value['court_type_id']) && $value['court_type_id'] != '' && $value['court_type_id'] != '--Please Select---') {
            $this->db->where('ch.`court_type_id`', $value['court_type_id']);

        }
        if (isset($value) && !empty($value) && isset($value['from_date']) && $value['from_date'] != '' && isset($value['to_date']) && $value['to_date'] != '') {
          $this->db->where('ch.`bussines_on_date` BETWEEN "'. date('Y-m-d', strtotime($value['from_date'])). '" and "'. date('Y-m-d', strtotime($value['to_date'])).'"');
        }

        if (isset($value) && !empty($value) && isset($value['court_id']) && $value['court_id'] != '' && $value['court_id'] != '--Please Select---') {
            $this->db->where('ch.`court_id`', $value['court_id']);
        }
        if (isset($value) && !empty($value) && isset($value['client_user_id']) && $value['client_user_id'] != '') {
          //  $this->db->where('cm.`client_user_id`', $value['client_user_id']);
            $client_user_id=$value['client_user_id'];
            $this->db->where_in('cm.id', "SELECT `case_master_id` FROM `case_client_user_details` WHERE `client_user_id` = '$client_user_id'",false);
        }
        if (isset($value) && !empty($value) && isset($value['case_master_id']) && $value['case_master_id'] != '') {
            $this->db->where('cm.`id`', $value['case_master_id']);
        }

        $this->db->order_by("cm.`case_no`", "asc");
        $this->db->order_by("ch.`bussines_on_date`", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_all_file_upload_list($limit, $offset, $value = '')
    {
        $this->db->select('m.*,ct.name as form_type_name,ct.bn_name');
        $this->db->from('`ad_file_upload AS m');
        $this->db->join('`ad_forms_types` AS ct', 'm.`forms_types_id`=ct.`id`', 'left');
        if (isset($value) && !empty($value) && isset($value['forms_types_id']) && $value['forms_types_id'] != '') {
            $this->db->where('m.`forms_types_id`', $value['forms_types_id']);
        }
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('m.name', $value['name']);
        }
        $this->db->order_by("m.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    function delete_file_upload($id)
    {
        return $this->db->delete('ad_file_upload', array('id' => $id));
    }
    function add_file_upload($data)
    {
        return $this->db->insert('ad_file_upload', $data);
    }
    function edit_file_upload($data, $id)
    {
        return $this->db->update('ad_file_upload', $data, array('id' => $id));
    }
    function read_file_upload($id)
    {
        return $this->db->get_where('ad_file_upload', array('id' => $id))->row();
    }
    function get_all_case_list_for_dropdown()
    {
        return $this->db->query("SELECT id, case_no AS name FROM `ad_master_case` ORDER BY id desc")->result_array();
    }
    function get_all_case_list_for_dropdown_by_language($lang)
    {
      if($lang=='bangla')
      {
        return $this->db->query("SELECT m.id,CONCAT(c.`bn_name`,' - ',history.case_no) AS name FROM `ad_master_case` AS m
                INNER JOIN `ad_case_sub_type` AS c ON m.`case_sub_type_id`=c.`id`
                INNER JOIN  (
                              SELECT `case_no`,`upazilla_id`,`case_master_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL
                            ) AS history ON m.id= history.`case_master_id`
                ORDER BY id desc")->result_array();
      }
      return $this->db->query("SELECT m.id,CONCAT(c.`name`,' - ',history.case_no) AS name FROM `ad_master_case` AS m
                       INNER JOIN `ad_case_sub_type` AS c ON m.`case_sub_type_id`=c.`id`
                       INNER JOIN  (
                                     SELECT `case_no`,`upazilla_id`,`case_master_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL
                                   ) AS history ON m.id= history.`case_master_id`
                       ORDER BY m.id desc")->result_array();
    }
    function get_all_case_list_with_thana_for_dropdown_by_language($lang)
    {
      if($lang=='bangla')
      {
        return $this->db->query("SELECT m.id,CONCAT(c.`bn_name`,' - ',history.case_no,' - ',u.`bn_name`) AS name FROM `ad_master_case` AS m
                INNER JOIN `ad_case_sub_type` AS c ON m.`case_sub_type_id`=c.`id`
                INNER JOIN  (
                              SELECT `case_no`,`upazilla_id`,`case_master_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL
                            ) AS history ON m.id= history.`case_master_id`
                LEFT JOIN `ad_upazilas` AS u ON m.`gr_cr_upazilla_id`=u.`id`
                ORDER BY m.id DESC")->result_array();
      }
      return $this->db->query("SELECT m.id,CONCAT(c.`name`,' - ',history.case_no,' - ',u.`name`) AS name FROM `ad_master_case` AS m
                       INNER JOIN `ad_case_sub_type` AS c ON m.`case_sub_type_id`=c.`id`
                       INNER JOIN  (
                                     SELECT `case_no`,`upazilla_id`,`case_master_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL
                                   ) AS history ON m.id= history.`case_master_id`
                       LEFT JOIN `ad_upazilas` AS u ON m.`gr_cr_upazilla_id`=u.`id`
                        ORDER BY m.id desc")->result_array();
    }
    function get_case_name_with_case_sub_type_by_language_and_caseid($lang,$case_id,$case_no)
    {
      if($lang=='bangla')
      {
        $result= $this->db->query("SELECT m.id,CONCAT(c.`bn_name`,'  ','$case_no') AS name FROM `ad_master_case` AS m
                INNER JOIN `ad_case_sub_type` AS c ON m.`case_sub_type_id`=c.`id`
                WHERE m.id=$case_id ")->row();
                return $result->name;
      }
      $result= $this->db->query("SELECT m.id,CONCAT(c.`name`,'  ',$case_no) AS name FROM `ad_master_case` AS m
                       INNER JOIN `ad_case_sub_type` AS c ON m.`case_sub_type_id`=c.`id`
                       WHERE m.id=$case_id ")->row();
                  return $result->name;
    }
    public function get_all_case_file_upload_list($limit, $offset, $value = '')
    {

        $this->db->select('m.`case_no`,ct.`name` AS case_name,cfd.`label_name`,cfd.`reference_id`');
        $this->db->from('`ad_master_case AS m');
        $this->db->join('`ad_case_file_details` AS cfd', 'm.`id`=cfd.`case_master_id`');
      //  $this->db->join('`ad_case_details` AS cd', 'm.`id`=cd.`case_master_id`', 'left');
        $this->db->join('`ad_case_type` AS ct', 'm.`case_type_id`=ct.`id`', 'left');
        if (isset($value) && !empty($value) && isset($value['case_master_id']) && $value['case_master_id'] != '') {
            $this->db->where('m.`id`', $value['case_master_id']);
        }
        $this->db->group_by(array("m.`case_no`","ct.`name`","cfd.`label_name`","cfd.`reference_id`"));
        $this->db->order_by("m.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        $data= $query->result_array();
    //
        foreach( $data as $key=>$each ){
            $id=$each['reference_id'];
            $data[$key]['file_list']   = $this->db->query("SELECT * FROM `ad_case_file_details`  WHERE `reference_id`=$id")->result_array();
        }
        return $data;
    }
    function get_max_reference_no_by_case_file_details()
    {

          $data1=$this->db->query("SELECT ISNULL(MAX(`reference_id`)) AS total FROM  `ad_case_file_details`")->row();
        if($data1->total==1)
        {
          return 1;
        }
        $data=$this->db->query("SELECT MAX(`reference_id`) AS total FROM  `ad_case_file_details`")->row();
        return $data->total+1;
    }
    function get_case_file_details_by_reference_no($reference_id)
    {

        return $this->db->query("SELECT cfd.*,m.`case_no`,c.`first_name`,c.`email`,m.`client_id` FROM `ad_case_file_details` AS cfd
                                  INNER JOIN`ad_master_case` AS m ON cfd.`case_master_id`=m.`id`
                                  INNER JOIN `ad_clients` AS c ON m.`client_id`=c.`id`
                                  WHERE cfd.`reference_id`=$reference_id order by cfd.id")->result_array();

    }
    function delete_case_file_details($id)
    {
        return $this->db->delete('ad_case_file_details', array('id' => $id));
    }
    function read_case_file_details($id)
    {
        return $this->db->get_where('ad_case_file_details', array('id' => $id))->row();
    }
    function edit_case_file_details($data, $reference_id)
    {
        return $this->db->update('ad_case_file_details', $data, array('reference_id' => $reference_id));
    }
    function get_case_file_details_with_client_info($id)
    {
      $result=$this->db->query("SELECT d.*,c.`first_name`,m.`case_no`,c.`email`,m.`client_id` FROM `ad_case_file_details` AS d
                                INNER JOIN `ad_master_case` AS m ON d.`case_master_id`=m.`id`
                                INNER JOIN `ad_clients` AS c ON m.`client_id`=c.`id`
                                WHERE d.`id`=$id")->result_array();

        return   $result;
    }
    public function get_all_forms_types_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_forms_types as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
            $this->db->or_like('s.bn_name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_forms_types($data)
    {
        return $this->db->insert('ad_forms_types', $data);
    }


    function edit_forms_types($data, $id)
    {
        return $this->db->update('ad_forms_types', $data, array('id' => $id));
    }


    function read_forms_types($forms_id)
    {
        return $this->db->get_where('ad_forms_types', array('id' => $forms_id))->row();
    }

    function delete_forms_types($forms_id)
    {
        return $this->db->delete('ad_forms_types', array('id' => $forms_id));
    }
    public function checkifexist_forms_types_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_forms_types where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_forms_types_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_forms_types where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    function get_forms_types_list_for_dropdown_by_language($lang)
    {
      if($lang=="bangla")
      {
        return $this->db->query("SELECT id,bn_name as name FROM ad_forms_types ORDER BY id")->result_array();
      }
        return $this->db->query("SELECT id,name FROM ad_forms_types ORDER BY id")->result_array();
    }

    public function checkifexist_case_by_case_no($case_no,$case_sub_type_id)
    {
    //  $count=$this->db->get_where('ad_master_case', array('case_no' => $case_no ,'case_sub_type_id'=>$case_sub_type_id))->num_rows();
          $count = $this->db->query("SELECT h.`id` FROM `ad_case_history` AS h
                                   INNER JOIN `ad_master_case` AS m ON h.case_master_id=m.id
                                   WHERE  h.`case_no`='$case_no' AND m.`case_sub_type_id`=$case_sub_type_id")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    public function checkifexist_update_case_by_case_no($case_no,$case_sub_type_id,$id)
    {
    //  $count=$this->db->get_where('ad_master_case', array('id !='=> $id  , 'case_no' => $case_no  ,'case_sub_type_id'=>$case_sub_type_id ))->num_rows();

    $count = $this->db->query("SELECT h.`id` FROM `ad_case_history` AS h
                             INNER JOIN `ad_master_case` AS m ON h.case_master_id=m.id
                             WHERE h.case_master_id!=$id AND h.`case_no`='$case_no' AND m.`case_sub_type_id`=$case_sub_type_id")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_case_history_date_by_case_master_id($master_id,$business_on_date)
    {
      $count=$this->db->get_where('ad_case_history', array('case_master_id'=> $master_id  , 'bussines_on_date' => $business_on_date))->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_case_history_date_by_case_master_id($master_id,$business_on_date,$id)
    {
      $count=$this->db->get_where('ad_case_history', array('id !='=> $id  , 'case_master_id'=> $master_id  , 'bussines_on_date' => $business_on_date))->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_case_history_status_closed_by_case_master_id($master_id)
    {
      $status= $this->db->get_where('ad_case_status', array('name'=> 'Closed'))->row();
      $count=$this->db->get_where('ad_case_history', array('`hearing_date`'=>NULL , 'case_master_id'=> $master_id  , 'case_status_id' => $status->id))->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function check_client_dependency_by_client_id($client_id)
    {
      $count=$this->db->get_where('ad_master_case', array('client_id' => $client_id))->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //File Colours

    public function get_all_file_colours_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_file_colours as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
            $this->db->or_like('s.bn_name', $value['name']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_file_colours($data)
    {
        return $this->db->insert('ad_file_colours', $data);
    }


    function edit_file_colours($data, $id)
    {
        return $this->db->update('ad_file_colours', $data, array('id' => $id));
    }


    function read_file_colours($forms_id)
    {
        return $this->db->get_where('ad_file_colours', array('id' => $forms_id))->row();
    }

    function delete_file_colours($forms_id)
    {
        return $this->db->delete('ad_file_colours', array('id' => $forms_id));
    }
    public function checkifexist_file_colours_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_file_colours where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_file_colours_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_file_colours where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    function get_file_colour_dropdown_list_by_language($language)
    {
      if($language=="english")
      {
        return $this->db->query("SELECT * FROM `ad_file_colours` ORDER BY id")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name as name FROM `ad_file_colours` ORDER BY id")->result_array();
      }

    }
    function read_case_client_user_details($master_id)
    {
        return $this->db->get_where('case_client_user_details', array('case_master_id' => $master_id))->result_array();
    }
   //case sub type error_reporting
   public function get_case_sub_type_list_report($limit, $offset, $value = '')
   {
     $subquery = "SELECT `case_no`,`upazilla_id`,`case_master_id`,`court_id`,`court_type_id` FROM `ad_case_history` WHERE `hearing_date` IS NULL";
       // echo '<pre>';
       //  print_r($value); die;
       $this->db->select('ch.`case_no` ,
     c.`name` AS court_name,c.bn_name as court_bn_name,(SELECT name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_name,cm.`parties_name`,
     cl.mobile as client_mobile,cl.total_person,s.bn_name as status_bn_name,s.`name` AS status_name,
     u.name as upazila_name,u.bn_name as upazila_bnname,cst.name as case_sub_type_name,cst.bn_name as case_sub_type_bname,
     (SELECT bn_name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_bn_name,cm.is_petitioner,fc.name as file_colour,fc.bn_name as bn_file_colour');
       $this->db->from('`ad_master_case` AS cm');
    //   $this->db->join('`ad_case_details` AS cd', 'cm.`id`=cd.`case_master_id`');
    //   $this->db->join('`ad_court_details` AS cot', 'cm.`id`=cot.`case_master_id`');
       $this->db->join("($subquery)  ch","cm.id = ch.case_master_id");
       $this->db->join('`ad_court` AS c', 'ch.`court_id`=c.`id`', 'left');
       $this->db->JOIN('`ad_case_status` AS s', 'cm.`case_status_id`=s.`id`', 'left');
       $this->db->JOIN('`ad_clients` AS cl', 'cm.`client_id`=cl.`id`', 'left');
       $this->db->JOIN('`ad_upazilas` AS u ', 'cm.`gr_cr_upazilla_id`=u.`id`', 'left');
       $this->db->JOIN('`ad_case_sub_type` AS cst ', 'cm.`case_sub_type_id`=cst.`id`', 'left');
       $this->db->join('ad_file_colours AS fc', 'cm.file_colour=fc.id', 'left');
       // $this->db->join('tbl_blood_group AS bg', 's.blood_group_id=bg.id', 'left');
       if (isset($value) && !empty($value) && isset($value['case_type_id'])  && $value['case_type_id'] != '') {
           $this->db->where('cm.`case_type_id`', $value['case_type_id']);
       }
       if (isset($value) && !empty($value) && isset($value['case_status_id'])  && $value['case_status_id'] != '') {
           $this->db->where('cm.`case_status_id`', $value['case_status_id']);
       }
       if (isset($value) && !empty($value) && isset($value['case_sub_type_id']) && $value['case_sub_type_id'] != '') {
           $this->db->where('cm.`case_sub_type_id`', $value['case_sub_type_id']);
       }
       if (isset($value) && !empty($value) && isset($value['case_no'])  && $value['case_no'] != '') {
           $this->db->where('ch.`case_no`', $value['case_no']);
       }
       if (isset($value) && !empty($value) && isset($value['district_id']) && $value['district_id'] != '') {
           $this->db->where('cm.`gr_cr_district_id`', $value['district_id']);
       }
       if (isset($value) && !empty($value) && isset($value['court_type_id']) && $value['court_type_id'] != '' && $value['court_type_id'] != '--Please Select---') {
           $this->db->where('ch.`court_type_id`', $value['court_type_id']);
       }
       // if (isset($value) && !empty($value) && isset($value['from_date']) && $value['from_date'] != '' && isset($value['to_date']) && $value['to_date'] != '') {
       //   $this->db->where('ch.`bussines_on_date` BETWEEN "'. date('Y-m-d', strtotime($value['from_date'])). '" and "'. date('Y-m-d', strtotime($value['to_date'])).'"');
       // }

       if (isset($value) && !empty($value) && isset($value['court_id']) && $value['court_id'] != '' && $value['court_id'] != '--Please Select---') {
           $this->db->where('ch.`court_id`', $value['court_id']);
       }
       if (isset($value) && !empty($value) && isset($value['client_user_id']) && $value['client_user_id'] != '') {
         $client_user_id=$value['client_user_id'];
         $this->db->where_in('cm.id', "SELECT `case_master_id` FROM `case_client_user_details` WHERE `client_user_id` = '$client_user_id'",false);
       }
       if (isset($value) && !empty($value) && isset($value['case_master_id']) && $value['case_master_id'] != '') {
           $this->db->where('cm.`id`', $value['case_master_id']);
       }

       $this->db->order_by("cm.`id`", "desc");
       $query = $this->db->get();
       return $query->result_array();
   }
   public function get_order_sheet_report($limit, $offset, $value = '')
   {

       // echo '<pre>';
       // print_r($value); die;
       $this->db->select('ch.`case_no`,ch.`bussines_on_date` AS befor_date,ch.`hearing_date` AS after_date ,
     c.`name` AS court_name,c.bn_name as court_bn_name,(SELECT name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_name,ch.`parties_name`,
     ch.`remarks`,cl.mobile as client_mobile,cl.total_person,s.bn_name as status_bn_name,s.`name` AS status_name,
     u.name as upazila_name,u.bn_name as upazila_bnname,cst.name as case_sub_type_name,cst.bn_name as case_sub_type_bname,
     (SELECT bn_name FROM `ad_court_type` WHERE id=ch.`court_type_id`) AS court_type_bn_name,cm.is_petitioner,fc.name as file_colour,
     fc.bn_name as bn_file_colour,ch.reason_for_next_date,cu.first_name as advocate_name,cu.mobile,cl.first_name as client_name,
     (SELECT  name FROM `ad_petitioner_details` WHERE `case_master_id`=cm.id LIMIT 1) as petision_name');
       $this->db->from('`ad_master_case` AS cm');
       $this->db->join('`ad_case_history` AS ch', 'cm.id=ch.`case_master_id`');
       $this->db->join('`ad_court` AS c', 'ch.`court_id`=c.`id`', 'left');
       $this->db->JOIN('`ad_case_status` AS s', 'ch.`case_status_id`=s.`id`', 'left');
       $this->db->JOIN('`ad_clients` AS cl', 'cm.`client_id`=cl.`id`', 'left');
       $this->db->JOIN('`ad_upazilas` AS u ', 'cm.`gr_cr_upazilla_id`=u.`id`', 'left');
       $this->db->JOIN('`ad_case_sub_type` AS cst ', 'cm.`case_sub_type_id`=cst.`id`', 'left');
       $this->db->join('ad_file_colours AS fc', 'cm.file_colour=fc.id', 'left');
       $this->db->join('ad_client_user AS cu', 'cm.client_user_id=cu.id', 'left');

       if (isset($value) && !empty($value) && isset($value['case_master_id']) && $value['case_master_id'] != '') {
           $this->db->where('cm.`id`', $value['case_master_id']);
       }

       $this->db->order_by("cm.`case_no`", "asc");
       $this->db->order_by("ch.`bussines_on_date`", "asc");
       if (isset($limit) && $limit > 0) {
           $this->db->limit($limit, $offset);
       }
       $query = $this->db->get();
       return $query->result_array();
   }
   public function get_all_missed_attendance_history_list()
   {
      $list= $this->db->query("SELECT tt.id,tt.`case_no`,m.id AS case_id,m.`is_petitioner`,ac.`first_name`,ac.`last_name`,ac.`mobile`,
        tt.bussines_on_date,cst.`name` AS case_sub_name,cst.`bn_name` AS case_sub_bnname,u.`bn_name` AS upazila_bnname,u.`name` AS upazila_name
        FROM ad_case_history tt
        INNER JOIN
          (SELECT case_master_id, MAX(bussines_on_date) AS MaxDateTime
          FROM ad_case_history
          GROUP BY case_master_id) groupedtt ON tt.case_master_id = groupedtt.case_master_id AND tt.bussines_on_date = groupedtt.MaxDateTime AND tt.bussines_on_date<CAST(NOW() AS DATE)
        INNER JOIN `ad_master_case` AS m ON tt.`case_master_id`=m.`id`
        LEFT JOIN `ad_clients` AS ac ON m.`client_id`=ac.`id`
        LEFT JOIN `ad_case_sub_type` AS cst ON m.`case_sub_type_id`=cst.`id`
        LEFT JOIN `ad_upazilas`  AS u ON m.`gr_cr_upazilla_id`=u.`id`
        WHERE m.`id` NOT IN(SELECT h.`case_master_id` FROM `ad_case_history` AS h
                            INNER JOIN `ad_case_status` AS s ON h.`case_status_id`=s.`id`
                            WHERE LOWER(s.`name`)='closed' AND h.`hearing_date` IS NULL )
        ORDER BY tt.bussines_on_date DESC")->result_array();
        return $list;
   }
   public function get_clients_by_mobile($mobile,$name)
   {
         $data_row=$this->db->get_where('ad_clients', array('mobile'=> $mobile  , 'first_name' => $name))->row();
     return $data_row;
   }
   public function get_clients_address_by_language($lang,$upazila_id)
   {
     if($lang=='bangla')
     {
        $data_row=$this->db->query("SELECT  u.`bn_name` AS u_name,d.bn_name AS d_name FROM  `ad_upazilas` AS u
                                    INNER JOIN `ad_districts` AS d ON u.`district_id`=d.id
                                    WHERE u.`id`=$upazila_id")->row();
        return $data_row->u_name.','.$data_row->d_name;
     }else{
       $data_row=$this->db->query("SELECT  u.`name` AS u_name,d.name AS d_name FROM  `ad_upazilas` AS u
                                   INNER JOIN `ad_districts` AS d ON u.`district_id`=d.id
                                   WHERE u.`id`=$upazila_id")->row();
       return $data_row->u_name.','.$data_row->d_name;
     }

   }
   function add_clients($data)
   {
       return $this->db->insert('ad_clients', $data);
   }
   public function checkifexist_update_clients_by_mobile($mobile,$name,$id)
   {
     $count = $this->db->query("SELECT id FROM ad_clients where mobile = '$mobile' and  first_name='$name' and id !='$id'")->num_rows();
     if((int)$count==0){return 0;}else{return 1;}
   }
   function edit_clients($data, $id)
   {
       return $this->db->update('ad_clients', $data, array('id' => $id));
   }
   public function checkifexist_advocate_setting()
   {
     $count = $this->db->query("SELECT id FROM ad_advocate_setting where `name` is not null")->num_rows();
     if((int)$count==1){return 0;}else{return 1;}
   }
   function read_advocate_setting()
   {
       $result= $this->db->query("SELECT * FROM ad_advocate_setting")->row();
       return $result;
   }

    function get_client_info_by_case_master_id($case_master_id)
   {
    $result= $this->db->query("SELECT s.`id`,u.`first_name` AS user_name,c.`first_name`,c.`last_name`,c.`mobile`,c.`id` AS client_id
            FROM `ad_master_case` AS s
            INNER JOIN `ad_client_user` AS u ON u.`id` = s.`client_user_id`
            INNER JOIN `ad_clients` AS c ON c.`id` = s.`client_id`
            WHERE s.`id`= $case_master_id")->result_array();
        return $result;
   }
   public function get_email_configuration()
   {
     return $this->db->get('tbl_mail_configuration')->row();
   }
  }
