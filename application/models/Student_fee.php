<?php

class Student_fee extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }


    public function getFeeRemoveData($year, $class_id, $shift_id, $section_id, $group_id, $fee_sub_category_id)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_fee_remove` WHERE `class_id` = '$class_id' AND
`shift_id` = '$shift_id' AND `section_id` = '$section_id' AND `group_id` = '$group_id'
AND `year` = '$year' AND `fee_sub_category_id` = '$fee_sub_category_id'")->result_array();
        $r_data = array();
        foreach ($rows as $row) {
            $r_data[$row['student_id']]['id'] = $row['id'];
            $r_data[$row['student_id']]['student_id'] = $row['student_id'];
        }
        return $r_data;
    }


    public function getStudentFessDataForNormalReceipt($from_date, $to_date, $class_id, $section_id)
    {
        $fee_accounts = $this->db->query("SELECT sf.*,s.name as student_name,s.roll_no,s.student_code FROM `tbl_student_fee_account` as sf
	     inner join tbl_student as s on s.id = sf.student_id
	     WHERE (`payment_date` BETWEEN '$from_date' AND '$to_date')
	     AND sf.`class_id` = '$class_id' AND sf.`section_id` = '$section_id'
	     ORDER BY s.roll_no ASC")->result_array();

        $report_data = array();
        $i = 0;
        foreach ($fee_accounts as $account_row) {
            $report_data[$i]['student_name'] = $account_row['student_name'];
            $report_data[$i]['roll_no'] = $account_row['roll_no'];
            $report_data[$i]['student_code'] = $account_row['student_code'];
            $report_data[$i]['total_paid_amount'] = $account_row['amount'];
            $report_data[$i]['student_return_money'] = $account_row['student_return_money'];
            $report_data[$i]['payment_date'] = $account_row['payment_date'];
            $report_data[$i]['year'] = $account_row['year'];

            $account_id =  $account_row['id'];


            $j = 0;
            $category_wise_fee = $this->db->query("select c.*,fc.name as category_name from tbl_category_wise_fee as c inner join tbl_fee_category as fc on fc.id = c.category_id WHERE c.fee_account_id = '$account_id'")->result_array();
            if (!empty($category_wise_fee)) {
                foreach ($category_wise_fee as $category_row) {
                    $report_data[$i]['fee_item'][$j]['category_name'] = $category_row['category_name'];
                    $report_data[$i]['fee_item'][$j]['allocated_fee'] = $category_row['allocated_fee'];
                    $report_data[$i]['fee_item'][$j]['paid_amount'] = $category_row['paid_amount'];
                    $report_data[$i]['fee_item'][$j]['due_amount'] = $category_row['due_amount'];
                    $j++;
                }
            }

            $month_wise_fee = $this->db->query("select m.* from tbl_month_wise_fee as m WHERE m.fee_account_id = '$account_id'")->result_array();
            if (!empty($month_wise_fee)) {
                foreach ($month_wise_fee as $month_row) {
                    $dateObj   = DateTime::createFromFormat('!m', $month_row['month']);
                    $monthName = $dateObj->format('F');
                    $report_data[$i]['fee_item'][$j]['category_name'] = $monthName;
                    $report_data[$i]['fee_item'][$j]['allocated_fee'] = $month_row['allocated_fee'];
                    $report_data[$i]['fee_item'][$j]['paid_amount'] = $month_row['paid_amount'];
                    $report_data[$i]['fee_item'][$j]['due_amount'] = $month_row['due_amount'];
                    $j++;
                }
            }


            $absent_and_play_truant_fine = $this->db->query("select a.* from tbl_absent_and_play_truant_fine as a WHERE a.fee_account_id = '$account_id'")->result_array();
            if (!empty($absent_and_play_truant_fine)) {
                foreach ($absent_and_play_truant_fine as $absent_and_play_truant_row) {
                    if ($absent_and_play_truant_row['is_absent_fine'] == '1') {
                        $report_data[$i]['fee_item'][$j]['category_name'] = 'Absent Fine';
                    } else {
                        $report_data[$i]['fee_item'][$j]['category_name'] = 'Play Truant Fine';
                    }
                    $report_data[$i]['fee_item'][$j]['allocated_fee'] = $absent_and_play_truant_row['allocated_fine'];
                    $report_data[$i]['fee_item'][$j]['paid_amount'] = $absent_and_play_truant_row['paid_amount'];
                    $report_data[$i]['fee_item'][$j]['due_amount'] = $absent_and_play_truant_row['due_amount'];
                    $j++;
                }
            }
            $i++;
        }
        //echo '<pre>';
        //print_r($report_data);
        //die;
        return $report_data;
    }

    public function getFeeWaiverListFromTotalAmount($class_id, $section_id, $group, $year)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_waiver_from_total_amount` WHERE `class_id` = '$class_id' AND `section_id` = '$section_id'
AND `group` = '$group' AND `year` = '$year'")->result_array();
        $waiver = array();
        foreach ($rows as $row) {
            $waiver[$row['student_id']]['id'] = $row['id'];
            $waiver[$row['student_id']]['student_id'] = $row['student_id'];
            $waiver[$row['student_id']]['amount'] = $row['amount'];
        }
        return $waiver;
    }

    public function getExpenseDataForTrailBalance($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        return $this->db->query("SELECT SUM(e.`amount`) AS total_amount,ec.`name` AS cat_name FROM `tbl_ba_expense_details` AS e
LEFT JOIN `tbl_ba_expense_category` AS ec ON ec.`id` = e.`expense_category_id`
LEFT JOIN `tbl_ba_expense` AS em ON em.`id` = e.`expense_id`
WHERE em.`date` BETWEEN '$from_date' AND '$to_date'
GROUP BY e.`expense_category_id`")->result_array();
    }


    public function getAssetDataForTrialBalance($data)
    {
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $user_wise_collection = $this->getUserWiseCollection($from_date, $to_date);
        $user_wise_expense = $this->getUserWiseExpense($from_date, $to_date);

        $users = $this->db->query("SELECT * FROM `tbl_user`")->result_array();

        $asset_data = array();
        $i = 0;
        foreach ($users as $user) {
            $asset_data[$i]['leader_name'] = 'Cash (' . $user['name'] . ')';
            //echo $asset_data[$i]['leader_name'];
            //die;

            $asset_data[$i]['dr'] = 0;
            $asset_data[$i]['cr'] = 0;
            if (isset($user_wise_collection[$user['id']])) {
                $asset_data[$i]['dr'] = $user_wise_collection[$user['id']]['amount'];
            }



            if (isset($user_wise_expense[$user['id']])) {
                if ($user_wise_expense[$user['id']]['amount'] <= $user_wise_collection[$user['id']]['amount']) {
                    $asset_data[$i]['dr'] = $user_wise_collection[$user['id']]['amount'] - $user_wise_expense[$user['id']]['amount'];
                    $asset_data[$i]['cr'] = 0;
                }
            }

            if (isset($user_wise_expense[$user['id']]) && isset($user_wise_collection[$user['id']])) {
                if ($user_wise_expense[$user['id']]['amount'] > $user_wise_collection[$user['id']]['amount']) {
                    $asset_data[$i]['cr'] = $user_wise_expense[$user['id']]['amount'] - $user_wise_collection[$user['id']]['amount'];
                    $asset_data[$i]['dr'] = 0;
                }
            }

            // echo '<pre>';
            // print_r($asset_data);
            // die;


            $i++;
        }
        return $asset_data;
    }

    public function getUserWiseCollection($from_date, $to_date)
    {
        $rows =  $this->db->query("SELECT c.`user_id`,SUM(c.`total_paid_amount`) AS amount FROM `tbl_fee_collection` AS c
WHERE c.`date` BETWEEN '$from_date' AND '$to_date'
GROUP BY c.`user_id`")->result_array();
        $r_data = array();
        foreach ($rows as $row) {
            $r_data[$row['user_id']]['user_id'] = $row['user_id'];
            $r_data[$row['user_id']]['amount'] = $row['amount'];
        }
        return $r_data;
    }

    public function getUserWiseExpense($from_date, $to_date)
    {
        $rows = $this->db->query("SELECT e.`user_id`,SUM(e.`expense_total_amount`) AS amount FROM `tbl_ba_expense` AS e
WHERE e.`date` BETWEEN '$from_date' AND '$to_date'
GROUP BY e.`user_id`")->result_array();
        $r_data = array();
        foreach ($rows as $row) {
            $r_data[$row['user_id']]['user_id'] = $row['user_id'];
            $r_data[$row['user_id']]['amount'] = $row['amount'];
        }
        return $r_data;
    }


    public function getCollectionDetailsStandard($data)
    {
        $class_id = $data['class_id'];
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        //echo $from_date . '/' . $to_date;
        //die;

        if ($class_id == 'all') {
            $where = "";
        } else {
            $where = " AND c.`class_id` = '$class_id'";
        }

        $get_all_sub_category = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
        $sub_category_wise_data = array();
        $j = 0;
        while ($j < count($get_all_sub_category)) {
            $sub_category_id = $get_all_sub_category[$j]['id'];
            $paid_info = $this->db->query("SELECT SUM(cc.paid_amount) AS total_amout
             FROM `tbl_fee_collection_details` AS cc
             LEFT JOIN `tbl_fee_collection` AS c ON c.`id` = cc.`collection_id`
WHERE cc.`sub_category_id` = '$sub_category_id'
AND (c.`date` BETWEEN '$from_date' AND '$to_date') $where")->result_array();
            if (!empty($paid_info)) {
                $sub_category_wise_data[$j]['total_amout'] = $paid_info[0]['total_amout'];
                $sub_category_wise_data[$j]['sub_category_id'] = $sub_category_id;
                $sub_category_wise_data[$j]['sub_category_name'] = $get_all_sub_category[$j]['name'];
            } else {
                $sub_category_wise_data[$j]['total_amout'] = 0.00;
                $sub_category_wise_data[$j]['sub_category_id'] = $sub_category_id;
                $sub_category_wise_data[$j]['sub_category_name'] = $get_all_sub_category[$j]['name'];
            }
            $j++;
        }

        $late_fee = $this->db->query("SELECT SUM(amount) as total_late_fee FROM tbl_fee_late_fine WHERE collection_id
         IN(SELECT id FROM tbl_fee_collection WHERE (date BETWEEN '$from_date' AND '$to_date'))
         ")->result_array();

        // echo '<pre>';
        // print_r($late_fee);
        //die;

        $sub_category_wise_data[$j]['total_amout'] = $late_fee[0]['total_late_fee'];
        $sub_category_wise_data[$j]['sub_category_id'] = 0;
        $sub_category_wise_data[$j]['sub_category_name'] = "Late Fee";


        $transport_fee = $this->db->query("SELECT SUM(amount) as total_transport_fee FROM tbl_student_monthly_transport_fee WHERE collection_id
         IN(SELECT id FROM tbl_fee_collection WHERE (date BETWEEN '$from_date' AND '$to_date'))
         ")->result_array();

        $sub_category_wise_data[$j + 1]['total_amout'] = $transport_fee[0]['total_transport_fee'];
        $sub_category_wise_data[$j + 1]['sub_category_id'] = 0;
        $sub_category_wise_data[$j + 1]['sub_category_name'] = "Transport Fee";

        return $sub_category_wise_data;
        //echo '<pre>';
    //	print_r($sub_category_wise_data);
        //die;
    }


    public function getStudentFeeStatusDetailsStandard($data)
    {
        $year = $data['year'];
        $student_id = $data['student_id'];

        $monthly_data = array();
        $i = 1;
        while ($i <= 12) {
            $get_all_sub_category = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
            $j = 0;
            while ($j < count($get_all_sub_category)) {
                $fee_type = $get_all_sub_category[$j]['fee_type'];
                $sub_category_id = $get_all_sub_category[$j]['id'];
                $paid_info = $this->db->query("SELECT cd.sub_category_id,cd.paid_amount,c.date,c.receipt_no FROM `tbl_fee_collection_details` as cd
                INNER JOIN tbl_fee_collection as c ON c.id = cd.collection_id
                WHERE cd.`student_id` = '$student_id' AND cd.`year` = '$year' AND cd.`month` = '$i' AND cd.`sub_category_id` = '$sub_category_id' AND cd.`fee_type` = '$fee_type'")->result_array();

                if (!empty($paid_info)) {
                    $paid_amount_single = 0;
                    $receipt_no_single = '';
                    $date_single = '';
                    $receipt_no_and_date_single = '';
                    foreach ($paid_info as $paid_info_single) {
                      $paid_amount_single += $paid_info_single['paid_amount'];
                      $receipt_no_single .= $paid_info_single['receipt_no'] . ',';
                      $date_single .= $paid_info_single['date'] . ',';
                      $receipt_no_and_date_single .=  $paid_info_single['receipt_no'] . '<br>(' . date("d-m-Y", strtotime($paid_info_single['date'])) . ')<br>';
                    }
                    $monthly_data[$i][$j]['paid_amount'] = $paid_amount_single;
                    $monthly_data[$i][$j]['sub_category_id'] = $paid_info[0]['sub_category_id'];
                    $monthly_data[$i][$j]['receipt_no'] = rtrim($receipt_no_single, ',');
                    $monthly_data[$i][$j]['date'] = rtrim($date_single, ',');
                    $monthly_data[$i][$j]['receipt_no_and_date'] = $receipt_no_and_date_single;
                } else {
                    $monthly_data[$i][$j]['paid_amount'] = 0.00;
                    $monthly_data[$i][$j]['sub_category_id'] = $sub_category_id;
                    $monthly_data[$i][$j]['receipt_no'] = "";
                    $monthly_data[$i][$j]['date'] = "";
                    $monthly_data[$i][$j]['receipt_no_and_date'] = "";
                }
                $j++;
            }

            $get_special_care_fee_amount = $this->db->query("SELECT * FROM tbl_monthly_special_care_fee WHERE student_id = '$student_id' AND `year` = '$year' AND `month` = '$i'")->result_array();
            if (empty($get_special_care_fee_amount)) {
                $special_care_fee_amount = 0.00;
                $special_care_receipt_no = "";
                $special_care_date = "";
            } else {
                $spe_amount = 0;
                $spe_receipt_no = "";
                $spe_date = "";
                foreach ($get_special_care_fee_amount as $spc_row):

                 $spe_amount = $spe_amount + $spc_row['amount'];
                $spe_receipt_no = ($spe_receipt_no .'/'. $spc_row['receipt_no']);
                $spe_date = ($spe_date .'/'. $spc_row['date']);
                endforeach;

                $special_care_fee_amount = $spe_amount;
                $special_care_receipt_no = $spe_receipt_no;
                $special_care_date = $spe_date;
            }

            //if($i == 7){
            // echo '<pre>';
            //print_r($special_care_receipt_no);
            // die;
            // }

            $monthly_data[$i][$j]['paid_amount'] = $special_care_fee_amount ;
            $monthly_data[$i][$j]['sub_category_id'] = 'SCF';
            $monthly_data[$i][$j]['receipt_no'] = $special_care_receipt_no;
            $monthly_data[$i][$j]['date'] = $special_care_date;
			$monthly_data[$i][$j]['receipt_no_and_date'] = "";

            $j++;
            $get_transport_fee_amount = $this->db->query("SELECT * FROM tbl_student_monthly_transport_fee WHERE student_id = '$student_id' AND `year` = '$year' AND `month` = '$i'")->result_array();
            if (empty($get_transport_fee_amount)) {
                $transport_fee_fee_amount = 0.00;
                $transport_fee_receipt_no = "";
                $transport_fee_date = "";
            } else {
                $transport_fee_fee_amount = $get_transport_fee_amount[0]['amount'];
                $transport_fee_receipt_no = "";
                $transport_fee_date = $get_transport_fee_amount[0]['date'];
            }

            $monthly_data[$i][$j]['paid_amount'] = $transport_fee_fee_amount ;
            $monthly_data[$i][$j]['sub_category_id'] = 'TF';
            $monthly_data[$i][$j]['receipt_no'] = $transport_fee_receipt_no;
            $monthly_data[$i][$j]['date'] = $transport_fee_date;
			$monthly_data[$i][$j]['receipt_no_and_date'] = "";


            $i++;
        }

        //echo '<pre>';
        //print_r($monthly_data);
      //  die;
        return $monthly_data;
    }


    public function createCollectionSheet($data)
    {
        $class_id = $data['class_id'];
        $year = $data['year'];
        $student_pay_amount = $data['amount'];
        $student_id = $data['student_id'];

        $waiver_setting = $this->db->query("SELECT * FROM tbl_fee_waiver_setting WHERE `year` = '$year'")->row();
        if (empty($waiver_setting)) {
            $sdata['exception'] = "Please set waiver setting.";
            $this->session->set_userdata($sdata);
            redirect("student_fees/student_fee_add_for_normal");
        }
        $waiver_applicable_month = $waiver_setting->applicable_month;
        $applicable_month_for_scholarship = $waiver_setting->applicable_month_for_scholarship;

        $waiver = $this->db->query("SELECT * FROM tbl_student_fee_waiver WHERE `year` = '$year' AND `student_id` = '$student_id'")->row();
        if (empty($waiver)) {
            $waiver_percentage_of_amount = 0;
        } else {
            $waiver_percentage_of_amount = $waiver->percentage_of_amount;
        }

        $scholarship = $this->db->query("SELECT * FROM tbl_student_scholarship WHERE `year` = '$year' AND `student_id` = '$student_id'")->row();


        $priority_setting = $this->db->query("SELECT priority_id FROM tbl_priority_setting WHERE class_id = '$class_id'")->result_array();
        if (!empty($priority_setting)) {
            $priority_id = $priority_setting[0]['priority_id'];
            $priority_details = $this->db->query("SELECT * FROM tbl_student_fee_priority_details WHERE priority_id = '$priority_id' ORDER BY priority_number")->result_array();
            if (!empty($priority_details)) {
                $monthly_fee_for_this_student = $this->db->query("SELECT `amount` FROM `tbl_class_wise_monthly_fee` WHERE `year` = '$year' AND `class_id`  = '$class_id'")->result_array();
                if (empty($monthly_fee_for_this_student)) {
                    //SSMFA = Please set student monthly fee amount./student monthly fee amount not set
                    $sdata['exception'] = "Please set student monthly fee amount.";
                    $this->session->set_userdata($sdata);
                    redirect("student_fees/student_fee_add_for_normal");
                } else {
                    $monthly_fee_for_this_student = $monthly_fee_for_this_student[0]['amount'];
                    $monthly_fee_for_this_student_without_waiver = $monthly_fee_for_this_student - ($monthly_fee_for_this_student * $waiver_percentage_of_amount / 100);
                }

                $category_wise_allocated_fee = $this->getAllCategoryWiseFee($class_id, $year);
                $total_absent_fee = $this->getAbsentFeeListBytudentId($student_id, $year);
                $total_play_truant_fine = $this->getPlayTruantFineListBytudentId($student_id, $year);
                $category_list = $this->getCategory();
                $already_paid_by_category = $this->getStudentPaidFeeByCategory($student_id, $year, '', '');
                $already_paid_by_month = $this->getStudentPaidFeeByMonth($student_id, $year, '', '');

                $already_absent_and_play_truant_fine_paid = $this->getStudentAbsentAndPlayTruantFinePaid($student_id, $year, '', '');
                if (isset($already_absent_and_play_truant_fine_paid[1])) {
                    $already_absent_fine_paid = $already_absent_and_play_truant_fine_paid[1]['paid_amount'];
                } else {
                    $already_absent_fine_paid = 0;
                }
                if (isset($already_absent_and_play_truant_fine_paid[0])) {
                    $already_play_truant_fine_paid = $already_absent_and_play_truant_fine_paid[0]['paid_amount'];
                } else {
                    $already_play_truant_fine_paid = 0;
                }
//                echo '<pre>';
//                print_r($already_absent_and_play_truant_fine_paid);
//                die;

                //fee collection process
                $monthly_fee_list = array();
                $category_wise_fee_list = array();
                $absent_and_play_truant_fine_list = array();
                $student_return_money = 0;
                $i = 0;
                $j = 0;
                $k = 0;
                foreach ($priority_details as $row):
                    if ($row['is_month'] == 1) {
                        //monthly fee
//                        echo $waiver_applicable_month;
//                        die;
                        //check waiver
                        if ($row['month'] >= $waiver_applicable_month) {
                            $monthly_fee_for_this_student = $monthly_fee_for_this_student_without_waiver;
                        }
                        //check waiver

                        //check scholarship
                        if (!empty($scholarship)) {
                            if ($row['month'] >= $applicable_month_for_scholarship) {
                                $monthly_fee_for_this_student = 0;
                            }
                        }
                        //check scholarship

                        $monthly_fee_collection_amount = ($monthly_fee_for_this_student * $row['percentage_of_amount']) / 100;

                        if (!empty($already_paid_by_month) && isset($already_paid_by_month[$row['month']]['paid_amount'])) {
                            $monthly_fee_payable_for_this_transaction = $monthly_fee_collection_amount - $already_paid_by_month[$row['month']]['paid_amount'];
                            $already_paid_amount = $already_paid_by_month[$row['month']]['paid_amount'];
                        } else {
                            $monthly_fee_payable_for_this_transaction = $monthly_fee_collection_amount;
                            $already_paid_amount = 0;
                        }

                        if ($monthly_fee_payable_for_this_transaction <= 0) {
                            // echo $monthly_fee_payable_for_this_transaction;
                            //  die;
                            continue;
                        }

                        $monthly_fee = array();
                        if ($student_pay_amount > $monthly_fee_payable_for_this_transaction) {
                            $monthly_fee[$i]['month'] = $row['month'];
                            $monthly_fee[$i]['allocated_fee'] = $monthly_fee_for_this_student;
                            $monthly_fee[$i]['paid_amount'] = $monthly_fee_payable_for_this_transaction;
                            $monthly_fee[$i]['already_paid_amount'] = $already_paid_amount;
                            $monthly_fee[$i]['due_amount'] = $monthly_fee_for_this_student - ($monthly_fee_payable_for_this_transaction + $already_paid_amount);
                            array_push($monthly_fee_list, $monthly_fee);
                            $student_pay_amount -= $monthly_fee_payable_for_this_transaction;
                        } else {
                            $monthly_fee[$i]['month'] = $row['month'];
                            $monthly_fee[$i]['allocated_fee'] = $monthly_fee_for_this_student;
                            $monthly_fee[$i]['already_paid_amount'] = $already_paid_amount;
                            $monthly_fee[$i]['paid_amount'] = $student_pay_amount;
                            $monthly_fee[$i]['due_amount'] = $monthly_fee_for_this_student - ($student_pay_amount + $already_paid_amount);
                            array_push($monthly_fee_list, $monthly_fee);
                            $student_pay_amount -= $student_pay_amount;
                            break;
                        }
                        $i++;

                    //end monthly fee
                    } elseif ($row['is_absent_fine'] == 1 || $row['is_play_truant_fine'] == 1) {
                        //absent & play truant fine

                        if ($row['is_absent_fine'] == 1) {
                            $is_absent_fine = 1;
                        } else {
                            $is_absent_fine = 0;
                        }

                        if ($is_absent_fine == 1) {
                            $already_paid = $already_absent_fine_paid;
                        } else {
                            $already_paid = $already_play_truant_fine_paid;
                        }

                        $absent_and_play_truant_fine = array();
                        if ($row['is_absent_fine'] == 1) {
                            if ($total_absent_fee <= 0) {
                                continue;
                            }
                            $allocated_fine = $total_absent_fee;
                            $collected_absent_or_play_truant_fee = ($total_absent_fee * $row['percentage_of_amount']) / 100;
                            $paidable_absent_or_play_truant_fee = $total_absent_fee - $already_paid;
                        } else {
                            if ($total_play_truant_fine <= 0) {
                                continue;
                            }
                            $allocated_fine = $total_play_truant_fine;
                            $collected_absent_or_play_truant_fee = ($total_play_truant_fine * $row['percentage_of_amount']) / 100;
                            $paidable_absent_or_play_truant_fee = $total_play_truant_fine - $already_paid;
                        }
                        if ($student_pay_amount > $paidable_absent_or_play_truant_fee) {
                            $absent_and_play_truant_fine[$k]['is_absent_fine'] = $is_absent_fine;
                            $absent_and_play_truant_fine[$k]['allocated_fine'] = $allocated_fine;
                            $absent_and_play_truant_fine[$k]['paid_amount'] = $paidable_absent_or_play_truant_fee;
                            $absent_and_play_truant_fine[$k]['already_paid_amount'] = $already_paid;
                            $absent_and_play_truant_fine[$k]['due_amount'] = $allocated_fine - ($paidable_absent_or_play_truant_fee + $already_paid);
                            array_push($absent_and_play_truant_fine_list, $absent_and_play_truant_fine);
                            $student_pay_amount -= $paidable_absent_or_play_truant_fee;
                        } else {
                            $absent_and_play_truant_fine[$k]['is_absent_fine'] = $is_absent_fine;
                            $absent_and_play_truant_fine[$k]['allocated_fine'] = $allocated_fine;
                            $absent_and_play_truant_fine[$k]['paid_amount'] = $student_pay_amount;

                            $absent_and_play_truant_fine[$k]['already_paid_amount'] = $already_paid;
                            $absent_and_play_truant_fine[$k]['due_amount'] = $allocated_fine - ($student_pay_amount + $already_paid);
                            array_push($absent_and_play_truant_fine_list, $absent_and_play_truant_fine);
                            $student_pay_amount -= $student_pay_amount;
                            break;
                        }
                        $k++;
                    //end absent & play truant fine
                    } else {
                        //category wise fee collect
                        $category_wise_fee_collection_amount = ($category_wise_allocated_fee[$row['fee_category_id']]['amount'] * $row['percentage_of_amount']) / 100;

                        $payable_for_this_transaction = $category_wise_fee_collection_amount;
                        if (!empty($already_paid_by_category) && isset($already_paid_by_category[$row['fee_category_id']]['paid_amount'])) {
                            if ($already_paid_by_category[$row['fee_category_id']]['paid_amount'] >= $category_wise_fee_collection_amount) {
                                continue;
                            } else {
                                $payable_for_this_transaction = $category_wise_fee_collection_amount - $already_paid_by_category[$row['fee_category_id']]['paid_amount'];
                            }
                        }

                        $category_wise_fee = array();
                        if ($student_pay_amount > $payable_for_this_transaction) {
                            $category_wise_fee[$j]['category_id'] = $row['fee_category_id'];
                            $category_wise_fee[$j]['category_name'] = $category_list[$row['fee_category_id']]['name'];
                            $category_wise_fee[$j]['allocated_fee'] = $category_wise_fee_collection_amount;
                            if (!empty($already_paid_by_category) && isset($already_paid_by_category[$row['fee_category_id']]['paid_amount'])) {
                                $already_paid_amount = $already_paid_by_category[$row['fee_category_id']]['paid_amount'];
                            } else {
                                $already_paid_amount = 0;
                            }
                            $category_wise_fee[$j]['already_paid_amount'] = $already_paid_amount;
                            $category_wise_fee[$j]['paid_amount'] = $payable_for_this_transaction;
                            $category_wise_fee[$j]['due_amount'] = $category_wise_fee_collection_amount - ($payable_for_this_transaction + $already_paid_amount);
                            array_push($category_wise_fee_list, $category_wise_fee);
                            $student_pay_amount -= $payable_for_this_transaction;
                        } else {
                            $category_wise_fee[$j]['category_id'] = $row['fee_category_id'];
                            $category_wise_fee[$j]['category_name'] = $category_list[$row['fee_category_id']]['name'];
                            $category_wise_fee[$j]['allocated_fee'] = $category_wise_fee_collection_amount;
                            if (!empty($already_paid_by_category) && isset($already_paid_by_category[$row['fee_category_id']]['paid_amount'])) {
                                $already_paid_amount = $already_paid_by_category[$row['fee_category_id']]['paid_amount'];
                            } else {
                                $already_paid_amount = 0;
                            }
                            $category_wise_fee[$j]['already_paid_amount'] = $already_paid_amount;
                            $category_wise_fee[$j]['paid_amount'] = $student_pay_amount;
                            $category_wise_fee[$j]['due_amount'] = $category_wise_fee_collection_amount - ($student_pay_amount + $already_paid_amount);
                            array_push($category_wise_fee_list, $category_wise_fee);
                            $student_pay_amount -= $student_pay_amount;
                            break;
                        }
                        $j++;
                        //end category wise fee collect
                    }
                endforeach;


                if ($student_pay_amount > 0) {
                    $student_return_money += $student_pay_amount;
                }
                //end fee collection process

                $return_data = array(
                    "absent_and_play_truant_fine_list" => $absent_and_play_truant_fine_list,
                    "monthly_fee_list" => $monthly_fee_list,
                    "category_wise_fee_list" => $category_wise_fee_list,
                    "student_return_money" => $student_return_money
                );

                return $return_data;
//                echo $student_return_money;
//                echo '<pre>';
//                print_r($return_data);
//                die;
            } else {
                //PDNF = Please set priority details for this priority./priority details not found for this priority
                $sdata['exception'] = "Please set priority details for this priority.";
                $this->session->set_userdata($sdata);
                redirect("student_fees/student_fee_add_for_normal");
            }
        } else {
            //PNF = Please set priority for this class./priority not found for this class
            $sdata['exception'] = "Please set priority for this class.";
            $this->session->set_userdata($sdata);
            redirect("student_fees/student_fee_add_for_normal");
        }
    }


    public function getStudentPaidFeeByCategory($student_id, $year, $from_transaction_date, $to_transaction_date)
    {
        $where = "";
        if ($to_transaction_date != '') {
            $where  = " AND (a.`payment_date` BETWEEN '$from_transaction_date' AND '$to_transaction_date') ";
        }
        $rows = $this->db->query("SELECT cf.`category_id`,SUM(cf.`paid_amount`) AS paid_amount FROM `tbl_category_wise_fee` AS cf WHERE cf.`fee_account_id`
IN (SELECT id FROM `tbl_student_fee_account` AS a WHERE a.`student_id` = '$student_id' AND a.`year` = '$year' $where)
GROUP BY cf.`category_id`;")->result_array();
        $paid_amount = array();
        foreach ($rows as $row) {
            $paid_amount[$row['category_id']]['category_id'] = $row['category_id'];
            $paid_amount[$row['category_id']]['paid_amount'] = $row['paid_amount'];
        }
        return $paid_amount;
    }


    public function getStudentPaidFeeByMonth($student_id, $year, $from_transaction_date, $to_transaction_date)
    {
        $where = "";
        if ($to_transaction_date != '') {
            $where  = " AND (a.`payment_date` BETWEEN '$from_transaction_date' AND '$to_transaction_date') ";
        }
        $rows = $this->db->query("SELECT mf.`month`,SUM(mf.`paid_amount`) AS paid_amount FROM `tbl_month_wise_fee` AS mf WHERE mf.`fee_account_id`
IN (SELECT id FROM `tbl_student_fee_account` AS a WHERE a.`student_id` = '$student_id' AND a.`year` = '$year' $where)
GROUP BY mf.`month`;")->result_array();
        $paid_amount = array();
        foreach ($rows as $row) {
            $paid_amount[$row['month']]['month'] = $row['month'];
            $paid_amount[$row['month']]['paid_amount'] = $row['paid_amount'];
        }
        return $paid_amount;
    }

    public function getStudentAbsentAndPlayTruantFinePaid($student_id, $year, $from_transaction_date, $to_transaction_date)
    {
        $where = "";
        if ($to_transaction_date != '') {
            $where  = " AND (a.`payment_date` BETWEEN '$from_transaction_date' AND '$to_transaction_date') ";
        }
        $rows = $this->db->query("SELECT SUM(af.`paid_amount`) AS paid_amount,af.`is_absent_fine` FROM `tbl_absent_and_play_truant_fine` AS af WHERE af.`fee_account_id`
IN (SELECT id FROM `tbl_student_fee_account` AS a WHERE a.`student_id` = '$student_id' AND a.`year` = '$year' $where)
GROUP BY af.`is_absent_fine`;")->result_array();
        $paid_amount = array();
        foreach ($rows as $row) {
            $paid_amount[$row['is_absent_fine']]['is_absent_fine'] = $row['is_absent_fine'];
            $paid_amount[$row['is_absent_fine']]['paid_amount'] = $row['paid_amount'];
        }
        return $paid_amount;
    }


    public function getCategory()
    {
        $rows = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
        $category = array();
        foreach ($rows as $row) {
            $category[$row['id']]['id'] = $row['id'];
            $category[$row['id']]['name'] = $row['name'];
        }
        return $category;
    }


    public function get_all_allocated_fee($limit, $offset, $value = '')
    {
        $this->db->select('a.*,c.name as category_name,sc.name as sub_category_name,sc.fee_type,tc.name as class_name');
        $this->db->from('tbl_fee_allocate as a');
        $this->db->join('tbl_fee_category AS c', 'a.category_id=c.id', 'left');
        $this->db->join('tbl_fee_sub_category AS sc', 'a.sub_category_id=sc.id', 'left');
        $this->db->join('tbl_class AS tc', 'a.class_id=tc.id', 'left');
        $this->db->order_by("a.class_id", "asc");
        $this->db->order_by("a.year", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_allocated_fee_for_normal($limit, $offset, $value = '')
    {
        $this->db->select('a.*,c.name as category_name,tc.name as class_name');
        $this->db->from('tbl_fee_category_wise_amount as a');
        $this->db->join('tbl_fee_category AS c', 'a.category_id=c.id', 'left');
        $this->db->join('tbl_class AS tc', 'a.class_id=tc.id', 'left');
        $this->db->order_by("a.class_id", "asc");
        $this->db->order_by("a.year", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_monthly_fee_for_normal($limit, $offset, $value = '')
    {
        $this->db->select('a.*,tc.name as class_name');
        $this->db->from('tbl_class_wise_monthly_fee as a');
        $this->db->join('tbl_class AS tc', 'a.class_id=tc.id');
        $this->db->order_by("a.class_id", "asc");
        $this->db->order_by("a.year", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_student_absent_fine($limit, $offset, $value = '')
    {
        $this->db->select('a.*,s.student_code,s.name as student_name,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_fee_absent_fine as a');
        $this->db->join('tbl_student AS s', 'a.student_id=s.id');
        $this->db->join('tbl_class AS c', 's.class_id=c.id');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id');
        $this->db->order_by("a.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_student_play_truant_fine($limit, $offset, $value = '')
    {
        $this->db->select('a.*,s.student_code,s.name as student_name,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_fee_play_truant_fine as a');
        $this->db->join('tbl_student AS s', 'a.student_id=s.id');
        $this->db->join('tbl_class AS c', 's.class_id=c.id');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id');
        $this->db->order_by("a.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_fee_priority($limit, $offset, $value = '')
    {
        $this->db->select('p.*');
        $this->db->from('tbl_student_fee_priority as p');
        $this->db->order_by("p.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_student_fee_waiver($limit, $offset, $value = '')
    {
        $this->db->select('a.*,st.name as student_name,st.roll_no,tc.name as class_name,sc.name as fees_category_name');
        $this->db->from('tbl_student_fee_waiver as a');
        $this->db->join('tbl_student AS st', 'a.student_id=st.id');
        $this->db->join('tbl_class AS tc', 'a.class_id=tc.id');
        $this->db->join('tbl_fee_sub_category AS sc', 'a.fees_category_id=sc.id');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_student_scholarship($limit, $offset, $value = '')
    {
        $this->db->select('s.*,st.name as student_name,tc.name as class_name');
        $this->db->from('tbl_student_scholarship as s');
        $this->db->join('tbl_student AS st', 's.student_id=st.id');
        $this->db->join('tbl_class AS tc', 's.class_id=tc.id');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_student_fee_for_normal($limit, $offset, $value = '')
    {
        $this->db->select('a.*,st.name as student_name,st.roll_no,tc.name as class_name');
        $this->db->from('tbl_student_fee_account as a');
        $this->db->join('tbl_student AS st', 'a.student_id=st.id');
        $this->db->join('tbl_class AS tc', 'a.class_id=tc.id');
        $this->db->order_by("a.id", "desc");
        // $this->db->order_by("st.roll_no", "asc");
        //$this->db->order_by("a.payment_date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_student_fee_status_report($class_id, $section_id, $group, $year, $to_month, $from_transaction_date, $to_transaction_date)
    {
        //echo 'Class-'.$class_id.'/section_id-'.$section_id.'/group-'.$group.'/year-'.$year; die;
        $students = $this->db->query("SELECT id,name,roll_no,student_code FROM tbl_student WHERE class_id = '$class_id' AND section_id = '$section_id' AND `group` = '$group' AND `status` = '1' ORDER BY ABS(roll_no)")->result_array();
        $get_monthly_fee = $this->db->query("SELECT * FROM `tbl_class_wise_monthly_fee` WHERE `class_id` = '$class_id' AND `year` = '$year'")->result_array();
        if (!empty($get_monthly_fee)) {
            $monthly_fee = $get_monthly_fee[0]['amount'];
        } else {
            $sdata['exception'] = "Please set monthly fee.";
            $this->session->set_userdata($sdata);
            redirect("report/getStudentFeeStatus");
        }

        //Waiver && scholarship List
        $waiver_setting = $this->db->query("SELECT * FROM tbl_fee_waiver_setting WHERE `year` = '$year'")->row();
        if (empty($waiver_setting)) {
            $sdata['exception'] = "Please set waiver setting.";
            $this->session->set_userdata($sdata);
            redirect("report/getStudentFeeStatus");
        }
        $waiver_applicable_month = $waiver_setting->applicable_month;
        $applicable_month_for_scholarship = $waiver_setting->applicable_month_for_scholarship;
        $waiver_list = $this->getFeeWaiverList($class_id, $section_id, $group, $year);

        $scholarship_list = $this->getScholarshipStudentList($class_id, $section_id, $group, $year);
        //End Waiver && scholarship List

        $waiver_list_from_total_amount = $this->getFeeWaiverListFromTotalAmount($class_id, $section_id, $group, $year);

        $absent_fee_list = $this->getAbsentFeeList($class_id, $section_id, $group, $year);
        //echo '<pre>';
        // print_r($absent_fee_list);
        $play_truant_fine_list = $this->getPlayTruantFineList($class_id, $section_id, $group, $year);

        $category_wise_fees = $this->getAllCategoryWiseFee($class_id, $year);

        $student_fee_info = array();
        $j = 0;
        foreach ($students as $row):
            $j++;

        $total_fee = 0;
        $total_fee_paid = 0;
        $total_fee_due = 0;
        //echo $row['id'].'/';
        //die;
        $student_fee_info[$j]['id'] = $row['id'];
        $student_fee_info[$j]['name'] = $row['name'];
        $student_fee_info[$j]['roll_no'] = $row['roll_no'];
        $student_fee_info[$j]['student_code'] = $row['student_code'];


        //monthly fee
        $already_paid_by_month = $this->getStudentPaidFeeByMonth($row['id'], $year, $from_transaction_date, $to_transaction_date);
        $monthly_fee_for_this_student = array();
        $jj = 1;
        //while ($jj <= 12) {
        while ($jj <= $to_month) {

                //check waiver
            if (isset($waiver_list[$row['id']]) && $jj >= $waiver_applicable_month) {
                $this_student_monthly_fee = $monthly_fee - (($monthly_fee * $waiver_list[$row['id']]['percentage_of_amount']) / 100);
            } else {
                $this_student_monthly_fee = $monthly_fee;
            }
            //check waiver

            //check scholarship
            if (isset($scholarship_list[$row['id']]) && $jj >= $applicable_month_for_scholarship) {
                $this_student_monthly_fee = 0;
            }
            //check scholarship

            //already paid monthly fee
            if (!empty($already_paid_by_month) && isset($already_paid_by_month[$jj]['paid_amount'])) {
                $already_paid_amount = $already_paid_by_month[$jj]['paid_amount'];
            } else {
                $already_paid_amount = 0;
            }
            //already paid monthly fee

            $paid_this_student_monthly_fee = $already_paid_amount;
            $monthly_fee_for_this_student[$jj]['monthly_fee'] = $this_student_monthly_fee;
            $monthly_fee_for_this_student[$jj]['paid_monthly_fee'] = $paid_this_student_monthly_fee;
            $monthly_fee_for_this_student[$jj]['due_monthly_fee'] = $this_student_monthly_fee - $paid_this_student_monthly_fee;

            $total_fee += $this_student_monthly_fee;
            $total_fee_paid += $paid_this_student_monthly_fee;
            $total_fee_due += $this_student_monthly_fee - $paid_this_student_monthly_fee;

            $jj++;
        }

        $student_fee_info[$j]['monthly_fee'] = $monthly_fee_for_this_student;
        //monthly fee

        //Category wise fee
        $already_paid_by_category = $this->getStudentPaidFeeByCategory($row['id'], $year, $from_transaction_date, $to_transaction_date);
        foreach ($category_wise_fees as $cat_fees_row):

                if (!empty($already_paid_by_category) && isset($already_paid_by_category[$cat_fees_row['category_id']]['paid_amount'])) {
                    $already_paid_amount = $already_paid_by_category[$cat_fees_row['category_id']]['paid_amount'];
                } else {
                    $already_paid_amount = 0;
                }


        $paid_for_this_category = $already_paid_amount;
        $category_wise_fees[$cat_fees_row['category_id']]['paid_fee'] = $paid_for_this_category;
        $category_wise_fees[$cat_fees_row['category_id']]['due_fee'] = $cat_fees_row['amount'] - $paid_for_this_category;

        $total_fee += $cat_fees_row['amount'];
        $total_fee_paid += $paid_for_this_category;
        $total_fee_due += $cat_fees_row['amount'] - $paid_for_this_category;

        endforeach;
//            echo '<pre>';
//            print_r($monthly_fee_for_this_student);
//            die;
        $student_fee_info[$j]['category_wise_fee'] = $category_wise_fees;
        //Category wise fee

        //already paid absent and play truant fine part
        $already_absent_and_play_truant_fine_paid = $this->getStudentAbsentAndPlayTruantFinePaid($row['id'], $year, $from_transaction_date, $to_transaction_date);
        if (isset($already_absent_and_play_truant_fine_paid[1])) {
            $already_absent_fine_paid = $already_absent_and_play_truant_fine_paid[1]['paid_amount'];
        } else {
            $already_absent_fine_paid = 0;
        }
        if (isset($already_absent_and_play_truant_fine_paid[0])) {
            $already_play_truant_fine_paid = $already_absent_and_play_truant_fine_paid[0]['paid_amount'];
        } else {
            $already_play_truant_fine_paid = 0;
        }
        //already paid absent and play truant fine part

        //absent fee part
        if (isset($absent_fee_list[$row['id']])) {
            $absent_fee_paid = $already_absent_fine_paid;
            $student_fee_info[$j]['absent_fee'] = $absent_fee_list[$row['id']]['amount'];
            $student_fee_info[$j]['absent_fee_paid'] = $absent_fee_paid;
            $student_fee_info[$j]['absent_fee_due'] = $absent_fee_list[$row['id']]['amount'] - $absent_fee_paid;
            $total_fee += $absent_fee_list[$row['id']]['amount'];
            $total_fee_paid += $absent_fee_paid;
            $total_fee_due += $absent_fee_list[$row['id']]['amount'] - $absent_fee_paid;
        } else {
            $student_fee_info[$j]['absent_fee'] = 0;
            $student_fee_info[$j]['absent_fee_paid'] = 0;
            $student_fee_info[$j]['absent_fee_due'] = 0;
        }
        //end absent fee part


        //play truant fine part

        if (isset($play_truant_fine_list[$row['id']])) {
            $play_truant_fine_paid = $already_play_truant_fine_paid;
            $student_fee_info[$j]['play_truant_fine'] = $play_truant_fine_list[$row['id']]['amount'];
            $student_fee_info[$j]['play_truant_fine_paid'] = $play_truant_fine_paid;
            $student_fee_info[$j]['play_truant_fine_due'] = $play_truant_fine_list[$row['id']]['amount'] - $play_truant_fine_paid;
            $total_fee += $play_truant_fine_list[$row['id']]['amount'];
            $total_fee_paid += $play_truant_fine_paid;
            $total_fee_due += $play_truant_fine_list[$row['id']]['amount'] - $play_truant_fine_paid;
        } else {
            $student_fee_info[$j]['play_truant_fine'] = 0;
            $student_fee_info[$j]['play_truant_fine_paid'] = 0;
            $student_fee_info[$j]['play_truant_fine_due'] = 0;
        }
        //end play truant fine part

        if (isset($waiver_list_from_total_amount[$row['id']])) {
            $student_fee_info[$j]['waiver_amount_from_total'] = $waiver_list_from_total_amount[$row['id']]['amount'];
            $student_fee_info[$j]['total_fee'] = $total_fee - $waiver_list_from_total_amount[$row['id']]['amount'];
            $student_fee_info[$j]['total_fee_due'] = $total_fee_due - $waiver_list_from_total_amount[$row['id']]['amount'];
        } else {
            $student_fee_info[$j]['waiver_amount_from_total'] = 0;
            $student_fee_info[$j]['total_fee'] = $total_fee;
            $student_fee_info[$j]['total_fee_due'] = $total_fee_due;
        }

        $student_fee_info[$j]['total_fee_paid'] = $total_fee_paid;




        endforeach;

        // echo '<pre>';
        // print_r($student_fee_info);
        // die;

        return $student_fee_info;
    }



    public function getAllCategoryWiseFee($class_id, $year)
    {
        $rows = $this->db->query("SELECT id,`category_id`,`amount`,`year` FROM `tbl_fee_category_wise_amount`
 WHERE `class_id` = '$class_id' AND `year` = '$year'")->result_array();
        $fees = array();
        foreach ($rows as $row) {
            $fees[$row['category_id']]['id'] = $row['id'];
            $fees[$row['category_id']]['category_id'] = $row['category_id'];
            $fees[$row['category_id']]['amount'] = $row['amount'];
            $fees[$row['category_id']]['year'] = $row['year'];
        }
        return $fees;
    }


    public function getAbsentFeeListBytudentId($student_id, $year)
    {
        $rows = $this->db->query("SELECT SUM(a.`amount`) as total_absent_fee FROM tbl_fee_absent_fine AS a WHERE a.`year` = $year AND a.`student_id` = $student_id")->result_array();
        $auto_approve_absent_fine_amount = 0;
        $auto_approve_absent_fine = $this->db->query("SELECT SUM(`amount`) as total_amount FROM `tbl_approve_absent_fine`
                                    WHERE `student_id` = '$student_id' AND `year` = '$year'")->result_array();
        $auto_approve_absent_fine_amount = $auto_approve_absent_fine[0]['total_amount'];
        if (empty($rows)) {
            return $auto_approve_absent_fine_amount;
        } else {
            return $rows[0]['total_absent_fee'] + $auto_approve_absent_fine_amount;
        }
    }


    public function getPlayTruantFineListBytudentId($student_id, $year)
    {
        $rows = $this->db->query("SELECT SUM(a.`amount`) as total_play_truant_fee FROM tbl_fee_play_truant_fine AS a WHERE a.`year` = $year AND a.`student_id` = $student_id")->result_array();
        if (empty($rows)) {
            return 0;
        } else {
            return $rows[0]['total_play_truant_fee'];
        }
    }

    public function getAbsentFeeList($class_id, $section_id, $group, $year)
    {
        $students = $this->db->query("SELECT id,roll_no FROM `tbl_student` WHERE `class_id` = '$class_id' AND
               `section_id` = '$section_id' AND `group` = '$group' AND `year` = '$year' AND `status` = 1")->result_array();

        $list = array();
        foreach ($students as $student) {
            $student_id = $student['id'];

            $absent_rows = $this->db->query("SELECT * FROM `tbl_fee_absent_fine` WHERE `student_id` = '$student_id' AND `year` = '$year'")->result_array();


            $list[$student['id']]['student_id'] = $student['id'];
            $list[$student['id']]['roll_no'] = $student['roll_no'];

            $total_absent_fine = 0;
            if (!empty($absent_rows)) {
                $total_absent_fine = $absent_rows[0]['amount'];
            }

            $auto_approve_absent_fine = $this->db->query("SELECT SUM(`amount`) as total_amount FROM `tbl_approve_absent_fine`
                                        WHERE `student_id` = '$student_id' AND `year` = '$year'")->result_array();
            /*if($student['roll_no'] == '11'){
                echo $auto_approve_absent_fine[0]['total_amount'];
                echo '<pre>';
                print_r($absent_rows);
                print_r($auto_approve_absent_fine);
                die;
            }*/

            $auto_approve_absent_fine = $auto_approve_absent_fine[0]['total_amount'];

            $list[$student['id']]['amount'] = $total_absent_fine + $auto_approve_absent_fine;
        }

        /* $rows = $this->db->query("SELECT * FROM `tbl_fee_absent_fine` WHERE `class_id` = '$class_id' AND `year` = '$year'")->result_array();
         $list = array();
         foreach ($rows as $row) {
             $list[$row['student_id']]['id'] = $row['id'];
             $list[$row['student_id']]['student_id'] = $row['student_id'];
             $student_id = $row['student_id'];
             $auto_approve_absent_fine = $this->db->query("SELECT SUM(`amount`) as total_amount FROM `tbl_approve_absent_fine`
                                         WHERE `student_id` = '$student_id' AND `year` = '$year'")->result_array();
             $auto_approve_absent_fine = 0;
             if($auto_approve_absent_fine[0]['total_amount'] != ''){
                 $auto_approve_absent_fine = $auto_approve_absent_fine[0]['total_amount'];
             }
             $list[$row['student_id']]['amount'] = $row['amount'] + $auto_approve_absent_fine;
         }*/
        //echo '<pre>';
        //print_r($list); die;
        return $list;
    }

    public function getPlayTruantFineList($class_id, $section_id, $group, $year)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_fee_play_truant_fine` WHERE `class_id` = '$class_id' AND `section_id` = '$section_id'
AND `group` = '$group' AND `year` = '$year'")->result_array();
        $list = array();
        foreach ($rows as $row) {
            $list[$row['student_id']]['id'] = $row['id'];
            $list[$row['student_id']]['student_id'] = $row['student_id'];
            $list[$row['student_id']]['amount'] = $row['amount'];
        }
        return $list;
    }


    public function getFeeWaiverList($class_id, $section_id, $group, $year)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_student_fee_waiver` WHERE `class_id` = '$class_id' AND `section_id` = '$section_id'
AND `group` = '$group' AND `year` = '$year'")->result_array();
        $waiver = array();
        foreach ($rows as $row) {
            $waiver[$row['student_id']]['id'] = $row['id'];
            $waiver[$row['student_id']]['student_id'] = $row['student_id'];
            $waiver[$row['student_id']]['percentage_of_amount'] = $row['percentage_of_amount'];
        }
        return $waiver;
    }

    public function getScholarshipStudentList($class_id, $section_id, $group, $year)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_student_scholarship` WHERE `class_id` = '$class_id' AND `section_id` = '$section_id'
AND `group` = '$group' AND `year` = '$year'")->result_array();
        $scholarship = array();
        foreach ($rows as $row) {
            $scholarship[$row['student_id']]['id'] = $row['id'];
            $scholarship[$row['student_id']]['student_id'] = $row['student_id'];
        }
        return $scholarship;
    }

    public function getSubCategory()
    {
        $rows = $this->db->query("SELECT * FROM tbl_fee_sub_category")->result_array();
        $sub_category = array();
        foreach ($rows as $row) {
            $sub_category[$row['id']]['id'] = $row['id'];
            $sub_category[$row['id']]['category_id'] = $row['category_id'];
            $sub_category[$row['id']]['name'] = $row['name'];
            $sub_category[$row['id']]['fee_type'] = $row['fee_type'];
        }
        return $sub_category;
    }

    public function checkFeeAlreadyAllocate($class_id, $category_id, $sub_category_id, $year)
    {
        $rows = $this->db->query("SELECT * FROM tbl_fee_allocate WHERE class_id = '$class_id' AND category_id = '$category_id'
		AND sub_category_id = '$sub_category_id' AND year = '$year'")->result_array();
        return $rows;
    }

    public function checkFeeAlreadyAllocateClassWiseFee($class_id, $year)
    {
        $rows = $this->db->query("SELECT * FROM tbl_class_wise_monthly_fee WHERE class_id = '$class_id'
        AND year = '$year'")->result_array();
        return $rows;
    }

    public function checkFeeAlreadyAllocateForNormal($class_id, $category_id, $year)
    {
        $rows = $this->db->query("SELECT * FROM tbl_fee_category_wise_amount WHERE class_id = '$class_id' AND category_id = '$category_id'
        AND year = '$year'")->result_array();
        return $rows;
    }

    public function get_all_collection_sheet_info($collection_id)
    {
        $info = $this->db->query("SELECT c.*,u.`name` as user_name,s.`name` as student_name,s.`roll_no`,s.`student_code`,s.`reg_no`,sc.`name` AS section,cl.`name` AS class
FROM `tbl_fee_collection` AS c
LEFT JOIN `tbl_student` AS s ON s.`id` = c.`student_id`
LEFT JOIN `tbl_class` AS cl ON cl.`id` = c.`class_id`
LEFT JOIN `tbl_section` AS sc ON sc.`id` = c.`section_id`
LEFT JOIN `tbl_user` AS u ON u.`id` = c.`user_id`
WHERE c.`id` = '$collection_id'")->result_array();
        return $info;
    }


    public function get_student_fee_report($student_id, $class_id, $section_id, $month, $year)
    {
        if ($student_id != '') {
            $where = "  AND fc.`student_id` = '$student_id'";
        } else {
            $where = "  AND fc.`class_id` = '$class_id' AND fc.`section_id` = '$section_id'";
        }

        if ($month != '') {
            $where .= "  AND fc.`month` = '$month'";
        }

        $info = $this->db->query("SELECT s.`name`,s.`roll_no`,fc.`receipt_no`,fc.`date`,fc.`month`,fc.`year`,
(SELECT SUM(fcd.`paid_amount`) FROM `tbl_fee_collection_details` AS fcd WHERE fcd.`collection_id` = fc.`id`) AS paid_amount
FROM `tbl_fee_collection` AS fc
LEFT JOIN `tbl_student` AS s ON s.`id` = fc.`student_id`
WHERE fc.`year` = '$year' $where")->result_array();
        return $info;
    }


    public function get_all_collected_fee($limit, $offset, $value = '')
    {
        $this->db->select('c.*,s.name as student_name,s.roll_no,s.reg_no,sc.name AS section,cl.name AS class');
        $this->db->from('tbl_fee_collection AS c');
        $this->db->join('tbl_student AS s', 's.id = c.student_id', 'left');
        $this->db->join('tbl_class AS cl', 'cl.id = c.class_id', 'left');
        $this->db->join('tbl_section AS sc', 'sc.id = c.section_id', 'left');
        if (isset($value) && !empty($value) &&  isset($value['receipt_no']) && $value['receipt_no'] != '') {
            $this->db->where('c.receipt_no', $value['receipt_no']);
        }
        if (isset($value) && !empty($value) &&  isset($value['amount']) && $value['amount'] != '') {
            $this->db->where('c.total_paid_amount', $value['amount']);
        }
        if (isset($value) && !empty($value) &&  isset($value['roll_no']) && $value['roll_no'] != '') {
          $this->db->where('s.roll_no', $value['roll_no']);
        }
        if (isset($value) && !empty($value) &&  isset($value['name']) && $value['name'] != '') {
          $this->db->like('s.name', $value['name']);
          $this->db->or_like('s.student_code', $value['name']);
        }
        if (isset($value) && !empty($value) &&  isset($value['student_id']) && $value['student_id'] != '') {
            $this->db->where('c.student_id', $value['student_id']);
        } else {
            if (isset($value) && !empty($value) && isset($value['class_id']) && $value['class_id'] != '') {
                $this->db->where('c.class_id', $value['class_id']);
            }
            if (isset($value) && !empty($value) &&  isset($value['section_id']) && $value['section_id'] != '') {
                $this->db->where('c.section_id', $value['section_id']);
            }
            if (isset($value) && !empty($value) && isset($value['shift_id']) &&  $value['shift_id'] != '') {
                $this->db->where('c.shift_id', $value['shift_id']);
            }
        }

        if (isset($value) && !empty($value) && isset($value['from_date'])  && $value['from_date'] != '' && isset($value['to_date'])   && $value['to_date'] != '') {
            $this->db->where('c.date >=', $value['from_date']);
            $this->db->where('c.date <=', $value['to_date']);
        }


        $this->db->order_by("c.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_special_care_collected_fee($limit, $offset, $value = '')
    {
        $this->db->select('c.*,s.name as student_name,s.roll_no,s.reg_no,sc.name AS section,cl.name AS class');
        $this->db->from('tbl_monthly_special_care_fee AS c');
        $this->db->join('tbl_student AS s', 's.id = c.student_id', 'left');
        $this->db->join('tbl_class AS cl', 'cl.id = c.class_id', 'left');
        $this->db->join('tbl_section AS sc', 'sc.id = c.section_id', 'left');
        if (isset($value) && !empty($value) && $value['student_id'] != '') {
            $this->db->where('c.student_id', $value['student_id']);
        }
        $this->db->order_by("c.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }



    public function get_all_collection_sheet_details_single($collection_id)
    {
        $info = $this->db->query("SELECT c.*,s.`name` AS sub_category FROM `tbl_fee_collection_details` AS c
LEFT JOIN `tbl_fee_sub_category` AS s ON s.`id` = c.`sub_category_id`
WHERE c.`collection_id` = '$collection_id'")->result_array();
        return $info;
    }

    public function get_all_collection_sheet_details($date, $student_id)
    {
        $info = $this->db->query("SELECT c.*,s.`name` AS sub_category FROM `tbl_fee_collection_details` AS c
LEFT JOIN `tbl_fee_sub_category` AS s ON s.`id` = c.`sub_category_id`
WHERE c.`entry_date` = '$date' AND c.`student_id` = '$student_id'")->result_array();
        return $info;
    }

    public function check_already_fee_paid_info($student_id, $month, $year)
    {
        $info = $this->db->query("SELECT id FROM `tbl_fee_collection` AS c WHERE c.`student_id` = '$student_id' AND c.`month` = '$month' AND c.`year` = '$year'")->result_array();
        if (count($info) > 0) {
            return false;
        } else {
            return true;
        }
    }


    public function get_student_fee_due_list_for_all_month($class_id, $section_id, $shift_id, $group_id, $months, $year)
    {
        $student_info = $this->db->query("SELECT s.id,s.name,s.class_id,s.section_id,s.student_code,s.group,s.roll_no,s.reg_no FROM
                        tbl_student as s WHERE s.class_id = '$class_id'
                        AND s.section_id = '$section_id'
                        AND s.group = '$group_id'
                        AND s.shift_id = '$shift_id'
                         AND s.year = '$year'
                        AND s.status = '1'
                        ORDER BY ABS(s.roll_no)")->result_array();
        $data = array();
        $i = 0;
        foreach ($student_info as $row):
            $data[$i]['student_id'] = $row['id'];
        $data[$i]['name'] = $row['name'];
        $data[$i]['student_code'] = $row['student_code'];
        $data[$i]['roll_no'] = $row['roll_no'];
        $data[$i]['reg_no'] = $row['reg_no'];
        $data[$i]['group'] = $row['group'];

        foreach ($months as $month):
                 $allocated_fee_list = $this->get_fee_allocated_list($class_id, $section_id, $row['id'], $month, $year);
        $data[$i]['fee_list_'.$month] = $allocated_fee_list;
        endforeach;
        $i++;
        endforeach;

        //  echo '<pre>';
        // print_r($data); die;

        return $data;
    }


    public function get_student_fee_due_list($class_id, $section_id, $shift_id, $group_id, $month, $year)
    {
        $student_info = $this->db->query("SELECT s.id,s.name,s.class_id,s.section_id,s.group,s.roll_no,s.reg_no
          FROM tbl_student as s WHERE s.class_id = '$class_id'
           AND s.class_id = '$class_id'
           AND s.section_id = '$section_id'
           AND s.group = '$group_id'
           AND s.shift_id = '$shift_id'
           AND s.year = '$year' AND s.status = '1' ORDER BY ABS(s.roll_no)")->result_array();

        $data = array();
        $i = 0;
        foreach ($student_info as $row):
            $allocated_fee_list = $this->get_fee_allocated_list($class_id, $section_id, $row['id'], $month, $year);
        $data[$i]['student_id'] = $row['id'];
        $data[$i]['name'] = $row['name'];
        $data[$i]['roll_no'] = $row['roll_no'];
        $data[$i]['reg_no'] = $row['reg_no'];
        $data[$i]['group'] = $row['group'];
        $data[$i]['fee_list'] = $allocated_fee_list;
        $i++;
        endforeach;
        //echo '<pre>';
        // print_r($data);
        //  die;
        return $data;
    }

    public function getStduentWiseAllocatedFeeCategory($class_id,$year,$student_id,$fee_type_id)
    {
      $allocated_list = $this->db->query("SELECT a.*,sc.`name` AS sub_category_name,sc.`is_exam_fee`,
                        sc.`is_waiver_applicable` FROM tbl_fee_allocate AS a
                        INNER JOIN `tbl_fee_sub_category` AS sc ON sc.`id` = a.`sub_category_id`
                        WHERE a.`class_id` = '$class_id' AND a.`year` = '$year' AND a.`fee_type` = '$fee_type_id'
                        AND sc.id NOT IN (SELECT fr.`fee_sub_category_id` FROM `tbl_fee_remove` AS fr
                        WHERE fr.`student_id` = '$student_id' AND fr.`year` = '$year');")->result_array();
      return $allocated_list;
    }


    public function get_fee_allocated_list($class_id, $section_id, $student_id, $month, $year)
    {

        $fee_type = $this->db->query("SELECT * FROM `tbl_fee_type` WHERE `year` = '$year' AND `month` = '$month'")->result_array();
        $i = 0;
        $data['allocated_list'] = array();
        while ($i < count($fee_type)) {
            $fee_type_id = $fee_type[$i]['fee_type'];

//             $get_allocated_list = $this->db->query("SELECT a.*,sc.`name` AS sub_category_name,sc.`is_exam_fee`,
//               sc.`is_waiver_applicable` FROM tbl_fee_allocate AS a
// INNER JOIN `tbl_fee_sub_category` AS sc ON sc.`id` = a.`sub_category_id`
// WHERE a.`class_id` = '$class_id' AND a.`year` = '$year' AND a.`fee_type` = '$fee_type_id'")->result_array();

            $get_allocated_list = $this->getStduentWiseAllocatedFeeCategory($class_id,$year,$student_id,$fee_type_id);

            $return_data = array();

            //echo '<pre>';
            //print_r($get_allocated_list);
            //die;

            $k = 0;
            while ($k < count($get_allocated_list)) {
                $sub_category_id = $get_allocated_list[$k]['sub_category_id'];


                //waiver calculate
                $waiver_amount = 0;
                if ($get_allocated_list[$k]['is_waiver_applicable'] == 1) {
                    $waiver_info = $this->db->query("SELECT * FROM `tbl_student_fee_waiver` AS w
                    WHERE w.`student_id` = '$student_id' AND w.`fees_category_id` = '$sub_category_id'
                     AND w.`year` = '$year'")->result_array();
                    if (!empty($waiver_info)) {
                        $is_percentage = $waiver_info[0]['is_percentage'];
                        if ($is_percentage == '0') {
                            $waiver_amount = $waiver_info[0]['amount'];
                        } else {
                            $waiver_amount = ($get_allocated_list[$k]['amount'] * $waiver_info[0]['amount']) / 100;
                        }
                    }
                }
                $get_allocated_list[$k]['waiver_amount'] = $waiver_amount;
                //waiver calculate end

                //actual paidable amount
                $resident_info = $this->db->query("SELECT * FROM `tbl_student_resident_info` AS r
WHERE r.`student_id` = '$student_id' AND r.`year` = '$year'")->result_array();
                if (!empty($resident_info)) {
                    $get_allocated_list[$k]['actual_allocated_amount_for_this_student'] = $get_allocated_list[$k]['resident_amount'] - $waiver_amount;
                } else {
                    $get_allocated_list[$k]['actual_allocated_amount_for_this_student'] = $get_allocated_list[$k]['amount'] - $waiver_amount;
                }
                //actual paidable amount


                $already_paid_where = "";
                if ($fee_type_id != 'A') {
                    $already_paid_where = " AND c.`month` = '$month'";
                }
                //echo $sub_category_id; die;
                $already_paid_info = $this->db->query("SELECT SUM(c.`paid_amount`) AS paid_amount, SUM(c.`discount_amount`) AS discount_amount FROM `tbl_fee_collection_details` AS c
WHERE c.`student_id` = '$student_id' AND c.`sub_category_id` = '$sub_category_id' AND c.`year` = '$year' $already_paid_where")->result_array();
                if ($already_paid_info[0]['paid_amount'] > 0) {
                    $get_allocated_list[$k]['already_paid_amount'] = $already_paid_info[0]['paid_amount'];
                } else {
                    $get_allocated_list[$k]['already_paid_amount'] = 0;
                }

                //already discount

                if ($already_paid_info[0]['discount_amount'] > 0) {
                    $get_allocated_list[$k]['already_total_discount_amount'] = $already_paid_info[0]['discount_amount'];
                } else {
                    $get_allocated_list[$k]['already_total_discount_amount'] = 0;
                }


                $return_data[$k]['category_id'] = $get_allocated_list[$k]['category_id'];
                $return_data[$k]['sub_category_id'] = $get_allocated_list[$k]['sub_category_id'];
                $return_data[$k]['sub_category_name'] = $get_allocated_list[$k]['sub_category_name'];
                $return_data[$k]['actual_allocated_amount_for_this_student'] = $get_allocated_list[$k]['actual_allocated_amount_for_this_student'];
                $return_data[$k]['already_paid_amount'] = $get_allocated_list[$k]['already_paid_amount'];
                $return_data[$k]['already_total_discount_amount'] = $get_allocated_list[$k]['already_total_discount_amount'];

                if (($get_allocated_list[$k]['already_total_discount_amount'] + $get_allocated_list[$k]['already_paid_amount']) < $get_allocated_list[$k]['actual_allocated_amount_for_this_student']) {
                    $return_data[$k]['due_amount'] = ($get_allocated_list[$k]['actual_allocated_amount_for_this_student'] - ($get_allocated_list[$k]['already_paid_amount'] + $get_allocated_list[$k]['already_total_discount_amount']));
                } else {
                    $return_data[$k]['due_amount'] = 0;
                }
                $k++;
            }

            if ($fee_type_id == 'M') {
                $paid_transport_fee_amount = 0;
                $allocated_transport_fee = 0;
                $student_transport_fee = $this->db->query("SELECT transport_fee_amount FROM `tbl_student` WHERE `id` = '$student_id'")->result_array();
                $allocated_transport_fee = $student_transport_fee[0]['transport_fee_amount'];
                if ($allocated_transport_fee != '' && $allocated_transport_fee > 0) {
                    $paid_transport_fee = $this->db->query("SELECT SUM(amount) as paid_transport_fee FROM
                  tbl_student_monthly_transport_fee WHERE student_id = '$student_id' AND month = '$month' AND year = '$year'")->result_array();
                    if (!empty($paid_transport_fee)) {
                        $paid_transport_fee_amount = $paid_transport_fee[0]['paid_transport_fee'];
                    }
                }

                $return_data[$k + 1]['category_id'] = "";
                $return_data[$k + 1]['sub_category_id'] = "";
                $return_data[$k + 1]['sub_category_name'] = "Transport Fee";
                $return_data[$k + 1]['actual_allocated_amount_for_this_student'] = $allocated_transport_fee;
                $return_data[$k + 1]['already_paid_amount'] = $paid_transport_fee_amount;
                $return_data[$k + 1]['due_amount'] = $allocated_transport_fee - $paid_transport_fee_amount;
            }




            $data['allocated_list'][$fee_type_id] = $return_data;
            $i++;
        }
        // echo '<pre>';
        // print_r($data['allocated_list']);
        // die;
        return $data['allocated_list'];
    }

    public function get_fee_payment_info($month, $year, $student_id)
    {
        $rows = $this->db->query("SELECT * FROM tbl_fee_collection WHERE student_id = '$student_id' AND month = '$month' AND year = '$year'")->result_array();
        //  echo '<pre>';
        //  print_r($rows);
        // die;
        $paid_student_list = array();
        $i = 0;
        foreach ($rows as $row) {
            $paid_student_list[$i]['id'] = $row['id'];
            $paid_student_list[$i]['student_id'] = $row['student_id'];
            $paid_student_list[$i]['class_id'] = $row['class_id'];
            $paid_student_list[$i]['section_id'] = $row['section_id'];
            $paid_student_list[$i]['receipt_no'] = $row['receipt_no'];
            $paid_student_list[$i]['month'] = $row['month'];
            $paid_student_list[$i]['year'] = $row['year'];
            $i++;
        }
        return $paid_student_list;
    }

    public function get_category_wise_fee_summery($year)
    {
        $rows = $this->db->query("SELECT  fc.`name` AS category_name,SUM(cf.`paid_amount`) AS total_collected_amount FROM `tbl_category_wise_fee` AS cf
INNER JOIN `tbl_fee_category` fc ON fc.`id` = cf.`category_id`
WHERE cf.`year` = '$year'
GROUP BY cf.`category_id`")->result_array();
        return $rows;
    }

    public function get_month_wise_fee_summery($year)
    {
        $rows = $this->db->query("SELECT  mf.`month`,SUM(mf.`paid_amount`) AS total_collected_amount FROM `tbl_month_wise_fee` AS mf
WHERE mf.`year` = '$year'
GROUP BY mf.`month`")->result_array();
        return $rows;
    }

    public function get_absent_play_truant_fine_summery($year)
    {
        $rows = $this->db->query("SELECT  af.`is_absent_fine`,SUM(af.`paid_amount`) AS total_collected_amount FROM `tbl_absent_and_play_truant_fine` AS af
WHERE af.`year` = '$year'
GROUP BY af.`is_absent_fine`")->result_array();
        return $rows;
    }
}
