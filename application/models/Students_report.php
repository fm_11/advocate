<?php

class Students_report extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        $this->load->model(array('Teacher_info', 'Student'));
        parent::__construct();
    }


    function get_student_attendance_sheet($data)
    {
        $class_id = $data['class_id'];
        $section_id = $data['section_id'];
        $shift_id = $data['shift_id'];
        $number_of_days = $data['number_of_days'];
        $sql = "SELECT * FROM `tbl_student` AS s WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id'
            AND s.`shift_id` = '$shift_id' AND s.`status` = '1' ORDER BY ABS(s.`roll_no`)";

        $students_info = $this->db->query($sql)->result_array();

        $result_information = array();
        $s = 0;
        foreach ($students_info as $student) {
            $blank_td = "";
            $i = 1;
            while ($i <= $number_of_days) {
				  $blank_td .= "<td  class='td_center'>&nbsp;</td>";
                  $i++;
            }
            //echo $status;
            //die;
            $result_information[$s]['name'] = $student['name'];
            $result_information[$s]['student_code'] = $student['student_code'];
            $result_information[$s]['roll_no'] =$student['roll_no'];
            $result_information[$s]['data'] = $blank_td;
            $s++;
        }

        return $result_information;
    }

    function get_class_name($class_id)
    {
      $this->db->select('tbl_class.*');
      $this->db->from('tbl_class');
      $this->db->where('tbl_class.id', $class_id);
      $query = $this->db->get();
      $result_info = $query->result_array();
      return $result_info[0]['name'];
    }


	function get_period_name($period_id)
	{
		$this->db->select('tbl_class_period.*');
		$this->db->from('tbl_class_period');
		$this->db->where('tbl_class_period.id', $period_id);
		$query = $this->db->get();
		$result_info = $query->result_array();
		return $result_info[0]['name'];
	}

    function get_section_name($section_id)
    {
      $this->db->select('tbl_section.*');
      $this->db->from('tbl_section');
      $this->db->where('tbl_section.id', $section_id);
      $query = $this->db->get();
      $result_info = $query->result_array();
      return $result_info[0]['name'];
    }
    function get_shift_name($shift_id)
    {
      $this->db->select('tbl_shift.*');
      $this->db->from('tbl_shift');
      $this->db->where('tbl_shift.id', $shift_id);
      $query = $this->db->get();
      $result_info = $query->result_array();
      return $result_info[0]['name'];
    }
    function get_group_name($group_id)
    {
      if(!empty($group_id))
      {
        $this->db->select('tbl_student_group.*');
        $this->db->from('tbl_student_group');
        $this->db->where('tbl_student_group.id', $group_id);
        $query = $this->db->get();
        $result_info = $query->result_array();
        return $result_info[0]['name'];
      }
      return '';

    }
    function get_month_name_by_id($monthNum)
    {
      $dateObj   = DateTime::createFromFormat('!m', $monthNum);
      $monthName = $dateObj->format('F'); // March
      // print_r($monthName);
      // die();
      return $monthName;
    }
}

?>
