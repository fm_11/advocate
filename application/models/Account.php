<?php

class Account extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }


    public function getAllFundTransferByType($type, $from_date, $to_date)
    {
        $rows = $this->db->query("SELECT ac.`name`,t.`asset_id`,SUM(t.`amount`) AS total_amount,t.`type` FROM `tbl_user_wise_asset_transaction` AS t
INNER JOIN `tbl_asset_category` AS ac ON ac.`id` = t.`asset_id`
WHERE (t.`date` BETWEEN '$from_date' AND '$to_date')
AND t.`from_transaction_type` = 'FT' AND t.`type` = '$type'
GROUP BY t.`asset_id`;")->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[$row['asset_id']]['asset_id'] = $row['asset_id'];
            $data[$row['asset_id']]['name'] = $row['name'];
            $data[$row['asset_id']]['total_amount'] = $row['total_amount'];
        }
        return $data;
    }


    public function getAllIncomeDataForBalanceSheet($date)
    {
        $rows = $this->db->query("SELECT t.`asset_id` ,SUM(t.`amount`) AS total_amount FROM `tbl_user_wise_asset_transaction` AS t
WHERE t.`date` <= '$date' AND t.`type` = 'I'
GROUP BY t.`asset_id`;")->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[$row['asset_id']]['asset_id'] = $row['asset_id'];
            $data[$row['asset_id']]['total_amount'] = $row['total_amount'];
        }
        return $data;
    }

    public function getAllIncomeTransactionByDateRange($from_date, $to_date)
    {
        $rows = $this->db->query("SELECT t.`asset_id` ,SUM(t.`amount`) AS total_amount
         FROM `tbl_user_wise_asset_transaction` AS t
WHERE (t.`date` BETWEEN '$from_date' AND '$to_date') AND t.`type` = 'I'
GROUP BY t.`asset_id`;")->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[$row['asset_id']]['asset_id'] = $row['asset_id'];
            $data[$row['asset_id']]['total_amount'] = $row['total_amount'];
        }
        return $data;
    }

    public function getAllExpenseTransactionByDateRange($from_date, $to_date)
    {
        $rows = $this->db->query("SELECT t.`asset_id` ,SUM(t.`amount`) AS total_amount
       FROM `tbl_user_wise_asset_transaction` AS t
WHERE (t.`date` BETWEEN '$from_date' AND '$to_date') AND t.`type` = 'O'
GROUP BY t.`asset_id`;")->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[$row['asset_id']]['asset_id'] = $row['asset_id'];
            $data[$row['asset_id']]['total_amount'] = $row['total_amount'];
        }
        return $data;
    }

    public function getAllExpenseDataForBalanceSheet($date)
    {
        $rows = $this->db->query("SELECT t.`asset_id` ,SUM(t.`amount`) AS total_amount FROM `tbl_user_wise_asset_transaction` AS t
WHERE t.`date` <= '$date' AND t.`type` = 'O'
GROUP BY t.`asset_id`;")->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[$row['asset_id']]['asset_id'] = $row['asset_id'];
            $data[$row['asset_id']]['total_amount'] = $row['total_amount'];
        }
        return $data;
    }

    public function getAllAssetHeadWithOpeningHead()
    {
        $rows = $this->db->query("SELECT * FROM `tbl_asset_category`")->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[$row['id']]['asset_id'] = $row['id'];
            $data[$row['id']]['name'] = $row['name'];
            $data[$row['id']]['code'] = $row['code'];
            $data[$row['id']]['opening_balance'] = $row['opening_balance'];
        }
        return $data;
    }

    public function get_all_deposit($limit, $offset, $value = '')
    {
        $this->db->select('tbl_ba_deposit.*,cc.name as cost_center');
        $this->db->from('tbl_ba_deposit');
        $this->db->join('tbl_cost_center AS cc', 'tbl_ba_deposit.cost_center_id=cc.id', 'left');

        if (isset($value) && !empty($value) && isset($value['from_date'])  && $value['from_date'] != '' && isset($value['to_date'])   && $value['to_date'] != '') {
            $this->db->where('tbl_ba_deposit.date >=', $value['from_date']);
            $this->db->where('tbl_ba_deposit.date <=', $value['to_date']);
        }

        $this->db->order_by("tbl_ba_deposit.date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_expense($limit, $offset, $value = '')
    {
        $this->db->select('tbl_ba_expense.*,cc.name as cost_center');
        $this->db->from('tbl_ba_expense');
        $this->db->join('tbl_cost_center AS cc', 'tbl_ba_expense.cost_center_id=cc.id', 'left');
        if (isset($value) && !empty($value) && isset($value['from_date'])  && $value['from_date'] != '' && isset($value['to_date'])   && $value['to_date'] != '') {
            $this->db->where('tbl_ba_expense.date >=', $value['from_date']);
            $this->db->where('tbl_ba_expense.date <=', $value['to_date']);
        }
        $this->db->order_by("tbl_ba_expense.date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_fund_transfer_info($limit, $offset, $value = '')
    {
        $this->db->select('tbl_ba_fund_transfer.*,dm.name as from_asset_category_name, dmm.name as to_asset_category_name');
        $this->db->from('tbl_ba_fund_transfer');
        $this->db->join('tbl_asset_category AS dm', 'tbl_ba_fund_transfer.from_asset_category_id=dm.id');
        $this->db->join('tbl_asset_category AS dmm', 'tbl_ba_fund_transfer.to_asset_category_id=dmm.id');
        $this->db->order_by("tbl_ba_fund_transfer.date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
}
