<?php

class User_role_wise_privilege extends CI_Model
{

    var $title = '';
    var $content = '';
    var $date = '';

    function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    /**
     * Generates a list of user wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To Generate a list of user wise privileges
     * @access  :   public
     * @param   :   int $offset, int $limit
     * @return  :   array
     */
    function get_list($offset, $limit)
    {
        $query = $this->db->get('user_role_wise_privileges', $offset, $limit);
        return $query->result();
    }

    function get_role_name_by_id($role_id)
    {
        return $this->db->query("SELECT `role_name` FROM `user_roles` WHERE `id` = '$role_id'")->row()->role_name;
    }

    /**
     * Counts number of rows of  user wise privileges table
     * @author  :   Amlan Chowdhury
     * @uses    :   To count number of rows of  user wise privileges table
     * @access  :   public
     * @return  :   int
     */
    function row_count()
    {
        return $this->db->count_all_results('user_role_wise_privileges');
    }

    /**
     * Adds data to user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To add data to user role wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function add($data)
    {
        if (isset($data['resources'])) {
            //echo '-------'.$data['role_id'].'=======';echo '<pre>';print_r($data);echo '</pre>';//die('model die');

            $this->db->trans_start();
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            //$this->insert_rows('user_role_wise_privileges', $data['column_names'],$data['column_rows']);
            $this->db->insert_batch('user_role_wise_privileges', $data['resources']);
            $this->db->trans_complete();

            return $this->db->trans_status();
        } else {
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            return true;
        }
    }

    /**
     * Updates data of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To update data of user roles wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function edit($data)
    {
        return $this->db->update('user_role_wise_privileges', $data, array('id' => $data['id']));
    }

    /**
     * Reads data of specific user role wise privilege
     * @author  :   Amlan Chowdhury
     * @uses    :   To  read data of specific user role wise privilege
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    function read($user_role_wise_privileges_id)
    {
        return $this->db->get_where('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id))->result();
    }

    /**
     * Gets data of user role wise privilege by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    function get_by_role_id($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by role id,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id, controller name and action
     * @access  :   public
     * @param   :   int $role_id, string $controller, string $action
     * @return  :   array
     */
    function get_by_role_id_controller_action($role_id, $controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name and action
     * @access  :   public
     * @param   :   string $controller, string $action
     * @return  :   array
     */
    function get_by_controller_action($controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name ,action and role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name, action and role id
     * @access  :   public
     * @param   :   string $controller, string $action, int $role_id
     * @return  :   array
     */
    function get_by_controller_action_role($controller, $action, $role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action, 'role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privileged resourses by controller role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privileged resourses by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    function get_privileged_resources($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        {
            $amlan_field_officers_reports_ccdas = array();
            $results = $query->result_array();
            foreach ($results as $result) {
                if ($result['controller'] == 'amlan_field_officers_reports') {
                    $result['id'] += 1000;
                    $result['controller'] = 'amlan_field_officers_reports_ccdas';
                    array_push($amlan_field_officers_reports_ccdas, $result);
                }
            }
            foreach ($amlan_field_officers_reports_ccdas as $amlan_field_officers_reports_ccda) {
                array_push($results, $amlan_field_officers_reports_ccda);
            }
            return $results;
        }
        return $query->result_array();
    }

    /**
     * Deletes data of specific user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    function delete($user_role_wise_privileges_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id));
    }

    /**
     * Deletes data of user role wise privileges by role id ,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of user role wise privileges by role id ,controller name and action
     * @access  :   public
     * @param   :   int $role_id,string $controller, string $action
     * @return  :   boolean
     */
    function delete_by_role_id_controller_action($role_id, $controller, $action)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
    }

    /**
     * Deletes data of specific user role wise privileges by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   boolean
     */
    function delete_by_role_id($role_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id));
    }

    /**
     * Checks permission of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  check permission of user role wise privileges
     * @access  :   public
     * @param   :   int $role_id, string $controller,string $action
     * @return  :   boolean
     */
    function check_permission($role_id, $controller, $action)
    {
        $controller = strtolower($controller);
        $action = strtolower($action);

        if ($this->is_globally_allowed_action($controller, $action)) { //if action is globally allowed, access is granted
            return true;
        } else {
            $check = $this->db->query("SELECT `id` FROM `user_role_wise_privileges` WHERE `controller` = '$controller' AND `action` = '$action' AND `role_id` = '$role_id'")->result_array();
            return !empty($check) ? true : false;
        }
    }

    /**
     * Checks if the action is globally dis-allowed or not
     * @author  :   Anis Alamgir
     * @uses    :   To check if the action is globally dis-allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     * @wiki    : http://203.188.255.195/tracker/projects/microfin360/wiki/Manage_User_Role#Only-For-Super-Admin
     */
    function is_globally_disallowed_action($controller, $action)
    {
        return isset($actions[$controller][$action]) ? true : false;
    }

    /**
     * Checks if the action is globally allowed or not
     * @author  :   Amlan Chowdhury
     * @uses    :   To check if the action is globally allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     */
    function is_globally_allowed_action($controller, $action)
    {
        $actions = array();
        $actions['login']['index'] = 1;
		$actions['login']['download_zone'] = 1;
        $actions['login']['student_guardian_login'] = 1;
        $actions['login']['student_guardian_login_authentication'] = 1;
        $actions['login']['student_guardian_logout'] = 1;
        $actions['login']['forgot_password'] = 1;
        $actions['login']['student_forgot_password'] = 1;
        $actions['login']['authentication'] = 1;
        $actions['login']['logout'] = 1;
        $actions['login']['denied'] = 1;
		$actions['login']['change_password'] = 1;
		$actions['students']['getStudentByClassShiftSection'] = 1;
		$actions['students']['getStudentInformationById'] = 1;
		$actions['service_bridge']['getNumberOfStudent'] = 1;
		$actions['service_bridge']['smsBalanceRecharge'] = 1;
        return isset($actions[$controller][$action]);
    }


    function get_all_resources_array()
    {
        define("VIEW", "View");
        define("ADD", "Add");
        define("EDIT", "Edit");
        define("DELETE", "Delete");
        //initializing
        $group_id = 0;
        $tmp = array();
        //Organization management group


        $group_name = $this->lang->line('dashboard');
        $subgroup_name = $this->lang->line('dashboard');

        $entity_name = $this->lang->line('dashboard');
        $controller = 'dashboard';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $group_name = $this->lang->line('setting');
        $subgroup_name = 'General Settings';

        $entity_name = $this->lang->line('office').' '.$this->lang->line('setting');
        $controller = 'institute_setup';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'update');

        $entity_name = $this->lang->line('dashboard_colour_settings');
        $controller = 'dashboard_colour_settings';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');

        $entity_name = $this->lang->line('court_type');
        $controller = 'ad_court_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('court_sub_type');
        $controller = 'ad_courts';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('judge_type');
        $controller = 'judge';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        $entity_name = $this->lang->line('case_type');
        $controller = 'ad_case_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('case_sub_type');
        $controller = 'ad_case_sub_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('case_status');
        $controller = 'ad_case_status';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('form_type');
        $controller = 'form_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('division');
        $controller = 'divisions';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = $this->lang->line('district');
        $controller = 'districts';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');


        //User management
    		$group_name = $this->lang->line('user').' '.$this->lang->line('management');
    		$subgroup_name = $this->lang->line('user').' '.$this->lang->line('management');

    		$entity_name = $this->lang->line('team').' '.$this->lang->line('member').' '.$this->lang->line('information');
        $controller = 'client_user';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('user').' '.$this->lang->line('role');
        $controller = 'user_roles';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        #Clients
        $group_name =  $this->lang->line('clients');
        $subgroup_name = $this->lang->line('clients');

        $entity_name = $this->lang->line('clients').' '.$this->lang->line('information');
        $controller = 'clients';
		    $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
    		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
    		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        #Cases

        $group_name = $this->lang->line('case');
        $subgroup_name = $this->lang->line('case');

        $entity_name = $this->lang->line('case').' '.$this->lang->line('details');
        $controller = 'cases';
    		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
    		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');

        $entity_name = $this->lang->line('case').' '.$this->lang->line('history');
        $controller = 'case_history';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('court_allowcations');
        $controller = 'data_copy';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'district');

        $entity_name = $this->lang->line('case').' '.$this->lang->line('task');
        $controller = 'tasks';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('case').' '.$this->lang->line('appointments');
        $controller = 'appointments';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        //report

    		$group_name = $this->lang->line('report');
    		$subgroup_name = $this->lang->line('report');

    		$entity_name = $this->lang->line('attendance_report');
    		$controller = 'attendance_reports';
    		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = $this->lang->line('forms').' '.$this->lang->line('file').' '.$this->lang->line('upload');
        $controller = 'file_upload';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = $this->lang->line('case').' '.$this->lang->line('file').' '.$this->lang->line('upload');
        $controller = 'file_upload';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete_case_file','remove_file'));



		    return $tmp;
    }

    function create_resource(&$tmp, $group_name, $subgroup_name, $entity_name, $controller, $action_title, $actions = null)
    {
        if (is_null($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = strtolower($action_title);
        } elseif (is_string($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = $actions;
        } elseif (is_array($actions)) {
            foreach ($actions as $key => $row) {
                $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][$key]['name'] = $row;
            }
        }

        return $tmp;
    }

}
