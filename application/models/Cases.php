<?php

class Cases extends CI_Model
{

    function __construct()
    {
      $this->load->database();
      $this->load->library('session');
      parent::__construct();
    }

    public function get_all_upazila_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*, d.name as dis_name, d.bn_name as dis_bnname');
        $this->db->from('ad_upazilas as s');
        $this->db->join('ad_districts AS d', 's.district_id=d.id', 'left');

        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
           $this->db->group_start();
            $this->db->like('s.name', $value['name']);
            $this->db->or_like('s.bn_name', $value['name']);
            $this->db->group_end();
        }
        if (isset($value) && !empty($value) && isset($value['district_id']) && $value['district_id'] != '') {
            $this->db->where('s.district_id', $value['district_id']);
        }
      //  $this->db->order_by("d.name", "asce");
        $this->db->order_by("s.serial_no", "asce");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_district_list_by_language($lang)
    {
      if($lang=='english')
      {
        return $this->db->query("SELECT id,name AS name FROM `ad_districts`")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name AS name FROM `ad_districts`")->result_array();
      }
    }

    function edit_upazila($data, $id)
    {
        return $this->db->update('ad_upazilas', $data, array('id' => $id));
    }
    function add_upazila($data)
    {

        return $this->db->insert('ad_upazilas', $data);
    }
    function read_upazila_by_upazila_id($upazila_id)
    {
        return $this->db->query("SELECT * FROM `ad_upazilas` where id= $upazila_id")->row();
    }

    //Case Status

    public function get_case_status_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_case_status as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_case_status($data)
    {
        return $this->db->insert('ad_case_status', $data);
    }


    function edit_case_status($data, $id)
    {
        return $this->db->update('ad_case_status', $data, array('id' => $id));
    }


    function read_case_status($id)
    {
        return $this->db->get_where('ad_case_status', array('id' => $id))->row();
    }

    function delete_case_status($id)
    {
        return $this->db->delete('ad_case_status', array('id' => $id));
    }

    public function checkifexist_case_status_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_case_status where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_case_status_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_case_status where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Tax

    public function get_tax_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_tax as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_tax($data)
    {
        return $this->db->insert('ad_tax', $data);
    }


    function edit_tax($data, $id)
    {
        return $this->db->update('ad_tax', $data, array('id' => $id));
    }


    function read_tax($id)
    {
        return $this->db->get_where('ad_tax', array('id' => $id))->row();
    }

    function delete_tax($id)
    {
        return $this->db->delete('ad_tax', array('id' => $id));
    }

    public function checkifexist_tax_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_tax where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_tax_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_tax where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Service

    public function get_all_services_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_service as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_services($data)
    {
        return $this->db->insert('ad_service', $data);
    }

    function edit_services($data, $id)
    {
        return $this->db->update('ad_service', $data, array('id' => $id));
    }

    function read_services($id)
    {
        return $this->db->get_where('ad_service', array('id' => $id))->row();
    }

    function delete_services($id)
    {
        return $this->db->delete('ad_service', array('id' => $id));
    }

    public function checkifexist_services_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_service where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_services_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_service where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Client

    public function get_all_clients_list($limit, $offset, $value = '')
    {
        // $this->db->select('v.*,c.`name` AS country_name,d.`name` AS district_name,d.`name` AS upazilas_name');
        $this->db->select('v.*,d.`name` AS district,d.bn_name as bn_district_name,u.name as upazila,u.bn_name as bn_upazila_name');
        $this->db->from('`ad_clients AS v');
        //$this->db->join('ad_country AS c', 'v.country_id=c.id', 'left');
        $this->db->join('ad_districts AS d', 'v.district_id=d.id', 'left');
        $this->db->join('ad_upazilas AS u', 'v.upazilas_id=u.id', 'left');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('v.first_name', $value['name']);
        }
        if (isset($value['status']) && isset($value['status']) && $value['status'] != '') {
            $this->db->where('v.is_active', $value['status']);
        }
        if (isset($value['mobile']) && isset($value['mobile']) && $value['mobile'] != '') {
            $this->db->like('v.mobile', $value['mobile']);
        }
        if (isset($value['district_id']) && isset($value['district_id']) && $value['district_id'] != '') {
            $this->db->where('v.district_id', $value['district_id']);
        }
        if (isset($value['upazilas_id']) && isset($value['upazilas_id']) && $value['upazilas_id'] != '') {
            $this->db->where('v.upazilas_id', $value['upazilas_id']);
        }
        $this->db->order_by("v.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_clients($data)
    {
        return $this->db->insert('ad_clients', $data);
    }

    function edit_clients($data, $id)
    {
        return $this->db->update('ad_clients', $data, array('id' => $id));
    }

    function read_clients($clients_id)
    {
        return $this->db->get_where('ad_clients', array('id' => $clients_id))->row();
    }

    function delete_clients($clients_id)
    {
        return $this->db->delete('ad_clients', array('id' => $clients_id));
    }

    public function checkifexist_clients_by_mobile($mobile,$name)
    {
      $count = $this->db->query("SELECT id FROM ad_clients where mobile = '$mobile' and  first_name='$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    public function checkifexist_update_clients_by_mobile($mobile,$name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_clients where mobile = '$mobile' and  first_name='$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    function get_country_list()
    {
        return $this->db->query("SELECT * FROM `ad_country` ")->result_array();
    }
    function get_districts_list_by_country($country_id)
    {
        return $this->db->query("SELECT id,name FROM `ad_districts` where country_id= $country_id")->result_array();
    }
    function read_districts_by_district_id($district_id)
    {
        return $this->db->query("SELECT * FROM `ad_districts` where id= $district_id")->row();
    }
    function edit_districts($data, $id)
    {
        return $this->db->update('ad_districts', $data, array('id' => $id));
    }
    function get_upazilas_list_by_district($district_id=0)
    {
        return $this->db->query("SELECT * FROM `ad_upazilas` where district_id= $district_id")->result_array();
    }
    function get_upazilas_list_by_district_and_lang($lang,$district_id=0)
    {
      if($lang=='english')
      {
        return $this->db->query("SELECT id,name FROM `ad_upazilas` where district_id= $district_id")->result_array();
      }
      return $this->db->query("SELECT id,bn_name as name FROM `ad_upazilas` where district_id= $district_id")->result_array();
    }
    function get_division_list_by_language($lang)
    {
      if($lang=='english')
      {
        return $this->db->query("SELECT id,name AS name FROM `ad_divisions`")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name AS name FROM `ad_divisions`")->result_array();
      }
    }
    function get_division_list_by_language_division_id($lang,$division_id)
    {
      if($lang=='english')
      {
        return $this->db->query("SELECT id,name AS name FROM `ad_divisions` where id=$division_id")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name AS name FROM `ad_divisions` where id=$division_id")->result_array();
      }
    }
    function get_division_list_by_language_country($lang,$country_id)
    {
      if($lang=='english')
      {
        return $this->db->query("SELECT id,name AS name FROM `ad_divisions` where country_id=$country_id")->result_array();
      }else{
        return $this->db->query("SELECT id,bn_name AS name FROM `ad_divisions` where country_id=$country_id")->result_array();
      }
    }
    function get_all_districts_list_by_division($division_id,$lang)
    {
        if($lang=='english')
        {
            return $this->db->query("SELECT id,name FROM `ad_districts` where division_id= $division_id")->result_array();
        }
        return $this->db->query("SELECT id,bn_name AS name FROM `ad_districts` where division_id= $division_id")->result_array();
    }
    function get_all_court_list()
    {

        return $this->db->query("SELECT c.`court_type_id`,c.`id`,c.`name`,t.`name` AS court_type FROM `ad_court` AS c
                                  INNER JOIN `ad_court_type` AS t ON c.`court_type_id`=t.`id`
                                  ORDER BY c.`name`")->result_array();

    }


    function get_all_districts_list_by_division_and_language($division_id,$lang){
      $data   = array();
      if($lang=='english')
      {
          $data= $this->db->query("SELECT id,name,division_id FROM `ad_districts` where division_id= $division_id order by serial_no asc")->result_array();
      }else{
        $data= $this->db->query("SELECT id,bn_name AS name,division_id FROM `ad_districts` where division_id= $division_id order by serial_no asc")->result_array();
      }
      foreach( $data as $key=>$each ){

          $data[$key]['court_list']   = $this->get_court_list_by_district_id($each['id']);

          //$this->db->where('category_id', $each['id'])->get('tbl_sub_category')->result_array();
      }

      return $data;
  }
  function get_all_districts_list_by_district_division_and_language($division_id,$district_id,$lang){
    $data   = array();
    if($lang=='english')
    {
        $data= $this->db->query("SELECT id,name,division_id FROM `ad_districts` where division_id= $division_id and id=$district_id order by serial_no asc")->result_array();
    }else{
      $data= $this->db->query("SELECT id,bn_name AS name,division_id FROM `ad_districts` where division_id= $division_id and id=$district_id order by serial_no asc")->result_array();
    }
    foreach( $data as $key=>$each ){

        $data[$key]['court_list']   = $this->get_court_list_by_district_id($each['id'],$lang);

        //$this->db->where('category_id', $each['id'])->get('tbl_sub_category')->result_array();
    }

    return $data;
}
  function get_division_name_by_language($division_id,$lang){
    $name   = "";
    if($lang=='english')
    {
        $data= $this->db->query("SELECT id,name FROM `ad_divisions` where id= $division_id")->row();
        $name=$data->name;
    }else{
       $data= $this->db->query("SELECT id,bn_name AS name FROM `ad_divisions` where id= $division_id")->row();
        $name=$data->name;
    }


    return $name;
}

  function get_court_list_by_district_id($district_id,$lang)
  {
        if($lang=='english')
        {
          return $this->db->query("SELECT * FROM (
                  SELECT c.`court_type_id`,d.`court_id`,c.`name` AS court_name,
                  (SELECT NAME FROM `ad_court_type` WHERE id=c.`court_type_id`) AS court_type,d.`district_id`,1 checked,
                  (SELECT `order_no` FROM `ad_court_type` WHERE id=c.`court_type_id`) AS order_no
                   FROM `district_wise_court_allowcations` AS d
                  INNER JOIN `ad_court` AS c ON d.`court_id`=c.`id`
                  WHERE c.is_active=1 and d.`district_id`=$district_id
                  UNION ALL
                  SELECT c.`court_type_id`,c.`id` AS court_id,c.`name` AS court_name,t.`name` AS court_type,$district_id district_id,0 checked,t.order_no FROM `ad_court` AS c
                  INNER JOIN `ad_court_type` AS t ON c.`court_type_id`=t.`id`
                  WHERE c.is_active=1 and c.`id` NOT IN(
                  SELECT d.`court_id` FROM `district_wise_court_allowcations` AS d
                  WHERE d.`district_id`=$district_id
                  )
                ) AS t ORDER BY t.order_no, t.court_name")->result_array();
        }
    return $this->db->query("SELECT * FROM (
            SELECT c.`court_type_id`,d.`court_id`,c.`bn_name` AS court_name,
            (SELECT bn_name FROM `ad_court_type` WHERE id=c.`court_type_id`) AS court_type,d.`district_id`,1 checked,
            (SELECT `order_no` FROM `ad_court_type` WHERE id=c.`court_type_id`) AS order_no
             FROM `district_wise_court_allowcations` AS d
            INNER JOIN `ad_court` AS c ON d.`court_id`=c.`id`
            WHERE c.is_active=1 and d.`district_id`=$district_id
            UNION ALL
            SELECT c.`court_type_id`,c.`id` AS court_id,c.`bn_name` AS court_name,t.`bn_name` AS court_type,$district_id district_id,0 checked,t.order_no FROM `ad_court` AS c
            INNER JOIN `ad_court_type` AS t ON c.`court_type_id`=t.`id`
            WHERE c.is_active=1 and c.`id` NOT IN(
            SELECT d.`court_id` FROM `district_wise_court_allowcations` AS d
            WHERE d.`district_id`=$district_id
            )
          ) AS t ORDER BY t.order_no, t.court_name")->result_array();
  }
    // Appointment

    public function get_all_appointments_list($limit, $offset, $value = '')
    {
        $this->db->select('a.*,c.first_name,c.last_name,c.mobile as client_mobile');
        $this->db->from('ad_appointment AS a');
        $this->db->join('ad_clients AS c', 'a.client_id=c.id', 'left');

        if (isset($value) && !empty($value) && isset($value['from_date'])  && $value['from_date'] != '' && isset($value['to_date'])   && $value['to_date'] != '') {
            $this->db->where('a.date >=', $value['from_date']);
            $this->db->where('a.date <=', $value['to_date']);
        }

        $this->db->order_by("a.date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_appointments($data)
    {
        return $this->db->insert('ad_appointment', $data);
    }

    function edit_appointments($data, $id)
    {
        return $this->db->update('ad_appointment', $data, array('id' => $id));
    }

    function read_appointments($id)
    {
        return $this->db->get_where('ad_appointment', array('id' => $id))->row();
    }

    function delete_appointments($id)
    {
        return $this->db->delete('ad_appointment', array('id' => $id));
    }

    function get_client_list()
    {
        return $this->db->query("SELECT * FROM `ad_clients` ")->result_array();
    }

    function get_mobile_by_client_id($client_id)
    {
        return $this->db->query("SELECT id,mobile FROM `ad_clients` where id= $client_id")->result_array();
    }

    // Tasks

    public function get_all_tasks_list($limit, $offset, $value = '')
    {
      $this->db->select('t.id,t.subject,t.related_to,t.start_date,t.deadline_date,CONCAT(c.`first_name`," ",c.`last_name`) AS members,s.name AS status_name,p.name AS priority_name');

      $this->db->from('ad_task AS t');
      $this->db->join('ad_task_priority AS p', 'p.id= t.priority_id');
      $this->db->join('ad_task_status AS s', 's.id=t.status_id');
      $this->db->join('ad_client_user AS c', 'c.id=t.client_user_id');
      // $this->db->select('s.*');
      // $this->db->from('ad_task as s');
      // if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
      //     $this->db->like('s.name', $value['name']);
      // }
      // if (isset($value['status']) && $value['status'] != '') {
      //     $this->db->where('s.is_active', $value['status']);
      // }
      $this->db->order_by("s.id", "desc");
      if (isset($limit) && $limit > 0) {
          $this->db->limit($limit, $offset);
      }
      $query = $this->db->get();
      return $query->result_array();
    }

    function add_tasks($data)
    {
        return $this->db->insert('ad_task', $data);
    }

    function edit_tasks($data, $id)
    {
        return $this->db->update('ad_task', $data, array('id' => $id));
    }

    function read_tasks($id)
    {
        return $this->db->get_where('ad_task', array('id' => $id))->row();
    }

    function delete_tasks($id)
    {
        return $this->db->delete('ad_task', array('id' => $id));
    }

    function get_status_list()
    {
        return $this->db->query("SELECT * FROM `ad_task_status` ")->result_array();
    }

    function get_priority_list()
    {
        return $this->db->query("SELECT * FROM `ad_task_priority` ")->result_array();
    }

    function get_client_user_list()
    {
        return $this->db->query("SELECT * FROM `ad_client_user` ")->result_array();
    }

    // function get_case_list()
    // {
    //     return $this->db->query("SELECT * FROM `ad_master_case` ")->result_array();
    // }

    //Appointment status

    public function get_appointment_status_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_appointment_status as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_appointment_status($data)
    {
        return $this->db->insert('ad_appointment_status', $data);
    }


    function edit_appointment_status($data, $id)
    {
        return $this->db->update('ad_appointment_status', $data, array('id' => $id));
    }


    function read_appointment_status($id)
    {
        return $this->db->get_where('ad_appointment_status', array('id' => $id))->row();
    }

    function delete_appointment_status($id)
    {
        return $this->db->delete('ad_appointment_status', array('id' => $id));
    }

    public function checkifexist_appointment_status_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_appointment_status where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_appointments_status_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_appointment_status where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Hiring status

    public function get_all_hiring_status_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_hiring_status as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_hiring_status($data)
    {
        return $this->db->insert('ad_hiring_status', $data);
    }


    function edit_hiring_status($data, $id)
    {
        return $this->db->update('ad_hiring_status', $data, array('id' => $id));
    }


    function read_hiring_status($judge_id)
    {
        return $this->db->get_where('ad_hiring_status', array('id' => $judge_id))->row();
    }

    function delete_hiring_status($judge_id)
    {
        return $this->db->delete('ad_hiring_status', array('id' => $judge_id));
    }
    public function checkifexist_hiring_status_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_hiring_status where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_hiring_status_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_hiring_status where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Hiring Type

    public function get_all_hiring_types_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('ad_hiring_type as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        if (isset($value['status']) && $value['status'] != '') {
            $this->db->where('s.is_active', $value['status']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_hiring_types($data)
    {
        return $this->db->insert('ad_hiring_type', $data);
    }

    function edit_hiring_types($data, $id)
    {
        return $this->db->update('ad_hiring_type', $data, array('id' => $id));
    }


    function read_hiring_types($judge_id)
    {
        return $this->db->get_where('ad_hiring_type', array('id' => $judge_id))->row();
    }

    function delete_hiring_types($judge_id)
    {
        return $this->db->delete('ad_hiring_type', array('id' => $judge_id));
    }
    public function checkifexist_hiring_types_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM ad_hiring_type where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_hiring_types_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM ad_hiring_type where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    function add_advocate_setting($data)
    {
        return $this->db->insert('ad_advocate_setting', $data);
    }

    function edit_advocate_setting($data, $id)
    {
        return $this->db->update('ad_advocate_setting', $data, array('id' => $id));
    }
    function add_email_setting($data)
    {
        return $this->db->insert('tbl_mail_configuration', $data);
    }

}
