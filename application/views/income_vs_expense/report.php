<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
}
table {
  border-collapse: collapse;
}
</style>

<?php
 } ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="2" style="text-align:center;">
            <b><?php echo $HeaderInfo['school_name']; ?><br>
            <?php if ($HeaderInfo['eiin_number'] != '') {
     echo "EIIN: " . $HeaderInfo['eiin_number'] . "<br>";
 } ?>
            <?php echo $title; ?>
           </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Date: <b><?php echo date("M jS, Y", strtotime($from_date)); ?> to <?php echo date("M jS, Y", strtotime($to_date)); ?></b>
        </td>
    </tr>
    </thead>

	  <tbody>
	    <tr>
            <td align="left" colspan="2">
                <b><?php echo $this->lang->line('income'); ?> <?php echo $this->lang->line('details'); ?></b>
            </td>
        </tr>
    <?php
    $i = 0;
    $sub_total_amount = 0;
    $total_expense_amount = 0;
    $total_amount = 0;
    foreach ($student_fee_details as $row):
        if ($row['total_amout'] != '' && $row['total_amout'] >= 0) {
            $i++; ?>
      <tr>
          <td width="40%" align="left">
              <?php echo $row['sub_category_name']; ?><br>
          </td>
			    <td>
              <?php $sub_total_amount+= $row['total_amout'];
            echo number_format($row['total_amout'], 2); ?><br>
          </td>
        </tr>
    <?php
        } endforeach; ?>
	   <tr>
	       <td align="left"><b><?php echo $this->lang->line('sub'); ?> <?php echo $this->lang->line('total'); ?>&nbsp;</b></td>
		     <td>
           <b>
		       <?php
               echo number_format($sub_total_amount, 2);
               $total_amount = $total_amount + $sub_total_amount;
               ?>
		      </b>
        </td>
	   </tr>

     <?php
     if (!empty($deposit_info)) {
         ?>
	   <tr>
	     <td align="left" colspan="2">
           <b><?php echo $this->lang->line('other'); ?> <?php echo $this->lang->line('deposited'); ?> <?php echo $this->lang->line('income'); ?></b>
       </td>
	   </tr>

     <?php
       $total_deposit_amount = 0;
         foreach ($deposit_info as $d_row):
     ?>
     <tr>
         <td align="left">
           <?php echo $d_row['income_cat_name']; ?>
         </td>
        <td>

        <?php
            echo number_format($d_row['total_amount'], 2);
         $total_deposit_amount = $total_deposit_amount + $d_row['total_amount']; ?>
        </td>
     </tr>
     <?php
       endforeach; ?>

       <tr>
  	       <td align="left"><b>Total <?php echo $this->lang->line('other'); ?> <?php echo $this->lang->line('deposited'); ?> <?php echo $this->lang->line('income'); ?></b></td>
    		   <td>
    		       <b>
    		       <?php
                  $total_amount = $total_amount + $total_deposit_amount;
         echo number_format($total_deposit_amount, 2); ?>
    		       </b>
    		  </td>
  	   </tr>

  <?php
     } ?>

<?php if ($special_care_fee_details != '' && $special_care_fee_details >= 0) { ?>
	   <tr>
	       <td align="left"><b><?php echo $this->lang->line('special_care'); ?> <?php echo $this->lang->line('fees'); ?> <?php echo $this->lang->line('income'); ?>&nbsp;</b></td>
		   <td><b>
		   <?php
           echo number_format($special_care_fee_details, 2);
            $total_amount = $total_amount + $special_care_fee_details;
           ?>
		  </b></td>
	   </tr>
       <?php } ?>

	    <tr>
	       <td align="left"><b> <?php echo $this->lang->line('total') . ' ' . $this->lang->line('income'); ?></b></td>
		   <td>
		       <b>
		       <?php
               echo number_format($total_amount, 2);
               ?>
		       </b>
		  </td>
	   </tr>

    <?php
    if (!empty($expense_details)) {
        ?>


	   <tr>
          <td align="left" colspan="2">
              <b><?php echo $this->lang->line('expense'); ?> <?php echo $this->lang->line('details'); ?></b>
          </td>
      </tr>

    <?php

        foreach ($expense_details as $e_row):
    ?>
    <tr>
        <td align="left">
          <?php echo $e_row['expense_cat_name']; ?>
        </td>
       <td>

       <?php
           echo number_format($e_row['total_amount'], 2);
        $total_expense_amount = $total_expense_amount + $e_row['total_amount']; ?>

       </td>
    </tr>
    <?php
      endforeach; ?>

     <tr>
	       <td align="left"><b> <?php echo $this->lang->line('expense'); ?> <?php echo $this->lang->line('amount'); ?>&nbsp;</b></td>
  		   <td>
  		       <b>
  		       <?php
                 echo number_format($total_expense_amount, 2); ?>
  		       </b>
  		  </td>
	   </tr>

    <?php
    } ?>

    <tr>
        <td align="left"><b><?php echo $this->lang->line('income') . ' - ' . $this->lang->line('expense') ; ?></b></td>
        <td>
            <b>
            <?php
                echo number_format($total_amount - $total_expense_amount, 2); ?>
            </b>
       </td>
    </tr>

    </tbody>
</table>
</div>
