<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
  font-size: 11px;
  padding-left: 3px !important;
  padding-right: 3px !important;
  padding: 0px;
}
table {
  border-collapse: collapse;
}
.h_td{
   font-weight: bold !important;
}
</style>

<?php
 } ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="5"  class="h_td" style="text-align:center; line-height:1.5;">
            <b><?php echo $HeaderInfo['school_name']; ?></b><br>
            <?php echo $HeaderInfo['address']; ?><br>
            <?php echo $title; ?> of <b>(<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)</b>
        </td>
    </tr>

    <tr>
        <td scope="col" class="h_td"><b>Ledger</b></td>
        <td scope="col" class="h_td"><b>Opening Balance</b></td>
        <td scope="col"  class="h_td" align="center">
          <b>In Amount</b></br>
          (During the Period)
        </td>
        <td scope="col"  class="h_td" align="center">
          <b>Out Amount</b>
        </br>
        (During the Period)
        </td>
        <td scope="col"  class="h_td" align="center">
          <b>Closing Balance</b>
        </td>
    </tr>
    </thead>
    <tbody>

    <tr>
      <td colspan="5" class="h_td"><b>Asset</b></td>
    </tr>

    <?php

    foreach ($asset_head_data as $arow):
          ?>
        <tr>
            <td>
                <?php echo $arow['name']; ?>
            </td>
            <td align="right">
              <?php
              echo number_format($arow['opening_balance_for_this_period'], 2); ?>
            </td>

            <td align="right">
              <?php
              echo number_format($arow['income_for_this_period'], 2); ?>
            </td>

            <td align="right">
              <?php
              echo number_format($arow['expense_for_this_period'], 2); ?>
            </td>

            <td align="right">
              <?php
              echo number_format($arow['closing_balance_for_this_period'], 2); ?>
            </td>

        </tr>
    <?php
       endforeach; ?>




    <tr>
      <td colspan="5"><b>Income</b></td>
    </tr>
    <?php
    $total_income = 0;
    foreach ($income_data as $row):

      if ($row['total_amout'] != '' && $row['total_amout'] > 0) {
          ?>
        <tr>
            <td>
                <?php echo $row['sub_category_name']; ?>
            </td>
            <td align="right">-</td>
            <td align="right">
              <?php
              $total_income += $row['total_amout'];
          echo number_format($row['total_amout'], 2); ?>
            </td>
            <td align="right">-</td>
            <td align="right">-</td>
        </tr>
    <?php
      } endforeach; ?>

      <tr>
        <td align="left"><b>Total Income<b/></td>
        <td align="right">-</td>
        <td align="right"><b><?php echo number_format($total_income, 2); ?><b/></td>
        <td align="right">-</td>
        <td align="right">-</td>
      </tr>


      <?php
        $total_deposit_amount = 0;
      if (!empty($other_deposit_info)) {
          ?>
 	   <tr>
 	     <td align="left" colspan="5">
            <b><?php echo $this->lang->line('other'); ?> <?php echo $this->lang->line('deposited'); ?> <?php echo $this->lang->line('income'); ?></b>
        </td>
 	   </tr>

      <?php

          foreach ($other_deposit_info as $d_row):
      ?>
      <tr>
          <td align="left">
            <?php echo $d_row['income_cat_name']; ?>
          </td>
          <td align="right">-</td>
         <td align="right">

         <?php
             echo number_format($d_row['total_amount'], 2);
          $total_deposit_amount = $total_deposit_amount + $d_row['total_amount']; ?>
         </td>
         <td align="right">-</td>
         <td align="right">-</td>
      </tr>
      <?php
        endforeach; ?>

        <tr>
   	       <td align="left"><b>Total <?php echo $this->lang->line('other'); ?> <?php echo $this->lang->line('deposited'); ?> <?php echo $this->lang->line('income'); ?></b></td>
           <td align="right">-</td>
           <td align="right">
     		       <b>
     		       <?php
                   //$total_amount = $total_amount + $total_deposit_amount;
                  echo number_format($total_deposit_amount, 2); ?>
     		       </b>
     		  </td>
          <td align="right">-</td>
          <td align="right">-</td>
   	   </tr>

   <?php
      } ?>


      <tr>
        <td colspan="5"><b>Expense</b></td>
      </tr>
      <?php
      $total_expense = 0;
      foreach ($expense_data as $e_row):
        if ($e_row['total_amount'] != '' && $e_row['total_amount'] > 0) {
            ?>
          <tr>
              <td>
                  <?php echo $e_row['cat_name']; ?>
              </td>
              <td align="right">-</td>
              <td align="right">-</td>
              <td align="right">
                <?php
                $total_expense += $e_row['total_amount'];
            echo number_format($e_row['total_amount'], 2); ?>
              </td>
              <td align="right">-</td>
          </tr>
      <?php
        } endforeach; ?>

        <tr>
           <td align="left"><b>Total Expense<b/></td>
           <td align="right">-</td>
           <td align="right">-</td>
           <td align="right"><b><?php echo number_format($total_expense, 2); ?><b/></td>
           <td align="right">-</td>
        </tr>

      <tr>
        <td colspan="5"><b>Fund Transfer</b></td>
      </tr>

      <?php
      foreach ($return_fund_transfer_data as $rt_row):
        if ($rt_row['name'] != 'NA') {
            ?>
          <tr>
              <td>
                  <?php echo $rt_row['name']; ?>
              </td>
              <td align="right">-</td>
              <td align="right">
                <?php
                echo number_format($rt_row['in'], 2); ?>
              </td>
              <td align="right">
                <?php
                echo number_format($rt_row['out'], 2); ?>
              </td>

              <td align="right">-</td>
          </tr>
      <?php
        } endforeach; ?>


      <tr>
         <td align="left" colspan="4"><b>Income - Expense<b/></td>
         <td align="right"><b><?php echo number_format(($total_income + $total_deposit_amount) - $total_expense, 2); ?><b/></td>
      </tr>

    </tbody>
</table>
</div>
