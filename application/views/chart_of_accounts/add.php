<form  name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>chart_of_accounts/add" method="post">
      <div class="form-row">
        <div class="form-group col-md-3">
          <label>Category</label>
          <select name="category_id" id="category_id" required class="js-example-basic-single w-100">
            <option value="">--Plase Select--</option>
              <?php
              foreach ($main_head_list as $cof) {
                  ?>
                  <option value="<?php echo $cof['id']; ?>"><?php echo $cof['name']; ?></option>
                          <?php
              }
                      ?>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label>Name</label>
          <input type="text" autocomplete="off"  name="name" required class="form-control" value="">
        </div>

        <div class="form-group col-md-3">
          <label>Code</label>
          <input type="text" autocomplete="off"  name="code" required class="form-control" value="">
        </div>

        <div class="form-group col-md-3">
          <label>Opening Balance</label>
          <input type="text" autocomplete="off"  name="opening_balance" required class="form-control" value="">
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-12">
          <label><?php echo $this->lang->line('remarks'); ?></label>
          <textarea rows="5" cols="50" class="form-control" name="remarks"></textarea>
        </div>
      </div>

      <div class="float-right">
    	   <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
    	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    	</div>
</form>
