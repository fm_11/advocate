<script type="text/javascript">


    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/updateMsgClientsStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }
</script>
<style>
 #order-listing_filter{
   float: right !important;
 }
</style>


<div class="row">

  <div class="col-12">

    <div class="table-responsive">
      <table id="order-listing" class="table">
        <thead>
          <tr>
            <th><?php echo $this->lang->line('sl'); ?></th>
            <th><?php echo $this->lang->line('case_no'); ?></th>
            <th><?php echo $this->lang->line('upazila'); ?></th>
            <th><?php echo $this->lang->line('respondent_or_pettion'); ?></th>
            <th><?php echo $this->lang->line('bangla').' '.$this->lang->line('name')?></th>
            <th><?php echo $this->lang->line('english').' '.$this->lang->line('name')?></th>
            <th><?php echo $this->lang->line('mobile'); ?></th>
            <th><?php echo $this->lang->line('next').' '.$this->lang->line('date'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i = 0;
          foreach ($missed_attendance as $row):
              $i++;
              ?>
          <tr>
              <td><?php echo $i; ?></td>
              <td>
               <a class="" target="_blank" href="<?php echo base_url(); ?>case_history/add/<?php echo $row['case_id']; ?>">
                <?php
                if($language=="english")  {
                echo $row['case_sub_name'].'-'.$row['case_no'];

                }else{
                echo $row['case_sub_bnname'].'-'.$row['case_no'];
                }
                ?>
                </a>
              </td>
              <td>
                <?php
                if($language=="english")  {
                echo $row['upazila_name'];

                }else{
                echo $row['upazila_bnname'];
                }
                ?>
              </td>
              <td>
                <?php
                if($language=="english")  {
                  if($row['is_petitioner']==1)
                  {
                     echo "Petitioner";
                  }else{
                    echo "Respondent";
                  }
                }else{
                  if($row['is_petitioner']==1)
                  {
                     echo "বাদী";
                  }else{
                    echo "বিবাদী";
                  }
                }
                ?>
              </td>
              <td><?php echo $row['first_name']; ?></td>
              <td> <?php echo $row['last_name']; ?></td>
              <td><?php echo $row['mobile']; ?></td>
              <td><?php echo date("d-m-Y", strtotime($row['bussines_on_date'])) ?></td>
         <!-- <a class="" href="<?php echo base_url(); ?>clients/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a> -->
          </tr>
  <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
