<form name="updateForm" action="<?php echo base_url(); ?>founder_infos/edit" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-2 text-center">
			<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $founder_info->picture; ?>" class="img-lg rounded-circle mb-3"/>
		</div>
		<div class="form-group col-md-4">
			<label>New Image</label>
			<input type="file" name="txtPhoto" class="form-control">
		</div>
		<div class="form-group col-md-6">
			<label>Name</label>
			<input type="text" autocomplete="off"  name="name"  class="form-control" value="<?php echo $founder_info->name;  ?>">
		</div>
	</div>


	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Years active</label>
			<input type="text" autocomplete="off"  name="years_of_active"  class="form-control" value="<?php echo $founder_info->years_of_active;  ?>">
		</div>
		<div class="form-group col-md-4">
			<label>Born</label>
			<input type="text" autocomplete="off"  name="born"  class="form-control" value="<?php echo $founder_info->born;  ?>">
		</div>
		<div class="form-group col-md-4">
			<label>Educational Qualification</label>
			<input type="text" autocomplete="off"  name="edu_qua"  class="form-control" value="<?php echo $founder_info->edu_qua;  ?>">
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			Occupation
			<input type="text" autocomplete="off"  name="occupation"  class="form-control" value="<?php echo $founder_info->occupation;  ?>">
		</div>
		<div class="form-group col-md-8">
			Address
			<input type="text" autocomplete="off"  name="address"  class="form-control" value="<?php echo $founder_info->address;  ?>">
		</div>
	</div>


	<input type="hidden" name="id"  class="form-control" value="<?php echo $founder_info->id;  ?>">
	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
