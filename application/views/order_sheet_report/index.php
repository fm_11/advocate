<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getDisTrictWiseThanaOrCourt(district_id,type){
       if(type=='T')
       {
         document.getElementById("gr_cr_upazilla_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?> --</option>";
       }else if (type=='C') {
         document.getElementById("court_type_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>---</option>";
         document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>---</option>";
       }

        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
              if(type=='T')
              {
                document.getElementById("gr_cr_upazilla_id").innerHTML = xmlhttp.responseText;
              }else if (type=='C') {
                document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
              }

            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
        xmlhttp.send();
    }
    function getCourtTypeByCourtId(court_id){
      //  alert(country_id);
        document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?> --</option>";
        var district_id=  document.getElementById("district_id").value;
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("court_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
        xmlhttp.send();
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>order_sheet_report/index" method="post">
  <div class="form-row">

    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('case_no'); ?></label><span class="required_label">*</span>
      <select   name="case_master_id" id="case_master_id" class="js-example-basic-single w-100" required>
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($case_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($case_master_id)) {
           if ($row['id'] == $case_master_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>

  </div>
  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printPartOfPage()" value="<?php echo $this->lang->line('print').' '.$this->lang->line('result'); ?>"/>
    <!-- <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/> -->
    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('view_reort'); ?>">
  </div>
</form>

<?php
if (isset($idata)) {
	echo $report;
}
?>
