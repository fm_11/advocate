<div id="divPrintable" class="box-body" style="padding-left:0px; padding-right:0px">

	<?php echo $report_header; ?>

	<table id="tblCustomer" style="width: 100%; border-top: 5px solid <?php echo $this->session->userdata('table_header_row_color'); ?>; margin-top: 10px; font-size: 18px">

		<tr>
				<td  scope="col" colspan="3" class="text-center" style="text-align: center !important;">
					<?php if(count($idata)>0){

								if($idata[0]['is_petitioner']==1)
								{
										echo $this->lang->line('petitioner').' :- '.$idata[0]['advocate_name'].'<br>';
								}else{
									echo $this->lang->line('respondent').' :- '.$idata[0]['advocate_name'].'<br>';
								}
								echo $this->lang->line('mobile').' : '.$idata[0]['mobile'].'<br>';
								if($language=="english")  {
								echo $idata[$total_row]['court_name'].'<br>';

								}else{
								echo $idata[$total_row]['court_bn_name'].'<br>';
								}
								if($language=="english")  {
								echo $this->lang->line('case_no').' - '.$idata[$total_row]['case_no'].' '.$idata[$total_row]['case_sub_type_name'].'<br>';

								}else{
								echo $this->lang->line('case_no').' - '.$idata[$total_row]['case_no'].' '.$idata[$total_row]['case_sub_type_bname'].'<br>';
								}
								echo $this->lang->line('report').' - '.$title.'<br>';
					 }?>
				</td>
		</tr>
		<tr>
				<td  scope="col" class="text-center"><?php echo $this->lang->line('petitioner'); ?></td>
				<td  scope="col" class="text-center" rowspan="2"><?php echo $this->lang->line('vs'); ?></td>
				<td  scope="col" class="text-center"><?php echo $this->lang->line('respondent'); ?></td>
		</tr>
		<tr>
				<td  scope="col" class="text-center" >
					<?php
					if(count($idata)>0){
					if($idata[0]['is_petitioner']==1)
					{
							echo $idata[0]['client_name'];
					}else{
							echo $idata[0]['petision_name'];
					}
				}
				 ?>
			 </td>

				<td  scope="col" class="text-center">
					<?php
					if(count($idata)>0){
					if($idata[0]['is_petitioner']==1)
					{
							echo $idata[0]['petision_name'];
					}else{
							echo $idata[0]['client_name'];
					}
				}
				 ?>
			 </td>
		</tr>
	</table>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="tblItems" class="sortable-table-1" style="width: 100%; margin-top: 15px; font-size:13px">
    <thead>

    <tr style="border-bottom: 1px solid #333;background-color:<?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">
	    	<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('date'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('note'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr <?php if($i % 2 == 0){ echo 'style="background-color:' . $this->session->userdata('REPORT_ROW_COLOR'). ' !important;"'; } ?>>
						<td class="td_center"><?php echo date("d-m-Y", strtotime($row['befor_date']));?></td>
						<td class="td_center"><?php
						if($language=="english")  {
						echo $row['status_name'];
						}else{
						echo $row['status_bn_name'];
						}
						?>
						</td>
						<td class="td_center"><?php echo $row['remarks'];?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
</div>

<?php echo $report_footer;	?>
