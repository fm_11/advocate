<script type="text/javascript">


    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>client_user/updateMsgStatusMemberStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }
</script>


<?php
$name = $this->session->userdata('name');
$status = $this->session->userdata('status');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>cases/index">
<div class="col-md-offset-2 col-md-12">
        <label class="sr-only" for="Name"><?php echo $this->lang->line('name'); ?></label>
        <?php
         $placeholder = $this->lang->line('case_no');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($name)) {
            echo $name;
        } ?>" class="form-control" id="name">

        <label class="sr-only" for="Status"><?php echo $this->lang->line('status'); ?></label>
        <select name="status" style="max-width: 150px;"  class="form-control mb-1 mr-sm-1">
            <option value="">-- <?php echo $this->lang->line('status'); ?> --</option>
            <option value="1" <?php if (isset($status)) {
            if ($status == '1') {
                echo 'selected';
            }
        } ?>>Active
            </option>
            <option value="0" <?php if (isset($status) && $status != '') {
            if ($status == '0') {
                echo 'selected';
            }
        } ?>>Inactive
            </option>
        </select>
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped" style="width:100%;">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('case_no'); ?></th>
        <th><?php echo $this->lang->line('upazila'); ?></th>
        <th><?php echo $this->lang->line('client'); ?></th>
        <th><?php echo  $this->lang->line('mobile').' '.$this->lang->line('no');?></th>
        <th><?php echo $this->lang->line('court'); ?></th>
        <!-- <th><?php echo $this->lang->line('respondent'); ?></th> -->
        <th><?php echo $this->lang->line('next').' '.$this->lang->line('date'); ?></th>
        <th><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?></th>
        <th><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($cases as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td >
                <?php echo $i; ?>
            </td>
               <td>

                 <?php
                 if($language=="english")  {
                 echo $row['case_sub_type_name'].'-'.$row['case_no'];

                 }else{
                 echo $row['case_sub_type_bname'].'-'.$row['case_no'];
                 }
                 ?>
               </td>
               <td>

                 <?php
                 if($language=="english")  {
                 echo $row['upazila_name'];

                 }else{
                 echo $row['upazila_bnname'];
                 }
                 ?>
               </td>
               <td><?php echo $row['client_first_name'].'('.$row['total_person'].')'; ?>

               </td>
                   <td class="td_center"><?php echo $row['client_mobile'];?></td>
              <td>
                <?php
                if($language=="english")  {
                echo $row['court_name'];

                }else{
                echo $row['court_bn_name'];
                }
                ?>


              </td>
              <!-- <td>

                   <?php echo $row['petitionervs']; ?>
              </td> -->
              <td><?php echo $row['next_date']; ?></td>

              <td><?php
              if($language=="english")  {
              echo $row['status_name'];

              }else{
             echo $row['status_bn_name'];
              }
              ?></td>
              <td>
                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>case_closed/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('change').' '.$this->lang->line('action').'/'.$this->lang->line('order'); ?></a>

                    </div>
                </div>
              </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
