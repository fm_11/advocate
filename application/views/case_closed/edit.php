
<style>
.fieldset_div{
  border: 3px solid #efdfdf;
  padding: 20px;
}
.tr_border{
  border-top: 2px solid #fff;
}
</style>
<script>
function process(date){
var parts = date.split("-");
return new Date(parts[2], parts[1], parts[0]);
}
function getDistrictByCountryId(){
  var GivenDate = document.getElementById("last_bussines_on_date").value;
  var CurrentDate = document.getElementById("bussines_on_date").value;
  //alert(GivenDate);
  var date=GivenDate;
  GivenDate = process(GivenDate);
  CurrentDate = process(CurrentDate);

  if(GivenDate < CurrentDate){
      //alert('Next Date is greater than the current date.');
      return true;
  }else{
      alert('Next Date  is not smaller than '+date+' previous next date.');
      return false;
  }
}

</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>case_closed/edit/<?php echo $row_case_master->id;?>" class="cmxform" enctype="multipart/form-data" method="post"  onsubmit=" return getDistrictByCountryId();">
<input type="hidden" name="last_bussines_on_date" id="last_bussines_on_date" value="<?php echo date("d-m-Y", strtotime($last_cast_history->bussines_on_date)); ?>" />
  <input type="hidden" id="language" value="<?php echo $language; ?>"/>
    <input type="hidden" id="id" name="id" value="<?php echo $row_case_master->id; ?>"/>
    <input type="hidden" name="last_id" value="<?php echo $last_cast_history->id;?>" />
    <input type="hidden" name="case_no"  value="<?php echo $last_cast_history->case_no;?>">
    <input type="hidden" name="district_id" id="district_id" value="<?php echo $last_cast_history->district_id;?>" />
    <input type="hidden" name="upazilla_id" id="upazilla_id" value="<?php echo $last_cast_history->upazilla_id;?>" />
    <input type="hidden" name="case_sub_type_id" id="case_sub_type_id" value="<?php echo $last_cast_history->case_sub_type_id;?>" />
    <input type="hidden" name="parties_name" value="<?php echo $last_cast_history->parties_name;?>" />

  <h2><?php echo $this->lang->line('court_details'); ?></h2>
  <fieldset class="fieldset_div">

     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCaseSubTypeByCaseType(this.value)"  name="case_type_id" class="js-example-basic-single w-100" disabled>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->case_type_id)) {
              if ($row['id'] == $row_case_master->case_type_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <?php  $sub_type_name="";?>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_sub_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCRorGR(this)"   name="case_sub_type_id" id="case_sub_type_id" class="js-example-basic-single w-100" disabled>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_sub_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->case_sub_type_id)) {
              if ($row['id'] == $row_case_master->case_sub_type_id) {
              $sub_type_name=$row['name'];
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_assignment'); ?><span class="required_label">*</span></label>
         <select  name="case_status_id" id="case_status_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_status as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->case_status_id)) {
              if ($row['id'] == $row_case_master->case_status_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div id="cr_div">
       <div class="form-row" >
         <div class="form-group col-md-3">
           <label for="inputEmail4" id="case_number_label"><?php echo $sub_type_name.' '.$this->lang->line('numbers'); ?></label><span class="required_label">*</span>
           <input type="text" autocomplete="off"  class="form-control" id="case_no"  name="case_no" value="<?php echo $row_case_master->case_no;?>"  disabled>
         </div>
         <div class="form-group col-md-3">
           <label for="inputEmail4" id="case_date_label"><?php echo $sub_type_name.' '.$this->lang->line('date'); ?></label><span class="required_label">*</span>
           <div class="input-group date">
                   <input type="text" autocomplete="off"  name="registration_date" id="registration_date"  class="form-control" value="<?php echo date("d-m-Y", strtotime($row_case_master->registration_date)); ?>" disabled>
                   <span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
           </div>
         </div>
         <div class="form-group col-md-3">
           <label for="inputEmail4"><?php echo $this->lang->line('previous_hearing_date'); ?><span class="required_label">*</span></label>
            <input type="text" autocomplete="off" disabled class="form-control"   value="<?php echo date("d-m-Y", strtotime($last_cast_history->bussines_on_date)); ?>">
         </div>
         <div class="form-group col-md-3">
           <label for="inputEmail4"><?php echo $this->lang->line('first_hearing_date'); ?><span class="required_label">*</span></label>
           <div class="input-group date">
                   <input type="text" autocomplete="off"  name="bussines_on_date" id="bussines_on_date" required class="form-control" >
                   <span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
           </div>
         </div>

        </div>
     </div>



  </fieldset>

  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
