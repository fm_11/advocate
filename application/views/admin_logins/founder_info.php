<form name="updateForm" action="<?php echo base_url(); ?>admin_logins/update_founder_info" method="post" enctype="multipart/form-data">
<table>
    <tr>
        <td valign="top" scope="col">
            <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $founder_info->picture; ?>"
                 style="border-radius: 10px;" width="250px" height="240px" class="img"/>
        </td>
    </tr>
    <tr>
        <td valign="top"scope="col">
            <table ellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                <tbody>
                <tr>
                    <label>New Picture</label>
                    <input type="file" name="txtPhoto" class="smallInput">
                </tr>
                <tr>
                    <th  style="text-align: left;">
                        Name
                        <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $founder_info->id;  ?>">
                        <input type="text" autocomplete="off"  name="name"  class="smallInput wide" value="<?php echo $founder_info->name;  ?>">
                    </th>
                </tr>
				
				
				<tr>
                    <th  style="text-align: left;">
                        Years active              
                        <input type="text" autocomplete="off"  name="years_of_active"  class="smallInput wide" value="<?php echo $founder_info->years_of_active;  ?>">
                    </th>
                </tr>
				
				<tr>
                    <th  style="text-align: left;">
                        Born              
                        <input type="text" autocomplete="off"  name="born"  class="smallInput wide" value="<?php echo $founder_info->born;  ?>">
                    </th>
                </tr>

			
             
                <tr>
                    <th style="text-align: left;">
                        Address
                        <input type="text" autocomplete="off"  name="address"  class="smallInput wide" value="<?php echo $founder_info->address;  ?>">
                    </th>
                </tr>
               
                <tr>
                    <th  style="text-align: left;">
                        Educational Qualification
                        <input type="text" autocomplete="off"  name="edu_qua"  class="smallInput wide" value="<?php echo $founder_info->edu_qua;  ?>">
                    </th>
                </tr>
				
				<tr>
                    <th  style="text-align: left;">
                        Occupation
                        <input type="text" autocomplete="off"  name="occupation"  class="smallInput wide" value="<?php echo $founder_info->occupation;  ?>">
                    </th>
                </tr>
               
                <tr>
                    <th>
                        <input type="submit" class="submit" value="Update">
                    </th>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
</form>
