<?php
$controller = $this->uri->segment(1);
?>
<ul class="nav">
  <li class="nav-item  <?php if ($controller == 'dashboard') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>dashboard/index">
      <i class="ion fa fa-dashboard menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('dashboard'); ?></span>
    </a>
  </li>




  <li class="nav-item  <?php if ($controller == 'Clients') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>Clients/index">
      <i class="ion fa fa-list-alt menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('client_enty_from'); ?></span>
    </a>
  </li>

  <li class="nav-item  <?php if ($controller == 'Cases') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>Cases/index">
      <i class="ion fa fa-delicious menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('case_entry'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'multiple_clients_update') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>multiple_clients_update/index">
      <i class="ion ion-md-cube menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('multiple').' '.$this->lang->line('client').' '.$this->lang->line('update'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'case_closed') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>case_closed/index">
      <i class="ion ion-md-close menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('closed').' '.$this->lang->line('case').' '.$this->lang->line('list'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'history') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>history/index">
      <i class="ti-gallery menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('attendance').' '.$this->lang->line('entry'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'missed_attendance') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>missed_attendance/index">
      <i class="ion ion-md-aperture menu-icon"></i>
     <span class="menu-title"><?php echo $this->lang->line('missed').' '.$this->lang->line('attendance').' '.$this->lang->line('list'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'data_copy') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>data_copy/index">
      <i class="ion ion-md-repeat menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('court_allowcations'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'tasks') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>tasks/index">
      <i class="ti-home menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('task_entry'); ?></span>
    </a>
  </li>
  <li class="nav-item  <?php if ($controller == 'Appointments') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>Appointments/index">
      <i class="ion ion-md-apps menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('appointments'); ?></span>
    </a>
  </li>
  <li class="nav-item <?php if (in_array($controller, array('templates','phonebook_categories','voice_sms','phone_books','sms_sends'))) { ?>active<?php } ?>">
    <a class="nav-link" data-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
      <i class="ion ion-md-mail menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('sms'); ?></span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse <?php if (in_array($controller, array('templates','phonebook_categories','voice_sms','phone_books','sms_sends'))) { ?>show<?php } ?>" id="charts">
      <ul class="nav flex-column sub-menu">
        <p class="sidebar-title"><?php echo $this->lang->line('settings'); ?></p>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>templates/index#charts"><?php echo $this->lang->line('sms') . ' ' . $this->lang->line('template'); ?></a></li>
        <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>phonebook_categories/index#charts"><?php echo $this->lang->line('category'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>phone_books/index#charts"><?php echo $this->lang->line('phone_book'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>voice_sms/audio_message_upload#charts"><?php echo $this->lang->line('audio') . ' ' . $this->lang->line('upload'); ?></a></li> -->
        <p class="sidebar-title"><?php echo $this->lang->line('sms').' '.$this->lang->line('send'); ?></p>
		    <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>voice_sms/voice_sms_send_list#charts"><?php echo $this->lang->line('voice') . ' ' .$this->lang->line('sms') . ' ' . $this->lang->line('list'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>voice_sms/voice_sms_send#charts"><?php echo $this->lang->line('voice') . ' ' .$this->lang->line('sms') . ' ' . $this->lang->line('send'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/absent_sms_send#charts"><?php echo $this->lang->line('absent') . ' ' .$this->lang->line('sms') . ' ' . $this->lang->line('send'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/to_guardian_sms_send#charts"><?php echo $this->lang->line('guardian') . ' ' .$this->lang->line('sms') . ' ' . $this->lang->line('send'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/to_teacher_sms_send_form#charts"><?php echo $this->lang->line('teacher') . ' ' .$this->lang->line('sms') . ' ' . $this->lang->line('send'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/to_guardian_sms_send#charts"><?php echo $this->lang->line('admission') . ' ' .$this->lang->line('sms') . ' ' . $this->lang->line('send'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/to_result_notify#charts"><?php echo $this->lang->line('result') . ' ' .$this->lang->line('notify'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/send_from_phoneBook_list#charts"><?php echo $this->lang->line('phone_book'); ?></a></li> -->
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/attendance_sms"><?php echo $this->lang->line('attendance').' '.$this->lang->line('sms'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/to_excel_file#charts"><?php echo $this->lang->line('excel'); ?></a></li>
        <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_sends/SendBirthDaySMS#charts"><?php echo $this->lang->line('birthday'); ?></a></li> -->
        <p class="sidebar-title"><?php echo $this->lang->line('sms').' '.$this->lang->line('report'); ?></p>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>sms_send_reports/index#charts"><?php echo $this->lang->line('sms').' '.$this->lang->line('send').' '.$this->lang->line('report'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>recharge_histories/index#charts"><?php echo $this->lang->line('recharge_history'); ?></a></li>

	  </ul>
    </div>
  </li>
  <li class="nav-item <?php if (in_array($controller, array('Attendance_reports'))) { ?>active<?php } ?>">
    <a class="nav-link" data-toggle="collapse" href="#ui-report" aria-expanded="false" aria-controls="ui-report">
      <i class="fa fa-file-code-o menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('report'); ?></span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse <?php if (in_array($controller, array('Attendance_reports'))) { ?>show<?php } ?>" id="ui-report">
      <ul class="nav flex-column sub-menu">
		  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>attendance_reports/index#ui-report"><?php echo $this->lang->line('attendance_report'); ?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>case_reports/index#ui-report"><?php echo $this->lang->line('case').' '.$this->lang->line('report'); ?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>case_sub_type_report/index#ui-report"><?php echo $this->lang->line('case_sub_type').' '.$this->lang->line('report'); ?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>file_upload/index"><?php echo $this->lang->line('forms').' '.$this->lang->line('file_upload'); ?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>case_file_upload/index"><?php echo $this->lang->line('case_file_upload'); ?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>order_sheet_report/index"><?php echo $this->lang->line('order_sheet').' '.$this->lang->line('report');?></a></li>
      <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>User_roles/index#ui-advanced"><?php echo $this->lang->line('role'); ?></a></li> -->
	  </ul>
    </div>
  </li>
  <li class="nav-item <?php if (in_array($controller, array('Client_user','User_roles'))) { ?>active<?php } ?>">
    <a class="nav-link" data-toggle="collapse" href="#ui-advanced" aria-expanded="false" aria-controls="ui-advanced">
      <i class="fa fa-user-o menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('users_name').' '.$this->lang->line('setting'); ?></span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse <?php if (in_array($controller, array('Client_user','User_roles'))) { ?>show<?php } ?>" id="ui-advanced">
      <ul class="nav flex-column sub-menu">
		  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Client_user/index#ui-advanced"><?php echo $this->lang->line('team').' '.$this->lang->line('member'); ?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>email_setting/index"><?php echo $this->lang->line('email').' '.$this->lang->line('setting');?></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>User_roles/index#ui-advanced"><?php echo $this->lang->line('role'); ?></a></li>
	  </ul>
    </div>
  </li>

  	<!-- <li class="nav-item <?php if (in_array($controller, array('chart_of_accounts','asset_categories','liabilities_categories','income_categories','expense_categories','income_deposits','expense_details','fund_transfers'))) { ?>active<?php } ?>">
  		<a class="nav-link" data-toggle="collapse" href="#accounts" aria-expanded="false" aria-controls="accounts">
  			<i class="ion ion-md-map menu-icon"></i>
  			<span class="menu-title"><?php echo $this->lang->line('basic') . ' ' . $this->lang->line('accounts'); ?></span>
  			<i class="menu-arrow"></i>
  		</a>
  		<div class="collapse  <?php if (in_array($controller, array('chart_of_accounts','asset_categories','liabilities_categories','income_categories','expense_categories','income_deposits','expense_details','fund_transfers'))) { ?>show<?php } ?>" id="accounts">
  			<ul class="nav flex-column sub-menu">
  				<p class="sidebar-title"><?php echo $this->lang->line('settings'); ?></p>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>chart_of_accounts/index#accounts">Accounts Head</a></li>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>asset_categories/index#accounts">Asset Category</a></li>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>liabilities_categories/index#accounts">Liabilities Category</a></li>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>income_categories/index#accounts"><?php echo $this->lang->line('income') . ' ' . $this->lang->line('category'); ?></a></li>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>expense_categories/index#accounts"><?php echo $this->lang->line('expense') . ' ' . $this->lang->line('category'); ?></a></li>
  				<p class="sidebar-title"><?php echo $this->lang->line('process'); ?></p>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>income_deposits/index#accounts"><?php echo $this->lang->line('income') . ' ' . $this->lang->line('add'); ?></a></li>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>expense_details/index#accounts"><?php echo $this->lang->line('expense') . ' ' . $this->lang->line('add'); ?></a></li>
  				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>fund_transfers/index#accounts"><?php echo $this->lang->line('fund') . ' ' . $this->lang->line('transfer'); ?></a></li>
  			</ul>
  		</div>
  	</li> -->
  <!-- <li class="nav-item  <?php if ($controller == 'Vendor') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>Vendor/index">
      <i class="ion ion ion-ios-archive menu-icon"></i>
    <span class="menu-title"><?php echo $this->lang->line('vendor'); ?></span>
    </a>
  </li> -->
	<li class="nav-item <?php if (in_array($controller, array('ad_case_types','ad_court_types','ad_courts','ad_case_status','judge','Tax'))) { ?>active<?php } ?>">
		<a class="nav-link" data-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
			<i class="ion ion-md-settings menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('setting'); ?></span>
			<i class="menu-arrow"></i>
		</a>
		<div class="collapse <?php if (in_array($controller, array('ad_case_types','ad_court_sub_types','ad_court_types','ad_courts','ad_case_status','judge','Tax','dashboard_color_settings'))) { ?>show<?php } ?>" id="tables">
			<ul class="nav flex-column sub-menu">
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>dashboard_color_settings/index"><?php echo $this->lang->line('dashboard').' '.$this->lang->line('color').' '.$this->lang->line('settings'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>institute_setup/index"><?php echo $this->lang->line('office').' '.$this->lang->line('settings'); ?></a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>advocate_setting/index"><?php echo $this->lang->line('advocate').' '.$this->lang->line('settings'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_court_types/index"><?php echo $this->lang->line('court').' '.$this->lang->line('type'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_courts/index"><?php echo $this->lang->line('court').' '.$this->lang->line('sub').' '.$this->lang->line('type'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>judge/index"><?php echo $this->lang->line('judge').' '.$this->lang->line('type'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_case_types/index"><?php echo $this->lang->line('case').' '.$this->lang->line('type'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_case_sub_types/index"><?php echo $this->lang->line('case').' '.$this->lang->line('sub').' '.$this->lang->line('type'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_case_status/index"><?php echo $this->lang->line('reason_for_assignment'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>form_types/index"><?php echo $this->lang->line('form_type'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>file_colours/index"><?php echo $this->lang->line('file_colour'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>divisions/index"><?php echo $this->lang->line('division'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>districts/index"><?php echo $this->lang->line('district'); ?></a></li>
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>upazilas/index"><?php echo $this->lang->line('upazila'); ?></a></li>

      </ul>
		</div>
	</li>



</ul>
