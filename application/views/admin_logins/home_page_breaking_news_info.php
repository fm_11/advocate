
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="100%" scope="col" colspan="3">
            <u><span style="font-size: 20px;"><?php echo $title; ?></span>
                &nbsp;<a href="<?php echo base_url(); ?>admin_logins/update_home_page_breaking_news_info/<?php echo $breaking_news_info->id; ?>">Update Info.</a></u>
        </th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <th width="15%" style="text-align: right;"><b>Breaking News</b></th>
        <th width="5%" style="text-align: center;"><b>:</b></th>
        <th width="80%" style="text-align: left;"><?php echo $breaking_news_info->breaking_news; ?></th>
    </tr>
    </tbody>
</table>