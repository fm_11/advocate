<?php
$controller = $this->uri->segment(1);
?>
<div class="container">
	<ul class="nav page-navigation">
		<li class="nav-item <?php if ($controller == 'dashboard') { ?>active<?php } ?>">
			<a class="nav-link" title="<?php echo $this->lang->line('dashboard'); ?>" href="<?php echo base_url(); ?>dashboard/index">
				<i class="ion ion ion-ios-archive menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('dashboard'); ?></span>
			</a>
		</li>
		<li class="nav-item  <?php if ($controller == 'Clients') { ?>active<?php } ?>">
	    <a class="nav-link" href="<?php echo base_url(); ?>Clients/index">
	      <i class="ion ion ion-ios-archive menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('client'); ?></span>
	    </a>
	  </li>
		<li class="nav-item  <?php if ($controller == 'Cases') { ?>active<?php } ?>">
			<a class="nav-link" href="<?php echo base_url(); ?>Cases/index">
				<i class="ion ion ion-ios-archive menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('case'); ?></span>
			</a>
		</li>
		<li class="nav-item  <?php if ($controller == 'task') { ?>active<?php } ?>">
			<a class="nav-link" href="<?php echo base_url(); ?>tasks/index">
				<i class="ion ion ion-ios-archive menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('task'); ?></span>
			</a>
		</li>
		<li class="nav-item  <?php if ($controller == 'Appointments') { ?>active<?php } ?>">
			<a class="nav-link" href="<?php echo base_url(); ?>Appointments/index">
				<i class="ion ion ion-ios-archive menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('appointments'); ?></span>
			</a>
		</li>

		<li class="nav-item <?php if (in_array($controller, array('Client_user','User_roles'))) { ?>active<?php } ?>">
			<a href="#" class="nav-link">
				<i class="ion ion-logo-codepen menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('team').' '.$this->lang->line('member'); ?></span>
				<i class="menu-arrow"></i></a>
					<div class="submenu">
						<ul class="submenu-item">
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Client_user/index#ui-advanced"><?php echo $this->lang->line('team').' '.$this->lang->line('member'); ?></a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>User_roles/index#ui-advanced"><?php echo $this->lang->line('role'); ?></a></li>
					 </ul>
				</div>
		</li>

		<li class="nav-item  <?php if ($controller == 'Vendor') { ?>active<?php } ?>">
	    <a class="nav-link" href="<?php echo base_url(); ?>Vendor/index">
	      <i class="ion ion ion-ios-archive menu-icon"></i>
	    <span class="menu-title"><?php echo $this->lang->line('vendor'); ?></span>
	    </a>
	  </li>

		<li class="nav-item <?php if (in_array($controller, array('ad_case_types','ad_court_types','ad_courts','ad_case_status','judge','Tax'))) { ?>active<?php } ?>">
			<a href="#" class="nav-link">
				<i class="ion ion-logo-codepen menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('setting'); ?></span>
				<i class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_case_types/index"><?php echo $this->lang->line('case').' '.$this->lang->line('type'); ?></a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_court_types/index"><?php echo $this->lang->line('court').' '.$this->lang->line('type'); ?></a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_courts/index"><?php echo $this->lang->line('court'); ?></a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_case_status/index"><?php echo $this->lang->line('case').' '.$this->lang->line('status'); ?></a></li>
		        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>judge/index"><?php echo $this->lang->line('judge'); ?></a></li>
		        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Tax/index"><?php echo $this->lang->line(''); ?></a></li>
		        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ad_case_status/index"><?php echo $this->lang->line('general').' '.$this->lang->line('setting'); ?></a></li>
           </ul>
			</div>
		</li>
	</ul>
</div>
