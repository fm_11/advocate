<div class="uk-width-large-2-4 midsection">
    <h3><?php echo $home_latest_news[0]['title']; ?></h3>
    <p align="justify">
        <?php echo $home_latest_news[0]['breaking_news']; ?>
    </p>

    <h3><?php echo $head_message[0]['title']; ?></h3>
    <p class="uk-clearfix chairman" align="justify">
        <img class="uk-align-medium-left" src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $head_message[0]['location']; ?>" alt="Image aligned to the right">
        <?php echo $head_message[0]['long_message']; ?>
        <a href="<?php echo base_url(); ?>homes/getHeadteacherMessage"><em><strong>read
                    More...</strong></em></a> <br/>
    </p>
</div>