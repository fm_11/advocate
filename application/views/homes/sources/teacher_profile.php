<div class="uk-width-large-2-4 midsection">
    <h3>Teacher Profile</h3>
    <hr>

    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <tbody>
            <tr>
                <td colspan="3" align="center">
                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/teacher/<?php echo $teacher_info[0]['photo_location']; ?>"
                            height="200" width="200" style=" border:1px solid #666666; border-radius:100px;"></td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <b><?php echo $teacher_info[0]['name']; ?></b>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <b><?php echo $teacher_info[0]['post_name']; ?></b>
                </td>
            </tr>
            <tr>
                <td align="right">Teacher Index No.</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['teacher_index_no']; ?> </td>
            </tr>
            <tr>
                <td align="right">Section</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['section_name']; ?> </td>
            </tr>
            <tr>
                <td align="right">Date of Joining</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['date_of_join']; ?> </td>
            </tr>
            <tr>
                <td align="right">Subject</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['subject']; ?> </td>
            </tr>
            <tr>
                <td align="right">Date of Birth</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['date_of_birth']; ?> </td>
            </tr>
            <tr>
                <td align="right">Gender</td>
                <td> :</td>
                <td>
                    <?php
                    if ($teacher_info[0]['gender'] == 'M') {
                        echo 'Male';
                    } else if ($teacher_info[0]['gender'] == 'F') {
                        echo 'Female';
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td align="right">Religion</td>
                <td> :</td>
                <td>
                    <?php
                    echo $teacher_info[0]['religion_name'];
                    ?>
                </td>
            </tr>
            <tr>
                <td align="right">Blood Group</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['blood_group']; ?> </td>
            </tr>
            <tr>
                <td align="right">National ID NO.</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['national_id']; ?> </td>
            </tr>
            <tr>
                <td align="right">Father's Name</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['fathers_name']; ?> </td>
            </tr>
            <tr>
                <td align="right">Mother's Name</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['mothers_name']; ?> </td>
            </tr>
            <tr>
                <td align="right">Present Address</td>
                <td align="center"> :</td>
                <td><?php echo $teacher_info[0]['present_address']; ?> </td>
            </tr>
            <tr>
                <td align="right">Permanent Address</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['permanent_address']; ?></td>
            </tr>
            <tr>
                <td align="right">Mobile</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['mobile']; ?></td>
            </tr>
            <tr>
                <td align="right">Email</td>
                <td> :</td>
                <td><?php echo $teacher_info[0]['email']; ?></td>
            </tr>

            <tr>
                <td align="right">Teacher Status</td>
                <td> :</td>
                <td>
                    <?php
                    if ($teacher_info[0]['is_permanent'] == 'P') {
                        echo 'Permanent';
                    } else if ($teacher_info[0]['is_permanent'] == 'PT') {
                        echo 'Part Time';
                    }
                    ?>
                </td>
            </tr>


            </tbody>
        </table>
    </div>
</div>
