<div class="uk-container uk-container-center navmenu">
    <ul class="uk-navbar-nav uk-hidden-small">
        <li class="menutext"><a href="<?php echo  base_url(); ?>homes/index">Home</a></li>
        <li class="uk-parent menutext" data-uk-dropdown>
            <a href="#">Academic <strong><i class="uk-icon-angle-down"></i></strong></a>
            <div class="uk-dropdown uk-dropdown-navbar menuitem">
                <ul class="uk-nav uk-nav-navbar">
                    <li><a href="<?php echo  base_url(); ?>homes/getTeacherInfo">Teacher Information</a></li>
                    <li><a href="<?php echo  base_url(); ?>homes/getMcMemberInfo">Managing Committee</a></li>
                    <li><a href="<?php echo  base_url(); ?>contents/getStudentInformation">Student Information</a></li>
                    <li><a href="<?php echo  base_url(); ?>homes/getStaffInfo">Staff Information</a></li>
                    <li><a href="<?php echo  base_url(); ?>contents/getAllHeadTeacherInfo">All Headmaster's</a></li>
                    <li><a href="<?php echo  base_url(); ?>contents/getAllSuccessStudentsInfo">Success Student's</a></li>
                </ul>
            </div>
        </li>
        <li class="menutext"><a href="<?php echo  base_url(); ?>homes/getAllNoticeInfo">Notice</a></li>
        <li class="uk-parent menutext" data-uk-dropdown>
            <a href="#">Result <strong><i class="uk-icon-angle-down"></i></strong></a>
            <div class="uk-dropdown uk-dropdown-navbar menuitem">
                <ul class="uk-nav uk-nav-navbar">
                    <!--<li>
                        <a href="<?php echo  base_url(); ?>contents/getResult2017">Result 2017</a>
                    </li>!-->
                    <li>
                        <a href="<?php echo  base_url(); ?>contents/getResult">Result Archive</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url(); ?>homes/getAllUploadedResultInfo">Result Download</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="uk-parent menutext" data-uk-dropdown>
            <a href="#">Admission <strong><i class="uk-icon-angle-down"></i></strong></a>
            <div class="uk-dropdown uk-dropdown-navbar menuitem">
                <ul class="uk-nav uk-nav-navbar">
                    <li>
                        <a href="<?php echo  base_url(); ?>contents/ApplyOnline" target="_blank">Apply Online</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url(); ?>contents/getAdmitCard" target="_blank">Admit Card</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url(); ?>contents/getAdmissionProcessInfo">Admission Process</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url(); ?>contents/getAdmissionForm">Admission Form Download</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url(); ?>contents/getAdmissionContact">Admission Contacts</a>
                    </li>
                </ul>
            </div>

        </li>
        <li class="uk-parent menutext" data-uk-dropdown>
            <a href="#">About Us <strong><i class="uk-icon-angle-down"></i></strong></a>
            <div class="uk-dropdown uk-dropdown-navbar menuitem">
                <ul class="uk-nav uk-nav-navbar">
                    <li>
                        <a href="<?php echo  base_url(); ?>homes/getAboutInfo">About</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url(); ?>homes/getHistoryInfo">History</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="menutext"><a href="<?php echo  base_url(); ?>homes/getContactInfo">Contact</a></li>
        <li class="dashbord"><a href="<?php echo  base_url(); ?>homes/getDashboard">Dashboard</a></li>
    </ul>
    <a href="#offcanvas-1" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
</div>