<script type="text/javascript">
    $(function () {
        $('#graph_container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 1, //null,
                plotShadow: false
            },
            title: {
                text: 'Teacher Information Graph <br>( <?php echo date('l, F dS, Y', strtotime(date('Y-m-d'))); ?> )'
            },

            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#000000'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Teacher',
                data: [
                    {
                        name: 'Present',
                        y: <?php echo $today_present_teacher; ?>,
                        sliced: false,
                        backgroundColor: '#FF0000',
                        selected: false,
                        color: '#338533'
                    },
                    {
                        name: 'Leave',
                        y: <?php echo $today_leave_teacher; ?>,
                        sliced: false,
                        backgroundColor: '#FF0000',
                        selected: false,
                        color: '#336699'
                    },
                    {
                        name: 'Absent',
                        y: <?php echo ($today_absent_teacher); ?>,
                        sliced: false,
                        backgroundColor: '#FF0000',
                        selected: false,
                        color: '#BA1919'
                    }
                ]
            }]
        });
    });


</script>

<script type="text/javascript">
    $(function () {
        $('#student_graph_container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 1, //null,
                plotShadow: false
            },
            title: {
                text: 'Student Information Graph <br>( <?php echo date('l, F dS, Y', strtotime(date('Y-m-d'))); ?> )'
            },

            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#000000'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Student',
                data: [
                    {
                        name: 'Present',
                        y: <?php echo $student_present; ?>,
                        sliced: false,
                        backgroundColor: '#FF0000',
                        selected: false,
                        color: '#338533'
                    },
                    {
                        name: 'Leave',
                        y: <?php echo $student_leave; ?>,
                        sliced: false,
                        backgroundColor: '#FF0000',
                        selected: false,
                        color: '#336699'
                    },
                    {
                        name: 'Absent',
                        y: <?php echo $student_absent; ?>,
                        sliced: false,
                        backgroundColor: '#FF0000',
                        selected: false,
                        color: '#BA1919'
                    }
                ]
            }]
        });
    });


</script>
<?php
$color = array();
$color[0] = "#194719";
$color[1] = "#338533";
$color[2] = "#336699";
$color[3] = "#BA1919";
$color[4] = "#80B2FF";
$color[5] = "#A34719";
$color[6] = "#7519FF";
$color[7] = "#666699";
$color[8] = "#009900";
$color[9] = "#194719";
$color[10] = "#338533";
$color[11] = "#336699";
$color[12] = "#BA1919";
$color[13] = "#BA1919";
$color[14] = "#80B2FF";
$color[15] = "#A34719";


$taotal_class = count($students);
$i = 0;
$str = "";
$catagory = "";
$col_str = "";
while($i < $taotal_class){
	$str .= "['".$students[$i]['class_name']."',". $students[$i]['total_student']."]";
	$catagory .= "'".$students[$i]['class_name']."'";
	$col_str .= "{y:".$students[$i]['total_student'].", color: '".$color[$i]."'}";
	$i++;
	if($i != $taotal_class){
		$str .= ",";
		$catagory .= ",";
		$col_str .= ",";
	}	
}

?>
<script type="text/javascript">
    function pieChart() {$(function() {
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 1, //null,
                plotShadow: false
            },
            title: {
                text: 'Student Information Graph'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                    type: 'pie',
                    name: 'Student Information',
                    data: [
                        <?php echo $str; ?>
                    ]
                }]
        });
    });
    }

    function columnChart() {
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: 'Student Information Graph'
                },
                xAxis: {
                    categories: [
                        <?php echo $catagory; ?>
                    ],
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Student Info.( Number )'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Student : <b>{point.y:.0f}</b>',
                },
                series: [{
                        name: 'Student Information',
                        data: [<?php echo $col_str; ?>],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            x: 4,
                            y: 10,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 3px black'
                            }
                        }
                    }]
            });
        });
    }

    function graphOpen() {
        pieChart();
        var type = 1;
        setInterval(function () {
            if (type % 2 != 0) {            
                columnChart();
                type = type + 1;
            }
            else {
                pieChart();
                type = type + 1;
            }
        }, 8000);
    }
    window.onload = graphOpen;
</script>

<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/highcharts/js/modules/exporting.js"></script>
<div class="uk-width-large-2-4 midsection">
    <h3>Dashboard</h3>
<div id="container" style="min-width: 310px; height: 400px; max-width: 430px; margin: 0 auto"></div>
<div id="graph_container" style="min-width: 310px; height: 400px; max-width: 430px; margin: 0 auto"></div>
<div id="student_graph_container" style="min-width: 310px; height: 400px; max-width: 430px; margin: 0 auto"></div>
<table width="82%" style="margin: 0px auto; border: 1px black solid;" cellpadding="0" cellspacing="0" id="box-table-a"
       summary="Employee Pay Sheet">
    <tr>
        <th colspan="4"><font size="3+" style="color: red;">Today Teacher Status</font></th>
    </tr>
	<tr>
	   <th style="color: #000000;">Total Teacher</th>
	   <th style="color: #000000;">Present</th>
	   <th style="color: #000000;">Leave</th>
	   <th style="color: #000000;">Absent</th>
	</tr>
	<tr>
	   <th style="color: #000000;"><?php echo $total_teacher; ?></th>
	   <th style="color: #000000;"><?php echo $today_present_teacher; ?></th>
	   <th style="color: #000000;"><?php echo $today_leave_teacher; ?></th>
	   <th style="color: #000000;"><?php echo $today_absent_teacher; ?></th>
	</tr>
  
    <tr>
        <th colspan="4"><font size="3+" style="color: red;">Today Student Status</font></th>
    </tr>
	<tr>
	   <th style="color: #000000;">Total Student</th>
	   <th style="color: #000000;">Present</th>
	   <th style="color: #000000;">Leave</th>
	   <th style="color: #000000;">Absent</th>
	</tr>
	<tr>
	   <th style="color: #000000;"><?php echo $total_student; ?></th>
	   <th style="color: #000000;"><?php echo $student_present; ?></th>
	   <th style="color: #000000;"><?php echo $student_leave; ?></th>
	   <th style="color: #000000;"><?php echo $student_absent; ?></th>
	</tr>
</table>
</div>
