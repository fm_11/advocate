<div class="uk-width-large-2-4 midsection">
    <div class="uk-width-medium-1-1 uk-row-first">
        <div class="uk-panel uk-panel-header">
            <h3 class="uk-panel-title">CONTACT US</h3>
            <hr>
            <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
                <tr>
                    <td colspan="2">
                        <b>
                            Our Address
                        </b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Institute Name:
                    </td>
                    <td>
                        <b><?php echo $contact[0]['school_name']; ?></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Address:
                    </td>
                    <td>
                        <b><?php echo $contact[0]['address']; ?></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Mobile:
                    </td>
                    <td>
                        <b><?php echo $contact[0]['mobile']; ?></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email:
                    </td>
                    <td>
                        <b><?php echo $contact[0]['email']; ?></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Website:
                    </td>
                    <td>
                        <b> <?php echo $contact[0]['web_address']; ?></b>
                    </td>
                </tr>
            </table>
            <form action="<?php echo base_url(); ?>homes/getContactInfo" method="post" class="uk-form uk-form-stacked">

                <div class="uk-form-row">
                    <?php
                    $message = $this->session->userdata('message');
                    if ($message != '') {
                        ?>
                        <b>
                            <u>
                                <?php
                                echo $message;
                                $this->session->unset_userdata('message');
                                ?>
                            </u>
                        </b>
                        <?php
                    }
                    ?>
                    </b>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label">Your Name</label>
                    <div class="uk-form-controls">
                        <input type="text" autocomplete="off"  placeholder="ex: Mr. Name" required="1" name="name"
                               class="uk-width-1-1">
                    </div>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label">Your Email</label>
                    <div class="uk-form-controls">
                        <input type="text" autocomplete="off"  placeholder="ex: s@yahoo.com" required="1" name="email" class="uk-width-1-1">
                    </div>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label">Your Message</label>
                    <div class="uk-form-controls">
                        <textarea class="uk-width-1-1" name="message" required="1" id="form-h-t" cols="100"
                                  rows="9"></textarea>
                    </div>
                </div>

                <div class="uk-form-row">
                    <div class="uk-form-controls">
                        <input type="submit" name="Submit" class="uk-button uk-button-primary" value="Submit"/>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>










