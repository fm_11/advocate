<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['online']; ?> Online </a>
</li>
<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['day_value']; ?> Today</a>
</li>
<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['yesterday_value']; ?>
        Yesterday</a></li>
<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['week_value']; ?> Week</a>
</li>
<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['month_value']; ?> Month</a>
</li>
<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['year_value']; ?> Year</a>
</li>
<li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['all_value']; ?> Total</a>
</li>
<li><a href="#"><i class="uk-icon-list"></i> Record: <?php echo $counter['record_value']; ?>
        (<?php echo date("d.m.Y", strtotime($counter['record_date'])) ?>)</a></li>