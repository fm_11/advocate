<div class="uk-width-large-2-4 midsection">
    <h3>Teacher Information</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Profile</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($teachers as $row):
            $i++;
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['post_name']; ?></td>
                <td><a href="<?php echo base_url(); ?>homes/getTeacherInfoByTeacherID/<?php echo $row['id']; ?>"><i class="uk-icon-small uk-icon-eye"></i></a></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>