<?php
foreach ($digital_contents as $row):
    ?>
    <li><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/digital_content/<?php echo $row['location']; ?>" target="_blank"><i
                    class="uk-icon-arrow-circle-right"></i> <?php echo $row['title']; ?> (<?php echo $row['date']; ?>)</a></li>
<?php endforeach; ?>
<a href="<?php echo base_url(); ?>contents/getAllDigitalContent">&nbsp;All Digital Contents...</a>