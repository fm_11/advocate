<div class="uk-width-large-2-4 midsection">
    <h3>Managing Committee</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Post</th>
                <th>Mobile</th>
                <th>Photo</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($mc_members as $row):
                $i++;
                ?>
                <tr>
                    <td style="vertical-align: middle;"><?php echo $i; ?></td>
                    <td style="vertical-align: middle;"><?php echo $row['name']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $row['post']; ?></td>
                    <td style="vertical-align: middle;"><?php echo $row['mobile']; ?></td>
                    <td>
                        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/managing_committee/<?php echo $row['photo_location']; ?>"
                                style=" border:1px solid #666666; border-radius:10px;">
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>