<div class="uk-width-large-2-4 midsection">
    <h3>All Result</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Result Type</th>
                <th>Date</th>
                <th>Notice</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($results as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['result_type']; ?></td>
                    <td><?php echo $row['date']; ?></td>
                    <td><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/result/<?php echo $row['location']; ?>"><i class="uk-icon-small uk-icon-eye"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>