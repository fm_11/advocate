
<div class="uk-width-large-2-4 midsection">
    <h3>Staff Information</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Index No.</th>
                <th>Post</th>
                <th>Section</th>
                <th>Mobile</th>
                <th>Photo</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($staff as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['staff_index_no']; ?></td>
                    <td><?php echo $row['post_name']; ?></td>
                    <td><?php echo $row['section_name']; ?></td>
                    <td><?php echo $row['mobile']; ?></td>
                    <td>
                        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/staff/<?php echo $row['photo_location']; ?>"
                             style=" border:1px solid #666666; border-radius:10px;">
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>