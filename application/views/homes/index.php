<!--Header section  Start -->

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
    
    <link rel="icon" href="<?php echo base_url(); ?>core_media/css/login_css/school360-favicon.png" type="image/png"
          sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/css/uikit.docs.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/highlight.css">
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/vendor/jquery.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/uikit.min.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/docs.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/slideshow.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/slideshow-fx.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/form-password.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/highlight.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/src/accordion.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/slider.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/vendor/scrolltop.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/parallax.js"></script>
    <script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/slideset.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/css/animate.css">

</head>
<body>


<div class="headsection">
    <div class="uk-container uk-container-center">
        <img class="uk-align-center" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/header_banner_logo.png"/>
        <nav class="uk-navbar uk-navbar-attached navsection">
            <?php
            echo $main_menu;
            ?>
        </nav>
    </div>
</div>


<div id="offcanvas-1" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
        <?php
        echo $main_menu_for_mobile;
        ?>
    </div>
</div>


<!--Header section  End -->

<div class="uk-container uk-container-large uk-container-center headimg">
    <div class="mainbody">
        <div class="uk-margin-small-bottom uk-margin-small-top">

            <!--slide section  Start -->

            <?php
            if ($is_open_slide == 1) {
                echo $slide;
            }
            ?>


            <div class="uk-grid uk-grid-margin uk-grid-collapse" data-uk-grid-margin="">
                <div class="uk-width-large-1-4 notice">
                    <ul class="uk-list uk-align-center">
                        <li><a href="#"><i class="uk-icon uk-icon-cog"></i> Top News</a></li>
                    </ul>
                </div>
                <div class="uk-width-large-3-4 notice">
                    <p align="left">
                        <marquee scrollamount="5" onMouseOver="this.setAttribute('scrollamount', 0, 0);this.stop();"
                                 OnMouseOut="this.setAttribute('scrollamount', 2, 0);this.start();">
                            <?php
                            foreach ($home_page_news as $row):
                                ?>
                                <span style="color: #ff0000;">*</span>
                                <span
                                    style="color:<?php echo $row['text_color']; ?>; font-weight: bold;"><?php echo $row['news']; ?></span>
                                &nbsp;&nbsp;
                            <?php endforeach; ?>
                        </marquee>

                    </p>
                </div>
            </div>
            <!--slide section  end -->
        </div>

        <hr/>

        <div class="uk-grid uk-grid-medium uk-margin-small-bottom" data-uk-grid-margin>

            <!--<!--left section  Start-->
            <div class="uk-width-large-1-4">

                <div class="uk-panel uk-panel-box quicklink">
                    <p align="center">Quick Links</p>
                    <ul class="uk-list">
                        <li><a href="<?php echo base_url(); ?>homes/index"><i class="uk-icon-arrow-circle-right"></i>
                                Home</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getSyllabus"><i
                                    class="uk-icon-arrow-circle-right"></i> Student Syllabus</a></li>
                        <li><a href="<?php echo base_url(); ?>homes/getTeacherTimekeepingInfo"><i
                                    class="uk-icon-arrow-circle-right"></i> Teacher Timekeeping</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getStudentTimeKeepingInformation"><i
                                    class="uk-icon-arrow-circle-right"></i> StudentTimekeeping</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getStaffTimeKeepingInformation"><i
                                    class="uk-icon-arrow-circle-right"></i> StaffTimekeeping</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getTeacherPostStatusInfo"><i
                                    class="uk-icon-arrow-circle-right"></i> Academic Post Status</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getClassRoutineInfo"><i
                                    class="uk-icon-arrow-circle-right"></i> Student Class Routine</a></li>
                        <li><a href="<?php echo base_url(); ?>homes/getAllNoticeInfo"><i
                                    class="uk-icon-arrow-circle-right"></i> Notice Board</a></li>
                        <li><a href="<?php echo base_url(); ?>homes/getRulesAndRegulation"><i
                                    class="uk-icon-arrow-circle-right"></i> Rules and Regulations</a></li>
                        <li><a href="<?php echo base_url(); ?>homes/getHistoryInfo"><i
                                    class="uk-icon-arrow-circle-right"></i> History</a></li>
                        <li><a href="<?php echo base_url(); ?>homes/getPhotoGallery"><i
                                    class="uk-icon-arrow-circle-right"></i> Photo Gallery</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getAcademicHoliday"><i
                                    class="uk-icon-arrow-circle-right"></i> Academic Holiday</a></li>
                        <li><a href="<?php echo base_url(); ?>contents/getAllDigitalContent"><i
                                    class="uk-icon-arrow-circle-right"></i> Digital Contents</a></li>
                        <li><a href="<?php echo base_url(); ?>homes/getContactInfo"><i
                                    class="uk-icon-arrow-circle-right"></i> Contact</a></li>
                    </ul>
                </div>

                <div class="uk-panel uk-panel-box quicklink">
                    <p align="center">Important Linkns</p>
                    <ul class="uk-list">
                        <li><a href="http://www.moedu.gov.bd/" target="_blank"><i
                                    class="uk-icon-arrow-circle-right"></i> Ministry of Education </a></li>
                        <li><a href="http://www.dshe.gov.bd/" target="_blank"><i class="uk-icon-arrow-circle-right"></i>
                                Director of Secondary Education</a></li>
                        <li><a href="http://www.dhakaeducationboard.gov.bd/" target="_blank"><i
                                    class="uk-icon-arrow-circle-right"></i> Dhaka Education Board</a></li>
                        <li><a href="http://www.emis.gov.bd/" target="_blank"><i class="uk-icon-arrow-circle-right"></i>
                                EMIS</a></li>
                        <li><a href="http://www.banbeis.gov.bd/" target="_blank"><i
                                    class="uk-icon-arrow-circle-right"></i> BANBEIS</a></li>
                        <li><a href="ntrca.teletalk.com.bd" target="_blank"><i class="uk-icon-arrow-circle-right"></i>
                                NTRCA</a></li>
                        <li><a href="http://www.nctb.gov.bd/" target="_blank"><i class="uk-icon-arrow-circle-right"></i>
                                NCTB</a></li>
                        <li><a href="http://www.tqi2.gov.bd/" target="_blank"><i class="uk-icon-arrow-circle-right"></i>
                                TQI</a></li>
                        <li><a href="http://www.ictd.gov.bd/" target="_blank"><i class="uk-icon-arrow-circle-right"></i>
                                ICT Division</a></li>
                        <li><a href="http://mmc.e-service.gov.bd/" target="_blank"><i
                                    class="uk-icon-arrow-circle-right"></i> Multimedia Class Room</a></li>
                        <li><a href="http://www.jamalpur.gov.bd/" target="_blank"><i
                                    class="uk-icon-arrow-circle-right"></i> Jamalpur District</a></li>
                    </ul>
                </div>


                <div class="buttn uk-margin-small-top"><a href="<?php echo base_url(); ?>login/student_guardian_login">
                        <i class="uk-icon-user"></i>&nbsp
                        Student Login</a>
                </div>

                <div class="buttn uk-margin-small-top"><a href="javascrip:void"><i class="uk-icon-tasks"></i>&nbsp;&nbsp;
                        EIIN No:
                        <?php
                        echo $eiin_number[0]['eiin_number'];
                        ?> </a>
                </div>
                <div class="buttn uk-margin-small-top"><a href="<?php echo base_url(); ?>contents/getResult"><i
                            class="uk-icon-qrcode"></i>&nbsp;&nbsp;
                        Result Archive</a></div>
                <div class="buttn uk-margin-small-top"><a href="<?php echo $contact_info[0]['web_address']; ?>:2095/"
                                                          target="_blank"><i
                            class="uk-icon-envelope-o"></i>&nbsp;&nbsp;
                        Webmail Login </a></div>
                <div class="buttn uk-margin-small-top"><a href="<?php echo base_url(); ?>contents/ApplyOnline"><i
                            class="uk-icon-bar-chart"></i>&nbsp;&nbsp;
                        Apply Online </a></div>


            </div>
            <!--left section complete-->

            <!--middle  Start-->
            <?php
            echo $main_content;
            ?>
            <!--middle complete-->

            <!--3rd section start-->
            <div class="uk-width-large-1-4">

                <div class="uk-panel uk-panel-box midsection1">
                    <h3 align="center"><?php echo $president_message[0]['title']; ?></h3>
                    <div class="mblock">
                        <h4 align="center"><img
                                src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $president_message[0]['location']; ?>"/>
                        </h4>
                        <p align="justify"><?php echo $president_message[0]['short_message']; ?>
                            <a href="<?php echo base_url(); ?>homes/getPresidentMessage"><em><strong>read
                                        More...</strong></em></a>
                        </p>
                    </div>
                </div>

                <?php if ($is_open_slide != 1) { ?>
                    <div class="uk-panel uk-panel-box quicklink">
                        <p align="center">Notice</p>
                        <ul class="uk-list">
                            <?php
                            foreach ($home_page_notice as $row):
                                ?>
                                <li><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>"
                                       target="_blank"><i
                                            class="uk-icon-arrow-circle-right"></i> <?php echo $row['title']; ?>
                                        <br><i
                                            class="uk-icon-calendar"></i> <?php echo date("d-m-Y", strtotime($row['date'])); ?>
                                    </a></li>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                <?php } ?>


                <div class="uk-panel uk-panel-box midsection1">
                    <h3 align="center">Administrative Area</h3>
                    <div class="adminblock">
                        <div class="uk-slidenav-position" data-uk-slideshow="autoplay:true,">
                            <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous"
                               data-uk-slideshow-item="previous" style="color: #333333; font-size:40px"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next"
                               data-uk-slideshow-item="next" style="color: #333333; font-size:40px"></a>
                            <ul class="uk-slideshow">


                                <!--slide adminitration-->
                                <?php
                                $i = 0;
                                foreach ($administration_members as $slide_row):
                                    $i++;
                                    ?>
                                    <li>
                                        <div class="uk-grid uk-flex-middle" data-uk-grid-margin
                                             style="margin-right:2px solid red;">
                                            <div class="uk-width-medium-1-1 advi">
                                                <p align="center"><img
                                                        src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $slide_row['photo_location']; ?>"
                                                        alt="Placeholder"></p>
                                                <ul class="uk-list uk-list-striped uk-width-medium-1-1 uk-text-center">
                                                    <li class="name"><?php echo $slide_row['name']; ?></li>
                                                    <li> <?php echo $slide_row['post']; ?></li>
                                                </ul>
                                            </div>

                                            <div class="uk-width-medium-1-1 uk-flex-middle">
                                                <p align="center" class="flow"><strong>Follow on</strong></p>
                                                <p align="center">
                                                    <a class=""><i
                                                            class="uk-icon-small uk-icon-button uk-icon-facebook-square"></i></a>
                                                    <a class=""><i
                                                            class="uk-icon-small uk-icon-button uk-icon-twitter"></i></a>
                                                    <a class=""><i
                                                            class="uk-icon-small uk-icon-button uk-icon-linkedin"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </li>

                                <?php endforeach; ?>
                                <!--slide adminitration end-->
                            </ul>

                        </div>
                    </div>
                </div>
                <!--end techer slide area-->

                <?php
                if ($is_open_slide == 1) {
                ?>
                <div class="uk-panel uk-panel-box midsection1">
                    <h3 align="center">Student Birthday</h3>
                    <div class="visit" style="text-align: center;">

                        <!-- Birthday -->

                        <?php
                        if ((!empty($student_birthday_info)) && $is_open_slide == 1) {
                        ?>
                        <script>
                            var birthday_infos = [];
                            var loop_time = "<?php echo count($student_birthday_info); ?>";
                        </script>
                        <?php
                        $i = 1;
                        foreach ($student_birthday_info as $row):
                            ?>
                            <script>
                                birthday_infos.push("Happy Birthday<br><span style='color:#006600; font-size: 1.2em;'><i><?php echo $row['name'] . "</i></span><br>(Roll No-" . $row['roll_no'] . ",Class:" . $row['class_name'] . ")"; ?>");
                            </script>
                            <?php
                        endforeach;
                        ?>
                        <script>
                            function test() {
                                var i = 0;
                                setInterval(function () {
                                    if (i < loop_time) {
                                        document.getElementById("birthday_name").innerHTML = birthday_infos[i];
                                        i = i + 1;
                                    }
                                    else {
                                        i = 0;
                                    }
                                }, 2000);
                            }
                        </script>
                        <?php
                        echo "<script> test(); </script>";
                        ?>
                        <div id="birthday_name"
                        <?php
                        echo "Happy Birthday<br><span style='color:#006600; font-size: 1.2em;'><i>" . $student_birthday_info[0]['name'] . "</i></span><br>(Roll No-" . $student_birthday_info[0]['roll_no'] . ",Class:" . $student_birthday_info[0]['class_name'] . ")";
                        ?>
                    </div>
                    <?php
                    } ?>

                    <!-- Birthday End -->

                </div>
            </div>


            <div class="uk-panel uk-panel-box midsection1">
                <h3 align="center">Founder</h3>
                <div class="visit" style="text-align: center; font-weight: bold;">
                    <?php if (isset($founder_inf) && $founder_info[0]['picture'] != '') { ?>
                        <img
                            src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $founder_info[0]['picture']; ?>"
                            alt="Placeholder"><br>
                    <?php } ?>
                    Name: <?php echo $founder_info[0]['name']; ?><br>
                    Years of Active: <?php echo $founder_info[0]['years_of_active']; ?><br>
                    Born: <?php echo $founder_info[0]['born']; ?><br>
                    Address: <?php echo $founder_info[0]['address']; ?><br>
                    Educational Qua.: <?php echo $founder_info[0]['edu_qua']; ?><br>
                    Occupation: <?php echo $founder_info[0]['occupation']; ?>
                </div>
            </div>
        <?php } ?>

            <div class="uk-panel uk-panel-box quicklink">
                <p align="center">Digital Content</p>
                <ul class="uk-list">
                    <?php
                    echo $digital_content;
                    ?>
                </ul>
            </div>

            <div class="uk-panel uk-panel-box midsection1">
                <h3 align="center">Visitor Statistics</h3>
                <div class="visit">
                    <ul class="uk-list">
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['online']; ?> Online </a>
                        </li>
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['day_value']; ?> Today</a>
                        </li>
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['yesterday_value']; ?>
                                Yesterday</a></li>
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['week_value']; ?> Week</a>
                        </li>
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['month_value']; ?> Month</a>
                        </li>
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['year_value']; ?> Year</a>
                        </li>
                        <li><a href="#"><i class="uk-icon-eye"></i> <?php echo $counter['all_value']; ?> Total</a>
                        </li>
                        <li><a href="#"><i class="uk-icon-list"></i> Record: <?php echo $counter['record_value']; ?>
                                (<?php echo date("d.m.Y", strtotime($counter['record_date'])) ?>)</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <!-- 3rd section delete-->
    </div>
    <hr/>
</div>
</div>


<!-- footer section -->


<div class="uk-flex-middle" data-uk-grid-margin>

    <div class="uk-block-large ftblock"><br>
        <div class="uk-container uk-container-center">
            <div class="uk-grid uk-grid-divider" data-uk-grid-match>
                <div style="text-align: center;" class="uk-width-medium-1-3 footersection"><br/>
                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $contact_info[0]['picture']; ?>"/>
                </div>
                <div class="uk-width-medium-1-3 footersection"><br/>
                    <span>Contact Us</span>
                    <ul class="uk-list">
                        <li><i class="uk-icon-building"></i>&nbsp; <?php echo $contact_info[0]['address']; ?></li>
                        <li><i class="uk-icon-phone-square"></i>&nbsp; Mobile: <?php echo $contact_info[0]['mobile']; ?>
                        </li>
                        <li><i class="uk-icon-map-marker"></i>&nbsp;&nbsp;<?php echo $contact_info[0]['email']; ?></li>
                    </ul>
                    <a href="<?php echo $contact_info[0]['facebook_address']; ?>"
                       class="uk-icon-button uk-icon-facebook-square"></a>

                </div>
                <div style="text-align: center;" class="uk-width-medium-1-3 footersection"><br>
                    <img src="<?php echo base_url(); ?>core_media/css/login_css/school360-logo.png"/>
					<br>
					<span style="font-size: 12px;">শিক্ষা প্রতিষ্ঠানের জন্য সব</span>
                </div>
            </div>
            <hr/>
            <div class="copy" style="font-size: 12px;">&copy; Copyright @ 2015-<?php echo date('Y') ?>,<span style="color:#0059b3;">School</span><span
                    style="color:#ff8080;">360</span> v 4.0.1, <a href="https://spatei.com/">Spate Initiative Limited</a>
            </div>

        </div>
    </div>
</div>

<!-- footer section end -->
</body>
</html>
