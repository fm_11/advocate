<h1>Full Information</h1>
<table width="100%" id="teacher-table-a">
    <tr>
        <td colspan="3"
            style="font-size:25px; text-align:center; border:0px; color: #1293EE;"><?php echo $students[0]['name']; ?>
        </td>
    </tr>
  
	<tr>
        <td align="right" style="width: 45%;">Year of Passing</td>
        <td> :</td>
        <td><?php echo $students[0]['year_of_passing']; ?>
        </td>
    </tr>
	<tr>
        <td align="right" style="width: 45%;">Educational Qualification</td>
        <td> :</td>
        <td><?php echo $students[0]['edu_qua']; ?>
        </td>
    </tr>
    <tr>
        <td align="right" style="width: 45%;">Organization</td>
        <td> :</td>
        <td><?php echo $students[0]['org']; ?> </td>
    </tr>
	 <tr>
        <td align="right" style="width: 45%;">Position</td>
        <td> :</td>
        <td><?php echo $students[0]['position']; ?> </td>
    </tr>
	
	<tr>
        <td align="right" style="width: 45%;">Address</td>
        <td> :</td>
        <td><?php echo $students[0]['address']; ?> </td>
    </tr>
    
    <tr>
        <td align="right" style="width: 45%;">Mobile</td>
        <td> :</td>
        <td><?php echo $students[0]['mobile']; ?> </td>
    </tr>
    <tr>
        <td align="right" style="width: 45%;">Email</td>
        <td> :</td>
        <td><?php echo $students[0]['email']; ?> </td>
    </tr>
</table>



