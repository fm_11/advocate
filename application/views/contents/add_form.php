<div class="uk-width-large-2-4 midsection">
    <h3>Admission Form</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Form Name</th>
                <th>Class</th>
                <th>Download</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($add_form as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['form_name']; ?></td>
                    <td><?php echo $row['class_name']; ?></td>
                    <td><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/add_form/<?php echo $row['location']; ?>"><i class="uk-icon-small uk-icon-eye"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>