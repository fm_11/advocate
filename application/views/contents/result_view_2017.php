<table width="100%" style="height: 550px; border-radius: 15px;" border="1" align="center" cellpadding="0"
       cellspacing="0" bgcolor="#FFFFFF">
    <tbody>
    <tr>
        <td width="12" align="left" valign="top">
            &nbsp;</td>
        <td valign="top">
            <table width="100%" border="1" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td height="100" align="center" valign="middle" class="black16bold">
                        <b>Annual Grand Result, 2017</b>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="middle">
                        <table width="100%" border="1" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="1"
                                           class="black12">
                                        <tbody>
                                        <tr>
                                            <td width="12%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                Roll No
                                            </td>
                                            <td width="27%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['roll_no']; ?>
                                            </td>
                                            <td width="22%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                Name
                                            </td>
                                            <td width="39%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">Class</td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['class_name']; ?>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">Father's Name
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['father_name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                Section
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['section_name']; ?>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                Mother's Name
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['mother_name']; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">Group</td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                               <?php echo $studnet_info[0]['group']; ?>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                Date of Birth
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $studnet_info[0]['date_of_birth']; ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="40" align="center" valign="middle"><span class="black16bold">Exam Wise Total Mark and Grade</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <table width="100%" border="1" cellpadding="3" cellspacing="1"
                                           class="black12">
                                        <tbody>
                                        <tr class="black12bold">
                                            <td width="33%" align="center" valign="middle" bgcolor="#AFB7BE">
                                                Exam
                                            </td>
                                            <td width="33%" align="center" valign="middle" bgcolor="#AFB7BE">
                                                Obtain Mark
                                            </td>
                                            <td width="33%" align="center" valign="middle" bgcolor="#AFB7BE">
                                                 Average Mark
                                            </td>
                                           
                                        </tr>
                                        
                                        
                                       <?php
	                              $i = 0;
	                              while($i < count($rdata[0]['marks_info'])){
	                              ?>
	                             
	                                <tr class="black12bold">
                                            <td width="33%" align="center" valign="middle">
                                                <?php echo $rdata[0]['marks_info'][$i]['exam_name']; ?>
                                            </td>
                                            <td width="33%" align="center" valign="middle">
                                               <?php echo number_format($rdata[0]['marks_info'][$i]['total_obtain_mark'],2); ?>
                                            </td>
                                            <td width="33%" align="center" valign="middle">
                                                 <?php echo number_format($rdata[0]['marks_info'][$i]['average_mark'],2); ?>
                                            </td>
                                           
                                        </tr>
	                             
	                             <?php $i++; } ?>
	                             
	                              <tr>
                                         <td colspan="2" align="right"><b>Total Mark</b></td>
                                         <td align="center"><b><?php echo number_format($rdata[0]['total_mark'],2); ?></b></td>
                                      </tr> 
                                        
                                        
                                       <tr>
                                         <td colspan="2" align="right"><b>Total Average Mark</b></td>
                                         <td align="center"><b><?php echo number_format($rdata[0]['total_average_mark'],2); ?></b></td>
                                      </tr> 
                                      
                                      <tr>
                                         <td colspan="2" align="right"><b>CGPA</b></td>
                                         <td align="center"><b><?php echo number_format($rdata[0]['cgpa'],2); ?></b></td>
                                      </tr> 
                                      
                                        <tr>
                                         <td colspan="2" align="right"><b>Grade</b></td>
                                         <td align="center"><b><?php echo $rdata[0]['grade']; ?></b></td>
                                      </tr> 
                                        
                                   </table>
                                </td>
                             </tr>  
                             
                            
                            
                                    
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<br><br>