<script>
    function exam_by_year(year) {
        if (year != '') {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("exam_space").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>contents/ajax_exam_by_year?year=" + year, true);
            xmlhttp.send();
        } else {
            document.getElementById("exam_space").innerHTML = 'Please Select Year....';
        }
    }
</script>
<form action="<?php echo base_url(); ?>contents/getResult" class="register" style="height: 320px;"
      enctype="multipart/form-data" method="post">
    <h1>Result Archive</h1>
    <fieldset class="row1">
        <legend>
            Please Input Correct Information
        </legend>


        <p>
            <label>Year *</label>
            <select name="year" id="year" required="1" onchange="exam_by_year(this.value)">
                <option value="">-- Select --</option>
                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
            </select>
        </p>

        <p>
            <label>Exam *</label>
            <span id="exam_space">Please Select Year....</span>
        </p>

        <p>
            <label>Class *
            </label>
            <select class="smallInput" name="class" required="1">
                <option value="">-- Select --</option>
                <?php
                $i = 0;
                if (count($class)) {
                    foreach ($class as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                    }
                }
                ?>
            </select>
        </p>

        <p>
            <label>Group *
            </label>
        <select name="group" class="smallInput" required="required">
            <option value="">-- Select --</option>
            <?php
            $i = 0;
            if (count($group)) {
                foreach ($group as $list) {
                    $i++;
                    ?>
                    <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>
        </select>
        </p>


        <p>
            <label>Section *
            </label>
            <select class="smallInput" name="section" required="1">
                <option value="">-- Select --</option>
                <?php
                $i = 0;
                if (count($section)) {
                    foreach ($section as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                    }
                }
                ?>
            </select>
        </p>
        
        
        <p>
            <label>Shift *
            </label>
            <select class="smallInput" name="shift" required="1">
                <option value="">-- Select --</option>
                <?php
                $i = 0;
                if (count($shift)) {
                    foreach ($shift as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                    }
                }
                ?>
            </select>
        </p>

        <p>
            <label>Roll *
            </label>
            <input type="text" autocomplete="off"  name="roll" id="roll" required/>
        </p>

        <p>

        <h2 style="color: red;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <u>
                <?php
                $exception = $this->session->userdata('exception');
                if ($exception != '') {
                    ?>
                    <span class="info_inner">
                                <?php
                                echo $exception;
                                $this->session->unset_userdata('exception');
                                ?>
                            </span>
                <?php
                }
                ?>
            </u>
        </h2>
        </p>
        <p>
            <label>
                <input type="submit" class="button" value="View">
            </label>
        </p>
    </fieldset>
</form>

