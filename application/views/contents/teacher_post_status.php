<div class="uk-width-large-2-4 midsection">
    <h3><?php echo $heading_msg; ?></h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Post Name</th>
                <th>Number of Post</th>
                <th>Exist Post</th>
                <th>Vacant Post</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($post_status_teacher as $row):
                $i++;
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['num_of_post']; ?></td>
                    <td><?php echo $row['NoOfExistPost']; ?></td>
                    <td><?php echo($row['num_of_post'] - $row['NoOfExistPost']); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>


        <h3>Staff Post Status</h3>
        <hr/>
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Post Name</th>
                <th>Number of Post</th>
                <th>Exist Post</th>
                <th>Vacant Post</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $j = 0;
            foreach ($post_status_staff as $row):
                $j++;
                ?>
                <tr>
                    <td>
                        <?php echo $j; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['num_of_post']; ?></td>
                    <td><?php echo $row['NoOfExistPost']; ?></td>
                    <td><?php echo($row['num_of_post'] - $row['NoOfExistPost']); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>