<script>
    $(function () {
        $("#from_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
        $("#to_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<div class="uk-width-large-2-4 midsection">
    <h3>Academic Holiday</h3>
    <hr/>
    <div class="uk-margin">
        <form class="uk-form" action="<?php echo base_url(); ?>contents/getAcademicHoliday" method="post">
            <?php
            $from_date = $this->session->userdata('from_date');
            $to_date = $this->session->userdata('to_date');
            if ($from_date != '') {
                $from_date = $from_date;
            } else {
                $from_date = "";
            }

            if ($to_date != '') {
                $to_date = $to_date;
            } else {
                $to_date = "";
            }
            ?>
            <fieldset data-uk-margin>

                <input type="text" autocomplete="off"  name="from_date" class="uk-width-large-1-4" id="from_date" size="15"
                       placeholder="From Date" value="<?php if ($from_date != '') {
                    echo $from_date;
                } ?>"/>
                <input type="text" autocomplete="off"  name="to_date" class="uk-width-large-1-4" id="to_date" size="15"
                       placeholder="To Date" value="<?php if ($to_date != '') {
                    echo $to_date;
                } ?>"/>

                <input type="submit" name="Search" class="uk-button uk-width-large-1-5" value="Search"/>
            </fieldset>
        </form>
    </div>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Holiday Type</th>
                <th>Date</th>
                <th>Reason</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($holiday_info as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['holiday_name']; ?></td>
                    <td><?php echo $row['date']; ?></td>
                    <td><?php echo $row['remarks']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->
</div>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>