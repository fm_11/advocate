<script>
    function checkPassword(){
        var pass1 = document.getElementById("txtPassword").value;
        var pass2 = document.getElementById("txtCPassword").value;
        if (pass1 != pass2) {
            alert("Passwords Do not match !");
            document.getElementById("txtPassword").style.borderColor = "#E34234";
            document.getElementById("txtCPassword").style.borderColor = "#E34234";
            return false;
        }
        else {
            return true;
        }
    }
</script>

<form action="<?php echo base_url(); ?>contents/ApplyOnline" class="register" onsubmit="return checkPassword()" enctype="multipart/form-data" method="post">
    <h1>Admission</h1>
    <fieldset class="row1">
        <legend>Admit Collect Account Details
        </legend>
        <p>
            <label>Mobile *
            </label>
            <input type="text" autocomplete="off"  name="txtAdmitMobile" required/>

            <label>Admission Year *</label>
            <select name="txtYear" required="required">
                <option value="">-- Please Select --</option>
                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                <option value="<?php echo (date('Y') + 1); ?>"><?php echo (date('Y') + 1); ?></option>
            </select>
        </p>

        <p>
            <label>Password*
            </label>
            <input type="password" name="txtPassword" id="txtPassword" required/>
            <label>Repeat Password*
            </label>
            <input type="password" name="txtCPassword" id="txtCPassword" required/>
            <label class="obinfo">* Important fields for get admit card.
            </label>
        </p>
    </fieldset>
    <fieldset class="row2">
        <legend>Personal Details
        </legend>
        <p>
            <label>Name *
            </label>
            <input type="text" autocomplete="off"  class="long" name="txtStudentName" required/>
        </p>

        <p>
            <label>Father's Name *
            </label>
            <input type="text" autocomplete="off"  class="long" name="txtFathersName" required/>
        </p>

        <p>
            <label>Father's NID *
            </label>
            <input type="text" autocomplete="off"  class="long" name="txtFathersNID" required/>
        </p>

        <p>
            <label>Mother's Name *
            </label>
            <input type="text" autocomplete="off"  class="long" name="txtMothersName" required/>
        </p>

        <p>
            <label>Mother's NID *
            </label>
            <input type="text" autocomplete="off"  class="long" name="txtMothersNID" required/>
        </p>

        <p>
            <label>Birthdate *
            </label>
            <select class="date" name="txtDOBDay" required>
                <option value="01">01
                </option>
                <option value="02">02
                </option>
                <option value="03">03
                </option>
                <option value="04">04
                </option>
                <option value="05">05
                </option>
                <option value="06">06
                </option>
                <option value="07">07
                </option>
                <option value="08">08
                </option>
                <option value="09">09
                </option>
                <option value="10">10
                </option>
                <option value="11">11
                </option>
                <option value="12">12
                </option>
                <option value="13">13
                </option>
                <option value="14">14
                </option>
                <option value="15">15
                </option>
                <option value="16">16
                </option>
                <option value="17">17
                </option>
                <option value="18">18
                </option>
                <option value="19">19
                </option>
                <option value="20">20
                </option>
                <option value="21">21
                </option>
                <option value="22">22
                </option>
                <option value="23">23
                </option>
                <option value="24">24
                </option>
                <option value="25">25
                </option>
                <option value="26">26
                </option>
                <option value="27">27
                </option>
                <option value="28">28
                </option>
                <option value="29">29
                </option>
                <option value="30">30
                </option>
                <option value="31">31
                </option>
            </select>
            <select name="txtDOBMonth" required>
                <option value="01">January
                </option>
                <option value="02">February
                </option>
                <option value="03">March
                </option>
                <option value="04">April
                </option>
                <option value="05">May
                </option>
                <option value="06">June
                </option>
                <option value="07">July
                </option>
                <option value="08">August
                </option>
                <option value="09">September
                </option>
                <option value="10">October
                </option>
                <option value="11">November
                </option>
                <option value="12">December
                </option>
            </select>
            <input class="year" type="text" name="txtDOBYear" required size="6" maxlength="4"/>e.g 1976
        </p>
        <p>
            <label>Gender *</label>
            <input type="radio" value="M" checked name="txtGender" required/>
            <label class="gender">Male</label>
            <input type="radio" value="F" name="txtGender"/>
            <label class="gender">Female</label>
        </p>

        <p>
            <label>Religion *</label>
            <select name="txtReligion" required>
                <option value="I">Islam</option>
                <option value="H">Hindu</option>
            </select>
        </p>

        <p>
            <label>
                Guardian Mobile *
            </label>
            <input type="text" autocomplete="off"  maxlength="11" name="txtGuardianMobile" required/>
        </p>

        <p>
            <label>
                Present Address *
            </label>
            <input class="long" type="text" name="txtPresentAddress" required/>
        </p>

        <p>
            <label>
                Permanent Address *
            </label>
            <input class="long" type="text" name="txtPermanentAddress" required/>
        </p>
        <p>
            <label>
                Photo *
            </label>
            <input class="long" type="file" name="txtPhoto" required/>
        </p>
    </fieldset>
    <fieldset class="row3">
        <legend>Admission Information
        </legend>

        <p>
            <label>
                Admission Class *
            </label>
            <select name="txtClass" required="1">
                <option value="">-- Please Select --</option>
                <?php
                $i = 0;
                if (count($class)) {
                    foreach ($class as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                    }
                }
                ?>
            </select>
        </p>

        <p>
            <label>Admission Group *</label>
            <select name="txtGroup" required="required">
                <option value="">-- Please Select --</option>
                <?php
                $i = 0;
                if (count($group)) {
                    foreach ($group as $list) {
                        $i++;
                        ?>
                        <option
                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </p>

        <p>
            <label>Last School Name *
            </label>
            <input class="long" type="text" name="txtLastSchoolName" required/>
        </p>

        <p>
            <label>Last Result *
            </label>
            <input class="long" type="text" name="txtLastResult" required/>
        </p>

        <div class="infobox"><h4>Helpful Information</h4>

            <p>Here comes some explaining text, sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et
                quasi architecto beatae vitae dicta sunt explicabo.</p>
        </div>
    </fieldset>
    <fieldset class="row4">
        <legend>Terms and Conditions
        </legend>
        <p class="agreement">
            <input type="checkbox" value="1" required/>
            <label>* I accept the <a href="#">Terms and Conditions</a></label>
        </p>
    </fieldset>
    <div>
        <input type="submit" class="button" value="Submit">
    </div>
</form>

