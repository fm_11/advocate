<div class="uk-width-large-2-4 midsection">
    <h3>Student Information</h3><hr />
    <div class="uk-margin">
        <form class="uk-form" action="<?php echo base_url(); ?>contents/getStudentInformation" method="post">
            <?php
            $class_id = $this->session->userdata('class_id');
            $section_id = $this->session->userdata('section_id');
            $group = $this->session->userdata('group');
            ?>
            <fieldset data-uk-margin>
                <select  class="uk-width-large-1-4" name="class_id" id="class_id">
                    <option value="">--Class--</option>
                    <?php foreach ($class_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if($row['id'] == $class_id){ echo 'selected'; } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>


                <select name="section_id" class="uk-width-large-1-4" id="section_id">
                    <option value="">--Section--</option>
                    <?php foreach ($section_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if($row['id'] == $section_id){ echo 'selected'; } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>

                <select name="group" class="uk-width-large-1-4">
                    <option value="">--Group--</option>
                    <?php foreach ($group_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if($row['id'] == $group){ echo 'selected'; } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>

                <input type="submit" name="Search" class="uk-button uk-width-large-1-5" value="Search"/>
            </fieldset>
        </form>
    </div>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Student ID</th>
                <th>Name</th>
                <th>Roll</th>
                <th>Class</th>
                <th>Section</th>
                <th>Group</th>
                <th>Profile</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($student_info as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['student_code']; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['roll_no']; ?></td>
                    <td><?php echo $row['class_name']; ?></td>
                    <td><?php echo $row['section_name']; ?></td>
                    <td><?php echo $row['group_name']; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>contents/getStudentInfoByStudentID/<?php echo $row['id']; ?>"><i class="uk-icon-small uk-icon-eye"></i> View</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
	<?php echo $this->pagination->create_links(); ?>
    <!--  PAGINATION END-->

</div>
