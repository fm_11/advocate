<script>
	function printPartOfPage() {
		var title = document.title;
		var divElements = document.getElementById('divPrintable').innerHTML;
		var printWindow = window.open("", "_blank", "");
		//open the window
		printWindow.document.open();
		//write the html to the new window, link to css file
		//printWindow.document.write('<html><head><title>' + title + '</title><link rel="stylesheet" type="text/css" href="/Content/PrintMedia.css"></head><body>');
		printWindow.document.write('<html><head><title>' + title + '</title></head><body>');
		printWindow.document.write(divElements);
		printWindow.document.write('</body></html>');
		printWindow.document.title = "";
		printWindow.document.date = "";
		printWindow.document.close();
		printWindow.focus();
		//The Timeout is ONLY to make Safari work, but it still works with FF, IE & Chrome.

		setTimeout(function () {
			printWindow.print();
			printWindow.close();
		}, 50);

	}
</script>
