<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?></title>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<style>
@media print
{
  #footer {
    display: block;
    position: fixed;
    bottom: 0;
  }
}

.reortPrintOption{
  float: left;
  padding: 5px;
  width: 100%;
}

.data_table{
  border-collapse: collapse;
}

table.data_table td,th{
  font-size: 13px;
  padding: 3px;
  border: 1px solid black;
  vertical-align: middle;
}

#header{
 width:100%;
 box-sizing: border-box;
 height: auto;
 display: table;
}
.header-left {
  display: table-cell;
	width:30%;
	float:left;
	height: 100%;
	box-sizing: border-box;
	text-align: left;
  vertical-align: middle;
}
.header-middle {
  display: table-cell;
	width:40%;
	float:left;
	min-height: 60px;
	box-sizing: border-box;
	text-align: center;
  vertical-align: middle;
}
.header-right {
  display: table-cell;
	width:30%;
	float:left;
	height: 100%;
	box-sizing: border-box;
	text-align: right;
  vertical-align: middle;
}
.frame{
	width: 100%;
	height: 100%;
	margin: auto;
	position: relative;
}

.printButton:link, .printButton:visited {
      background-color: #f44336;
      color: white;
      padding: 8px 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
}

.printButton:hover, .printButton:active {
  background-color: red;
}

.backToButton:link, .backToButton:visited {
      background-color: #a3c2c2;
      color: white;
      padding: 8px 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
}

.backToButton:hover, .backToButton:active {
  background-color: #527a7a;
}

.td_center{
  text-align: center;
}
.td_left{
  text-align: left;
}
.td_right{
  text-align: right;
}

.report_title{
	font-size: 18px;
	text-decoration: underline;
}
* {
	-webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
	color-adjust: exact !important;                 /*Firefox*/
}
</style>

</head>

<body>
<div class="reortPrintOption">
    <a class="backToButton" href="">
      Back to Index
    </a>
    <a class="printButton" href="javascript:void" onclick="printDiv('printableArea')">
     Print
   </a>
</div>

<div id="printableArea">
<table border="0" width="100%">
   <thead>
    <tr>
		<th style="width:100%">
			 <div id="header">
				 <div class="header-left">
					<div class="frame">
						 <img style="max-width:80px; " src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $HeaderInfo[0]['picture']; ?>" />
					</div>
				 </div>
         <div class="header-middle">
				  	<div style="">
						<b style="font-size:22px;"><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
						<span style="font-weight: normal;font-size:13px;"><?php echo $HeaderInfo[0]['address']; ?><br>
					 Mobile: <?php echo $HeaderInfo[0]['mobile']; ?><br>
					 Email: <?php echo $HeaderInfo[0]['email']; ?></span><br>
						<span class="report_title"><?php echo $title; ?></span>
					</div>
				 </div>
				 <div class="header-right">

				 </div>
			</div>
		</th>
    </tr>

  </thead>
  <tfoot>
   <tr>
    <td width="100%">
     <table width="100%" border="0">
       <tr>
         <td colspan="4"><br>&nbsp;</td>
      </tr>
    </table>
  </tfoot>
  <tbody>
    <tr>
      <td width="100%">
         <?php echo $report; ?>
      </td>
   </tr>
 </tbody>
</table>
<table id="footer" width="100%">
  <tr>
  <td width="100%" style="font-size:13px;">
     Powered By: Spate-i | Print Date: <?php echo date("d-m-Y", strtotime(date('Y-m-d'))); ?>
  </td>
  </tr>
</table>
</div>
</body>
</html>
