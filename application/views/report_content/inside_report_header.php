<?php
if (isset($is_pdf)) {
		?>
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	 <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<?php }?>
<style>
body {
    font-family: 'SolaimanLipi',sans-serif;
}
	footer {
		/*font-size: 9px;*/
		color: #adadad;
		text-align: left;
		font-family: sans-serif;
	}

	#tblItems td {
		border: 1px solid #acacac;
	}

	#tblItems {
		border-collapse: collapse;
		width: 100%;
	}

	#tblHeader, #tblCustomer {
		margin-left: -5px;
	}

	th, td {
		padding: 5px;
	}

	<?php
	if(isset($page_type) && isset($paper_size)){
	 ?>
	@page {
		size: <?php echo $paper_size . ' ' . $page_type; ?>;
		margin: 15mm 15mm 15mm 15mm;
	}
	<?php }else{ ?>
	@page {
		size: A4;
		margin: 15mm 15mm 15mm 15mm;
	}
	<?php } ?>

	@media print {
		footer {
			position: fixed;
			bottom: 0;
			display: block !important;
		}

		header {
			position: fixed;
			top: 0;
		}

		.content-block, p {
			page-break-inside: avoid;
		}
	}
	* {
		-webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
		color-adjust: exact !important;                 /*Firefox*/
	}


	.printButton:link, .printButton:visited {
		background-color: #f44336;
		color: white;
		padding: 8px 8px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
	}

	.printButton:hover, .printButton:active {
		background-color: red;
	}

	.backToButton:link, .backToButton:visited {
		background-color: #a3c2c2;
		color: white;
		padding: 8px 8px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
	}

	.backToButton:hover, .backToButton:active {
		background-color: #527a7a;
	}

</style>

<div class="" style="width:100%; height:auto; border:1px solid #ffffff">
	<table id="tblHeader" style="width: 100%;">
		<tr>
			<td style="width:100px;">


				<?php
				if(isset($is_pdf) && SERVER_TYPE == 'WINDOWS'){
				$image_url = PUBPATH . MEDIA_FOLDER . "\\logos\\" . $HeaderInfo[0]['picture'];
				}else{
				$image_url = base_url() . MEDIA_FOLDER . '/logos/' . $HeaderInfo[0]['picture'];
				}
				?>
				<img style="height: 80px; width: 80px; border: 1px solid #acacac; padding: 5px" src="<?php echo $image_url; ?>" alt="Logo" />
			</td>
			<td>
				<span style="text-transform: uppercase; font-size:20px"><b><?php echo $HeaderInfo[0]['school_name']; ?></b></span>
				<br />
				<span><?php echo $HeaderInfo[0]['address']; ?></span>
				<br />
				<span>Phone : </span><span><?php echo $HeaderInfo[0]['mobile']; ?></span>
				<span>, Email : </span> <span><?php echo $HeaderInfo[0]['email']; ?></span>
				<!-- <br />
				<span>Website : </span> <span><?php echo $HeaderInfo[0]['web_address']; ?></span> -->
			</td>
		</tr>
	</table>
</div>
