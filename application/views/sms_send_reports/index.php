<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>sms_send_reports/index#sms_report" method="post">
	<div class="form-row">

		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('sms').' '.$this->lang->line('type'); ?></label><span class="required_label">*</span><br>
			<select name="sms_from" class="js-example-basic-single w-100">
				<option value="ALL" <?php if(isset($sms_from)){ if($sms_from == 'ALL'){ echo 'selected'; }} ?>>ALL SMS</option>
				<!-- <option value="TG" <?php if(isset($sms_from)){ if($sms_from == 'TG'){ echo 'selected'; }} ?>>To Guardian</option> -->
				<!-- <option value="RN" <?php if(isset($sms_from)){ if($sms_from == 'RN'){ echo 'selected'; }} ?>>Result Notify</option> -->
				<option value="EX" <?php if(isset($sms_from)){ if($sms_from == 'EX'){ echo 'selected'; }} ?>>From Excel</option>
				<option value="PH" <?php if(isset($sms_from)){ if($sms_from == 'PH'){ echo 'selected'; }} ?>>Phone Book</option>
				<option value="AS" <?php if(isset($sms_from)){ if($sms_from == 'HS'){ echo 'selected'; }} ?>>Attendance SMS</option>
				<option value="TS" <?php if(isset($sms_from)){ if($sms_from == 'APS'){ echo 'selected'; }} ?>>Appointment SMS</option>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('from_date'); ?></label><span class="required_label">*</span><br>
			<div class="input-group date">
				<input type="text" autocomplete="off"  name="from_date" required value="<?php if (isset($from_date)) {
					echo date("d-m-Y", strtotime($from_date));
				} ?>" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('to_date'); ?></label><span class="required_label">*</span><br>
			<div class="input-group date">
				<input type="text" autocomplete="off"  name="to_date" required value="<?php if (isset($to_date)) {
					echo date("d-m-Y", strtotime($to_date));
				} ?>" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('sms').' '.$this->lang->line('status'); ?></label><span class="required_label">*</span><br>
			<select name="sms_status" class="js-example-basic-single w-100">
				<option value="all" <?php if(isset($sms_status)){ if($sms_status == 'ALL'){ echo 'selected'; }} ?>>ALL</option>
				<option value="success" <?php if(isset($sms_status)){ if($sms_status == 'success'){ echo 'selected'; }} ?>>SUCCESS</option>
				<option value="invalid" <?php if(isset($sms_status)){ if($sms_status == 'invalid'){ echo 'selected'; }} ?>>Invalid MSISDN</option>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="button" class="btn btn-secondary" onclick="printPartOfPage()" value="Print Result"/>
		<!-- <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/> -->
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>
</form>

<?php
if (isset($report)) {
	echo $report;
}
?>
