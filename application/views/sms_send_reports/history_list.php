<div id="divPrintable" class="box-body" style="padding-left:0px; padding-right:0px">
	<?php echo $report_header;	?>

	<table id="tblCustomer" style="border-top: 5px solid <?php echo $this->session->userdata('table_header_row_color'); ?>;width: 100%; margin-top: 10px; font-size: 14px">
		<tr>
			<td colspan="3" style="text-align: center; text-decoration: underline;">
				<span style="text-transform: uppercase;"><b><?php echo $title; ?></b></span>
			</td>
		</tr>
	</table>

		<table id="tblItems" style="width: 100%; border-top: 1px solid #dedede; margin-top: 20px; font-size:13px">
			   <tr style="border-bottom: 1px solid #333;background-color: <?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('sl'); ?></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('date'); ?></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('message'); ?></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('client_name'); ?></td></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('number_of_sms'); ?></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('sms').' '.$this->lang->line('type'); ?></td>
					<td scope="col" style="color:#ffffff;text-align: center; padding: 5px;" class="h_td">&nbsp;<?php echo $this->lang->line('sms').' '.$this->lang->line('status'); ?></td>
				</tr>

				<?php
				$i = 0;
				foreach ($history_list as $rt_row):
					$i++;
						?>
					<tr <?php if($i % 2 == 0){ echo 'style="background-color:' . $this->session->userdata('REPORT_ROW_COLOR') . ' !important;"'; } ?>>
							<td style="text-align: center;">
								<?php echo $i; ?>
							</td>
							<td>
								<?php echo $rt_row['date_time']; ?>
							</td>
							<td>
								<?php echo $rt_row['msisdn']; ?>
							</td>
							<td>
								<?php echo $rt_row['sms_body']; ?>
							</td>
							<td>
								<?php
								if($rt_row['client_id'] != '0'){
									echo $rt_row['client_name'] . '(' . $rt_row['mobile'] . ')';
								}
								if($rt_row['teacher_id'] != '0'){
									echo $rt_row['teacher_name'] . '(' . $rt_row['teacher_code'] . ')';
								}
								if($rt_row['student_id'] == '0' && $rt_row['teacher_id'] == '0'){
									echo '-';
								}
								?>
							</td>
							<td>
								<?php echo $rt_row['number_of_sms']; ?>
							</td>
							<td style="text-align: center;min-width: 120px;">
								<?php
									if($rt_row['sender_type'] == 'TG'){
										echo 'To Guardian';
									}else if($rt_row['sender_type'] == 'RN'){
										echo 'Result Notify';
									}else if($rt_row['sender_type'] == 'EX'){
										echo 'From Excel';
									}else if($rt_row['sender_type'] == 'PH'){
										echo 'Phone Book';
									}else if($rt_row['sender_type'] == 'HS'){
										echo 'Attendance SMS';
									}else if($rt_row['sender_type'] == 'APS'){
										echo 'Appointment SMS';
									}else{
										echo 'Unidentified';
									}
								?>
							</td>
							<td>
								<?php echo $rt_row['sms_status']; ?>
							</td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
</div>

<?php echo $report_footer;	?>
