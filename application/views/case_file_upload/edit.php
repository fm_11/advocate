<script type="text/javascript">

///file upload
function add_file_row() {

  var language=document.getElementById('language').value;
  var file_name="",label_name="";
    file_name="File Upload";
    label_name="File Name";
    if(language=="bangla")
    {
      file_name="ফাইল আপলোড";
      label_name="ফাইল নাম";
    }


  debugger;
  var table = document.getElementById("file_upload_table");
  var table_len = Number(document.getElementById('file_upload_num_of_row').value);
      var row = table.insertRow(table_len).outerHTML = "<tr id='file_row_" + table_len + "' class='tr_border'><td> <label for='inputEmail4'>"+file_name+" <span class='required_label'>*</span></label> <input type='file' autocomplete='off'  class='form-control' id='file_name_" + table_len + "'  name='file_name_" + table_len + "' required value=''></td><td id='delete_file_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_fil_row(" + table_len + ")'></td></tr>";
  //alert(row);
  if(table_len > 1){
         document.getElementById("delete_file_section_" + (table_len - 1)).innerHTML = "";
      }
      document.getElementById('file_upload_num_of_row').value = table_len + 1;
}
function delete_fil_row(row_no) {
    var no = Number(row_no);
    document.getElementById("file_row_" + no + "").outerHTML = "";
    if(no != 1){
        document.getElementById("delete_file_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_fil_row(" + (no - 1) + ")'>";
    }
    document.getElementById('file_upload_num_of_row').value = no;
}
function delete_case_file_row(id) {

  var result = confirm("Are you sure to delete?");
    if (result) {

      if (window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else
      {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function()
      {
          if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
          {
              document.getElementById("case_file_row_" + id).outerHTML = "";
          }
      }
      xmlhttp.open("GET", "<?php echo base_url(); ?>case_file_upload/delete_case_file?id=" + id, true);
      xmlhttp.send();
    }
}
</script>
<style>
.fieldset_div{
  border: 3px solid #efdfdf;
  padding: 20px;
}
.tr_border{
  border-top: 2px solid #fff;
}
</style>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>case_file_upload/edit/<?php echo $case_file_details[0]['reference_id']; ?>" class="cmxform" enctype="multipart/form-data" method="post">
  <input type="hidden" id="language" value="<?php echo $language; ?>"/>
  <input type="hidden" name="reference_id" value="<?php echo $case_file_details[0]['reference_id']; ?>"/>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?><span class="required_label">*</span></label>
      <select   name="case_master_id" class="js-example-basic-single w-100" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($case_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($case_file_details[0]['case_master_id'])) {
           if ($row['id'] == $case_file_details[0]['case_master_id']) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1" value="<?php echo $case_file_details[0]['label_name']; ?>"/>
    </div>
  </div>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <table class="table"  style="border-top: 2px solid #fff;">
         <th><?php echo $this->lang->line('sl'); ?></th>
         <th><?php echo $this->lang->line('file').' '.$this->lang->line('upload').' '.$this->lang->line('list'); ?></th>
         <th><?php echo $this->lang->line('download'); ?></th>
         <th><?php echo $this->lang->line('delete'); ?></th>
         <?php $i=1; foreach ($case_file_details as  $value): ?>
           <tr class="tr_border" id="case_file_row_<?php echo $value['id']; ?>">
               <td>
                <?php echo $i; ?>
               </td>
               <td>
                <div>
                   <img src="<?php echo base_url(). MEDIA_FOLDER; ?>/case_file/<?php echo $value['file_path']; ?>" alt="not found" style="width: 150px;height: 100px;border-radius: 0px;">
                </div>
               </td>
               <td>
                 <a  target="_blank" href="<?php echo base_url(). MEDIA_FOLDER; ?>/case_file/<?php echo $value['file_path']; ?>">
                   <button type="button" class="btn btn-secondary btn-xs mb-1"><?php echo $this->lang->line('download'); ?></button>
                 </a>
               </td>
               <td>

                 <input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="delete_case_file_row(<?php echo $value['id']; ?>);" value="<?php echo $this->lang->line('delete'); ?>">
               </td>
           </tr>
         <?php $i++; ?>
         <?php endforeach; ?>

       </table>
      </div>
  </fieldset>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <table class="table" id="file_upload_table" style="border-top: 2px solid #fff;">
           <tr class="tr_border">
               <td>
                 <label for="inputEmail4"><?php echo $this->lang->line('file').' '.$this->lang->line('upload'); ?></label>
                 <input type="file" autocomplete="off"  class="form-control" id="file_name_0"  name="file_name_0" >
               </td>
               <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_file_row();" value="<?php echo $this->lang->line('add_more'); ?>"></td>
           </tr>
       </table>
         <input type="hidden" id="file_upload_num_of_row" name="file_upload_num_of_row" value="1">
      </div>
  </fieldset>
  <br><br>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
