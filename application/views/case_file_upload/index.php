

<?php
$case_master_id = $this->session->userdata('case_master_id');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>case_file_upload/index">
<div class="col-md-12">

        <label class="sr-only"> <?php echo $this->lang->line('case_no'); ?></label>
        <select   name="case_master_id" id="case_master_id" class="js-example-basic-single w-100 col-md-6" >
          <option value="">-- <?php echo $this->lang->line('case_no'); ?> --</option>
          <?php foreach ($case_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"<?php if (isset($case_master_id)) {
             if ($row['id'] == $case_master_id) {
               echo 'selected';
             }
           } ?>><?php echo $row['name']; ?></option>
          <?php } ?>
        </select>
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>

            <!-- <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body"> -->

                  <div class="mt-4">
                    <div class="accordion accordion-solid-header" id="accordion-4" role="tablist">
                      <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-1" class="table table-striped">
                      <thead>
                      <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('case_no'); ?></th>
                            <th><?php echo $this->lang->line('case_file_name'); ?></th>
                            <th><?php echo $this->lang->line('file').' '.$this->lang->line('view'); ?></th>
                            <th><?php echo $this->lang->line('actions'); ?></th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = (int)$this->uri->segment(3);
                        foreach ($case_file_list as $row):
                            $i++;
                            ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $row['case_no']; ?></td>
                              <td>
                                <div class="card">
                                  <div class="card-header" role="tab" id="heading-<?php echo $i; ?>">
                                    <h6 class="mb-0">
                                      <a class="collapsed" data-toggle="collapse" href="#collapse-<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $i; ?>">
                                        <?php echo $row['label_name']; ?>
                                      </a>
                                    </h6>
                                  </div>
                                  <div id="collapse-<?php echo $i; ?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?php echo $i; ?>" data-parent="#accordion-4">
                                    <div class="card-body" style="padding-left: 0px !important;">
                                      <?php $j=1; foreach ($row['file_list'] as  $value): ?>
                                        <?php if(isset($value['file_path']) && $value['file_path']!='0'){?>
                                          <div class="container">
                                            <div class="row">
                                              <div class="col-md-6">
                                                <p><?php echo $j;?></p>
                                              </div>
                                              <div class="col-md-6">
                                                <a  target="_blank" href="<?php echo base_url(). MEDIA_FOLDER; ?>/case_file/<?php echo $value['file_path']; ?>">
                                                  <button type="button" class="btn btn-secondary btn-xs mb-1"><?php echo $this->lang->line('download'); ?></button>
                                                </a>
                                              </div>
                                            </div>
                                          </div>
                                         <?php }?>
                                          <?php $j++;?>
                                      <?php endforeach; ?>

                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td>
                                <div class="container">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <a  target="_blank" href="<?php echo base_url(); ?>case_file_upload/view/<?php echo $row['reference_id']; ?>">
                                        <button type="button" class="btn btn-secondary btn-xs mb-1"><?php echo $this->lang->line('view'); ?></button>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td>
                                  <div class="dropdown">
                                      <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <i class="ti-pencil-alt"></i>
                                      </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                                          <a class="dropdown-item" href="<?php echo base_url(); ?>case_file_upload/edit/<?php echo $row['reference_id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                                          <!-- <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>cases/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a> -->
                                          <!-- <a class="dropdown-item" href="<?php echo base_url(); ?>case_file_upload/view/<?php echo $row['reference_id']; ?>"><?php echo $this->lang->line('view'); ?></a> -->
                                      </div>
                                  </div>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                          </tbody>
                      </table>
                      <div class="float-right">
                      <?php echo $this->pagination->create_links(); ?>
                      </div>
                    </div>
                  </div>
                </div>
              <!-- </div>
            </div>
        </div> -->
