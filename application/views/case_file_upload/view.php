<script type="text/javascript">

function delete_file_row(id) {

  var result = confirm("Are you sure to delete?");
    if (result) {

      if (window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else
      {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function()
      {
          if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
          {
              document.getElementById("file_row_" + id).outerHTML = "";
          }
      }
      xmlhttp.open("GET", "<?php echo base_url(); ?>case_file_upload/delete_case_file?id=" + id, true);
      xmlhttp.send();
    }
}

</script>
<style>
.fieldset_div{
  border: 3px solid #efdfdf;
  padding: 20px;
}
.tr_border{
  border-top: 2px solid #fff;
}
.modal .modal-dialog {
    margin-top: 0px !important;
}
.modal .modal-dialog .modal-content .modal-header {
      padding: 8px 26px;
}
.modal-header {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    padding: 6px 13px;
    border-bottom: 1px solid rgba(90, 113, 208, 0.11);
    border-top-left-radius: 0.3rem;
    border-top-right-radius: 0.3rem;
}
.modal .modal-dialog .modal-content .modal-body {
    padding: 0px 26px;
}
.modal-body {
    position: relative;
    flex: 1 1 auto;
    padding: 0px 15px;
}
.form-group {
    margin-bottom: 5px;
}
.form-group label {
    font-size: 0.875rem;
    /* line-height: 1.4rem; */
    vertical-align: top;
    margin-bottom: 0px;
}
.col-form-label {
    padding-top: calc(0.875rem + 1px);
    padding-bottom: calc(0.5rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1;
}
</style>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?></label>
      <input type="text" autocomplete="off"  class="form-control"  disabled value="<?php echo $case_file_details[0]['case_no'];?>"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $case_file_details[0]['label_name'];?>"/>
    </div>
  </div>
  <div class="container">
   <?php $i=1; foreach ($case_file_details as  $value): ?>
   <div class="row overflow-auto" style="padding-bottom: 30px;">
     <div class="col-md-1 col-lg-1 col-sm-1">
       <?php echo $i; ?>
       </div>
     <div class="col-md-8 col-lg-9 col-sm-12">
      <img style="height: 100% !important;width: 100% !important;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/case_file/<?php echo $value['file_path']; ?>" alt="not found" >
     </div>
     <div class="col-md-2 col-lg-1 col-sm-6">
       <button type="button"  id="send_lead_email" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
        data-target="#SendMailApplicantModalContent_<?php echo $i; ?>" data-whatever="">Send Email
        </button>
       <a target="_blank" href="<?php echo base_url(). MEDIA_FOLDER; ?>/case_file/<?php echo $value['file_path']; ?>">
         <button type="button" class="btn btn-secondary btn-xs mb-1"><?php echo $this->lang->line('download'); ?></button>
       </a>
     </div>
     <div class="col-md-2 col-lg-1 col-sm-6">
     <input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="delete_file_row(<?php echo $value['id']; ?>);" value="<?php echo $this->lang->line('delete'); ?>">
     </div>
   </div>

   <div class="modal fade" id="SendMailApplicantModalContent_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalContentLabel">Send New Mail</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
            <form action="<?php echo base_url(); ?>case_file_upload/email_send" method="post">
             <div class="modal-body">
                    <input type="hidden" name="id"  value="<?php echo $value['id']; ?>"/>
                    <input type="hidden" name="file_path"  value="<?php echo $value['file_path']; ?>"/>
                    <div class="form-group">
                        <label for="recipient-name"
                            class="col-form-label">Dear<span style="color:red;"> * </span>  </label>
                            <input type="text" class="form-control" required  name="dear" value="<?php if(empty($mail_config->dear)) {echo $value['first_name'];}else{echo $value['dear'];} ?>" id="dear">
                    </div>
                   <div class="form-group">
                       <label for="recipient-name"
                           class="col-form-label">Recipient<span style="color:red;"> * </span>
                            <?php if(empty($value['email'])){?>
                                   <span><a target="_blank" href="<?php echo base_url(); ?>clients/edit/<?php echo $value['client_id']; ?>"> Go to client email setup </a></span>
                         <?php } ?>
                         </label>
                           <input type="text" class="form-control" required  name="recipient" value="<?php echo $value['email']; ?>" id="recipient">
                   </div>
                     <div class="form-group">
                         <label for="header-name"
                             class="col-form-label">Subject<span style="color:red;"> *  </span></label>
                         <input type="text" class="form-control" required name="subject" id="subject" value="<?php echo $mail_config->subject; ?>">
                     </div>
                     <div class="form-group">
                         <label for="recipient-name"
                             class="col-form-label">Message<span style="color:red;"> *  </span></label>
                             <textarea style="height: 120px;" required class="form-control" name="message" id="message">
                               <?php echo $mail_config->message_body; ?>
                             </textarea>
                         <!-- <input type="text" class="form-control" name="recipientList" id="recipientList"> -->
                     </div>
             </div>
             <div class="modal-footer">
                <button type="submit"  class="btn btn-primary mr-auto">Send Mail</button>

                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div>
          </form>
         </div>
     </div>
   </div>



      <?php $i++; ?>
   <?php endforeach; ?>
   </div>

   <!-- <button type="button"  id="send_lead_email" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
    data-target="#SendMailApplicantModalContent" data-whatever="">Send Email
    </button> -->
