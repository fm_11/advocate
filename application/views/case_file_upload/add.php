<script type="text/javascript">

///file upload
function add_file_row() {

  var language=document.getElementById('language').value;
  var file_name="",label_name="";
    file_name="File Upload";
    label_name="File Name";
    if(language=="bangla")
    {
      file_name="ফাইল আপলোড";
      label_name="ফাইল নাম";
    }


  debugger;
  var table = document.getElementById("file_upload_table");
  var table_len = Number(document.getElementById('file_upload_num_of_row').value);
      var row = table.insertRow(table_len).outerHTML = "<tr id='file_row_" + table_len + "' class='tr_border'><td> <label for='inputEmail4'>"+file_name+" <span class='required_label'>*</span></label> <input type='file' autocomplete='off'  class='form-control' id='file_name_" + table_len + "'  name='file_name_" + table_len + "' required value=''></td><td id='delete_file_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_fil_row(" + table_len + ")'></td></tr>";
  //alert(row);
  if(table_len > 1){
         document.getElementById("delete_file_section_" + (table_len - 1)).innerHTML = "";
      }
      document.getElementById('file_upload_num_of_row').value = table_len + 1;
}
function delete_fil_row(row_no) {
    var no = Number(row_no);
    document.getElementById("file_row_" + no + "").outerHTML = "";
    if(no != 1){
        document.getElementById("delete_file_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_fil_row(" + (no - 1) + ")'>";
    }
    document.getElementById('file_upload_num_of_row').value = no;
}

</script>
<style>
.fieldset_div{
  border: 3px solid #efdfdf;
  padding: 20px;
}
.tr_border{
  border-top: 2px solid #fff;
}
</style>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>case_file_upload/add" class="cmxform" enctype="multipart/form-data" method="post">
  <input type="hidden" id="language" value="<?php echo $language; ?>"/>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?><span class="required_label">*</span></label>
      <select   name="case_master_id" class="js-example-basic-single w-100" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($case_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>
  </div>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <table class="table" id="file_upload_table" style="border-top: 2px solid #fff;">
           <tr class="tr_border">
               <td>
                 <label for="inputEmail4"><?php echo $this->lang->line('file').' '.$this->lang->line('upload'); ?><span class="required_label">*</span></label>
                 <input type="file" autocomplete="off"  class="form-control" id="file_name_0"  name="file_name_0" required>
               </td>
               <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_file_row();" value="<?php echo $this->lang->line('add_more'); ?>"></td>
           </tr>
       </table>
         <input type="hidden" id="file_upload_num_of_row" name="file_upload_num_of_row" value="1">
      </div>
  </fieldset>
  <br><br>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
