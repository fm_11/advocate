<script type="text/javascript">
    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_section_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>ad_case_sub_types/updateCaseSubTypeStatus?id=" + id + '&&is_active=' + status, true);
        xmlhttp.send();
    }
</script>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('case_type')?></th>
        <th scope="col"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?></th>
        <th scope="col"><?php echo $this->lang->line('bn_name')?></th>
        <th scope="col"><?php echo $this->lang->line('status')?></th>
        <th scope="col"><?php echo $this->lang->line('actions')?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($case_subtype as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td><?php echo $i; ?></td>
            <td><?php
              if($language=="english")
              {
                echo $row['type'];
              }else{
                echo $row['bn_type'];
              }
            ?>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['bn_name']; ?></td>

            <td id="status_section_<?php echo $row['id']; ?>">
                <?php
                if ($row['is_active'] == 1) {
                    ?>
                    <a class="deleteTag" title="Active" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)"><i class="ti-check-box"></i></a>
                <?php
                } else {
                    ?>
                    <a class="deleteTag" title="Inactive" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)"><i class="ti-na"></i></a>
                <?php
                }
                ?>
            </td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>ad_case_sub_types/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>ad_case_sub_types/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
