
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>ad_case_sub_types/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $case_subtype[0]['name']; ?>" required="1"/>
    </div>
  </div>
  <div class="form-group col-md-12">
    <label><?php echo $this->lang->line('bangla').' '.$this->lang->line('name'); ?><span style="color:red">*</span></label>
    <input type="text" autocomplete="off"  class="form-control" name="bn_name" value="<?php echo $case_subtype[0]['bn_name']; ?>" required="1"/>
  </div>
  <div class="form-group col-md-6">
    <label><?php echo $this->lang->line('case_type'); ?>&nbsp;<span class="required_label">*</span></label>
    <select class="js-example-basic-single w-100" name="case_type_id" required="1">
      <option value="">-- <?php echo $this->lang->line('please_select');?> --</option>
      <?php
      if (count($case_types)) {
        foreach ($case_types as $list) {
          ?>
          <option value="<?php echo $list['id']; ?>"<?php if (isset($case_subtype[0]['case_type_id'])) {
           if ($list['id'] == $case_subtype[0]['case_type_id']) {
             echo 'selected';
           }
         } ?>><?php echo $list['name']; ?></option>
          <?php
        }
      } ?>
    </select>
  </div>
    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $case_subtype[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
