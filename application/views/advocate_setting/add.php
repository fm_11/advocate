<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>advocate_setting/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('name'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mobile" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('day_of_advance_message'); ?><span style="color:red">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" min="0" name="advance_day"  required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('designation'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="designation" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('address'); ?><span style="color:red">*</span></label>
      <textarea class="form-control" name="address" required="1" style="height: 100px;"></textarea>

    </div>
  </div>
    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
