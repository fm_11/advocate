<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
			text-align: center !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td colspan="3"  class="h_td" style="text-align:center; line-height:1.5;">
				<b><?php echo $HeaderInfo['school_name']; ?></b><br>
				<?php echo $HeaderInfo['address']; ?><br>
				<?php echo $title; ?> of <b>(<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)</b>
			</td>
		</tr>

		<tr>
			<td scope="col" align="center" class="h_td"><b>Date</b></td>
			<td scope="col"  align="center" class="h_td"><b>Transaction ID</b></td>
			<td scope="col" align="center"   class="h_td" align="center">
				<b>Balance</b>
			</td>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach ($recharge_histories as $rt_row):
				?>
				<tr>
					<td align="center">
						<?php echo date("d-m-Y", strtotime($rt_row['added_on'])); ?>
					</td>
					<td align="center">
						<?php
						echo $rt_row['transaction_id'];
						?>
					</td>
					<td align="center">
						<?php
						echo $rt_row['quantity'];
						?>
					</td>
				</tr>
				<?php
			 endforeach; ?>

		</tbody>
	</table>
</div>
