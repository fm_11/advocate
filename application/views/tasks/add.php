<script type="text/javascript">
    function getRelatedTo(related_to){
       //alert(related_to);
        // document.getElementById("district_id").innerHTML = "<option><?php echo $this->lang->line('case'); ?></option>";
        // document.getElementById("upazilas_id").innerHTML = "<option><?php echo $this->lang->line('other'); ?></option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if(related_to=='1')
        {
          // document.getElementById("case").show();
          document.getElementById("case_ids").show();
        }
        // xmlhttp.onreadystatechange = function()
        // {
        //     if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        //     {
        //         document.getElementById("district_id").innerHTML = xmlhttp.responseText;
        //     }
        // }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_district_by_country?country_id=" + country_id, true);
        xmlhttp.send();
    }



</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>tasks/add" class="cmxform" method="post">
  <fieldset>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('subject')?></label>
          <input type="text" autocomplete="off"  class="form-control" id="subject"  name="subject" value="">
        </div>

        <div class="form-group col-md-4">
        <label for="inputEmail4"><?php echo $this->lang->line('start_date')?><span class="required_label">*</span></label>
        <div class="input-group date">

        <input type="text" autocomplete="off" name="start_date" value="" class="form-control" id="start_date">
        <span class="input-group-text input-group-append input-group-addon">
            <i class="simple-icon-calendar"></i>
        </span>
      </div>
      </div>

      <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('deadline')?><span class="required_label">*</span></label>
      <div class="input-group date">

      <input type="text" autocomplete="off" name="deadline_date" value="" class="form-control" id="deadline_date">
      <span class="input-group-text input-group-append input-group-addon">
          <i class="simple-icon-calendar"></i>
      </span>
    </div>
    </div>
      </div>

        <div class="form-row">
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('status'); ?><span class="required_label">*</span></label>
             <select name="status_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($status as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('priority'); ?><span class="required_label">*</span></label>
             <select name="priority_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($priority as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('assign_to'); ?><span class="required_label">*</span></label>
             <select class="js-example-basic-single w-100" multiple name="client_user_id[]" id="client_user_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($client_user as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'].' '.$row['last_name']; ?></option>
               <?php } ?>
             </select>
           </div>
         </div>
         <div class="form-row">
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('related').' '.$this->lang->line('to'); ?><span class="required_label">*</span></label>
             <select onchange="getRelatedTo(this.value)" name="related_to" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <option value="1"><?php echo $this->lang->line('case'); ?></option>
               <option value="2"><?php echo $this->lang->line('other'); ?></option>
             </select>
           </div>

           <div class="form-group col-md-4" id="case_ids" hidden>
             <label for="inputEmail4"><?php echo $this->lang->line('case');?><span class="required_label">*</span></label>
             <select name="case_id" id="case_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <option value="11"><?php echo $this->lang->line('student'); ?></option>
               <option value="21"><?php echo $this->lang->line('student'); ?></option>
               <!-- <?php foreach ($case as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?> -->
             </select>
           </div>

          </div>
          <div class="form=-row">
            <div class="form-row">
               <div class="form-group col-md-12">
                 <label for="inputEmail4"><?php echo $this->lang->line('description'); ?></label>
                 <textarea class="form-control" id="description" autocomplete="off" name="description"></textarea>
             </div>
             </div>
          </div>

  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
