<script type="text/javascript">
    function getDistrictByCountryId(country_id){
      //  alert(country_id);
        document.getElementById("district_id").innerHTML = "<option>--Please Select---</option>";
        document.getElementById("upazilas_id").innerHTML = "<option>--Please Select---</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("district_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_district_by_country?country_id=" + country_id, true);
        xmlhttp.send();
    }

</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>tasks/edit/<?php echo $row_data->id; ?>" class="cmxform" method="post">
	<fieldset>
    <!-- <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('country'); ?><span class="required_label">*</span></label>
      <select onchange="getDistrictByCountryId(this.value)" name="country_id" class="js-example-basic-single w-100" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($countries as $row) { ?>
          <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->country_id)) {
           if ($row['id'] == $row_data->country_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div> -->
    <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('subject')?></label>
         <input type="text" autocomplete="off"  class="form-control" id="subject"  name="subject" value="<?php echo $row_data->subject; ?>">
       </div>

       <div class="form-group col-md-4">
       <label for="inputEmail4"><?php echo $this->lang->line('start_date')?><span class="required_label">*</span></label>
       <div class="input-group date">

       <input type="text" autocomplete="off" name="start_date" value="<?php echo date("d-m-Y", strtotime($row_data->start_date)); ?>" class="form-control" id="start_date">
       <span class="input-group-text input-group-append input-group-addon">
           <i class="simple-icon-calendar"></i>
       </span>
     </div>
     </div>

     <div class="form-group col-md-4">
     <label for="inputEmail4"><?php echo $this->lang->line('deadline')?><span class="required_label">*</span></label>
     <div class="input-group date">

     <input type="text" autocomplete="off" name="deadline_date" value="<?php echo date("d-m-Y", strtotime($row_data->deadline_date)); ?>" class="form-control" id="deadline_date">
     <span class="input-group-text input-group-append input-group-addon">
         <i class="simple-icon-calendar"></i>
     </span>
   </div>
   </div>
     </div>

       <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('status'); ?><span class="required_label">*</span></label>
            <select name="status_id" class="js-example-basic-single w-100" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php foreach ($status as $row) { ?>
                <option value="<?php echo $row['id']; ?>"  <?php if (isset($row_data->status_id)) {
                 if ($row['id'] == $row_data->status_id) {
                   echo 'selected';
                 }
               } ?>><?php echo $row['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('priority'); ?><span class="required_label">*</span></label>
            <select name="priority_id" class="js-example-basic-single w-100" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php foreach ($priority as $row) { ?>
                <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->priority_id)) {
                 if ($row['id'] == $row_data->priority_id) {
                   echo 'selected';
                 }
               } ?>><?php echo $row['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('assign_to'); ?><span class="required_label">*</span></label>
            <select class="js-example-basic-single w-100" multiple name="client_user_id[]" id="client_user_id" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php foreach ($client_user as $row) { ?>
                <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->client_user_id)) {
                 if ($row['id'] == $row_data->client_user_id) {
                   echo 'selected';
                 }
               } ?>><?php echo $row['first_name'].' '.$row['last_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('related').' '.$this->lang->line('to'); ?><span class="required_label">*</span></label>
            <select onchange="getRelatedTo(this.value)" value="<?php echo $row_data->related_to; ?>" name="related_to" class="js-example-basic-single w-100" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <option value="1"><?php echo $this->lang->line('other'); ?></option>
              <option value="2"><?php echo $this->lang->line('other'); ?></option>
            </select>
          </div>

          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('case');?><span class="required_label">*</span></label>
            <select name="case_id" id="case_id" class="js-example-basic-single w-100" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <option value="11"><?php echo $this->lang->line('student'); ?></option>
              <option value="21"><?php echo $this->lang->line('student'); ?></option>
              <!-- <?php foreach ($case as $row) { ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
              <?php } ?> -->
            </select>
          </div>

         </div>
         <div class="form=-row">
           <div class="form-row">
              <div class="form-group col-md-12">
                <label for="inputEmail4"><?php echo $this->lang->line('description'); ?></label>
                <input type="text" autocomplete="off"name="description" value="<?php echo $row_data->description; ?>" class="form-control" id="description">
            </div>
            </div>
         </div>
  </fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
