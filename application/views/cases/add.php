<script type="text/javascript">
function getDisTrictWiseThanaOrCourt(district_id,type){
   if(type=='T')
   {
     document.getElementById("gr_cr_upazilla_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }else if (type=='C') {
     document.getElementById("court_type_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
     document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }else if (type=='CT') {
     document.getElementById("client_upazilla_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }

    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
          if(type=='T')
          {
            document.getElementById("gr_cr_upazilla_id").innerHTML = xmlhttp.responseText;
          }else if (type=='C') {
            document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
          }else if (type=='CT') {//client thana
            document.getElementById("client_upazilla_id").innerHTML = xmlhttp.responseText;
          }

        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
    xmlhttp.send();
}


function getClientNameByClientId(client_id){
  //  alert(country_id);
    document.getElementById("parties_name").value = "";
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("parties_name").value = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_client_name_by_client_id?client_id=" + client_id, true);
    xmlhttp.send();
}

function getCaseSubTypeByCaseType(CaseType_id){
  //  alert(country_id);
    document.getElementById("case_sub_type_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {

            document.getElementById("case_sub_type_id").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_case_sub_type_by_type_id?type_id=" + CaseType_id, true);
    xmlhttp.send();
}
function getCRorGR(sel){
  var case_sub_type=sel.options[sel.selectedIndex].text;

    if(case_sub_type=="-- অনুগ্রহ করে নির্বাচন করুন --")
    {
      // document.getElementById("gr_div").style.display="none";<?php echo $this->lang->line('numbers'); ?>;
      document.getElementById("cr_div").style.display="none";

    }else {

     document.getElementById("case_number_label").innerHTML=case_sub_type+" <?php echo $this->lang->line('numbers'); ?>";
     document.getElementById("case_date_label").innerHTML=case_sub_type+" <?php echo $this->lang->line('date'); ?>";
      document.getElementById("cr_div").style.display="block";
    }
}

function getCourtTypeByCourtId(court_id){
  //  alert(country_id);
    document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
    var district_id=  document.getElementById("district_id").value;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("court_id").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
    xmlhttp.send();
}
function delete_row(row_no) {
    var no = Number(row_no);
    document.getElementById("row_" + no + "").outerHTML = "";
    if(no != 1){
        document.getElementById("delete_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_row(" + (no - 1) + ")'>";
    }
    document.getElementById('num_of_row').value = no;
}

function add_row() {
  var radios = document.querySelectorAll('input[type="radio"]:checked');
  var value = radios.length>0? radios[0].value: null;
  var language=document.getElementById('language').value;
  var petination_text="";
  if(value==1)
  {
    petination_text="Respondent";
    if(language=="bangla")
    {
      petination_text="বিবাদী পক্ষের ";
    }

  }else{
    petination_text="Petitioner";
    if(language=="bangla")
    {
      petination_text="বাদী পক্ষের";
    }

  }
  var table = document.getElementById("my_data_table");
  var table_len = Number(document.getElementById('num_of_row').value);
      var row = table.insertRow(table_len).outerHTML = "<tr id='row_" + table_len + "' class='tr_border'><td><label for='inputEmail4' class='petitioner_name'>"+petination_text+"<?php if($language=="bangla"){echo " নাম";}else {echo " Name";}?><span class='required_label'>*</span></label><input type='text' autocomplete='off'  class='form-control' id='petitioner_name_" + table_len + "'  name='petitioner_name_" + table_len + "' required value=''></td><td> <label for='inputEmail4' class='petitioner_advocate'>"+petination_text+"<?php if($language=="bangla"){echo " উকিল";}else {echo " Advocate";}?> <span class='required_label'>*</span></label> <input type='text' autocomplete='off'  class='form-control' id='advocate_" + table_len + "'  name='advocate_" + table_len + "' required value=''></td><td id='delete_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_row(" + table_len + ")'></td></tr>";
  //alert(row);
  if(table_len > 1){
         document.getElementById("delete_section_" + (table_len - 1)).innerHTML = "";
      }
      document.getElementById('num_of_row').value = table_len + 1;
}
function petitioner_check() {
 var petitioner = document.getElementsByClassName("petitioner_name");
 var i;
   var language=document.getElementById('language').value;
 for (i = 0; i < petitioner.length; i++) {
   if(language=="bangla")
   {
      petitioner[i].innerHTML = "বিবাদী পক্ষের নাম<span class='required_label'>*</span>";
   }else{
      petitioner[i].innerHTML = "Respondent Name<span class='required_label'>*</span>";
   }

 }
 var advocate = document.getElementsByClassName("petitioner_advocate");
 var j;
 for (j = 0; j < advocate.length; j++) {
   if(language=="bangla")
   {
     advocate[j].innerHTML = "বিবাদী পক্ষের উকিল<span class='required_label'>*</span>";
   }else{
     advocate[j].innerHTML = "Respondent Advocate<span class='required_label'>*</span>";
   }

 }
}
function respondent_check() {
  var petitioner = document.getElementsByClassName("petitioner_name");
  var i;
    var language=document.getElementById('language').value;
  for (i = 0; i < petitioner.length; i++) {
    if(language=="bangla")
    {
      petitioner[i].innerHTML = "বাদী পক্ষের নাম<span class='required_label'>*</span>";
    }else{
      petitioner[i].innerHTML = "Petitioner Name<span class='required_label'>*</span>";
    }

  }
  var advocate = document.getElementsByClassName("petitioner_advocate");
  var j;
  for (j = 0; j < advocate.length; j++) {
    if(language=="bangla")
    {
      advocate[j].innerHTML = "বাদী পক্ষের উকিল<span class='required_label'>*</span>";
    }else{
      advocate[j].innerHTML = "Petitioner Advocate<span class='required_label'>*</span>";
    }

  }
}
///file upload
function add_file_row() {

  var language=document.getElementById('language').value;
  var file_name="",label_name="";
    file_name="File Upload";
    label_name="File Name";
    if(language=="bangla")
    {
      file_name="ফাইল আপলোড";
      label_name="ফাইল নাম";
    }


  var table = document.getElementById("file_upload_table");
  var table_len = Number(document.getElementById('file_upload_num_of_row').value);
      var row = table.insertRow(table_len).outerHTML = "<tr id='file_row_" + table_len + "' class='tr_border'><td><label for='inputEmail4'>"+label_name+"<span class='required_label'>*</span></label><input type='text' autocomplete='off'  class='form-control' id='file_label_name_" + table_len + "'  name='file_label_name_" + table_len + "' required value=''></td><td> <label for='inputEmail4'>"+file_name+" <span class='required_label'>*</span></label> <input type='file' autocomplete='off'  class='form-control' id='file_name_" + table_len + "'  name='file_name_" + table_len + "' required value=''></td><td id='delete_file_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_fil_row(" + table_len + ")'></td></tr>";
  //alert(row);
  if(table_len > 1){
         document.getElementById("delete_file_section_" + (table_len - 1)).innerHTML = "";
      }
      document.getElementById('file_upload_num_of_row').value = table_len + 1;
}
function delete_fil_row(row_no) {
    var no = Number(row_no);
    document.getElementById("file_row_" + no + "").outerHTML = "";
    if(no != 1){
        document.getElementById("delete_file_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_fil_row(" + (no - 1) + ")'>";
    }
    document.getElementById('file_upload_num_of_row').value = no;
}
function click_client_enty_from(type){

//alert(type);
      document.getElementById("mobile").value="";
      document.getElementById("first_name").value="";
    if(type=='y')
    {

      document.getElementById("div_client_id").style.display="none";
      document.getElementById("div_client_district_id").style.display = 'block';
      document.getElementById("div_client_upazilla_id").style.display = 'block';
      document.getElementById("mobile").required = true;
      document.getElementById("first_name").required = true;
      document.getElementById("client_id").required = false;
      document.getElementById("client_district_id").required = true;
      document.getElementById("client_upazilla_id").required = true;

      var class_client = document.getElementsByClassName("class_client_enty_from");
        var i;
        for (i = 0; i < class_client.length; i++) {
            class_client[i].style.display = 'block';
        }

    }else {

      document.getElementById("div_client_id").style.display="block";
      document.getElementById("div_client_district_id").style.display = 'none';
      document.getElementById("div_client_upazilla_id").style.display = 'none';
      var class_client = document.getElementsByClassName("class_client_enty_from");
        var i;
        for (i = 0; i < class_client.length; i++) {
            class_client[i].style.display = 'none';
        }
        document.getElementById("client_id").required = true;
        document.getElementById("mobile").required = false;
        document.getElementById("first_name").required = false;
        document.getElementById("client_district_id").required = false;
        document.getElementById("client_upazilla_id").required = false;

    }
}
function divhidefunction(){
  document.getElementById("div_client_district_id").style.display = 'none';
  document.getElementById("div_client_upazilla_id").style.display = 'none';
}
window.addEventListener('load', function () {
    divhidefunction();
  //document.getElementById("txt_student_code").focus();
})
</script>
<style>
.fieldset_div{
  border: 3px solid #efdfdf;
  padding: 20px;
}
.tr_border{
  border-top: 2px solid #fff;
}
.class_client_enty_from{
  display: none;
}
</style>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>cases/add" class="cmxform" enctype="multipart/form-data" method="post">
  <h2> <?php echo $this->lang->line('client_details'); ?></h2>
  <input type="hidden" id="language" value="<?php echo $language; ?>"/>
  <input type="hidden" autocomplete="off"  class="form-control" id="parties_name"  name="parties_name" required value="">
  <fieldset class="fieldset_div">
    <div class="form-row">
      <div class="form-group col-md-3">
         <label for="inputEmail4"><?php echo $this->lang->line('client_enty_from'); ?>&nbsp;<span class="required_label">*</span></label><br>
        <input type="radio"  name="client_enty_from" onclick="click_client_enty_from('y');" value="1" ><?php echo $this->lang->line('yes'); ?>
        <input type="radio"  name="client_enty_from" onclick="click_client_enty_from('n');" value="0" checked> <?php echo $this->lang->line('no'); ?>
      </div>
       <div class="form-group col-md-3 class_client_enty_from">
         <label for="inputEmail4"><?php echo $this->lang->line('bangla').' '.$this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="first_name"    name="first_name" value="">
       </div>
       <div class="form-group col-md-3 class_client_enty_from">
         <label for="inputEmail4"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="last_name"    name="last_name" value="">
       </div>
       <div class="form-group col-md-3 class_client_enty_from">
         <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?><span class="required_label">*</span></label>
         <input type="number" autocomplete="off"  class="form-control" id="mobile" maxlength="11" minlength="11"  name="mobile"  value="">
       </div>

     </div>
     <div class="form-row">
       <div class="form-group col-md-3" id="div_client_district_id">
         <label for="inputEmail4"><?php echo $this->lang->line('client').' '.$this->lang->line('district'); ?></label><span class="required_label">*</span>
         <select onchange="getDisTrictWiseThanaOrCourt(this.value,'CT')" name="client_district_id" id="client_district_id" class="js-example-basic-single w-100">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($all_districts as $row) { ?>
             <option value="<?php  echo $row['id']; ?>"<?php if (isset($user_district_id)) {
              if ($row['id'] == $user_district_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-3" id="div_client_upazilla_id">
         <label for="inputEmail4"><?php echo $this->lang->line('client').' '.$this->lang->line('police').' '.$this->lang->line('station'); ?></label><span class="required_label">*</span>
          <select   name="client_upazilla_id" id="client_upazilla_id" class="js-example-basic-single w-100" >
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($fir_upazilla_id as $row) { ?>
             <option value="<?php  echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>

       <div class="form-group col-md-3 class_client_enty_from">
         <label for="inputEmail4"><?php echo $this->lang->line('reference').' '.$this->lang->line('name'); ?></label>
         <input type="text" autocomplete="off"  class="form-control" id="reference_name"    name="reference_name" value="">
       </div>
       <div class="form-group col-md-3 class_client_enty_from">
         <label for="inputEmail4"><?php echo $this->lang->line('reference').' '.$this->lang->line('mobile'); ?></label>
         <input type="number" autocomplete="off"  class="form-control" id="reference_mobile" maxlength="11" minlength="11"  name="reference_mobile"  value="">
       </div>

      </div>

     <div class="form-row">

       <div class="form-group col-md-4" id="div_client_id">
         <label for="inputEmail4"><?php echo $this->lang->line('client_name'); ?><span class="required_label">*</span></label>
         <select onchange="getClientNameByClientId(this.value)"  name="client_id" id="client_id" class="js-example-basic-single w-100" required="true">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($clients as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('advocate').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
         <select   name="advocate_user_id" id="advocate_user_id" class="js-example-basic-single w-100" required>
           <option  value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($client_user as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($client_user_id)) {
              if ($row['id'] == $client_user_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
        <div class="form-group col-md-4" >
         <br>
          <input type="radio"  name="is_petitioner" onclick="petitioner_check();" value="1" > <?php echo $this->lang->line('petitioner'); ?><br>
          <input type="radio"  name="is_petitioner" onclick="respondent_check();" value="0" checked> <?php echo $this->lang->line('respondent'); ?>
        </div>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('section_of_the_case'); ?></label>
            <textarea class="form-control" id="section_of_the_case" autocomplete="off" name="section_of_the_case"  value=""></textarea>
        </div>
      </div>
      <div class="form-row">
        <table class="" id="my_data_table" style="border-top: 2px solid #fff;width: 100%;">
            <tr class="tr_border">
                <td>
                 <label for="inputEmail4" class="petitioner_name"><?php echo $this->lang->line('petitioner_name'); ?><span class="required_label">*</span></label>
                  <input type="text" autocomplete="off"  class="form-control" id="petitioner_name_0"  name="petitioner_name_0" required value="">
                </td>
                <td>
                  <label for="inputEmail4" class="petitioner_advocate"><?php echo $this->lang->line('petitioner_advocate'); ?></label>
                  <input type="text" autocomplete="off"  class="form-control" id="advocate_0"  name="advocate_0" value="">
                </td>
                <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_row();" value="<?php echo $this->lang->line('add_more'); ?>"></td>
            </tr>
        </table>
          <input type="hidden" id="num_of_row" name="num_of_row" value="1">
       </div>
  </fieldset>
  <br><br>
  <h2><?php echo $this->lang->line('court_details'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('district'); ?></label></label><span class="required_label">*</span>
         <select onchange="getDisTrictWiseThanaOrCourt(this.value,'C')"  name="district_id" id="district_id" class="js-example-basic-single w-100" required>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($district_id as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($user_district_id)) {
              if ($row['id'] == $user_district_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('court_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCourtTypeByCourtId(this.value)"  name="court_type_id" id="court_type_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($courts_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('court_sub_type'); ?><span class="required_label">*</span></label>
         <select   name="court_id" id="court_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($courts as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCaseSubTypeByCaseType(this.value)"  name="case_type_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_sub_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCRorGR(this)"   name="case_sub_type_id" id="case_sub_type_id" class="js-example-basic-single w-100" required>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_sub_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_assignment'); ?><span class="required_label">*</span></label>
         <select  name="case_status_id" id="case_status_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_status as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div id="cr_div" style="display:none;">
       <div class="form-row" >
         <div class="form-group col-md-4">
           <label for="inputEmail4" id="case_number_label"><?php echo $this->lang->line('cr_case_no').' '.$this->lang->line('numbers'); ?></label><span class="required_label">*</span>
           <input type="text" autocomplete="off"  class="form-control" id="case_no"  name="case_no" value="" required>
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4" id="case_date_label"><?php echo $this->lang->line('filling_date'); ?></label><span class="required_label">*</span>
           <div class="input-group date">
                   <input type="text" autocomplete="off"  name="registration_date" id="registration_date"  class="form-control" required>
                   <span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
           </div>
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4" id="case_remarks_label"><?php echo $this->lang->line('description'); ?></label>
           <input type="text" autocomplete="off"  class="form-control" id="description"  name="description" value="">
         </div>
        </div>
     </div>
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('courts').' '.$this->lang->line('district'); ?></label><span class="required_label">*</span>
         <select onchange="getDisTrictWiseThanaOrCourt(this.value,'T')"  name="gr_cr_district_id" id="gr_cr_district_id" class="js-example-basic-single w-100" required>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($fir_district_id as $row) { ?>
             <option value="<?php  echo $row['id']; ?>"<?php if (isset($user_district_id)) {
              if ($row['id'] == $user_district_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('courts').' '.$this->lang->line('police').' '.$this->lang->line('station'); ?></label><span class="required_label">*</span>
          <select   name="gr_cr_upazilla_id" id="gr_cr_upazilla_id" class="js-example-basic-single w-100" required>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($fir_upazilla_id as $row) { ?>
             <option value="<?php  echo $row['id']; ?>"<?php if (isset($upazilla_id)) {
              if ($row['id'] == $upazilla_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
        <br>  <label for="inputEmail4"><?php echo $this->lang->line('priority_of_the_case'); ?></label> <br>
          <input type="radio"  name="priority" value="H" ><?php echo $this->lang->line('high'); ?>
          <input type="radio"  name="priority" value="M" checked><?php echo $this->lang->line('medium'); ?>
          <input type="radio"  name="priority" value="L"> <?php echo $this->lang->line('low'); ?>
        </div>
      </div>



      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('judge_type'); ?><span class="required_label">*</span></label>
          <select   name="judge_type_id" id="judge_type_id" class="js-example-basic-single w-100" required="required">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php $i=0; foreach ($judges as $row) { ?>
              <option value="<?php echo $row['id']; ?>"<?php if ($i == 0) {
                $i++;
                 echo 'selected';
               }?>><?php echo $row['name']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('judge_name'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="judge_name"  name="judge_name" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('remarks'); ?></label>
          <textarea class="form-control" id="remarks" autocomplete="off" name="remarks"  value=""></textarea>
        </div>
       </div>
       <div class="form-row">
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('file_colour'); ?></label>
           <select   name="file_colour" id="file_colour" class="js-example-basic-single w-100">
             <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
             <?php foreach ($file_colour as $row) { ?>
               <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
             <?php } ?>
           </select>
         </div>
      </div>
  </fieldset>
  <!-- <br><br>
  <h2><?php echo $this->lang->line('case_details'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('registration_date'); ?><span class="required_label">*</span></label>
         <div class="input-group date">
                 <input type="text" autocomplete="off"  name="registration_date" id="registration_date" required class="form-control" >
                 <span class="input-group-text input-group-append input-group-addon">
                     <i class="simple-icon-calendar"></i>
                 </span>
         </div>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?><span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="case_no"  name="case_no" required value="">
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('parties_name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="parties_name"  name="parties_name" required value="">
       </div>

     </div>
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_assignment'); ?><span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="reason_for_assignment"  name="reason_for_assignment" required value="">
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('description'); ?></label>
         <textarea class="form-control" id="description" autocomplete="off" name="description"  value=""></textarea>
       </div>

       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('first_hearing_date'); ?><span class="required_label">*</span></label>
         <div class="input-group date">
                 <input type="text" autocomplete="off"  name="first_hearing_date" id="first_hearing_date" required class="form-control" >
                 <span class="input-group-text input-group-append input-group-addon">
                     <i class="simple-icon-calendar"></i>
                 </span>
         </div>
       </div>


     </div>
     <div class="form-row">

       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_next_date'); ?><span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="reason_for_next_date"  name="reason_for_next_date" required value="">
       </div>


     </div>

         <div class="form-row">


        </div>
  </fieldset> -->
  <br><br>
  <h2><?php echo $this->lang->line('task_assign'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-12">
         <label for="inputEmail4"><?php echo $this->lang->line('users_name'); ?></label>
         <select  multiple="multiple" name="client_user_id[]" id="client_user_id" class="js-example-basic-single w-100" >
           <!-- <option  value="">-- <?php echo $this->lang->line('please_select'); ?> --</option> -->
           <?php foreach ($client_user as $row) { ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>

  </fieldset>
  <br><br>
  <!-- <h2><?php echo $this->lang->line('file_upload'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <table class="table" id="file_upload_table" style="border-top: 2px solid #fff;">
           <tr class="tr_border">
               <td>
                <label for="inputEmail4"><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?></label>
                 <input type="text" autocomplete="off"  class="form-control" id="file_label_name_0"  name="file_label_name_0">
               </td>
               <td>
                 <label for="inputEmail4"><?php echo $this->lang->line('file').' '.$this->lang->line('upload'); ?></label>
                 <input type="file" autocomplete="off"  class="form-control" id="file_name_0"  name="file_name_0">
               </td>
               <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_file_row();" value="<?php echo $this->lang->line('add_more'); ?>"></td>
           </tr>
       </table>
         <input type="hidden" id="file_upload_num_of_row" name="file_upload_num_of_row" value="1">
      </div>
  </fieldset>
  <br><br> -->
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
