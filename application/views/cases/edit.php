<script type="text/javascript">
function getDisTrictWiseThanaOrCourt(district_id,type){
   if(type=='T')
   {
     document.getElementById("gr_cr_upazilla_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }else if (type=='C') {
     document.getElementById("court_type_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
     document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }

    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
          if(type=='T')
          {
            document.getElementById("gr_cr_upazilla_id").innerHTML = xmlhttp.responseText;
          }else if (type=='C') {
            document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
          }

        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
    xmlhttp.send();
}


function getClientNameByClientId(sel){

    var client=sel.options[sel.selectedIndex].text;
    var nameArr = client.split('-');
    debugger;
    var client_id=sel.options[sel.selectedIndex].value;
  //  alert(country_id);
    document.getElementById("parties_name").value = "";
    document.getElementById("edit_client_name").value = nameArr[0];
    document.getElementById("edit_client_mobile").value = nameArr[1];
    document.getElementById("edit_client_id").value = client_id;

    // if (window.XMLHttpRequest)
    // {
    //     xmlhttp = new XMLHttpRequest();
    // }
    // else
    // {
    //     xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    // }
    // xmlhttp.onreadystatechange = function()
    // {
    //     if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    //     {
    //       alert(xmlhttp.responseText);
            document.getElementById("parties_name").value = nameArr[0];
    //     }
    // }
    // xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_client_name_by_client_id?client_id=" + client_id, true);
    // xmlhttp.send();
}


function getCaseSubTypeByCaseType(CaseType_id){
  //  alert(country_id);
    document.getElementById("case_sub_type_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {

            document.getElementById("case_sub_type_id").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_case_sub_type_by_type_id?type_id=" + CaseType_id, true);
    xmlhttp.send();
}
function getCRorGR(sel){
  debugger;
  var case_sub_type=sel.options[sel.selectedIndex].text;

    if(case_sub_type=="-- অনুগ্রহ করে নির্বাচন করুন --")
    {
      // document.getElementById("gr_div").style.display="none";<?php echo $this->lang->line('numbers'); ?>;
      document.getElementById("cr_div").style.display="none";

    }else {

     document.getElementById("case_number_label").innerHTML=case_sub_type+" <?php echo $this->lang->line('numbers'); ?>";
     document.getElementById("case_date_label").innerHTML=case_sub_type+" <?php echo $this->lang->line('date'); ?>";
      document.getElementById("cr_div").style.display="block";
    }
}

function getCourtTypeByCourtId(court_id){
  //  alert(country_id);
    document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
    var district_id=  document.getElementById("district_id").value;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("court_id").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
    xmlhttp.send();
}
function delete_row(row_no) {
    var no = Number(row_no);
    document.getElementById("row_" + no + "").outerHTML = "";
    if(no != 1){
        document.getElementById("delete_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_row(" + (no - 1) + ")'>";
    }
    document.getElementById('num_of_row').value = no;
}

function add_row() {
  var radios = document.querySelectorAll('input[type="radio"]:checked');
  var value = radios.length>0? radios[0].value: null;
  var language=document.getElementById('language').value;
  var petination_text="";
  if(value==1)
  {
    petination_text="Respondent";
    if(language=="bangla")
    {
      petination_text="বিবাদী পক্ষের ";
    }

  }else{
    petination_text="Petitioner";
    if(language=="bangla")
    {
      petination_text="বাদী পক্ষের";
    }

  }
  debugger;
  var table = document.getElementById("my_data_table");
  var table_len = Number(document.getElementById('num_of_row').value);
      var row = table.insertRow(table_len).outerHTML = "<tr id='row_" + table_len + "' class='tr_border'><td><label for='inputEmail4' class='petitioner_name'>"+petination_text+"<?php if($language=="bangla"){echo " নাম";}else {echo " Name";}?><span class='required_label'>*</span></label><input type='text' autocomplete='off'  class='form-control' id='petitioner_name_" + table_len + "'  name='petitioner_name_" + table_len + "' required value=''></td><td> <label for='inputEmail4' class='petitioner_advocate'>"+petination_text+"<?php if($language=="bangla"){echo " উকিল";}else {echo " Advocate";}?> <span class='required_label'>*</span></label> <input type='text' autocomplete='off'  class='form-control' id='advocate_" + table_len + "'  name='advocate_" + table_len + "' required value=''></td><td id='delete_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_row(" + table_len + ")'></td></tr>";
  //alert(row);
  if(table_len > 1){
         document.getElementById("delete_section_" + (table_len - 1)).innerHTML = "";
      }
      document.getElementById('num_of_row').value = table_len + 1;
}
function petitioner_check() {
 var petitioner = document.getElementsByClassName("petitioner_name");
 var i;
   var language=document.getElementById('language').value;
 for (i = 0; i < petitioner.length; i++) {
   if(language=="bangla")
   {
      petitioner[i].innerHTML = "বিবাদী পক্ষের নাম<span class='required_label'>*</span>";
   }else{
      petitioner[i].innerHTML = "Respondent Name<span class='required_label'>*</span>";
   }

 }
 var advocate = document.getElementsByClassName("petitioner_advocate");
 var j;
 for (j = 0; j < advocate.length; j++) {
   if(language=="bangla")
   {
     advocate[j].innerHTML = "বিবাদী পক্ষের উকিল<span class='required_label'>*</span>";
   }else{
     advocate[j].innerHTML = "Respondent Advocate<span class='required_label'>*</span>";
   }

 }
}
function respondent_check() {
  var petitioner = document.getElementsByClassName("petitioner_name");
  var i;
    var language=document.getElementById('language').value;
  for (i = 0; i < petitioner.length; i++) {
    if(language=="bangla")
    {
      petitioner[i].innerHTML = "বাদী পক্ষের নাম<span class='required_label'>*</span>";
    }else{
      petitioner[i].innerHTML = "Petitioner Name<span class='required_label'>*</span>";
    }

  }
  var advocate = document.getElementsByClassName("petitioner_advocate");
  var j;
  for (j = 0; j < advocate.length; j++) {
    if(language=="bangla")
    {
      advocate[j].innerHTML = "বাদী পক্ষের উকিল<span class='required_label'>*</span>";
    }else{
      advocate[j].innerHTML = "Petitioner Advocate<span class='required_label'>*</span>";
    }

  }
}
///file upload
function add_file_row() {

  var language=document.getElementById('language').value;
  var file_name="",label_name="";
    file_name="File Upload";
    label_name="File Name";
    if(language=="bangla")
    {
      file_name="ফাইল আপলোড";
      label_name="ফাইল নাম";
    }


  debugger;
  var table = document.getElementById("file_upload_table");
  var table_len = Number(document.getElementById('file_upload_num_of_row').value);
      var row = table.insertRow(table_len).outerHTML = "<tr id='file_row_" + table_len + "' class='tr_border'><td><label for='inputEmail4'>"+label_name+"<span class='required_label'>*</span></label><input type='text' autocomplete='off'  class='form-control' id='file_label_name_" + table_len + "'  name='file_label_name_" + table_len + "' required value=''></td><td> <label for='inputEmail4'>"+file_name+" <span class='required_label'>*</span></label> <input type='file' autocomplete='off'  class='form-control' id='file_name_" + table_len + "'  name='file_name_" + table_len + "' required value=''></td><td id='delete_file_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_fil_row(" + table_len + ")'></td></tr>";
  //alert(row);
  if(table_len > 1){
         document.getElementById("delete_file_section_" + (table_len - 1)).innerHTML = "";
      }
      document.getElementById('file_upload_num_of_row').value = table_len + 1;
}
function delete_fil_row(row_no) {
    var no = Number(row_no);
    document.getElementById("file_row_" + no + "").outerHTML = "";
    if(no != 1){
        document.getElementById("delete_file_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_fil_row(" + (no - 1) + ")'>";
    }
    document.getElementById('file_upload_num_of_row').value = no;
}

</script>
<style>
.fieldset_div{
  border: 3px solid #efdfdf;
  padding: 20px;
}
.tr_border{
  border-top: 2px solid #fff;
}
</style>
<?php
 $disable_for_drop="";
 $readonly_for_text="";
 if($case_disable=='y')
 {
    $disable_for_drop="disabled";
    $readonly_for_text="readonly";
 }
 ?>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>cases/edit/<?php echo $row_case_master->id;?>" class="cmxform" enctype="multipart/form-data" method="post">
  <h2> <?php echo $this->lang->line('client_details'); ?></h2>
  <input type="hidden" id="language" value="<?php echo $language; ?>"/>
  <?php if($case_disable=='y'){?>
    <input type="hidden" name="district_id" value="<?php echo $row_case_master->district_id; ?>"/>
    <input type="hidden" name="court_type_id" value="<?php echo $row_case_master->court_type_id; ?>"/>
    <input type="hidden" name="court_id" value="<?php echo $row_case_master->court_id; ?>"/>
    <input type="hidden" name="case_type_id" value="<?php echo $row_case_master->case_type_id; ?>"/>
    <input type="hidden" name="case_sub_type_id" value="<?php echo $row_case_master->case_sub_type_id; ?>"/>
    <input type="hidden" name="case_status_id" value="<?php echo $row_case_master->case_status_id; ?>"/>
    <input type="hidden" name="case_no" value="<?php echo $row_case_master->case_no; ?>"/>
    <input type="hidden" name="registration_date" value="<?php echo $row_case_master->registration_date; ?>"/>
    <input type="hidden" name="gr_cr_upazilla_id" value="<?php echo $row_case_master->gr_cr_upazilla_id; ?>"/>
    <input type="hidden" name="gr_cr_district_id" value="<?php echo $row_case_master->gr_cr_district_id; ?>"/>

  <?php }?>
<?php $parties_name="";$parties_id="";$parties_mobile="";?>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('client_name'); ?><span class="required_label">*</span></label>
         <select onchange="getClientNameByClientId(this)"  name="client_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($clients as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->client_id)) {
              if ($row['id'] == $row_case_master->client_id) {
                $keywords = explode('-', $row['name']);
                $parties_name= $keywords[0];
                $parties_name= $keywords[0];
                $parties_id=$row['id'];
                $parties_mobile= $keywords[1];
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('advocate').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
         <select   name="advocate_user_id" id="advocate_user_id" class="js-example-basic-single w-100" required>
           <option  value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($client_user as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->client_user_id)) {
              if ($row['id'] == $row_case_master->client_user_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>

       <input type="hidden" autocomplete="off"  class="form-control" id="parties_name"  name="parties_name" required value="<?php echo $parties_name;?>">
        <div class="form-group col-md-4" >

         <br> <br>
          <input type="radio"  name="is_petitioner" onclick="petitioner_check();" value="1" <?php if($row_case_master->is_petitioner==1){echo "checked";}?> ><?php echo $this->lang->line('petitioner'); ?>
          <input type="radio"  name="is_petitioner" onclick="respondent_check();" value="0" <?php if($row_case_master->is_petitioner==0){echo "checked";}?>> <?php echo $this->lang->line('respondent'); ?>
        </div>

        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('section_of_the_case'); ?></label>
            <textarea class="form-control" id="section_of_the_case" autocomplete="off" name="section_of_the_case" ><?php echo $row_case_master->section_of_the_case; ?></textarea>
        </div>
      </div>

      <div class="form-row" >
        <div class="form-group col-md-4">
          <label for="inputEmail4" ><?php echo $this->lang->line('name'); ?></label><span class="required_label">*</span>
          <input type="text" autocomplete="off"  class="form-control" id="edit_client_name"  name="edit_client_name" value="<?php echo $parties_name; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></label><span class="required_label">*</span>
            <input type="text" autocomplete="off"  class="form-control" id="edit_client_mobile"  name="edit_client_mobile" value="<?php echo $parties_mobile;?>">
        </div>
        <input type="hidden" autocomplete="off"  class="form-control" id="edit_client_id"  name="edit_client_id" value="<?php echo $parties_id;?>">
       </div>


      <div class="form-row">
        <table class="" id="my_data_table" style="border-top: 2px solid #fff;width: 100%;">
          <?php $p_loop=0; foreach ($row_petitioner_details as $value): ?>
            <tr id="row_<?php echo $p_loop;?>" class="tr_border">
                <td>
                 <label for="inputEmail4" class="petitioner_name"><?php if($row_case_master->is_petitioner==1){ echo $this->lang->line('respondent_name');}else{echo $this->lang->line('petitioner_name');} ?><span class="required_label">*</span></label>
                  <input type="text" autocomplete="off"  class="form-control" id="petitioner_name_<?php echo $p_loop;?>"  name="petitioner_name_<?php echo $p_loop;?>" required value="<?php echo $value['name'];?>">
                </td>
                <td>
                  <label for="inputEmail4" class="petitioner_advocate"><?php if($row_case_master->is_petitioner==1){ echo $this->lang->line('respondent_advocate');}else{echo $this->lang->line('petitioner_advocate');} ?></label>
                  <input type="text" autocomplete="off"  class="form-control" id="advocate_<?php echo $p_loop;?>"  name="advocate_<?php echo $p_loop;?>" value="<?php echo $value['advocate_name'];?>">
                </td>
                <?php  if($p_loop==0){?>
                <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_row();" value="<?php echo $this->lang->line('add_more'); ?>"></td>
              <?php }elseif (count($row_petitioner_details)==$p_loop+1) {?>
                        <td id="delete_section_<?php echo $p_loop;?>"><input type="button" value="Delete" class="btn btn-outline-danger btn-fw btn-xs" onclick="delete_row(<?php echo $p_loop;?>)"></td>
            <?php  }else{?>
                    <td id="delete_section_<?php echo $p_loop;?>"></td>
            <?php }?>
            </tr>
           <?php  $p_loop++;?>
          <?php endforeach; ?>

        </table>
          <input type="hidden" id="num_of_row" name="num_of_row" value="<?php echo $p_loop;?>">
       </div>
  </fieldset>
  <br><br>
  <h2><?php echo $this->lang->line('court_details'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('district'); ?></label></label><span class="required_label">*</span>
         <select onchange="getDisTrictWiseThanaOrCourt(this.value,'C')"   name="district_id" id="district_id"  <?php echo $disable_for_drop; ?> class="js-example-basic-single w-100" required>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($district_id as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->district_id)) {
              if ($row['id'] == $row_case_master->district_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('court_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCourtTypeByCourtId(this.value)"  name="court_type_id"  id="court_type_id" <?php echo $disable_for_drop; ?> class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($courts_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->court_type_id)) {
              if ($row['id'] == $row_case_master->court_type_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('court_sub_type'); ?><span class="required_label">*</span></label>
         <select   name="court_id" id="court_id" class="js-example-basic-single w-100" <?php echo $disable_for_drop; ?> required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($courts as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->court_id)) {
              if ($row['id'] == $row_case_master->court_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCaseSubTypeByCaseType(this.value)"  name="case_type_id" <?php echo $disable_for_drop; ?> class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->case_type_id)) {
              if ($row['id'] == $row_case_master->case_type_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <?php  $sub_type_name="";?>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_sub_type'); ?><span class="required_label">*</span></label>
         <select onchange="getCRorGR(this)"   name="case_sub_type_id" id="case_sub_type_id" <?php echo $disable_for_drop; ?> class="js-example-basic-single w-100" required>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_sub_type as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->case_sub_type_id)) {
              if ($row['id'] == $row_case_master->case_sub_type_id) {
              $sub_type_name=$row['name'];
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_assignment'); ?><span class="required_label">*</span></label>
         <select  name="case_status_id" id="case_status_id" class="js-example-basic-single w-100" <?php echo $disable_for_drop; ?> required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_status as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->case_status_id)) {
              if ($row['id'] == $row_case_master->case_status_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
     </div>
     <div id="cr_div">
       <div class="form-row" >
         <div class="form-group col-md-4">
           <label for="inputEmail4" id="case_number_label"><?php echo $sub_type_name.' '.$this->lang->line('numbers'); ?></label><span class="required_label">*</span>
           <input type="text" autocomplete="off" <?php echo $readonly_for_text; ?>  class="form-control" id="case_no"  name="case_no" value="<?php echo $row_case_master->case_no;?>" required>
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4" id="case_date_label"><?php echo $sub_type_name.' '.$this->lang->line('date'); ?></label><span class="required_label">*</span>
           <div class="input-group date">
                   <input type="text" autocomplete="off" <?php echo $disable_for_drop; ?>  name="registration_date" id="registration_date"  class="form-control" value="<?php echo date("d-m-Y", strtotime($row_case_master->registration_date)); ?>" required>
                   <span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
           </div>
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4" id="case_remarks_label"><?php echo $this->lang->line('description'); ?></label>
           <input type="text" autocomplete="off"  class="form-control" id="description"  name="description" value="<?php echo $row_case_master->description;?>">
         </div>
        </div>
     </div>
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('courts').' '.$this->lang->line('district'); ?></label><span class="required_label">*</span>
         <select onchange="getDisTrictWiseThanaOrCourt(this.value,'T')"  name="gr_cr_district_id" id="gr_cr_district_id" class="js-example-basic-single w-100" required <?php echo $disable_for_drop; ?> >
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($fir_district_id as $row) { ?>
             <option value="<?php  echo $row['id']; ?>"<?php if (isset($row_case_master->gr_cr_district_id)) {
              if ($row['id'] == $row_case_master->gr_cr_district_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('courts').' '.$this->lang->line('police').' '.$this->lang->line('station'); ?></label><span class="required_label">*</span>
          <select   name="gr_cr_upazilla_id" id="gr_cr_upazilla_id" class="js-example-basic-single w-100" required <?php echo $disable_for_drop; ?> >
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($fir_upazilla_id as $row) { ?>
             <option value="<?php  echo $row['id']; ?>"<?php if (isset($row_case_master->gr_cr_upazilla_id)) {
              if ($row['id'] == $row_case_master->gr_cr_upazilla_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <div class="form-group col-md-4">
        <br>  <label for="inputEmail4"><?php echo $this->lang->line('priority_of_the_case'); ?></label> <br>
          <input type="radio"  name="priority" value="H" <?php if($row_case_master->priority=="H"){ echo "checked";} ?>><?php echo $this->lang->line('high'); ?>
          <input type="radio"  name="priority" value="M" <?php if($row_case_master->priority=="M"){ echo "checked";} ?>><?php echo $this->lang->line('medium'); ?>
          <input type="radio"  name="priority" value="L" <?php if($row_case_master->priority=="L"){ echo "checked";} ?>> <?php echo $this->lang->line('low'); ?>
        </div>
      </div>



      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('judge_type'); ?><span class="required_label">*</span></label>
          <select   name="judge_type_id" id="judge_type_id" class="js-example-basic-single w-100" required="required">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php foreach ($judges as $row) { ?>
              <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->judge_type_id)) {
               if ($row['id'] == $row_case_master->judge_type_id) {
                 echo 'selected';
               }
             } ?>><?php echo $row['name']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('judge_name'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="judge_name"  name="judge_name" value="<?php echo $row_case_master->judge_name;?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('remarks'); ?></label>
          <textarea class="form-control" id="remarks" autocomplete="off" name="remarks"  value=""><?php echo $row_case_master->remarks;?></textarea>
        </div>
       </div>
       <div class="form-row">
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('file_colour'); ?></label>
           <select   name="file_colour" id="file_colour" class="js-example-basic-single w-100" >
             <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
             <?php foreach ($file_colour as $row) { ?>
               <option value="<?php echo $row['id']; ?>"<?php if (isset($row_case_master->file_colour)) {
                if ($row['id'] == $row_case_master->file_colour) {
                  echo 'selected';
                }
              } ?>><?php echo $row['name']; ?></option>
             <?php } ?>
           </select>
         </div>
      </div>
  </fieldset>
  <!-- <br><br>
  <h2><?php echo $this->lang->line('case_details'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('registration_date'); ?><span class="required_label">*</span></label>
         <div class="input-group date">
                 <input type="text" autocomplete="off"  name="registration_date" id="registration_date" required class="form-control" >
                 <span class="input-group-text input-group-append input-group-addon">
                     <i class="simple-icon-calendar"></i>
                 </span>
         </div>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?><span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="case_no"  name="case_no" required value="">
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('parties_name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="parties_name"  name="parties_name" required value="">
       </div>

     </div>
     <div class="form-row">
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_assignment'); ?><span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="reason_for_assignment"  name="reason_for_assignment" required value="">
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('description'); ?></label>
         <textarea class="form-control" id="description" autocomplete="off" name="description"  value=""></textarea>
       </div>

       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('first_hearing_date'); ?><span class="required_label">*</span></label>
         <div class="input-group date">
                 <input type="text" autocomplete="off"  name="first_hearing_date" id="first_hearing_date" required class="form-control" >
                 <span class="input-group-text input-group-append input-group-addon">
                     <i class="simple-icon-calendar"></i>
                 </span>
         </div>
       </div>


     </div>
     <div class="form-row">

       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('reason_for_next_date'); ?><span class="required_label">*</span></label>
         <input type="text" autocomplete="off"  class="form-control" id="reason_for_next_date"  name="reason_for_next_date" required value="">
       </div>


     </div>

         <div class="form-row">


        </div>
  </fieldset> -->
  <br><br>
  <h2><?php echo $this->lang->line('task_assign'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <div class="form-group col-md-12">
         <label for="inputEmail4"><?php echo $this->lang->line('users_name'); ?></label>
         <select  multiple="multiple" name="client_user_id[]" id="client_user_id" class="js-example-basic-single w-100" >

           <?php foreach ($client_user as $row) { ?>
               <?php if(count($row_case_client_user_details)>0){
                // $selected = (in_array($row, $row_case_client_user_details[0]) ? 'selected' : NULL);
              $selected = '';
              foreach ($row_case_client_user_details as  $value) {
                if($row['id']==$value['client_user_id'])
                {
                    $selected = 'selected';
                    break;
                }
              }
                 ?>

                   <option value="<?php echo $row['id']; ?>" <?php echo $selected;?>><?php echo $row['name']; ?></option>
             <?php }else{?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php } ?>
           <?php } ?>
         </select>
       </div>
     </div>

  </fieldset>
  <br><br>
  <!-- <h2><?php echo $this->lang->line('file_upload'); ?></h2>
  <fieldset class="fieldset_div">
     <div class="form-row">
       <table class="table" id="file_upload_table" style="border-top: 2px solid #fff;">
           <tr class="tr_border">
               <td>
                <label for="inputEmail4"><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?></label>
                 <input type="text" autocomplete="off"  class="form-control" id="file_label_name_0"  name="file_label_name_0">
               </td>
               <td>
                 <label for="inputEmail4"><?php echo $this->lang->line('file').' '.$this->lang->line('upload'); ?></label>
                 <input type="file" autocomplete="off"  class="form-control" id="file_name_0"  name="file_name_0">
               </td>
               <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_file_row();" value="<?php echo $this->lang->line('add_more'); ?>"></td>
           </tr>
       </table>
         <input type="hidden" id="file_upload_num_of_row" name="file_upload_num_of_row" value="1">
      </div>
  </fieldset>
  <br><br> -->
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
