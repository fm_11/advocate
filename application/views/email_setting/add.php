<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>email_setting/add" method="post">
<a style="color:red;font-size:12px;" href="https://www.youtube.com/watch?v=eG_ARa-T8_k" target="_blank">How to configure email for the ukildoptor software.Please click the link </a>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('from').' '.$this->lang->line('email'); ?><span style="color:red">*</span></label>
      <input type="email" autocomplete="off"  class="form-control" name="from_email" value="" required="1"/>

    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('email').' '.$this->lang->line('password'); ?><span style="color:red">*</span></label>
      <input type="password" autocomplete="off"  class="form-control" name="txt_password" value="" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('subject'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="subject" value="" />
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('message'); ?></label>
      <textarea class="form-control" name="message_body" id="tinyMceExample2" style="height: 100px;"></textarea>

    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('signature'); ?><span style="color:red">*</span></label>
        <textarea class="form-control" name="mail_signatures" id="tinyMceExample" required="1" style="height: 100px;"></textarea>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('facebook').' '.$this->lang->line('link'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="facebook" value="" />
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('linkedin'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="linkedin" value="" />
    </div>

  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('twitter').' '.$this->lang->line('link'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="twitter" value="" />
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('instagram').' '.$this->lang->line('link'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="instagram" value=""/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('youtube').' '.$this->lang->line('link'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="youtube" value="" />
    </div>
  </div>

    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
