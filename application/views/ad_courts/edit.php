
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>ad_courts/edit" method="post">
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('court_type'); ?>&nbsp;<span class="required_label">*</span></label>
      <select name="court_type_id" class="js-example-basic-single w-100" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($court_type as $row) { ?>
          <option value="<?php echo $row['id']; ?>" <?php if (isset($court[0]['court_type_id'])) {
           if ($row['id'] == $court[0]['court_type_id']) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>  </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $court[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('bn_name'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="bn_name" value="<?php echo $court[0]['bn_name']; ?>" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $court[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
