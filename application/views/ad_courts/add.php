<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>ad_courts/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('court_type'); ?>&nbsp;<span class="required_label">*</span></label>
      <select class="js-example-basic-single w-100" name="court_type_id" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        if (count($court_type)) {
          foreach ($court_type as $list) {
            ?>
            <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
            <?php
          }
        } ?>
      </select>
    </div>

  </div>
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('bn_name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="bn_name" required="1"/>
    </div>

  </div>
    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
