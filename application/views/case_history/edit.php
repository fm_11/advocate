<script type="text/javascript">
function getCourtTypeByCourtId(court_id){
  //  alert(country_id);
    document.getElementById("court_id").innerHTML = "<option>--Please Select---</option>";
    var district_id=  document.getElementById("district_id").value;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("court_id").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
    xmlhttp.send();
}
function getDisTrictWiseThanaOrCourt(district_id,type){
   if(type=='T')
   {
     document.getElementById("fir_upazilla_id").innerHTML = "<option>--Please Select---</option>";
   }else if (type=='C') {
     document.getElementById("court_type_id").innerHTML = "<option>--Please Select---</option>";
     document.getElementById("court_id").innerHTML = "<option>--Please Select---</option>";
   }

    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
          if(type=='T')
          {
            document.getElementById("fir_upazilla_id").innerHTML = xmlhttp.responseText;
          }else if (type=='C') {
            document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
          }

        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
    xmlhttp.send();
}
function process(date){
var parts = date.split("-");
return new Date(parts[2], parts[1], parts[0]);
}

function getDistrictByCountryId(){
  var GivenDate = document.getElementById("last_bussines_on_date").value;
  var CurrentDate = document.getElementById("bussines_on_date").value;
  //alert(GivenDate);
  var date=GivenDate;
  GivenDate = process(GivenDate);
  CurrentDate = process(CurrentDate);

  if(GivenDate < CurrentDate){
      //alert('Next Date is greater than the current date.');
      return true;
  }else{
      alert('Next Date  is not smaller than '+date+' previous next date.');
      return false;
  }
}

</script>
  <a style="float: right;margin-top: -119px;margin-right: -32px;" class="btn btn-outline-light" href="<?php echo base_url(); ?>case_history/index/<?php echo $case_master_id;?>"> BACK TO LIST</a>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>case_history/edit" class="cmxform" enctype="multipart/form-data" method="post" onsubmit=" return getDistrictByCountryId();">
<input type="hidden" name="last_bussines_on_date" id="last_bussines_on_date" value="<?php echo date("d-m-Y", strtotime($last_cast_history->bussines_on_date)); ?>" />
<input type="hidden" name="last_id" value="<?php echo $last_cast_history->id;?>" />
<input type="hidden" name="district_id" id="district_id" value="<?php echo $last_cast_history->district_id;?>" />
<input type="hidden" name="upazilla_id" id="upazilla_id" value="<?php echo $case_history->upazilla_id;?>" />
<input type="hidden" name="case_sub_type_id" id="case_sub_type_id" value="<?php echo $case_history->case_sub_type_id;?>" />
<input type="hidden" name="case_master_id" value="<?php echo $case_history->case_master_id;?>" />
<input type="hidden" name="id" value="<?php echo $case_history->id;?>" />
<input type="hidden" name="parties_name" value="<?php echo $case_history->parties_name;?>" />
  <fieldset>
     <div class="form-row">

       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('previous_hearing_date'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off" disabled class="form-control"   value="<?php echo date("d-m-Y", strtotime($last_cast_history->bussines_on_date)); ?>">
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off" name="case_no" class="form-control"   value="<?php echo $case_history->case_no;?>">
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('parties_name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="parties_name"  name="parties_name" disabled value="<?php echo $case_history->parties_name;?>">
       </div>

      </div>
        <!-- <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('reason_for_assignment'); ?><span class="required_label">*</span></label>
            <input type="text" autocomplete="off"  class="form-control" id="reason_for_assignment"  name="reason_for_assignment" required value="<?php echo $case_history->reason_for_assignment;?>">
          </div>
          <div class="form-group col-md-8">
            <label for="inputEmail4"><?php echo $this->lang->line('description'); ?></label>
            <textarea class="form-control" id="remarks" autocomplete="off" name="remarks"  value="<?php echo $case_history->remarks;?>"></textarea>
          </div>


         </div> -->

         <div class="form-row">
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('first_hearing_date'); ?><span class="required_label">*</span></label>
             <div class="input-group date">
                     <input type="text" autocomplete="off"  name="bussines_on_date" id="bussines_on_date" value="<?php echo date("d-m-Y", strtotime($case_history->bussines_on_date)); ?>" required class="form-control" >
                     <span class="input-group-text input-group-append input-group-addon">
                         <i class="simple-icon-calendar"></i>
                     </span>
             </div>
           </div>
           <!-- <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?><span class="required_label">*</span></label>
             <input type="text" autocomplete="off"  class="form-control" id="reason_for_next_date"  value="<?php echo $case_history->reason_for_next_date;?>" name="reason_for_next_date" required >
           </div> -->
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?><span class="required_label">*</span></label>
             <select name="case_status_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($case_status as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"<?php if (isset($case_history->case_status_id)) {
                  if ($row['id'] == $case_history->case_status_id) {
                    echo 'selected';
                  }
                } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></label>
             <input type="text" autocomplete="off"  disabled class="form-control"  value="<?php echo $case_history->mobile;?>"/>
           </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4"><?php echo $this->lang->line('courts').' '.$this->lang->line('police').' '.$this->lang->line('station'); ?></label>
               <input type="text" autocomplete="off" disabled class="form-control"   value="<?php if($language=="bangla"){echo $case_history->upazila_bn_name;}else{ echo $case_history->upazila_name;}?>">
            </div>

            <div class="form-group col-md-8">
              <label for="inputEmail4"><?php echo $this->lang->line('remarks'); ?></label>
              <input type="text" autocomplete="off"  class="form-control" maxlength="200" id="remarks"  name="remarks"  value="<?php echo $case_history->remarks;?>">
            </div>

           </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4"><?php echo $this->lang->line('court_type'); ?><span class="required_label">*</span></label>
              <select onchange="getCourtTypeByCourtId(this.value)"  name="court_type_id" id="court_type_id" class="js-example-basic-single w-100" required="required">
                <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                <?php foreach ($courts_type as $row) { ?>
                  <option value="<?php echo $row['id']; ?>"<?php if (isset($case_history->court_type_id)) {
                   if ($row['id'] == $case_history->court_type_id) {
                     echo 'selected';
                   }
                 } ?>><?php echo $row['name']; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4"><?php echo $this->lang->line('court_sub_type'); ?><span class="required_label">*</span></label>
              <select   name="court_id" id="court_id" class="js-example-basic-single w-100" required="required">
                <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                <?php foreach ($courts as $row) { ?>
                  <option value="<?php echo $row['id']; ?>"<?php if (isset($case_history->court_id)) {
                   if ($row['id'] == $case_history->court_id) {
                     echo 'selected';
                   }
                 } ?>><?php echo $row['name']; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4"><?php echo $this->lang->line('sms'); ?></label>
              <br> <br>
               <input type="radio"  name="is_sms" value="1" <?php if($case_history->is_sms==1){echo "checked";} ?>><?php echo $this->lang->line('yes'); ?>
               <input type="radio"  name="is_sms" value="0" <?php if($case_history->is_sms==0){echo "checked";} ?>> <?php echo $this->lang->line('no'); ?>
            </div>
          </div>

  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
