
<?php
$name = $this->session->userdata('name');
$status = $this->session->userdata('status');
//echo $student_status; die;
?>

<form class="form-inline" method="post" action="<?php echo base_url(); ?>case_history/index/<?php echo $case_master_id;?>">
<input type="hidden" name="case_master_id" value="<?php echo $case_master_id;?>" />

<div class="col-md-offset-2 col-md-12">
        <!-- <label class="sr-only" for="Name"><?php echo $this->lang->line('name'); ?></label>
        <?php
         $placeholder = $this->lang->line('vendor');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($roll)) {
            echo $name;
        } ?>" class="form-control" id="name">

        <label class="sr-only" for="Status"><?php echo $this->lang->line('status'); ?></label>
        <select name="status" style="max-width: 150px;"  class="form-control mb-1 mr-sm-1">
            <option value="">-- <?php echo $this->lang->line('status'); ?> --</option>
            <option value="1" <?php if (isset($status)) {
            if ($status == '1') {
                echo 'selected';
            }
        } ?>>Active
            </option>
            <option value="0" <?php if (isset($status) && $status != '') {
            if ($status == '0') {
                echo 'selected';
            }
        } ?>>Inactive
            </option>
        </select>
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button> -->

        <a style="float: right;margin-top: -14px;" class="btn btn-outline-primary" href="<?php echo base_url(); ?>case_history/add/<?php echo $case_master_id;?>"> ADD NEW  </a>
        <a style="float: right;margin-top: -96px;margin-right: -120px;" class="btn btn-outline-primary" href="<?php echo base_url(); ?>Cases/index/"> Back To List  </a>
</div>

</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('earlier_date'); ?></th>
        <th><?php echo $this->lang->line('case').' '.$this->lang->line('nong'); ?></th>
        <th><?php echo $this->lang->line('upazila'); ?></th>
        <th><?php echo $this->lang->line('defendant'); ?></th>
        <th><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></th>
        <th><?php echo $this->lang->line('court_details'); ?></th>
        <th><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?></th>
        <th><?php echo $this->lang->line('first_hearing_date'); ?></th>
        <th><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)0;
    $total=count($case_historys);
    foreach ($case_historys as $row):
        $i++;
        ?>
        <tr>
        <tr>
            <td>
                <?php echo $i; ?>

            </td>
              <td><?php echo date("d-m-Y", strtotime($row['bussines_on_date'])) ?></td>
              <td>
                <?php
                if($language=="english")  {
                echo $row['case_sub_type_name'].'-'.$row['case_no'];

                }else{
                echo $row['case_sub_type_bname'].'-'.$row['case_no'];
                }
                ?>

              </td>
              <td>
                <?php
                if($language=="english")  {
                echo $row['upazila_name'];

                }else{
                echo $row['upazila_bnname'];
                }
                ?>

              </td>
              <td><?php echo $row['parties_name']; ?></td>
              <td><?php echo $row['client_mobile']; ?></td>
              <td>

                <?php
                if($language=="english")  {
                echo $row['court_name'];
                }else{
               echo $row['court_bn_name'];
                }

                ?>
              </td>
              <td>
                <?php
                if($language=="english")  {
                echo $row['status_name'];

                }else{
               echo $row['status_bn_name'];
                }
                ?>
              </td>
              <td><?php if(!empty($row['hearing_date'])){ echo date("d-m-Y", strtotime($row['hearing_date']));}  ?></td>
              <td>
                <?php if($total==$i){?>
                  <div class="dropdown">
                      <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="ti-pencil-alt"></i>
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                       <?php if($total!=1){?>
                          <a class="dropdown-item" href="<?php echo base_url(); ?>case_history/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                          <?php } ?>
                          <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>case_history/delete/<?php echo $row['id']; ?>/<?php echo $case_master_id; ?>"><?php echo $this->lang->line('delete'); ?></a>

                      </div>
                  </div>
                  <?php } ?>
              </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<!-- <div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div> -->
</div>
