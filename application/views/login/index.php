<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login to Advocate Management v1.0.1</title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/css/horizontal-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>core_media/admin_v3/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                <img src="<?php echo base_url(); ?>core_media/admin_v3/images/school360-logo.png" alt="logo">
				  <!-- <h6 class="font-weight-light">শিক্ষা প্রতিষ্ঠানের জন্য সব </h6> -->
              </div>

              <h4><?php echo $school_name; ?></h4>
              <h6 class="font-weight-light">Login to <span style="color:#0059b3;">Advocate</span><span style="color:#ff8080;">Management</span> v1.0.1</h6>
              <form class="pt-3" method="post" action="<?php echo base_url(); ?>login/authentication">
                <?php
                                    $message = $this->session->userdata('message');
                                    if ($message != '') {
                                        ?>
										<div class="alert alert-success" role="alert">
												<?php
                                                echo $message;
                                        $this->session->unset_userdata('message'); ?>
										</div>
										<?php
                                    }
                           ?>

						   <?php
                                    $exception = $this->session->userdata('exception');
                                    if ($exception != '') {
                                        ?>
										<div class="alert alert-danger" role="alert">
												<?php
                                                echo $exception;
                                        $this->session->unset_userdata('exception'); ?>
										</div>
										<?php
                                    }
                           ?>
                <div class="form-group">
                  <label for="exampleInputEmail">Username</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ion ion-md-person text-primary"></i>
                      </span>
                    </div>
                    <input type="text" autocomplete="off"  name="user_name" required class="form-control form-control-lg border-left-0" id="exampleInputEmail" placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword">Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="ion ion-md-lock text-primary"></i>
                      </span>
                    </div>
                    <input type="password" required name="password" class="form-control form-control-lg border-left-0" id="exampleInputPassword" placeholder="Password">
                  </div>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="<?php echo base_url(); ?>login/forgot_password" class="auth-link text-black">Forgot password?</a>
                </div>
                <div class="my-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">LOGIN</button>
                </div>
                <!-- <div class="mb-2 d-flex">
                  <button type="button" class="btn btn-facebook auth-form-btn flex-grow mr-1">
                    <i class="ion ion-logo-facebook mr-2"></i>Facebook
                  </button>
                  <button type="button" class="btn btn-google auth-form-btn flex-grow ml-1">
                    <i class="ion ion-logo-google mr-2"></i>Google
                  </button>
                </div> -->
                <div class="text-center mt-4 font-weight-light">
					<span style="color:#0059b3;">Advocate</span><span style="color:#ff8080;">Management</span> <a target="_blank" href="<?php echo  base_url(); ?>login/download_zone" class="text-primary">Download</a> Zone
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row">
            <p class="text-white font-weight-medium text-center flex-grow align-self-end">
				<span style="background-color: red !important; color: #000000 !important; font-size: 14px;padding: 5px" >Copyright &copy;
				<a href="https://spatei.com/" target="_blank" style="color: #ffffff !important; font-size: 14px;" class="text-primary">Spate Initiative Limited</a>
				All rights reserved
				</span>
            </p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/template.js"></script>
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/settings.js"></script>
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
</body>

</html>
