<script>
function myFunction() {
    var pass1 = document.getElementById("new_pass").value;
    var pass2 = document.getElementById("con_new_pass").value;
    var ok = true;
    if (pass1 != pass2) {
        alert("Password Doesn't Match !");
        document.getElementById("new_pass").style.borderColor = "#E34234";
        document.getElementById("con_new_pass").style.borderColor = "#E34234";
        ok = false;
    }
    return ok;
}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>login/change_password" method="post" onsubmit="return myFunction()">
  <div class="form-row">
        <div class="form-group col-md-4">
          <label>Current Password</label>
          <input class="form-control" type="password" required="1" name="current_pass" id="current_pass">
        </div>
        <div class="form-group col-md-4">
          <label>New Password</label>
          <input class="form-control" type="password" required="1" name="new_pass" id="new_pass">
        </div>
        <div class="form-group col-md-4">
          <label>Confirm New Password</label>
          <input class="form-control" type="password" required="1" name="con_new_pass" id="con_new_pass">
          <input class="form-control" type="hidden" required="1" name="user_id" id="user_id" value="<?php echo $user_id;?>">
        </div>
  </div>

  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="Change">
  </div>
</form>
