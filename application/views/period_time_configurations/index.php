<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>period_time_configurations/index" enctype="multipart/form-data" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Shift</label>
			<select name="shift_id" id="shift_id" class="js-example-basic-single w-100" required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($A = 0; $A < count($shifts); $A++) { ?>
					<option value="<?php echo $shifts[$A]['id']; ?>"
						<?php if (isset($shift_id)) {
							if ($shift_id == $shifts[$A]['id']) {
								echo 'selected';
							}
						} ?>><?php echo $shifts[$A]['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>


	<div class="btn-group float-right">
		<input class="btn btn-primary" name="process" type="submit" value="Process">
	</div>
</form>


<?php
if(isset($process_list)){
	echo $process_list;
}
?>
