<script type="text/javascript">


    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/updateMsgClientsStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }
</script>
<style>
 #order-listing_filter{
   float: right !important;
 }
</style>


<div class="row">

  <div class="col-12">

    <div class="table-responsive">
      <table id="order-listing" class="table">
        <thead>
          <tr>
            <th><?php echo $this->lang->line('sl'); ?></th>
            <th><?php echo $this->lang->line('bangla').' '.$this->lang->line('name')?></th>
            <th><?php echo $this->lang->line('english').' '.$this->lang->line('name')?></th>
            <th><?php echo $this->lang->line('total_person')?></th>
            <th><?php echo $this->lang->line('mobile'); ?></th>
            <th><?php echo $this->lang->line('address'); ?></th>
            <th><?php echo $this->lang->line('district'); ?></th>
            <th><?php echo $this->lang->line('upazila'); ?></th>
            <th><?php echo $this->lang->line('status'); ?></th>
            <th><?php echo $this->lang->line('actions'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i = 0;
          foreach ($clients as $row):
              $i++;
              ?>
          <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo $row['first_name']; ?></td>
              <td> <?php echo $row['last_name']; ?></td>
             <td><?php echo $row['total_person']; ?></td>
             <td><?php echo $row['mobile']; ?></td>
             <td><?php echo $row['address']; ?></td>
             <td><?php if($language=="english"){echo $row['district'];}else{echo $row['bn_district_name']; }  ?>  </td>
             <td><?php if($language=="english"){echo $row['upazila'];}else{echo $row['bn_upazila_name']; }  ?>  </td>
               <td>
               <span id="status_sction_<?php echo $row['id']; ?>">
               <?php
               if ($row['is_active'] == 1) {
                   ?>
                   <a title="Active" href="#"
                   class="deleteTag"  onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)"><i class="ti-check-box"></i></a>
               <?php
               } else {
                   ?>
                   <a title="In Active" href="#"
                   class="deleteTag"  onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)"><i class="ti-na"></i></a>
               <?php
               }
               ?>
               </span>
           </td>
           <td>
               <div class="dropdown">
                   <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="ti-pencil-alt"></i>
                   </button>
                   <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                       <a class="dropdown-item" href="<?php echo base_url(); ?>clients/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                       <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>clients/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                   </div>
               </div>
           </td>
          </tr>
  <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
