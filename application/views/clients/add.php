<script type="text/javascript">
    function getDistrictByCountryId(country_id){
      //  alert(country_id);
        document.getElementById("district_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("district_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_district_by_country?country_id=" + country_id, true);
        xmlhttp.send();
    }
    function getUpazilasByDistrictId(district_id){
        document.getElementById("address").innerHTML="";
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("upazilas_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_upazilas_by_district?district_id=" + district_id, true);
        xmlhttp.send();
    }
    function from_validation()
    {
      var phone=	document.getElementById("mobile").value;
      if (/^\d{11}$/.test(phone)) {

      } else {
          var language=	document.getElementById("language").value;
          if(language=='english')
          {
             alert('Phone number is incorrect! The number must be 11 digits.');
          }else{
            alert('ফোন নম্বরটি সঠিক নয় ! সংখ্যাটি 11 ডিজিটের হতে হবে ।');
          }

          return false;
      }
        return true;
    }
    function changeUpazila(sel)
      {
          var upazila=sel.options[sel.selectedIndex].text;
          var sel2 = document.getElementById("district_id");
          var district = sel2.options[sel2.selectedIndex].text;
         document.getElementById("address").innerHTML=upazila+','+district;
      }
</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>clients/add" class="cmxform" method="post" onSubmit="return from_validation()">
<input type="hidden" id="language" value="<?php echo $language;?>"/>
 <?php $address="";?>
  <fieldset>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('bangla').' '.$this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="first_name"  required="required"  name="first_name" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="last_name"  required="required"  name="last_name" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('total_person'); ?></label>
          <input type="number" autocomplete="off"  class="form-control" id="total_person" min="0" max="100" name="total_person" value="">
        </div>

      </div>


        <div class="form-row">
           <!-- <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('country'); ?><span class="required_label">*</span></label>
             <select onchange="getDistrictByCountryId(this.value)" name="country_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($countries as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div> -->
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?><span class="required_label">*</span></label>
             <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile" required value="">
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('district'); ?><span class="required_label">*</span></label>
             <select onchange="getUpazilasByDistrictId(this.value)"  name="district_id" class="js-example-basic-single w-100" id="district_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>

               <?php foreach ($districts as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"<?php if (isset($district_id)) {
	 							  if ($row['id'] == $district_id) {
                      $address=$row['name'];
	 								  echo 'selected';

	 							  }
	 						  } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('upazila'); ?><span class="required_label">*</span></label>
             <select onchange="changeUpazila(this)" name="upazilas_id" class="js-example-basic-single w-100" id="upazilas_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($upazilas as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"<?php if (isset($upazilla_id)) {
	 							  if ($row['id'] == $upazilla_id) {

	 								  echo 'selected';
                     $address =$row['name'].",".$address;
                  //
	 							  }
	 						  } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
         </div>
         <div class="form-row">
           <div class="form-group col-md-4">
            <label><?php echo $this->lang->line('gender'); ?>&nbsp;<span class="required_label">*</span></label>
            <select name="gender" class="js-example-basic-single w-100" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>

              <option selected value="M"> <?php echo $this->lang->line('male'); ?> </option>
              <option value="F"> <?php echo $this->lang->line('female'); ?></option>
            </select>
          </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4"><?php echo $this->lang->line('alternate').' '.$this->lang->line('number'); ?></label>
              <input type="text" autocomplete="off"  class="form-control" id="alternate_mobile_no"  name="alternate_mobile_no" value="">
            </div>

            <div class="form-group col-md-4">
              <label for="inputEmail4"><?php echo $this->lang->line('address'); ?><span class="required_label">*</span></label>
              <textarea class="form-control" id="address" autocomplete="off" name="address" required value=""><?php echo $address;?></textarea>
            </div>
          </div>

   <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('reference').' '.$this->lang->line('name'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" id="reference_name"  name="reference_name" value="">
    </div>
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('reference').' '.$this->lang->line('mobile'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" id="reference_mobile"  name="reference_mobile" value="">
    </div>
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('email'); ?></label>
      <input type="email" autocomplete="off"  class="form-control" id="email"  name="email" value="">
    </div>
  </div>
  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
