<script type="text/javascript">
    function getDistrictByCountryId(country_id){
      //  alert(country_id);
        document.getElementById("district_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("district_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_district_by_country?country_id=" + country_id, true);
        xmlhttp.send();
    }
    function getUpazilasByDistrictId(district_id){
          document.getElementById("address").innerHTML="";
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>---</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("upazilas_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_upazilas_by_district?district_id=" + district_id, true);
        xmlhttp.send();
    }
    function from_validation()
    {
      var phone=	document.getElementById("mobile").value;
      if (/^\d{11}$/.test(phone)) {

      } else {
          var language=	document.getElementById("language").value;
          if(language=='english')
          {
             alert('Phone number is incorrect! The number must be 11 digits.');
          }else{
            alert('ফোন নম্বরটি সঠিক নয় ! সংখ্যাটি 11 ডিজিটের হতে হবে ।');
          }

          return false;
      }
        return true;
    }
    function changeUpazila(sel)
      {
          var upazila=sel.options[sel.selectedIndex].text;
          var sel2 = document.getElementById("district_id");
          var district = sel2.options[sel2.selectedIndex].text;
         document.getElementById("address").innerHTML=upazila+','+district;
      }
</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>clients/edit/<?php echo $row_data->id; ?>" class="cmxform" method="post" onSubmit="return from_validation()">
	<fieldset>
    <input type="hidden" id="language" value="<?php echo $language;?>"/>
    <input type="hidden" id="country_id" value="<?php echo $row_data->country_id;?>"/>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('name'); ?></label><span class="required_label">*</span>
          <input type="text" autocomplete="off"  class="form-control" id="first_name"  name="first_name" required="required" value="<?php echo $row_data->first_name; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="last_name"  required="required"  name="last_name" value="<?php echo $row_data->last_name; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('total_person'); ?></label>
          <input type="number" autocomplete="off"  class="form-control" id="total_person"  name="total_person" value="<?php echo $row_data->total_person; ?>">
        </div>

      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile" required value="<?php echo $row_data->mobile; ?>">
        </div>

         <!-- <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('country'); ?><span class="required_label">*</span></label>
           <select onchange="getDistrictByCountryId(this.value)" name="country_id" class="js-example-basic-single w-100" required="required">
             <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
             <?php foreach ($countries as $row) { ?>
               <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->country_id)) {
                if ($row['id'] == $row_data->country_id) {
                  echo 'selected';
                }
              } ?>><?php echo $row['name']; ?></option>
             <?php } ?>
           </select>
         </div> -->
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('district'); ?><span class="required_label">*</span></label>
           <select onchange="getUpazilasByDistrictId(this.value)"  name="district_id" class="js-example-basic-single w-100" id="district_id" required="required">
             <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
             <?php foreach ($districts as $row) { ?>
               <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->district_id)) {
                if ($row['id'] == $row_data->district_id) {
                  echo 'selected';
                }
              } ?>><?php echo $row['name']; ?></option>
             <?php } ?>
           </select>
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('upazila'); ?><span class="required_label">*</span></label>
           <select onchange="changeUpazila(this)" name="upazilas_id" class="js-example-basic-single w-100" id="upazilas_id" required="required">
             <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
             <?php foreach ($upazilas as $row) { ?>
               <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->upazilas_id)) {
                if ($row['id'] == $row_data->upazilas_id) {
                  echo 'selected';
                }
              } ?>><?php echo $row['name']; ?></option>
             <?php } ?>
           </select>
         </div>
       </div>

      <div class="form-row">
        <div class="form-group col-md-4">
         <label><?php echo $this->lang->line('gender'); ?>&nbsp;<span class="required_label">*</span></label>
         <select name="gender" class="js-example-basic-single w-100" required="required">
           <option value="<?php echo $row_data->gender; ?>">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <option value="M" <?php if (isset($row_data->gender)) {
            if ('M' == $row_data->gender) {
              echo 'selected';
            }
          } ?>> <?php echo $this->lang->line('male'); ?> </option>
           <option value="F" <?php if (isset($row_data->gender)) {
            if ('F' == $row_data->gender) {
              echo 'selected';
            }
          } ?>> <?php echo $this->lang->line('female'); ?></option>
         </select>
       </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('alternate').' '.$this->lang->line('number'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="alternate_mobile_no"  name="alternate_mobile_no" value="<?php echo $row_data->alternate_mobile_no; ?>">
        </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('address'); ?><span class="required_label">*</span></label>
           <textarea class="form-control" id="address" autocomplete="off" name="address" required ><?php echo $row_data->address; ?></textarea>
         </div>

       </div>

   <div class="form-row">
     <div class="form-group col-md-4">
       <label for="inputEmail4"><?php echo $this->lang->line('reference').' '.$this->lang->line('name'); ?></label>
       <input type="text" autocomplete="off"  class="form-control" id="reference_name"  name="reference_name" value="<?php echo $row_data->reference_name; ?>">
     </div>
     <div class="form-group col-md-4">
       <label for="inputEmail4"><?php echo $this->lang->line('reference').' '.$this->lang->line('mobile'); ?></label>
       <input type="text" autocomplete="off"  class="form-control" id="reference_mobile"  name="reference_mobile" value="<?php echo $row_data->reference_mobile; ?>">
     </div>
     <div class="form-group col-md-4">
       <label for="inputEmail4"><?php echo $this->lang->line('email'); ?></label>
       <input type="email" autocomplete="off"  class="form-control" id="email"  name="email" value="<?php echo $row_data->email; ?>">
     </div>
  </div>
  </fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
