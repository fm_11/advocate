<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fund_transfers/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>From Head</label>
      <select class="js-example-basic-single w-100" name="from_asset_category_id" required="1">
          <option value="">-- Select --</option>
          <?php
          $i = 0;
          if (count($asset_category)) {
              foreach ($asset_category as $list) {
                  $i++; ?>
                  <option
                          value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                  <?php
              }
          }
          ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label>To Head</label>
      <select class="js-example-basic-single w-100" name="to_asset_category_id" required="1">
          <option value="">-- Select --</option>
          <?php
          $i = 0;
          if (count($asset_category)) {
              foreach ($asset_category as $list) {
                  $i++; ?>
                  <option
                          value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                  <?php
              }
          }
          ?>
      </select>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Date</label>
      <div class="input-group date">
              <input type="text" autocomplete="off"  name="date" id="date" required class="form-control">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label>Amount</label>
      <input type="text" autocomplete="off"  name="amount" id="amount" class="form-control" value="">
    </div>
  </div>


  <div class="form-row">
    <div class="form-group col-md-12">
      <label>Description</label>
      <textarea rows="5" cols="50" class="form-control" name="description"></textarea>
    </div>
  </div>

  <div class="float-right">
     <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
