<script type="text/javascript">
    function getDistrictByCountryId(country_id){
      //  alert(country_id);
        document.getElementById("district_id").innerHTML = "<option>--Please Select---</option>";
        document.getElementById("upazilas_id").innerHTML = "<option>--Please Select---</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("district_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>vendor/ajax_district_by_country?country_id=" + country_id, true);
        xmlhttp.send();
    }
    function getUpazilasByDistrictId(district_id){
      //  alert(country_id);
        document.getElementById("upazilas_id").innerHTML = "<option>--Please Select---</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("upazilas_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>vendor/ajax_upazilas_by_district?district_id=" + district_id, true);
        xmlhttp.send();
    }

</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>vendor/add" class="cmxform" method="post">
  <fieldset>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('company').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="company_name" required name="company_name" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('first').' '.$this->lang->line('name'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="first_name"  name="first_name" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('last').' '.$this->lang->line('name'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="last_name"  name="last_name" value="">
        </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('email'); ?></label>
           <input type="text" autocomplete="off"  class="form-control" id="email"  name="email" value="">
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('no'); ?><span class="required_label">*</span></label>
           <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile" required value="">
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('alternate').' '.$this->lang->line('no'); ?></label>
           <input type="text" autocomplete="off"  class="form-control" id="alternative_mobile"  name="alternative_mobile" value="">
         </div>
       </div>

        <div class="form-row">
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('country'); ?><span class="required_label">*</span></label>
             <select onchange="getDistrictByCountryId(this.value)" name="country_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($countries as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('district'); ?><span class="required_label">*</span></label>
             <select onchange="getUpazilasByDistrictId(this.value)"  name="district_id" class="js-example-basic-single w-100" id="district_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($districts as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('upazila'); ?><span class="required_label">*</span></label>
             <select name="upazilas_id" class="js-example-basic-single w-100" id="upazilas_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($upazilas as $row) { ?>
                 <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
         </div>
         <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4"><?php echo $this->lang->line('address'); ?><span class="required_label">*</span></label>
              <textarea class="form-control" id="address" autocomplete="off" name="address" required value=""></textarea>

            </div>

          </div>
   <div class="form-row">
	  <div class="form-group col-md-4">
		  <label for="inputEmail4"><?php echo $this->lang->line('is_active'); ?> ?</label>
		  <input name="is_active" type="checkbox" value="1">
	  </div>
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('gstin'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" id="gstin"  name="gstin" value="">
    </div>
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('pan'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" id="pan"  name="pan" value="">
    </div>
  </div>
  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
