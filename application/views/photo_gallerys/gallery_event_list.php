<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;" href="<?php echo base_url(); ?>photo_gallerys/gallery_event_add"><span>Add New Event</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
        <tr>
            <th width="50" scope="col">SL</th>
            <th width="200" scope="col">Event Name</th>
            <th width="100" scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>


        <?php
        $i = 0;
        foreach ($events as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <?php echo $i; ?>
                </td>
                <td><?php echo $row['event_name']; ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>photo_gallerys/gallery_event_edit/<?php echo $row['id']; ?>" class="edit_icon" title="Edit"></a>
                    <a href="<?php echo base_url(); ?>photo_gallerys/gallery_event_delete/<?php echo $row['id']; ?>" onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach; ?>

        <tr class="footer">
            <td colspan="4" align="right">
                <!--  PAGINATION START  -->             
                <div class="pagination">
                    <?php echo $this->pagination->create_links(); ?>
                </div>  
                <!--  PAGINATION END  -->       
            </td>
        </tr>
    </tbody>
</table>