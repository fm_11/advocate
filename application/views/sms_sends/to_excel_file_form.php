<script>
    function getTemplateBody(template_id) {
        if (template_id == '' || template_id == 'N') {
			document.getElementById("template_body").value  = "";
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

</script>



<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th colspan="2" scope="col">
				<b><?php echo $this->lang->line('file') . ' ' . $this->lang->line('format') . ' ' . $this->lang->line('download'); ?></b>
			</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				<?php echo $this->lang->line('file'); ?><?php echo $this->lang->line('format'); ?><?php echo $this->lang->line('download'); ?>
			</td>
			<td>
				<a href="<?php echo base_url(); ?>uploads/WithMessageBody.xls"><?php echo $this->lang->line('download'); ?></a>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo $this->lang->line('without') . ' ' . $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
			</td>
			<td>
				<a href="<?php echo base_url(); ?>uploads/WithoutMessageBody.xls"><?php echo $this->lang->line('download'); ?></a>
			</td>
		</tr>
		</tbody>
	</table>
</div>

<hr>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>sms_sends/to_excel_file" method="post"
      enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('message') . ' ' . $this->lang->line('template'); ?></label><br>
			<select class="js-example-basic-single w-100" name="template_id" onchange="getTemplateBody(this.value)"
					required="1">
				<option value="N"><?php echo $this->lang->line('excel') . ' ' . $this->lang->line('file') . ' ' . $this->lang->line('with') . ' ' . $this->lang->line('template'); ?></option>
				<?php
				$i = 0;
				if (count($templates)) {
					foreach ($templates as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
		    <label><?php echo $this->lang->line('message') . ' ' . $this->lang->line('type'); ?></label><br>
		 	<select class="js-example-basic-single w-100" name="sms_type" required="1">
				  <option value="E"><?php echo $this->lang->line('english'); ?></option>
				  <option value="B"><?php echo $this->lang->line('bangla'); ?></option>
		   </select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('file'); ?></label><br>
			<input type="file" name="txtFile" required="1" class="smallInput"><br>
			<span style="font-size: 13px;">* File Format -> Excel-2003</span>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-12">
			<label><?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?></label><br>
			<textarea name="template_body" rows="5" class="form-control" id="template_body"></textarea>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Process">
	</div>
</form>
