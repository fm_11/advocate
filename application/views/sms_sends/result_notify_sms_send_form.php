<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>


<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>sms_sends/to_result_notify" method="post"
      enctype="multipart/form-data">
	  
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label><br>
			<select class="js-example-basic-single w-100"
					name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
								value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
								<?php if (isset($class_shift_section_id)) {
									if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
										echo 'selected';
									}
								} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('group'); ?></label><br>
			<select class="js-example-basic-single w-100" name="txtGroup" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($groups)) {
					foreach ($groups as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($group_id)) {
							if ($group_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('exam'); ?></label><br>
			<select class="js-example-basic-single w-100" name="txtExam" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($exams)) {
					foreach ($exams as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($exam_id)) {
							if ($exam_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name'] . '(' . $list['year'] . ')'; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>	
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('send') . ' ' . $this->lang->line('marks') . ' ' . $this->lang->line('or') . ' ' .  $this->lang->line('gpa'); ?></label><br>
			<select class="js-example-basic-single w-100" name="txtSMSType" required="1">
				<option value="MG"><?php echo $this->lang->line('marks') . ' ' . $this->lang->line('and') . ' ' . $this->lang->line('gpa'); ?></option>
				<option value="M"><?php echo $this->lang->line('marks'); ?></option>
				<option value="G"><?php echo $this->lang->line('gpa'); ?></option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('with_institute_name'); ?> ?</label><br>
			<select class="js-example-basic-single w-100" name="txtWithSchoolName" required="1">
				<option value="Y"><?php echo $this->lang->line('yes'); ?></option>
				<option value="N"><?php echo $this->lang->line('no'); ?></option>
			</select>
		</div>
    </div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Process">
	</div>
</form>

    <?php
    if (isset($student_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>sms_sends/to_result_notify_sms_send"
              method="post"
              enctype="multipart/form-data">
			<div class="table-sorter-wrapper col-lg-12 table-responsive">
				<table id="sortable-table-1" class="table">
                <thead>
                <tr>
                    <td colspan="7" style="text-align: right;">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-warning" value="Send SMS">
						</div>
                    </td>
                </tr>
                <tr>
                    <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
					<th scope="col"><?php echo $this->lang->line('roll'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('name'); ?></th>
					<th scope="col"><?php echo $this->lang->line('student_code'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('father_name'); ?></th>
					<th scope="col">GPA</th>
                    <th scope="col"><?php echo $this->lang->line('guardian_mobile'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($student_info as $row):
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
						<td><?php echo $row['roll_no']; ?></td>
						<td>
							<?php echo $row['name']; ?>
							<input type="hidden" name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>">
							<input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['student_id']; ?>">
							<input type="hidden" name="roll_no_<?php echo $i; ?>" value="<?php echo $row['roll_no']; ?>">
						</td>
                        <td><?php echo $row['student_code']; ?></td>

                        <td><?php echo $row['father_name']; ?></td>
						<td><?php echo $row['c_alpha_gpa_with_optional']; ?></td>
                        <td>
                            <input type="text" autocomplete="off"  value="<?php echo $row['guardian_mobile']; ?>"
                                   name="guardian_mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                    <?php $i++; endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
                        <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
						<input type="hidden" name="shift_id" value="<?php echo $shift_id; ?>">
                        <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
                        <input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
                        <input type="hidden" name="exam_id" value="<?php echo $exam_id; ?>">
                        <input type="hidden" name="sms_type" value="<?php echo $sms_type; ?>">
                        <input type="hidden" name="with_school_name" value="<?php echo $with_school_name; ?>">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-warning" value="Send SMS">
						</div>
                    </td>
                </tr>
                </tbody>
            </table>
			</div>
        </form>
        <?php
    }
    ?>
