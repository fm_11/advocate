<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
                var list = document.getElementsByClassName('message_dynamic');
                var n;
                for (n = 0; n < list.length; ++n) {
                    list[n].value=xmlhttp.responseText;
                }
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some Clients.");
            return false;
        } else {
            return true;
        }
    }
</script>


    <?php
    if (isset($attendance_sms)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>sms_sends/attendance_sms_send_post" method="post"
              enctype="multipart/form-data">
			<div class="table-sorter-wrapper col-lg-12 table-responsive">
				<table id="sortable-table-1" class="table">
					<thead>
                <!-- <tr>
                    <td colspan="2" style="text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('name'); ?>
                    </td>
                    <td colspan="5">
                        <select style="padding:6px;" name="template_id" onchange="getTemplateBody(this.value)"
                                required="1">
                            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                            <?php
							if (count($templates)) {
							foreach ($templates as $list) {
							  ?>
							  <option value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
							<?php
							  }
							  } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="vertical-align: middle;text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
                    </td>
                    <td colspan="5">
                        <textarea name="template_body" required rows="10" cols="35" id="template_body"></textarea>
                    </td>
                </tr> -->
                <!-- <tr>
                    <td colspan="7" style="text-align: right;">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-primary" value="Send SMS">
						</div>
                    </td>
                </tr> -->
                <tr>
                    <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('client_name'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('registration_date'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('case_no'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('message'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
        foreach ($attendance_sms as $row):
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $i + 1; ?></td>
                        <td>
							              <?php echo $row['parties_name']; ?>
						            </td>
                        <td>
                            <?php echo date("d-m-Y", strtotime($row['bussines_on_date'])); ?>
                        </td>
                        <td>
                            <?php echo $row['case_name'].'-'.$row['case_no']; ?>
                        </td>
                        <td>
                            <input type="hidden" autocomplete="off"  value="<?php echo $row['client_id']; ?>" name="client_id_<?php echo $i; ?>">
                            <input type="text" autocomplete="off"  value="<?php echo $row['mobile']; ?>" name="mobile_<?php echo $i; ?>">
                            <!-- <?php echo $row['mobile']; ?> -->
                        </td>
                        <td>
							              <input type="hidden" autocomplete="off"  value="<?php echo $row['client_id']; ?>" name="client_id_<?php echo $i; ?>">
                            <input type="hidden" autocomplete="off"  value="<?php echo $row['case_history_id']; ?>" name="case_history_id_<?php echo $i; ?>">
                            <textarea name="message_template_<?php echo $i; ?>" readonly rows="4" cols="40"><?php echo $row['case_name']."-".$row['case_no']. " তাং ".date("d-m-Y", strtotime($row['bussines_on_date']))." আপনি সহ সবাইকে সশরীরে আসার জন্য অনুরোধ করা হলো (".$setting->name.") ধন্যবাদ।"?></textarea>
                        </td>
                    </tr>
                <?php $i++;
        endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
						<!-- <input type="hidden" value="<?php echo $shift_id; ?>" name="shift_id"> -->
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-primary" value="Send SMS">
						</div>
                    </td>
                </tr>
                </tbody>
            </table>
			</div>
        </form>
        <?php
    }
    ?>
