<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>store_management/inventory_category_Index"><span>Category</span></a></li>
	<li><a href="<?php echo base_url(); ?>store_management/inventory_sub_category_Index"><span>Sub Category</span></a></li>
	<li><a href="<?php echo base_url(); ?>store_management/inventory_ware_house_Index"><span>Ware House</span></a></li>	
    <li><a href="<?php echo base_url(); ?>store_management/vendors_index"><span>Vendors</span></a></li>
	<li><a href="<?php echo base_url(); ?>store_management/inventory_item_index"><span>Item</span></a></li>
	<li><a href="<?php echo base_url(); ?>store_management/inventory_item_in_index"><span>Item In</span></a></li>
	<li><a href="<?php echo base_url(); ?>store_management/inventory_scrapped_goods_index"><span>Scrapped Goods</span></a></li>
    <li><a href="<?php echo base_url(); ?>store_management/stock_register_index"><span>Stock Register</span></a></li>
</ul>

