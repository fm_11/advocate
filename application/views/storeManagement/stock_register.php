<h2>
    Stock Register
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="150" scope="col">Category</th>
        <th width="100" scope="col">Sub Category</th>
        <th width="200" scope="col">Item Name</th>
        <th width="120" scope="col">Quantity</th>
        <th width="120" scope="col">Scrapped Goods</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    /* echo '<pre>';
    print_r($inventory_item_info); */
    foreach ($scrapped_goods_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['category_name']; ?></td>
            <td><?php echo $row['sub_category_name']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo ($row['opening_quantity'] + $row['in_quantity']) - $row['scrapped_goods_quantity']; ?></td>
            <td>
                <?php
                if($row['scrapped_goods_quantity'] == ''){
                    echo '0';
                }else{
                    echo $row['scrapped_goods_quantity'];
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>



