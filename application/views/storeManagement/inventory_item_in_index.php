<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>store_management/add_inventory_item_in"><span>In a Item</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="150" scope="col">Item Name</th>
        <th width="100" scope="col">Quantity</th>
        <th width="200" scope="col">Warehouse</th>
        <th width="120" scope="col">Warranty From</th>
        <th width="120" scope="col">Warranty To</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    /* echo '<pre>';
    print_r($inventory_item_info); */
    foreach ($item_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['quantity'] . ' ' . $row['unit_name']; ?></td>
            <td><?php echo $row['warehouse']; ?></td>
            <td><?php echo $row['warrenty_from']; ?></td>
            <td><?php echo $row['warrenty_to']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>store_management/inventor_iny_item_edit/<?php echo $row['id']; ?>"
                   title="Edit">Edit</a>&nbsp;
                <a href="<?php echo base_url(); ?>store_management/inventory_in_item_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>



