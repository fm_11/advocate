<script>
    $(function() {
        $( "#txtFromDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
        $( "#txtToDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>
<?php
if($action == 'edit') {
    ?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/all_headmasters_edit" method="post" enctype="multipart/form-data">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $headmasters[0]['name']; ?>" name="txtName" required="1"/>

        <label>Educational Qualification</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $headmasters[0]['edu_qua']; ?>" name="txtEduQua" required="1"/>

        <label>From Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" placeholder="YYYY-mm-dd" value="<?php echo $headmasters[0]['from_date']; ?>" id="txtFromDate"  name="txtFromDate"  required="1"/>

        <label>To Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" placeholder="YYYY-mm-dd"value="<?php echo $headmasters[0]['to_date']; ?>" id="txtToDate"  name="txtToDate"  required="1"/>
        <input type="hidden" class="smallInput wide" placeholder="YYYY-mm-dd"value="<?php echo $headmasters[0]['id']; ?>"  name="id"  required="1"/>
        <br>
        <input type="submit" class="submit" value="Update">
    </form><br />
<?php
}else{
?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/all_headmaster_add" method="post" enctype="multipart/form-data">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtName" required="1"/>
		
		 <label>Educational Qualification</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtEduQua" required="1"/>

        <label>From Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" placeholder="YYYY-mm-dd" id="txtFromDate" name="txtFromDate"  required="1"/>

        <label>To Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" placeholder="YYYY-mm-dd" id="txtToDate"  name="txtToDate"  required="1"/>
        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />

<?php
}
?>
<div class="clear"></div><br />

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
