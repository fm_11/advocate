<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="100" scope="col">Name</th>
        <th width="60" scope="col">Email</th>
        <th width="60" scope="col">Date</th>
        <th width="300" scope="col">Message</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($messages as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['email']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['date']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['message']; ?></td>

            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>dashboard/message_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>