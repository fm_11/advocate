<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>dashboard/digital_content_add"><span>Add New Content</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Title</th>
        <th width="150" scope="col">Date</th>
        <th width="200" scope="col">Download</th>
        <th width="150" scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($digital_content_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['title']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td>
                <a href="<?php echo base_url() . MEDIA_FOLDER; ?>/digital_content/<?php echo $row['location']; ?>"
                   target="_blank"><?php echo $row['location']; ?></a>
            </td>
        <td>
            <a href="<?php echo base_url(); ?>dashboard/digital_content_delete/<?php echo $row['id']; ?>"
               onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
        </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="7" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>