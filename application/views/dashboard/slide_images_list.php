<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }


    function msgStatusUpdate(id,is_view){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("is_view_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>dashboard/updateMsgStatusSlideImageStatus?id=" + id + '&&is_view=' + is_view, true);
        xmlhttp.send();
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;" href="<?php echo base_url(); ?>dashboard/slide_image_add"><span>Add New Photo</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="150" scope="col">Photo Title</th>
        <th width="100" scope="col">Date</th>
        <th width="100" scope="col">Is View</th>
        <th width="200" scope="col">Photo</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>


    <?php
    $i = 0;
    foreach ($images as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['title']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['date']; ?></td>
            <td style="vertical-align:middle" id="is_view_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['is_view'] == 1) {
                    ?>
                    <a class="approve_icon" title="Approve" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_view']; ?>)"></a>
                <?php
                } else {
                    ?>
                    <a class="reject_icon" title="Reject" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_view']; ?>)"></a>
                <?php
                }
                ?>
            </td>
            <td style="vertical-align:middle">
                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/images/slide/<?php echo $row['photo_location'] ?>" height="120" width="150">
            </td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>dashboard/slide_image_delete/<?php echo $row['id']; ?>" onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>