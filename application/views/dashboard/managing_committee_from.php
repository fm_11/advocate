<?php
if($action == 'edit'){
    ?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/managing_committee_edit" method="post" enctype="multipart/form-data">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $members[0]['name']; ?>" name="txtName" required="1"/>

        <label>Post</label>
        <input type="text" autocomplete="off"  class="smallInput wide"  value="<?php echo $members[0]['post']; ?>" name="txtPost" required="1"/>

        <label>Mobile</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $members[0]['mobile']; ?>" name="txtMobile"  required="1"/>
        <input type="hidden" class="smallInput wide" value="<?php echo $members[0]['id']; ?>" name="id"  required="1"/>

        <label>File</label>
        <input type="file" name="txtPhoto" class="smallInput"> * File Format -> JPEG , JPG and PNG.

        <br>
        <br>
        <input type="submit" class="submit" value="Update">
    </form><br />

<?php
}else{
    ?>


    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/managing_committee_add" method="post" enctype="multipart/form-data">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtName" required="1"/>
		
		 <label>Post</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtPost" required="1"/>

        <label>Mobile</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtMobile"  required="1"/>

        <label>File</label>
        <input type="file" name="txtPhoto" required="1" class="smallInput"> * File Format ->JPEG , JPG and PNG.

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />

<?php
}
?>

<div class="clear"></div><br />
