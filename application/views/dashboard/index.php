<style>
	.custom_text_cls{
		font-weight: bold !important;
		color: #000000 !important;
	}
	.class_wise_student_border{
		border: 6px <?php echo $dashboard_color_info->class_wise_student_border_color; ?> solid !important;
	}
	.gender_wise_student_border{
		border: 6px <?php echo $dashboard_color_info->gender_wise_wise_border_color; ?> solid !important;
	}
	.income_vs_expense_border{
		border: 6px <?php echo $dashboard_color_info->income_vs_expense_border_color; ?> solid !important;
	}
	.last_7_days_border{
		border: 6px <?php echo $dashboard_color_info->last_7_days_collection_border_color; ?> solid !important;
	}
</style>

<?php
$session_user = $this->session->userdata('user_info');
$logo_name = PUBPATH . MEDIA_FOLDER . '/logos/' . $session_user[0]->contact_info[0]['picture'];
if(file_exists($logo_name)){
	//echo 88;die;
	$logo_name = base_url() . MEDIA_FOLDER . '/logos/' . $session_user[0]->contact_info[0]['picture'];
}else{
	$logo_name = base_url() . 'core_media/admin_v3/images/school360-logo.png';
}
$teacher_image = "core_media/images/teacher-big.png";
if (isset($session_user[0]->photo_location)) {
	$teacher_image = MEDIA_FOLDER . "/logos/" . $session_user[0]->photo_location;
}

?>

<div class="row">
	<div class="col-md-7 grid-margin stretch-card">
		<div class="card" style="border: 12px <?php echo $dashboard_color_info->school_name_border_color; ?> solid !important;">
			<div class="card-body">
				<div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
					<img src="<?php echo $logo_name ; ?>" class="rounded" style="height: 92px;" alt="profile image">
					<div class="ml-sm-3 ml-md-0 ml-xl-3 mt-2 mt-sm-0 mt-md-2 mt-xl-0">
						<h6 class="mb-0">
							<?php
							if ($this->session->userdata('site_lang') == 'bangla') {
								echo $session_user[0]->contact_info[0]['school_name_bangla'];
							} else {
								echo $session_user[0]->contact_info[0]['school_name'];
							}
							?>
						</h6>
						<p class="text-muted mb-1">
							<!-- <?php
							echo 'Year : ' .  $session_user[0]->config_info[0]['software_current_year'];
							?> -->
						</p>
						<p class="mb-0 text-success font-weight-bold">
							<!-- Institute ID : <?php echo $session_user[0]->contact_info[0]['school_short_name']; ?> -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5 grid-margin stretch-card">
		<div class="card" style="border: 12px <?php echo $dashboard_color_info->head_name_border_color; ?> solid !important;">
			<div class="card-body">
				<div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
					<img src="<?php echo base_url() . $teacher_image; ?>" class="rounded" style="height: 92px;" alt="profile image">
					<div class="ml-sm-3 ml-md-0 ml-xl-3 mt-2 mt-sm-0 mt-md-2 mt-xl-0">
						<h6 class="mb-0">
							<?php

								echo $session_user[0]->name;

							?>
						</h6>
						<p class="mb-0 text-success font-weight-bold">
							<?php

								echo $session_user[0]->mobile;

							?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-xl-12 d-flex grid-margin stretch-card">
	<div class="card">
		<div class="card-body" style="border:15px <?php echo $dashboard_color_info->today_attendance_border_color; ?> solid;">
				<h3 class="text-dark card-title">  <?php echo $this->lang->line('today_attendance'); ?></h3>
				<div class="table-responsive">
					<table class="table">
						<thead>
								<tr>
										<th class="border-0 pt-0 pl-0">
												<?php echo $this->lang->line('sl'); ?>
										</th>
										<th class="border-0 pt-0">
												<?php echo $this->lang->line('todays').' '.$this->lang->line('date'); ?>
										</th>
										<th class="border-0 pt-0">
												<?php echo $this->lang->line('case_no'); ?>
										</th>
										<th class="border-0 pt-0">
												<?php echo $this->lang->line('upazila'); ?>
										</th>
										<th class="border-0 pt-0">
											<?php echo  $this->lang->line('court_details');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('petitioner');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('defendant').' '.$this->lang->line('party');?>
										</th>

										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('reason_for_assignment');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('file_colour');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('first_hearing_date');?>
										</th>
									</tr>
						</thead>
						<tbody>
							<?php $i=1; foreach ($today_attendace_list as $row): ?>
								<tr>
									<td class="pl-0">
											<?php echo $i;?>
									</td>
									<td>
										<?php echo date("d-m-Y", strtotime($row['befor_date']));?>

									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['case_sub_type_bname'].'-'.$row['case_no'];
										} else {
											echo $row['case_sub_type_name'].'-'.$row['case_no'];
										}
										?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['upazila_bnname'];
										} else {
											echo $row['upazila_name'];
										}
										?>

									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['court_bn_name'];
										} else {
											echo $row['court_name'];
										}
										?>

									</td>
									<td>
										<?php
										if($row['is_petitioner']=='1')
										{
											if(empty($row['total_person']))
											{
												echo $row['parties_name'];
											}else{
												echo $row['parties_name'].'('.$row['total_person'].')';
											}

										}
											?>
									</td>
									<td>
										<?php
										 if($row['is_petitioner']=='0')
										 {
											 if(empty($row['total_person']))
											 {
												 echo $row['parties_name'];
											 }else{
												 echo $row['parties_name'].'('.$row['total_person'].')';
											 }

										 }
									?>

									</td>

									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['status_bn_name'];
										} else {
											echo $row['status_name'];
										}
										?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['bn_file_colour'];
										} else {
											echo $row['file_colour'];
										}
										?>
									</td>
									<td>
											<?php
											if(!empty($row['after_date']))
											{
												 echo date("d-m-Y", strtotime($row['after_date']));
											}

											 ?>

									</td>
								</tr>
								<?php $i++;?>
							<?php endforeach; ?>


						</tbody>
					</table>
				</div>

					<a class="btn btn-link text-secondary p-0 mt-4 font-weight-bold" href="<?php echo base_url();?>todays_attendance/index">  <?php echo  $this->lang->line('read_more');?></a>
		</div>
	</div>
</div>

<div class="col-xl-12 d-flex grid-margin stretch-card">
	<div class="card">
		<div class="card-body" style="border:15px <?php echo $dashboard_color_info->today_attendance_border_color; ?> solid;">
				<h3 class="text-dark card-title">  <?php echo $this->lang->line('tomorrow_attendence'); ?></h3>
				<div class="table-responsive">
					<table class="table">
						<thead>
								<tr>
										<th class="border-0 pt-0 pl-0">
												<?php echo $this->lang->line('sl'); ?>
										</th>
										<th class="border-0 pt-0">
												<?php echo $this->lang->line('todays').' '.$this->lang->line('date'); ?>
										</th>
										<th class="border-0 pt-0">
												<?php echo $this->lang->line('case_no'); ?>
										</th>
										<th class="border-0 pt-0">
												<?php echo $this->lang->line('upazila'); ?>
										</th>
										<th class="border-0 pt-0">
											<?php echo  $this->lang->line('court_details');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('petitioner');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('defendant').' '.$this->lang->line('party');?>
										</th>

										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('reason_for_assignment');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('file_colour');?>
										</th>
										<th class="border-0 pt-0">
												<?php echo  $this->lang->line('first_hearing_date');?>
										</th>
									</tr>
						</thead>
						<tbody>
							<?php $i=1; foreach ($tomorrow_attendace_list as $row): ?>
								<tr>
									<td class="pl-0">
											<?php echo $i;?>
									</td>
									<td>
										<?php echo date("d-m-Y", strtotime($row['befor_date']));?>

									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['case_sub_type_bname'].'-'.$row['case_no'];
										} else {
											echo $row['case_sub_type_name'].'-'.$row['case_no'];
										}
										?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['upazila_bnname'];
										} else {
											echo $row['upazila_name'];
										}
										?>

									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['court_bn_name'];
										} else {
											echo $row['court_name'];
										}
										?>

									</td>
									<td>
										<?php
										if($row['is_petitioner']=='1')
										{
											if(empty($row['total_person']))
											{
												echo $row['parties_name'];
											}else{
												echo $row['parties_name'].'('.$row['total_person'].')';
											}

										}
											?>
									</td>
									<td>
										<?php
										 if($row['is_petitioner']=='0')
										 {
											 if(empty($row['total_person']))
											 {
												 echo $row['parties_name'];
											 }else{
												 echo $row['parties_name'].'('.$row['total_person'].')';
											 }

										 }
									?>

									</td>

									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['status_bn_name'];
										} else {
											echo $row['status_name'];
										}
										?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['bn_file_colour'];
										} else {
											echo $row['file_colour'];
										}
										?>
									</td>
									<td>
											<?php
											if(!empty($row['after_date']))
											{
												 echo date("d-m-Y", strtotime($row['after_date']));
											}

											 ?>

									</td>
								</tr>
								<?php $i++;?>
							<?php endforeach; ?>


						</tbody>
					</table>
				</div>

					<a class="btn btn-link text-secondary p-0 mt-4 font-weight-bold" href="<?php echo base_url();?>todays_attendance/tomorrow">  <?php echo  $this->lang->line('read_more');?></a>
		</div>
	</div>
</div>

<div class="row  mt-3">
	<div class="col-xl-4 d-flex grid-margin stretch-card">
		<div class="card">
			<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->support_center_border_color; ?> solid">
				<div class="d-flex flex-wrap justify-content-between">
					<h2 class="mb-0 extra-large-title mt-1">
						Support Center
					</h2>
					<div class="badge badge-pill badge-success"></div>
				</div>
				<!-- <p class="mt-2"><?php echo date('F Y'); ?></p> -->
				<!-- <div class="d-lg-flex align-items-baseline justify-content-between mt-3"> -->

						<label class="badge badge-success">
							<a style="color: #ffffff;font-size: 15px;" href="tel:01768711081">01768711081</a> <a  style="color: #ffffff;font-size: 15px;" href="tel:01721736226">& 01721736226</a>
						</label>
						<br>
						<label class="badge badge-danger">
							<a  style="color: #ffffff;font-size: 15px;" href="tel:01777871596">01777871596</a> <a style="color: #ffffff;font-size: 15px;"  href="tel:01765175644">& 01765175644</a>
						</label>

				<!-- </div> -->

			</div>
		</div>
	</div>
		<div class="col-xl-4 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->total_running_case_border_color; ?> solid">
					<div class="d-flex flex-wrap justify-content-between">
						<h2 class="mb-0 extra-large-title mt-1">
						<?php echo $this->lang->line('total_running_case'); ?>
						</h2>
						<div class="badge badge-pill badge-success"></div>
					</div>
					<p class="mt-2"><?php echo date('F Y'); ?></p>
					<div class="d-lg-flex align-items-baseline justify-content-between mt-3">
						<div class="d-flex align-items-center">
							<div class="icon-bg-square">
								<a href="<?php echo base_url(); ?>cases/index">
								<i class="ion ion-ios-cash"></i>
							</a>
							</div>
							<h1 class="text-dark font-weight-bold">
								<?php
								if ($total_running_case == '') {
									echo '0';
								} else {
									echo $total_running_case;
								};
								?>
							</h1>
						</div>
					</div>
					<!-- <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><?php echo $this->lang->line('view_reort'); ?></button> -->
				</div>
			</div>
		</div>
		<div class="col-xl-4 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->total_colsed_case_border_color; ?> solid">
					<div class="d-flex flex-wrap justify-content-between">
						<h2 class="mb-0 extra-large-title mt-1">
								<?php echo $this->lang->line('total_colsed_case'); ?>
						</h2>
						<div class="badge badge-pill badge-success"></div>
					</div>
					<p class="mt-2"><?php echo date('F Y'); ?></p>
					<div class="d-lg-flex align-items-baseline justify-content-between mt-3">
						<div class="d-flex align-items-center">
							<div class="icon-bg-square">
								<a href="<?php echo base_url(); ?>case_closed/index">
								<i class="ion ion-ios-cash"></i>
							</a>
							</div>
							<h1 class="text-dark font-weight-bold">
								<?php if ($total_colsed_case == '') {
									echo '0';
								} else {
									echo $total_colsed_case;
								}; ?>
							</h1>
						</div>
					</div>
					<!-- <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><?php echo $this->lang->line('view_reort'); ?></button> -->
				</div>
			</div>
		</div>
</div>

<div class="row">
	<div class="col-lg-4 col-sm-12 flex-column d-flex grid-margin stretch-card">
		<div class="row flex-grow">
			<div class="col-sm-12  stretch-card grid-margin">
				<div class="card" style="border: 15px <?php echo $dashboard_color_info->total_missaed_attendance_border_color; ?> solid;">
					<div class="card-body">
						<div class="d-lg-flex justify-content-between">
							<h3 class="text-dark card-title">
										<?php echo $this->lang->line('total_missaed_attendance'); ?>
							</h3>
							<h3 class="text-dark font-weight-bold">

							</h3>
						</div>
						<div class="mt-2 custom_text_cls">
							<?php
							if ($total_missed_attendance == '') {
								echo '0';
							} else {
								echo $total_missed_attendance;
							};
							?>
						</div>
						<button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><a href="<?php echo base_url(); ?>missed_attendance/index"> <?php echo $this->lang->line('view_reort'); ?></a></button>
					</div>

				</div>
			</div>

		</div>
	</div>

	<div class="col-lg-4 col-sm-12 flex-column d-flex grid-margin stretch-card">
		<div class="row flex-grow">
			<div class="col-sm-12  stretch-card grid-margin">
				<div class="card" style="border: 15px <?php echo $dashboard_color_info->sms_balance_border_color; ?> solid;">
					<div class="card-body">
						<div class="d-lg-flex justify-content-between">
							<h3 class="text-dark card-title">
								<?php echo $this->lang->line('sms') . ' ' . $this->lang->line('balance'); ?> (<?php echo $this->lang->line('tk'); ?>)
							</h3>
							<h3 class="text-dark font-weight-bold">
								<?php echo $sms_balance_tk; ?>
							</h3>
						</div>
						<div class="mt-2 custom_text_cls">
							<?php
							echo $this->lang->line('total') . ' ' . $this->lang->line('sms'); ?>
							= <?php echo $sms_quantity; ?> | Rate = <?php echo $sms_rate;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-sm-12 flex-column d-flex grid-margin stretch-card">
		<div class="row flex-grow">
			<div class="col-sm-12  stretch-card grid-margin">
				<div class="card" style="border: 15px <?php echo $dashboard_color_info->total_client_border_color; ?> solid;">
					<div class="card-body">
						<div class="d-lg-flex justify-content-between">
							<h3 class="text-dark card-title">
									<?php echo $this->lang->line('total_clients'); ?>
							</h3>
							<h3 class="text-dark font-weight-bold">

							</h3>
						</div>
						<div class="mt-2 custom_text_cls">
							<?php
							if ($total_clients == '') {
								echo '0';
							} else {
								echo $total_clients;
							};
							?>
						</div>
						<button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><a href="<?php echo base_url(); ?>clients/index"> <?php echo $this->lang->line('view_reort'); ?></a></button>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- <div class="col-xl-8 d-flex grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h3 class="text-dark card-title"><?php echo $this->lang->line('attendance') . ' ' . $this->lang->line('overview'); ?></h3>
				<div class="owl-carousel owl-theme detailed-report">
					<div class="item">
						<div class="row">
							<div class="col-sm-6">
								<div class="detailed-report-wrap" style="background-color:<?php echo $dashboard_color_info->total_student_background_color; ?>;">
									<div class="title text-dark"><?php echo $this->lang->line('total') . ' ' . $this->lang->line('employee'); ?></div>
									<div class="text-date custom_text_cls">
										<?php echo date("F j, Y"); ?>
									</div>
									<div class="d-lg-flex justify-content-between mb-3">
										<h3 class="text-dark mb-0 font-weight-medium"><?php echo $total_student; ?></h3>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="detailed-report-wrap" style="background-color:<?php echo $dashboard_color_info->present_student_background_color; ?>;">
									<div class="title text-dark"><?php echo $this->lang->line('present') . ' ' . $this->lang->line('employee'); ?></div>
									<div class="text-date custom_text_cls">
										<?php echo date("F j, Y"); ?>
									</div>
									<div class="d-lg-flex justify-content-between mb-3">
										<h3 class="text-dark mb-0 font-weight-medium"><?php echo $total_student_login; ?></h3>
									</div>
								</div>
							</div>
						</div>
						<div class="row">

							<div class="col-sm-6">
								<div class="detailed-report-wrap" style="background-color:<?php echo $dashboard_color_info->absent_student_background_color; ?>;">
									<div class="title text-dark"><?php echo $this->lang->line('absent') . ' ' . $this->lang->line('employee'); ?></div>
									<div class="text-date custom_text_cls">
										<?php echo date("F j, Y"); ?>
									</div>
									<div class="d-lg-flex justify-content-between mb-3">
										<h3 class="text-dark mb-0 font-weight-medium"><?php echo $total_student - $total_student_login; ?></h3>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="detailed-report-wrap" style="background-color:<?php echo $dashboard_color_info->leave_student_background_color; ?>;">
									<div class="title text-dark"><?php echo $this->lang->line('leave') . ' ' . $this->lang->line('employee'); ?></div>
									<div class="text-date custom_text_cls">
										<?php echo date("F j, Y"); ?>
									</div>
									<div class="d-lg-flex justify-content-between mb-3">
										<h3 class="text-dark mb-0 font-weight-medium">0</h3>
									</div>
								</div>
							</div>

						</div>
					</div>



				</div>
			</div>
		</div>
	</div> -->
</div>
<div class="row  mt-3">
	<div class="col-xl-4 d-flex grid-margin stretch-card">
		<div class="card">
			<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->total_importan_case_border_color; ?> solid">
				<div class="d-flex flex-wrap justify-content-between">
					<h2 class="mb-0 extra-large-title mt-1">
							<?php echo $this->lang->line('total_importan_case'); ?>
					</h2>
					<div class="badge badge-pill badge-success"></div>
				</div>
				<p class="mt-2"><?php echo date('F Y'); ?></p>
				<div class="d-lg-flex align-items-baseline justify-content-between mt-3">
					<div class="d-flex align-items-center">
						<div class="icon-bg-square">
							<i class="ion ion-ios-cash"></i>
						</div>
						<h1 class="text-dark font-weight-bold">
							<?php if ($total_important_case == '') {
								echo '0';
							} else {
								echo $total_important_case;
							}; ?>
						</h1>
					</div>
				</div>
				<!-- <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><?php echo $this->lang->line('view_reort'); ?></button> -->
			</div>
		</div>
	</div>
</div>
<!-- <input type="hidden" id="students_chart_level" value="<?php echo $students_chart_level; ?>">
<input type="hidden" id="students_chart_color"  value="<?php echo $students_chart_color; ?>">
<input type="hidden" id="students_chart_value"  value="<?php echo $students_chart_value; ?>"> -->



<!-- <div class="row">

	<?php if (isset($general_config->income_vs_expense_graph) && $general_config->income_vs_expense_graph == '1') { ?>
		<div class="col-lg-6 grid-margin stretch-card">
			<div class="card income_vs_expense_border">
				<div class="card-body">
					<input type="hidden" id="i_vs_e_income_data" value="<?php echo rtrim($i_vs_e_income_data, ','); ?>">
					<input type="hidden" id="i_vs_e_exense_data" value="<?php echo rtrim($i_vs_e_exense_data, ','); ?>">
					<h4 class="card-title"><?php echo $this->lang->line('income') . ' vs ' . $this->lang->line('expense'); ?> (<?php echo date('Y'); ?>)</h4>
					<canvas id="areachart-multi"></canvas>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if (isset($general_config->is_last_7_days_collection_graph) && $general_config->is_last_7_days_collection_graph == '1') { ?>
		<div class="col-xl-6 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-6 last_7_days_border">
					<input type="hidden" id="last_7_days_graph_value" value="<?php echo $last_7_days_graph_value; ?>">
					<textarea id="last_7_days_graph_level" style="display:none;"><?php echo $last_7_days_graph_level; ?></textarea>
					<h4 class="card-title"><?php echo $this->lang->line('last_7_days_collection'); ?></h4>
					<canvas id="barChart"></canvas>
				</div>
			</div>
		</div>

	<?php } ?>

</div>




<div class="row  mt-3">
	<?php if (isset($general_config->todays_collection) && $general_config->todays_collection == '1') { ?>
		<div class="col-xl-4 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->todays_collection_border_color; ?> solid">
					<div class="d-flex flex-wrap justify-content-between">
						<h2 class="mb-0 extra-large-title mt-1">
							<?php echo ($this->session->userdata('site_lang') == 'english')?'Todays Collection':'আজকের আয়';?>
						</h2>
						<div class="badge badge-pill badge-success">৳</div>
					</div>
					<p class="mt-2"><?php echo date("F j, Y, g:i a"); ?></p>
					<div class="d-lg-flex align-items-baseline justify-content-between mt-3">
						<div class="d-flex align-items-center">
							<div class="icon-bg-square">
								<i class="ion ion-ios-cash"></i>
							</div>
							<h1 class="text-dark font-weight-bold">
								<?php
								if ($total_collection_amount == '') {
									echo '0';
								} else {
									echo $total_collection_amount;
								};
								?>
							</h1>
						</div>
					</div>
					<button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><?php echo $this->lang->line('view_reort'); ?></button>
				</div>
			</div>
		</div>
	<?php } if (isset($general_config->monthly_collection) && $general_config->monthly_collection == '1') { ?>
		<div class="col-xl-4 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->monthly_collection_border_color; ?> solid">
					<div class="d-flex flex-wrap justify-content-between">
						<h2 class="mb-0 extra-large-title mt-1">
							<?php echo ($this->session->userdata('site_lang') == 'english')?'Monthly Collection':'মাসিক আয়';?>
						</h2>
						<div class="badge badge-pill badge-success">৳</div>
					</div>
					<p class="mt-2"><?php echo date('F Y'); ?></p>
					<div class="d-lg-flex align-items-baseline justify-content-between mt-3">
						<div class="d-flex align-items-center">
							<div class="icon-bg-square">
								<i class="ion ion-ios-cash"></i>
							</div>
							<h1 class="text-dark font-weight-bold">
								<?php if ($total_monthly_collection_amount == '') {
									echo '0';
								} else {
									echo $total_monthly_collection_amount;
								}; ?>
							</h1>
						</div>
					</div>
					<button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><?php echo $this->lang->line('view_reort'); ?></button>
				</div>
			</div>
		</div>
	<?php } if (isset($general_config->monthly_expense) && $general_config->monthly_expense == '1') { ?>
		<div class="col-xl-4 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-4" style="border:15px <?php echo $dashboard_color_info->monthly_expense_border_color; ?> solid">
					<div class="d-flex flex-wrap justify-content-between">
						<h2 class="mb-0 extra-large-title mt-1">
							<?php echo ($this->session->userdata('site_lang') == 'english')?'Monthly Expenses':'মাসিক ব্যয়';?>
						</h2>
						<div class="badge badge-pill badge-danger">৳</div>
					</div>
					<p class="mt-2"><?php echo date('F Y'); ?></p>
					<div class="d-lg-flex align-items-baseline justify-content-between mt-3">
						<div class="d-flex align-items-center">
							<div class="icon-bg-square">
								<i class="ion ion-ios-document"></i>
							</div>
							<h1 class="text-dark font-weight-bold">
								<?php if ($total_monthly_expense_amount == '') {
									echo '0';
								} else {
									echo $total_monthly_expense_amount;
								}; ?>
							</h1>
						</div>
					</div>
					<button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold"><?php echo $this->lang->line('view_reort'); ?></button>
				</div>
			</div>
		</div>
	<?php } ?>
</div> -->
<div class="row">
            <!-- <div class="col-xl-4 d-flex grid-margin stretch-card">
              <div class="card">
                <div class="card-body" style="border:15px #F36368 solid" >
                    <h3 class="text-dark card-title">New user Registrations</h3>
                    <div class="table-responsive">
                      <table class="table">
                        <tbody>
                          <tr>
                            <td class="border-0 pt-0 pl-0">
                              <div class="d-flex">
                                <div class="mr-3">
                                  <img class="img-sm rounded-circle mb-md-0" src="../../images/faces/face2.jpg" alt="profile image">
                                </div>
                                <div>
                                  <h6 class="font-weight-bold text-dark mb-1">Eliza Hawkins</h6>
                                  <div>Grenada</div>
                                </div>
                              </div>
                            </td>
                            <td class="border-0 pt-0">
                              <button type="button" class="btn btn-secondary btn-sm">More</button>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div> -->

          </div>
