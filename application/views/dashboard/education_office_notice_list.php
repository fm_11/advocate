<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Title</th>
        <th width="200" scope="col">Date</th>
        <th width="200" scope="col">Notice</th>      
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($notices as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row->title; ?></td>
            <td><?php echo $row->date; ?></td>
            <td>
                <a href="<?php echo $eo_domain; ?>media/notice/<?php echo $row->url; ?>"
                   target="_blank"><?php echo $row->url; ?></a>
            </td>         
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="5" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>