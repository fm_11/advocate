<?php
$name = $this->session->userdata('name');
$bn_name = $this->session->userdata('bn_name');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>divisions/index">
<div class="col-md-offset-2 col-md-12">
        <label class="sr-only" for="Name"><?php echo $this->lang->line('name'); ?></label>
        <?php
         $placeholder = $this->lang->line('division_name_english');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($roll)) {
            echo $name;
        } ?>" class="form-control" id="name">
<!--
        <label class="sr-only" for="Bn_Name"><?php echo $this->lang->line('bn_name'); ?></label>
        <?php
         $placeholder = $this->lang->line('judge');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="bn_name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($roll)) {
            echo $bn_name;
        } ?>" class="form-control" id="bn_name"> -->
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?></th>
        <th><?php echo $this->lang->line('bn_name'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($divisions as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td >
                <?php echo $i; ?>
            </td>

               <td><?php echo $row['name']; ?></td>
               <td><?php echo $row['bn_name']; ?></td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
