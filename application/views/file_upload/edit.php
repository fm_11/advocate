<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>file_upload/edit" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('form_type'); ?><span class="required_label">*</span></label>
      <select   name="forms_types_id" class="js-example-basic-single w-100" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($forms_types as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($files->forms_types_id)) {
           if ($row['id'] == $files->forms_types_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $files->name; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('file_upload'); ?></label>
      <input type="file" autocomplete="off"  class="form-control" name="txtPhoto" />
        <input type="hidden" autocomplete="off"  class="form-control" name="oldtxtPhoto" value="<?php echo $files->file_path; ?>" />
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $files->id; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
<div class="form-row">

  <div class="form-group col-md-8">
  <img style="height: 100% !important;width: 100% !important;" src="<?php echo base_url(); ?>core_media/file_upload/<?php echo $files->file_path; ?>" alt="not found" >

  </div>
</div>
