<?php
$name = $this->session->userdata('name');
$forms_types_id = $this->session->userdata('forms_types_id');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>file_upload/index">

<div class="col-md-offset-2 col-md-12">
        <?php
         $placeholder = $this->lang->line('file').' '.$this->lang->line('name');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($name)) {
            echo $name;
        } ?>" class="form-control" id="name">


        <select   name="case_type_id" class="form-control mb-1 mr-sm-1 " style="max-width: 250px;">
          <option value="">-- <?php echo $this->lang->line('form_type'); ?> --</option>
          <?php foreach ($forms_types as $row) { ?>
            <option value="<?php echo $row['id']; ?>"<?php if (isset($forms_types_id)) {
             if ($row['id'] == $forms_types_id) {
               echo 'selected';
             }
           } ?>><?php echo $row['name']; ?></option>
          <?php } ?>
        </select>
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>

</form>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('form_type'); ?></th>
        <th scope="col"><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?></th>
        <th scope="col"><?php echo $this->lang->line('file'); ?></th>
        <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($file_upload_list as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td><?php echo $i; ?></td>
            <td><?php if($language=="bangla"){ echo $row['bn_name'];}else{ echo $row['form_type_name'];} ?></td>
            <td><?php echo $row['name']; ?></td>
            <td>
              <?php if(isset($row['file_path']) && $row['file_path']!='0'){?>
                 <a  target="_blank" href="<?php echo base_url(); ?>core_media/file_upload/<?php echo $row['file_path']; ?>">
                   <button type="button" class="btn btn-secondary btn-xs mb-1"><?php echo $this->lang->line('download'); ?></button>
                 </a>
               <?php }?>
            </td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">

                         <a class="dropdown-item" href="<?php echo base_url(); ?>file_upload/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>file_upload/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
