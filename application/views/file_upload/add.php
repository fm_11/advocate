<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>file_upload/add" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4"><?php echo $this->lang->line('form_type'); ?><span class="required_label">*</span></label>
      <select   name="forms_types_id" class="js-example-basic-single w-100" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($forms_types as $row) { ?>
          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('file').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('file_upload'); ?><span class="required_label">*</span></label>
      <input type="file" autocomplete="off"  class="form-control" name="txtPhoto" required="1"/>
    </div>

  </div>
    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
