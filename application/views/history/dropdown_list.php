
  <form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>history/index/<?php echo $case_master_id;?>" class="cmxform" enctype="multipart/form-data" method="post" onsubmit=" return getDistrictByCountryId();">
  <input type="hidden" name="last_bussines_on_date" id="last_bussines_on_date" value="<?php echo date("d-m-Y", strtotime($last_cast_history->bussines_on_date)); ?>" />
  <input type="hidden" name="last_id" value="<?php echo $last_cast_history->id;?>" />
  <input type="hidden" name="case_master_id" value="<?php echo $last_cast_history->case_master_id;?>" />
  <input type="hidden" name="district_id" id="district_id" value="<?php echo $last_cast_history->district_id;?>" />
  <input type="hidden" name="upazilla_id" id="upazilla_id" value="<?php echo $last_cast_history->upazilla_id;?>" />
  <input type="hidden" name="case_sub_type_id" id="case_sub_type_id" value="<?php echo $last_cast_history->case_sub_type_id;?>" />

  <input type="hidden" name="parties_name" value="<?php echo $last_cast_history->parties_name;?>" />
    <fieldset>
       <div class="form-row">

         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('previous_hearing_date'); ?><span class="required_label">*</span></label>
            <input type="text" autocomplete="off" disabled class="form-control"   value="<?php echo date("d-m-Y", strtotime($last_cast_history->bussines_on_date)); ?>">
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('case_no'); ?><span class="required_label">*</span></label>
            <input type="text" autocomplete="off" class="form-control" name="case_no"   value="<?php echo $last_cast_history->case_no;?>">
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('parties_name'); ?><span class="required_label">*</span></label>
            <input type="text" autocomplete="off"  class="form-control" id="parties_name"  name="parties_name" disabled value="<?php echo $last_cast_history->parties_name;?>">
         </div>

        </div>


           <div class="form-row">
             <div class="form-group col-md-4">
               <label for="inputEmail4"><?php echo $this->lang->line('first_hearing_date'); ?><span class="required_label">*</span></label>
               <div class="input-group date">
                       <input type="text" autocomplete="off"  name="bussines_on_date" id="bussines_on_date" required class="form-control" placeholder="dd-mm-yyyy" >
                       <span class="input-group-text input-group-append input-group-addon">
                           <i class="simple-icon-calendar"></i>
                       </span>
               </div>
             </div>

             <div class="form-group col-md-4">
               <label for="inputEmail4"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?><span class="required_label">*</span></label>
               <select name="case_status_id" class="js-example-basic-single w-100" required="required">
                 <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                 <?php foreach ($case_status as $row) { ?>
                   <option value="<?php echo $row['id']; ?>"<?php if (isset($last_cast_history->case_status_id)) {
                    if ($row['id'] == $last_cast_history->case_status_id) {
                      echo 'selected';
                    }
                  } ?>><?php echo $row['name']; ?></option>
                 <?php } ?>
               </select>
             </div>
             <div class="form-group col-md-4">
               <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></label>
               <input type="text" autocomplete="off"  disabled class="form-control"  value="<?php echo $last_cast_history->mobile;?>"/>
             </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputEmail4"><?php echo $this->lang->line('courts').' '.$this->lang->line('police').' '.$this->lang->line('station'); ?></label>
                 <input type="text" autocomplete="off" disabled class="form-control"   value="<?php if($language=="bangla"){echo $last_cast_history->upazila_bn_name;}else{ echo $last_cast_history->upazila_name;}?>">
              </div>

              <div class="form-group col-md-8">
                <label for="inputEmail4"><?php echo $this->lang->line('remarks'); ?></label>
                <input type="text" autocomplete="off"  class="form-control" maxlength="200" id="remarks"  name="remarks"  value="<?php echo $last_cast_history->remarks;?>">
              </div>

             </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputEmail4"><?php echo $this->lang->line('court_type'); ?><span class="required_label">*</span></label>
                <select onchange="getCourtTypeByCourtId(this.value)"  name="court_type_id" id="court_type_id" class="js-example-basic-single w-100" required="required">
                  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                  <?php foreach ($courts_type as $row) { ?>
                    <option value="<?php echo $row['id']; ?>"<?php if (isset($last_cast_history->court_type_id)) {
                     if ($row['id'] == $last_cast_history->court_type_id) {
                       echo 'selected';
                     }
                   } ?>><?php echo $row['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="inputEmail4"><?php echo $this->lang->line('court_sub_type'); ?><span class="required_label">*</span></label>
                <select   name="court_id" id="court_id" class="js-example-basic-single w-100" required="required">
                  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                  <?php foreach ($courts as $row) { ?>
                    <option value="<?php echo $row['id']; ?>"<?php if (isset($last_cast_history->court_id)) {
                     if ($row['id'] == $last_cast_history->court_id) {
                       echo 'selected';
                     }
                   } ?>><?php echo $row['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="inputEmail4"><?php echo $this->lang->line('sms'); ?></label>
                <br> <br>
                 <input type="radio"  name="is_sms" value="1" ><?php echo $this->lang->line('yes'); ?>
                 <input type="radio"  name="is_sms" value="0" checked> <?php echo $this->lang->line('no'); ?>
              </div>
            </div>


    </fieldset>
    <div class="float-right">
      <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
  </form>
