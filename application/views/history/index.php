<script type="text/javascript">
function ajax_get_case_history_by_case_id(case_id){
    //alert(case_id);
    document.getElementById("div_case_history").innerHTML = "";
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("div_case_history").innerHTML = xmlhttp.responseText;
            basic_function_load();
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>history/ajax_get_case_history_by_case_id?case_master_id=" + case_id, true);
    xmlhttp.send();
}
function basic_function_load() {
  $(".date").datepicker({
		autoclose: true,
		todayHighlight: true,
		format: 'dd-mm-yyyy',
	}).datepicker();
  	$(".js-example-basic-single").select2();
}
function getCourtTypeByCourtId(court_id){

    document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
    var district_id=  document.getElementById("district_id").value;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("court_id").innerHTML = xmlhttp.responseText;
            var element = document.getElementById("dateDiv");
                         element.classList.add("date");


        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
    xmlhttp.send();
}
function getDisTrictWiseThanaOrCourt(district_id,type){
   if(type=='T')
   {
     document.getElementById("fir_upazilla_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }else if (type=='C') {
     document.getElementById("court_type_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
     document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
   }

    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
          if(type=='T')
          {
            document.getElementById("fir_upazilla_id").innerHTML = xmlhttp.responseText;
          }else if (type=='C') {
            document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
          }

        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
    xmlhttp.send();
}

    function process(date){
    var parts = date.split("-");
    return new Date(parts[2], parts[1], parts[0]);
    }

    function getDistrictByCountryId(){
      var GivenDate = document.getElementById("last_bussines_on_date").value;
      var CurrentDate = document.getElementById("bussines_on_date").value;
      //alert(GivenDate);
      var date=GivenDate;
      GivenDate = process(GivenDate);
      CurrentDate = process(CurrentDate);

      if(GivenDate < CurrentDate){
          //alert('Next Date is greater than the current date.');
          return true;
      }else{
          alert('Next Date  is not smaller than '+date+' previous next date.');
          return false;
      }
    }

</script>

  <div class="form-row">

    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('case_no'); ?></label><span class="required_label">*</span>
      <select  onchange="ajax_get_case_history_by_case_id(this.value)"  name="case_master_id" id="case_master_id" class="js-example-basic-single w-100" required>
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($case_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>

  </div>


<div id="div_case_history">
</div>
