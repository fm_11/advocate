<script type="text/javascript">
function count_alpha(val){
	var length = val.length;
/*	alert(length);
*/
	var diff = 1000-length;
	document.getElementById("count_alert").innerHTML = diff+" characters left.";

/*	document.getElementById("count_alert").innerHTML = length+" characters.";
*/
}
</script>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>templates/add" method="post">

	<div class="form-row">
		<div class="form-group col-md-8">
			<label><?php echo $this->lang->line('template') . ' ' . $this->lang->line('name'); ?><span class="required_label">*</span></label>
			<input required type="text" name="templateName" class="form-control" value="">
		</div>

		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('bangla') . ' ' . $this->lang->line('sms'); ?> ? <span class="required_label">*</span></label>
			<select class="js-example-basic-single w-100" name="is_bangla_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>

		<!-- <div class="form-group col-md-2">
		<label><?php echo $this->lang->line('admission'); ?> ?</label>
			<select class="form-control" name="is_admission_welcome_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>

		<div class="form-group col-md-2">
		<label><?php echo $this->lang->line('student') . ' ' . $this->lang->line('present'); ?> ?</label>
			<select class="form-control" name="is_present_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div> -->

	</div>

	<!-- <div class="form-row">

		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('student') . ' ' . $this->lang->line('absent'); ?> ?</label>
			<select class="form-control" name="is_absent_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('student') . ' ' . $this->lang->line('leave'); ?> ?</label>
			<select class="form-control" name="is_leave_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('teacher') . ' ' . $this->lang->line('present'); ?> ?</label>
			<select class="form-control" name="is_teacher_present_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('teacher') . ' ' . $this->lang->line('absent'); ?> ?</label>
			<select class="form-control" name="is_teacher_absent_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('teacher') . ' ' . $this->lang->line('leave'); ?> ?</label>
			<select class="form-control" name="is_teacher_leave_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>


		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('bangla') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="form-control" name="is_bangla_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>
	</div> -->

	<div class="form-row">
		<div class="form-group col-md-12">
			<label><?php echo $this->lang->line('template') . ' ' . $this->lang->line('body'); ?><span class="required_label">*</span></label>
			<textarea required name="templateBody" id="sms_content" maxlength="1000"onkeyup="count_alpha(this.value);" rows="5" cols="50" class="form-control"></textarea>
		</div>
	</div>
	<div class="row"><span id="count_alert"></span></div>

  <div class="float-right">
	   <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
