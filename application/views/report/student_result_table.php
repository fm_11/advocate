<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="9" style="text-align:center">
            <br>
            <b style="font-size:20px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="9">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
			<?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
			<?php echo $this->lang->line('shift'); ?>: <b><?php echo $shift_name; ?></b>
        </td>
    </tr>

	 <tr>
        <th width="20" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
        <th width="150" scope="col" rowspan="2" align="center">&nbsp;<?php echo $this->lang->line('student_code'); ?></th>
		<th width="300" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('name'); ?></th>
        <th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('obtain'); ?> <?php echo $this->lang->line('mark'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('mark'); ?> <?php echo $this->lang->line('for'); ?>
     <?php echo $this->lang->line('position'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('gpa'); ?> <?php echo $this->lang->line('without_optional'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('gpa'); ?> <?php echo $this->lang->line('without_optional'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('position'); ?></th>
    </tr>

     </thead>

	  <tbody>
		<?php
		$i = 0;
		foreach ($rdata as $row):

		?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i + 1; ?>
            </td>
            <td align="center">
                &nbsp;<?php echo $row['student_code']; ?><br>
            </td>
			<td>
                &nbsp;<?php echo $row['name']; ?><br>
            </td>
             <td align="center">
                &nbsp;<?php echo $row['roll_no']; ?><br>
            </td>

			<td align="center">
				&nbsp;<?php echo $row['total_obtain_mark']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['calculable_total_mark'] + $row['total_extra_mark']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['gpa_without_optional']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['gpa_with_optional']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php
				if($row['position'] <= 0){
				    echo 'N/A';
				}else{
				    	echo $row['position'];
				}
				?><br>
			</td>
        </tr>
    <?php $i++; endforeach; ?>
    </tbody>

</table>
