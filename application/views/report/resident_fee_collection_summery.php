<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

   $(function () {
        $("#from_date,#to_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/getResidentFeeCollection" method="post">
    <label><?php echo $this->lang->line('class'); ?></label>
    <select class="smallInput" name="class_id" id="class_id">
        <option value="all">-- <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('class'); ?> --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>" <?php if(isset($class_id)){if($class_id == $list['id']){echo 'selected';}} ?>><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>

	<label><?php echo $this->lang->line('from_date'); ?></label>
	<input type="text" autocomplete="off"  name="from_date" required value="<?php if (isset($from_date)) { echo $from_date;
	} ?>" placeholder="yyyy-mm-dd" style="display: -moz-stack !important; width: 150px;"
		   class="smallInput" id="from_date">


	 <label><?php echo $this->lang->line('to_date'); ?></label>
		<input type="text" autocomplete="off"  name="to_date" required value="<?php if (isset($to_date)) { echo $to_date;
		} ?>" placeholder="yyyy-mm-dd" style="display: -moz-stack !important; width: 150px;"
			   class="smallInput" id="to_date">
	<br>
    <input type="submit" class="submit" value="View Report">
    <input type="submit" name="pdf_download" value="PDF Download"/>
    <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
    <br><br>
</form>

<div id="printableArea">
    <?php
    if (isset($rdata)) {
        echo $report;
    }
    ?>
</div>


<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
