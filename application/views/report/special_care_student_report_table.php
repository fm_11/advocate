<table width="100%" <?php if(isset($is_pdf) && $is_pdf == 1){ echo 'border="1"';} ?> cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td colspan="5" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                      <?php echo $title; ?></b>
                </td>
            </tr>

			<tr>
			    <td colspan="5" >
				   <b style="font-size:13px;">
				   Class: <?php echo $class_name; ?>,
				   <?php echo $this->lang->line('section'); ?>: <?php echo $section_name; ?>,
				   <?php echo $this->lang->line('group'); ?>: <?php echo $group_name; ?>,
				   <?php echo $this->lang->line('shift'); ?>: <?php echo $shift_name; ?>,
				   <?php echo $this->lang->line('year'); ?>: <?php echo $year; ?>
				   </b>
				</td>
			</tr>

            <tr>
                <th width="50" scope="col">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
                <th width="200" scope="col">&nbsp;<?php echo $this->lang->line('student_id'); ?></th>
				<th width="200" scope="col">&nbsp;<?php echo $this->lang->line('Name'); ?></th>
				<th width="100" scope="col">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
				<th width="100" scope="col">&nbsp;<?php echo $this->lang->line('Mobile'); ?></th>
            </tr>

            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($idata as $row):
                $i++;
                ?>
                <tr>
                    <td width="34">
                        &nbsp;<?php echo $i; ?>
                    </td>
                    <td>
					&nbsp;<?php echo $row['student_code']; ?>
					</td>
					 <td>
					&nbsp;<?php echo $row['name']; ?>
					</td>
                    <td>&nbsp;<?php echo $row['roll_no']; ?></td>
                    <td>&nbsp;<?php echo $row['guardian_mobile']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
