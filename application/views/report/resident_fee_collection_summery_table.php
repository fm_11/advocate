<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="7" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            Class: <b><?php echo $class_name; ?></b>,
            Date: <b><?php echo date("M jS, Y", strtotime($from_date)); ?> - <?php echo date("M jS, Y", strtotime($to_date)); ?></b>
        </td>
    </tr>

	 <tr>
        <th width="20" scope="col">&nbsp;SL</th>
        <th width="150" scope="col" align="center">&nbsp;<?php echo $this->lang->line('student_code'); ?></th>
		<th width="150" scope="col">&nbsp;<?php echo $this->lang->line('name'); ?></th>
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('month'); ?></th>
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('year'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('receipt_no'); ?>.</th>
		<th width="150" scope="col">&nbsp;<?php echo $this->lang->line('amount'); ?> (<?php echo $this->lang->line('tk'); ?>.)</th>
    </tr>

     </thead>

	  <tbody>
    <?php
    $i = 0;
	$total_amount = 0;
    foreach ($rdata as $row):
        $i++;
        ?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i; ?>
            </td>
            <td>
                &nbsp;<?php echo $row['student_code']; ?><br>
            </td>
			<td>
                &nbsp;<?php echo $row['name']; ?><br>
            </td>
            <td align="center">&nbsp;
			<?php
			$monthNum  = $row['month'];
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			echo $dateObj->format('F'); // March
			?>
			</td>
           <td align="center">&nbsp;<?php echo $row['year']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['receipt_no']; ?></td>
           <td align="right">&nbsp;<?php $total_amount+= $row['amount'];
		   echo number_format($row['amount'],2); ?>&nbsp;
		   </td>
        </tr>
    <?php endforeach; ?>
	   <tr>
	       <td align="right" colspan="6"><b><?php echo $this->lang->line('total'); ?>&nbsp;</b></td>
		   <td align="right"><b><?php echo number_format($total_amount,2); ?> &nbsp;</b></td>
	   </tr>
    </tbody>

</table>
