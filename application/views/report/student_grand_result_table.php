<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="18" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="18">
            Class: <b><?php echo $class_name; ?></b>,
            Section: <b><?php echo $section_name; ?></b>,
			Group: <b><?php echo $group_name; ?></b>,
			Shift: <b><?php echo $shift_name; ?></b>
        </td>
    </tr>

	 <tr>
        <th width="20" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
        <th width="150" scope="col" rowspan="2" align="center">&nbsp;<?php echo $this->lang->line('student_id'); ?></th>
		<th width="150" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('name'); ?></th>
<th width="150" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
		<?php
        $k = 0;
        while ($k < count($exam_list)) {
            echo '<th width="150" scope="col" colspan="2">&nbsp;'. $exam_list[$k]['name']. '<br>(' . $exam_list[$k]['percentage_of_grand_result'] . '%)' .'</th> ';
            $k++;
        }
        ?>

		<th width="150" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('obtain'); ?> <?php echo $this->lang->line('mark'); ?></th>
		<th width="150" scope="col" rowspan="2">&nbsp;Total Average Mark</th>
		<th width="150" scope="col" rowspan="2">&nbsp;Annual Position</th>
		<th width="150" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('grade'); ?></th>
    </tr>
	 <tr>
	  <?php
        $k = 0;
        while ($k < count($exam_list)) {
            echo '<th width="150" scope="col">&nbsp Obtain Mark</th> ';
            echo '<th width="150" scope="col">&nbsp;Average Mark</th> ';
            $k++;
        }
        ?>
	 </tr>

     </thead>

	  <tbody>
		<?php
        $i = 0;
        foreach ($rdata as $row):

        ?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i + 1; ?>
            </td>
            <td>
                &nbsp;<?php echo $row['student_code']; ?><br>
            </td>
			<td>
                &nbsp;<?php echo $row['name']; ?><br>
            </td>
             <td align="center">
                &nbsp;<?php echo $row['roll_no']; ?><br>
            </td>
           <?php
            $k = 0;
            while ($k < count($rdata[$i]['marks_info'])) {
                echo '<td width="150">&nbsp;'.number_format($rdata[$i]['marks_info'][$k]['total_obtain_mark'], 2).'</td> ';
                echo '<td width="150">&nbsp;'.number_format($rdata[$i]['marks_info'][$k]['average_mark'], 2).'</td> ';
                $k++;
            }
            ?>
			<td align="center">
				&nbsp;<?php echo $row['total_mark']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['total_average_mark']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['annual_position']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['grade']; ?><br>
			</td>
        </tr>
    <?php $i++; endforeach; ?>
    </tbody>

</table>
