<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>

<script>
    function exam_by_year(year) {
        if (year != '') {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("exam_space").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>contents/ajax_exam_by_year?year=" + year, true);
            xmlhttp.send();
        } else {
            document.getElementById("exam_space").innerHTML = 'Please Select Year....';
        }
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/studentFailedList" method="post">
    <label><?php echo $this->lang->line('year'); ?></label>
    <select name="year"   class="smallInput"  id="year" required="1" onchange="exam_by_year(this.value)">
        <option value="">-- <?php echo $this->lang->line('select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label><?php echo $this->lang->line('exam'); ?></label>
    <span id="exam_space"><?php echo $this->lang->line('please_select'); ?>....</span>

    <label><?php echo $this->lang->line('/**
     *
     */
    class ClassName extends AnotherClass
    {

      function __construct(argument)
      {
        // code...
      }
    }
    '); ?></label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

 <br><br>
    <input type="submit" class="submit" value="View Report">
    <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" name="pdf_download" value="PDF Download"/>
    <br><br>
</form>

<div id="printableArea">
    <?php
    if (isset($result_data)) {
        echo $report;
    }
    ?>
</div>
