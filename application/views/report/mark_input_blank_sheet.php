<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


    function subject_by_class() {
        var ClassID = document.getElementById("class_id").value;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("SubjectSpace").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>index.php/report/ajax_subject_by_class?class_id=" + ClassID + "&&type=1", true);
        xmlhttp.send();
    }


</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>report/mark_input_blank_sheet" method="post">

    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tbody>
        <tr>

            <td>
                <label><?php echo $this->lang->line('exam'); ?></label>
                <select name="exam_id" style="width: 150px;"  required class="smallInput" id="exam_id">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($exam_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if (isset($exam_id)) {
                            if ($row['id'] == $exam_id) {
                                echo 'selected';
                            }
                        } ?>><?php echo $row['name'].'('. $row['year'] . ')'; ?></option>
                    <?php } ?>
                </select>
            </td>

            <td>
                <label><?php echo $this->lang->line('class'); ?></label>
                <select name="class_id" style="width: 150px;"  onchange="subject_by_class()" required class="smallInput" id="class_id">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($class_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if (isset($class_id)) {
                            if ($row['id'] == $class_id) {
                                echo 'selected';
                            }
                        } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>

            <td>
                <label><?php echo $this->lang->line('section'); ?></label>
                <select name="section_id" style="width: 150px;" required class="smallInput" id="section_id">
                    <option value="all"><?php echo $this->lang->line('all'); ?></option>
                    <?php foreach ($section_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if (isset($section_id)) {
                            if ($row['id'] == $section_id) {
                                echo 'selected';
                            }
                        } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>

            <td>
                <label><?php echo $this->lang->line('group'); ?></label>
                <select name="group" style="width: 150px;" required class="smallInput">
                    <option value="all"><?php echo $this->lang->line('all'); ?></option>
                    <?php foreach ($group_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if (isset($group)) {
                            if ($row['id'] == $group) {
                                echo 'selected';
                            }
                        } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>




        </tr>

        <tr>
             <td>
                <label>Shift</label>
                <select name="shift_id" style="width: 150px;" required class="smallInput" id="shift_id">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($shift_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if (isset($shift_id)) {
                            if ($row['id'] == $shift_id) {
                                echo 'selected';
                            }
                        } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>


            <td colspan="3">
                <label><?php echo $this->lang->line('subject'); ?></label>
                <span id="SubjectSpace">
                    <select name="subject_id" id="subject_id" style="width: 150px;" class="smallInput">
                        <option value="">--<?php echo $this->lang->line('blank'); ?>--</option>
                    </select>
                </span>
            </td>
        </tr>

        <tr>
            <td colspan="4" align="right">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
                <input type="submit" name="pdf_download" value="PDF Download"/>
                <input type="submit" name="excel_download" value="Excel Download"/>
                <input type="submit" class="submit" value="View Report">
            </td>
        </tr>
        </tbody>
    </table>
</form>
<br>


<div id="printableArea">
    <?php
    if (isset($info)) {
        echo $report;
    }
    ?>
</div>
