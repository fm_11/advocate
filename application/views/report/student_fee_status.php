<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/getStudentFeeStatus" method="post">
    <label>Year</label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label>To <?php echo $this->lang->line('month'); ?></label>
	<select class="smallInput" style="width:200px;" name="to_month" id="to_month" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 1;
				while ($i <= 12) {
					$dateObj = DateTime::createFromFormat('!m', $i);
					?>
					<option value="<?php echo $i; ?>"><?php echo $dateObj->format('F'); ?></option>
					<?php
					$i++;
				}
				?>
	</select>

	<label><?php echo $this->lang->line('from_date'); ?> Transaction Date</label>
    <input type="text" autocomplete="off"  class="smallInput" placeholder="YYYY-MM-DD" name="txtFromDate" id="txtFromDate" required="1"/>

    <label><?php echo $this->lang->line('to_date'); ?></label>
    <input type="text" autocomplete="off"  class="smallInput" placeholder="YYYY-MM-DD" name="txtToDate" id="txtToDate" required="1"/>

    <label><?php echo $this->lang->line('class'); ?></label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('section'); ?></label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('group'); ?></label>
    <select name="group" required="1" class="smallInput">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <br>
    <br>
    <input type="submit" class="submit" value="View">
</form>

<script>
    $(function () {
        $("#txtFromDate,#txtToDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
