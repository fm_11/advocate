<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $this->lang->line('receipt'); ?></title>
    <link href="<?php echo base_url() . MEDIA_FOLDER; ?>/receipt_style/receipt.css" rel="stylesheet">

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <style>
        .page_breack {
            page-break-after: always;

        }
    </style>
</head>
<body>
<table width="73%" cellpadding="0" cellspacing="0" id="box-table-a">
    <tr>
        <th width="100%" style="text-align:right" scope="col">
            <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
        </th>
    </tr>
</table>
<br>
    <div id="printableArea">
        <?php
        $i = 0;
        foreach ($report_data as $row) {
        ?>
        <div class="wrapper">
                <table class="header">
                    <tbody>
                    <tr>
                        <td nowrap="nowrap" style="text-align:center;" colspan="2" width="100%">
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <p><img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/receipt_logo_s.png"></p>
                                    </td>

                                    <td>
                                        <strong><?php echo $this->lang->line('pay_to'); ?></strong><br>
                                        <b><?php echo $Info['school_name']; ?></b><br>
                                      <?php echo $this->lang->line('eiin_number'); ?> <?php echo $Info['eiin_number']; ?><br>
                                        <?php echo $Info['web_address']; ?>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td nowrap="nowrap" width="50%">
                           <b><?php echo $this->lang->line('receipt'); ?> <?php echo $this->lang->line('date'); ?>: <?php echo date("d/m/Y", strtotime($row['payment_date'])); ?></b>
                        </td>
                        <td width="50%" align="center">
                            <font class="paid"><?php echo $this->lang->line('receipt'); ?></font><br>
                            (<b><?php echo $row['student_code']; ?></b>)<br>
                            <span style="color:red;"><?php echo $copy_for; ?></span>
                        </td>
                    </tr>
                    </tbody>
                </table>


                <table class="items">
                <thead>
                    <tr>
                        <td colspan="4">
                            <b><?php echo $this->lang->line('name'); ?>: <?php echo $row['student_name']; ?></b>,
                            <?php echo $this->lang->line('class_name'); ?>: <?php echo $Info['ClassName']; ?>,
                            <?php echo $this->lang->line('Section'); ?>: <?php echo $Info['SectionName']; ?>,
                            <?php echo $this->lang->line('roll_no'); ?>: <?php echo $row['roll_no']; ?>

                       </td>
                    </tr>
                </thead>


                <tbody>
                <tr class="title textcenter">
                    <td width="40%"><?php echo $this->lang->line('fee'); ?> <?php echo $this->lang->line('Category'); ?></td>
                    <td width="20%"><?php echo $this->lang->line('allocated'); ?> <?php echo $this->lang->line('fee'); ?></td>
                    <td width="20%"><?php echo $this->lang->line('paid'); ?> <?php echo $this->lang->line('amount'); ?></td>
                    <td width="20%"><?php echo $this->lang->line('due'); ?> <?php echo $this->lang->line('amount'); ?></td>
                </tr>
                

                <?php
                    $total_allocated_fee = 0;
                    $total_due_amount = 0;
                    foreach ($report_data[$i]['fee_item'] as $fee_item_row) {
                ?>
                    <tr>
                        <td align="center"><?php echo $fee_item_row['category_name']; ?></td>
                        <td align="center">
                            <?php
                            $total_allocated_fee = $total_allocated_fee + $fee_item_row['allocated_fee'];
                            echo $fee_item_row['allocated_fee'];
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $fee_item_row['paid_amount']; ?>
                        </td>
                        <td align="center">
                            <?php
                            $total_due_amount = $total_due_amount + $fee_item_row['due_amount'];
                            echo $fee_item_row['due_amount'];
                            ?>
                        </td>
                    </tr>
                <?php } ?>

                <tr class="title">
                    <td class="textright">Total:</td>
                    <td class="textcenter">৳<?php echo number_format($total_allocated_fee, 2); ?></td>
                    <td class="textcenter">৳<?php echo number_format($row['total_paid_amount'], 2); ?></td>
                    <td class="textcenter">৳<?php echo number_format($total_due_amount, 2); ?></td>
                </tr>


                <tr>
                    <td colspan="4">
                        <br> <br>
                        ...............................................<br>
                       <b>Accountant</b>
                    </td>
                </tr>

                </tbody>
            </table>
            Powered by: School360°
        </div><br><br>
        <?php
                if(($i + 1) % 2 == 0){
                    echo '<div class="page_breack">&nbsp;</div>';
                }
                $i++;
            }
        ?>
    </div>
</body>
</html>
