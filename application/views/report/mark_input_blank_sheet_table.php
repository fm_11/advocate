<style>
#pdf_table{
     border-collapse: collapse;
}
#pdf_table td, #pdf_table th {
    border: 1px solid #00000;
    padding: 5px;
}
</style>

    <?php
     if (isset($is_pdf) && $is_pdf == 1) {
    ?>
       <table width="100%" id="pdf_table" style="font-size:13px;" summary="Employee Pay Sheet">
    <?php
     }else{
    ?>
      <table width="100%" <?php if (isset($is_excel) && $is_excel == 1) { ?> border="1" <?php } ?> cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <?php
     }
    ?>

    <thead>
    <tr>
        <td colspan="8" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $this->lang->line('marking') . $this->lang->line('blank') . ' ' . $this->lang->line('sheet'); ?></b>

        </td>
    </tr>

    <tr>
        <td align="center">
            <?php echo $this->lang->line('academic'); ?> <?php echo $this->lang->line('year'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('class'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('section'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('group'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('students'); ?>
        </td>
        <td align="center">
           <?php echo $this->lang->line('shift'); ?>
        </td>
        <td align="center">
           <?php echo $this->lang->line('exam'); ?>
        </td>
        <td align="center">
           <?php echo $this->lang->line('subject'); ?>
        </td>
    </tr>

    <tr>
        <td align="center">
            <b><?php echo date('Y'); ?></b>
        </td>
        <td align="center">
            <b><?php echo $class_name; ?></b>
        </td>
        <td align="center">
            <b><?php echo $section_name; ?></b>
        </td>
        <td align="center">
            <b>
            <?php
             echo $group_name;
            ?>
            </b>
        </td>
        <td align="center">
            <b><?php echo count($info); ?></b>
        </td>
        <td align="center">
           <b><?php echo $shift_name; ?></b>
        </td>
        <td align="center">
           <b><?php echo $exam_info[0]['name']; ?></b>
        </td>
        <td align="center">
            <b>
            <?php
              if(!empty($subject_info)){
                  echo $subject_info[0]['name'];
              }
            ?></b>
        </td>
    </tr>

    <tr>
        <th width="20" scope="col">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
        <th width="150" scope="col">&nbsp;<?php echo $this->lang->line('name'); ?></th>
        <th width="50" scope="col" align="center">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
        <th width="80" scope="col">&nbsp;<?php echo $this->lang->line('class_test'); ?></th>
        <th width="80" scope="col">&nbsp;<?php echo $this->lang->line('creative'); ?></th>
        <th width="80" scope="col">&nbsp;<?php echo $this->lang->line('objective'); ?></th>
        <th width="80" scope="col">&nbsp;<?php echo $this->lang->line('practical'); ?></th>
        <th width="80" scope="col">&nbsp;<?php echo $this->lang->line('students'); ?> <?php echo $this->lang->line('signature'); ?></th>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($info as $row):
        $i++;
        ?>
        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td>
               <b><?php echo $row['name']; ?></b>
            </td>
            <td align="center">&nbsp;<?php echo $row['roll_no']; ?></td>
            <td> &nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> &nbsp;</td>
            <td> &nbsp;&nbsp;</td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
