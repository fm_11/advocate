<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function () {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>report/get_report_list_for_student_info"><span>Student Info.</span></a></li>
    <li><a href="<?php echo base_url(); ?>report/get_report_list_for_resut"><span>Result Report</span></a></li>
    <li><a href="<?php echo base_url(); ?>report/get_report_list_for_student_fees"><span>Student Fees</span></a></li>
	<li><a href="<?php echo base_url(); ?>report/get_accounts_report_list"><span>Accounts Report</span></a></li>
	<li><a href="<?php echo base_url(); ?>report/get_attendance_report_list"><span>Attendance Report</span></a></li>
</ul>

