<select name="subject_id" class="smallInput">
        <option value="">--<?php echo $this->lang->line('blank'); ?>--</option>
        <?php for ($A = 0; $A < count($subject_list); $A++) { ?>
            <option
                value="<?php echo $subject_list[$A]['subject_id']; ?>"><?php echo $subject_list[$A]['name'] . ' [' . $subject_list[$A]['code'] . ']'; ?></option>
        <?php } ?>
</select>
