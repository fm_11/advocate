<style>
#pdf_table{
     border-collapse: collapse;
}
#pdf_table td, #pdf_table th {
    border: 1px solid #00000;
    padding: 5px;
}
</style>
<?php
     if (isset($is_pdf) && $is_pdf == 1) {
    ?>
       <table width="100%" id="pdf_table" style="font-size:13px;" summary="Employee Pay Sheet">
<?php
 }else{
?>
  <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
<?php
 }
?>
    <thead>
    <tr>
        <td class="center_td" colspan="10" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="10">
            <?php echo $this->lang->line('Class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('date'); ?>: <b><?php echo date("M jS, Y", strtotime($from_date)); ?> - <?php echo date("M jS, Y", strtotime($to_date)); ?></b>
        </td>
    </tr>

	 <tr>
        <th width="20" scope="col">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
        <th width="120" scope="col" align="center">&nbsp;<?php echo $this->lang->line('student_id'); ?></th>
		<th width="130" scope="col">&nbsp;<?php echo $this->lang->line('name'); ?></th>
		<th width="130" scope="col">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('Days'); ?></th>
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('working'); ?> <?php echo $this->lang->line('days'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('Present'); ?> <?php echo $this->lang->line('days'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('holydays'); ?></th>
		<th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('leave'); ?></th>
		<th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('absent'); ?></th>
    </tr>

     </thead>
	 
	  <tbody>
    <?php
    $i = 0;
    foreach ($adata as $row):
        $i++;
        ?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i; ?>
            </td>
            <td>
                &nbsp;<?php echo $row['student_code']; ?><br>
            </td>
			<td>
                &nbsp;<?php echo $row['name']; ?><br>
            </td>
          <td>
                &nbsp;<?php echo $row['roll_no']; ?><br>
            </td>
           <td align="center">&nbsp;<?php echo $total_days; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_working_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_present_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_holidays']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_leave_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_absent_days']; ?></td>

        </tr>
    <?php endforeach; ?>

    </tbody>

</table>
