<style>
    .columnHeader {
        font-size: 14px;
        border-bottom: 1px #c7c7c7 solid;
    }
</style>

<html>
<head>
    <title>
        <?php
        echo $title;
        ?>
    </title>
</head>
<body>
<div id="printableArea">
    <table width="40%" align="center" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <td colspan="3" style="text-align:center;">
                <b style="font-size:16px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                <b style="font-size:14px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?></b><br>
                <?php echo $this->lang->line('students'); ?> <?php echo $this->lang->line('fees'); ?> <?php echo $this->lang->line('collection'); ?> <?php echo $this->lang->line('summary'); ?>
                <?php echo $this->lang->line('for_year_of'); ?> <?php echo $HeaderInfo['year']; ?></b><br><br>
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <th colspan="3" style="text-align: center; border-top:  1px #c7c7c7 solid;">
                <b><?php echo $this->lang->line('fees'); ?> <?php echo $this->lang->line('category'); ?></b>
            </th>
        </tr>


        <tr>
            <td class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"
                scope="col">SL
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"
                scope="col">Name
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"
                scope="col">Amount (TK)
            </td>
        </tr>

        <?php
        $j = 0;
        $total = 0;
        $category_wise_total = 0;
        foreach ($category_wise_fee_summery as $row):
            $j++;
            ?>
            <tr>

            <tr>
                <td style="text-align: center;" class="columnHeader" width="34">
                    <?php echo $j; ?>
                </td>
                <td class="columnHeader" style="text-align: center;"><?php echo $row['category_name']; ?></td>
                <td class="columnHeader" align="right">
                    <?php
                    $category_wise_total += $row['total_collected_amount'];
                    echo number_format($row['total_collected_amount'], 2);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>


        <?php
        foreach ($absent_play_truant_fine_summery as $row3):
            $j++;
            ?>
            <tr>

            <tr>
                <td style="text-align: center;" class="columnHeader" width="34">
                    <?php echo $j; ?>
                </td>
                <td class="columnHeader" style="text-align: center;">
                    <?php
                    if ($row3['is_absent_fine'] == 1) {
                        echo 'Absent Fine';
                    } else {
                        echo 'Play Truant Fine';
                    }
                    ?>
                </td>
                <td class="columnHeader" align="right">
                    <?php
                    $category_wise_total += $row3['total_collected_amount'];
                    echo number_format($row3['total_collected_amount'], 2);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>


        <tr>
            <td class="columnHeader" colspan="2" align="right"
                style="font-weight: bold;text-align: right;border-top: 1px #c7c7c7 solid;" scope="col">
                <?php echo $this->lang->line('sub'); ?> <?php echo $this->lang->line('total'); ?>
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: right;border-top: 1px #c7c7c7 solid;"
                scope="col">
                <?php
                $total += $category_wise_total;
                echo number_format($category_wise_total, 2);
                ?>
            </td>
        </tr>


        <tr>
            <th colspan="3" style="text-align: center; border-top:  1px #c7c7c7 solid;">
                <b><?php echo $this->lang->line('monthly'); ?> <?php echo $this->lang->line('fees'); ?></b>
            </th>
        </tr>


        <tr>
            <td class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"
                scope="col">SL
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"
                scope="col">Name
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"
                scope="col"><?php echo $this->lang->line('amount'); ?> (<?php echo $this->lang->line('tk'); ?>)
            </td>
        </tr>

        <?php
        $j = 0;
        $month_wise_total = 0;
        foreach ($month_wise_fee_summery as $row1):
            $j++;
            ?>
            <tr>

            <tr>
                <td style="text-align: center;" class="columnHeader" width="34">
                    <?php echo $j; ?>
                </td>
                <td class="columnHeader" style="text-align: center;">
                    <?php echo date("F", mktime(0, 0, 0, $row1['month'], 10)); ?>
                </td>
                <td class="columnHeader" align="right">
                    <?php
                    $month_wise_total += $row1['total_collected_amount'];
                    echo number_format($row1['total_collected_amount'], 2);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <tr>
            <td class="columnHeader" colspan="2" align="right"
                style="font-weight: bold;text-align: right;border-top: 1px #c7c7c7 solid;" scope="col">
                <?php echo $this->lang->line('sub'); ?> <?php echo $this->lang->line('total'); ?>
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: right;border-top: 1px #c7c7c7 solid;"
                scope="col">
                <?php
                $total += $month_wise_total;
                echo number_format($month_wise_total, 2);
                ?>
            </td>
        </tr>

        <tr>
            <td class="columnHeader" colspan="2" align="right"
                style="font-weight: bold;text-align: right;border-top: 1px #c7c7c7 solid;" scope="col">
                 Total
            </td>
            <td class="columnHeader" style="font-weight: bold;text-align: right;border-top: 1px #c7c7c7 solid;"
                scope="col">
                <?php
                echo number_format($total, 2);
                ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
