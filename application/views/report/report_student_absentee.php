<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    $(function () {
        $("#txtDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function start() {
        selectMonth();
    }
    window.onload = start;

    function selectMonth() {
        document.my_form.get_month.options[new Date().getMonth()].selected = true;
        document.my_form.get_day.options[(new Date().getUTCDate()) - 1].selected = true;
    }
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>


<form action="<?php echo base_url(); ?>report/student_report_absentee" id="my_form" name="my_form"
      method="post">
    &nbsp;
    <select name="class_id"
            class="smallInput" id="class_id" required="1">
        <option value="">-- <?php echo $this->lang->line('class'); ?> --</option>
        <?php foreach ($class as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    &nbsp;
    <select name="section_id"
            class="smallInput" id="section_id" required="1">
        <option value="">-- <?php echo $this->lang->line('section'); ?> --</option>
        <?php foreach ($section as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    &nbsp;
    <select name="group" id="group" class="smallInput" required="1">
        <option value="">-- <?php echo $this->lang->line('group'); ?> --</option>
        <?php foreach ($group as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    &nbsp;Day: <select name="get_day" id="get_day" required="required">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>
    </select>
    </select>
    &nbsp;Month: <select name="get_month" id="get_month">
        <option value='01'>January</option>
        <option value='02'>February</option>
        <option value='03'>March</option>
        <option value='04'>April</option>
        <option value='05'>May</option>
        <option value='06'>June</option>
        <option value='07'>July</option>
        <option value='08'>August</option>
        <option value='09'>September</option>
        <option value='10'>October</option>
        <option value='11'>November</option>
        <option value='12'>December</option>
    </select>
    &nbsp;Year: <select name="get_year" name="get_year">
        <?php
        for ($i = date("Y") - 10; $i <= date("Y"); $i++) {
            $sel = ($i == date('Y')) ? 'selected' : '';
            echo "<option value=" . $i . " " . $sel . ">" . date("Y", mktime(0, 0, 0, 0, 1, $i + 1)) . "</option>"; // change This Line
        }
        ?>
    </select>
    <button type="submit"><?php echo $this->lang->line('view'); ?></button>
</form>
<br>
<?php
if (isset($absentee_info)) {
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
        <tr>
            <th width="100%" style="text-align:right" scope="col">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
            </th>
        </tr>
    </table>
<?php
}
?>

<div id="printableArea">
    <?php
    if (isset($absentee_info)) {
        ?>
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td colspan="7" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                    <b style="font-size:13px;"><?php echo $this->lang->line('class'); ?>: <?php echo $HeaderInfo['class']; ?></b>,
                    <b style="font-size:13px;"><?php echo $this->lang->line('section'); ?>: <?php echo $HeaderInfo['section']; ?></b><br>
                    <b style="font-size:13px;"><?php echo $this->lang->line('group'); ?>:
                        <?php if ($HeaderInfo['group'] == 'S') {
                            echo "Science";
                        } elseif ($HeaderInfo['group'] == 'A') {
                            echo "Arts";
                        } elseif ($HeaderInfo['group'] == "C") {
                            echo 'Commerce';
                        } else {
                            echo "N/A";
                        } ?>
                    </b><br>
                        <?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('absentees'); ?> <?php echo $this->lang->line('list'); ?> <?php echo $this->lang->line('report_for'); ?> <?php echo date('d-M-Y', strtotime($date)); ?></b><br>
                </td>
            </tr>

            <tr>
                <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
                <th width="200" scope="col"><?php echo $this->lang->line('name'); ?></th>
                <th width="200" scope="col"><?php echo $this->lang->line('roll'); ?>.</th>
                <th width="200" scope="col"><?php echo $this->lang->line('reg_no'); ?>.</th>
                <th width="200" scope="col"><?php echo $this->lang->line('guardian_mobile'); ?></th>
            </tr>

            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($absentee_info as $row):
                $i++;
                ?>
                <tr>
                    <td width="34">
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['roll_no']; ?></td>
                    <td><?php echo $row['reg_no']; ?></td>
                    <td><?php echo $row['guardian_mobile']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php
    }
    ?>
</div>
