<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/getClassWiseCollectionSummery" method="post">
    <label>Year</label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>
    <br><br>

    <input type="submit" class="submit" value="View">
</form>
