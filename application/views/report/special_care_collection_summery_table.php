<style>
#pdf_table{
     border-collapse: collapse;
}
#pdf_table td, #pdf_table th {
    border: 1px solid #00000;
    padding: 5px;
}
</style>
<?php
     if (isset($is_pdf) && $is_pdf == 1) {
    ?>
       <table width="100%" id="pdf_table" style="font-size:13px;" summary="Employee Pay Sheet">
<?php
 }else{
?>
  <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
<?php
 }
?>
    <thead>
    <tr>
        <td class="center_td" colspan="8" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="8">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('date'); ?>: <b><?php echo date("M jS, Y", strtotime($from_date)); ?> - <?php echo date("M jS, Y", strtotime($to_date)); ?></b>
        </td>
    </tr>

	 <tr>
        <th width="20" scope="col">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
        <th width="120" scope="col" align="center">&nbsp;<?php echo $this->lang->line('student_id'); ?></th>
		<th width="130" scope="col">&nbsp;<?php echo $this->lang->line('name'); ?></th>
		<th width="80" scope="col">&nbsp;<?php echo $this->lang->line('Class'); ?></th>
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('month'); ?>/<?php echo $this->lang->line('year'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('receipt_no'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('date'); ?></th>
		<th width="130" scope="col">&nbsp;<?php echo $this->lang->line('amount'); ?> (<?php echo $this->lang->line('tk'); ?>)</th>
    </tr>

     </thead>

	  <tbody>
    <?php
    $i = 0;
	$total_amount = 0;
    foreach ($rdata as $row):
        $i++;
        ?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i; ?>
            </td>
            <td>
                &nbsp;<?php echo $row['student_code']; ?><br>
            </td>
			<td>
                &nbsp;<?php echo $row['name']; ?><br>
            </td>
            <td>&nbsp;
			&nbsp;<?php echo $row['class_name']; ?>
			</td>
           <td align="center">
                    &nbsp;<?php
			$monthNum  = $row['month'];
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			echo $dateObj->format('F'); // March
		     ?>,
                    <?php echo $row['year']; ?>
           </td>
		   <td align="center">&nbsp;<?php echo $row['receipt_no']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['date']; ?></td>
           <td align="right">&nbsp;<?php $total_amount+= $row['amount'];
		   echo number_format($row['amount'],2); ?>&nbsp;
		   </td>
        </tr>
    <?php endforeach; ?>
	   <tr>
	       <td align="right" colspan="7"><b>Total&nbsp;</b></td>
		   <td align="right"><b><?php echo number_format($total_amount,2); ?> &nbsp;</b></td>
	   </tr>
	   <tr>
            <td class="textleft" colspan="8">&nbsp;<b>In Words: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
        </tr>
    </tbody>

</table>
