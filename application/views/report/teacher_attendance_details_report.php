<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


</script>

<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>report/getTeacherAttendanceDetailReport" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('month'); ?></label>
      <select  class="js-example-basic-single w-100" name="month" id="month" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 1;
        while ($i <= 12) {
            $dateObj = DateTime::createFromFormat('!m', $i); ?>
          <option value="<?php echo $i; ?>"><?php echo $dateObj->format('F'); ?></option>
          <?php
          $i++;
        }
        ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('year'); ?></label>
      <select name="year" class="js-example-basic-single w-100" name="year">
        <?php
        if (count($years)) {
            foreach ($years as $list) {
                ?>
                <option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
                    if ($year == $list['value']) {
                        echo 'selected';
                    }
                } ?>><?php echo $list['text']; ?></option>
                <?php
            }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
      <!--<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/> -->
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>


</form>



<div id="printableArea">
    <?php
    if (isset($adata)) {
        echo $report;
    }
    ?>
</div>
