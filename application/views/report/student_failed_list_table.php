<style>
    .center-justified {
    margin: 0 auto;
    text-align: justify;
    width: 80%;
    }
</style>
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td style="text-align:center;">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:15px;"><?php echo $HeaderInfo['address']; ?></b><br>
            <b style="font-size:13px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php
                    echo $title;
                ?> <br>
            </b>
            <br>
        </td>
    </tr>
    
    <tbody>
            <?php
               foreach ($result_data as $row):
               ?>
            
                        
                            <tr>
                                <td style=" text-align: left;">
                                        <?php echo $row['subject_name'].' ('. $row['code'] . ')'; ?><br>
                                        <p style="padding-left:15px;">
                                        <?php
                                            foreach ($row['failed_list'] as $result_row):
                                        ?>
                                                   <?php echo $result_row; ?>
                                       <?php endforeach; ?>
                                       </p>
                                           
                                 </td>
                            </tr>
        
            <?php endforeach; ?>
    </tbody>
</table>