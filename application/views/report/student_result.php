<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

   $(function () {
        $("#from_date,#to_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

</script>

<script>
    function exam_by_year(year) {
        if (year != '') {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("exam_space").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>contents/ajax_exam_by_year?year=" + year, true);
            xmlhttp.send();
        } else {
            document.getElementById("exam_space").innerHTML = 'Please Select Year....';
        }
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/studentResultReport" method="post">
    <label><?php echo $this->lang->line('year'); ?></label>
    <select name="year"   class="smallInput"  id="year" required="1" onchange="exam_by_year(this.value)">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label><?php echo $this->lang->line('exam'); ?></label>
    <span id="exam_space"><?php echo $this->lang->line('please_select'); ?> <?php echo $this->lang->line('year'); ?>....</span>

    <label>Class</label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>



    <label><?php echo $this->lang->line('section'); ?></label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="all"><?php echo $this->lang->line('all'); ?></option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('group'); ?></label>
    <select name="group" required="1" class="smallInput">
        <option value="all"><?php echo $this->lang->line('all'); ?></option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('shift'); ?></label>
    <select name="shift_id" class="smallInput" required="1" id="shift_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($shifts as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

 <br><br>
    <input type="submit" class="submit" value="View Report">
    <br><br>
</form>

<div id="printableArea">
    <?php
    if (isset($rdata)) {
        echo $report;
    }
    ?>
</div>


<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
