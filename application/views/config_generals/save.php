<style>
.formContainer	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
		}
.formContainer	ol{width:auto;}
.formContainer	ol li {
		/*min-height: 20px!important;*/
	}
</style>
<script type="text/javascript">
	//datepicker
	$(function(){
		$("#txt_po_establishment_date").datepicker({dateFormat: 'yy-mm-dd'});
		$("#txt_sw_start_date_of_operation").datepicker({dateFormat: 'yy-mm-dd'});	
	});	
	$(document).ready(function(){	
		var skt_required = '<?php if(isset($row->is_SKT_required) ){ echo $row->is_SKT_required; }?>';
		if(skt_required=='0'){
			$('#txt_skt_amount').attr('readonly',"readonly");
		}
		else{
			$('#txt_skt_amount').attr('readonly',"");
		}
		//alert(skt_required);
		$("#cbo_is_skt_required").change(function(){			
			if($("#cbo_is_skt_required").val() == 1) {				
				$("#txt_skt_amount").attr('readonly',"");
			}
			else{
				$("#txt_skt_amount").val('');
				$("#txt_skt_amount").attr('readonly',"readonly");
			}
		});	
		
		var savings_balance_used = '<?php if(isset($row->savings_balance_used_for_interest_calculation) ){ echo $row->savings_balance_used_for_interest_calculation; }?>';
		//alert(savings_balance_used);
		if(savings_balance_used=='MINIMUM_BALANCE'){
			$('#txt_savings_minimum_balance_required_for_interest_calculation').attr('readonly',"");
		}
		else{
			$('#txt_savings_minimum_balance_required_for_interest_calculation').attr('readonly',"readonly");
		}
		//alert(skt_required);
		$("#cbo_savings_balance_used_for_interest_calculation").change(function(){
			var savings_balance_used= $("#cbo_savings_balance_used_for_interest_calculation").val();
			//alert(savings_balance_used);
			if(savings_balance_used == 'MINIMUM_BALANCE') {				
				$("#txt_savings_minimum_balance_required_for_interest_calculation").attr('readonly',"");
			}
			else{
				$("#txt_savings_minimum_balance_required_for_interest_calculation").val('');
				$("#txt_savings_minimum_balance_required_for_interest_calculation").attr('readonly',"readonly");
			}
		});	
	});
</script>
<?php                                   
	echo form_open_multipart('config_generals/edit');
	$img_name = '/media/images/add_big.png';
	$class_name = 'class="formTitleBar_edit"';
	$select_class = 'class="input_select"';
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
		<tr>
			<td class="formTitleBar">
				<div <?php echo $class_name?>>
					<h2><?php echo $headline;?></h2>
				</div>				
			</td>
			<td class="formTitleBar">
				<div style="float:right;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals')."'"));?>
				</div>
			</td>
		</tr>
		<tr>
			<td width="100%" colspan=2>
				<div class="formContainer" style="border:none;width:98%">
					<ol> 
						<li>
							<label for="txt_po_name">Organization Name:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_po_name','id'=>'txt_po_name','class'=>'input_textbox','maxlength'=>'150');
								echo form_input($attr,set_value('txt_po_name',isset($row->po_name)?$row->po_name:""));
								echo form_error('txt_po_name'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_po_code">Organization Code:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_po_code','id'=>'txt_po_code','class'=>'input_textbox','maxlength'=>'50');
								echo form_input($attr,set_value('txt_po_code',isset($row->po_code)?$row->po_code:""));
								echo form_error('txt_po_code'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_po_establishment_date">Organization Establishment Date:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_po_establishment_date','id'=>'txt_po_establishment_date','class'=>'date_picker','maxlength'=>'10');
								echo form_input($attr,set_value('txt_po_establishment_date',isset($row->po_establishment_date)?$row->po_establishment_date:""));
								echo form_error('txt_po_establishment_date'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_po_logo">Organization Logo:</label>
							<div class="form_input_container">
							<?php echo form_hidden('txt_po_logo_edit',isset($row->po_logo)?$row->po_logo:"");	?>
								<input type="file" id="txt_po_logo"  name="txt_po_logo" size="20" />
								<span class="explain" id="explain">File must be .jpg, .gif or .png and Allowed size <?php echo IMAGE_UPLOAD_SIZE . "KB"; ?></span><span style="float:right;"><?php if (isset($row->po_logo)) echo img(array('src'=>base_url().IMAGE_UPLOAD_PATH.$row->po_logo,'border'=>'0','alt'=>'','width'=>'25','height'=>'25'))?></span>	<?php echo form_error('txt_po_logo'); ?>
							</div>
						</li>  
						<li>
							<label for="txt_sw_start_date_of_operation">Software Start date of Operation:</label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_sw_start_date_of_operation','id'=>'txt_sw_start_date_of_operation','class'=>'date_picker','maxlength'=>'10');
								echo form_input($attr,set_value('txt_sw_start_date_of_operation',isset($row->sw_start_date_of_operation)?$row->sw_start_date_of_operation:""));
								echo form_error('txt_sw_start_date_of_operation'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_default_interest_calculation_method">Default Interest Calculation Method:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_default_interest_calculation_method',$interest_calculation_methods,set_value('cbo_default_interest_calculation_method',isset($row->default_interest_calculation_method)?$row->default_interest_calculation_method:"",'id="cbo_default_interest_calculation_method"'));
								echo form_error('cbo_default_interest_calculation_method'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_is_other_interest_calculation_method_allowed">Is Other Interest Calculation Method Allowed?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_other_interest_calculation_method_allowed',$options,set_value('cbo_is_other_interest_calculation_method_allowed',isset($row->is_other_interest_calculation_method_allowed)?$row->is_other_interest_calculation_method_allowed:"",'id="cbo_is_other_interest_calculation_method_allowed"'));
								echo form_error('cbo_is_other_interest_calculation_method_allowed'); 
							?>
							</div>
						</li>  
						<li>
							<label for="cbo_is_multiple_loan_allowed_for_primary_products">Is Multiple Loan Allowed for Primary Products?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_multiple_loan_allowed_for_primary_products',$options,set_value('cbo_is_multiple_loan_allowed_for_primary_products',isset($row->is_multiple_loan_allowed_for_primary_products)?$row->is_multiple_loan_allowed_for_primary_products:""));
								echo form_error('cbo_is_multiple_loan_allowed_for_primary_products'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_financial_year_start_month">Financial Year Start Month<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								
								echo form_dropdown('cbo_financial_year_start_month',$financial_year_start_month,set_value('cbo_financial_year_start_month',isset($row->financial_year_start_month)?$row->financial_year_start_month:"",'id="cbo_financial_year_start_month"'));
								echo form_error('cbo_financial_year_start_month'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_balance_used_for_interest_calculation">Saving Balance used for Interest Calculation<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_balance_used_for_interest_calculation',$savings_balance_used,set_value('cbo_savings_balance_used_for_interest_calculation',isset($row->savings_balance_used_for_interest_calculation)?$row->savings_balance_used_for_interest_calculation:""),'id="cbo_savings_balance_used_for_interest_calculation"');
								echo form_error('cbo_savings_balance_used_for_interest_calculation'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_nominee_information">Nominee Information Required for Member <span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								//echo form_dropdown('cbo_nominee_information',$nominee_info,set_value('cbo_nominee_information',isset($row->nominee_info)?$row->nominee_info:""),'id="cbo_nominee_information"');
								//echo form_error('cbo_nominee_information'); 
							?>
			<div style="float:left;"><?php //print_r($row->member_wise_nominee_info_required);
                $yes = array(
                            'name'        => 'cbo_nominee_information',
                            'id'          => 'cbo_nominee_information_yes',
                            'value'       => '1',
                            'style'       => 'margin:0px 10px',
                            //'checked'     => isset($row->is_branch_auto_code_need)?$row->is_branch_auto_code_need:"",
                            'checked'     => (isset($row->member_wise_nominee_info_required)&&($row->member_wise_nominee_info_required == 1))?'checked':''                            
                            );
                echo form_radio($yes) . "YES";
           ?></div>
           <div style="float:left;"><?php
                $no = array(
                            'name'        => 'cbo_nominee_information',
                            'id'          => 'cbo_nominee_information_no',
                            'value'       => '0',
                            'style'       => 'margin:0px 10px',
                            //'checked'     => (isset($is_branch_auto_code_need)&&$is_branch_auto_code_need==1)?'':(! isset($row->is_branch_auto_code_need) || (isset($row->is_branch_auto_code_need) && $row->is_branch_auto_code_need =='0'))?'checked':"",
                            'checked'     => (isset($row->member_wise_nominee_info_required)&&($row->member_wise_nominee_info_required == 0))?'checked':''
                            );
                echo form_radio($no) . "NO" ;
             ?></div>
             <?php echo form_error('cbo_nominee_information'); ?> 
							</div>
						</li>
						<li>
							<label for="txt_savings_minimum_balance_required_for_interest_calculation">Minimum Balance required for Interest Calculation</label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_savings_minimum_balance_required_for_interest_calculation','id'=>'txt_savings_minimum_balance_required_for_interest_calculation');
								echo form_input($attr,set_value('txt_savings_minimum_balance_required_for_interest_calculation',isset($row->savings_minimum_balance_required_for_interest_calculation)?$row->savings_minimum_balance_required_for_interest_calculation:""));
								echo form_error('txt_savings_minimum_balance_required_for_interest_calculation'); 
							?>
							</div>
						</li>						
						<li>
							<label for="txt_savings_minimum_account_duration_to_receive_interest">Minimum Account Duration to receive interest<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								
								echo form_dropdown('cbo_savings_minimum_account_duration_to_receive_interest',$frequency_in_months,set_value('cbo_savings_minimum_account_duration_to_receive_interest',isset($row->savings_minimum_account_duration_to_receive_interest)?$row->savings_minimum_account_duration_to_receive_interest:""));
								echo form_error('cbo_savings_minimum_account_duration_to_receive_interest'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_is_inactive_member_eligible_to_receive_interest">Is Inactive member eligible to receive interest?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_is_inactive_member_eligible_to_receive_interest',$options,set_value('cbo_savings_is_inactive_member_eligible_to_receive_interest',isset($row->savings_is_inactive_member_eligible_to_receive_interest)?$row->savings_is_inactive_member_eligible_to_receive_interest:"",'id="cbo_savings_is_inactive_member_eligible_to_receive_interest"'));
								echo form_error('cbo_savings_is_inactive_member_eligible_to_receive_interest'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_frequency_of_interest_posting_to_accounts">Frequency of Interest Posting to Accounts<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_frequency_of_interest_posting_to_accounts',$frequency_in_months,set_value('cbo_savings_frequency_of_interest_posting_to_accounts',isset($row->savings_frequency_of_interest_posting_to_accounts)?$row->savings_frequency_of_interest_posting_to_accounts:"",'id="cbo_savings_frequency_of_interest_posting_to_accounts"'));
								echo form_error('cbo_savings_frequency_of_interest_posting_to_accounts');
							?>
							</div>
						</li>
						
						
						<li>
							<label for="cbo_savings_interest_calculation_closing_month">Interest Calculation Closing Month<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_interest_calculation_closing_month',$month_list,set_value('cbo_savings_interest_calculation_closing_month',isset($row->savings_interest_calculation_closing_month)?$row->savings_interest_calculation_closing_month:""));
								echo form_error('cbo_savings_interest_calculation_closing_month');
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_interest_disbursment_month">Interest Dsibursment Month<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_interest_disbursment_month',$month_list,set_value('cbo_savings_interest_disbursment_month',isset($row->savings_interest_disbursment_month)?$row->savings_interest_disbursment_month:""));
								echo form_error('cbo_savings_interest_disbursment_month');
							?>
							</div>
						</li>
                        <li>
							<label for="cbo_both_father_spouse_name_required_for_member">Both Father and Spouse name required for member<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php
								echo form_dropdown('cbo_both_father_spouse_name_required_for_member',$options,set_value('cbo_both_father_spouse_name_required_for_member',isset($row->is_both_father_spouse_name_required_for_member)?$row->is_both_father_spouse_name_required_for_member:""));
								echo form_error('cbo_both_father_spouse_name_required_for_member');
							?>
							</div>
						</li>
						<li>
							<label for="txt_is_skt_required">Is SKT required?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_skt_required',$options,set_value('cbo_is_skt_required',isset($row->is_SKT_required)?$row->is_SKT_required:""),'id="cbo_is_skt_required"');
								echo form_error('cbo_is_skt_required');?>
							</div>
						</li>
						<li>
							<label for="txt_skt_amount">SKT Amount</label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_skt_amount','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_skt_amount',isset($row->SKT_amount)?$row->SKT_amount:""),'id="txt_skt_amount"');
								echo form_error('txt_skt_amount'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_max_member">Maximum Member per Samity<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_max_member','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_max_member',isset($row->max_member)?$row->max_member:""));
								echo form_error('txt_max_member'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_is_date_of_birth_required_in_member_information">Is Date of Birth required in Member Information?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_date_of_birth_required_in_member_information',$options,set_value('cbo_is_date_of_birth_required_in_member_information',isset($row->is_date_of_birth_required_in_member_information)?$row->is_date_of_birth_required_in_member_information:""),'id="cbo_is_date_of_birth_required_in_member_information"');
								echo form_error('cbo_is_date_of_birth_required_in_member_information');?>
							</div>
						</li>
						<li>
							<label for="txt_report_header_line_1">Report Header Line #1<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_header_line_1','class'=>'input_textbox','maxlength'=>'150'),set_value('txt_report_header_line_1',isset($row->report_header_line_1)?$row->report_header_line_1:""));
								echo form_error('txt_report_header_line_1'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_header_line_2">Report Header Line #2<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_header_line_2','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_report_header_line_2',isset($row->report_header_line_2)?$row->report_header_line_2:""));
								echo form_error('txt_report_header_line_2'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_header_line_3">Report Header Line #3<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_header_line_3','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_report_header_line_3',isset($row->report_header_line_3)?$row->report_header_line_3:""));
								echo form_error('txt_report_header_line_3'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_footer_line_1">Report Footer Line #1<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 								
								echo form_input(array('name'=>'txt_report_footer_line_1','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_report_footer_line_1',isset($row->report_footer_line_1)?$row->report_footer_line_1:""));
								echo form_error('txt_report_footer_line_1'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_footer_line_2">Report Footer Line #2<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_footer_line_2','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_report_footer_line_2',isset($row->report_footer_line_2)?$row->report_footer_line_2:""));
								echo form_error('txt_report_footer_line_2'); 
							?>
							</div>							
						</li>
						
						
						
						
						
						
						<li>
							<label FOR="cbo_is_service_rules_present">is service rules present?<span class="required_field_indicator">*</span></label>
							<DIV class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_service_rules_present',$OPTIONS,set_value('cbo_is_service_rules_present',isset($ROW->is_service_rules_present)?$ROW->is_service_rules_present:""));
								echo form_error('cbo_is_service_rules_present'); 
							?>
							</DIV>
						</li>
						<li>
							<label FOR="cbo_is_recruitment_policy_present">is_recruitment_policy_present?<span class="required_field_indicator">*</span></label>
							<DIV class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_recruitment_policy_present',$OPTIONS,set_value('cbo_is_recruitment_policy_present',isset($ROW->is_recruitment_policy_present)?$ROW->is_recruitment_policy_present:""));
								echo form_error('cbo_is_recruitment_policy_present'); 
							?>
							</DIV>
						</li>
						<li>
							<label FOR="cbo_is_financial_policy_present">is_financial_policy_present?<span class="required_field_indicator">*</span></label>
							<DIV class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_financial_policy_present',$OPTIONS,set_value('cbo_is_financial_policy_present',isset($ROW->is_financial_policy_present)?$ROW->is_financial_policy_present:""));
								echo form_error('cbo_is_financial_policy_present'); 
							?>
							</DIV>
						</li>
						<li>
							<label for="cbo_is_savings_and_credit_policy_present">is_savings_and_credit_policy_present?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_savings_and_credit_policy_present',$options,set_value('cbo_is_savings_and_credit_policy_present',isset($row->is_savings_and_credit_policy_present)?$row->is_savings_and_credit_policy_present:""));
								echo form_error('cbo_is_savings_and_credit_policy_present'); 
							?>
							</div>
						</li>
						
						<li>
							<label for="txt_mra_licence_no">Mra Licence No:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_mra_licence_no','id'=>'txt_mra_licence_no','class'=>'input_textbox','maxlength'=>'50');
								echo form_input($attr,set_value('txt_mra_licence_no',isset($row->mra_licence_no)?$row->mra_licence_no:""));
								echo form_error('txt_mra_licence_no'); 
							?>
							</div>
						</li>  
								
					</ol>
				</div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
<?php echo form_close(); ?>
