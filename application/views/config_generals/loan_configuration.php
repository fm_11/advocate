<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<?php
    echo form_open('config_generals/loan_configuration');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">		
		<tr>
			<td width="100%" colspan=2>
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%"> 					
						<li>
							<label for="txt_default_interest_calculation_method">Default Interest Calculation Method:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_default_interest_calculation_method',$interest_calculation_methods,set_value('cbo_default_interest_calculation_method',isset($row->default_interest_calculation_method)?$row->default_interest_calculation_method:"",'id="cbo_default_interest_calculation_method"'));
								echo form_error('cbo_default_interest_calculation_method'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_is_other_interest_calculation_method_allowed">Is Other Interest Calculation Method Allowed?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_other_interest_calculation_method_allowed',$options,set_value('cbo_is_other_interest_calculation_method_allowed',isset($row->is_other_interest_calculation_method_allowed)?$row->is_other_interest_calculation_method_allowed:"",'id="cbo_is_other_interest_calculation_method_allowed"'));
								echo form_error('cbo_is_other_interest_calculation_method_allowed'); 
							?>
							</div>
						</li>  
						<li>
							<label for="cbo_is_multiple_loan_allowed_for_primary_products">Is Multiple Loan Allowed for Primary Products?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_multiple_loan_allowed_for_primary_products',$options,set_value('cbo_is_multiple_loan_allowed_for_primary_products',isset($row->is_multiple_loan_allowed_for_primary_products)?$row->is_multiple_loan_allowed_for_primary_products:""));
								echo form_error('cbo_is_multiple_loan_allowed_for_primary_products'); 
							?>
							</div>
						</li>	
						<li>
							<label for="cbo_is_loan_proposal_form_mandatory">is loan proposal form mandatory?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_loan_proposal_form_mandatory',$options,set_value('cbo_is_loan_proposal_form_mandatory',isset($row->is_loan_proposal_form_mandatory)?$row->is_loan_proposal_form_mandatory:""));
								echo form_error('cbo_is_loan_proposal_form_mandatory'); 
							?>
							</div>
						</li>	
						<li>
							<label for="additional_fee_label_name">Additional Fee label Name:<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<input type="text" autocomplete="off"  name="additional_fee_label_name" id="additional_fee_label_name" value="<?php echo isset($row->additional_fee_label_name)?$row->additional_fee_label_name:""?>" /><?php echo form_error('additional_fee_label_name'); ?>	
							</div>
						</li>	
						<li>
							<label for="insurence_amount_label_name"> Insurance Amount Label Name:<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<input type="text" autocomplete="off"  name="insurence_amount_label_name" id="insurence_amount_label_name" value="<?php echo isset($row->insurence_amount_label_name)?$row->insurence_amount_label_name:""?>" /><?php echo form_error('insurence_amount_label_name'); ?>	
							</div>
						</li>
                                                <li>
							<label for="is_rebate_allowed_during_loan_waiver">IS LOAN REBATE ALLOWED DURING LOAN WAIVER FOR DEATH MEMBER?<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
                                                        <?php
                                                            echo form_dropdown('cbo_is_loan_rebate_allowed_loan_waiver',array("No","Yes"),  set_value('cbo_is_loan_rebate_allowed_loan_waiver',isset($row->is_rebate_allowed_during_loan_waiver)?$row->is_rebate_allowed_during_loan_waiver:0));
                                                            echo form_error('cbo_is_loan_rebate_allowed_loan_waiver'); 
                                                        ?>	
							</div>
						</li>
                                                <li>
							<label for="is_insurance_amount_editable">IS INSURANCE AMOUNT EDITABLE?<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
                                                        <?php
                                                            echo form_dropdown('cbo_is_insurance_amount_editable',array("No","Yes"),  set_value('cbo_is_insurance_amount_editable',isset($row->is_insurance_amount_editable)?$row->is_insurance_amount_editable:0));
                                                            echo form_error('cbo_is_insurance_amount_editable'); 
                                                        ?>	
							</div>
						</li>
                                                <li>
							<label for="is_multiple_onetime_loan_disburse_allowed">IS MULTIPLE ONE TIME LOAN DISBURSE ALLOWED?<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
                                                        <?php
                                                            echo form_dropdown('cbo_is_multiple_onetime_loan_disburse_allowed',array("No","Yes"),  set_value('cbo_is_insurance_amount_editable',isset($row->is_multiple_onetime_loan_disburse_allowed)?$row->is_multiple_onetime_loan_disburse_allowed:0));
                                                            echo form_error('cbo_is_multiple_onetime_loan_disburse_allowed'); 
                                                        ?>	
							</div>
						</li>
					</ol>
				</div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
