<style type="text/css">
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>

<?php
    echo form_open('config_generals/config_daily_collections');
    
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
		<tr>
			<td width="100%" colspan="2">
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%"> 
                                            <?php
                                            foreach ($daily_collection_configurations as $collectionsheet) {
                                               // echo "<pre>";print_r($collectionsheet);die;
                                                echo "<li><label for='".$collectionsheet->field_name."'>".$collectionsheet->label_name."</label>";
                                                echo "<div class='form_input_container'>";
                                                    if($collectionsheet->input_type=="select"){
                                                        $select_options=array();
                                                        $value=explode(",",$collectionsheet->value);
                                                        if(is_array($value)){
                                                            foreach($value as $new_value){
                                                                $new_value=explode("=>",$new_value);
                                                                for($i=0;$i<count($new_value);$i+=2){
                                                                    $select_options[$new_value[$i]]=$new_value[$i+1];
                                                                }                                                            
                                                            }                                                           
                                                        }
                                                        echo form_dropdown($collectionsheet->field_name, $select_options,$collectionsheet->default_value,' id="'.$collectionsheet->field_name.'"');
                                                    }
                                                    else if($collectionsheet->input_type="text"){
                                                        echo form_input($collectionsheet->field_name,$collectionsheet->default_value,' id="'.$collectionsheet->field_name.'"');
                                                    }
                                                echo "</div></li>";
                                            }
                                            ?>
							
					</ol>
				</div>
                            <div id="temp"></div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
