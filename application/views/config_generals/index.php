<div class="col-12 mb-4">
	  <div class="card">
          <div class="card-body">
              <h5 class="mb-4"><?php echo $title; ?></h5>
              <ul class="nav">
                  <li class="nav-item">
                      <a class="nav-link active" href="<?php echo base_url(); ?>config_generals/index">View</a>
                  </li>
									<li class="nav-item">
                      <a class="nav-link" href="<?php echo base_url(); ?>config_generals/general_configuration">General Config</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo base_url(); ?>config_generals/dashboard_configuration">Dashboard Config</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo base_url(); ?>config_generals/operational_policy">Operational Policy</a>
                  </li>
				  <li class="nav-item">
                      <a class="nav-link" href="<?php echo base_url(); ?>config_generals/marksheet_configuration">Marksheet Config</a>
                  </li>
              </ul>

							<?php if (!isset($load_view)) {
    $load_view = 'config_generals/view';
} ?>
							<?php $this->load->view($load_view); ?>
							<?php unset($load_vew);?>

          </div>
		</div>
</div>
