
	<table  class="table">
		<tbody>
                    <?php
                        foreach($purposes as $purpose_name=>$label){
                            foreach($$purpose_name as $purpose){
                                if($purpose->is_admin_access_only==0 ||($purpose->is_admin_access_only==1 && $login_name=="admin")){
                                    echo "<tr><td>",  ucwords(strtolower($purpose->label_name)),"</td>";
                                    echo "<td><span>";
                                    if($purpose->field_type=="text"||$purpose->field_type=="number"){
                                        echo $purpose->default_value;
                                    }else if($purpose->field_type=="date"){
                                        echo date('d/m/Y',strtotime($purpose->default_value));
                                    }else if($purpose->field_type=="file"){
                                        echo "<img src='".base_url()."IMAGE_UPLOAD_PATH/".$purpose->default_value."' width='50px' height='50px' />";
                                    }else{
                                        $field_value= json_decode($purpose->field_value);
                                        $default_value=$purpose->default_value;
                                        if(is_object($field_value)){
                                            echo $field_value->$default_value;
                                        }else{
                                            echo $field_value[$default_value];
                                        }
                                    }
                                    echo "</span></td></tr>";
                                }
                            }
                        }
                    ?>
		</tbody>
    </table>
