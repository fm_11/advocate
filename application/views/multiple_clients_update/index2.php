<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getUpazilasByDistrictId(district_id){
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("upazilas_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_upazilas_by_district?district_id=" + district_id, true);
        xmlhttp.send();
    }
</script>
<div>
<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>multiple_clients_update/index" method="post">

  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile"  value="">
    </div>
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('district'); ?></label>
      <select onchange="getUpazilasByDistrictId(this.value)"  name="district_id" class="js-example-basic-single w-100" id="district_id">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>

        <?php foreach ($districts as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($district_id)) {
           if ($row['id'] == $district_id) {
               $address=$row['name'];
             echo 'selected';

           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('upazila'); ?></label>
      <select onchange="changeUpazila(this)" name="upazilas_id" class="js-example-basic-single w-100" id="upazilas_id">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($upazilas as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($upazilla_id)) {
           if ($row['id'] == $upazilla_id) {

             echo 'selected';
              $address =$row['name'].",".$address;
           //
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>

  </div>
  <div class="btn-group float-right">
    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('view_reort'); ?>">
  </div>
</form>




<!-- <div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table class="sortable-table-1" style="width: 100%; border-top: 1px solid #dedede; margin-top: 15px; font-size:13px">
    <thead>
    <tr style="border-bottom: 1px solid #333;background-color:<?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">

        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('case_no'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('upazila'); ?></th>

    </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>1</td>
        <td>1</td>
      </tr>
    <tbody>
</div>
<div> -->

<!-- <?php
if (isset($clients)) {
	echo $data_table;

}
?> -->
<?php
if (isset($clients)) {?>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table class="sortable-table-1" style="width: 100%; border-top: 1px solid #dedede; margin-top: 15px; font-size:13px">
    <thead>
    <tr style="border-bottom: 1px solid #333;background-color:<?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">

        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('sl'); ?></th>
	    	<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('case_no'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('upazila'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('client'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('mobile').' '.$this->lang->line('number');?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('court');?></th>
				<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('first_hearing_date');?></th>
			 <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('file_colour');?></th>
			 <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($clients as $row):
        $i++;
        ?>
        <tr <?php if($i % 2 == 0){ echo 'style="background-color:' . $this->session->userdata('REPORT_ROW_COLOR'). ' !important;"'; } ?>>

						<td>
                <?php echo $i; ?>
            </td>
						<td class="td_center">
							<!-- <?php
							if($language=="english")  {
							echo $row['case_sub_type_name'].'-'.$row['case_no'];

							}else{
							echo $row['case_sub_type_bname'].'-'.$row['case_no'];
							}
							?> -->

						</td>
						<td class="td_center">
							<!-- <?php
							if($language=="english")  {
							echo $row['upazila_name'];

							}else{
							echo $row['upazila_bnname'];
							}
							?> -->

						</td>
						<td class="td_center"><?php echo $row['first_name']; ?>


						</td>
						<td class="td_center"><?php echo $row['mobile'];?></td>
						<td class="td_center">
								<!-- <?php
								if($language=="english")  {
								echo $row['court_name'];
								}else{
							 echo $row['court_bn_name'];
								}

								?> -->
							</td>
							<td>
								<!-- <?php echo date("d-m-Y", strtotime($row['next_date'])); ?> -->

							</td>
							<td>
								<!-- <?php
								if($language=="english")  {
								echo $row['file_colour'];

								}else{
							 echo $row['bn_file_colour'];
								}
								?> -->
							</td>
							<td class="td_center">
							<!-- <?php
							if($language=="english")  {
							echo $row['status_name'];

							}else{
						 echo $row['status_bn_name'];
							}
							?> -->
						</td>
        </tr>
    <?php endforeach; ?>




      <!-- <tr>
           <td class="textleft" colspan="6"><b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
       </tr> -->
    </tbody>
</table>
</div>
</div>
<?php

}
?>
