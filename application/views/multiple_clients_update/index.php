<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getUpazilasByDistrictId(district_id){
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("upazilas_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>clients/ajax_upazilas_by_district?district_id=" + district_id, true);
        xmlhttp.send();
    }
    function checkCheckBox() {

        var loop_time=  document.getElementById("loop_time").value;
        var   count = 0;
        for (var i = 1; i <= loop_time; i++) {
          if (document.getElementById("is_allow_"+i).checked == true) {

              count++;
          }

        }
        if (count < 1) {
            alert("Please select atleast one clients.");
            return false;
        } else {
            return true;
        }
    }

</script>


<?php
$name = $this->session->userdata('name');
$status = $this->session->userdata('status');
//echo $student_status; die;
?>

  <form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>multiple_clients_update/index" method="post">

    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></label>
        <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile"  value="">
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4"><?php echo $this->lang->line('district'); ?></label>
        <select onchange="getUpazilasByDistrictId(this.value)"  name="district_id" class="js-example-basic-single w-100" id="district_id">
          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>

          <?php foreach ($districts as $row) { ?>
            <option value="<?php echo $row['id']; ?>"<?php if (isset($district_id)) {
             if ($row['id'] == $district_id) {
                 $address=$row['name'];
               echo 'selected';

             }
           } ?>><?php echo $row['name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4"><?php echo $this->lang->line('upazila'); ?></label>
        <select onchange="changeUpazila(this)" name="upazilas_id" class="js-example-basic-single w-100" id="upazilas_id">
          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
          <?php foreach ($upazillas as $row) { ?>
            <option value="<?php echo $row['id']; ?>"<?php if (isset($upazilla_id)) {
             if ($row['id'] == $upazilla_id) {

               echo 'selected';
                $address =$row['name'].",".$address;
             //
             }
           } ?>><?php echo $row['name']; ?></option>
          <?php } ?>
        </select>
      </div>

    </div>
    <div class="btn-group float-right">
      <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('view_reort'); ?>">
    </div>
</form>
<style>
.th_class{
  color:#ffffff;
  text-align: center;
  padding: 5px
}
</style>
<?php
if (isset($clients)) {?>
  <form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>multiple_clients_update/data_save" method="post" onsubmit="return checkCheckBox()">
<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped" style="width: 100%; border-top: 1px solid #dedede; margin-top: 15px; font-size:13px">
    <thead>
      <tr style="border-bottom: 1px solid #333;background-color:<?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('bangla').' '.$this->lang->line('name'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('total_person'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('upazila'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('gender'); ?></th>

    </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach ($clients as $row):
          $i++;
          ?>
            <tr <?php if($i % 2 == 0){ echo 'style="background-color:' . $this->session->userdata('REPORT_ROW_COLOR'). ' !important;"'; } ?>>
              <td>
                <input type="checkbox" name="is_allow_<?php echo $i; ?>" id="is_allow_<?php echo $i; ?>">
                <input type="hidden" class="input-text-short" name="id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
              </td>
              <td>
                <input style="width: 200px !important;" type="text" autocomplete="off"  class="form-control"
                     name="first_name_<?php echo $i; ?>" value="<?php echo $row['first_name']; ?>"/>
              </td>
              <td>
                <input style="width: 200px !important;" type="text" autocomplete="off"  class="form-control"
                     name="last_name_<?php echo $i; ?>" value="<?php echo $row['last_name']; ?>"/>
              </td>
              <td>
                <input style="width: 80px !important;" type="number" autocomplete="off"  min="0" max="100" class="form-control"
                     name="total_person_<?php echo $i; ?>" value="<?php echo $row['total_person']; ?>"/>
              </td>
              <td>
                <input style="width: 150px !important;" type="text" autocomplete="off"  class="form-control"
                     name="mobile_<?php echo $i; ?>" value="<?php echo $row['mobile']; ?>"/>
              </td>
              <td>
                <select style="width: 200px !important;"  name="upazilas_id_<?php echo $i; ?>" class="js-example-basic-single w-100"  required="required">
                  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                  <?php foreach ($upazillas as $row2) { ?>
                    <option value="<?php echo $row2['id']; ?>"<?php if (isset($row['upazilas_id'])) {
                     if ($row2['id'] == $row['upazilas_id']) {echo 'selected';}} ?>
                     ><?php echo $row2['name']; ?>
                   </option>
                  <?php } ?>
                </select>
              </td>

              <td>
                <select style="width: 150px !important;"  name="gender_<?php echo $i; ?>" class="js-example-basic-single w-100" required="required">
                  <option value="<?php echo $row['gender']; ?>">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                  <option value="M" <?php if (isset($row['gender'])) {
                   if ('M' == $row['gender']) {
                     echo 'selected';
                   }
                 } ?>> <?php echo $this->lang->line('male'); ?> </option>
                  <option value="F" <?php if (isset($row['gender'])) {
                   if ('F' == $row['gender']) {
                     echo 'selected';
                   }
                 } ?>> <?php echo $this->lang->line('female'); ?></option>
                </select>
              </td>

            </tr>
      <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="float-right">
<input type="hidden" name="loop_time" id="loop_time" value="<?php echo $i; ?>">
   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
</div>
</form>
<?php

}
?>
