<!-- <div id="divPrintable" class="box-body" style="padding-left:0px; padding-right:0px"> -->

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="tblItems" class="sortable-table-1" style="width: 100%; border-top: 1px solid #dedede; margin-top: 15px; font-size:13px">
    <thead>
    <tr style="border-bottom: 1px solid #333;background-color:<?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">

        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('sl'); ?></th>
	    	<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('case_no'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('upazila'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('client'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('mobile').' '.$this->lang->line('number');?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('court');?></th>
				<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('first_hearing_date');?></th>
			 <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('file_colour');?></th>
			 <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($clients as $row):
        $i++;
        ?>
        <tr <?php if($i % 2 == 0){ echo 'style="background-color:' . $this->session->userdata('REPORT_ROW_COLOR'). ' !important;"'; } ?>>

						<td>
                <?php echo $i; ?>
            </td>
						<td class="td_center">
							<!-- <?php
							if($language=="english")  {
							echo $row['case_sub_type_name'].'-'.$row['case_no'];

							}else{
							echo $row['case_sub_type_bname'].'-'.$row['case_no'];
							}
							?> -->

						</td>
						<td class="td_center">
							<!-- <?php
							if($language=="english")  {
							echo $row['upazila_name'];

							}else{
							echo $row['upazila_bnname'];
							}
							?> -->

						</td>
						<td class="td_center"><?php echo $row['first_name']; ?>


						</td>
						<td class="td_center"><?php echo $row['mobile'];?></td>
						<td class="td_center">
								<!-- <?php
								if($language=="english")  {
								echo $row['court_name'];
								}else{
							 echo $row['court_bn_name'];
								}

								?> -->
							</td>
							<td>
								<!-- <?php echo date("d-m-Y", strtotime($row['next_date'])); ?> -->

							</td>
							<td>
								<!-- <?php
								if($language=="english")  {
								echo $row['file_colour'];

								}else{
							 echo $row['bn_file_colour'];
								}
								?> -->
							</td>
							<td class="td_center">
							<!-- <?php
							if($language=="english")  {
							echo $row['status_name'];

							}else{
						 echo $row['status_bn_name'];
							}
							?> -->
						</td>
        </tr>
    <?php endforeach; ?>




      <!-- <tr>
           <td class="textleft" colspan="6"><b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
       </tr> -->
    </tbody>
</table>
</div>
<!-- </div> -->
