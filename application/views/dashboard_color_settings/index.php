<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jscolor/jscolor.js"></script>
<script>
	function resetConfirm() {
		var result = confirm("Are you sure to reset all color?");
		if (result == true) {
			return true;
		} else {
			return false;
		}
	}
</script>
	<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard_color_settings/index" method="post">
		<p class="mb-0">
			<b>	বিঃদ্রঃ ১.</b> বাম পাশের মেনুর জন্য এবং উপরের বারের জন্য এমন রং সেট করবেন না যেটা লেখার সাদা রং এবং হোভার এর কালো  রং এর সাথে খারাপ লাগে।
			<b>২.</b> বাম পাশের মেনু , উপরের বার এবং মেনু খোলা ও বন্ধ করার বাটনের রং পরবর্তী লগইন করার পর কার্যকর হবে।
			<b>৩.</b> কারিগরি টীম এর পক্ষ থেকে নির্ধারিত রং সেট করতে রিসেট বাটন এ ক্লিক করুন এবং কন্ফার্ম করুন। ধন্যবাদ
		</p>

		<hr>

		<div class="form-row">

			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('left_menu_background_color'); ?></label>
				<input type="text" onchange="checkSideBar(this.value)" autocomplete="off" style="background-color: <?php echo $config_info->left_menu_background_color; ?>" value="<?php echo $config_info->left_menu_background_color; ?>"  required name="left_menu_background_color" id="left_menu_background_color" class="form-control color">
			</div>

			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('top_bar_background_color'); ?></label>
				<input type="text" onchange="checkTopBar(this.value)"  autocomplete="off" style="background-color: <?php echo $config_info->top_bar_background_color; ?>" value="<?php echo $config_info->top_bar_background_color; ?>"  required name="top_bar_background_color" id="top_bar_background_color" class="form-control color">
			</div>

			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('top_menu_show_hide_button_color'); ?></label>
				<input type="text"  onchange="checkTopBarButton(this.value)"  autocomplete="off" style="background-color: <?php echo $config_info->top_menu_show_hide_button_color; ?>" value="<?php echo $config_info->top_menu_show_hide_button_color; ?>"  required name="top_menu_show_hide_button_color" id="top_menu_show_hide_button_color" class="form-control color">
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('office').' '.$this->lang->line('information').' '.$this->lang->line('border'); ?></label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->school_name_border_color; ?>"  required name="school_name_border_color" class="form-control color" value="<?php echo $config_info->school_name_border_color; ?>">
			</div>
			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('office').' '.$this->lang->line('chief').' '.$this->lang->line('advocate').' '.$this->lang->line('border'); ?></label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->head_name_border_color; ?>;"  required name="head_name_border_color" class="form-control color" value="<?php echo $config_info->head_name_border_color; ?>">
			</div>
			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('today_attendance').' '.$this->lang->line('border'); ?></label>
				<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->today_attendance_border_color; ?>;" required name="today_attendance_border_color" class="form-control color" value="<?php echo $config_info->today_attendance_border_color; ?>">
			</div>

		</div>
		<div class="form-row">
			<div class="form-group col-md-4">
				<label>Support Center Border</label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->support_center_border_color; ?>"  required name="support_center_border_color" class="form-control color" value="<?php echo $config_info->support_center_border_color; ?>">
			</div>
			<!-- Total Cases -->
			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('total_running_case').' '.$this->lang->line('border'); ?></label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->total_running_case_border_color; ?>"  required name="total_running_case_border_color" class="form-control color" value="<?php echo $config_info->total_running_case_border_color; ?>">
			</div>
			<!-- Important Cases -->
			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('total_colsed_case').' '.$this->lang->line('border'); ?></label>
				<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->total_colsed_case_border_color; ?>" required name="total_colsed_case_border_color" class="form-control color" value="<?php echo $config_info->total_colsed_case_border_color; ?>">
			</div>
		</div>
		<div class="form-row">
			<!-- Archived Cases -->
			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('total_missaed_attendance').' '.$this->lang->line('border'); ?></label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->total_missaed_attendance_border_color; ?>"  required name="total_missaed_attendance_border_color" class="form-control color" value="<?php echo $config_info->total_missaed_attendance_border_color; ?>">
			</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('sms').' '.$this->lang->line('balance').' '.$this->lang->line('border'); ?></label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->sms_balance_border_color; ?>"  required name="sms_balance_border_color" class="form-control color" value="<?php echo $config_info->sms_balance_border_color; ?>">
		</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('total_clients').' '.$this->lang->line('border'); ?></label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->total_client_border_color; ?>" value="<?php echo $config_info->total_client_border_color; ?>"  required name="total_client_border_color" class="form-control color">
		</div>
	</div>

	<div class="form-row">
		<!-- Archived Cases -->
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('total_importan_case').' '.$this->lang->line('border'); ?></label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->total_importan_case_border_color; ?>"  required name="total_importan_case_border_color" class="form-control color" value="<?php echo $config_info->total_importan_case_border_color; ?>">
		</div>

	<!-- <div class="form-group col-md-4">
		<label><?php echo $this->lang->line('sms').' '.$this->lang->line('balance').' '.$this->lang->line('border'); ?></label>
		<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->sms_balance_border_color; ?>"  required name="sms_balance_border_color" class="form-control color" value="<?php echo $config_info->sms_balance_border_color; ?>">
	</div>

	<div class="form-group col-md-4">
		<label><?php echo $this->lang->line('total_clients').' '.$this->lang->line('border'); ?></label>
		<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->birthday_border_color; ?>" value="<?php echo $config_info->birthday_border_color; ?>"  required name="birthday_border_color" class="form-control color">
	</div> -->
	</div>
	<!-- <div class="form-row">
		<div class="form-group col-md-3">
			<label>Class Wise Student Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->class_wise_student_border_color; ?>"  required name="class_wise_student_border_color" class="form-control color" value="<?php echo $config_info->class_wise_student_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Gender Wise Student Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->gender_wise_wise_border_color; ?>" required name="gender_wise_wise_border_color" class="form-control color" value="<?php echo $config_info->gender_wise_wise_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Income vs Expense Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->income_vs_expense_border_color; ?>"  required name="income_vs_expense_border_color" class="form-control color" value="<?php echo $config_info->income_vs_expense_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Last 7 days collection Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->last_7_days_collection_border_color; ?>" value="<?php echo $config_info->last_7_days_collection_border_color; ?>"  required name="last_7_days_collection_border_color" class="form-control color">
		</div>

	</div> -->






	<hr>

		<div class="form-row">

			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('table_header_row_color'); ?></label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->todays_collection_border_color; ?>"  required name="table_header_row_color" class="form-control color" value="<?php echo $config_info->table_header_row_color; ?>">
			</div>

			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('table_body_row_color'); ?></label>
				<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->monthly_collection_border_color; ?>" required name="table_body_row_color" class="form-control color" value="<?php echo $config_info->table_body_row_color; ?>">
			</div>

			<div class="form-group col-md-4">
				<label><?php echo $this->lang->line('table_row_color_enable_for_eport'); ?></label>
				<select style="padding: 13px;" class="form-control" name="table_row_enable_for_report">
					<option <?php if($config_info->table_row_enable_for_report == '1'){ echo 'selected'; } ?> value="1">Yes</option>
					<option <?php if($config_info->table_row_enable_for_report == '0'){ echo 'selected'; } ?> value="0">No</option>
				</select>
			</div>

		</div>


	<input type="hidden" readonly autocomplete="off"  required name="id" class="form-control" value="<?php echo $config_info->id; ?>">
	<div class="float-right">
		<button type="submit" onclick="return resetConfirm()" class="btn btn-outline-warning btn-icon-text">
			<i class="ion ion-md-refresh"></i>
			<?php echo $this->lang->line('reset'); ?>
		</button>
		<input class="btn btn-primary" type="submit" name="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>

<script>
	function checkSideBar(value){
		$('#sidebar').css('background-color',"#" + value);
	}
	function checkTopBar(value){
		$('#top_bar_background').css('background-color',"#" + value);
	}
	function checkTopBarButton(value){
		$('#top_menu_show_hide_button').css('background-color',"#" + value);
	}
</script>

</script>
