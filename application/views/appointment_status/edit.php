
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>appointment_status/edit//<?php echo $row->id; ?>" method="post">

  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('name'); ?></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $row->name; ?>" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $row->id; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
