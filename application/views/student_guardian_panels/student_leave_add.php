<div class="col-md-12">
    <div class="card">
        <div class="header">

            <?php
            $message = $this->session->userdata('message');
            if ($message != '') {
                ?>
                <div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span>
                        <?php
                        echo $message;
                        $this->session->unset_userdata('message');
                        ?>
                    </span>
                </div>
                <?php
            }
            ?>

            <?php
            $exception = $this->session->userdata('exception');
            if ($exception != '') {
                ?>
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span>
                        <?php
                        echo $exception;
                        $this->session->unset_userdata('exception');
                        ?>
                    </span>
                </div>
                <?php
            }
            ?>
            <h4 class="title">Student Leave Information</h4>
        </div>
        <div class="content">
            <form action="<?php echo base_url(); ?>student_guardian_panels/get_student_leave_application" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Student Name/ ID</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled
                                   value="<?php echo $name; ?> (<?php echo $student_code; ?>)">

                            <input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
                            <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                            <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
                            <input type="hidden" name="group" value="<?php echo $group; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Date From</label>
                            <input type="text" autocomplete="off"  value="<?php echo date('Y-m-d'); ?>" format="yyyy-MM-dd"
                                   class="form-control" name="txtFromDate" id="txtFromDate" required>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>To Date</label>
                            <input type="text" autocomplete="off"  value="<?php echo date('Y-m-d'); ?>" class="form-control"
                                   name="txtToDate" id="txtToDate" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Reason</label>
                            <textarea rows="5" name="txtReason" class="form-control"
                                      placeholder="Here can be your reason"></textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
