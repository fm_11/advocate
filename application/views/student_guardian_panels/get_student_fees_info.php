<div class="col-md-12">
    <div class="card">
        <div class="header">
            <h4 class="title">Student Fees Information</h4>
        </div>
        <div class="content">
            <form action="<?php echo base_url(); ?>student_guardian_panels/get_student_fees_info" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Year</label>
                            <select class="form-control" name="year" id="year" required="1">
                                <option value="">-- Please Select --</option>
                                <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                            </select>
                            <input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
                        </div>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-info btn-fill pull-right">Preview</button>
                <div class="clearfix"></div>
            </form>
        </div>

<hr>

<?php
if(isset($rdata)){
?>

<table class="table table-hover table-striped">
    <thead>
    <tr>
        <td colspan="<?php echo count($sub_category) + 2; ?>" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="<?php echo count($sub_category) + 3; ?>">
            Name: <b><?php echo $student_info[0]['name']; ?></b>,
            Class: <b><?php echo $student_info[0]['roll_no']; ?></b>,
            Roll No.: <b><?php echo $student_info[0]['class_name']; ?></b>
            Shift: <b><?php echo $student_info[0]['shift_name']; ?></b>
        </td>
    </tr>

    <tr>
        <th>
            Month
        </th>
        <th style="text-align:center;">
            Receipt No.<br>
            Date
        </th>
        <?php
        foreach ($sub_category as $row):
            ?>
            <th>
                <?php echo $row['name']; ?>
            </th>
        <?php endforeach; ?>

       <th>Special Care</th>

    </tr>
    </thead>

    <tbody>
    <?php
    //echo '<pre>';
    //print_r($rdata);
   // die;
    $i = 1;
   
    while ($i <= count($rdata)) {
         $k = 0;
        ?>
        <tr>
            <td>
                <?php
                $dateObj   = DateTime::createFromFormat('!m', $i);
                echo $dateObj->format('F'); // March
                ?>
            </td>
            
            <td style="text-align:center;">
                    <?php
                    foreach ($rdata[$i] as $row2):
                        ?>
                        <?php if($k==0 && $row2['receipt_no'] != ''){ ?>
                           <?php echo $row2['receipt_no']; ?><br>
                           <?php echo date("d-m-Y", strtotime($row2['date'])); ?>
                        <?php 
                           $k = 1; 
                            break;
                           } 
                        endforeach;
                        if($k == 0){
                            echo '-';
                        }
                    ?>
            </td>
          
          
          
            <?php
            foreach ($rdata[$i] as $row1):
                ?>
                <td style="text-align:center;">
                    <?php echo $row1['paid_amount']; ?>
                </td>
                
                <?php
            endforeach;
            ?>
            
            
            
        </tr>
        <?php
        $i++;
    }
    ?>
    </tbody>

</table>

<?php
}
?>


    </div>
</div>



