<div class="col-md-8">
    <div class="card">
        <div class="header">
            <h4 class="title">Profile of <?php echo $student_info[0]['name']; ?></h4>
        </div>
        <div class="content">
            <form>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled
                                   value="<?php echo $student_info[0]['name']; ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Student ID</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['student_code']; ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Class Name</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['class_name']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Roll No.</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['roll_no']; ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Reg. No.</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['reg_no']; ?>">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Section</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['section_name']; ?>">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Group</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['group_name']; ?>">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['date_of_birth']; ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Gender</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php
                            if ($student_info[0]['gender'] == 'M') {
                                echo 'Male';
                            } else if ($student_info[0]['gender'] == 'F') {
                                echo 'Female';
                            } else {
                                echo 'N/A';
                            }
                            ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Religion</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php
                            if ($student_info[0]['religion'] == 'I') {
                                echo 'Islam';
                            } else if ($student_info[0]['religion'] == 'H') {
                                echo 'Hindu';
                            } else {
                                echo 'N/A';
                            }
                            ?>">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Blood Group</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['blood_group']; ?>">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Father's Name</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled
                                   value="<?php echo $student_info[0]['father_name']; ?>">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Father's NID</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['father_nid']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Mother's Name</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['mother_name']; ?>">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Mother's NID</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['mother_nid']; ?>">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Present Address</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled
                                   value="<?php echo $student_info[0]['present_address']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Permanent Address</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled
                                   value="<?php echo $student_info[0]['permanent_address']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Guardian Mobile</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['guardian_mobile']; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled value="<?php echo $student_info[0]['email']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Subject List</label>
                            <table class="table table-hover table-striped">
                                <tr>
                                    <td>Sl</td>
                                    <td>Subject</td>
                                    <td>Is Optional</td>
                                </tr>
                                <?php for ($l = 0; $l < count($subject_list_by_student); $l++) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $l + 1; ?>
                                        </td>
                                        <td>
                                            <?php echo $subject_list_by_student[$l]['name']; ?>
                                        </td>
                                        <td>
                                            <?php if ($subject_list_by_student[$l]['is_optional'] == '1') {
                                                echo "YES";
                                            } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="card card-user">
        <div class="image">
            <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/css/logo.png"
                 alt="..."/>
        </div>
        <div class="content">
            <div class="author">
                <a href="#">
                    <img class="avatar border-gray" src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $student_info[0]['photo']; ?>" alt="..."/>
                    <h4 class="title"><?php echo $student_info[0]['name']; ?><br/>
                        <small><?php echo $student_info[0]['student_code']; ?></small>
                    </h4>
                </a>
            </div>
            <p class="description text-center"> Class: <?php echo $student_info[0]['class_name']; ?>  <br>
                Section: <?php echo $student_info[0]['section_name']; ?> <br>
                Group: <?php echo $student_info[0]['group_name']; ?><br>
                Roll No.: <?php echo $student_info[0]['roll_no']; ?><br>
            </p>
        </div>
        <hr>
        <div class="text-center">
            আপনার সন্তানকে নিয়মিত স্কুলে পাঠান
        </div>
    </div>
</div>
