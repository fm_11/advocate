<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login to School360° v 2.0.0</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/core_media/css/login_css/style.css">
    <link rel="icon" href="<?php echo base_url(); ?>/core_media/css/login_css/school360-favicon.png" type="image/png">

    <style>
        .isa_info, .isa_success, .isa_warning, .isa_error {
            padding:12px;
        }
        .isa_success {
            color: #4F8A10;
            background-color: #DFF2BF;
        }
        .isa_error {
            color: #D8000C;
            background-color: #FFBABA;
        }
    </style>
</head>
<body>
<section class="container">
    <div class="login">
        <h1>Login to <span style="color:#0059b3;">School</span><span style="color:#ff8080;">360°</span> v 2.0.0</h1>
        <form method="post" action="<?php echo base_url(); ?>login/student_guardian_login_authentication">
            <p>

                <?php
                $message = $this->session->userdata('message');
                if ($message != '') {
                ?>
            <div class="isa_success">
                <?php
                echo $message;
                $this->session->unset_userdata('message');
                ?>
            </div>
            <?php
            }
            ?>

            <?php
            $exception = $this->session->userdata('exception');
            if ($exception != '') {
                ?>
                <div class="isa_error">
                    <?php
                    echo $exception;
                    $this->session->unset_userdata('exception');
                    ?>
                </div>
                <?php
            }
            ?>
            </p>
            <p>
                <input type="text" autocomplete="off"  name="user_name" value="" placeholder="Student ID / Mobile" required="1" >
            </p>
            <p>
                <input type="password" name="password" value="" placeholder="Password" required="1" >
            </p>

            <p class="remember_me">
                <label>
                    <input type="checkbox" name="remember_me" id="remember_me">
                    Remember me on this computer
                </label>
            </p>
            <p class="submit"><input type="submit" name="submit" value="Login"></p>
        </form>
    </div>

    <div class="login-help">
        <p>Forgot your password? <a href="<?php echo base_url(); ?>login/student_forgot_password">Click here to reset it</a>.</p>
    </div>
</section>

<section class="about">
    <img src="<?php echo base_url(); ?>/core_media/css/login_css/school360-logo.png">
    <p class="about-author">
        &copy; 2015&ndash;2017 <a href="https://school360.app" target="_blank">School360</a>
    </p>
</section>
</body>
</html>
