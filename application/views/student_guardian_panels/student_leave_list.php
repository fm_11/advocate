<div class="col-md-12">
    <div class="card">
        <div class="header">
            <h4 class="title">
                Leave Information
            </h4>
        </div>
        <div class="content table-responsive table-full-width">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <td>
                        <a href="<?php echo base_url(); ?>student_guardian_panels/get_student_leave_application" class="btn btn-round btn-fill btn-default">Send Leave Application</a>
                    </td>
                </tr>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Student ID</th>
                    <th>Class</th>
                    <th>Section</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $i = 0;
                foreach ($leaves as $row):
                    $i++;
                    ?>
                    <tr>

                    <tr>
                        <td width="34">
                            <?php echo $i; ?>
                        </td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['roll_no']; ?></td>
                        <td><?php echo $row['class']; ?></td>
                        <td><?php echo $row['section_name']; ?></td>
                        <td><?php echo $row['date_from']; ?></td>
                        <td><?php echo $row['date_to']; ?></td>
                        <td>
                            <?php
                            if ($row['status'] == 0) {
                                echo 'Pending';
                            }elseif ($row['status'] == 1){
                                echo 'Accepted';
                            } else {
                                echo 'Rejected';
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

                <tr class="footer">
                    <td colspan="8" align="center">
                        <!--  PAGINATION START  -->
                        <div class="pagination">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!--  PAGINATION END  -->
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
