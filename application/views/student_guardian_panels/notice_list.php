<div class="col-md-12">
    <div class="card">
        <div class="header">
            <h4 class="title">
                Notice
            </h4>
        </div>
        <div class="content table-responsive table-full-width">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Notice</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($notices as $row):
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['title']; ?></td>
                        <td><?php echo $row['date']; ?></td>
                        <td><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>">Download</a></td>
                    </tr>
                <?php endforeach; ?>

                <tr class="footer">
                    <td colspan="4" align="center">
                        <!--  PAGINATION START  -->
                        <div class="pagination">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!--  PAGINATION END  -->
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
