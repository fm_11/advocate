<script>
    function myFunction() {
        var pass1 = document.getElementById("new_pass").value;
        var pass2 = document.getElementById("con_new_pass").value;
        var ok = true;
        if (pass1 != pass2) {
            alert("Password Doesn't Match !");
            document.getElementById("new_pass").style.borderColor = "#E34234";
            document.getElementById("con_new_pass").style.borderColor = "#E34234";
            ok = false;
        }
        return ok;
    }
</script>

<div class="col-md-12">
    <div class="card">
        <div class="header">

            <?php
            $message = $this->session->userdata('message');
            if ($message != '') {
                ?>
                <div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span>
                        <?php
                        echo $message;
                        $this->session->unset_userdata('message');
                        ?>
                    </span>
                </div>
                <?php
            }
            ?>

            <?php
            $exception = $this->session->userdata('exception');
            if ($exception != '') {
                ?>
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span>
                        <?php
                        echo $exception;
                        $this->session->unset_userdata('exception');
                        ?>
                    </span>
                </div>
                <?php
            }
            ?>
            <h4 class="title">Student Leave Information</h4>
        </div>
        <div class="content">
            <form action="<?php echo base_url(); ?>student_guardian_panels/change_password" onsubmit="return myFunction()" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Student Name/ ID</label>
                            <input type="text" autocomplete="off"  class="form-control" disabled
                                   value="<?php echo $name; ?> (<?php echo $student_code; ?>)">
                            <input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Current Password</label>
                            <input type="password"
                                   class="form-control" name="current_pass" id="current_pass" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password"
                                   class="form-control" name="new_pass" id="new_pass" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Confirm New Password</label>
                            <input type="password"
                                   class="form-control" name="con_new_pass" id="con_new_pass" required>
                        </div>
                    </div>
                </div>
                

                <button type="submit" class="btn btn-info btn-fill pull-right">Change</button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
