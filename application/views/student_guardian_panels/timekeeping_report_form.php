<div class="col-md-12">
    <div class="card">
        <div class="header">

            <?php
            $message = $this->session->userdata('message');
            if ($message != '') {
                ?>
                <div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span>
                        <?php
                        echo $message;
                        $this->session->unset_userdata('message');
                        ?>
                    </span>
                </div>
                <?php
            }
            ?>

            <?php
            $exception = $this->session->userdata('exception');
            if ($exception != '') {
                ?>
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    <span>
                        <?php
                        echo $exception;
                        $this->session->unset_userdata('exception');
                        ?>
                    </span>
                </div>
                <?php
            }
            ?>
            <h4 class="title">Student Leave Information</h4>
        </div>
        <div class="content">
            <form action="<?php echo base_url(); ?>student_guardian_panels/timekeeping_report" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Year</label>
                            <select class="form-control" name="year" id="year" required="1">
                                <option value="">-- Please Select --</option>
                                <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                            </select>
                            <input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Month</label>
                            <select class="form-control" name="month" id="month" required="1">
                                <option value="">-- Please Select --</option>
                                <?php
                                $i = 1;
                                while ($i <= 12) {
                                    $dateObj = DateTime::createFromFormat('!m', $i);
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $dateObj->format('F'); ?></option>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-info btn-fill pull-right">Preview</button>
                <div class="clearfix"></div>
            </form>
        </div>

<hr>
        <?php
        if (isset($timekeeping_info)) {
            ?>
            <div class="header">
                <h4 class="title">
                    Attendance Information
                </h4>
            </div>
            <div class="content table-responsive table-full-width">
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Date</th>
                        <th>Login Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $i = 0;
                    foreach ($timekeeping_info as $row):
                        $i++;
                        ?>
                        <tr>

                        <tr>
                            <td width="34">
                                <?php echo $i; ?>
                            </td>
                            <td><?php echo $row['date']; ?></td>
                            <td>
                                <?php
                                if($row['login_status'] == 'P'){
                                    echo 'Present';
                                }else if($row['login_status'] == 'A'){
                                    echo 'Absent';
                                }else{
                                    echo 'Leave';
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <?php
        }
        ?>

    </div>
</div>



