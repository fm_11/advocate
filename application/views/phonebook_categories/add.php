
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>phonebook_categories/add" method="post">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('category') . ' ' . $this->lang->line('name'); ?></label>
			<input required type="text" name="categoryName" class="form-control" value="">
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('category') . ' ' . $this->lang->line('note'); ?></label>
			<input required type="text" name="CategoryNote" class="form-control" value="">
		</div>

	</div>

	<div class="float-right">
		 <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		 <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
