<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
  font-size: 11px;
  padding-left: 3px !important;
  padding-right: 3px !important;
  padding: 0px;
}
table {
  border-collapse: collapse;
}
.h_td{
   font-weight: bold !important;
}
</style>

<?php
 } ?>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table table-bordered">
    <thead>
    <tr>
        <td colspan="7"  class="h_td" style="text-align:center; line-height:1.5;">
            <b><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
            <?php echo $HeaderInfo[0]['address']; ?><br>
            <?php echo $title; ?> <?php if(!empty($from_date)){?> of <b>(<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)</b><?php }?>
        </td>
    </tr>

    <tr>
        <td scope="col" class="h_td"><?php echo  $this->lang->line('sl');?></td>
        <td scope="col" class="h_td" align="center"><?php echo $this->lang->line('case_no');?></td>
        <td scope="col" class="h_td"><?php echo  $this->lang->line('previous_hearing_date');?></td>
        <td scope="col" class="h_td"><?php echo  $this->lang->line('courts').' '.$this->lang->line('name');?></td>
        <td scope="col" class="h_td"><?php echo  $this->lang->line('parties_name');?></td>
        <td scope="col" class="h_td"><?php echo  $this->lang->line('description');?></td>
        <td scope="col" class="h_td"><?php echo  $this->lang->line('first_hearing_date');?></td>
    </tr>
    </thead>
    <tbody>
    <?php
  $i = 0;
    foreach ($attendaces as $row):
        $i++;
          ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td class="td_center"><?php echo $row['case_no'];?></td>
            <td class="td_center"><?php echo $row['befor_date'];?></td>
            <td class="td_center"><?php echo $row['court_name'].'('.$row['court_type_name'].')';?></td>
            <td class="td_center"><?php echo $row['parties_name'];?></td>
            <td class="td_center"><?php echo $row['remarks'];?></td>
            <td class="td_center"><?php echo $row['after_date'];?></td>

        </tr>
    <?php
       endforeach; ?>

    </tbody>
</table>
</div>
