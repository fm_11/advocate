<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getDisTrictWiseThanaOrCourt(district_id,type){
       if(type=='T')
       {
         document.getElementById("gr_cr_upazilla_id").innerHTML = "<option>--Please Select---</option>";
       }else if (type=='C') {
         document.getElementById("court_type_id").innerHTML = "<option>--Please Select---</option>";
         document.getElementById("court_id").innerHTML = "<option>--Please Select---</option>";
       }

        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
              if(type=='T')
              {
                document.getElementById("gr_cr_upazilla_id").innerHTML = xmlhttp.responseText;
              }else if (type=='C') {
                document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
              }

            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
        xmlhttp.send();
    }
    function getCourtTypeByCourtId(court_id){
      //  alert(country_id);
        document.getElementById("court_id").innerHTML = "<option>--Please Select---</option>";
        var district_id=  document.getElementById("district_id").value;
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("court_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
        xmlhttp.send();
    }
</script>

<form name="addForm" id="addForm"  action="<?php echo base_url(); ?>Case_file_download_report/index" method="post">
  <div class="form-row">

    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('case_no'); ?></label>
      <select   name="case_master_id" id="case_master_id" class="js-example-basic-single w-100" >
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($case_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
      <!-- <input type="text" name="case_no" value="<?php if (isset($case_no)) {echo $case_no;} ?>" placeholder="<?php echo $this->lang->line('case_no'); ?>" class="form-control" id="case_no"> -->
    </div>
    <div class="form-group col-md-6">
      <label for="inputEmail4"><?php echo $this->lang->line('users_name').' '.$this->lang->line('name'); ?></label>
      <select   name="client_user_id" id="client_user_id" class="js-example-basic-single w-100" >
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($client_user as $row) { ?>
          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>


<hr>

  <div class="btn-group float-right">
    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('view_report'); ?>">
  </div>

</form>


<div id="printableArea">
    <?php
    if (isset($report)) {
        echo $report;
    }
    ?>
</div>
