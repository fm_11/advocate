<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>institute_setup/update" method="post" enctype="multipart/form-data">
  <div class="form-row">

    <div class="form-group col-md-6">
      <label>Office Name (English)</label>
      <input type="hidden" name="id"  class="form-control" value="<?php echo $contact_info->id;  ?>">
      <input type="text" autocomplete="off"  name="txtSchoolName"  class="form-control" value="<?php echo $contact_info->school_name;  ?>">
    </div>

    <div class="form-group col-md-6">
      <label>Office Name (Bangla)</label>
      <input type="text" autocomplete="off"  name="school_name_bangla"  class="form-control" value="<?php echo $contact_info->school_name_bangla;  ?>">
    </div>



  </div>

  <div class="form-row">
    <div hidden class="form-group col-md-4">
      <label>Institute Type</label>
      <select class="js-example-basic-single w-100" name="institute_type" required="1">
       <option value="">-- Select --</option>
       <?php
       $i = 0;
       if (count($institute_type)) {
           foreach ($institute_type as $list) {
               $i++; ?>
               <option
                       value="<?php echo $list['id']; ?>" <?php if ($list['id'] == $contact_info->institute_type) {
                   echo 'selected';
               } ?>><?php echo $list['name']; ?></option>
               <?php
           }
       }
       ?>
        </select>
    </div>
    <div hidden class="form-group col-md-4">
      <label>Institute ID</label>
      <input type="text" autocomplete="off" readonly  name="txtSchoolShortName"  class="form-control" value="<?php echo $contact_info->school_short_name;  ?>">
    </div>
    <div hidden class="form-group col-md-4">
      <label>EIIN Number</label>
      <input type="text" autocomplete="off"  name="txtEiinNumber"  class="form-control" value="<?php echo $contact_info->eiin_number;  ?>">
    </div>
  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="text" autocomplete="off"  name="txtEmail"  class="form-control" value="<?php echo $contact_info->email;  ?>">
    </div>
    <div class="form-group col-md-6">
      <label>Mobile</label>
      <input type="text" autocomplete="off"  name="txtMobile"  class="form-control" value="<?php echo $contact_info->mobile;  ?>">
    </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Address (English)</label>
      <textarea name="txtAddress"  class="form-control" rows="5"><?php echo $contact_info->address;  ?></textarea>
    </div>

    <div class="form-group col-md-6">
      <label>Address (Bangla)</label>
      <textarea name="address_bangla"  class="form-control" rows="5"><?php echo $contact_info->address_bangla;  ?></textarea>
    </div>
  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Facebook Address</label>
      <input type="text" autocomplete="off"  name="txtFacebookAddress"  class="form-control" value="<?php echo $contact_info->facebook_address;  ?>">
    </div>

    <div class="form-group col-md-6">
      <label>Website Address</label>
      <input type="text" autocomplete="off"  name="txtWebAddress"  class="form-control" value="<?php echo $contact_info->web_address;  ?>">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6 text-center">
      <img style="max-width: 250px; max-height:  250px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $contact_info->picture; ?>"
      class="img"/>
    </div>

    <div class="form-group col-md-6">
      <label>New Picture</label>
      <input type="file" name="txtPhoto" id="txtPhoto" class="form-control">
      <span style="color: #FF0000;" id="file_error"></span>
    </div>
  </div>



  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
  </div>

</form>
