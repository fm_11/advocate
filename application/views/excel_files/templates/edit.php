<script type="text/javascript">
    function count_alpha(val) {
        var length = val.length;
        /*	alert(length);
         */
        var diff = 160 - length;
        document.getElementById("count_alert").innerHTML = diff + " characters left.";

        /*	document.getElementById("count_alert").innerHTML = length+" characters.";
         */
    }
</script>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>templates/edit/<?php echo $this->uri->segment(3); ?>"
      method="post">
	  
	  <div class="form-row">
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('template') . ' ' . $this->lang->line('name'); ?></label>
			<input value="<?php echo $EditData['template_name']; ?>" required type="text" name="templateName"
				   class="form-control" value="">
		</div>
		
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('template') . ' ' . $this->lang->line('body'); ?></label>
			<textarea required name="templateBody" id="sms_content" maxlength="160" onkeyup="count_alpha(this.value);" rows="5"
					  cols="50" class="form-control"><?php echo $EditData['template_body']; ?></textarea>
		</div>
		
		<div class="form-group col-md-3">
		
			 <label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('admission') . ' ' . $this->lang->line('welcome') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="form-control" name="is_admission_welcome_sms">
				<option value="0" <?php if ($EditData['is_admission_welcome_sms'] == 0) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('no'); ?>
				</option>
				<option value="1" <?php if ($EditData['is_admission_welcome_sms'] == 1) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('yes'); ?>
				</option>
			</select>
		</div>
		
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('present') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="form-control" name="is_present_sms">
				<option value="0" <?php if ($EditData['is_present_sms'] == 0) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('no'); ?>
				</option>
				<option value="1" <?php if ($EditData['is_present_sms'] == 1) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('yes'); ?>
				</option>
			</select>
		</div>
	</div>	
	<div class="form-row">	
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('absent') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="form-control" name="is_absent_sms">
				<option value="0" <?php if ($EditData['is_absent_sms'] == 0) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('no'); ?>
				</option>
				<option value="1" <?php if ($EditData['is_absent_sms'] == 1) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('yes'); ?>
				</option>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('leave') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="form-control" name="is_leave_sms">
				<option value="0" <?php if ($EditData['is_leave_sms'] == 0) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('no'); ?>
				</option>
				<option value="1" <?php if ($EditData['is_leave_sms'] == 1) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('yes'); ?>
				</option>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('bangla') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="form-control" name="is_bangla_sms">
				<option value="0" <?php if ($EditData['is_bangla_sms'] == 0) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('no'); ?>
				</option>
				<option value="1" <?php if ($EditData['is_bangla_sms'] == 1) {
					echo 'selected';
				} ?>><?php echo $this->lang->line('yes'); ?>
				</option>
			</select>
		</div>
	</div>	
    <br>
    <div class="row">&nbsp;&nbsp;<span id="count_alert"></span></div>
    <br>
    <div class="float-right">
	   <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
