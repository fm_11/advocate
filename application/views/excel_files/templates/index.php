<div class="table-responsive-sm">
   <table  class="table"> 
	   <thead>
		<tr>
			<th scope="col"><?php echo $this->lang->line('sl'); ?></th>
			<th scope="col"><?php echo $this->lang->line('template') . ' ' . $this->lang->line('name'); ?></th>
			<th scope="col"><?php echo $this->lang->line('present') . ' ' . $this->lang->line('sms'); ?> ?</th>
			<th scope="col"><?php echo $this->lang->line('absent') . ' ' . $this->lang->line('sms'); ?></th>
			<th scope="col"><?php echo $this->lang->line('leave') . ' ' . $this->lang->line('sms'); ?></th>
			<th scope="col"><?php echo $this->lang->line('template'). ' ' . $this->lang->line('body'); ?></th>
			<th scope="col"><?php echo $this->lang->line('action'); ?></th>
		</tr>
		</thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($templateList as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td style="vertical-align: middle;" width="34">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align: middle;"><?php echo $row['template_name']; ?></td>
            <td style="vertical-align: middle;">
                <?php
                if($row['is_present_sms'] == 1){
                    echo $this->lang->line('sl');
                }else{
                    echo $this->lang->line('sl');
                }
                ?>
            </td>
            <td style="vertical-align: middle;">
                <?php
                if($row['is_absent_sms'] == 1){
                    echo $this->lang->line('sl');
                }else{
                    echo $this->lang->line('sl');
                }
                ?>
            </td>
            <td style="vertical-align: middle;">
                <?php
                if($row['is_leave_sms'] == 1){
                    echo $this->lang->line('sl');
                }else{
                    echo $this->lang->line('sl');
                }
                ?>
            </td>
            <td><?php echo $row['template_body']; ?></td>
            <td>
                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>templates/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                        <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>templates/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                    </div>
                </div>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
