<div id="divPrintable" class="box-body" style="padding-left:0px; padding-right:0px">

	<?php echo $report_header; ?>

	<table id="tblCustomer" style="width: 100%; border-top: 5px solid <?php echo $this->session->userdata('table_header_row_color'); ?>; margin-top: 10px; font-size: 14px">

		<tr>
			<td style="text-align: center; text-decoration: underline;">
				<span style="text-transform: uppercase;"><b>
					<?php echo $title; ?>
					             <?php if(!empty($from_date)){?> of <b>(<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)</b><?php }?>
				</b>
				</span>
			</td>
		</tr>
	</table>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="tblItems" class="sortable-table-1" style="width: 100%; border-top: 1px solid #dedede; margin-top: 15px; font-size:13px">
    <thead>
    <tr style="border-bottom: 1px solid #333;background-color:<?php echo $this->session->userdata('table_header_row_color'); ?>; border-top: 1px solid #333;">

        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('sl'); ?></th>
	    	<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('case_no'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('upazila'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('client'); ?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('mobile').' '.$this->lang->line('number');?></th>
        <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('court');?></th>
				<th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('first_hearing_date');?></th>
			 <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo  $this->lang->line('file_colour');?></th>
			 <th style="color:#ffffff;text-align: center; padding: 5px;" scope="col"><?php echo $this->lang->line('action').'/'.$this->lang->line('order'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr <?php if($i % 2 == 0){ echo 'style="background-color:' . $this->session->userdata('REPORT_ROW_COLOR'). ' !important;"'; } ?>>

						<td>
                <?php echo $i; ?>
            </td>
						<td class="td_center">
							<?php
							if($language=="english")  {
							echo $row['case_sub_type_name'].'-'.$row['case_no'];

							}else{
							echo $row['case_sub_type_bname'].'-'.$row['case_no'];
							}
							?>

						</td>
						<td class="td_center">
							<?php
							if($language=="english")  {
							echo $row['upazila_name'];

							}else{
							echo $row['upazila_bnname'];
							}
							?>

						</td>
						<td class="td_center"><?php echo $row['client_first_name']; ?></td>
						<td class="td_center"><?php echo $row['client_mobile'];?></td>
						<td class="td_center">
								<?php
								if($language=="english")  {
								echo $row['court_name'];
								}else{
							 echo $row['court_bn_name'];
								}

								?>
							</td>
							<td><?php echo date("d-m-Y", strtotime($row['next_date'])); ?></td>
							<td>
								<?php
								if($language=="english")  {
								echo $row['file_colour'];

								}else{
							 echo $row['bn_file_colour'];
								}
								?>
							</td>
							<td class="td_center">
							<?php
							if($language=="english")  {
							echo $row['status_name'];

							}else{
						 echo $row['status_bn_name'];
							}
							?></td>
        </tr>
    <?php endforeach; ?>




      <!-- <tr>
           <td class="textleft" colspan="6"><b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
       </tr> -->
    </tbody>
</table>
</div>
</div>

<?php echo $report_footer;	?>
