
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>tax/add" class="cmxform" method="post">
  <fieldset>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="">
        </div>
    <div class="form-group">
      <div class="form-group col-md-8">
        <label for="inputEmail4"><?php echo $this->lang->line('percentage'); ?><span class="required_label">*</span></label>
        <input type="text" autocomplete="off"  class="form-control" id="percentage" required name="percentage" value="">
      </div>
      <div class="form-group col-md-4">
  		  <label for="inputEmail4"><?php echo $this->lang->line('is_active'); ?> ?</label>
  		  <input name="is_active" type="checkbox" value="1">
  	  </div>
    </div>

  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
