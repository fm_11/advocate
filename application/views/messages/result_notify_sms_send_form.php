<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>


<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>messages/to_result_notify" method="post"
      enctype="multipart/form-data">
	  
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('class'); ?></label>
			<select class="smallInput" name="txtClass" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class)) {
					foreach ($class as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($class_id)) {
							if ($class_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('section'); ?></label>
			<select class="smallInput" name="txtSection" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($section)) {
					foreach ($section as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($section_id)) {
							if ($section_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>	
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('group'); ?></label>
			<select class="smallInput" name="txtGroup" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($group)) {
					foreach ($group as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($group_id)) {
							if ($group_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>	
	<div class="form-row">	
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('shift'); ?></label>
			<select class="smallInput" name="txtShift" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($shift)) {
					foreach ($shift as $list) {
						$i++;
						?>
						<option
							value="<?php echo $list['id']; ?>" <?php if (isset($shift_id)) {
							if ($shift_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-3">

			<label><?php echo $this->lang->line('exam'); ?></label>
			<select class="smallInput" name="txtExam" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($exam)) {
					foreach ($exam as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($exam_id)) {
							if ($exam_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name'] . '(' . $list['year'] . ')'; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-3">
		
			<label><?php echo $this->lang->line('send') . ' ' . $this->lang->line('marks') . ' ' . $this->lang->line('or') . ' ' .  $this->lang->line('gpa'); ?></label>
			<select class="smallInput" name="txtSMSType" required="1">
				<option value="MG"><?php echo $this->lang->line('marks') . ' ' . $this->lang->line('and') . ' ' . $this->lang->line('gpa'); ?></option>
				<option value="M"><?php echo $this->lang->line('marks'); ?></option>
				<option value="G"><?php echo $this->lang->line('gpa'); ?></option>
			</select>
		</div>
		
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('with_institute_name'); ?> ?</label>
			<select class="smallInput" name="txtWithSchoolName" required="1">
				<option value="Y"><?php echo $this->lang->line('yes'); ?></option>
				<option value="N"><?php echo $this->lang->line('no'); ?></option>
			</select>
		</div>
    </div>
	
    <input type="submit" class="submit" value="Process">
</form><br/>

<div>
    <?php
    if (isset($student_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>messages/to_result_notify_sms_send"
              method="post"
              enctype="multipart/form-data">
            <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="submit" class="submit" value="Send SMS">
                    </td>
                </tr>
                <tr>
                    <th width="50" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th width="110" scope="col"><?php echo $this->lang->line('student_code'); ?></th>
                    <th width="160" scope="col"><?php echo $this->lang->line('name'); ?></th>
                    <th width="50" scope="col"><?php echo $this->lang->line('roll'); ?></th>
                    <th width="70" scope="col"><?php echo $this->lang->line('gender'); ?></th>
                    <th width="110" scope="col"><?php echo $this->lang->line('father_name'); ?></th>
                    <th width="100" scope="col"><?php echo $this->lang->line('guardian_mobile'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($student_info as $row):
                    ?>
                    <tr>
                        <td width="34">
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $row['student_code']; ?></td>
                        <td>
                            <?php echo $row['name']; ?>
                            <input type="hidden" name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>">
                            <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
                        </td>
                        <td><?php echo $row['roll_no']; ?></td>
                        <td>
                            <?php
                            if ($row['gender'] == 'M') {
                                echo "Male";
                            } else {
                                echo "Female";
                            }
                            ?>
                        </td>
                        <td><?php echo $row['father_name']; ?></td>
                        <td>
                            <input type="text" autocomplete="off"  value="<?php echo $row['guardian_mobile']; ?>"
                                   name="guardian_mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                    <?php $i++; endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
                        <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                        <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
                        <input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
                        <input type="hidden" name="exam_id" value="<?php echo $exam_id; ?>">
                        <input type="hidden" name="sms_type" value="<?php echo $sms_type; ?>">
                        <input type="hidden" name="with_school_name" value="<?php echo $with_school_name; ?>">
                        <input type="submit" class="submit" value="Send SMS">
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
        </form><br/>
        <?php
    }
    ?>
</div>
<div class="clear"></div><br/>
