<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>messages/AddNewTemplate"><span><?php echo $this->lang->line('add') . ' ' . $this->lang->line('new') . ' ' . $this->lang->line('template'); ?></span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('template') . ' ' . $this->lang->line('name'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('present') . ' ' . $this->lang->line('sms'); ?> ?</th>
        <th width="100" scope="col"><?php echo $this->lang->line('absent') . ' ' . $this->lang->line('sms'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('leave') . ' ' . $this->lang->line('sms'); ?></th>
        <th width="250" scope="col"><?php echo $this->lang->line('template'). ' ' . $this->lang->line('body'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('action'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($templateList as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td style="vertical-align: middle;" width="34">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align: middle;"><?php echo $row['template_name']; ?></td>
            <td style="vertical-align: middle;">
                <?php
                if($row['is_present_sms'] == 1){
                    echo $this->lang->line('sl');
                }else{
                    echo $this->lang->line('sl');
                }
                ?>
            </td>
            <td style="vertical-align: middle;">
                <?php
                if($row['is_absent_sms'] == 1){
                    echo $this->lang->line('sl');
                }else{
                    echo $this->lang->line('sl');
                }
                ?>
            </td>
            <td style="vertical-align: middle;">
                <?php
                if($row['is_leave_sms'] == 1){
                    echo $this->lang->line('sl');
                }else{
                    echo $this->lang->line('sl');
                }
                ?>
            </td>
            <td><?php echo $row['template_body']; ?></td>
            <td style="vertical-align:middle;">
				<a href="<?php echo base_url(); ?>messages/template_edit/<?php echo $row['id']; ?>"
                   title="Edit"><?php echo $this->lang->line('edit'); ?></a>&nbsp;
                <a href="<?php echo base_url(); ?>messages/template_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
