<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>


    <?php
    if (isset($student_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>messages/absent_sms_send_to_guardian" method="post"
              enctype="multipart/form-data">
            <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('name'); ?>
                    </td>
                    <td colspan="5">
                        <select class="smallInput" name="template_id" onchange="getTemplateBody(this.value)"
                                required="1">
                            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                            <?php
                            $i = 0;
        if (count($templates)) {
            foreach ($templates as $list) {
                $i++; ?>
                                    <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
                                    <?php
            }
        } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="vertical-align: middle;text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
                    </td>
                    <td colspan="5">
                        <textarea name="template_body" required rows="5" cols="35" id="template_body"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="submit" class="submit" value="Send SMS">
                    </td>
                </tr>
                <tr>
                    <th width="50" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th width="20" scope="col"><?php echo $this->lang->line('sl'); ?></th>
                    <th width="110" scope="col"><?php echo $this->lang->line('student_code'); ?></th>
                    <th width="160" scope="col"><?php echo $this->lang->line('name'); ?></th>
                    <th width="50" scope="col"><?php echo $this->lang->line('roll'); ?></th>
                    <th width="70" scope="col"><?php echo $this->lang->line('class'); ?></th>
                    <th width="100" scope="col"><?php echo $this->lang->line('guardian_mobile'); ?> Number</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
        foreach ($student_info as $row):
                    ?>
                    <tr>
                        <td width="34">
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo $row['student_code']; ?></td>
                        <td>
                            <?php echo $row['name']; ?>
                        </td>
                        <td><?php echo $row['roll_no']; ?></td>

                        <td><?php echo $row['class_name']; ?></td>
                        <td>
                            <input type="text" autocomplete="off"  value="<?php echo $row['guardian_mobile']; ?>" name="guardian_mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                <?php $i++;
        endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
                        <input type="submit" class="submit" value="Send SMS">
                    </td>
                </tr>
                </tbody>
            </table>      
        </form>
        <?php
    }
    ?>
