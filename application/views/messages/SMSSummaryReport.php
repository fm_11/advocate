<form name="addForm" class="cmxform" id="commentForm"   enctype="multipart/form-data" action="<?php echo base_url(); ?>messages/SMSSummaryReport" method="post">
    <!--<label>From Date: <input type="text" autocomplete="off"  class="smallInput" name="FromDate" id="FromDate" required="1"/> </label>

	<label>To Date</label>
    <input type="text" autocomplete="off"  class="smallInput" name="ToDate" id="ToDate" required="1"/>



    <br>
    <br>
    <input name="submit" type="submit" class="submit" value="Search">
	 <br>
    <br>-->
	<table width="80%" id="box-table-a">
		<tr>
			<th width="70" scope="col"><b><?php echo $this->lang->line('from_date'); ?>:<b></th>
			<td><input type="text" autocomplete="off"  class="smallInput" name="FromDate" id="FromDate" required="1"/></td>
			<th width="70" scope="col"><b><?php echo $this->lang->line('to_date'); ?>:<b></th>
			<td><input type="text" autocomplete="off"  class="smallInput" name="ToDate" id="ToDate" required="1"/></td>
		</tr>
		<!--<tr>
			<th width="70" scope="col">Select Category: </th>
			<td>
				<select class="smallInput" name="ReceiverType" required="1">
					<option value="">-- Please Select --</option>
					<option value="Student">To Guardian</option>
					<option value="Teacher">Teacher</option>
				</select>
			</td>
		</tr>-->
		<tr>
			<td colspan='4' align='center' ><input name="submit" type="submit" class="submit" value="Search"></td>
		</tr>
		<br>

	</table>
</form>

<?php

	if(isset($DetailsSMSReport)) {
		/* echo '<pre>';
			print_r($DetailsSMSReport['report']);
			die();  */
		if($DetailsSMSReport['status']==1){
?>
			<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
				<thead>
					<tr>
						<th width="20" scope="col"> <?php echo $this->lang->line('sl'); ?> </th>
						<th width="150" scope="col"> <?php echo $this->lang->line('date'); ?> </th>
						<th width="40" scope="col"><?php echo $this->lang->line('sms') . ' ' . $this->lang->line('type'); ?> </th>
						<th width="40" scope="col"> <?php echo $this->lang->line('total') . ' ' . $this->lang->line('sms'); ?> </th>
					</tr>
				</thead>
    <tbody>
    <?php
    $i = 0;
	$totalSMS = 0;
    foreach ($DetailsSMSReport['report'] as $row):
        $i++;
		/* echo '<pre>';
			print_r($row);
			die(); */
		$totalSMS = $totalSMS + $row['numberOfSMS'];
        ?>
        <tr>
            <td width="34"><?php echo $i; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['messageName']; ?></td>
            <td><?php echo $row['numberOfSMS'];?></td>
        </tr>
    <?php endforeach; ?>
	<tr>
            <th width="34" colspan="3">Total</th>

            <td><?php echo $totalSMS; ?></td>
        </tr>
    </tbody>
</table>
<?php
		}else{
			echo "No Result Found";
		}
	}
?>




<script>
    $(function() {
        $( "#FromDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
	$(function() {
        $( "#ToDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
