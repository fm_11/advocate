<form name="addForm"  class="cmxform" id="commentForm"    action="<?php echo base_url(); ?>messages/absent_sms_send" method="post">
		<div class="form-row">
			<div class="form-group col-md-6">
				<label>Shift</label>
				<select class="smallInput" name="txtShift" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
                    $i = 0;
                    if (count($shift)) {
                        foreach ($shift as $list) {
                            $i++; ?>
							<option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
							<?php
                        }
                    }
                    ?>
				</select>
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('date'); ?></label>
			<input type="text" autocomplete="off"  class="smallInput" placeholder="YYYY-MM-DD" name="txtDate" id="txtDate" required="1"/>
		</div>
	</div>

	<div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('process'); ?>">
  </div>


</form>
