<form name="addForm" class="cmxform" id="commentForm"   enctype="multipart/form-data" action="<?php echo base_url(); ?>messages/GetGuardianSMSReport" method="post">

	<div class="form-row">
		<div class="form-group col-md-6">
			<label>From Date</label>
			<input type="text" autocomplete="off"  class="smallInput" name="FromDate" id="FromDate" required="1"/>
		</div>
		
		<div class="form-group col-md-6">
			<label>To Date</label>
			<input type="text" autocomplete="off"  class="smallInput" name="ToDate" id="ToDate" required="1"/>
		</div>

    <input name="submit" type="submit" class="submit" value="Search">
	 <br>
    <br>
</form>

<?php

	if(isset($GuardianSMSReport)) {
		/* echo '<pre>';
			print_r($GuardianSMSReport);
			die(); */
		if($GuardianSMSReport['status']==1){
?>
			<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
				<thead>
					<tr>
						<th width="20" scope="col"><?php echo $this->lang->line('sl'); ?></th>
						<th width="150" scope="col"><?php echo $this->lang->line('student') . ' ' . $this->lang->line('name'); ?></th>
						<th width="40" scope="col"><?php echo $this->lang->line('class'); ?></th>
						<th width="40" scope="col"><?php echo $this->lang->line('section'); ?></th>
						<th width="40" scope="col"><?php echo $this->lang->line('group'); ?></th>
						<th width="70" scope="col"><?php echo $this->lang->line('mobile'); ?></th>
						<th width="70" scope="col"><?php echo $this->lang->line('date'); ?></th>
						<th width="90" scope="col"><?php echo $this->lang->line('template') . ' ' . $this->lang->line('name'); ?></th>
						<th width="100" scope="col"><?php echo $this->lang->line('sms'); ?></th>
					</tr>
				</thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($GuardianSMSReport['GuardianSMSReport'] as $row):
        $i++;
        ?>
        <tr>
            <td width="34"><?php echo $i; ?></td>
            <td><?php echo $row['studentName']; ?></td>
            <td><?php echo $row['className']; ?></td>
            <td><?php echo $row['sectionName']; ?></td>
            <td><?php echo $row['groupName']; ?></td>
            <td><?php echo $row['mobile_number']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['template_name']; ?></td>
            <td><?php echo $row['template_body']; ?></td>


        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php
		}else{
			echo "No Result Found";
		}
	}
?>




<script>
    $(function() {
        $( "#FromDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
	$(function() {
        $( "#ToDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
