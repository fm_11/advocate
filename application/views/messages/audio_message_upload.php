<h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>messages/audio_message_upload" method="post" enctype="multipart/form-data">
	
		<div class="form-row">
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('title'); ?></label>
				<input type="text" autocomplete="off"  class="smallInput wide" name="title" required="1"/>
			</div>
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('file'); ?></label>
				<input type="file" name="txtFile" required="1" class="smallInput"> * File Format -> mp3
			</div>
		</div>	
        
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />
<div class="clear"></div><br />
