<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getDisTrictWiseThanaOrCourt(district_id,type){
       if(type=='T')
       {
         document.getElementById("gr_cr_upazilla_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?> --</option>";
       }else if (type=='C') {
         document.getElementById("court_type_id").innerHTML = "<option>--Please Select---</option>";
         document.getElementById("court_id").innerHTML = "<option>--Please Select---</option>";
       }

        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
              if(type=='T')
              {
                document.getElementById("gr_cr_upazilla_id").innerHTML = xmlhttp.responseText;
              }else if (type=='C') {
                document.getElementById("court_type_id").innerHTML = xmlhttp.responseText;
              }

            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_disTrictWiseThanaOrCourt_by_district_id?district_id=" + district_id+"&type="+type, true);
        xmlhttp.send();
    }
    function getCourtTypeByCourtId(court_id){
      //  alert(country_id);
        document.getElementById("court_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?> --</option>";
        var district_id=  document.getElementById("district_id").value;
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("court_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>cases/ajax_courts_type_by_court?court_id=" + court_id+"&district_id="+district_id, true);
        xmlhttp.send();
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>income_reports/index" method="post">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label><?php echo $this->lang->line('from_date'); ?></label>
      <div class="input-group date">
              <input type="text" autocomplete="off" name="from_date" value="<?php if (isset($from_date)) {echo $from_date;} ?>" class="form-control">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label><?php echo $this->lang->line('to_date'); ?></label>
      <div class="input-group date">
              <input type="text" autocomplete="off" name="to_date"  value="<?php if (isset($to_date)) {
    echo $to_date;
} ?>" class="form-control">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label><?php echo $this->lang->line('case_no'); ?></label>
      <select   name="case_master_id" id="case_master_id" class="js-example-basic-single w-100" >
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($case_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($case_master_id)) {
           if ($row['id'] == $case_master_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="inputEmail4"><?php echo $this->lang->line('users_name').' '.$this->lang->line('name'); ?></label>
      <select   name="client_user_id" id="client_user_id" class="js-example-basic-single w-100" >
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($client_user as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($client_user_id)) {
           if ($row['id'] == $client_user_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label><?php echo $this->lang->line('district'); ?></label>
      <select onchange="getDisTrictWiseThanaOrCourt(this.value,'C')"  name="district_id" id="district_id" class="js-example-basic-single w-100" >
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($districts as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($district_id)) {
           if ($row['id'] == $district_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>

    </div>
    <div class="form-group col-md-3">
      <label for="inputEmail4"><?php echo $this->lang->line('court_type'); ?></label>
      <select onchange="getCourtTypeByCourtId(this.value)"  name="court_type_id" id="court_type_id" class="js-example-basic-single w-100">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($courts_type as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($court_type_id)) {
           if ($row['id'] == $court_type_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="inputEmail4"><?php echo $this->lang->line('court_sub_type'); ?></label>
      <select   name="court_id" id="court_id" class="js-example-basic-single w-100">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($courts as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($court_id)) {
           if ($row['id'] == $court_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printPartOfPage()" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>
</form>

<?php
if (isset($idata)) {
	echo $report;
}
?>
