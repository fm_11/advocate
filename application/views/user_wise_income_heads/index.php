<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>user_wise_income_heads/index" method="post">
<div class="table-responsive-sm">
    <table class="table table-hover">
 <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="100" scope="col">User Name</th>
        <th width="100" scope="col">Asset Category</th>
        <!-- <th width="100" scope="col">Opening Balance</th> -->
    </tr>

    <?php
    $i = 0;
    if (isset($user_info)) {
        ?>
        <?php
        foreach ($user_info as $row):
            ?>
            <tr>

            <tr>
                <td>
                    <?php echo $i + 1; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" name="user_id_<?php echo $i; ?>"
                           value="<?php echo $row['user_id_main']; ?>">
                </td>
                <td>
                  <select class="form-control" id="asset_category_id_" name="asset_category_id_<?php echo $i; ?>">
                      <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                      <?php

        if (count($asset_category)) {
            foreach ($asset_category as $list) {
                ?>
                              <option
                                  value="<?php echo $list['id']; ?>" <?php if ($row['asset_category_id'] == $list['id']) {
                    echo 'selected';
                } ?>><?php echo $list['name']; ?></option>
                          <?php
            }
        } ?>
                  </select>
                </td>

                <!-- <td>
                    <input type="text" autocomplete="off"  class="form-control" style="text-align: right;"
                           name="opening_balance_<?php echo $i; ?>"
                           value="<?php if ($row['opening_balance'] != '') {
            echo $row['opening_balance'];
        } else {
            echo 0;
        } ?>">
                </td> -->

            </tr>
        <?php $i++;
        endforeach;
    } ?>

</table>
</div>
<div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
    <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div>
<input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
</form>
