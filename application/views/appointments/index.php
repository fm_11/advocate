<script type="text/javascript">


    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById(id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>appointments/updateMsgClientsStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }
</script>


<?php
$from_date = $this->session->userdata('from_date');
$to_date = $this->session->userdata('to_date');
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>appointments/index">
<div class="col-md-offset-2 col-md-12">
        <label class="sr-only" for="FromDate"><?php echo $this->lang->line('from_date'); ?></label>
        <?php
         $placeholder = 'dd-mm-yyyy';
        ?>
        <div class="form-group row">

        <div class="form-group col-md-3">
        <div class="input-group date">
        <input type="text" autocomplete="off"  style="max-width:180px;height: 45px;margin-top: -4px;"
         name="from_date" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($from_date)) {
            echo date("d-m-Y", strtotime($from_date));
        } ?>" class="form-control" id="from_date">
        <span class="input-group-text input-group-append input-group-addon">
            <i class="simple-icon-calendar"></i>
        </span>
      </div>
      </div>

        <label class="sr-only" for="ToDate"><?php echo $this->lang->line('to_date'); ?></label>
        <?php
         $placeholder = 'dd-mm-yyyy';
        ?>
        <div class="form-group col-md-3">
        <div class="input-group date">
        <input type="text" autocomplete="off"  style="max-width:180px;height: 45px;margin-top: -4px;"
         name="to_date" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($to_date)) {
            echo date("d-m-Y", strtotime($to_date));
        } ?>" class="form-control" id="to_date">
        <span class="input-group-text input-group-append input-group-addon">
            <i class="simple-icon-calendar"></i>
        </span>
      </div>
      </div>

        <!-- <button type="reset" style="margin-top: -5px;" class="btn btn-light"><?php echo $this->lang->line('cancel'); ?></button> -->
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('clients') .' '. $this->lang->line('name')?></th>
        <th><?php echo $this->lang->line('mobile'); ?></th>
        <th><?php echo $this->lang->line('date'); ?></th>
        <th><?php echo $this->lang->line('time'); ?></th>
        <th><?php echo $this->lang->line('sms'); ?></th>
        <th><?php echo $this->lang->line('is_any_number'); ?></th>
        <th><?php echo $this->lang->line('notes'); ?></th>
        <th><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>

    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($appointments as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td >
                <?php echo $i; ?>
            </td>

               <td><?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?></td>
              <td><?php
                 if($row['is_any_number']==1)
                 {
                     echo $row['mobile'];
                 }else{
                     echo $row['client_mobile'];
                 }
              ?></td>
              <td><?php echo date("d-m-Y", strtotime($row['date'])) ?></td>
              <td><?php echo date('h:i A', strtotime($row['time'])); ?></td>
              <td>
                <?php
                   if($row['is_sms']==1)
                   {
                       echo 'Yes';
                   }else{
                       echo 'No';
                   }
                ?>
              </td>
              <td>
                <!-- <select id="<?php echo $row['id']; ?>" onchange="msgStatusUpdate(this.id,this.value)" class="js-example-basic-single w-200">
                <option value="O">Open</option>
                <option value="CBC">Closed By Client</option>
                <option value="CBA">Closed By Advocate</option>
              </select> -->
              <?php
                 if($row['is_any_number']==1)
                 {
                     echo 'Yes';
                 }else{
                     echo 'No';
                 }
              ?>
            </td>
                  <td><?php echo $row['notes']; ?></td>

            <td>
                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                      <?php if($row['is_any_number']==0){?>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>appointments/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                      <?php }?>

                        <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>appointments/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                    </div>
                </div>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
