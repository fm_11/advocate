<script type="text/javascript">
function getMobileByClientId(client_id){
   // alert(client_id);
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("mobile").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>appointments/ajax_mobile_by_client_id?client_id=" + client_id, true);
    xmlhttp.send();
}
</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>appointments/edit/<?php echo $appointments->id; ?>" class="cmxform" method="post">
	<fieldset>


    <div class="form-row">
       <div class="form-group col-md-12">
          <label><?php echo $this->lang->line('case_no'); ?></label><span class="required_label">*</span>
         <select  name="client_id" class="js-example-basic-single w-100" disabled>
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
           <?php foreach ($case_list as $row) { ?>
             <option value="<?php echo $row['id']; ?>"<?php if (isset($appointments->case_master_id)) {
              if ($row['id'] == $appointments->case_master_id) {
                echo 'selected';
              }
            } ?>><?php echo $row['name']; ?></option>
           <?php } ?>
         </select>
       </div>
       <input type="hidden" name="client_id" value="<?php echo $appointments->client_id;?>" />
       <input type="hidden" name="case_master_id" value="<?php echo $appointments->case_master_id;?>" />

      </div>

      <div class="form-row">
         <div class="form-group col-md-6">
             <label for="inputEmail4"><?php echo $this->lang->line('client_name')?><span class="required_label">*</span></label>
             <input type="text" autocomplete="off"  class="form-control" id="client"  name="client_name" value="<?php echo $appointments->first_name;?>" readonly>
         </div>
         <div class="form-group col-md-6">
           <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number');?><span class="required_label">*</span></label>
           <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile" value="<?php echo $appointments->mobile;?>" readonly>
         </div>
       </div>

      <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('date')?><span class="required_label">*</span></label>
            <div class="input-group date">
              <input type="text" autocomplete="off"  class="form-control" id="date"  name="date" required value="<?php echo date("d-m-Y", strtotime($appointments->date)); ?>">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
            </div>
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('time')?><span class="required_label">*</span></label>
          <input type="time" autocomplete="off"  class="form-control" id="time"  name="time" value="<?php echo $appointments->time; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('sms'); ?></label>
          <br> <br>
           <input type="radio"  name="is_sms" value="1" <?php if($appointments->is_sms==1){echo "checked";}?>><?php echo $this->lang->line('yes'); ?>
           <input type="radio"  name="is_sms" value="0" <?php if($appointments->is_sms==0){echo "checked";}?>> <?php echo $this->lang->line('no'); ?>
        </div>
      </div>
      <div class="form-row">
         <div class="form-group col-md-12">
           <label for="inputEmail4"><?php echo $this->lang->line('notes'); ?></label>
             <input type="notes" autocomplete="off"  class="form-control" id="notes"  name="notes" value="<?php echo $appointments->notes; ?>">
           <!-- <textarea class="form-control" id="notes" autocomplete="off" name="notes" value="<?php echo $appointments->notes; ?>"></textarea> -->
         </div>
       </div>

  </fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $appointments->id; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
