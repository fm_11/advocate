<script type="text/javascript">
function getMobileByClientId(client_id){
   // alert(client_id);
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("mobile").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>appointments/ajax_mobile_by_client_id?client_id=" + client_id, true);
    xmlhttp.send();
}
function ajax_get_client_info_by_case_id(case_id){
    //alert(case_id);
    document.getElementById("div_case_history").innerHTML = "";
    document.getElementById("div_any_number").style.display="none";
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("is_any_number").checked  = false;
            document.getElementById("div_case_history").innerHTML = xmlhttp.responseText;
            basic_function_load();
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>appointments/ajax_get_client_info_by_case_id?case_master_id=" + case_id, true);
    xmlhttp.send();
}
function basic_function_load() {
  $(".date").datepicker({
		autoclose: true,
		todayHighlight: true,
		format: 'dd-mm-yyyy',
	}).datepicker();
  	$(".js-example-basic-single").select2();

}
function get_any_number(chk)
{

  if(chk)
  {
    document.getElementById("div_any_number").style.display="block";
    document.getElementById("mobile").required = true;
    document.getElementById("notes").required = true;
    document.getElementById("date").required = true;
    document.getElementById("time").required = true;
    document.getElementById("case_master_id").required = false;
    document.getElementById("div_case_history").innerHTML = "";

  }else{
    document.getElementById("div_any_number").style.display="none";
    document.getElementById("mobile").required = false;
    document.getElementById("notes").required = false;
    document.getElementById("date").required = false;
    document.getElementById("time").required = false;
    document.getElementById("case_master_id").required = true;
  }

}
</script>


<div class="form-row">
  <div class="form-group col-md-6">
    <label><?php echo $this->lang->line('case_no'); ?></label><span class="required_label">*</span>
    <select  onchange="ajax_get_client_info_by_case_id(this.value)"  name="case_master_id" id="case_master_id" class="js-example-basic-single w-100">
      <option value="0">-- <?php echo $this->lang->line('please_select'); ?> --</option>
      <?php foreach ($case_list as $row) { ?>
        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group col-md-6" style="padding-left: 50px;">
                               <br><br>
                             <label class="form-check-label">
                              <input type="checkbox"  id="is_any_number" class="form-check-input" onclick="get_any_number(this.checked);">
                              <label><?php echo $this->lang->line('is_any_number'); ?></label>
                            <i class="input-helper"></i></label>
  </div>
</div>


<div id="div_case_history">


</div>
<div id="div_any_number" style="display:none;">
   <form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>appointments/add" class="cmxform" method="post">
     <input type="hidden" name="is_any_number" id="chk_is_any_number" value="1"/>
     <div class="form-row">
       <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('date')?><span class="required_label">*</span></label>
           <div class="input-group date">
             <input type="text" autocomplete="off"  class="form-control" id="date"  name="date" value="">
             <span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
           </div>
       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('time')?><span class="required_label">*</span></label>
         <input type="time" autocomplete="off"  class="form-control" id="time"  name="time"  value="">

       </div>
       <div class="form-group col-md-4">
         <label for="inputEmail4"><?php echo $this->lang->line('sms'); ?></label>
         <br> <br>
          <input type="radio"  name="is_sms" value="1" checked><?php echo $this->lang->line('yes'); ?>
          <input type="radio"  name="is_sms" value="0" > <?php echo $this->lang->line('no'); ?>
       </div>
     </div>
  <div class="form-row">

     <div class="form-group col-md-4">
       <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number');?><span class="required_label">*</span></label>
       <input type="number" autocomplete="off"  class="form-control" id="mobile"  name="mobile" value="" minlength="11">
     </div>
     <div class="form-group col-md-8">
         <label for="inputEmail4"><?php echo $this->lang->line('write_message')?><span class="required_label">*</span></label>
        <textarea class="form-control" id="notes" autocomplete="off" name="notes" >(<?php echo $setting->name;?>) ধন্যবাদ।</textarea>
     </div>
   </div>
   <div class="float-right">
     <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
      <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
   </div>
   </form>
</div>
