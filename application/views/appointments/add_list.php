
 <form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>appointments/add" class="cmxform" method="post">
  <fieldset>
     <!-- <div class="form-row">
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('clients').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
          <select onchange="getMobileByClientId(this.value)" name="client_id" class="js-example-basic-single w-100" required="required">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php foreach ($clients as $row) { ?>
              <option value="<?php echo $row['id']; ?>"><?php echo $row['first_name']; ?><?php echo" ";?><?php echo $row['last_name']; ?></option>
            <?php } ?>
          </select>
          </span>
        </div>
       </div> -->
       <input type="hidden" name="client_id" value="<?php echo $contact_info[0]['client_id'];?>" />
       <input type="hidden" name="case_master_id" value="<?php echo $case_master_id;?>" />
       <div class="form-row">
          <div class="form-group col-md-6">
              <label for="inputEmail4"><?php echo $this->lang->line('client_name')?><span class="required_label">*</span></label>
              <input type="text" autocomplete="off"  class="form-control" id="client"  name="client_name" value="<?php echo $contact_info[0]['first_name'];?>" readonly>
          </div>
          <div class="form-group col-md-6">
            <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number');?><span class="required_label">*</span></label>
            <input type="text" autocomplete="off"  class="form-control" id="mobile"  name="mobile" value="<?php echo $contact_info[0]['mobile'];?>" readonly>
          </div>
        </div>

      <div class="form-row">

         <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('date')?><span class="required_label">*</span></label>
             <div class="input-group date">
               <input type="text" autocomplete="off"  class="form-control" id="date"  name="date" required value="">
               <span class="input-group-text input-group-append input-group-addon">
                   <i class="simple-icon-calendar"></i>
               </span>
             </div>
         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('time')?><span class="required_label">*</span></label>
           <input type="time" autocomplete="off"  class="form-control" id="time"  name="time" required value="">

         </div>
         <div class="form-group col-md-4">
           <label for="inputEmail4"><?php echo $this->lang->line('sms'); ?></label>
           <br> <br>
            <input type="radio"  name="is_sms" value="1" ><?php echo $this->lang->line('yes'); ?>
            <input type="radio"  name="is_sms" value="0" checked> <?php echo $this->lang->line('no'); ?>
         </div>
       </div>

         <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4"><?php echo $this->lang->line('write_message'); ?><span class="required_label">*</span></label>
              <textarea class="form-control" id="notes" autocomplete="off" name="notes" required>(<?php echo $setting->name;?>) ধন্যবাদ।</textarea>
            </div>
          </div>
  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
