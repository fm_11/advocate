<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>services/edit/<?php echo $row->id; ?>" class="cmxform" method="post">
	<fieldset>
		<div class="form-group col-md-12">
			<label for="inputEmail4"><?php echo $this->lang->line('name'); ?><span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="<?php echo $row->name; ?>">
		</div>
		<div class="form-group col-md-12">
			<label for="inputEmail4"><?php echo $this->lang->line('amount'); ?><span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" id="amount" required name="amount" value="<?php echo $row->amount; ?>">
		</div>
		<div class="form-group col-md-12">
			<label for="inputEmail4"><?php echo $this->lang->line('is_active'); ?> ?</label>
			<input name="is_active" <?php if ($row->is_active == '1') {
							echo 'checked';
					} ?> type="checkbox" value="1">

		</div>

	</fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row->id; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
