<script type="text/javascript">
  function getDivisionByCountryId(country_id){
    //  alert(country_id);
      document.getElementById("division_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
      document.getElementById("district_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
      document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
      if (window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else
      {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function()
      {
          if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
          {
              document.getElementById("division_id").innerHTML = xmlhttp.responseText;
          }
      }
      xmlhttp.open("GET", "<?php echo base_url(); ?>client_user/ajax_division_by_country?country_id=" + country_id, true);
      xmlhttp.send();
  }
    function getDistrictByDivision(division_id){
      //  alert(country_id);
        document.getElementById("district_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("district_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>client_user/ajax_district_by_division?division_id=" + division_id, true);
        xmlhttp.send();
    }
    function getUpazilasByDistrictId(district_id){
      //  alert(country_id);
        document.getElementById("upazilas_id").innerHTML = "<option>--<?php echo $this->lang->line('please_select'); ?>--</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("upazilas_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>client_user/ajax_upazilas_by_district?district_id=" + district_id, true);
        xmlhttp.send();
    }
    function from_validation()
    {
      var phone=	document.getElementById("mobile").value;
      if (/^\d{11}$/.test(phone)) {

      } else {
          var language=	document.getElementById("language").value;
          if(language=='english')
          {
             alert('Phone number is incorrect! The number must be 11 digits.');
          }else{
            alert('ফোন নম্বরটি সঠিক নয় ! সংখ্যাটি 11 ডিজিটের হতে হবে ।');
          }

          return false;
      }
        return true;
    }
</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>client_user/edit/<?php echo $row_data->id; ?>" enctype="multipart/form-data" class="cmxform" method="post" onSubmit="return from_validation()">
<input type="hidden" id="language" value="<?php echo $language;?>"/>
  <fieldset>
    <div class="form-row">
          <?php if(!empty($row_data->photo)){?>
         <div class="form-group col-md-12" style="text-align:center;">
            <img style="width: 25%;border-radius: 50%;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/member/<?php echo $row_data->photo;?>" />
         </div>
        <br>   <br>
        <?php }?>
  </div>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="first_name"  name="first_name" required value="<?php echo $row_data->first_name; ?>">
        </div>
        <!-- <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('last').' '.$this->lang->line('name'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="last_name"  name="last_name" value="<?php echo $row_data->last_name; ?>">
        </div> -->
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('no'); ?><span class="required_label">*</span></label>
          <input type="number" autocomplete="off"  class="form-control" id="mobile"  name="mobile" required value="<?php echo $row_data->mobile; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('email'); ?></label>
          <input type="email" autocomplete="off"  class="form-control" id="email"  name="email" value="<?php echo $row_data->email; ?>">
        </div>
      </div>
      <div class="form-row">

        <!-- <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('zip_code'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="zip_code"  name="zip_code" value="<?php echo $row_data->zip_code; ?>">
        </div> -->



       </div>

        <div class="form-row">
           <!-- <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('country'); ?><span class="required_label">*</span></label>
             <select onchange="getDivisionByCountryId(this.value)" name="country_id" class="js-example-basic-single w-100" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($countries as $row) { ?>
                 <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->country_id)) {
                  if ($row['id'] == $row_data->country_id) {
                    echo 'selected';
                  }
                } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div> -->
           <input type="hidden" name="country_id" value="<?php echo $row_data->country_id;?>"/>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('division'); ?><span class="required_label">*</span></label>
             <select onchange="getDistrictByDivision(this.value)"  name="division_id" class="js-example-basic-single w-100" id="division_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($divisions as $row) { ?>
                 <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->division_id)) {
                 if ($row['id'] == $row_data->division_id) {
                   echo 'selected';
                 }
               } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('district'); ?><span class="required_label">*</span></label>
             <select onchange="getUpazilasByDistrictId(this.value)"  name="district_id" class="js-example-basic-single w-100" id="district_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($districts as $row) { ?>
                 <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->district_id)) {
                 if ($row['id'] == $row_data->district_id) {
                   echo 'selected';
                 }
               } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
           <div class="form-group col-md-4">
             <label for="inputEmail4"><?php echo $this->lang->line('upazila'); ?><span class="required_label">*</span></label>
             <select name="upazilas_id" class="js-example-basic-single w-100" id="upazilas_id" required="required">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php foreach ($upazilas as $row) { ?>
                 <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->upazilas_id)) {
                  if ($row['id'] == $row_data->upazilas_id) {
                    echo 'selected';
                  }
                } ?>><?php echo $row['name']; ?></option>
               <?php } ?>
             </select>
           </div>
         </div>
         <div class="form-row">
           <div class="form-group col-md-8">
             <label for="inputEmail4"><?php echo $this->lang->line('address'); ?><span class="required_label">*</span></label>
             <textarea class="form-control" id="address" autocomplete="off" name="address" required value=""><?php echo $row_data->address; ?></textarea>

           </div>
          <div class="form-group col-md-4">
            <label for="inputEmail4"><?php echo $this->lang->line('role'); ?> <span class="required_label">*</span></label>
            <select name="role_id" class="js-example-basic-single w-100" id="role_id" required="required">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php foreach ($roles as $row) { ?>
                <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->role_id)) {
                 if ($row['id'] == $row_data->role_id) {
                   echo 'selected';
                 }
               } ?>><?php echo $row['name']; ?></option>
              <?php } ?>
            </select>
          </div>
          

         </div>

   <div class="form-row">

    <div class="form-group col-md-4">
      <label for="inputEmail4"><?php echo $this->lang->line('password'); ?><span class="required_label">*</span></label>
      <input type="password" autocomplete="off"  class="form-control"  required name="password" value="<?php echo $row_data->password; ?>">
    </div>
    <div class="form-group col-md-4">
        <input type="hidden" name="photo_img_name" value="<?php echo $row_data->photo; ?>" />
      <label for="inputEmail4"><?php echo $this->lang->line('photo'); ?></label>
      <!-- <input type="file" id="logo" name="logo" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff"> -->
      <input type="file" autocomplete="off"  class="form-control" id="photo"  name="photo" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
    </div>
  </div>

  </fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
