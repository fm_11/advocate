<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $school_info->school_name; ?> - EXPENSE VIEW
        No. <?php echo $collection_sheet_info[0]['receipt_no']; ?></title>
    <link href="<?php echo base_url() . MEDIA_FOLDER; ?>/receipt_style/receipt.css" rel="stylesheet">

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>
<body>
<table width="73%" cellpadding="0" cellspacing="0" id="box-table-a">
    <tr>
        <th width="100%" style="text-align:right" scope="col">
            <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
        </th>
    </tr>
</table>
<br>
<div class="wrapper" id="printableArea">
    <table class="header">
        <tbody>
        <tr>
            <td nowrap="nowrap" style="text-align:center;" colspan="2" width="100%">
            <table width="100%">
                  <tr>
                      <td width="30%">
                      <p><img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info->picture; ?>"></p>
                      </td>

                      <td>
                        <strong>Pay To</strong><br>
                    <b><?php echo $school_info->school_name; ?></b><br>
                    EIIN No. <?php echo $school_info->eiin_number; ?><br>
                    <?php echo $school_info->address; ?><br>
                    <?php echo $school_info->web_address; ?>
                      </td>
                  </tr>
            </table>

            </td>
        </tr>

         <tr>
            <td nowrap="nowrap" width="50%">
				<span class="title">Voucher No. #<?php echo $deposit_info[0]['income_voucher_id']; ?></span><br>
				Voucher Date: <?php echo date("d/m/Y", strtotime($deposit_info[0]['date'])); ?>
            </td>
            <td width="50%" align="center">
                  <font class="paid">Income Voucher</font><br>
                  <span style="color:green;">Cost Center: <?php echo $deposit_info[0]['cost_center']; ?></span><br>
                  <span style="color:red;">Confidential</span>
            </td>
        </tr>
        </tbody>
    </table>




	<table class="items">

        <tbody>
        <tr class="title textcenter">
            <td width="20%">Category</td>
            <td width="30%">Description</td>
            <td width="20%">Payment Method</td>
            <td width="30%">Amount</td>
        </tr>

        <?php
        $total = 0;
        if (isset($deposit_details_info)) {
            ?>

            <?php
            foreach ($deposit_details_info as $row):
                ?>

                <tr>
                    <td align="center"><?php echo $row['deposit_category_name']; ?></td>
                    <td align="center"><?php echo $row['income_description']; ?></td>
                    <td align="center"><?php echo $row['deposit_method_name']; ?></td>
                    <td class="textcenter">
                        <?php
                        $total += $row['amount']; ?>
                        ৳<?php echo number_format($row['amount'], 2); ?>BDT
                    </td>
                </tr>

            <?php endforeach; ?>


        <?php
        }
        ?>



        <tr class="title">
            <td class="textright" colspan="3">Total:</td>
            <td class="textcenter">৳<?php echo number_format($total, 2); ?>BDT</td>
        </tr>

        <tr>
            <td class="textleft" colspan="4">&nbsp;<b>In Words: <?php echo $this->numbertowords->convert_number($total); ?> Taka Only.</b></td>
        </tr>


            <tr>
                <td colspan="4">
                    <br> <br>
                    ...............................................<br>
                   <b>Accountant</b>
                </td>
            </tr>

        </tbody>
    </table>
    Powered by: School360
</div>
</body>
</html>
