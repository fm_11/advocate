<?php
$from_date = $this->session->userdata('from_date');
$to_date = $this->session->userdata('to_date');
?>

<form class="form-inline" method="post" action="<?php echo base_url(); ?>income_deposits/index">
<div class="col-md-offset-2 col-md-12">

        <label class="sr-only" for="FromDate">From Date</label>
        <?php
         $placeholder = 'yyyy-mm-dd';
        ?>
        <input type="text" autocomplete="off"  style="max-width:150px;height: 37px;margin-top: -4px;"
         name="from_date" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($from_date)) {
            echo $from_date;
        } ?>" class="form-control" id="from_date">

        <label class="sr-only" for="ToDate">To Date</label>
        <?php
         $placeholder = 'yyyy-mm-dd';
        ?>
        <input type="text" autocomplete="off"  style="max-width:150px;height: 37px;margin-top: -4px;"
         name="to_date" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($to_date)) {
            echo $to_date;
        } ?>" class="form-control" id="to_date">

        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
      <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
            <th scope="col"><?php echo $this->lang->line('date'); ?></th>
            <th scope="col"><?php echo $this->lang->line('voucher_no'); ?></th>
            <th scope="col"><?php echo $this->lang->line('cost_center'); ?></th>
            <th scope="col"><?php echo $this->lang->line('amount'); ?></th>
            <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = (int)$this->uri->segment(3);
        foreach ($deposits as $row):
            $i++;
            ?>
            <tr>
                <td>
                    <?php echo $i; ?>
                </td>
                <td><?php echo $row['date']; ?></td>
                <td><?php echo $row['income_voucher_id']; ?></td>
                <td><?php echo $row['cost_center']; ?></td>
                <td><?php echo $row['income_total_amount']; ?></td>
                <td>
                     <div class="dropdown">
                         <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="ti-pencil-alt"></i>
                         </button>
                         <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                             <a class="dropdown-item" target="_blank" href="<?php echo base_url(); ?>income_deposits/deposit_details_view/<?php echo $row['id']; ?>"><?php echo $this->lang->line('view'); ?></a>
                             <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>income_deposits/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                         </div>
                     </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    <div class="float-right">
    <?php echo $this->pagination->create_links(); ?>
    </div>
</div>
