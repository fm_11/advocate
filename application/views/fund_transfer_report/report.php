<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
}
table {
  border-collapse: collapse;
}
</style>

<?php
 } ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="5" style="text-align:center;">
            <b><?php echo $HeaderInfo['school_name']; ?><br>
            <?php if ($HeaderInfo['eiin_number'] != '') {
     echo "EIIN: " . $HeaderInfo['eiin_number'] . "<br>";
 } ?>
            <?php echo $this->lang->line('income'); ?> <?php echo $this->lang->line('report'); ?> (<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)
           </b>
        </td>
    </tr>

    <tr>
        <td scope="col"><?php echo $this->lang->line('sl'); ?></td>
        <td scope="col">Date</td>
        <td scope="col">From</td>
        <td scope="col">To</td>
        <td scope="col">User</td>
        <td scope="col"><?php echo $this->lang->line('amount'); ?></td>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td>
                <?php echo $row['date']; ?>
            </td>
            <td><?php echo $row['from_asset_head']; ?></td>
            <td><?php echo $row['to_asset_head']; ?></td>
            <td><?php echo $row['user_name']; ?></td>
            <td>
              <?php
              echo $row['amount'];
              $total_amount+= $row['amount'];
              ?>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>
</div>
