

        <div class="table-sorter-wrapper col-lg-12 table-responsive">
          <table id="sortable-table-1" class="table">
            <thead>
              <tr>
                  <th class="border-0 pt-0 pl-0">
                      <?php echo $this->lang->line('sl'); ?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo $this->lang->line('todays').' '.$this->lang->line('date'); ?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo $this->lang->line('case_no'); ?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo $this->lang->line('upazila'); ?>
                  </th>
                  <th class="border-0 pt-0">
                    <?php echo  $this->lang->line('court_details');?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo  $this->lang->line('defendant').' '.$this->lang->line('party');?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo  $this->lang->line('petitioner');?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo  $this->lang->line('reason_for_assignment');?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo  $this->lang->line('file_colour');?>
                  </th>
                  <th class="border-0 pt-0">
                      <?php echo  $this->lang->line('first_hearing_date');?>
                  </th>
                </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach ($today_attendace_list as $row): ?>
                <tr>
									<td class="pl-0">
											<?php echo $i;?>
									</td>
									<td>
											<?php echo $row['befor_date'];?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['case_sub_type_bname'].'-'.$row['case_no'];
										} else {
											echo $row['case_sub_type_name'].'-'.$row['case_no'];
										}
										?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['upazila_bnname'];
										} else {
											echo $row['upazila_name'];
										}
										?>

									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['court_bn_name'];
										} else {
											echo $row['court_name'];
										}
										?>

									</td>

									<td>
										<?php
										 if($row['is_petitioner']=='0')
										 {
											 if(empty($row['total_person']))
											 {
												 echo $row['parties_name'];
											 }else{
												 echo $row['parties_name'].'('.$row['total_person'].')';
											 }

										 }
									?>

									</td>
										<td>
											<?php
											if($row['is_petitioner']=='1')
											{
												if(empty($row['total_person']))
												{
													echo $row['parties_name'];
												}else{
													echo $row['parties_name'].'('.$row['total_person'].')';
												}

											}
												?>
										</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['status_bn_name'];
										} else {
											echo $row['status_name'];
										}
										?>
									</td>
									<td>
										<?php
										if ($this->session->userdata('site_lang') == 'bangla') {
											echo $row['bn_file_colour'];
										} else {
											echo $row['file_colour'];
										}
										?>
									</td>
									<td>
										<?php echo $row['after_date'];?>
									</td>
								</tr>
                <?php $i++;?>
              <?php endforeach; ?>


            </tbody>
          </table>
        </div>
