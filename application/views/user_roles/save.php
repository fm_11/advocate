<?php echo form_open("user_roles/" . $action); ?>
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Role Name</label>
			<input value="<?php echo isset($row->role_name) ? $row->role_name : ''; ?>"
				   class="form-control" type="text" required="1" name="role_name">
		</div>
		<div class="form-group col-md-6">
			<label>Description</label>
			<input class="form-control"
				   value="<?php echo isset($row->role_description) ? $row->role_description : ''; ?>"
				   type="text" required="1" name="role_description">
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
<?php echo form_close(); ?>
