<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
      <thead>
      <tr>
          <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
          <th scope="col"><?php echo $this->lang->line('name'); ?></th>
          <th scope="col"><?php echo $this->lang->line('description'); ?></th>
          <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
      </tr>
      </thead>
      <tbody>
      <?php
      $i = 0;
      foreach ($user_roles as $row):
          $i++;
          ?>
          <tr>
              <td>
                  <?php echo $i; ?>
              </td>
              <td><?php echo $row->role_name; ?></td>
              <td><?php echo $row->role_description; ?></td>
              <td>
                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                      <a class="dropdown-item" href="<?php echo base_url(); ?>user_role_wise_privileges/index/<?php echo $row->id; ?>"><?php echo $this->lang->line('set_permission'); ?></a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>user_roles/edit/<?php echo $row->id; ?>"><?php echo $this->lang->line('edit'); ?></a>
                        <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>user_roles/delete/<?php echo $row->id; ?>"><?php echo $this->lang->line('delete'); ?></a>
                    </div>
                </div>
              </td>
          </tr>
      <?php endforeach; ?>
      </tbody>
  </table>
</div>
