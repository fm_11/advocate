<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>ad_case_status/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-12">
      <label><?php echo $this->lang->line('bangla').' '.$this->lang->line('name'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="bn_name" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
