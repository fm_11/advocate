<script type="text/javascript">
    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>ad_case_status/updateCaseStatus?id=" + id + '&&is_active=' + status, true);
        xmlhttp.send();
    }
</script>

<?php
$name = $this->session->userdata('name');
$status = $this->session->userdata('status');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>ad_case_status/index">
<div class="col-md-offset-2 col-md-12">
        <label class="sr-only" for="Name"><?php echo $this->lang->line('name'); ?></label>
        <?php
         $placeholder = $this->lang->line('case_status');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($roll)) {
            echo $name;
        } ?>" class="form-control" id="name">

        <label class="sr-only" for="Status"><?php echo $this->lang->line('status'); ?></label>
        <select name="status" style="max-width: 150px;"  class="form-control mb-1 mr-sm-1">
            <option value="">-- <?php echo $this->lang->line('status'); ?> --</option>
            <option value="1" <?php if (isset($status)) {
            if ($status == '1') {
                echo 'selected';
            }
        } ?>>Active
            </option>
            <option value="0" <?php if (isset($status) && $status != '') {
            if ($status == '0') {
                echo 'selected';
            }
        } ?>>Inactive
            </option>
        </select>
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?></th>
        <th scope="col"><?php echo $this->lang->line('bangla').' '.$this->lang->line('name'); ?></th>
        <th scope="col"><?php echo $this->lang->line('status'); ?></th>
        <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
      $i = (int)$this->uri->segment(3);
    foreach ($case_status as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['bn_name']; ?></td>
            <td id="status_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['is_active'] == 1) {
                    ?>
                    <a class="deleteTag" title="Active" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)"><i class="ti-check-box"></i></a>
                <?php
                } else {
                    ?>
                    <a class="deleteTag" title="Inactive" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_active']; ?>)"><i class="ti-na"></i></a>
                <?php
                }
                ?>
            </td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>ad_case_status/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>ad_case_status/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
