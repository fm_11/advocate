
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>file_colours/add" class="cmxform" method="post">
  <fieldset>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="">
        </div>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('bn_name'); ?><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="bn_name" required name="bn_name" value="">
        </div>

  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
