
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>ad_court_types/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-8">
      <label><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $court_type[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('serial_no'); ?><span style="color:red">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="order_no" value="<?php echo $court_type[0]['order_no']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-8">
      <label><?php echo $this->lang->line('bn_name'); ?><span style="color:red">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="bn_name" value="<?php echo $court_type[0]['bn_name']; ?>" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $court_type[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
