
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr class="school360HeadingRow">
			<th class="school360HeadingColumn" scope="col"><?php echo $this->lang->line('sl'); ?></th>
			<th class="school360HeadingColumn" scope="col"><?php echo $this->lang->line('date'); ?></th>
			<th class="school360HeadingColumn" scope="col">Stakeholder</th>
			<th class="school360HeadingColumn" scope="col">Unit Price</th>
			<th class="school360HeadingColumn" scope="col">Quantity</th>
			<th class="school360HeadingColumn" scope="col">Total Price</th>
			<th class="school360HeadingColumn" scope="col">Transaction ID</th>
			<th class="school360HeadingColumn" scope="col">Payment Option</th>
			<th class="school360HeadingColumn" scope="col">Status</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($purchase_list as $row):
			$i++;
			?>
			<tr <?php if($i % 2 == 0){ echo 'class="school360CustomRow"'; } ?>>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['date']; ?></td>
				<td><?php echo $row['stakeholder']; ?></td>
				<td><?php echo $row['unit_price']; ?></td>
				<td><?php echo $row['quantity']; ?></td>
				<td><?php echo $row['total_price']; ?></td>
				<td><?php echo $row['transaction_id']; ?></td>
				<td><?php echo strtoupper($row['payment_option']); ?></td>
				<td>
					<?php
					if ($row['payment_status'] == 'C'){
						echo 'CANCEL';
					}else if($row['payment_status'] == 'S'){
						echo 'SUCCESS';
					}else if($row['payment_status'] == 'F'){
						echo 'FAILED';
					}else{
						echo 'PENDING';
					}
					?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<br>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
