<script>
	function calculateTotalPrice(){
		var quantity = document.getElementById("quantity").value;
		var unit_price = document.getElementById("unit_price").value;
		document.getElementById("total_price").value = Math.round(Number(quantity) * Number(unit_price));
	}

	function changeSMSRate(sms_type){
		if(sms_type == 'NM'){
			document.getElementById("unit_price").value = '0.30';
		}else{
			document.getElementById("unit_price").value = '0.55';
		}
		calculateTotalPrice();
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>sms_purchases/add" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Date</label>
			<input type="text" value="<?php echo date('Y-m-d'); ?>" readonly autocomplete="off"  class="form-control" name="date" required="1"/>
		</div>
		<div class="form-group col-md-4">
			<label>SMS Type</label>
			<input type="text" readonly autocomplete="off" value="<?php echo $message_type; ?>"  class="form-control"/>
			<input type="hidden" name="stakeholder" id="stakeholder" autocomplete="off" value="<?php echo $msg_config->stakeholder; ?>"  class="form-control"/>
		</div>
		<div class="form-group col-md-4">
			<label>SMS Rate</label>
			<input type="text" readonly name="unit_price" id="unit_price" autocomplete="off" value="<?php echo $msg_config->sms_rate; ?>"  class="form-control"/>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Quantity <span class="required_label">*</span></label>
			<input type="number" min="100" name="quantity" onkeyup="calculateTotalPrice()" id="quantity" autocomplete="off" required min="1" value=""  class="form-control"/>
		</div>
		<div class="form-group col-md-4">
			<label>Total Price</label>
			<input type="text" readonly name="total_price" id="total_price" autocomplete="off" class="form-control"/>
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="Purchase">
	</div>
</form>
