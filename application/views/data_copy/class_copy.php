<script>
	function checkCheckBox() {
		var inputElems = document.getElementsByTagName("input"),
				count = 0;
		for (var i = 0; i < inputElems.length; i++) {
			if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
				count++;
			}
		}
		if (count < 1) {
			alert("Please select some class");
			return false;
		} else {
			return true;
		}
	}

	function checkItemAll(ele) {
		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox' && checkboxes[i].disabled ==  false) {
					checkboxes[i].checked = true;
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				console.log(i)
				if (checkboxes[i].type == 'checkbox' && checkboxes[i].disabled ==  false) {
					checkboxes[i].checked = false;
				}
			}
		}
	}
</script>
<style>

</style>

<form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>data_copy/class_copy#ui-basic" method="post">

<!-- <div class="row">
	<div class="col-md-1 p-2 bg-primary border text-center">
		<input type="checkbox" onclick="checkItemAll(this)" class="form-check-input">
	</div>
	<div class="col-md-2 p-2 bg-primary border text-center text-white">Name (English)</div>
	<div class="col-md-2 p-2 bg-primary border text-center text-white">Name (Bangla)</div>
	<div class="col-md-3 p-2 bg-primary border text-center text-white">ID Short Form</div>
</div>

	<?php
foreach ($classes as $key=>$value){
	$disabled = "";
	if(isset($class_list[trim(strtolower($value['name_english']))])){
		$disabled = "disabled";
	}
?>


<div class="form-inline">
	<div class="form-group">
		<div class="form-check form-check-success">
			<label class="form-check-label">
				<input <?php echo $disabled; ?> type="checkbox" name="selected_item_<?php echo $key; ?>" value="<?php echo $key; ?>"
					   class="form-check-input" <?php if($disabled == ''){ echo 'checked="1"'; } ?>>
				<i class="input-helper"></i>
			</label>
		</div>
		<input style="padding:8px;" name="name_english_<?php echo $key; ?>"  type="text" <?php echo $disabled; ?> value="<?php echo $value['name_english']; ?>" class="form-control" /> &nbsp;
		<input style="padding:8px;" name="name_bangla_<?php echo $key; ?>"  type="text" <?php echo $disabled; ?> value="<?php echo $value['name_bangla']; ?>" class="form-control" />&nbsp;
		<input style="padding:8px;" name="short_form_<?php echo $key; ?>"  type="text" <?php echo $disabled; ?> value="<?php echo $value['short_form']; ?>" class="form-control" />
	</div>
</div>


<?php } ?>

<div class="float-right">
	<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div> -->


	<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Solid header accordion</h4>
                  <p class="card-description">Use class <code>.accordion-solid-header</code> for basic accordion</p>
                  <div class="mt-4">
                    <div class="accordion accordion-solid-header" id="accordion-4" role="tablist">
                      <div class="card">
                        <div class="card-header" role="tab" id="heading-10">
                          <h6 class="mb-0">
                            <a data-toggle="collapse" href="#collapse-10" aria-expanded="true" aria-controls="collapse-10">
                              How can I pay for an order I placed?
                            </a>
                          </h6>
                        </div>
                        <div id="collapse-10" class="collapse show" role="tabpanel" aria-labelledby="heading-10" data-parent="#accordion-4">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-3">
                                <img src="../../../../images/samples/300x300/10.jpg" class="mw-100" alt="image"/>
                              </div>
                              <div class="col-9">
                                <p class="mb-0">You can pay for the product you have purchased using credit cards, debit cards, or via online banking.
                                We also on-delivery services.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" role="tab" id="heading-11">
                          <h6 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
                              I can’t sign in to my account
                            </a>
                          </h6>
                        </div>
                        <div id="collapse-11" class="collapse" role="tabpanel" aria-labelledby="heading-11" data-parent="#accordion-4">
                          <div class="card-body">
                              <p>If while signing in to your account you see an error message, you can do the following</p>
                            <ol class="pl-3 mt-4">
                              <li>Check your network connection and try again</li>
                              <li>Make sure your account credentials are correct while signing in</li>
                              <li>Check whether your account is accessible in your region</li>
                            </ol>
                            <br>
                            <p class="text-success">
                              <i class="ion ion-md-alert-octagon mr-2"></i>If the problem persists, you can contact our support.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" role="tab" id="heading-12">
                          <h6 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">
                              Can I add money to the wallet?
                            </a>
                          </h6>
                        </div>
                        <div id="collapse-12" class="collapse" role="tabpanel" aria-labelledby="heading-12" data-parent="#accordion-4">
                          <div class="card-body">
                            <p class="mb-0">You can add money to the wallet for any future transaction from your bank account using net-banking, or credit/debit card transaction. The money in the wallet can be used for an easier and faster transaction.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


</form>
