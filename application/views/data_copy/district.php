<script>
	function checkCheckBox() {
		var inputElems = document.getElementsByTagName("input"),
				count = 0;
		for (var i = 0; i < inputElems.length; i++) {
			if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
				count++;
			}
		}
		if (count < 1) {
			alert("Please select atleast one court.");
			return false;
		} else {
			return true;
		}
	}

	function checkItemAll(ele) {
		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox' && checkboxes[i].disabled ==  false) {
					checkboxes[i].checked = true;
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				console.log(i)
				if (checkboxes[i].type == 'checkbox' && checkboxes[i].disabled ==  false) {
					checkboxes[i].checked = false;
				}
			}
		}
	}
</script>
<style>

</style>

<form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>data_copy/district#ui-basic" method="post">
	<!-- <div class="col-lg-12 col-xs-12 col-sm-6 grid-margin stretch-card"> -->
              <!-- <div class="card"> -->
                <!-- <div class="card-body"> -->
                  <h4 class="card-title"><?php echo $division_name;?></h4>

                  <div class="mt-4">
                    <div class="accordion accordion-solid-header" id="accordion-4" role="tablist">

											<?php $row=1; foreach ($district_list as $value): ?>

												<div class="card">
	                        <div class="card-header" role="tab" id="heading-<?php echo $row;?>">
	                          <h6 class="mb-0">
	                            <a class="collapsed" data-toggle="collapse" href="#collapse-<?php echo $row;?>" aria-expanded="false" aria-controls="collapse-<?php echo $row;?>">
	                              <?php echo $value['name'];?>
	                            </a>
															<input type="hidden" name="district[<?php echo $row;?>][district_id]" value=" <?php echo $value['id'];?>">
															<input type="hidden" name="district[<?php echo $row;?>][division_id]" value=" <?php echo $value['division_id'];?>">
	                          </h6>
	                        </div>
	                        <div id="collapse-<?php echo $row;?>" class="collapse" role="tabpanel" aria-labelledby="heading-11" data-parent="#accordion-4">
	                          <div class="card-body" style="padding-left: 0px !important;">
															<div class="table-responsive">
															<table class="table table-hover">
															  <thead>
															    <tr>
															      <th class="text-success" scope="col">#</th>
															      <th class="text-success" scope="col">

																			<div class="form-check form-check-success">
																				<label class="form-check-label">
																					<input type="checkbox" onclick="checkItemAll(this)">
																				</label>
																			</div>
																		</th>
															      <th class="text-success" scope="col"><?php echo $this->lang->line('court_type'); ?></th>
															      <th class="text-success" scope="col"><?php echo $this->lang->line('court_sub_type'); ?></th>
															    </tr>
															  </thead>
															  <tbody>

																	<?php $i=0; foreach ($value['court_list'] as $key): ?>

																		<tr>
																	 	 <th scope="row"><?php echo $i+1; ?></th>
																	 	 <td>
																			 <div class="form-check form-check-success">
																				 <label class="form-check-label">
																					 <input  type="checkbox" name="district[<?php echo $row;?>][selected_court_<?php echo $i; ?>]" value="<?php echo $key['court_id']; ?>"
																								class="form-check-input" <?php if($key['checked']){ echo 'checked="1"'; } ?>>
																					 <i class="input-helper"></i>
																				 </label>
																			 </div>
																		 </td>
																	 	 <td><?php echo $key['court_type']; ?>
																			 <input  type="hidden" name="district[<?php echo $row;?>][selected_court_type_<?php echo $i; ?>]" value="<?php echo $key['court_type_id']; ?>">
																		 </td>
																	 	 <td><?php echo $key['court_name']; ?></td>
																	  </tr>
		                               <?php $i++; ?>
																	<?php endforeach; ?>
                               	<input type="hidden" name="district[<?php echo $row;?>][total_court]" value=" <?php echo $i;?>">
															  </tbody>
															</table>
															</div>
	                          </div>
	                        </div>
	                      </div>
                      <?php $row++; ?>
											<?php endforeach; ?>
                    </div>
                  </div>
                <!-- </div> -->
              <!-- </div> -->
            <!-- </div> -->

						<div class="float-right">
							<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
						</div>
</form>
