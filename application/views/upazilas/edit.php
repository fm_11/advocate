<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>upazilas/edit" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4"><?php echo $this->lang->line('district'); ?><span class="required_label">*</span></label>
      <select   name="case_type_id" class="js-example-basic-single w-100" required="required" disabled>
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php foreach ($district_list as $row) { ?>
          <option value="<?php echo $row['id']; ?>"<?php if (isset($upazila->district_id)) {
           if ($row['id'] == $upazila->district_id) {
             echo 'selected';
           }
         } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('serial_no'); ?><span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="serial_no" value="<?php echo $upazila->serial_no; ?>" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('upazila_name_english'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $upazila->name; ?>"/>
    </div>
    <div class="form-group col-md-6">

        <label><?php echo $this->lang->line('bn_name'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="bn_name" value="<?php echo $upazila->bn_name; ?>" />
    </div>
  </div>
    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $upazila->id; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
