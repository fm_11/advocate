<?php
$name = $this->session->userdata('name');
$district = $this->session->userdata('district_id');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>upazilas/index">

<div class="col-md-offset-2 col-md-12">
        <?php
         $placeholder = $this->lang->line('upazila_name_english');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($name)) {
            echo $name;
        } ?>" class="form-control" id="name">


        <select   name="district_id" class="form-control mb-1 mr-sm-1 " style="max-width: 250px;">
          <option value="">-- <?php echo $this->lang->line('district'); ?> --</option>
          <?php foreach ($district_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"<?php if (isset($district)) {
             if ($row['id'] == $district) {
               echo 'selected';
             }
           } ?>><?php echo $row['name']; ?></option>
          <?php } ?>
        </select>
        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>

</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('district_name'); ?></th>
        <th><?php echo $this->lang->line('english').' '.$this->lang->line('name'); ?></th>
        <th><?php echo $this->lang->line('bn_name'); ?></th>
        <th><?php echo $this->lang->line('serial_no'); ?></th>
        <th><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($upazilas as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td >
                <?php echo $i; ?>
            </td>
              <td><?php if($language=="bangla"){echo $row['dis_bnname'];}else{echo $row['dis_name'];}  ?>

              </td>
               <td><?php echo $row['name']; ?></td>
               <td><?php echo $row['bn_name']; ?></td>
              <td><?php echo $row['serial_no']; ?></td>
               <td>
                   <div class="dropdown">
                       <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <i class="ti-pencil-alt"></i>
                       </button>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                           <a class="dropdown-item" href="<?php echo base_url(); ?>upazilas/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                       </div>
                   </div>
               </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
