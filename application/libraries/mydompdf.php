<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');

class Mydompdf
{
	public function createPDF($html, $filename='', $download=true, $paper='A4', $orientation='landscape')
	{
		//echo $paper;die;
		$dompdf = new Dompdf\DOMPDF();

		$options = $dompdf->getOptions();
		$options->set('isRemoteEnabled', TRUE);
		$options->set('isPhpEnabled', TRUE);
		$context = stream_context_create([
			'ssl' => [
				'verify_peer' => FALSE,
				'verify_peer_name' => FALSE,
				'allow_self_signed'=> TRUE
			]
		]);
		$dompdf->setHttpContext($context);

		$dompdf->load_html($html);
		$dompdf->set_paper($paper, $orientation);
		$dompdf->render();
		$canvas = $dompdf->getCanvas();
		//$font = Font_Metrics::get_font("SolaimanLipi", "regular");
		date_default_timezone_set('Asia/Dhaka');
		$currentDateTime = date("Y/m/d H:i:s");
		$newDateTime = date('d-m-Y h:i A', strtotime($currentDateTime));


		$footer = $canvas->open_object();
		if($paper == 'A4'){
			if($orientation == 'landscape'){
				//$canvas->line(0,560,850,560,array(0,0,0),3);
				$canvas->page_text(40, 570, "Print Date: $newDateTime | Powered By: Spate Initiative Limited", '', 8, array(0,0,0));
				$canvas->page_text(760, 570, "Page: {PAGE_NUM} of {PAGE_COUNT}", '', 8, array(0,0,0));
			}else{
				//$canvas->line(0,780,560,780,array(0,0,0),3);
				$canvas->page_text(40, 800, "Print Date: $newDateTime | Powered By: Spate Initiative Limited", '', 8, array(0,0,0));
				$canvas->page_text(512, 800, "Page: {PAGE_NUM} of {PAGE_COUNT}", '', 8, array(0,0,0));
			}
		}
		else if($paper == 'legal'){
			if($orientation == 'landscape'){
				$canvas->page_text(40, 580, "Print Date: $newDateTime | Powered By: Spate Initiative Limited", '', 8, array(0,0,0));
				$canvas->page_text(925, 580, "Page: {PAGE_NUM} of {PAGE_COUNT}", '', 8, array(0,0,0));
			}else{
				$canvas->page_text(16, 800, "Print Date: $newDateTime | Powered By: Spate Initiative Limited", '', 8, array(0,0,0));
				$canvas->page_text(520, 800, "Page: {PAGE_NUM} of {PAGE_COUNT}", '', 8, array(0,0,0));
			}
		}

		$canvas->close_object();
		$canvas->add_object($footer, 'all');


		if ($download) {
			$dompdf->stream($filename.'.pdf', array('Attachment' => 1));
		} else {
			$dompdf->stream($filename.'.pdf', array('Attachment' => 0));
		}
	}
}
