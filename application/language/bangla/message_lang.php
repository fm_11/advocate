<?php
$lang['dashboard_title'] = "Advocate -> ড্যাশবোর্ড";
$lang['dashboard'] = "ড্যাশবোর্ড";
$lang['configuration'] = 'কনফিগারেশন';
$lang['settings'] = 'সেটিংস';
$lang['general'] = 'সাধারণ';
$lang['manage'] = 'পরিচালনা';
$lang['add_success_message'] = 'ডাটা সফলভাবে সংরক্ষণ করা হয়েছে';
$lang['edit_success_message'] = 'ডাটা সফলভাবে আপডেট করা হয়েছে';
$lang['delete_success_message'] = 'ডাটা সফলভাবে মোছা হয়েছে';
$lang['add_error_message'] = 'দুঃখিত ! ডাটা সংরক্ষণ করা যায়নি';
$lang['edit_error_message'] = 'দুঃখিত ! ডাটা সম্পাদন করা যায়নি';
$lang['delete_error_message'] = 'দুঃখিত ! ডাটা মোছা যায়নি';
$lang['spatei_title'] = "Your Dream is Ours";
$lang['spatei'] = "স্পেট ইনিশিয়েটিভ লিমিটেড";
$lang['dashboard_title_msg'] = "এক নজরে সবকিছু";
$lang['add_button'] = "নতুন যুক্ত করুন";
$lang['back_button'] = "পিছনে যান";
$lang['batch_add'] = "এক সাথে যুক্ত করুন";
$lang['language'] = "বাংলা";
$lang['password_change'] = "পাসওয়ার্ড পরিবর্তন";
$lang['change'] = "পরিবর্তন";
$lang['logout'] = "প্রস্থান করুন";
$lang['student_list'] = 'শিক্ষার্থীদের তালিকা';
$lang['student_add'] = 'নতুন ছাত্র যুক্ত করুন';
$lang['upload'] = 'আপলোড';
$lang['student_update'] = 'শিক্ষার্থীদের তথ্য সম্পাদনা করুন';
$lang['update'] = 'সম্পাদনা করুন';
$lang['global_config'] = 'গ্লোবাল কনফিগারেশন';
$lang['student_code_config'] = 'শিক্ষার্থীদের আইডি কনফিগারেশন';
$lang['class'] = 'শ্রেণী';
$lang['section'] = 'সেকশন';
$lang['group'] = 'গ্রুপ';
$lang['global_configuration'] = 'গ্লোবাল কনফিগারেশন';
$lang['name'] = 'নাম';
$lang['student_code'] = 'শিক্ষার্থীর আইডি';
$lang['process_code'] = 'প্রসেস কোড';
$lang['shift'] = 'শিফট';
$lang['roll'] = 'রোল';
$lang['reg_no'] = 'রেজিস্ট্রেশন নং';
$lang['date_of_birth'] = 'জন্ম তারিখ';
$lang['date_of_admission'] = 'ভর্তির তারিখ';
$lang['gender'] = 'লিঙ্গ';
$lang['religion'] = 'ধর্ম';
$lang['blood_group'] = 'রক্তের গ্রুপ';
$lang['email'] = 'ই-মেইল';
$lang['father_name'] = 'বাবার নাম';
$lang['mother_name'] = 'মায়ের নাম';
$lang['father_nid'] = 'বাবার জাতীয় পরিচয় নম্বর';
$lang['mother_nid'] = 'মায়ের জাতীয় পরিচয় নম্বর';
$lang['guardian_mobile'] = 'অভিভাবকের মোবাইল';
$lang['mobile'] = 'মোবাইল';
$lang['optional'] = 'ঐচ্ছিক';
$lang['student'] = 'শিক্ষার্থী';
$lang['students'] = 'শিক্ষার্থীদের';
$lang['year'] = 'বছর';
$lang['present_address'] = 'বর্তমান ঠিকানা';
$lang['permanent_address'] = 'স্থায়ী ঠিকানা';
$lang['photo'] = 'ছবি';
$lang['submit'] = 'সংরক্ষণ করুন';
$lang['cancel'] = 'বাতিল করুন';
$lang['search'] = 'অনুসন্ধান';
$lang['subject_list'] = 'বিষয় এর তালিকা';
$lang['subject'] = 'বিষয়';
$lang['sl'] = 'ক্রমিক নং';
$lang['status'] = 'অবস্থা';
$lang['or'] = 'অথবা';
$lang['multiple'] = 'এক সাথে';
$lang['special_care'] = 'স্পেশাল কেয়ার';
$lang['resident'] = 'আবাসিক';
$lang['non-resident'] = 'অনাবাসিক';
$lang['transfer'] = 'বদলি';
$lang['migrate'] = 'মাইগ্রেশন';
$lang['print'] = 'প্রিন্ট';
$lang['admit_card'] = 'প্রবেশপত্র';
$lang['seat_plan'] = 'আসন পরিকল্পনা';
$lang['id_card'] = 'পরিচয় পত্র';
$lang['exam'] = 'পরীক্ষা';
$lang['fees'] = 'বেতন';
$lang['sms'] = 'বার্তা';
$lang['send'] = 'পাঠান';
$lang['report'] = 'রিপোর্ট';
$lang['module'] = 'মডিউল';
$lang['other'] = 'অন্যান্য';
$lang['website'] = 'ওয়েবসাইট';
$lang['income'] = 'আয়';
$lang['expense'] = 'ব্যয়';
$lang['details'] = 'ডিটেলস';
$lang['category'] = 'ক্যাটাগরি';
$lang['add'] = 'যুক্ত করুন';
$lang['edit'] = 'সম্পাদন করুন';
$lang['delete'] = 'মুছে ফেলুন';
$lang['actions'] = 'অপশন';
$lang['remarks'] = 'মন্তব্য লিখুন';
$lang['receipt_no'] = 'রসিদ নং';
$lang['short_form'] = 'সংক্ষিপ্ত রূপ';
$lang['type'] = 'ধরণ';
$lang['wise'] = 'ভিত্তিক';
$lang['assign'] = 'বরাদ্দ';
$lang['please_select'] = 'অনুগ্রহ করে নির্বাচন করুন';
$lang['month'] = 'মাস';
$lang['year'] = 'বছর';
$lang['template'] = 'টেমপ্লেট';
$lang['message'] = 'বার্তা';
$lang['please'] = 'দয়া করে';
$lang['total'] = 'মোট';
$lang['blank'] = 'ফাঁকা';
$lang['days'] = 'দিন';
$lang['working'] = 'কার্য';
$lang['holidays'] = 'ছুটির দিন';
$lang['holiday'] = 'ছুটির দিন';
$lang['present'] = 'উপস্থিত';
$lang['leave'] = 'ছুটি';
$lang['amount'] = 'পরিমাণ';
$lang['absent'] = 'অনুপস্থিত';
$lang['attendance'] = 'হাজিরা';
$lang['body'] = 'বার্তাংশ';
$lang['cost'] = 'খরচ';
$lang['center'] = 'কেন্দ্র';
$lang['from_date'] = 'কত তারিখ থেকে';
$lang['to_date'] = 'কত তারিখ পর্যন্ত';
$lang['summary'] = 'সংক্ষিপ্ত';
$lang['for_month_of'] = 'মাসের জন্য';
$lang['for_year_of'] = 'বছরের জন্য';
$lang['index_no'] = 'ইনডেক্স নম্বর';
$lang['note'] = 'মন্তব্য';
$lang['allocate'] = 'বরাদ্দ';
$lang['allocated'] = 'বরাদ্দকৃত';
$lang['contact'] = 'যোগাযোগ';
$lang['phone_book'] = 'ফোন বুক';
$lang['sub'] = 'সাব';
$lang['deposited'] = 'জমাকৃত';
$lang['deposit'] = 'জমা';
$lang['file'] = 'ফাইল';
$lang['format'] = 'ফরমেট';
$lang['download'] = 'ডাউনলোড';
$lang['number'] = 'নম্বর';
$lang['monthly'] = 'মাসিক';
$lang['yearly'] = 'বাত্সরিক';
$lang['is_this'] = 'এটা কি';
$lang['admission'] = 'ভর্তি';
$lang['welcome'] = 'স্বাগত';
$lang['bangla'] = 'বাংলা';
$lang['english'] = 'ইংরেজি';
$lang['code'] = 'কোড';
$lang['credit'] = 'মোট নম্বর';
$lang['yes'] = 'হ্যাঁ';
$lang['no'] = 'না';
$lang['creative'] = 'সৃজনশীল';
$lang['objective'] = 'নৈর্ব্যক্তিক';
$lang['practical'] = 'ব্যবহারিক';
$lang['class_test'] = 'ক্লাস পরীক্ষা';
$lang['letter_grade'] = 'লেটার গ্রেড';
$lang['grade_point'] = 'গ্রেড পয়েন্ট';
$lang['grade'] = 'গ্রেড';
$lang['marks'] = 'মার্কস';
$lang['mark'] = 'মার্ক';
$lang['title'] = 'শিরনাম';//confused
$lang['tk'] = 'টাকা';
$lang['all'] = 'সকল';
$lang['obtain'] = 'অর্জন করা';//confused
$lang['eiin'] = 'EIIN';//confused
$lang['gpa'] = 'জি.পি.এ';
$lang['date'] = 'তারিখ';
$lang['in_words'] = 'কথায়';
$lang['position'] = 'অবস্থান';
$lang['with_optional'] = 'ওচ্ছিক সহ';
$lang['without_optional'] = 'ওচ্ছিক ছাড়া';
$lang['report_for'] = 'রিপোর্টের জন্য';//confused
$lang['na'] = 'প্রযোজ্য নয়';
$lang['collection'] = 'আদায়';
$lang['collect'] = 'আদায়';
$lang['new'] = 'নতুন';
$lang['its'] = "এটা";
$lang['is'] = 'Is';//confused
$lang['list'] = 'তালিকা';
$lang['process'] = 'প্রসেস করুন';
$lang['marking'] = 'মার্কিং';//confused
$lang['result'] = 'ফলাফল';
$lang['copy'] = 'কপি';
$lang['institute'] = 'প্রতিষ্ঠান';
$lang['payment'] = 'প্রদান';//confused
$lang['sheet'] = 'পাতা';//confused
$lang['percentage'] = 'শতকরা হার';
$lang['fine'] = 'জরিমানা';
$lang['paid'] = 'অর্থ প্রদান';//confused
$lang['priority'] = 'অগ্রাধিকার';
$lang['time'] = 'সময়';
$lang['num_of'] = 'সংখ্যা';
$lang['balance'] = 'অবশিষ্ট অংশ';
$lang['of'] = 'এর';
$lang['guardian'] = 'অভিভাবক';
$lang['play_truant_fine'] = 'স্কুল পলাতক জরিমানা';//confused
$lang['absent_fine'] = 'অনুপস্থিত জরিমানা';
$lang['audio'] = 'অডিও';
$lang['information'] = 'তথ্য';
$lang['actual_amount'] = 'প্রকৃত পরিমাণ';
$lang['waiver'] = 'ছাড়';//confused
$lang['after'] = 'পরে';
$lang['already'] = 'ইতিমধ্যে';
$lang['due'] = 'বকেয়া';
$lang['discount'] = 'ডিসকাউন্ট';
$lang['payable'] = 'প্রদেয়';//confused
$lang['voucher_no'] = 'ভাউচার নং.';
$lang['and'] = 'এবং';
$lang['with_institute_name'] = 'ইনস্টিটিউট এর নাম সহ';
$lang['deposit_method'] = 'আমানত পদ্ধতি';
$lang['payment_method'] = 'মূল্যপরিশোধ পদ্ধতি';
$lang['annual'] = 'বার্ষিক';
$lang['tri-annual'] = 'ত্রি-বার্ষিক';
$lang['quaterly'] = 'ত্রৈমাসিক';
$lang['receiver'] = 'গ্রাহক';
$lang['transport'] = 'পরিবহন';
$lang['late_fee'] = 'বিলম্ব জরিমানা';
$lang['do_you'] = 'আপনি কি';
$lang['do_you_collect_late_fee'] = 'আপনি কি বিলম্ব জরিমানি সংগ্রহ করতে চান ?';
$lang['do_you_want_send_sms'] = 'আপনি কি এসএমএস পাঠাতে চান ?';
$lang['do_you_want_receipt'] = 'আপনি কি রসিদ চান';
$lang['cash'] = 'নগদ';
$lang['bank'] = 'ব্যাংক';
$lang['teacher'] = 'শিক্ষক';
$lang['staff'] = 'কর্মী';
$lang['teachers'] = "শিক্ষক/শিক্ষিকাদের";
$lang['teacher_id'] = 'শিক্ষকের আইডি';
$lang['return'] = 'ফেরৎ দেত্তয়া';
$lang['with'] = 'সঙ্গে';
$lang['without'] = 'ছাড়া';
$lang['pay_to'] = 'পরিশোধ করো';
$lang['receipt'] = 'রিসিট';
$lang['login'] = 'লগইন';
$lang['excel'] = 'এক্সেল';
$lang['view'] = 'দেখুন';
$lang['academic'] = 'একাডেমিক';
$lang['designation'] = 'উপাধি';
$lang['description'] = 'বর্ণনা';
$lang['signature'] = 'স্বাক্ষর';
$lang['list_for'] = 'তালিকার জন্য';
$lang['history'] = 'ইতিহাস';
$lang['accountant'] = 'হিসাবরক্ষক';
$lang['only'] = 'মাত্র';
$lang['approve'] = 'অনুমোদন';
$lang['auto'] = 'স্বয়ংক্রিয়';
$lang['show'] = 'প্রদর্শন';
$lang['setting'] = 'সেটিং';
$lang['is_in'] = 'Is In';
$lang['from'] = 'থেকে';
$lang['to'] = 'নিকট';
$lang['scholarship'] = 'বৃত্তি';
$lang['set'] = 'সেট';
$lang['view_reort'] = 'রিপোর্ট দেখুন';
$lang['overview'] = 'ওভারভিউ';
$lang['last_10_receipt'] = 'সর্বশেষ ১০ শিক্ষার্থীর ফি রিসিট';
$lang['last_7_days_collection'] = 'সর্বশেষ 7 দিনের আয়';
$lang['allocation'] = 'নির্ধারণ';
$lang['voice'] = 'বাক্য';
$lang['notify'] = 'অবহিত করুন';
$lang['birthday'] = 'জন্মদিন';
$lang['extra'] = 'অতিরিক্ত';
$lang['grace'] = 'গ্রেস';
$lang['user'] = 'ব্যবহারকারী';
$lang['management'] = 'ব্যবস্থাপনা';
$lang['role'] = 'ভূমিকা';
$lang['notice'] = 'বিজ্ঞপ্তি';
$lang['managing_committee'] = 'পরিচালনা কমিটি';
$lang['headmaster'] = 'প্রধানশিক্ষক';
$lang['administration'] = 'প্রশাসন';
$lang['fund'] = 'তহবিল';
$lang['cost_center'] = 'ব্যয় কেন্দ্র';
$lang['basic'] = 'বেসিক';
$lang['accounts'] = 'একাউন্টস';
$lang['number_of_post'] = 'পদবি সংখ্যা';
$lang['shorting_order'] = 'ক্রম নাম্বার';
$lang['if_any'] = 'যদি থাকে';
$lang['organization']  = "প্রতিষ্ঠান";
$lang['address'] = 'ঠিকানা';
$lang['quick'] = "দ্রুত";
$lang['progress_report'] = "শিক্ষার্থীদের অগ্রগতি রিপোর্ট";
$lang['birth_certificate_no'] = "জন্ম নিবন্ধন নম্বর";
$lang['routine'] = "রুটিন";
$lang['accessories'] = "আনুষঙ্গিক উপকরণ";

$lang['male']="পুরুষ";
$lang['female']="মহিলা";
$lang['client_delete_msg']="কেস নির্ভরতার কারণে এই মক্কেল মোছা যাবে না।";
$lang['file_colour']="ফাইলের রঙ";



$lang['judge'] = "বিচারক";
$lang['judge_list'] = "বিচারকের তালিকা";
$lang['is_active'] = "সক্রিয়?";
$lang['vendor_list'] = "বিক্রেতার তালিকা";
$lang['vendor'] = "বিক্রেতা";
$lang['company'] = "প্রতিষ্ঠান";
$lang['first'] = "প্রথম";
$lang['last'] = "শেষ";
$lang['number'] = "নম্বর";
$lang['gstin'] = "GSTIN";
$lang['pan'] = "PAN";
$lang['country'] = "দেশ";
$lang['district'] = "জেলা";
$lang['upazila'] = "উপজেলা";
$lang['alternate'] = "বিকল্প";
$lang['invoice_no'] = "চালান নং";
$lang['bill'] = "বিল";
$lang['member'] = "সদস্য";
$lang['zip_code'] = "জিপ কোড";
$lang['confirm'] = "নিশ্চিত করা";
$lang['password'] = "পাসওয়ার্ড";
$lang['case'] = "কেস";
$lang['petitioner'] = "বাদী পক্ষ";
$lang['respondent'] = "বিবাদী পক্ষ";
$lang['advocate'] = "উকিল";
$lang['act'] = "আইন";
$lang['filling'] = "ফিলিং";
$lang['registration'] = "নিবন্ধন";
$lang['hearing'] = "মকদ্দমার শুনানি";
$lang['cnr'] = "সিএনআর";
$lang['court'] = "আদালত";
$lang['court_sub_type'] = "আদালতের উপ ধরণ";
$lang['client'] = "মক্কেল";
$lang['&'] = "&";
$lang['vs'] = "বনাম";
$lang['next'] = "পরবর্তী";
$lang['police'] = "";
$lang['station'] = "থানা";
$lang['appointments'] = "অ্যাপয়েন্টমেন্ট";
$lang['task'] = "করণীয় কাজ";
$lang['team'] = "কর্মিদল";
$lang['tax'] = "শুল্ক";
$lang['clients'] = "মক্কেল";

$lang['services'] = "সেবা";
$lang['reference'] = "সম্পর্কস্থাপন";
$lang['appointment'] = "সাক্ষাৎ";
$lang['notes'] = "মন্তব্য";
$lang['tasks'] = "করণীয় কাজ";
$lang['related'] = "সম্পর্কিত";
$lang['start_date'] = "শুরুর তারিখ";
$lang['deadline'] = "শেষ তারিখ";
$lang['members'] = "সদস্যরা";
$lang['assign_to'] = "বরাদ্দ করুন";
$lang['case'] = "মামলা";
$lang['employee'] = "কর্মচারী";
$lang['appointment_status'] = "নিয়োগের স্থিতি";
$lang['hiring_types'] = "নিয়োগের প্রকারগুলি";
$lang['hiring_status'] = "নিয়োগের স্থিতি";
$lang['color'] = "রং";
$lang['office'] = "অফিস";


$lang['client_details'] = "বাদী/বিবাদীর বিবরণ";
$lang['client_name'] = "বাদী/বিবাদীর নাম";
$lang['respondent_or_pettion'] = "বাদী/বিবাদীর";
$lang['respondent_name'] = "বিবাদী পক্ষের নাম";
$lang['respondent_advocate'] = "বিবাদী পক্ষের উকিল";
$lang['petitioner_name'] = "বাদী পক্ষের নাম";
$lang['petitioner_advocate'] = "বাদী পক্ষের উকিল";
$lang['add_more'] = "আরও যুক্ত করুন";
$lang['case_details'] = "মামলার বিবরণ";
$lang['case_no'] = "মামলার নং";
$lang['case_type'] = "মামলার ধরণ";
$lang['case_sub_type'] = "মামলার উপ ধরণ";
$lang['case_status'] = "মামলার ধরণ";
$lang['high'] = "উচ্চ";
$lang['medium'] = "মধ্যম";
$lang['low'] = "নিম্ন";
$lang['parties_name'] = "পক্ষগণের নাম";
$lang['reason_for_assignment'] = "ধার্যকরণের কারণ";
$lang['first_hearing_date'] = "পরবর্তী তারিখ";
$lang['previous_hearing_date'] = "পূর্ববর্তী তারিখ";
$lang['registration_date'] = "মামলার তারিখ";
$lang['reason_for_next_date'] = "পরবর্তী তারিখের কারণ";
$lang['priority_of_the_case'] = "মামলার  অগ্রাধিকার";
$lang['fir_details'] = "এফআইআর বিবরণ ";
$lang['fir_number'] = "এফআইআর নাম্বার";
$lang['fir_date'] = "এফআইআর তারিখ";
$lang['fir'] = "এফআইআর";
$lang['court_details'] = "আদালতের বিবরণ";
$lang['court_type'] = "আদালতের ধরণ";
$lang['judge_type'] = "বিচারকের ধরণ";
$lang['judge_name'] = "বিচারকের নাম";
$lang['courts'] = "আদালতের";
$lang['task_assign'] = "মামলা দেখাশুনার দায়িত্ব";
$lang['user_name'] = "উকিলের নাম";
$lang['client_enty_from'] = "বাদী/বিবাদী নতুন ফরম লিখন";
$lang['client_enty_from'] = "বাদী/বিবাদী নতুন ফরম লিখন";
$lang['case_entry'] = "মামলার ফরম লিখন";
$lang['task_entry'] = "অফিস অর্ডার";
$lang['district_wise_court_allowcations'] = "অধস্তন আদালতের মামলার কার্যতালিকা";
$lang['court_allowcations'] = "মামলার কার্যতালিকা";
$lang['serial_no'] = "ক্রমিক নং";
$lang['users_name']="ব্যবহারকারীর নাম";
$lang['total_person']="মোট ব্যক্তি";
$lang['gr_number']="জি.আর নাম্বার";
$lang['gr_date']="জি.আর তারিখ";


$lang['cr_case_no']="সি.আর মামলা নাম্বার";
$lang['order']="আদেশ";
$lang['filling_date']="ফাইলিং তারিখ";
$lang['view_report'] = 'রিপোর্ট দেখুন';
$lang['attendance_report']="হাজিরা রিপোর্ট";
$lang['bn_name']="বাংলা নাম";
$lang['division_list']="বিভাগের তালিকা";
$lang['district_list']="জেলার তালিকা";
$lang['division_name_english']="ইংরেজিতে বিভাগের নাম";
$lang['district_name_english']="ইংরেজিতে জেলার নাম";
$lang['district_name']="জেলার নাম";
$lang['file']="ফাইল";
$lang['file_upload']="ফাইল আপলোড";
$lang['file_upload_list']="ফাইলের  তালিকা";
$lang['file_add'] = 'নতুন ফাইল যুক্ত করুন';
$lang['download'] = 'ডাউনলোড';
$lang['division']="বিভাগ";
$lang['division_name']="বিভাগের নাম";
$lang['courts']="মামলার";
$lang['case_file_upload']="মামলার ফাইল আপলোড";
$lang['case_file_upload_list']="মামলার ফাইল আপলোড তালিকা";
$lang['case_file_name']="মামলার ফাইল নাম";
$lang['forms']="ফরমস";
$lang['form_type']="ফর্মের ধরন";
$lang['today_attendance']="আজকের হাজিরা";
$lang['todays']="আজকের";
$lang['read_more']="আরও পড়ুন";
$lang['numbers']="নাম্বার";
$lang['earlier_date']="পূর্বের তারিখ";
$lang['defendant']="বিবাদী";
$lang['nong']="নং";
$lang['dashboard_colour_settings']="ড্যাশবোর্ড কালার সেটিং";
$lang['set_permission']="পারমিশন সেট করুন";
$lang['party']="পক্ষ";
$lang['closed']="বন্ধ";
$lang['important']="গুরুত্বপূর্ণ";
$lang['archived']="সংরক্ষণাগারভুক্ত";
$lang['cases']="মামলাগুলি";
$lang['border']="বর্ডার";
$lang['left_menu_background_color']="লেফট মেনু ব্যাকগ্রাউন্ড কালার ";
$lang['top_bar_background_color']="টপ বার ব্যাকগ্রাউন্ড কালার ";
$lang['top_menu_show_hide_button_color']="টপ মেনু শো/হাইড বাটন ব্যাকগ্রাউন্ড ";
$lang['reset']="রিসেট ";
$lang['due_to_history_dependency']="ইতিহাস নির্ভরতা কারণে";
$lang['table_header_row_color']="টেবিল শিরোনাম সারি রঙ";
$lang['table_body_row_color']="টেবিল বডি সারি রঙ";
$lang['table_row_color_enable_for_eport']="প্রতিবেদনের জন্য সারণি সারি রঙ সক্ষম করুন";
$lang['upazila_name_english']="ইংরেজিতে উপজেলার নাম";
$lang['upazila_list']="উপজেলার তালিকা";
$lang['district_name']="জেলার নাম";
$lang['order_sheet']="আদেশ পত্র";
$lang['action']="পদক্ষেপ";
$lang['entry']="এন্ট্রি";
$lang['missed']="মিস হওয়া";
$lang['behalf_of_the_employer']="নিযুক্ত পক্ষে";
$lang['advocate_setting_message']="অ্যাডভোকেট সেটিংস কনফিগার করুন.";
$lang['sender']="প্রেরক";
$lang['number_of_sms']="এসএমএসের সংখ্যা";
$lang['recharge_history']="রিচার্জের ইতিহাস";
$lang['is_any_number']="যে কোনও নাম্বার";
$lang['write_message']="বার্তা লিখুন";
$lang['link']="লিংক";
$lang['facebook']="ফেসবুক";
$lang['youtube']="ইউটিউব";
$lang['twitter']="টুইটার";
$lang['linkedin']="লিংকডিন";
$lang['instagram']="ইনস্টাগ্রাম";
$lang['day_of_advance_message']="অগ্রীম বার্তা প্রেরণের দিন";
$lang['section_of_the_case']="মামলার ধারা";
$lang['total_running_case']="মোট চলমান মামলা";
$lang['total_colsed_case']="মোট বন্ধ মামলা";
$lang['total_missaed_attendance']="মোট মিস হওয়া হাজিরা";
$lang['total_clients']="মোট বাদী/বিবাদী";
$lang['total_importan_case']="মোট গুরুত্বপূর্ণ মামলা";
$lang['chief']="প্রধান";
$lang['tomorrow_attendence']="আগামীকালের হাজিরা";
