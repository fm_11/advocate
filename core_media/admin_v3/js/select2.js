(function($) {
	'use strict';

	if ($(".js-example-basic-single").length) {
		$(".js-example-basic-single").select2();
	}
	if ($(".js-example-basic-multiple").length) {
		$(".js-example-basic-multiple").select2();
	}
})(jQuery);

$(document).on('keyup', ".select2-search__field", function(e) {
	//	alert(5555);
	//if Ctrl+A pressed
	if (e.keyCode == 65 && e.ctrlKey) {
		//Select only filtered results
		$.each($(".select2-results__options").find('li'), function(i, item) {
			$(".js-example-basic-multiple > option:contains('" + $(item).text() + "')").prop("selected", "selected");
		});
		//Remove entered letters and close suggestions
		$(this).val("").click();
		//Select with select2
		$(".js-example-basic-multiple").trigger("change");
	}
});
