/* 01. Css Loading Util */
function deleteConfirm() {
	var result = confirm("Are you sure to delete?");
	if (result == true) {
		return true;
	} else {
		return false;
	}
}

$(document).ready(function() {

	var counter = $('#myTable tbody tr').length;
	$("#addrow").on("click", function() {

		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><input type="text" autocomplete="off"  required class="small_custom_input" name="start_range_' + counter + '"/></td>';
		cols += '<td><input type="text" autocomplete="off"  required class="small_custom_input" name="end_range_' + counter + '"/></td>';
		cols += '<td><input type="text" autocomplete="off"  required class="small_custom_input" name="alpha_gpa_' + counter + '"/></td>';
		cols += '<td><input type="text" autocomplete="off"  required class="small_custom_input" name="numeric_gpa_' + counter + '"/></td>';



		cols += '<td><button style="padding: 0.20rem .80rem;" class="ibtnDel btn btn-danger btn-sm" type="button"><i class="ti-trash"></i></button></td>';
		newRow.append(cols);
		$("table.grade-list").append(newRow);
		counter++;
		//alert(counter);
		$("#total_submited_row").val(counter);
	});



	$("table.grade-list").on("click", ".ibtnDel", function(event) {
		$(this).closest("tr").remove();
		counter -= 1
		$("#total_submited_row").val(counter);
	});


});



function image_validate(fileId, in_height, in_width) {

	//Get reference of FileUpload.
	var fileUpload = document.getElementById(fileId);
	var imagefilename = $('#' + fileId).val();
	if (imagefilename != '') {
		//Check whether the file is valid Image.
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
		if (regex.test(fileUpload.value.toLowerCase())) {

			//Check whether HTML5 is supported.
			if (typeof(fileUpload.files) != "undefined") {
				//Initiate the FileReader object.
				var reader = new FileReader();
				//Read the contents of Image File.
				reader.readAsDataURL(fileUpload.files[0]);
				reader.onload = function(e) {
					//Initiate the JavaScript Image object.
					var image = new Image();

					//Set the Base64 string return from FileReader as source.
					image.src = e.target.result;

					//Validate the File Height and Width.
					image.onload = function() {
						var height = this.height;
						var width = this.width;
						if (height > in_height || width > in_width) {
							$("#file_error").html("Height and Width must not exceed 200px.");
							return false;
						}
						$("#file_error").html("Uploaded image has valid Height and Width.");
						return true;
					};

				}
			} else {
				$("#file_error").html("This browser does not support HTML5.");
				return false;
			}
		} else {
			$("#file_error").html("Please select a valid Image file");
			$(".imageInputBoxForVal").css("border-color", "#FF0000");
			return false;
		}


		$("#file_error").html("");
		$(".imageInputBoxForVal").css("border-color", "#F0F0F0");
		var file_size = $('#' + fileId)[0].files[0].size;
		if (file_size > 2097152) {
			$("#file_error").html("File size is greater than 2MB");
			$(".imageInputBoxForVal").css("border-color", "#FF0000");
			return false;
		}
		return true;
	}
	return true;

}


$(function() {
	$(".date").datepicker({
		autoclose: true,
		todayHighlight: true,
		format: 'dd-mm-yyyy',
	}).datepicker();
});
